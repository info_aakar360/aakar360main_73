<?php echo $header?>
<div id="mountRoot" style="min-height:500px;margin-top:-2px;">
    <div data-reactroot="" data-reactid="1" data-react-checksum="525647675">
        <div data-reactid="2">
            <div class="tac-container clearfix" data-reactid="3">
                <div class="tac" data-reactid="9">
                    <div class="XYZ container">
                        <div class="content clearfix">
                            <div class="sale-header"><u>Returns Policy </u></div>
                            <div class="sale-header">
                                <div> Returns is a scheme provided by respective sellers directly under this policy in terms of which the option of exchange, replacement and/ or refund is offered by the respective sellers to you. All products listed under a particular category may not have the same returns policy. For all products, the returns/replacement policy provided on the product page shall prevail over the general returns policy. Do refer the respective item's applicable return/replacement policy on the product page for any exceptions to this returns policy and the table below
                                </div>
                                <p>The return policy is divided into three parts; Do read all sections carefully to understand the conditions and cases under which returns will be accepted.</p>
                                <p>In case of returns where you would like item(s) to be picked up from a different address, the address can only be changed if pick-up service is available at the new address</p>
                                <p>The field executive will refuse to accept the return if any of the above conditions are not met.</p>
                                <p>For any products for which a refund is to be given, the refund will be processed once the returned product has been received by the seller.</p>
                                <b><ul>General Rules for a successful Return</ul></b>
                                <ol>
                                    <li>In certain cases where the seller is unable to process a replacement for any reason whatsoever, a refund will be given.</li>
                                    <li>During open box deliveries, while accepting your order, if you received a different or a damaged product, you will be given a refund (on the spot refunds for cash-on-delivery orders). Once you have accepted an open box delivery, no return request will be processed, except for manufacturing defects. In such cases, this category-specific replacement/return general conditions will be applicable. Click here to know more about Open Box Delivery</li>
                                    <li>For products where installation is provided by Aakar360's service partners, do not open the product packaging by yourself. Aakar360 authorised personnel shall help in unboxing and installation of the product.</li>
                                    <li>For Furniture, any product related issues will be checked by an authorised service personnel (free of cost) and attempted to be resolved by replacing the faulty/ defective part of the product. Full replacement will be provided only in cases where the service personnel opines that replacing the faulty/defective part will not resolve the issue.</li>
                                </ol>
                                <b><ul>Wrong Delivery - (Customer received delivery message, product not delivered):</ul></b>
                                <p>'In case the product was not delivered and you received a delivery confirmation email/SMS, report the issue within 7 days from the date of delivery confirmation for the seller to investigate.'</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer?>