<?php
echo $header;
?>

<?php if(isset($_GET['id'])){ ?>
    <div class="row">
        <div class="col-lg-offset-2 col-lg-8" >
            <form action="product-discount?id=<?=$_GET['id'];?>" method="post" class="form-horizontal" id="modal-form" enctype="multipart/form-data" style="max-width: 100%;padding: 0;">
                <h2>Add Discount </h2>
                <div class="panel panel-default">
                    <div class="panel-heading"> Add Discount  of Product <span style="color: green; font-weight: 800;text-transform: uppercase;"> <?=$products->title?></span></div>
                    <div class="panel-body">
                        <form action="add_discount?add='.$_GET['add_discount'].'" method="post" class="form-horizontal" enctype="multipart/form-data" style="max-width: 100%;">
                            <?=csrf_field();?>

                            <fieldset>
                                <div class="form-group col-md-3" style="padding-left: 30px;padding-right: 30px;">
                                    <label class="control-label">User Type</label>
                                    <select  name="user_type" class="form-control" id="user_type" required="required">
                                        <option value="">Select User Type </option>
                                        <option value="individual"> Individual Buyer </option>
                                        <option value="institutional"> Institutional Buyer</option>
                                    </select>
                                </div>

                                <div class="form-group col-md-2" style="padding-left: 30px;padding-right: 30px;">
                                    <label class="control-label">Quantity</label>
                                    <input name="discount_pid" type="hidden" class="form-control" required="required" value="<?=$_GET['id'];?>"/>
                                    <input name="quantity" type="text" class="form-control" required="required"/>
                                </div>
                                <div class="form-group col-md-2" style="padding-left: 30px;padding-right: 30px;">
                                    <label class="control-label">Discount</label>
                                    <input name="discount" type="text" class="form-control" required="required"/>
                                </div>
                                <div class="form-group col-md-3" style="padding-left: 30px;padding-right: 30px;">
                                    <label class="control-label">Discount Type</label>
                                    <select name="discount_type" class="form-control" required="required">
                                        <option value="">  Select Discount Type </option>
                                        <option value="%"> % </option>
                                        <option value="Flat"> Flat </option>
                                    </select>
                                </div>
                                <input name="add_discount" type="submit" value="Add Discount" class="btn btn-primary col-md-2" style="padding-left: 30px;padding-right: 30px;margin-top: 20px;margin-left: 40px;"/>
                            </fieldset>
                        </form>
                        <div class="table-responsive">
                            <b>All Discounts</b>
                            <table class="table table-striped table-bordered" id="datatable-editable2">
                                <thead>
                                <tr class="bg-blue">
                                    <th>Sr. No.</th>
                                    <th>Type</th>
                                    <th>Quantity</th>
                                    <th>Discount</th>
                                    <th>Discount Type</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $sr = 1;

                                foreach ($discounts as $var){

                                    echo'<tr>
								<td>'.$sr.'</td>
								<td>'.$var->user_type.'</td>
								<td>'.$var->quantity.'</td>
								<td>'.$var->discount.'</td>
								<td>'.$var->discount_type.'</td>
								<td>'?>
									<a href="product-discount?id=<?=$_GET['id'];?>&delete=<?=$var->id;?>"
									onClick="return confirm('Do you really want to delete ?');"><i class="icon-trash"></i></a>
									<a href="javascript:void(0);" onClick="editDiscount(<?=$var->id;?>)" id ="product_discount"><i class="icon-pencil"></i></a>
								</td>
							  </tr>
							  <?php
                                    $sr++;}
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">

    </div>
<?php } ?>
<script type="text/javascript">
    $(function() {
//----- OPEN
        $('[data-popup-open]').on('click', function(e) {
            var targeted_popup_class = jQuery(this).attr('data-popup-open');
            $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
            e.preventDefault();
        });
//----- CLOSE
        $('[data-popup-close]').on('click', function(e) {
            var targeted_popup_class = jQuery(this).attr('data-popup-close');
            $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
            e.preventDefault();
        });
    });

        function editDiscount1(){
            $('#edit_discount').click();
        }

    function editDiscount(vid){
        $('#edit_discount').click();
        $.ajax({
            url: '{{url('retailGetDiscount')}}',
            type: 'post',
            data: 'discount_id='+vid+'&_token=<?=csrf_token(); ?>',
            success: function(datax){
                var data = JSON.parse(datax);
                $('#modal-form input[name=dis_id]').val(data.id);
                $('#modal-form input[name=quantity]').val(data.quantity);
                $('#modal-form input[name=discount]').val(data.discount);
                $('#modal-form input[name=discount_type]').val(data.discount_type);
                if(data.discount_type == '%'){
                    $('#modal-form .discount_type').html('<label class="control-label">Discount Type</label>'
                        +'<select name="discount_type" class="form-control" required="required">'
                        +'	<option value=""> Select Discount Type </option>'
                        +'	<option value="%" selected="selected"> % </option>'
                        +'	<option value="Flat"> Flat </option>'
                        +'</select>');
                }else{
                    $('#modal-form .discount_type').html('<label class="control-label">Discount Type</label>'
                        +'<select name="discount_type" class="form-control" required="required">'
                        +'	<option value=""> Select Discount Type </option>'
                        +'	<option value="%"> % </option>'
                        +'	<option value="Flat" selected="selected"> Flat </option>'
                        +'</select>');
                }
                if(data.user_type == 'individual'){
                    $('#modal-form .user_type').html('<label class="control-label">User Type</label>'
                        +'<select name="user_type" class="form-control" required="required">'
                        +'	<option value=""> Select User Type </option>'
                        +'	<option value="individual" selected="selected"> Individual </option>'
                        +'	<option value="institutional"> Institutional </option>'
                        +'</select>');
                }else{
                    $('#modal-form .user_type').html('<label class="control-label">User Type</label>'
                        +'<select name="user_type" class="form-control" required="required">'
                        +'	<option value=""> Select User Type </option>'
                        +'	<option value="individual" > Individual </option>'
                        +'	<option value="institutional" selected="selected"> Institutional </option>'
                        +'</select>');
                }
                $('#edit-dis-button').trigger('click');
            }
        });
    }
</script>
<?php
echo $footer;
?>
<?php if(isset($_GET['id'])){ ?>

    <!-- Edit Discount  Popup Start -->
    <a class="btn btn-primary hidden" id="edit_discount" data-popup-open="popup-sku" href="#">Get Started !</a>
    <div class="popup" id="popup-sku" data-popup="popup-sku">
        <div class="popup-inner" style="padding-top: 120px;">
            <div class="row">
                <form action="product-discount?id=<?=$_GET['id'];?>" method="post" class="form-horizontal" id="modal-form" enctype="multipart/form-data" style="max-width: 100%;padding: 0;">
                    <?php echo csrf_field(); ?>
                    <input type="hidden" name="dis_id" value=""/>
                    <fieldset>

                        <div class="form-group col-md-12 user_type" style="padding-left: 30px;padding-right: 30px;">
                            <label class="control-label">User Type</label>
                            <select  name="user_type" class="form-control" required="required">
                                <option value="">Select User Type </option>
                                <option value="individual"> Individual Buyer </option>
                                <option value="institutional"> Institutional Buyer</option>
                            </select>
                        </div>
                        <div class="form-group col-md-12" style="padding-left: 30px;padding-right: 30px;">
                            <label class="control-label">Quantity</label>
                            <input name="quantity" type="text" class="form-control" required="required"/>
                        </div>
                        <div class="form-group col-md-12" style="padding-left: 30px;padding-right: 30px;">
                            <label class="control-label">Discount</label>
                            <input name="discount" type="text" class="form-control" required="required"/>
                        </div>
                        <div class="form-group col-md-12 discount_type" style="padding-left: 30px;padding-right: 30px;" >
                            <label class="control-label">Discount Type</label>
                            <select name="discount_type" class="form-control" required="required">
                                <option value=""> Select Discount Type </option>
                                <option value="%"> % </option>
                                <option value="Flat"> Flat </option>
                            </select>
                        </div>
                        <center><input name="edit_discount" type="submit" value="Edit Discount" class="btn btn-primary"/><center>
                    </fieldset>
                </form>

            </div>

            <a class="popup-close" data-popup-close="popup-sku" href="#">x</a>
        </div>
    </div>
    <!-- Edit Discount Popup End -->
<?php } ?>
