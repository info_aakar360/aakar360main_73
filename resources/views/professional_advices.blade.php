<?php echo $header?>

<div class="profile-root bg-white ng-scope">
    <div class="clearfix buffer-top text-center wrapper-1400 bg-color">
        <div class="clearfix wrapper-1140 two-bgs">
            <?php if(customer('header_image') != '' && customer('header_image_1') == ''){ ?>
                <div class="col-sm-12">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/user_image/<?php echo customer('header_image'); ?>" class="cover">
                    </div>
                </div>
            <?php } elseif(customer('header_image') == '' && customer('header_image_1') != ''){ ?>
                <div class="col-sm-12">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/user_image/<?php echo customer('header_image_1'); ?>" class="cover">
                    </div>
                </div>
            <?php } elseif(customer('header_image') == '' && customer('header_image_1') == ''){ ?>
                <div class="col-sm-12">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/banners/<?=$dimg->image; ?>" class="cover">
                    </div>
                </div>
            <?php } elseif(customer('header_image') != '' && customer('header_image_1') != ''){ ?>
                <div class="col-sm-6" style=" padding-left: 2px; padding-right: 2px;">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/user_image/<?php echo Customer('header_image'); ?>" class="cover">
                    </div>
                </div>
                <div class="col-sm-6 hidden-xs" style=" padding-left: 2px; padding-right: 2px;">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/user_image/<?php echo Customer('header_image_1'); ?>" class="cover">
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="profile-overlay clearfix wrapper-980 bgs-white ng-isolate-scope" style="width: 980px;">
            <img itemprop="image" class="profile-icon" src="assets/user_image/<?php echo Customer('image'); ?>">
            <h1 itemprop="name" class="name hand-writing font-44 font-36-xs">
                <a itemprop="url" class="url">
                    <?php echo Customer('name'); ?>
                </a>
                <p style="text-align: center;"><a href="#" onClick="getprofile()" class="btn">My Profile</a></p>
            </h1>
            <div class="sans-light font-15 line-height-md hidden-xs">
                <p style="text-align: justify; padding: 30px;">Vanessa&nbsp;has a Bachelor’s Degree in Architecture and a Minor in Interior Design from the University of Oklahoma. Her architecture education has given her a strong passion for incorporating clean lines, unexpected color palettes, and simple but meaningful details into the spaces she designs.</p>
            </div>
            <div class="sans-light font-15 line-height-md hidden-xs" style="padding-bottom: 0px; position: absolute; width: 100%; bottom: 0;">
                <p style="text-align: center;">
                    <a href="{{route('retailProfessionalProduct')}}" class="btn col-lg-2">Products</a>
                    <a href="{{route('retailProfessionalServices')}}" class="btn col-lg-2">Services</a>
                    <a href="{{route('retailProfessionalDesign')}}" class="btn col-lg-2">Design</a>
                    <a href="/professional_advices" class="btn btn-primary col-lg-2">Advices</a>
                    <a href="{{route('retailMyProjects')}}"  class="btn col-lg-2">My Projects</a>
                </p>
            </div>
        </div>

        <div class="clearfix wrapper-980 bg-white m-t-xs m-b-md">
            <div class="about-designer clearfix">
                <div class="text-left">
                    <section class="at-property-sec" style="padding-top: 0px;">
                        <div class="container" style="margin-left: -15px; width: 1140px;">
                            <ul class="nav nav-pills" style="background-color: white;">
                                <li class="active"><a href="#liked" data-toggle="tab" >Liked Advices</a></li>
                                <li><a href="#saved" data-toggle="tab" >Saved Advices</a></li>

                            </ul>

                            <div class="tab-content" style="padding-top: 60px;">
                                <div id="liked" class="tab-pane fade in active">
                                    <br>
                                    <?php foreach ($liked as $pos){
                                        $post = DB::select("SELECT * FROM blog WHERE id = '".$pos->advice_id."'")[0];?>
                                        <div class="profile-overlay clearfix wrapper-1140 ng-isolate-scope" style="margin-bottom: 75px;">
                                            <a data-title="<?php echo translate($post->title); ?>" class="smooth" href="blog/<?php echo path($post->title,$post->id); ?>">
                                                <div class="col-lg-12">
                                                    <div class="col-lg-6 col-md-6">
                                                        <img itemprop="image" src="<?php echo url('/assets/blog/'.$post->images); ?>" style="width: 500px; height: 350px;">
                                                    </div>
                                                    <div class="col-lg-6 col-md-6" style="padding-top: 25px;">
                                                        <div style="background-color: white;height: 300px;margin-left: -150px;">
                                                            <h3 style="padding: 25px;"><?=$post->title; ?>

                                                                <p style="padding-top: 10px; text-transform: none;"><small class="pull-left"><i class="fa fa-bars"></i> <?php echo getadvicesCategoryName($post->category); ?></small>
                                                                    <small class="pull-right"><i class="fa fa-calendar"></i> <?=$post->date; ?></small>
                                                                </p>
                                                            </h3>

                                                            <p style="padding: 25px; padding-top: 0px;"><?=$post->short_des; ?></p>

                                                        </div>
                                                        <div class="" style="text-align: center;bottom: 10px;position: absolute;margin-left: -150px;left: 0;right: 0;">
                                                            <?php
                                                            $total_ratings = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 1 AND advice_id = ".$post->id)[0]->count;
                                                            $total_reviews = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 1 AND review <> '' AND advice_id = ".$post->id)[0]->count;
                                                            $like = DB::select("SELECT COUNT(*) as count FROM advices_like WHERE advice_id = ".$post->id)[0]->count;
                                                            $rating = 0;
                                                            if ($total_ratings > 0){
                                                                $rating_summ = DB::select("SELECT SUM(rating) as sum FROM advices_reviews WHERE active = 1 AND advice_id = ".$post->id)[0]->sum;
                                                                $rating = $rating_summ / $total_ratings;
                                                            }
                                                            $reviews = DB::select("SELECT * FROM advices_reviews WHERE advice_id = ".$post->id." AND active = 1 ORDER BY time DESC");
                                                            $faqs = DB::select("SELECT * FROM advices_faq WHERE advice_id = ".$post->id." AND status = 'Approved' ORDER BY id DESC");
                                                            $rating = DB::select("SELECT count(*) as total_user, SUM(rating) as total_rating FROM advices_reviews WHERE active = '1' AND advice_id = '".$post->id."'")[0];
                                                            $total_rating =  $rating->total_rating;
                                                            $total_user = $rating->total_user;
                                                            if($total_rating==0){
                                                                $avg_rating = 0;
                                                            }
                                                            else{
                                                                $avg_rating = round($total_rating/$total_user,1);
                                                            }
                                                            ?>

                                                            <p style="width: 25%; float: left; bottom: 10px;"> <i class="fa fa-eye"></i> <?=$post->visits; ?> Visits</p>
                                                            <p style="width: 25%; float: left; bottom: 10px;"><i class="fa fa-pencil-square-o"></i> <?=$total_reviews; ?> Reviews</p>
                                                            <p style="width: 25%; float: left; bottom: 10px;"><i class="fa fa-star"></i> <?=$avg_rating; ?> Ratings</p>
                                                            <p style="width: 25%; float: left; bottom: 10px;"><i class="fa fa-thumbs-up"></i> <?=$like; ?> Likes</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    <?php } ?>
                                    <?php if (count($liked) == 0) {?>
                                        <table>
                                            <tr>
                                                <td>
                                                    <i class="icon-basket"></i>
                                                    <?=translate('You do not have any liked advices!')?>
                                                </td>
                                            </tr>
                                        </table>
                                    <?php }?>
                                </div>

                                <div id="saved" class="tab-pane fade">
                                    <br>
                                    <?php foreach ($saved as $pos){
                                        $post = DB::select("SELECT * FROM blog WHERE id = '".$pos->advice_id."'")[0];?>
                                        <div class="profile-overlay clearfix wrapper-1140 ng-isolate-scope" style="margin-bottom: 75px;">
                                            <a data-title="<?php echo translate($post->title); ?>" class="smooth" href="blog/<?php echo path($post->title,$post->id); ?>">
                                                <div class="col-lg-12">
                                                    <div class="col-lg-6 col-md-6">
                                                        <img itemprop="image" src="<?php echo url('/assets/blog/'.$post->images); ?>" style="width: 500px; height: 350px;">
                                                    </div>
                                                    <div class="col-lg-6 col-md-6" style="padding-top: 25px;">
                                                        <div style="background-color: white;height: 300px;margin-left: -150px;">
                                                            <h3 style="padding: 25px;"><?=$post->title; ?>

                                                                <p style="padding-top: 10px; text-transform: none;"><small class="pull-left"><i class="fa fa-bars"></i> <?php echo getadvicesCategoryName($post->category); ?></small>
                                                                    <small class="pull-right"><i class="fa fa-calendar"></i> <?=$post->date; ?></small>
                                                                </p>
                                                            </h3>

                                                            <p style="padding: 25px; padding-top: 0px;"><?=$post->short_des; ?></p>

                                                        </div>
                                                        <div class="" style="text-align: center;bottom: 10px;position: absolute;margin-left: -150px;left: 0;right: 0;">
                                                            <?php
                                                            $total_ratings = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 1 AND advice_id = ".$post->id)[0]->count;
                                                            $total_reviews = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 1 AND review <> '' AND advice_id = ".$post->id)[0]->count;
                                                            $like = DB::select("SELECT COUNT(*) as count FROM advices_like WHERE advice_id = ".$post->id)[0]->count;
                                                            $rating = 0;
                                                            if ($total_ratings > 0){
                                                                $rating_summ = DB::select("SELECT SUM(rating) as sum FROM advices_reviews WHERE active = 1 AND advice_id = ".$post->id)[0]->sum;
                                                                $rating = $rating_summ / $total_ratings;
                                                            }
                                                            $reviews = DB::select("SELECT * FROM advices_reviews WHERE advice_id = ".$post->id." AND active = 1 ORDER BY time DESC");
                                                            $faqs = DB::select("SELECT * FROM advices_faq WHERE advice_id = ".$post->id." AND status = 'Approved' ORDER BY id DESC");
                                                            $rating = DB::select("SELECT count(*) as total_user, SUM(rating) as total_rating FROM advices_reviews WHERE active = '1' AND advice_id = '".$post->id."'")[0];
                                                            $total_rating =  $rating->total_rating;
                                                            $total_user = $rating->total_user;
                                                            if($total_rating==0){
                                                                $avg_rating = 0;
                                                            }
                                                            else{
                                                                $avg_rating = round($total_rating/$total_user,1);
                                                            }
                                                            ?>

                                                            <p style="width: 25%; float: left; bottom: 10px;"> <i class="fa fa-eye"></i> <?=$post->visits; ?> Visits</p>
                                                            <p style="width: 25%; float: left; bottom: 10px;"><i class="fa fa-pencil-square-o"></i> <?=$total_reviews; ?> Reviews</p>
                                                            <p style="width: 25%; float: left; bottom: 10px;"><i class="fa fa-star"></i> <?=$avg_rating; ?> Ratings</p>
                                                            <p style="width: 25%; float: left; bottom: 10px;"><i class="fa fa-thumbs-up"></i> <?=$like; ?> Likes</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    <?php } ?>
                                    <?php if (count($saved) == 0) {?>
                                        <table>
                                            <tr>
                                                <td>
                                                    <i class="icon-basket"></i>
                                                    <?=translate('You do not have any saved advices!')?>
                                                </td>
                                            </tr>
                                        </table>
                                    <?php }?>
                                </div>


                            </div>

                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    <?php echo $footer?>

    <script>
        $(function() {
//----- OPEN
            $('[data-popup-open]').on('click', function(e) {
                var targeted_popup_class = jQuery(this).attr('data-popup-open');
                $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
                e.preventDefault();
            });
//----- CLOSE
            $('[data-popup-close]').on('click', function(e) {
                var targeted_popup_class = jQuery(this).attr('data-popup-close');
                $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
                e.preventDefault();
            });
        });
        function getprofile(){
            event.preventDefault();
            $('#personal-modal').click();
        }
    </script>

    <a class="btn btn-primary hidden" id="personal-modal" data-popup-open="popup-personal" href="#">Get Started !</a>
    <div class="popup" id="popup-personal" data-popup="popup-personal">
        <div class="popup-inner" style="padding-top: 120px;">
            <div class="col-md-10 account" style="border: none; box-shadow: none;">
                <?php
                if (session('error') != ''){
                    echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
                }
                ?>
                <form action="<?php echo url('account'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal single">
                    <table class="responsive-table bordered" style="width: 50%; margin: auto;">
                        <tbody>
                        <tr>
                            <td>Name (*)</td>
                            <td>:</td>
                            <td><input type="text" name="name" required class="form-control" value="<?php echo Customer('name'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Mobile (*)</td>
                            <td>:</td>
                            <td><input type="text" name="mobile" required class="form-control" value="<?php echo Customer('mobile'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Alternate Mobile</td>
                            <td>:</td>
                            <td><input type="text" name="alternate_mobile" class="form-control" value="<?php echo Customer('alternate_mobile'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Email (*)</td>
                            <td>:</td>
                            <td><input type="text" name="email" required class="form-control" value="<?php echo Customer('email'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Profile Image</td>
                            <td>:</td>
                            <td><input type="file" name="image" class="form-control" ></td>
                        </tr>

                        <tr>
                            <td>Banner Image 1</td>
                            <td>:</td>
                            <td><input type="file" name="header_image" class="form-control" ></td>
                        </tr>

                        <tr>
                            <td>Banner Image 2</td>
                            <td>:</td>
                            <td><input type="file" name="header_image_1" class="form-control" ></td>
                        </tr>
                        <tr>
                            <td>Company (*)</td>
                            <td>:</td>
                            <td><input type="text" name="company" class="form-control" required value="<?php echo Customer('company'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Address Line 1 (*)</td>
                            <td>:</td>
                            <td><input type="text" name="address_line_1" required class="form-control" value="<?php echo Customer('address_line_1'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Address Line 2</td>
                            <td>:</td>
                            <td><input type="text" name="address_line_2" class="form-control" value="<?php echo Customer('address_line_2'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Country (*)</td>
                            <td>:</td>
                            <td>
                                <select name="country" id="country_shipping" required class="form-control" onChange="getStates(this.value, 'state_shipping')">
                                    <option value="<?php Customer('country'); ?>"><?php echo getCountryName(Customer('country')); ?></option>
                                    <option value="">Select country</option>
                                    <?php foreach($countries as $country){
                                        echo '<option value="'.$country->id.'">'.$country->name.'</option>';
                                    }?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>State (*)</td>
                            <td>:</td>
                            <td>
                                <select name="state" class="form-control" required id="state_shipping" onChange="getCities(this.value, 'city_shipping')">
                                    <option value="<?php Customer('state'); ?>"><?php echo getStateName(Customer('state')); ?></option>
                                    <option value="">Please select region, state or province</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>City (*)</td>
                            <td>:</td>
                            <td>
                                <select name="city" class="form-control" required id="city_shipping">
                                    <option value="<?php Customer('city'); ?>"><?php echo getCityName(Customer('city')); ?></option>
                                    <option value="">Please select city or locality</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Postcode (*)</td>
                            <td>:</td>
                            <td><input type="text" name="postcode" required class="form-control" value="<?php echo Customer('postcode'); ?>"></td>
                        </tr>
                        <?php echo csrf_field(); ?>
                        <tr>
                            <td colspan="3"><input type="submit" name="submit" value="Update" class="btn btn-primary btn-lg"></td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>
            <a class="popup-close" data-popup-close="popup-personal" href="#">x</a>
        </div>
    </div>