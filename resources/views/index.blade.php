<?php echo $data['header'];?>
<?php
if($data['banners']){ ?>
<section class="at-main-slider">
	<div class="flexslider">
		<ul class="slides">
            <?php foreach($data['banners'] as $banner){ ?>
			<li data-thumb="assets/banners/<?=$banner->image; ?>">
				<img src="assets/banners/<?=$banner->image; ?>" alt="">
                <?php if($banner->title != ''){ ?>
				<div class="inner-border">
					<div class="at-inner-title-box text-center">
						<h1 style="text-transform: none;"><?=$banner->title; ?></h1>
						<p style="text-transform: none;"><?=$banner->short_description; ?></p>
					</div>
				</div>
                <?php } ?>
			</li>
            <?php } ?>
		</ul>
	</div>
</section>
<?php } ?>
<?php if(Customer('id') == '') { ?>
<section style="padding-top: 40px;" id="sign">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="at-sec-title">
					<h2 style="font-size: 15px;">Sign Up <span>As</span></h2>
					<div class="at-heading-under-line">
						<div class="at-heading-inside-line"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="agent-carousel" data-slick='{"slidesToShow": 4, "slidesToScroll": 1}'>
					<div class="at-agent-col">
						<a class="" href="{{route('retail.retailRegister',['individual'])}}">
							<div class="col-sm-12 pcs hover-effect-border" style="background-color: #ffffff; padding: 0;">
								<div class="col-sm-12">
									<center style="padding-top: 20px;">
										<img src="<?=$data['tp'];?>/images/individual.png" style="width: 70px;height: auto;"/>
									</center>
									<h6 class="text-center">
										<h5>Individual Buyer</h5>
										<p>Starting your own Project?</p>
									</h6>
								</div>
							</div>
						</a>
					</div>
					<div class="at-agent-col">
						<a class="" href="{{route('business.institutionalRegister',['institutional'])}}">
							<div class="col-sm-12 pcs hover-effect-border" style="background-color: #ffffff; padding: 0;">
								<div class="col-sm-12">
									<center style="padding-top: 20px;">
										<img src="<?=$data['tp'];?>/images/institutional.png" style="width: 70px;height: auto;"/>
									</center>
									<h6 class="text-center">
										<h5>Institutional Buyer</h5>
										<p>Require Material in Bulk Quantity?</p>
									</h6>
								</div>
							</div>
						</a>
					</div>
					<div class="at-agent-col">
						<a class="" href="{{route('retail.retailRegister',['professional'])}}">
							<div class="col-sm-12 pcs hover-effect-border" style="background-color: #ffffff; padding: 0;">
								<div class="col-sm-12">
									<center style="padding-top: 20px;">
										<img src="<?=$data['tp'];?>/images/professional.png" style="width: 70px;height: auto;"/>
									</center>
									<h6 class="text-center">
										<h5>Professional Buyer</h5>
										<p style="line-height: 1.38;">Want Technology Driven Solution for your Client?</p>
									</h6>
								</div>
							</div>
						</a>
					</div>
					<div class="at-agent-col">
						<a class="" href="{{route('retail.retailRegister',['seller'])}}">
							<div class="col-sm-12 pcs hover-effect-border" style="background-color: #ffffff; padding: 0;">
								<div class="col-sm-12">
									<center style="padding-top: 20px;">
										<img src="<?=$data['tp'];?>/images/supplyer.png" style="width: 70px;height: auto;"/>
									</center>
									<h6 class="text-center">
										<h5>Manufacturer / Supplier</h5>
										<p>Looking for New Way to Reach?</p>
									</h6>
								</div>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php } ?>

<section class="at-product-category" style="margin: 0; padding: 50px 0;border:none;background: #ffffff;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="at-sec-title">
					<h2 style="font-size: 15px;">Shop Popular <span>Categories</span> &  <span>Brands</span></h2>
					<div class="at-heading-under-line">
						<div class="at-heading-inside-line"></div>
					</div>
				</div>
			</div>
		</div>
        <?php $pcs = getPCategories(); $i=1; ?>
		<div class="row">
			<div class="col-md-12">
				<div class="agent-carousel" data-slick='{"slidesToShow": 4, "slidesToScroll": 1}'>
                    <?php foreach($pcs as $pc){
                    if($i%2 !=0) echo '<div class="at-agent-col">';?>
					<a class="" href="{{route('retail.retailProducts',[$pc->path])}}">
						<div class="col-sm-12 pcs">
							<div class="col-sm-12 hover-effect-zoom pd-0 min180" style="background-color: #ffffff;">
								<center>
									<img src="assets/products/<?=$pc->image; ?>"/>
								</center>
								<h6 class="text-center">
                                    <?=$pc->name; ?>
									<i class="fa fa-angle-right" style="font-size: 14px;font-weight: 600;"></i>
								</h6>
							</div>
						</div>
					</a>
                    <?php if($i%2 == 0)
                        echo '</div>';
                    $i++;?>
                    <?php }
                    if($i%2 == 0)
                        echo '</div>';
                    ?>
				</div>
			</div>
		</div>
        <?php if($data['categoryforhomebrand'] == 'Data Not Available'){ ?>
		<div class="row" id="brands">
			<div class="col-sm-12">
				<div class="brand-panel">
					<h3></h3>
				</div>
			</div>
		</div>
        <?php } else { ?>
        <?php foreach($data['categoryforhomebrand'] as $topbrand) {?>
		<div class="row" id="brands">
			<input type="hidden" name="brands" value="<?=$topbrand->id; ?>"/>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="brand-panel">
					<div class="brand-panel-title text-center">
						<h5>Top <?php echo $topbrand->name; ?> Brands</h5>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 brand-panel-left">
						<div class="img-container">
							<img src="assets/products/<?php echo $topbrand->brand_banner; ?>"/>
						</div>
						<a class="btn btn-primary btn-above" href="{{route('retail.retailProducts',[$topbrand->path])}}">Shop Now</a>
						<div class="col-md-8 col-sm-12 col-xs-12 brand-panel-items"></div>
						<div class="agent-carousel" data-slick='{"slidesToShow": 4, "slidesToScroll": 1}'>
                            <?php $brand = $topbrand->brands;
                            $i=1;
                            $expbrands = explode(',',$brand);
                            foreach($expbrands as $br){
                            $bd = DB::select("SELECT * FROM brand WHERE id = '$br' ORDER BY id DESC")[0];
                            if($i%2 !=0)
                                echo '<div class="at-agent-col">'; ?>
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="hover-effect">
									<div>
										<a href="{{route('retail.retailBrand',[$bd->path])}}" >
											<img src="assets/brand/<?php echo $bd->image; ?>">
										</a>
									</div>
								</div>
							</div>
                            <?php
                            if($i%2 == 0)
                                echo '</div>';
                            $i++;
                            ?>
                            <?php }
                            if($i%2 == 0)
                                echo '</div>';
                            ?>
						</div>
					</div>
				</div>
			</div>
		</div>

        <?php } ?>
		<div id="brand-nav" class="col-md-12">
			<a href="javascript:void(0);" id="load-brand" class="load-brand" onClick="nextBrand()">Load More Popular Brands <i class="fa fa-caret-down"></i></a>
		</div>
        <?php }?>
	</div>
</section>
<?php if(count($data['pcss'])){ ?>
	<section class="at-product-category" style="margin: 0; padding: 15px 0; border:none;">
		<?php $postID = '';
		$nb = 1;
		if($data['pcss']){
			foreach($data['pcss'] as $pcs){ ?>
				<div class="container" id="brandsx">
					<div class="row">
						<div class="col-md-12 main-title">
							<div class="lineseparator">
								<h4 class="title"><?=$pcs->name; ?></h4>
								<a class="view-all-category" href="{{route('retail.retailProducts',[$pc->path])}}">View All <i class="fa fa-caret-right"></i></a>
							</div>
	    					<?php $cityProducts = DB::table('city_wise')->where('category_id', $pcs->id)->get();
							$count = 0; ?>
							<div class="slide-6 theme-arrow product-m">
	    					<?php foreach ($cityProducts as $cityProduct){
								if($count == 6){ break; }
									$cwproducts = json_decode($cityProduct->prices);
									foreach ($cwproducts as $cw){
										$pcps = DB::table('products')->where('id', $cityProduct->product_id)->first();
										$pc = DB::table('category')->where('id', $pcs->id)->first();
										$nb = $pc->popular_priority;
										$cp = DB::table('products')->orderBy('id', 'DESC')->first(); ?>
										<div class="swiper-wrapper">
											<article class="swiper-slide">
												<div class="product-layout">
													<div class="product-thumb ">
														<div class="product-inner">
															<div class="product-image">
																<a href="{{ route('retail.retailProduct',[path($pcps->title,$pcps->id)])}}">
																	<img src="<?=url('/assets/products/'.image_order($pcps->images)); ?>" alt="<?=translate($pcps->title); ?>" style="height: 138px;">
																</a>
																<div class="action-links">
																	<a href="javascript:void(0);" class="ajax-spin-cart">
																		<i class="fa fa-shopping-cart"></i>
																	</a>
																	<a class="wishlist action-btn btn-wishlist" href="" title="Wishlist">
																		<i class="fa fa-heart"></i>
																	</a>
																</div>
															</div>
															<!-- end of product-image -->
															<div class="product-caption">
																<h4 class="product-name text-ellipsis">
																	<a href="{{route('retail.retailProduct',[path($pcps->title,$pcps->id)])}}" title="<?=translate($pcps->title); ?>">
																		<?=translate($pcps->title); ?>
																	</a>
																</h4>
																<a href="{{route('retail.retailProduct',[path($pcps->title,$pcps->id)])}}" class="btn btn-primary btn-sm">View Detail</a>
															</div>
														</div>
													</div>
												</div>
											</article>
										</div>
										<?php $count++;   ?>
									<?php } ?>
		                        <?php } ?>
							</div>
						</div>
					</div>
				</div>
			<?php }
		} ?>
		<input type="hidden" name="brands" class="band" value="<?=$nb; ?>"/>
		<input type="hidden" name="rem_pre" value="<?=$data['rem_pre']; ?>"/>
		<div class="container">
			<div id="brand-nav" class="col-md-12">
				<a href="javascript:void(0);" id="load-brand" class="load-brand" onClick="nextBrand()">Load More Products <i class="fa fa-caret-down"></i></a>
			</div>
		</div>
	</section>
<?php } ?>
<?php if(!empty($data['posts'])){ ?>
<section class="at-product-category" style="margin: 0; padding: 50px 0;border:none;background: #ffffff;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="at-sec-title">
					<h2 style="font-size: 15px;">Top <span>Advices</span></h2>
					<div class="at-heading-under-line" style="margin: auto;">
						<div class="at-heading-inside-line" ></div>
					</div>
				</div>
			</div>
		</div>
        <?php if($data['posts'] == 'Data Not Available'){ ?>
		<div class="row" id="brands">
			<div class="col-sm-12">
				<div class="brand-panel">
					<h3>Data Not Available</h3>
				</div>
			</div>
		</div>
        <?php } else { ?>
		<div class="row">
			<div class="col-md-12">
				<section class="regular slider">
                    <?php
                    foreach($data['posts'] as $post){
                    echo '<div>
    							<a data-title="'.translate($post->title).'" href="'.route('retail.retailAdPost',[path($post->title, $post->id)]).'">
        							<div class="post hover-effect" id="'.$post->id.'">
            							<div class="post-image" style="background-image:url(\''.url('/assets/blog/'.$post->images).'\')"></div>
                                        <h4>'.translate($post->title).'</h4>
        								<div class="i">
        									<div class="pull-left"><i class="icon-clock"></i> '.timegap($post->time).translate(' ago').'</div>
        									<div class="pull-right"><i class="icon-eye"></i> '.$post->visits.' '.translate('Views').'</div>
        									<div class="clearfix"></div>
        								</div>
        							</div>
    							</a>
                        </div>';
                    } ?>
				</section>
			</div>
		</div>
        <?php } ?>
		<div class="row">
			<div class="col-lg-12" style="text-align: center;">
				<a href="all-advices" class="btn btn-primary">See More</a>
			</div>
		</div>
	</div>
</section>
<?php } ?>
<?php
if (mb_substr($data['blocs']->content, 0, 7) == 'widget:') {
echo $__env->make('widgets/'.mb_substr($data['blocs']->content, 7, 255))->render();
} else {
echo $data['blocs']->content;
}
echo $data['footer']
?>
<script>
    var collapse = false;
    function nextBrand(){
        var brandsLoaded = $('input[name=brands]').val();
        $.ajax({
            url: '{{route('retail.retailGetBrands')}}',
            type: 'post',
            data: 'brands='+brandsLoaded+'&_token=<?=csrf_token();?>',
            beforeSend: function(){
                if(!collapse){
                    $('#brand-nav').html('Loading...');
                }
            },
            success: function(data){
                var retData = JSON.parse(data);
                $('input[name=brands]').val(retData.brands);
                $('#brands').append(retData.html);
                collapse = retData.collapse;
                $(".brand-panel-items.new .agent-carousel").slick({
                    dots: false,
                    arrows: true,
                    infinite: false,
                    autoplay: false,
                    speed: 800,
                    autoplaySpeed: 3000,
                    responsive: [
                        {
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 1,
                                infinite: true,
                                dots: false
                            }
                        },
                        {
                            breakpoint: 750,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 2
                            }
                        },
                        {
                            breakpoint: 470,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 2
                            }
                        }
                    ]
                });
            },
            complete: function(){
                if(!collapse){
                    $('#brand-nav').html('<a href="javascript:void(0);" id="unload-brand" onClick="collapseBrands()">Collapse Popular Brands <i class="fa fa-caret-up"></i></a> | <a href="javascript:void(0);" id="load-brand" onClick="nextBrand()">Load More Popular Brands <i class="fa fa-caret-down"></i></a>');
                }else{
                    $('#brand-nav').html('<a href="javascript:void(0);" id="unload-brand" onClick="collapseBrands()">Collapse Popular Brands <i class="fa fa-caret-up"></i></a>');
                }
            }
        });
    }
    function collapseBrands(){
        var brandsLoaded = $('input[name=brands]').val();
        var brands = brandsLoaded.toString().split(',')[0];
        $.ajax({
            url: '{{route('retail.retailGetBrand')}}',
            type: 'post',
            data: 'brands='+brands+'&_token=<?=csrf_token();?>',
            beforeSend: function(){
                $('#brand-nav').html('Loading...');
            },
            success: function(data){
                var retData = JSON.parse(data);
                $('#brands').html('<input type="hidden" name="brands" value="'+brands+'"/>'+retData.html);
                collapse = false;
                $(".brand-panel-items .agent-carousel").slick({
                    dots: false,
                    arrows: true,
                    infinite: false,
                    autoplay: false,
                    speed: 800,
                    autoplaySpeed: 3000,
                    responsive: [
                        {
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 1,
                                infinite: true,
                                dots: false
                            }
                        },
                        {
                            breakpoint: 750,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 2
                            }
                        },
                        {
                            breakpoint: 470,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 2
                            }
                        }
                    ]
                });
            },
            complete: function(){
                $('#brand-nav').html('<a href="javascript:void(0);" id="load-brand" onClick="nextBrand()">Load More Popular Brands <i class="fa fa-caret-down"></i></a>');
                $('html, body').animate({
                    scrollTop: $("#brands").offset().top-200
                }, 'slow');
            }
        });
    }
    $(function() {
        $('[data-popup-open]').on('click', function(e) {
            var targeted_popup_class = jQuery(this).attr('data-popup-open');
            $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
            e.preventDefault();
        });
        $('[data-popup-close]').on('click', function(e) {
            var targeted_popup_class = jQuery(this).attr('data-popup-close');
            $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
            e.preventDefault();
        });
    });
</script>
<!-- Service Category Popup Start -->
<div class="popup" data-popup="popup-1">
	<div class="popup-innerx">
        <?php if($data['service_categories'] == 'Data Not Available'){ ?>
		<div class="item">
			<p>Data Not Available</p>
		</div>
        <?php } else { ?>
        <?php
        foreach($data['service_categories'] as $pc){ ?>
		<div class="item">
			<a href="<?=route('retail.retailServicesCategory',[path($pc->name, $pc->id)]); ?>" class="serviceImg" style="width: 100%;height: auto;margin:0;">
				<img src="<?=url('assets/services/'.$pc->image); ?>" style="width: 100%;height: 145px;"/>
				<span class="serviceImgtitle" style="font-size: 16px;"> <?=$pc->name; ?> </span>
				<span class="overlay"></span>
			</a>
		</div>
        <?php } }?>
		<a class="popup-close" data-popup-close="popup-1" href="#">x</a>
	</div>
</div>
<div class="popup" data-popup="popup-2">
	<div class="popup-innerx">
        <?php if($data['design_categories'] == 'Data Not Available'){ ?>
		<div class="item">
			<p>Data Not Available</p>
		</div>
        <?php } else { ?>
        <?php
        $ur = 'retail.retailLayoutPlan';
        foreach($data['design_categories'] as $pc){
        $ur = 'retail.retailLayoutPlan'.$pc->layout;
        ?>
		<div class="item">
			<a href="<?=route($ur,[$pc->slug]); ?>" class="serviceImg" style="width: 100%;height: auto;margin:0;">
				<img src="<?=url('assets/design/'.$pc->image); ?>" style="width: 100%;height: 145px;;"/>
				<span class="serviceImgtitle" style="font-size: 16px;"> <?=$pc->name; ?> </span>
				<span class="overlay"></span>
			</a>
		</div>
        <?php } }?>
		<a class="popup-close" data-popup-close="popup-2" href="#">x</a>
	</div>
</div>
<?php
if(customer('id') !== ''){
if($data['show_cities']){ ?>
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<form action="{{route('retail.retailIndex')}}" method="post">
            <?=csrf_field()?>
			<input type="hidden" id="cities" name="cities" value=""/>
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" id="modalClose">&times;</button>
					<h5 class="modal-title">Which city you looking for?</h5>
				</div>
				<div class="modal-body">
					<div class="row">
                        <?php if($data['cities']){
                        foreach($data['cities'] as $category){
                        $ur = 'assets/images/';
                        ?>
						<div class="col-md-4 proSuggestions" data-id="<?=$category->name?>">
							<a href="javascript:void(0);" class="serviceImg" style="width: 100%;height: auto;margin:0;">
								<img src="<?=url($ur.$category->image)?>" style="width: 100%;height: 112px;">
								<span class="serviceImgtitle" style="font-size: 14px;"><?=$category->name?></span>
								<span class="overlay"></span>
							</a>
							<span class="selector"></span>
						</div>
                        <?php } } ?>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Done</button>
				</div>
			</div>
		</form>
	</div>
</div>
<?php }
}else{
if($data['show_cities']){ ?>
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<form action="{{route('retail.retailIndex')}}" method="post">
            <?=csrf_field()?>
			<input type="hidden" id="cities" name="cities" value=""/>
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" id="modalClose">&times;</button>
					<h5 class="modal-title">Which city you looking for?</h5>
				</div>
				<div class="modal-body">
					<div class="row">
                        <?php if($data['cities']){

                        foreach($data['cities'] as $category){
                        $ur = 'assets/images/';
                        ?>
						<div class="col-md-4 proSuggestions" data-id="<?=$category->name?>">
							<a href="javascript:void(0);" class="serviceImg" style="width: 100%;height: auto;margin:0;">
								<img src="<?=url($ur.$category->image)?>" style="width: 100%;height: 112px;">
								<span class="serviceImgtitle" style="font-size: 14px;"><?=$category->name?></span>
								<span class="overlay"></span>
							</a>
							<span class="selector"></span>
						</div>
                        <?php } } ?>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Done</button>
				</div>
			</div>
		</form>
	</div>
</div>
<?php } } ?>
<script>
    function selectCity(id) {
        var cityid = id;
        $.ajax({
            type: "POST",
            url: "{{route('retail.retailSelectCity')}}",
            data:'_token=<?=csrf_token();?>&cityid='+cityid,
            success: function(data){
                $('#modalClose').click();
            }
        });
    }
    function submitHub(){
        if($('input[name=hub]:checked').val()) {
            $('form#hubForm').submit();
        }else{
            alert('Please select rate type to proceed!')
        }
    }
    $('#myModal').modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    });
    var removeValue = function(list, value, separator){
        separator = separator || ",";
        var values = list.split(separator);
        for(var i = 0 ; i < values.length ; i++) {
            if(values[i] == value) {
                values.splice(i, 1);
                return values.filter(function(v){return v!==''}).join(separator);
            }
        }
        return list;
    }
    $(document).on('click','.proSuggestions', function(){
        var ele = $(this);
        ele.find('.selector').toggleClass('active');
        var data = $('#cities').val();
        if(ele.find('.selector').hasClass('active')){
            var ar = data.split(',');
            ar.push(ele.data('id'));
            $('#cities').val(ar.filter(function(v){return v!==''}).join(','));
        }else{
            $('#cities').val(removeValue(data, ele.data('id'), ','));
        }
    })
</script>
