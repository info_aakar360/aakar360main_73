<?php
echo $header;
if(isset($_GET['add'])) {
    echo $notices.'<form action="" method="post" enctype="multipart/form-data" class="form-horizontal single">
			'.csrf_field().'
			<h5><a href="design_category"><i class="icon-arrow-left"></i></a>Add new category</h5>
				<fieldset>
					  <div class="form-group">
						<label class="control-label">Category name</label>
						<input name="name" type="text"  class="form-control" />
					  </div>
					  <div class="form-group">
						<label class="control-label">Short Description</label>
						<textarea name="description" class="form-control" placeholder="Enter description..."></textarea>
					  </div>
					  <div class="form-group">
						<label class="control-label">Category path</label>
						<input name="path" type="text" class="form-control"  />
					  </div>
					  <div class="form-group">
						<label class="control-label">Category image</label>
						<input name="image" type="file" class="form-control"  />
					  </div>
						<div class="form-group">
							<label class="control-label">Layout</label>
							<select name="layout" class="form-control">
								<option value="layout_plans">Layout Plan</option>
								<option value="photos">Photos</option>
							</select>
						</div>
					  <div class="form-group">
							<label class="control-label">Parent category</label>
							<select name="parent" class="form-control">
							<option value="0"></option>';
								foreach ($parents as $parent){
									echo '<option value="'.$parent->id.'">'.$parent->name.'</option>';
									$childs = DB::select("SELECT * FROM design_category WHERE parent = ".$parent->id);
                                    foreach ($childs as $child){
                                        echo '<option value="'.$child->id.'">- '.$child->name.'</option>';
                                        $subchilds = DB::select("SELECT * FROM design_category WHERE parent = ".$child->id);
										foreach ($subchilds as $subchild){
											echo '<option value="'.$subchild->id.'"><i style="padding-left: 10px;">--> '.$subchild->name.'</i></option>';
										}
                                    }
								}
								echo '</select>
												  </div>
												  <input name="add" type="submit" value="Add category" class="btn btn-primary" />
											</fieldset>
										</form>';
} elseif(isset($_GET['edit'])) {
    echo $notices.'<form action="" method="post" class="form-horizontal single" enctype="multipart/form-data">
			'.csrf_field().'
			<h5><a href="design_category"><i class="icon-arrow-left"></i></a>Edit category</h5>
				<fieldset>
					  <div class="form-group">
						<label class="control-label">Category name</label>
						<input name="name" type="text"  value="'.$design_category->name.'" class="form-control" />
					  </div>
					  <div class="form-group">
						<label class="control-label">Short Description</label>
						<textarea name="description" class="form-control" placeholder="Enter description...">'.$design_category->description.'</textarea>
					  </div>
					  <div class="form-group">
						<label class="control-label">Category path</label>
						<input name="path" type="text" value="'.$design_category->path.'" class="form-control"  />
					  </div>';
						  if (!empty($design_category->image)){

							  echo '<p>Uploading new images will overwrtite current images .</p>';

									echo '<img class="col-md-2" src="'.url('/assets/layout_plans/'.$design_category->image).'" />';

							  echo '<div class="clearfix"></div>';
						  }

						  if (!empty($design->download)){
							  echo '<p>Uploading new file will overwrite current file .</p>';
						  }
						  echo '
					  <div class="form-group">
						<label class="control-label">Image</label>
						<input type="file" class="form-control" name="image"/>
					  </div>
						<div class="form-group">
							<label class="control-label">Layout</label>
							<select name="layout" class="form-control" placeholder="Select Layout for this category">
								<option></option>
								<option value="layout_plans" '.($design_category->layout == 'layout_plans' ? 'selected' : '').'>Layout Plan</option>
								<option value="photos" '.($design_category->layout == 'photos' ? 'selected' : '').'>Photos</option>
							</select>
						</div>
					  <div class="form-group">
							<label class="control-label">Parent category</label>
							<select name="parent" class="form-control">
							<option value="0"></option>';
								foreach ($parents as $parent){
									echo '<option value="'.$parent->id.'" '.($parent->id == $design_category->parent ? 'selected' : '').'>'.$parent->name.'</option>';
									$childs = DB::select("SELECT * FROM design_category WHERE parent = ".$parent->id);
                                    foreach ($childs as $child){
                                        echo '<option value="'.$child->id.'" '.($child->id == $design_category->parent ? 'selected' : '').'>- '.$child->name.'</option>';
                                        $subchilds = DB::select("SELECT * FROM design_category WHERE parent = ".$child->id);
                                    foreach ($subchilds as $subchild){
                                        echo '<option value="'.$subchild->id.'" '.($subchild->id == $design_category->parent ? 'selected' : '').'><i style="padding-left: 10px;">--> '.$subchild->name.'</i></option>';
                                    }
								}
    }
    echo '</select>
					  </div>
					  <input name="edit" type="submit" value="Edit category" class="btn btn-primary" />
				</fieldset>
			</form>';
} else {
    ?>
    <div class="head">
        <h3>Categories<a href="design_category?add" class="add">Add category</a></h3>
        <p>Manage your design categories</p>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-bordered" id="datatable-editable">
            <thead>
            <tr class="bg-blue">
                <th>Sr. No.</th>
                <th>Design Image</th>
                <th>Design Name</th>
                <th>Parent Category Name</th>
                <th>Layout</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
    <?php
    $sr = 1;
    echo $notices;
    foreach ($design_category as $category){
        echo'<tr>
            <td>'.$sr.'</td>
            <td><img src="../assets/design/'.image_order($category->image).'" style="height: 100px; width: 100px;"></td>
            <td><a href="../'.$category->path.'">'.$category->name.'</a></td>';
            if($category->parent != 0){
           $parent = DB::select("SELECT * FROM design_category WHERE id = ".$category->parent)[0];
            echo'<td>'.$parent->name.'</td>';
            } else {
                echo'<td>No Parent</td>';
            }
            echo'
			<td>
				'.$category->layout.'
			</td>
            <td><a href="design_category?delete='.$category->id.'"><i class="icon-trash"></i></a>
				<a href="design_category?edit='.$category->id.'"><i class="icon-pencil"></i></a>
            </td>
          </tr>
          <!--div class=" col-md-3">
		<div class="product">
			<div class="pi">
				<img src="../assets/design/'.image_order($category->image).'">
			</div>
			<h5><a href="../'.$category->path.'">'.$category->name.'</a></h5>
			
			<div class="tools">
				<a href="design_category?delete='.$category->id.'"><i class="icon-trash"></i></a>
				<a href="design_category?edit='.$category->id.'"><i class="icon-pencil"></i></a>
			</div>
		</div>
	</div-->';
    $sr++; }
}?>
            </tbody>
        </table>
    </div>
<?php
echo $footer;
?>