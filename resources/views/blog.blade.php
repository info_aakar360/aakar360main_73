<?php echo $header?>
    <div class="home-header">
        <div class="video-container" style="overflow: hidden;background-size: cover;background-color: transparent;background-repeat: no-repeat;background-position: 50% 50%;background-image: none;float: left;width: 100%;max-height: 250px;">
            <img src="<?php url('/assets/blog/'.$post->images); ?>" class="cover" style="">
        </div>
        <div class="title" style="float: left;width: 100%;margin-top: -100px;">
            <div class="container">
                <div class="row bgs-white">
                    <div class="col-xs-12 col-sm-offset-1 col-sm-10 col-lg-offset-2 col-lg-8">
                        <h1 class="home-h1" style="padding: 20px;"><?=translate('Blog') ?></h1>
                        <p class="grey"></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="container">
	<h1 class="title"><?=translate('Blog') ?></h1>
	<div class="row m50">
		<?php
			foreach($posts as $post){
				echo '<div class="col-md-4">
					<a data-title="'.translate($post->title).'" class="smooth" href="'.route('retail.retailPost',[path($post->title, $post->id)]).'">
					<div class="post" id="'.$post->id.'">
					<div class="post-image" style="background-image:url(\''.url('/assets/blog/'.$post->images).'\')"></div>
						<h4>'.translate($post->title).'</h4>
						<div class="i">
							<div class="pull-left"><i class="icon-clock"></i> '.translate('Posted ').timegap($post->time).translate(' ago').'</div>
							<div class="pull-right"><i class="icon-eye"></i> '.$post->visits.' '.translate('Views').'</div>
							<div class="clearfix"></div>
						</div>
					</div>
					</a>
				</div>';
			}
		?>
	</div>
</div>
<?php echo $footer?>