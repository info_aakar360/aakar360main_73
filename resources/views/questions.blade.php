<?php echo $header; ?>
    <section id="at-inner-title-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="at-inner-title-box">
                        <h2>Let's find you <?=translate($service->title)?> Experts</h2>
                        <p><a href="<?=url('')?>">Home</a> <i class="fa fa-angle-double-right" aria-hidden="true"></i><i class="fa fa-angle-double-right" aria-hidden="true"></i> <a href="#"><?=translate($service->title)?></a>
                        </p>
                    </div>

                </div>
                

            </div>
        </div>
    </section>
    <!-- Inner page heading end -->

    <!-- Property start from here -->
    <section class="at-property-sec at-property-right-sidebar">
        <div class="container">
            <div class="">
<?php if($questions) {  ?>
				<div class="stepwizard">
					<div class="stepwizard-row setup-panel">
						<div class="stepwizard-step col-xs-3"> 
							<a href="#step-0" type="button" class="btn btn-success btn-circle">0</a>
						</div>
					<?php
					$sr = 1;
					foreach($questions as $question){ ?>
						<div class="stepwizard-step col-xs-3"> 
							<a href="#step-<?=$sr;?>" type="button" class="btn btn-default btn-circle"><?=$sr; ?></a>
						</div>
					<?php $sr++; } ?>
						
						
					</div>
				</div>
				
				<form role="form">
				<div class="panel panel-primary setup-content" id="step-0">
						
						<div class="panel-body" style="padding:40px 20px 10px 20px;min-height: 350px;">
							<div class="form-group">
								<h2 class="text-center" style="text-transform: none;">What is the location of your Project?</h2>
								<hr style="position: relative; max-width: 100px; margin:40px auto;">
								<center><i class="fa fa-map-marker" style="font-size:48px;"></i><br><label class="location">Pincode</label><br><input maxlength="6" minlength="6" type="text"  required="required" class="form-control pincode" style="max-width: 200px;margin:20px 0;" placeholder="Enter Location" /></center>
									
							</div>
							<div style="padding: 20px;text-align:center">
								<hr>
								<button class="btn btn-primary prevBtn" disabled type="button">Previous</button>
								<button class="btn btn-primary nextBtn" type="button">Next</button>
							</div>
						</div>
						
					</div>
				<?php $sr = 1;
					foreach($questions as $question){ ?>
					<div class="panel panel-primary setup-content" id="step-<?=$sr; ?>">
						
						<div class="panel-body" style="padding:40px 20px 10px 20px;min-height: 350px;">
							<div class="form-group">
								<h2 class="text-center" style="text-transform: none;"><?php echo getQuestion($question->question_id); ?></h2>
								<hr style="position: relative; max-width: 100px; margin:40px auto;">
								<?php 
									$type = getQuestionType($question->question_id); 
									if($type == 'text'){
								?>
								<center><i class="fa fa-map-marker" style="font-size:48px;"></i><br><label class="location">Pincode</label><br><input maxlength="6" minlength="6" type="text"  required="required" class="form-control pincode" style="max-width: 200px;margin:20px 0;" placeholder="Enter Location" /></center>
									<?php }else if($type == 'single'){ 
										echo '<div style="position:relative; max-width: 350px;margin:auto;">';
										$options = json_decode(getQuestionOptions($question->question_id));
										$i = 0;
										foreach($options as $option){
									?>
									<input type="radio" name="answer[]" value="<?=$option;?>" style="margin-right:10px;" id="<?=$sr.$i;?>"></input> <label class="label-hover" for="<?=$sr.$i;?>" style="cursor: pointer;""><?=$option; ?></label><br>
									
										<?php $i++; } echo '</div>'; }
										else if($type == 'multiple'){ 
										echo '<div style="position:relative; max-width: 350px;margin:auto;">';
										$options = json_decode(getQuestionOptions($question->question_id));
										$i = 0;
										foreach($options as $option){
									?>
									<input type="checkbox" name="answer[]" value="<?=$option;?>" style="margin-right:10px;" id="<?=$sr.$i;?>"></input> <label class="label-hover" for="<?=$sr.$i;?>" style="cursor: pointer;""><?=$option; ?></label><br>
									
										<?php $i++; } echo '</div>'; }
										else if($type == 'textarea'){ 
										echo '<div style="position:relative;width: 60%;margin:auto;">';
									?>
									<textarea name="answer[]" style="margin-right:10px;" rows="7" class="form-control" placeholder="Enter a description..."></textarea>
									
										<?php echo '</div>'; }
										else if($type == 'dropdown'){ 
										echo '<div style="position:relative; max-width: 350px;margin:auto;">
													<select class="form-control select2" name="answer[]">';
										$options = json_decode(getQuestionOptions($question->question_id));
										$i = 0;
										foreach($options as $option){
									?>
									<option value="<?=$option;?>"><?=$option; ?></option>
									
										<?php $i++; } echo '</select></div>'; } ?>
							</div>
							<div style="padding: 20px;text-align:center">
								<hr>
								<button class="btn btn-primary prevBtn" <?=($sr == 0) ? 'disabled' : ''; ?> type="button">Previous</button>
								<?php if($sr != count($questions)){ ?>
									<button class="btn btn-primary nextBtn" type="button">Next</button>
								<?php }else{ ?>
									<button class="btn btn-primary" type="submit">Finish</button>
								<?php } ?>
								
							</div>
						</div>
						
					</div>
					<?php $sr++; } ?>
					
				</form>
<?php }else{
	echo 'Ooops...! Something Went Wrong. Please try again later';
} ?>
               
            </div>
        </div>
    </section>


<?php echo $footer?>
<script>
$(document).ready(function () {

    var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn');
        allPrevBtn = $('.prevBtn');

    allWells.hide();
	var ajax_valid = true;
    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-success').addClass('btn-default');
            $item.addClass('btn-success');
            //allWells.hide('slide', {direction: 'left'}, 1000);
			//allWells.hide();
			allWells.hide();
            $target.show('slide', {direction: 'right'}, 1000);
			
            //$target.find('input:eq(0)').focus();
			
        }
    });

    allNextBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url'],input[type='tel'],select,input[type='radio'],input[type='checkbox'],textarea"),
            isValid = true;
		if(ajax_valid){
			$(".form-group").removeClass("has-error");
			for (var i = 0; i < curInputs.length; i++) {
				if (!curInputs[i].validity.valid) {
					isValid = false;
					$(curInputs[i]).closest(".form-group").addClass("has-error");
				}
			}
		}else{
			isValid = false;
		}

        if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');
    });
	allPrevBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url'],input[type='tel'],select,input[type='radio'],input[type='checkbox'],textarea"),
            isValid = true;
			allWells.hide();
			curStep.prev().show('slide', {direction: 'left'}, 1000);
    });

    $('div.setup-panel div a.btn-success').trigger('click');
	$('.pincode').on('keyup', function(){
		var csrf_token = '<?=csrf_token();?>';
		var pin = $(this).val();
		$.ajax({
			url: 'getlocality',
			type: 'post',
			async:false,
			data: 'pincode='+pin+'&_token='+csrf_token,
			success:function(data){
				if(data !== null){
					$('.location').html(data);
					if(data == 'Pincode Not Available' || data == 'Invalid Pincode'){
						$(".form-group").addClass("has-error");
						ajax_valid = false;
					}else{
						$(".form-group").removeClass("has-error");
						ajax_valid = true;
					}
				}else{
					$('.location').html('Pincode');
					$(".form-group").addClass("has-error");
					ajax_valid = false;
				}
			},
			error: function(){
				$('.location').html('Pincode Not Available');	
				$(".form-group").addClass("has-error");
				ajax_valid = false;
			}
		})
	});
	$('.select2').select2({
			placeholder: "Please Select Option...",
			allowClear: true,
			width: '100%'
		});
});

</script>