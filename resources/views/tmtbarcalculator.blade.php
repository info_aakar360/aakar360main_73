<?php echo $header?>

<div class="home-header">
    <div class="video-container" style="overflow: hidden;background-size: cover;background-color: transparent;background-repeat: no-repeat;background-position: 50% 50%;background-image: none;float: left;width: 100%;max-height: 250px;">
        <img src="assets/banners/<?=$dimg->image; ?>" class="cover" style="">
    </div>
    <div class="title" style="float: left;width: 100%;margin-top: -100px;">
        <div class="container">
            <div class="row bgs-white">
                <div class="col-xs-12 col-sm-offset-1 col-sm-10 col-lg-offset-2 col-lg-8">
                    <h1 class="home-h1" style="padding: 20px;">TMT Bar Conversion.</h1>
                    <p class="grey"></p>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="at-property-sec at-property-right-sidebar">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="col-md-12">
                    <div class="at-sidebar">
                        <div class="at-categories clearfix">
                            <h3 class="at-sedebar-title">Calculate TMT Bars</h3>
                            <fieldset>
                                <div class="form-group">
                                    <label class="control-label">Brand name</label>
                                    <select class="form-control" required id="brand_name">
                                        <option value=""> Select Brand </option>
                                        <?php foreach($tmtcalculator as $tmt){
                                            echo '<option  value="'.$tmt->brand_id.'">'.translate($tmt->brand_name).'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </fieldset>

<p id="tmt_calculator_table"></p>
<p ><h6>NOTE</h6>
    <span>*</span> testing
    <span>*</span> testing
    <span>*</span> testing
</p>


                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<?php echo $footer?>

<script>
    $(document).ready(function(){

        $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
            localStorage.setItem('activeTab', $(e.target).attr('href'));
        });
        var activeTab = localStorage.getItem('activeTab');
        if(activeTab){
            $('#myTab a[href="' + activeTab + '"]').tab('show');
        }

        $('#brand_name').on('change', function(){
            var csrf_token = '<?=csrf_token();?>';
            var brand = $(this).val();
            $.ajax({
                url: 'ajaxbrandchange',
                type: 'post',
                async:false,
                data: 'brand='+brand+'&_token='+csrf_token,
                success:function(data){
                    if(data !== null){
                        $('#tmt_calculator_table').html(data);
                    }
                },
            })
        });
        $(document).on('click','.checkme', function(e) {
           var myval = $(this).val();
           if(myval=='kg'){
               $('.kg').prop('readonly', false);
               $('.rod').prop('readonly', true);
               $('.bundle').prop('readonly', true);
               $('#tmt_calculator_table').find('input:text').val('');
           }
           else if(myval=='rods'){
               $('.kg').prop('readonly', true);
               $('.rod').prop('readonly', false);
               $('.bundle').prop('readonly', true);
               $('#tmt_calculator_table').find('input:text').val('');
           }else if(myval == 'bundle'){
               $('.kg').prop('readonly', true);
               $('.rod').prop('readonly', true);
               $('.bundle').prop('readonly', false);
               $('#tmt_calculator_table').find('input:text').val('');
           }

            $(document).on('keyup','.kg_calculate', function(e) {
                var kg = $(this).val();
                var kg_val = $(this).next("span").text();
                var rod_val = $(this).closest('.first-row').find('.rod_val').text();
                var bundle_val = $(this).closest('.first-row').find('.bundle_val').text();
                var per_rod = parseFloat(kg_val) / parseFloat(rod_val);
                var per_bundle = parseFloat(rod_val) / parseFloat(bundle_val);

                var tot_rod = parseFloat(kg) / parseFloat(per_rod);
                var tot_rod = parseFloat(tot_rod.toFixed(2));
                $(this).closest('.first-row').find('.rod').val(tot_rod);
                var tot_bundle = parseFloat(tot_rod) / parseFloat(per_bundle);
                var tot_bundle = parseFloat(tot_bundle.toFixed(2));
                $(this).closest('.first-row').find('.bundle').val(tot_bundle);

                    calculateKg();
                    calculateRod();
                    calculateBundle();

            });

            $(document).on('keyup','.rod_calculate', function(e) {
                var rod = $(this).val();
                var rod_val = $(this).next("span").text();
                var kg_val = $(this).closest('.first-row').find('.kg_val').text();
                var bundle_val = $(this).closest('.first-row').find('.bundle_val').text();
                var per_kg = parseFloat(rod_val) / parseFloat(kg_val);
                var per_bundle = parseFloat(rod_val) / parseFloat(bundle_val);

                var tot_kg = parseFloat(rod) / parseFloat(per_kg);
                var tot_kg = parseFloat(tot_kg);
                $(this).closest('.first-row').find('.kg').val(tot_kg);

                var tot_bundle = parseFloat(rod) / parseFloat(per_bundle);
                var tot_bundle = parseFloat(tot_bundle);
                $(this).closest('.first-row').find('.bundle').val(tot_bundle);

                calculateKg();
                calculateRod();
                calculateBundle();

            });

            $(document).on('keyup','.bundle_calculate', function(e) {
                var bundle = $(this).val();
                var bundle_val = $(this).next("span").text();
                var kg_val = $(this).closest('.first-row').find('.kg_val').text();
                var rod_val = $(this).closest('.first-row').find('.rod_val').text();
                var per_kg = parseFloat(kg_val) / parseFloat(bundle_val);
                var per_rod = parseFloat(rod_val) / parseFloat(bundle_val);

                var tot_kg = parseFloat(bundle) * parseFloat(per_kg);
                var tot_kg = parseFloat(tot_kg);
                $(this).closest('.first-row').find('.kg').val(tot_kg);

                var tot_rod = parseFloat(bundle) * parseFloat(per_rod);
                var tot_rod = parseFloat(tot_rod);
                $(this).closest('.first-row').find('.rod').val(tot_rod);

                calculateKg();
                calculateRod();
                calculateBundle();

            });

            $("#kg-total").html(0);
            $("#rods-total").html(0);
            $("#bundles-total").html(0);
        });

        function calculateKg() {
            var sum = 0;
            $(".kg").each(function() {
                if (!isNaN(this.value) && this.value.length != 0) {
                    sum += parseFloat(this.value);
                }
                else if (this.value.length != 0){
                }
            });
            $("#kg-total").html(sum.toFixed(2));
        }

        function calculateRod() {
            var sum = 0;
            $(".rod").each(function() {
                if (!isNaN(this.value) && this.value.length != 0) {
                    sum += parseFloat(this.value);
                }
                else if (this.value.length != 0){
                }
            });
            $("#rods-total").html(sum.toFixed(2));
        }

        function calculateBundle() {
            var sum = 0;
            $(".bundle").each(function() {
                if (!isNaN(this.value) && this.value.length != 0) {
                    sum += parseFloat(this.value);
                }
                else if (this.value.length != 0){
                }
            });
            $("#bundles-total").html(sum.toFixed(2));
        }



    });
    $(function() {
//----- OPEN
        $('[data-popup-open]').on('click', function(e) {
            var targeted_popup_class = jQuery(this).attr('data-popup-open');
            $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
            e.preventDefault();
        });
//----- CLOSE
        $('[data-popup-close]').on('click', function(e) {
            var targeted_popup_class = jQuery(this).attr('data-popup-close');
            $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
            e.preventDefault();
        });
    });






</script>



