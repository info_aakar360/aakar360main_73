<?php echo $header?>
<div class="b-hero1">
    <figure style="background-image: url('assets/design/<?=$cat->image; ?>')" class="b-post__hero-image">
        <div data-w-id="403de785-9462-5110-ab3f-b13328bf2c04" class="b-post__hero-inner">
            <div class="b-container cc-read cc-hero">
                <div class="b-read__wrap cc-hero">
                    <h1 class="b-post__title"> <?=translate($cat->name) ?></h1>
                    <p class="b-post__subhead"><?=$cat->description;?></p>

                </div>

            </div>

        </div>

    </figure>

</div>
<div class="row" style="text-align: center;">
    <div class="col-xs-12 col-sm-offset-1 col-sm-10 col-lg-offset-2 col-lg-8" style="margin-top: -105px; z-index: 1">
        <div class="header-bottom">
            <div class="header-navigation">
                <ul class="navigation navigation--horizontal">
                    <li class="navigation-item navigation-architecture">
                        <?php $totvisit = 0;
                        $btotal = DB::select("SELECT * FROM design_category WHERE parent = '".$cat->id."'");
                        foreach($btotal as $btv){
                            $pla = DB::select("SELECT * FROM photo_gallery WHERE category = '".$btv->id."'");
                            foreach ($pla as $pl) {
                                $pla = DB::select("SELECT COUNT(*) as count FROM photo_reviews WHERE active = 1 AND review <> '' AND photo = '".$pl->id."'")[0]->count;
                                $totvisit = $totvisit + $pla;
                            }
                        }
                        ?>
                        <a href="javascript:void(0)" title="Total Visits"><span><?=$totvisit; ?> Total Review</span><i class="fa fa-pencil-square-o"> <?=$totvisit; ?></i> </a>
                    </li>
                    <li class="navigation-item navigation-art">
                        <?php $totvisits = 0;
                        foreach($btotal as $btvs){
                            $plas = DB::select("SELECT * FROM photo_gallery WHERE category = '".$btvs->id."'");
                            foreach ($plas as $pls) {
                                $totvisits = $totvisits + $pls->visits;
                            }
                        }
                        ?>
                        <a href="javascript:void(0)" title="Liked"><span><?=$totvisits; ?> Total Visits</span><i class="fa fa-eye"> <?=$totvisits; ?></i></a>
                    </li>
                    <li class="navigation-item navigation-art">
                        <?php
                        $totli = 0;
                        foreach ($btotal as $btl){
                            $plal = DB::select("SELECT * FROM photo_gallery WHERE category = '".$btl->id."'");
                            foreach ($plal as $pll) {
                                $li = DB::select("SELECT COUNT(*) as count FROM image_like WHERE image_id = " . $pll->id)[0]->count;
                                $totli = $totli + $li;
                            }
                        }
                        ?>

                        <a href="javascript:void(0)" title="Liked"><span><?=$totli; ?> Total Likes</span><i class="fa fa-thumbs-up"> <?=$totli; ?></i></a>
                    </li>
                </ul>
            </div>
        </div>


    </div>

</div>
    <section class="pdtopbtm-50">
	<div class="container">
		<div class="row">
			<button class="btn btn-default filter-button"><i class="fa fa-filter"></i> Filter</button>
			<aside class="sidebar-shop col-md-3 order-md-first">
			<div class="filter-div-close"></div>
				<div class="sidebar-wrapper at-categories" style="padding: 10px;">
					<div class="widget">
						<h3 class="widget-title">
							<a data-toggle="collapse" href="#widget-body-c" role="button" aria-expanded="true" aria-controls="widget-body-c" class="cat_accord">Categories</a>
						</h3>
						<div class="show collapse" id="widget-body-c">
							<div class="widget-body" style="height: 250px; overflow-y: auto;">
								<ul class="cat-list">
									<?php foreach($cats as $cat){
										echo '<li><a href="./'.$cat->layout.'/'.$cat->slug.'">'.translate($cat->name).'</a></li>';
										$childs = DB::select("SELECT * FROM design_category WHERE parent = ".$cat->id." ORDER BY id DESC");
										foreach ($childs as $child){
											echo '<li><a href="./'.$child->layout.'/'.$child->slug.'">- '.$child->name.'</a></li>';
										}
									}
									?>
								</ul>
							</div><!-- End .widget-body -->
						</div><!-- End .collapse -->
					</div><!-- End .widget -->
				</div>
			</aside>
			<div class="filter-wrapper"></div>
			<div class="col-md-9 col-lg-9">
                <div class="row " style="padding-top: 30px;">
                    <div class="col-md-4">
                        <div class="featured-item feature-bg-box gray-bg text-center m-bot-0 inline-block radius-less">
                            <div class="icon">
                                <i class="icon-layers"></i>
                            </div>
                            <div class="title text-uppercase">
                                <h4>Customizable Designs</h4>
                            </div>
                            <div class="desc1">
                                Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus.
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="featured-item feature-bg-box gray-bg text-center m-bot-0 inline-block radius-less">
                            <div class="icon">
                                <i class="fa fa-inr"></i>
                            </div>
                            <div class="title text-uppercase">
                                <h4>Value Pricing</h4>
                            </div>
                            <div class="desc1">
                                Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus.
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="featured-item feature-bg-box gray-bg text-center m-bot-0 inline-block radius-less">
                            <div class="icon">
                                <i class="fa fa-check-circle-o"></i>
                            </div>
                            <div class="title text-uppercase">
                                <h4>Wider Selection</h4>
                            </div>
                            <div class="desc1">
                                Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus.
                            </div>
                        </div>
                    </div>
                </div>
				<div id="works" class="package_page">
                    <div class="blocks" style="width: 100%">
                        <div class="works block col-lg-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="at-sec-title">
                                        <h2 style="font-size: 15px;">Featured <span>Photos</span></h2>
                                        <div class="at-heading-under-line">
                                            <div class="at-heading-inside-line"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--section class="call_action">
                                <div>
                                    <p>We regularly add fresh new images to our collection, he's some of the subject catagories we like the best:</p>
                                </div>
                            </section-->
                            <div class="home_categories col-lg-12">
                                <?php foreach ($featured as $layout){ ?>
                                   <a href="<?php echo url('/photo-gallery/'.$layout->slug); ?>" class="grow" title="<?=$layout->title; ?>" style="background: url('<?php echo url('/assets/images/gallery/'.$layout->image); ?>') center center;">
                                        <span><?=$layout->title; ?></span></a>
                                <?php } ?>
                                <div class="cleaner"></div>
                            </div>
                        </div>
                    </div>
                </div><!-- /works -->
                
                <div class="MultiStoryModule Grid Grid--gutters" style="padding:0 25px 30px 25px; width: 100%; margin-top: -60px;">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="at-sec-title">
                                <h2 style="font-size: 15px;">Best For <span>You</span></h2>
                                <div class="at-heading-under-line">
                                    <div class="at-heading-inside-line"></div>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="agent-carousel-3 slider" data-slick='{"slidesToShow": 2, "slidesToScroll": 1}'>
						<?php foreach ($best as $b) {?>
						<div class="Grid-col MultiStoryModule-tile MultiStoryModule-tile-0" style="float: left; padding-right: 10px;">
							<div class="TempoCategoryTile  u-focusTile">
								<div class="TempoCategoryTile-tile valign-top">
									<div class="TempoCategoryTile-imgContainer">
										<div class="border-img" style="padding-bottom: 140.09%; height: 0px; width: 100%;">
											<img aria-hidden="true" tabindex="-1" src="assets/products/<?=$b->image; ?>" class="TempoCategoryTile-img" style="position: absolute; width: 100%; left: 0px; top: 0px;">
										</div>
									</div>
								</div>
							</div>
							<div class="MultiStoryModule-overlay" aria-hidden="true">
								<p class="MultiStoryModule-overlayHeading"><?=$b->title; ?></p>
								<p class="MultiStoryModule-overlayText"><?=$b->content; ?></p>
								<div>&nbsp;</div>
								<div class="text-center">
									<a href="<?=$b->link; ?>" class="btn btn-primary btn-sm text-center" style="width: 50%;">View More</a>
								</div>
							</div>
							<a class="MultiStoryModule-overlayLink"></a>
						</div>
						<?php } ?>
					</div>

                </div>

                <div class="clearfix"></div>
                <div class="row" style="padding-top: 30px;">
                    <div class="col-md-12">
                        <div class="at-sec-title">
                            <h2 style="font-size: 15px;">Photos By <span>Category</span></h2>
                            <div class="at-heading-under-line">
                                <div class="at-heading-inside-line"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid" style="padding-top: 10px;">
                    <?php foreach($galleries as $gallery){ ?>
                        <div class="grid-item">
                            <div class="tiles">
                                <a href="<?=$gallery->layout; ?>/<?=$gallery->slug; ?>">
                                    <img src="assets/design/<?=$gallery->image; ?>"/>
                                    <div class="icon-holder">
                                        <div class="icons1 mrgntop borderRight0" style="width: 100%;"><?=substr(translate($gallery->name), 0, 20); if(strlen($gallery->name)>20) echo '...';?></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php } ?>
				</div>
                <?php if(!empty($link)){?>
                    <div class="">
                        <div class="blockquote blockquote--style-1">
                            <div class="row inner-div">
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <img src="/assets/products/<?=$link->image; ?>" style="width:50%;height:auto;"/>
                                    </div>
                                    <div class="col-md-7" style="padding-top: 25px;">
                                        <h3><?=$link->content; ?></h3>
                                    </div>
                                    <div class="col-md-2" style="padding:15px 0 0 0">
                                        <a class="btn btn-primary" href="<?=$link->link; ?>" type="submit" style="padding: 10px auto !important;">
                                            GET STARTED
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
			</div>
		</div>
</div>
		
    </section>
    <!-- Inner page heading end -->


<?php echo $footer?>
<script>
jQuery(window).on('load', function(){ 
	var $ = jQuery;
	var $container = $('.grid');
    $container.masonry({
          columnWidth:10, 
          gutterWidth: 15,
          itemSelector: '.grid-item'
    });
});
$(document).ready(function(){
	$(document).on('click', '.filter-button', function(){
		$('aside.sidebar-shop').addClass('open');
	});
	$(document).on('click', '.filter-div-close', function(){
		$('aside.sidebar-shop').removeClass('open');
	});
});
</script>