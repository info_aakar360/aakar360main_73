<?php echo $header?>
    <section class="at-contact-sec" style="background: white;">
        <div class="container">
            <div class="row">
                <div class="_2nrNOEKvLZj1z3yyPX5KGC">
                    <div class="N4_sxP3xXGLgWL75xMc6G"></div>
                    <div class="AMQGiqU3AGJV3rIm3i644">
                        <div class="RMJUe-NKALCUqnEWQa4pF" style="background: rgb(255, 255, 255); margin-top: 0px;">
                            <div class="_3xvSzB8Tm2ip43xgYv8F4p">
                                <span id="Feed-0"></span>
                                <div class="_2w8yaE1yA_MmjuHqpTfl6R">
                                    <p>
                                        <span>At Tata Unistore, we are dedicated to excellence and innovation in every aspect of the online business,
                                            from fashion and technology to the creative arts and logistics.
                                        </span>
                                    </p>
                                    <p>
                                        <span>Working with us means staying ahead of trends and moving quickly in a unique and fast-paced environment,
                                            in one of a variety of challenging and fulfilling roles.
                                        </span>
                                    </p>
                                    <p>
                                        <span>We are constantly looking for talented individuals who will help us take our business to the next level,
                                            and keep us at the forefront of the digital domain. To apply for a position, email us at
                                            <a href="mailto:joinus@xyz.com">joinus@xyz.com</a><br><br>
                                        </span>
                                    </p>
                                    <p>
                                        <u><b>BEWARE OF FAKE JOB OFFERS</b></u>
                                    </p>
                                    <p>
                                      <span>
                                          <b>Dear Job Seeker:</b>
                                      </span>
                                    </p>
                                    <p>
                                        <span>Tata UniStore Limited (‘TUL’) owns and operates an online e-commerce portal under the brand names of TATA CLiQ and TATA CLiQ Luxury and </span>
                                        <span>accessible through web (</span><span> and </span>
                                        <span>
                                             ) and mobile application of TATA CLiQ and TATA CLiQ Luxury.<br><br>
                                        </span>
                                    </p>
                                    <p>
                                        <span>XYZ</span>
                                        <span>
                                             is part of the Tata group which is a global enterprise headquartered in India, and comprises more than 100 operating companies, with operations in more than 100 countries across six continents, employing more than 6,50,000 people worldwide. Being an emerging and evolving business, TUL actively hires talent from the market and information regarding any job vacancies is disseminated through official channels only.<br><br>
                                        </span>
                                    </p>
                                    <p>
                                        <span>
                                            Nevertheless, as a socially aware and responsible organisation, we would like to spread a word of caution amongst all job seekers.<br><br>
                                        </span>
                                    </p>
                                    <p class="MsoNormal" style="line-height:250%">
                                        <span>
                                             Our attention has been drawn to some fraudulent individuals / agencies / job portals approaching job seekers through unauthorised telephone calls / emails / SMS / links on WhatsApp, etc. offering job opportunities in various Tata companies and demanding money by way of a refundable security deposit or other fees. Offers are also being made on fake letterheads using the TATA Logo and fraudulent signatures of HR department personnel of various Tata companies. Please note that TUL follows a merit-based recruitment process and does not demand / accept any money whether by way of security deposit or otherwise from applicants for jobs.<br><br>
                                        </span>
                                    </p>
                                    <p>
                                        <span>
                                             If anyone receives an interview call / letter stating that it is from or for a Tata company, he / she should immediately check for authenticity and refrain from submitting any original documents or making any form of payment either in cash or through bank remittances.<br><br>
                                        </span>
                                    </p>
                                    <p>
                                        <span>Please note that TUL does not send you e-mails asking for your credit card number or other personally identifiable information nor do we charge or accept any amount or security deposit from any participant for any competition or marketing promotion or campaign. We urge you to be cautious when opening links or attachments from unknown third parties. Any communication suggesting such payment is contrary to Tata policy.<br><br></span>
                                    </p>
                                    <p>
                                        <u>
                                             <span>
                                                Disclaimer:<br><br>
                                             </span>
                                        </u>
                                    </p>
                                    <p>
                                        <span>
                                             Anyone dealing with such fake interview calls would be doing so at his / her own risk and TUL will not be
                                            held responsible for any loss or damage suffered by such persons, directly or indirectly.<br><br>
                                        </span>
                                    </p>
                                    <p>
                                        <span>We shall not accept any liability towards the representation made in any fraudulent communication or its consequences
                                            and such fraudulent communication shall not be treated as any kind of offer or representation by TUL.<br><br>
                                        </span>
                                    </p>
                                    <p>
                                        <span>In case of any queries please contact us at&nbsp;</span>
                                        <span><a href="mailto:joinus@xyz.com"><span>joinus@xyz.com</span></a></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php echo $footer?>