<?php echo $header?>
    <section class="at-property-sec at-property-right-sidebar" style="padding: 0;">
       <div class="container">
            <div class="content product-page">
				<h4 style="text-transform: none"><?=$cat->title; ?></h4>
				<div class="col-md-12" id="photo-slider" style="padding: 0;">
				
					<div id="myCarousel" class="carousel slide" data-ride="carousel">
						<!-- Wrapper for slides -->
						<div class="carousel-inner text-center" id="photo_gallery">
                            <div class="item active" style="padding: 2px; text-align: center;">
                                <center><img src="<?=url('/assets/images/gallery/'.$cat->image)?>" /></center>
                                <div class="image-actions" style="padding: 20% 0;">
                                    <?php if(customer('id') !== ''){
                                    $user_id = customer('id');
                                    $image_id = $cat->id;
                                    $img = DB::select("SELECT * FROM image_like WHERE user_id = '".$user_id."' AND image_id = '".$image_id."'");
                                    if(!empty($img)){
                                        ?>
                                        <button class="btn btn-primary" id="likebutton"><span class="tickgreen">✔</span> Liked</button>
                                    <?php } else{?>
                                        <button class="btn btn-primary" id="likebutton" onclick="likeImage(this.value);"><i class="fa fa-heart"></i> Like</button>
                                    <?php } $user_id = customer('id');
                                    $image_id = $cat->id;
                                    $ins = DB::select("SELECT * FROM image_inspiration WHERE user_id = '".$user_id."' AND image_id = '".$image_id."'");

                                    if(empty($ins)){
                                        ?>
                                        <button class="btn btn-primary" id="insbutton" onclick="imageInspiration(this.value);"><i class="fa fa-lightbulb"></i> Add to Project</button>
                                    <?php } else { ?>
                                        <button class="btn btn-primary" id="insbutton"><span class="tickgreen">✔</span> Added to Project</button>
                                    <?php } } else { ?>
                                        <button class="btn btn-primary" onClick="getModel()" ><i class="fa fa-heart"></i> Like</button>
                                        <button class="btn btn-primary" onClick="getModel()" ><i class="fa fa-lightbulb"></i> Add to Project</button>
                                    <?php } ?>
                                </div>
                            </div>
							<?php
							$i = 0;
							foreach($images as $image){
								if($i == 0){?>
								<div class="item" style="padding: 2px; text-align: center;">
									<center><img src="<?=url('/assets/images/gallery/photos/'.$image->image)?>"/></center>
									<div class="image-actions" style="padding: 20% 0;">
                                    <?php if(customer('id') !== ''){
                                        $user_id = customer('id');
                                        $image_id = $cat->id;
                                        $img = DB::select("SELECT * FROM image_like WHERE user_id = '".$user_id."' AND image_id = '".$image_id."'");
                                        if(!empty($img)){
                                        ?>
                                            <button class="btn btn-primary" id="likebutton"><span class="tickgreen">✔</span> Liked</button>
                                        <?php } else{?>
										    <button class="btn btn-primary" id="likebutton" onclick="likeImage(this.value);"><i class="fa fa-heart"></i> Like</button>
                                        <?php } $user_id = customer('id');
                                        $image_id = $cat->id;
                                        $ins = DB::select("SELECT * FROM image_inspiration WHERE user_id = '".$user_id."' AND image_id = '".$image_id."'");
                                        if(empty($ins)){
                                            ?>
                                            <button class="btn btn-primary" id="insbutton" onclick="imageInspiration(this.value);"><i class="fa fa-lightbulb"></i> Add to Project</button>
                                        <?php } else { ?>
                                            <button class="btn btn-primary" id="insbutton"><span class="tickgreen">✔</span> Added to Project</button>
                                        <?php } } else { ?>
                                            <button class="btn btn-primary" onClick="getModel()" ><i class="fa fa-heart"></i> Like</button>
                                            <button class="btn btn-primary" onClick="getModel()" ><i class="fa fa-lightbulb"></i> Add to Project</button>
                                    <?php } ?>
									</div>
								</div>
							<?php }  else {?>
								<div class="item" style="padding: 2px; text-align: center;">
									<center><img src="<?=url('/assets/images/gallery/photos/'.$image->image)?>" /></center>
                                    <div class="image-actions" style="padding: 20% 0;">
                                        <?php if(customer('id') !== ''){
                                            $user_id = customer('id');
                                            $image_id = $cat->id;
                                            $img = DB::select("SELECT * FROM image_like WHERE user_id = '".$user_id."' AND image_id = '".$image_id."'");
                                            if(!empty($img)){
                                                ?>
                                                <button class="btn btn-primary" id="likebutton"><span class="tickgreen">✔</span> Liked</button>
                                            <?php } else{?>
                                                <button class="btn btn-primary" id="likebutton" onclick="likeImage(this.value);"><i class="fa fa-heart"></i> Like</button>
                                            <?php } $user_id = customer('id');
                                            $image_id = $cat->id;
                                            $ins = DB::select("SELECT * FROM image_inspiration WHERE user_id = '".$user_id."' AND image_id = '".$image_id."'");
                                            if(empty($ins)){
                                                ?>
                                                <button class="btn btn-primary" id="insbutton" onclick="imageInspiration(this.value);"><i class="fa fa-lightbulb"></i> Add to Project</button>
                                            <?php } else { ?>
                                                <button class="btn btn-primary" id="insbutton"><span class="tickgreen">✔</span> Added to Project</button>
                                            <?php } }  else { ?>
                                            <button class="btn btn-primary" onClick="getModel()" ><i class="fa fa-heart"></i> Like</button>
                                            <button class="btn btn-primary" onClick="getModel()" ><i class="fa fa-lightbulb"></i> Add to Project</button>
                                        <?php } ?>
                                    </div>
								</div>
							<?php } $i++; } ?>
						</div>
						<!-- End Carousel Inner -->
                        <ul class="pro-image-slider" data-slick='{"slidesToShow": 6, "slidesToScroll": 1}'>
                            <?php
                            $i = 0;
                            if(count($images)){ ?>
                            <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" class="active" >
                                <a href="#"><img src="<?=url('/assets/images/gallery/thumbs/'.$cat->image)?>" /></a>
                            </li>
                            <?php $i++; } ?>
							<?php
							foreach($images as $image){
							if($i == 0){?>
								<li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" >
									<a href="#"><img src="<?=url('/assets/images/gallery/photos/thumbs/'.$image->image)?>" /></a>
								</li>
							<?php } else {?>
								<li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>">
									<a href="#"><img src="<?=url('/assets/images/gallery/photos/thumbs/'.$image->image)?>" /></a>
								</li>
							<?php } $i++; } ?>
						</ul>
					</div>
					<div class="icon-holderx">
                        <div class="icons"><i class="fa fa-heart"></i> <b><?=$likes; ?></b> <span class="hidden-xs">Likes</span></div>
                        <div class="icons"><i class="fa fa-eye"></i> <b>48</b> <span class="hidden-xs">Views</span></div>
                        <div class="icons"><i class="fa fa-comment"></i> <b><?=$total_reviews; ?></b> <span class="hidden-xs">Reviews</span></div>
                        <div class="icons borderRight0"><i class="fa fa-lightbulb"></i> <b><?=$inspi; ?></b> <span class="hidden-xs">Collections</span></div>
					</div>
				</div>
				<div class="clearfix"></div>
            </div>
			
			<div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" style="color: #ffffff;"><h4 class="panel-title">
                            Description
                        </h4></a>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <p><?=$cat->description?></p>
                        </div>
                    </div>
                </div>
				<div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" style="color: #ffffff;"><h4 class="panel-title">
                            Ratings & Reviews
                        </h4></a>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse">
                        <div class="panel-body">

                            <div class="feedback-container">
                                <div class="col-lg-12">
                                    <div class="col-lg-4">
                                        <ul class="rating">
                                            <li class="float-left">
                                                <h5 style="margin-bottom: 0px; font-family: Sans-Serif; font-style: bold;">Avarage rating <?=$avg_rating;?></h5>
                                                <div class="rating" style="padding-bottom: 10px;">
                                                    <?php
                                                    $a=$avg_rating;
                                                    $b = explode('.',$a);
                                                    $first = $b[0];
                                                    $rr = $avg_rating;
                                                    $i = 0;
                                                    while($i<5){ $i++;?>
                                                        <i class="star<?=($i<=$avg_rating) ? '-selected' : '';?>"></i>
                                                        <?php $rr--; }
                                                    echo '</div>';
                                                    ?>
                                                    <h5 style=" font-family: Sans-Serif; font-style: bold;">Total rating <?=$total_user;?></h5>
                                            </li>
                                        </ul>
                                        <hr>
                                        <ul class="rating">
                                            <li class="float-left">
                                                <h5 style=" font-family: Sans-Serif; font-style: bold;">Total comment <?=$total_reviews; ?></h5>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-8 reviews-graph">
                                        <ul>
                                            <li class="float-left">
                                                <?php
                                                if($rating5 != ''){
                                                    ?>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 25px;">
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;">5 <i class="fa fa-star"></i></div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="<?php $a = $rating5->rating5_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>"
                                                                     aria-valuemin="0" aria-valuemax="100" style="width:<?php $a = $rating5->rating5_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>%">
                                                                    <span class="sr-only"><?=$rating5->rating5_user;?> Reviews</span>
                                                                </div>
                                                            </div>
                                                            <span class="mob-rev-count">(<?=$rating5->rating5_user;?> Reviews)</span>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 rev-count">(<?=$rating5->rating5_user;?> Reviews)</div>
                                                    </div>

                                                    <?php
                                                }
                                                if($rating4 != ''){
                                                    ?>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 25px;">
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;">4 <i class="fa fa-star"></i></div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="<?php $a = $rating4->rating4_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>"
                                                                     aria-valuemin="0" aria-valuemax="100" style="width:<?php $a = $rating4->rating4_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>%">
                                                                    <span class="sr-only"><?=$rating4->rating4_user;?> Reviews</span>
                                                                </div>
                                                            </div>
                                                            <span class="mob-rev-count">(<?=$rating4->rating4_user;?> Reviews)</span>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 rev-count">(<?=$rating4->rating4_user;?> Reviews)</div>
                                                    </div>
                                                    <?php
                                                }
                                                if($rating3 != ''){
                                                    ?>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 25px;">
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;">3 <i class="fa fa-star"></i></div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="<?php $a = $rating3->rating3_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>"
                                                                     aria-valuemin="0" aria-valuemax="100" style="width:<?php $a = $rating3->rating3_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>%">
                                                                    <span class="sr-only"><?=$rating3->rating3_user;?> Reviews</span>
                                                                </div>
                                                            </div>
                                                            <span class="mob-rev-count">(<?=$rating3->rating3_user;?> Reviews)</span>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 rev-count">(<?=$rating3->rating3_user;?> Reviews)</div>
                                                    </div>

                                                    <?php
                                                }
                                                if($rating2 != ''){
                                                    ?>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 25px;">
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;">2 <i class="fa fa-star"></i></div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="<?php $a = $rating2->rating2_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>"
                                                                     aria-valuemin="0" aria-valuemax="100" style="width:<?php $a = $rating2->rating2_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>%">
                                                                    <span class="sr-only"><?=$rating2->rating2_user;?> Reviews</span>
                                                                </div>
                                                            </div>
                                                            <span class="mob-rev-count">(<?=$rating2->rating2_user;?> Reviews)</span>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 rev-count">(<?=$rating2->rating2_user;?> Reviews)</div>
                                                    </div>
                                                    <?php
                                                }
                                                if($rating1 != ''){
                                                    ?>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 25px;">
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;">1 <i class="fa fa-star"></i></div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="<?php $a = $rating1->rating1_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>"
                                                                     aria-valuemin="0" aria-valuemax="100" style="width:<?php $a = $rating1->rating1_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>%">
                                                                    <span class="sr-only"><?=$rating1->rating1_user;?> Reviews</span>
                                                                </div>
                                                            </div>
                                                            <span class="mob-rev-count">(<?=$rating1->rating1_user;?> Reviews)</span>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 rev-count">(<?=$rating1->rating1_user;?> Reviews)</div>
                                                    </div>

                                                    <?php
                                                }
                                                ?>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12" style="padding-bottom: 10px;">
                                <form action="<?php echo url('review');?>" method="post" class="form-horizontal single">
                                    <?=csrf_field();?>
                                    <h5 style="float: left; width: 100%; margin-top: 10px; border-top: 1px solid #000000; padding-top: 20px;"><?=translate('Add a review')?> :</h5>
                                    <div id="response" style="float: left; width: 100%"></div>
                                    <?php if(customer('id') !== ''){?>
                                        <fieldset style="float: left; width: 100%;">
                                            <div class="col-md-12">
                                                <input name="name" readonly value="<?=customer('id')?>" class="form-control" type="hidden">
                                                <input name="product" value="<?=$cat->id?>" class="form-control" type="hidden">

                                                <div class="form-group col-md-12">
                                                    <label class="control-label"><?=translate('Rating')?></label>
                                                    <div id="star-rating">
                                                        <input type="radio" name="rating" class="rating" value="1" />
                                                        <input type="radio" name="rating" class="rating" value="2" />
                                                        <input type="radio" name="rating" class="rating" value="3" />
                                                        <input type="radio" name="rating" class="rating" value="4" />
                                                        <input type="radio" name="rating" class="rating" value="5" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label"><?=translate('Review')?></label>
                                                    <textarea name="review" type="text" rows="5" class="form-control"></textarea>
                                                </div>
                                                <button data-product="<?=$cat->id?>" name="submit" class="btn btn-primary" ><?=translate('submit')?></button>
                                            </div>
                                        </fieldset>
                                    <?php } else { ?>
                                        <div class="row" style="padding: 15px 0; margin-top: -55px;">
                                            <div class="col-md-12 text-center">
                                                <button class="btn btn-primary" onClick="getModel()" type="button" value="Login">Login</button>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </form>
                                <br><br><br>
                                <p>
                                    <?php
                                    $count = 0;
                                    foreach($reviews as $review){
                                        $usid = $review->name;
                                        $uimg = DB::select("SELECT * FROM customers WHERE id = '".$usid."'")[0];
                                        echo '<img class="review-image" src="assets/user_image/'; if($uimg->image != ''){ echo $uimg->image; } else { echo 'user.png'; }; echo'">
											<div class="review-meta"><b>'.$uimg->name.'</b><br/>
											<span class="time">'.date('M d, Y',$review->time).'</span><br/></div>
											<div class="review">
												<div class="rating pull-right">';
                                        $rr = $review->rating; $i = 0; while($i<5){ $i++;?>
                                            <i class="star<?=($i<=$review->rating) ? '-selected' : '';?>"></i>
                                            <?php $rr--; }
                                        echo '</div>
											<div class="clearfix"></div>
											<p>'.nl2br($review->review).'</p></div>';
                                        $count++;
                                    }
                                    if($count > 0){
                                        echo '<hr/>';
                                    }
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
    <section class="at-property-sec">
	<div class="container">
		<main>
			<input type="hidden" value="<?=$cat->slug; ?>" name="gallery_slug"/>
			
		<?php echo csrf_field(); ?>
		  <input id="tab1" type="radio" name="tabs" checked onclick="getRelatedGallery()">
		  <label for="tab1" >Related Galleries</label>
			
		  <input id="tab2" type="radio" name="tabs" onclick="getRecentViewedGallery();">
		  <label for="tab2">Recently Viewed Galleries</label>
			<section id="content1">
				<div class="row">
					<div class="col-md-12" id="data1">
						<div class="grid">
							<?php foreach($related as $rel){  ?>
								<div class="grid-item">
									<div class="tiles">
										<a href="<?php echo url('/photo-gallery/'.$rel->slug); ?>">
											<img src="<?php echo url('/assets/images/gallery/'.$rel->image); ?>"/>
											<div class="icon-holder">
												<div class="icons1 mrgntop"><?=substr(translate($rel->title), 0, 20); if(strlen($rel->title)>20) echo '...';?></div>
												<div class="icons2 mrgntop borderRight0"><?=getPlanPhotsCount($rel->id); ?> Photos</div>
											</div>
										</a>
									</div>
								</div>
                            <?php } ?>
						</div>
					</div>
				</div>
			</section>
			
			<section id="content2">
				<div class="row">
					<div class="col-md-12" id="data2">
						
					</div>
				</div>
			</section>
		</main>
		</div>
	</section>

    <button class="btn btn-primary hidden" id="open-service-modal1" data-popup-open="popup-login">Get Started !</button>
    <div class="popup" id="popup-login" data-popup="popup-login">
        <div class="popup-inner" style="padding-top: 120px;">
            <div class="col-md-6 account" style="border: none; box-shadow: none;">
                <?php
                if (session('error') != ''){
                    echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
                }
                ?>
                <form action="<?php echo url('login-model'); ?>" method="post" class="form-horizontal single">
                    <?=csrf_field() ?>
                    <fieldset>
                        <div class="form-group">
                            <label class="control-label"><?=translate('E-mail') ?></label>
                            <input name="email" type="email" value="<?=isset($_POST['email']) ? $_POST['email'] : '' ?>" class="form-control"  />
                        </div>
                        <div class="form-group">
                            <label class="control-label"><?=translate('Password') ?></label>
                            <input name="password" type="password" class="form-control"  />
                        </div>
                        <input name="login" type="submit" value="<?=translate('Login') ?>" class="btn btn-primary" />
                    </fieldset>
                </form>
                <div class="text-center"><a class="smooth" href="<?=url('reset-password')?>">Forgot your password ?</a></div>
            </div>
            <a class="popup-close" data-popup-close="popup-login" href="#">x</a>
        </div>
    </div>
    <!-- Services Popup End -->


    <button class="btn btn-primary hidden" id="open-project-modal1" data-popup-open="popup-project">Get Started !</button>
    <div class="popup" id="popup-project" data-popup="popup-project">
        <div class="popup-inner" style="padding-top: 120px;">
            <div class="col-lg-12 account" style="border: none; box-shadow: none;">
                <?=csrf_field() ?>
                <label class="control-label">Select Your Project</label>
                <button type="button" name="create" class="btn btn-primary" onClick="getcreate()" style="float: right;"><i class="fa fa-plus"></i> Create a new project </button>
                <div id="projectName">

                </div>
            </div>
            <a class="popup-close" id="pclose" data-popup-close="popup-project" href="#">x</a>
        </div>
    </div>

<a class="btn btn-primary hidden" id="project-modal2" data-popup-open="popup-project2" href="#">Get Started !</a>
<div class="popup" id="popup-project2" data-popup="popup-project2">
    <div class="popup-inner" style="padding-top: 120px;">
        <div class="col-md-10 account" style="border: none; box-shadow: none;">
            <?php
            if (session('error') != ''){
                echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
            }
            ?>
            <form action="<?php echo url('create-project'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal single">
                <table class="responsive-table bordered" style="width: 50%; margin: auto;">
                    <tbody>
                    <input type="hidden" name="user_id" value="<?=customer('id'); ?>">
                    <tr>
                        <td>Project Title</td>
                        <td>:</td>
                        <td><input type="text" name="title" required class="form-control"></td>
                    </tr>
                    <tr>
                        <td>Project Image</td>
                        <td>:</td>
                        <td><input type="file" name="image" required class="form-control"></td>
                    </tr>
                    <?php echo csrf_field(); ?>
                    <tr>
                        <td colspan="3"><input type="submit" name="submit" value="Update" class="btn btn-primary btn-lg"></td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <a class="popup-close" data-popup-close="popup-project2" href="#">x</a>
    </div>
</div>
<?php echo $footer?>
<script>
    $(function() {
//----- OPEN
        $('[data-popup-open]').on('click', function(e) {
            var targeted_popup_class = jQuery(this).attr('data-popup-open');
            $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
            e.preventDefault();
        });
//----- CLOSE
        $('[data-popup-close]').on('click', function(e) {
            var targeted_popup_class = jQuery(this).attr('data-popup-close');
            $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
            e.preventDefault();
        });
    });
    function getModel(){
        //alert('hello');
        $('#open-service-modal1').click();
    }

    function likeImage() {
        $.ajax({
            type: "POST",
            url: "{{route('retailLikeImage')}}",
            data:'_token=<?=csrf_token();?>&image_id=<?=$cat->id; ?>',
            success: function(data){
                $('#likebutton').html('<span class="tickgreen">✔</span> Liked');
            }
        });
    }

    function addProject(val) {
        var project_id = val;
        $.ajax({
            type: "POST",
            url: "{{route('retailAddToProject')}}",
            data:'_token=<?=csrf_token();?>&image_id=<?=$cat->id; ?>&project_id='+project_id,
            success: function(data){
                $('#insbutton').html('<span class="tickgreen">✔</span> Added to Project');
                $('#pclose').click();
            }
        });
    }

    function imageInspiration() {
        $.ajax({
            type: "POST",
            url: "{{route('retailImageInspiration')}}",
            data:'_token=<?=csrf_token();?>&image_id=<?=$cat->id; ?>',
            success: function(data){
                    //alert('hello');
                $('#open-project-modal1').click();
                $('#projectName').html(data);
            }
        });
    }

    function getcreate(){
        event.preventDefault();
        $('#project-modal2').click();
    }

</script>
<!-- Services Popup Start -->
<script src="<?=$tp;?>/assets/jquery.flexslider.js"></script>
<script src="<?=$tp;?>/assets/jquery.zoom.js"></script>
<script>
jQuery(window).on('load', function(){ 
	var $ = jQuery;
	var $container = $('.grid');
    $container.masonry({
          columnWidth:10, 
          gutterWidth: 15,
          itemSelector: '.grid-item'
    });
});
(function() {

	[].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {
		new CBPFWTabs( el );
	});

})();

$( document ).ready(function() {
	
	$('#star-rating').rating();
	$('#carousel').flexslider({
		animation: "slide",
		controlNav: true,
		animationLoop: true,
		slideshow: true,
		itemWidth: 210,
		itemMargin: 5,
		minItems: 4,
		maxItems: 6,
		asNavFor: '#slider'
	});
	
	$('#slider').flexslider({
		animation: "slide",
		controlNav: true,
		animationLoop: true,
		slideshow: true,
		sync: "#carousel",
		touch: true,
		keyboard: true,
		smoothHeight: true, 
	});
	$('.select2').select2();
	
	
});
</script>


