<?php echo $header?>
    <div id="mountRoot" style="min-height:500px;margin-top:-2px;">
    <div data-reactroot="" data-reactid="1" data-react-checksum="525647675">
        <div data-reactid="2">
            <div class="tac-container clearfix" data-reactid="3">
                <div class="tac" data-reactid="9">
                    <div class="XYZ container">
                        <div class="content clearfix">
                            <div class="sale-header"><u>TERMS AND CONDITIONS </u></div>
                            <div class="sale-header">
                                <div> Welcome to XYZ. This page details a limited period Offer provided by our participating sellers redeemable through our XYZ Marketplace Platform only in the mobile application. Please read these terms and conditions
                                    carefully before participating. You should understand that by participating in the Offer and using our Platform, you agree to be bound by these terms and conditions including Terms of Use and Privacy Policy of the Platform.
                                    Please understand that if you refuse to accept these terms and conditions, you will not be able to access the offers provided under our Platform.
                                </div>
                                <br><br>
                                <b id="offer41"><u>Fashion your sport - Terms and Conditions</u></b>
                                <p><strong><u>Coupons Policy</u></strong></p>
                                <ol>
                                    <li>The Player needs to go through the full quiz to reach level 1</li>
                                    <li>The Player gets&nbsp;Rs 300 worth coupon (within 2 working days of completing the Quiz)in a mail and/or notification applicable on Min purchase of 999</li>
                                    <li>The Player needs to make a purchase worth 3k to reach Level 2 to get Rs 450 worth coupon (within 2 working days) applicable on Min purchase of 999</li>
                                    <li>The coupon will be valid till 19<sup>th</sup> May, 2019 and on More than 2L styles</li>
                                    <li>The validity and use of Coupon will be subject to the XYZ Terms and Conditions.</li>
                                </ol>
                                <p><strong><u>XYZ Campaign Policy</u></strong></p>
                                <p><strong><u>Wrogn RCB Campaign Policy</u></strong></p>
                                <p>This document is an electronic record in terms of Information Technology Act, 2000, and the Rules thereunder as applicable and the amended provisions pertaining to electronic records in various statutes as amended by the
                                    Information Technology Act, 2000. This electronic record does not require any physical or digital signatures. Wherever the context so requires “<strong>Participant/You/Your</strong>” shall mean the person who purchases
                                    products worth a minimum bill value of INR 5,000 (Rupees Five Thousand only) after accounting for all discounts from the brand named “Wrogn” (“<strong>Offering Brand</strong>”) which is available at the XYZ app,
                                    M-site or website during the Offer Period.</p>
                                <p>By participating in this Offer, You agree to be bound by the terms of this Policy, the Terms of Use, Privacy Policy and other relevant documentation that are available on <a href="http://www.XYZ.com/">www.XYZ.com</a>&nbsp;including
                                    any modifications, alterations or updates.</p>
                                <p>Universal Sportsbiz Private Limited (“<strong>Seller</strong>”) is offering:</p>
                                <ul>
                                    <li>two (2) tickets to Participants who purchase products worth INR 5,000 (Rupees Five Thousand only) (“<strong>Qualifying Amount</strong>”) from the Offering Brand;</li>
                                </ul>
                                <p>for one of the cricket matches held in Bangalore where the Royal Challengers Bangalore team will be contesting (“<strong>Tickets</strong>”) throughout the Offer Period until the ticket stocks last (“<strong>Offer</strong>”).
                                    The Offer is available on 22nd March, 2019 from 00:01 hours till 23:59 hours on 19th May, 2019 (“<strong>Offer Period</strong>”).</p>
                                <p>The winner would be the Participant who shops from the same user account amounting to Qualifying Amount during the Offer Period (“<strong>Winner</strong>”). Winners will be declared via an email to Your registered ID.</p>
                                <p>Once the Winner is finalized from XYZ, the details of the Winner shall be shared with the Seller. The Seller will directly reach out to the Winner via a phone call and an email and specify the place from which the Ticket(s)
                                    need to be collected by the Winners.</p>
                                <p>The Tickets may be utilized by the Winners solely for their personal use.</p>
                                <p>This Offer is not transferable and shall not be settled with cash in lieu by XYZ or the Seller.</p>
                                <p><strong>Eligibility and Disqualification:</strong></p>
                                <ul>
                                    <li>To be eligible for the Offer, the Participant should:</li>
                                    <li>be a permanent resident of India;</li>
                                    <li>above the age of 18 years as on 25<sup>th</sup> March, 2018;</li>
                                    <li>have shopped from the same user account; and</li>
                                    <li>shopped for the Qualifying Amount during Offer Period according to XYZ’s records.</li>
                                </ul>
                                <p>If the Participant cancels/ returns any part of his/her order, the amount spent shall be revised for this Offer. If a Participant’s order is in the replacement cycle, that particular item’s spend will be deducted for the
                                    purpose of calculating the Winners.</p>
                                <ul>
                                    <li>The Winner for a day is not eligible to win for a subsequent day during the Offer Period.</li>
                                </ul>
                                <ul>
                                    <li>Participants are not bound in any way to participate in this Offer. Any participation is voluntary and the Offer is being made purely on a best effort basis.</li>
                                </ul>
                                <ul>
                                    <li>Seller and/or XYZ, in its sole discretion, reserves the right to disqualify You from the benefits of this Offer, if any fraudulent activity is identified as being carried out for the purpose of availing the benefits
                                        under the said Offer.</li>
                                </ul>
                                <ul>
                                    <li>XYZ/Seller reserves the right, in its sole discretion, to disqualify any Participant that tampers or attempts to violate the Policy.</li>
                                </ul>
                                <p><strong>Other terms:</strong></p>
                                <ul>
                                    <li>This is a limited period offer.</li>
                                    <li>This Offer will be applicable when the Participant purchases products from the Offering Brand during the Offer Period.</li>
                                    <li>The Offer cannot be combined with any other online offer on XYZ sponsored and/or offered by the Seller.</li>
                                    <li>Once You have collected the Ticket(s), You cannot return the Ticket(s) to the Seller.</li>
                                    <li>The Ticket(s) need to be collected from the place specified by the Seller when the Seller contacts You and XYZ/Seller shall not bear any transport related expenses that the Winners might incur in collecting the Ticket(s)
                                        or in attending the match.</li>
                                    <li>The Seller might require You to prove your identity by producing any identity proof at the time of collection of the Ticket(s) and You agree to fulfill such requirements to obtain the Ticket(s).</li>
                                    <li>The Seller, shall, at its sole discretion decide the match for which the Ticket(s) will be offered to the Winners.</li>
                                    <li>The Seller reserves the right to decide the category of the Ticket(s) that will be given to the Winners under this Offer.</li>
                                    <li>It is clarified that in addition to these terms and conditions agreed with XYZ, the Participant shall also be bound by the terms and conditions imposed by the Seller and the Tickets.</li>
                                    <li>Notwithstanding anything contained in this Policy or the terms and conditions imposed by the Tickets, in the event that the particular match gets cancelled or postponed, the Winner will not get any refund or a new ticket.</li>
                                    <li>All Participants shall ensure that the mobile number(s), e-mail address and/or other details provided by them to XYZ are true, accurate and in use, at the time of the online retail transaction. Any liability, consequence
                                        or claim arising on account of any incorrect or outdated information provided by a Participant to XYZ, shall solely be borne by the affected Participant. XYZ shall not be liable to verify the accuracy and/or
                                        correctness of the information so provided by the Participant.</li>
                                    <li>XYZ/Seller reserves the right, at their sole discretion, to change, modify, add or remove portions of this Policy, at any time without any prior written notice to You.</li>
                                    <li>You acknowledge and confirm that You are aware of the nature of telecommunications/ internet services and that an e-mail transmission may not be received properly and may be read or be known to any unauthorised persons.
                                        You agree to assume and bear all the risks involved in respect of such errors and misunderstanding and XYZ and the Seller shall not be responsible in any manner.</li>
                                    <li>This Offer is valid only in India.</li>
                                    <li>Further, such Offer does not give any additional guarantee or warranty whatsoever.</li>
                                    <li>XYZ and the Seller reserve the right to determine the Winner and such determination shall be as per the terms of this Policy. XYZ and the Seller’s decision in this regard shall be final.</li>
                                    <li>The Offer shall be subject to force majeure events and on occurrence of such an event, the Offer may be withdrawn at the discretion of XYZ and/or the Seller.</li>
                                    <li>XYZ/Seller shall not be responsible for any loss, injury or any other liability arising due to participation by any person in the Offer.</li>
                                    <li>You hereby agree to indemnify and keep XYZ/Seller harmless against all damages, liabilities, costs, expenses, claims, suits and proceedings (including reasonable attorney’s fee) that may be suffered by XYZ/Seller
                                        as a consequence of (i) violation of terms of this Policy by You; (ii) violation of applicable laws; (iii) any action or inaction resulting in willful misconduct or negligence on Your part.</li>
                                    <li>This Policy shall be governed in accordance with the applicable laws in India. Courts at Bangalore shall have the exclusive jurisdiction to settle any dispute that may arise under this Policy.</li>
                                    <li>This Offer is not applicable for the Participants from the State of Tamil Nadu in view of the provisions of Tamil Nadu Prize Scheme (Prohibition) Act 1979.</li>
                                </ul>
                                <p>The offer shall be subject to the Income-Tax Act, 1961 and all disbursements shall be subject to TDS, as applicable.</p>
                                <br><br>
                                <b id="offer2"><u>WIN A TRIP TO COPENHAGEN CONTEST TERMS &amp; CONDITIONS</u></b>
                                <p><strong>Important: Please read these Terms &amp; Conditions before entering the #WinaTriptoCopenhagenDenmark </strong><strong>Contest </strong><strong>to be conducted by Fossil India Pvt Ltd. By participating in the Contest, you agree to be bound by these Terms &amp; Conditions and represent that you satisfy all of the eligibility requirements set out herein below</strong></p>
                                <p>TERMS &amp; CONDITIONS</p>
                                <ol>
                                    <li>Who can participate?</li>
                                </ol>
                                <ul>
                                    <li>&nbsp;&nbsp;The participant should be a citizen of India residing in India and above 18 years of age at the time of participating in the contest to participate in this Contest (‘Participant’)</li>
                                    <li>&nbsp;&nbsp;This Contest is not open to Fossil employees, officers and directors and their immediate family members (Parents, spouse, siblings and children) and its Dealer(s), Partner(s), Agencies, Trade Partners and
                                        their immediate family members (spouse, children, parents, siblings, regardless of where they live), Agencies and their immediate family members (spouse, children, parents, siblings, regardless of where they live)</li>
                                </ul>
                                <ol start="2">
                                    <li>Contest Period: Contest will be open from 12:01 p.m. IST on 22<sup>th</sup> Mar 2019 to 11:59 p.m. IST 15 April 2019.</li>
                                    <li>Contest details – Buy a Skagen watch at full price and stand a chance to Win a trip to Copenhagen, Denmark :</li>
                                </ol>
                                <ul>
                                    <li>&nbsp;&nbsp; Buy a full price (non-discounted) Skagen Watch from any authorized online partner between 12:01 p.m. IST on 22 Mar 2019 to 11:59 p.m. IST 15 April 2019.</li>
                                    <li>&nbsp; &nbsp;&nbsp; Please keep your original invoice handy to participate</li>
                                    <li>&nbsp;&nbsp; The offer is valid on Watches Only</li>
                                    <li>&nbsp;&nbsp; Customers who would like to participate in this Contest will be requested to share their personal information – including but not limited to the following for the purposes of verification:</li>
                                    <li>&nbsp;Name</li>
                                    <li>&nbsp;Email address</li>
                                    <li>&nbsp;Mobile number</li>
                                    <li>&nbsp;Purchase details</li>
                                    <li>&nbsp;City</li>
                                    <li>&nbsp;Invoice no</li>
                                    <li>&nbsp;Upload Invoice</li>
                                </ul>
                                <ol start="4">
                                    <li>Prizes:</li>
                                </ol>
                                <ul>
                                    <li>&nbsp;&nbsp;The winner will be picked at random.</li>
                                    <li>&nbsp;&nbsp;The winner will be announced not later than 15<sup>th</sup> June 2019</li>
                                    <li>&nbsp;&nbsp;The winner will be announced on @skagendenmark Facebook Page. Fossil India Pvt. Ltd. will contact the winner to arrange for the Prize fulfilment and travel details. The winner will be eligible for the final
                                        prize only if the product has not been returned.</li>
                                    <li>&nbsp;&nbsp;The winner will stand a chance to experience a Holiday Package in Skagen, Denmark, for two adults, <em>worth a maximum value of Rs.3.0 Lacs only.</em></li>
                                    <li>&nbsp;&nbsp; The package will comprise of:</li>
                                </ul>
                                <ol>
                                    <li>3 Nights &amp; 4 Days getaway to the Copenhagen, Denmark</li>
                                    <li>Economy class return flight tickets for two adults, on receipt of the valid visa subject to availability and the departing from [New Delhi / Mumbai / Bangalore]</li>
                                </ol>
                                <p>&nbsp;&nbsp;&nbsp;iii. &nbsp;&nbsp; Fixed Boarding, Lodging and travel expense will be paid to the winner closer to the actual date of travel.</p>
                                <ol>
                                    <li>The fixed expense will be paid in Euros equal to INR 90,000</li>
                                    <li>The winner will book hotel, all meals, arrange local travel &amp; sightseeing by him/her-self</li>
                                </ol>
                                <ul>
                                    <li>&nbsp;&nbsp; If the selected winner is ineligible, cannot be reached, fails to claim the Prize within three (3) days of receiving the notification, or does not abide by the terms and conditions at any time, or is unable
                                        to get the necessary visas for travel to Denmark, the Prize will be forfeited and may be awarded to a substitute winner. If Fossil India Pvt. Ltd. is unable to award a Prize to the winner or substitute winner, Fossil
                                        India Pvt. Ltd reserves the right to consider that Prize unclaimed.</li>
                                    <li>&nbsp;&nbsp; Winner is required to present the original receipt registered in this Contest for the purchase of Skagen watch (duplicate or copy of the original receipt is not accepted) to redeem the Prize</li>
                                    <li>&nbsp;&nbsp; All incidentals cost such as but not limited to, passport and visa costs and all other personal expenditures will be the winner’s responsibility and sole expense. Airline will be selected by Fossil India
                                        Pvt. Ltd. at its sole discretion. The Prize may also be subject to black-out dates claimed by the airline. The length of the stay may not be extended. Once the travel period dates have been booked, no changes, extensions
                                        or substitutions of the travel dates is permitted, except at Fossil India Pvt. Ltd sole discretion. Fossil India Pvt. Ltd shall not be responsible for any cancellations, delays, diversions or substitution or any
                                        act or omissions whatsoever by the air carriers, hotels or other transportation companies or any other persons providing any of the services and accommodations to the any Prize winner. No refund or compensation
                                        will be made in the event of the cancellation or delay of any flight. Further, Fossil India shall not be responsible in case of failed visa or passport application</li>
                                    <li>&nbsp;&nbsp; Travel and other insurance are not included. Prize winner is advised to purchase his/her own insurance</li>
                                    <li>&nbsp;&nbsp; All cost, fees and/or expenses incurred or to be incurred by the winner in relation to the Contest and/or to claim the Prize, including but not limited, visas, additional occupancy charges, telephone bills,
                                        and/or any other costs, shall be borne and are the sole responsibility of the winner. Fossil India shall not bear the cost of any damage to the hotel or service provided during the trip.</li>
                                    <li>&nbsp;&nbsp; Prize is non-transferable and not exchangeable for cash or any other items or packages. &nbsp;The winner must be on the trip. The winner and the companion must travel together.&nbsp; If the winner does
                                        not start the trip for any reason, his/her companion may not start the trip.</li>
                                    <li>&nbsp;&nbsp; The winner (including the companion) shall conduct themselves in a reasonable manner during the entire course of the trip.</li>
                                </ul>
                                <ol start="5">
                                    <li>Fossil reserves the right to cancel, discontinue, prematurely withdraw, change, alter or modify this Contest and/or the Prize or any part thereof, the Terms &amp; Conditions at their sole discretion at any time during
                                        its validity as may be required in view of business exigencies and/or changes by regulatory authority and/or statutory changes and the same shall be binding on the participant</li>
                                    <li>The Participant shall read these Terms &amp; Conditions carefully while participating in the Contest. Further, it will be assumed that the Participant has read and understood and agreed to abide by all the Terms &amp;
                                        Conditions of this Contest</li>
                                    <li>Fossil reserves the right to modify these Terms &amp; Conditions without any prior notice</li>
                                    <li>By participating in the Contest, it shall be construed that the Participants, the Winner of the Prize have waived his/ her right to raise any dispute with regard to the Prize and/or in any manner whatsoever in connection
                                        to the Prize.</li>
                                    <li>The entries submitted by any individual/ Participant under this Contest or otherwise shall be treated as his / her own independent personal views and submissions. Fossil and/or its affiliates do not acknowledge or agree
                                        with any such content expressed in submitted entries and further shall not be held liable for any defamatory, abusive language used and loss occurring to any third party</li>
                                    <li>Participant shall indemnify Fossil and its group companies/ officials/ directors/ sister concern / parent Company or its affiliate/its licensors and their respective affiliates from any loss that may occur to Fossil
                                        or any liability/ damages/ cost / cost of litigation/ cost of attorney / third party liabilities / claims resulting due to non-adherence to these Terms and Conditions or due to submission of abusive, defamatory
                                        content submitted for the Contest</li>
                                    <li>Fossil reserves the right to remove, refuse or cancel any posted entry/ answer or any access to any Participant without any reason whatsoever</li>
                                    <li>The Participant shall not use the Trade Mark- Skagen/ Logo/ Brand Name/ or other Intellectual Property Rights belonging to Fossil or its Group Company/ affiliates and or concerned third parties etc. and the same shall
                                        always remain the property of Fossil</li>
                                    <li>Contest is subject to force majeure events and situations beyond the control of Fossil and/or its affiliates</li>
                                    <li>General Release. By entering the Contest, you indemnify Fossil, its licensors/participating sponsors and any of their respective parent companies, subsidiaries, affiliates, directors, officers, employees and agencies
                                        (collectively, the "Indemnified Parties") from any liability whatsoever, and waive any and all causes of action, related to any claims, costs, injuries, losses, or damages of any kind arising out of or in connection
                                        with the Contest or delivery, wrong delivery, acceptance, possession, use of or inability to use any Prize (including, without limitation, claims, costs, injuries, losses and damages related to personal injuries,
                                        death, damage to or destruction of property, rights of publicity or privacy, defamation or portrayal in a false light, whether intentional or unintentional), whether under a theory of contract, tort (including negligence),
                                        warranty or other theory</li>
                                    <li>The Winner of the Contest agree that he/she shall make himself/herself available from time to time and co-operate with the Company and participate in any activity and/or campaign (Promotional Activity) of any nature
                                        whatsoever without any money payable to the Winner. Further, the Winner shall at no point of time refuse or disagree to participate in any such Promotional Activity for any reason whatsoever. The Winner agree that
                                        the footage of any nature with regard to the them shall vest with the Company, including but not limited to all intellectual property rights and any other rights for worldwide and in perpetuity</li>
                                    <li>Fossil Ownership of Entries. All entries and materials submitted to Fossil in connection with the Contest (collectively, "Entry Materials"), along with all copyright, trademark and other proprietary rights associated
                                        therewith, become the property of Fossil upon submission, and Entry Materials shall not be returned to any entrant. Without limiting the generality of the foregoing, you acknowledge that Fossil shall own all rights
                                        to use, modify, reproduce, publish, perform, display, distribute, make derivative works of and otherwise commercially and non-commercially exploit the Entry Materials in perpetuity and throughout the universe, in
                                        any manner or medium now existing, without separate compensation to you or any other person or entity. You agree to take, at Fossil's expense, any further action (including, without limitation, execution of affidavits
                                        and other documents) reasonably requested by Fossil to effect, perfect or confirm Fossil's ownership rights as set forth above in this Paragraph.</li>
                                    <li>Disclaimer. Fossil, and any of their respective parent companies, subsidiaries, affiliates, directors, officers, professional advisors, employees and agencies shall not be responsible for: (a) any late, lost, misrouted,
                                        garbled or distorted or damaged transmissions or entries; (b) telephone, electronic, hardware, software, network, Internet, or other computer- or communications-related malfunctions or failures; (c) any Contest
                                        disruptions, injuries, losses or damages caused by events beyond the control of Fossil; (d) any printing or typographical errors in any materials associated with the Contest; or (e) in the event the response to
                                        a Contest Question infringes the intellectual property, privacy, or publicity rights or any other legal or moral rights of any third party, or violates applicable laws and regulations of India, or is discriminatory
                                        (including without limitation racial, gender or religious slurs) and/or constituting hatred statements; or is designed to incite violence or hatred, or threats to cause bodily harm, or comprises of illegal or unlawful
                                        content, or content that is known to be false or misleading. You acknowledge and agree that the Contest is not sponsored, endorsed or administered by Twitter or Facebook and you shall not make any claims or demands
                                        or suits against Twitter or Facebook for any reason with respect to the Contest.</li>
                                </ol>
                                <p>Other Terms &amp; conditions</p>
                                <ol>
                                    <li>Fossil is not liable for the damages/ loss that the User/ Participant may suffer to his/ her Device or Instrument or Cell Phone or otherwise any loss/ damages arising out of his/ her participation in this Contest or
                                        loss of content of Contest of any Participants.</li>
                                    <li>Participant hereby authorizes Fossil to communicate with him / her from time to time through telecommunication, email etc. about the matters connected with the Contest.</li>
                                    <li>Participant unconditionally agrees that he/she shall not:</li>
                                </ol>
                                <ul>
                                    <li>&nbsp;&nbsp; do anything which violates any of the Terms &amp; Conditions;</li>
                                    <li>&nbsp;&nbsp; make any misrepresentation;</li>
                                    <li>&nbsp;&nbsp; do anything that does not comply with generally accepted internet etiquette including (without limitation) use of inflammatory or antagonistic criticism ("flaming"), or wastefully and unnecessarily including
                                        previous communications in any postings;</li>
                                    <li>&nbsp;&nbsp; perform system abuse;</li>
                                    <li>&nbsp;&nbsp; propagate, distribute or transmit Destructive Code, whether or not damage is actually caused thereby;</li>
                                    <li>&nbsp;&nbsp; Post abusive, obscene, threatening, harassing, defamatory, libelous, offensive or sexually explicit material;</li>
                                    <li>&nbsp;&nbsp; Intentionally make false or misleading statements;</li>
                                    <li>&nbsp;&nbsp; Post material that infringes copyright/ Trade Mark / IPR;</li>
                                    <li>&nbsp;&nbsp; Post information that he/ she knows to be confidential or sensitive or otherwise in breach of the law.</li>
                                </ul>
                                <ol start="4">
                                    <li>Participants agree to make themselves available for publicity purposes if requested by Fossil.</li>
                                    <li>Participants agree to the use of their entry photo/ pictures for such publicity by Fossil on various digital media such as Facebook, Twitter, email etc. without any extra / additional compensation.</li>
                                    <li>Whenever called for, Participant will be required to complete all formalities as may be communicated by Fossil and/or its affiliate including but not limited to providing his/her authentic and correct information or
                                        other required documents, failing which he/she shall be disqualified at any time or at any stage. In case false information is revealed at any time after participation, the Participant shall be liable to return
                                        all benefits/ Prizes provided under this Contest. Winner shall unequivocally and completely comply with the following requirement simultaneously for claiming the Prize-</li>
                                    <li>Provide proof of identity and age e.g. copy of Aadhar Card, Passport, or other documents as required by Fossil.</li>
                                    <li>Fossil reserves the right to restrict / bar any person from participating in the Contest without quoting any reasons for the same.</li>
                                    <li>Participants found indulging in any malpractices such as cheating or creating dubious profiles, multiple entries, participation in the contest for more than one time etc., shall be disqualified from the Contest at any
                                        time and shall not be eligible for any Prize and Fossil reserves the right to take any further action including initiating legal proceedings as it may deem necessary.</li>
                                    <li>All the content entered by participant becomes Fossil property and can be used by Fossil as, where &amp; when they deem appropriate.</li>
                                    <li>This Contest is not applicable in a country outside India or any state of India if it is prohibited by law in such state.</li>
                                    <li>&nbsp; Fossil shall not be responsible for any loss or damage due to Act of God, Governmental action, other force majeure circumstances and shall not be liable to pay any amount as compensation or otherwise for the
                                        same.</li>
                                    <li>The winner shall be asked for his/ her valid and reachable mobile number or valid and working email id, Fossil shall not be liable in any manner whatsoever for any Participant who cannot be contacted on the mobile number
                                        or if his / her email id is incorrect.</li>
                                    <li>The Prize winner, if required by Fossil, shall execute all such documentation as deemed distribute, publish, sell, in all and all locations, in any medium, form or format, in any number, the picture, image or likeness
                                        and the reproductions of the winner, and any biographical information furnished by the winner to Fossil.</li>
                                    <li>&nbsp; The database of mobile numbers can be used by Fossil in future for any lawful purpose as deemed fit by Fossil and Participants unequivocally agrees and consent for the same.</li>
                                    <li>&nbsp; Prizes under this Contest will be subject to Indian laws, including tax regulation, as and when applicable. All applicable taxes, gift tax, levies, charges or any tax applicable on the Prize under this Contest
                                        and TDS, if applicable on any prize, will have to be exclusively borne by the Winner.</li>
                                </ol>
                                <p>k. &nbsp;&nbsp;&nbsp; The terms of this rules and regulations shall be construed in accordance with the laws of India and the courts of Bangalore have sole and exclusive jurisdiction.</p>
                                <br><br>
                                <b id="offer2"><u>Terms and Conditions Shout and Earn - Referral Program</u></b>
                                <p>Welcome to XYZ. This page details a limited period Offer (defined below) made available on XYZ mobile application, m-site platform and desktop (shall collectively be referred as “<strong>Platform</strong>”). XYZ
                                    reserves the right to revoke or suspend this Offer at any point of time.</p>
                                <p>Please read these terms and conditions carefully before using the Platform (defined below). You should understand that by using the Platform, You agree to be bound by these terms and conditions including ‘Terms of Use’
                                    and ‘Privacy Policy’. Please understand that if You refuse to accept these terms and conditions, You will not be able to access the Offer provided under the Platform. These terms and conditions are in addition to the
                                    XYZ Terms of Use and Your usage of the Platform shall be construed as Your acceptance of these terms and conditions and You shall remain bound by the same. &nbsp;</p>
                                <p>This document is an electronic record in terms of Information Technology Act, 2000, and the Rules thereunder as applicable and the amended provisions pertaining to electronic records in various statutes as amended by the
                                    Information Technology Act, 2000. This electronic record does not require any physical or digital signatures. Wherever the context so requires “<strong>Participant/You/Your</strong>” shall mean the person who browses,
                                    uses, exchanges products and redeems XYZ Insider Points on the Platform.</p>
                                <p><strong><u>Referral Program:</u></strong></p>
                                <p>XYZ is offering the Participant with a unique link to encourage engagement on the Platform and to gain rewards (“<strong>XYZ Points</strong>”). Under this Program, a Participant can share an unique referral link inviting
                                    users to the Platform. In the event, a user visits the Platform using a unique referral link for the first time during the Offer Period (defined below) or a user signs up on the Platform using the unique referral link;
                                    both the Participant and the Invitee shall be rewarded Rs 50 worth XYZ Points (“<strong>Offer</strong>”). An “<strong>Invitee</strong>” under this Offer is defined as someone who hasn't visited the Platform during
                                    the Offer Period (defined below) using a referral link. This Offer is available from 20:00 hours on 19<sup>th</sup> December, 2018 till 23:59 hours of 25th December, 2018 (“<strong>Offer Period</strong>”). All XYZ
                                    Points earned during the Offer Period shall expire at the end date of the Offer Period.</p>
                                <p>Every Participant on the Platform will be allowed to send unique links to other users during the Offer Period. A Participant can send a unique link to multiple users and attain rewards.</p>
                                <p><strong>Other terms:</strong></p>
                                <ol>
                                    <li>XYZ, in its sole discretion, reserves the right to disqualify any Participant/ Invitee from the benefits of this Offer, if it identifies any fraudulent activity being carried out for the purpose of availing the benefits
                                        under the said Offer. The decision of XYZ in this regard shall be final and binding on the Participant/ Invitee.</li>
                                </ol>
                                <ol start="2">
                                    <li>Each XYZ Point earned by the Participant/ Invitee during the Offer Period shall be equivalent to INR 1 (Rupee One only). Such XYZ Points cannot be settled in lieu of cash. Any claim or request towards encashment
                                        of XYZ Points shall not be entertained in any manner whatsoever.</li>
                                </ol>
                                <ol start="3">
                                    <li>In addition to the XYZ Points mentioned above, each Participant whose unique referral link is used by:</li>
                                </ol>
                                <ul>
                                    <li>a minimum of 6 Invitees to visit the Platform during the Offer Period will get an Eazydiner Coupon for a maximum discount of Rs. 200 when the minimum amount payable to EazyDiner is Rs. 400 for a purchasing a prepaid
                                        deal on EazyDiner ("<strong>EazyDiner Coupon</strong>");</li>
                                    <li>a minimum of 11 Invitees to visit the Platform during the Offer Period will get an EazyDiner Coupon and an EaseMyTrip coupon for a maximum discount of Rs. 550 on flight booking charges when the minimum amount payable
                                        to EaseMyTrip for such flight bookings is Rs. 3,500 ("<strong>EaseMyTrip Coupon</strong>”); and</li>
                                    <li>a minimum of 16 Invitees to visit the Platform during the Offer Period will get an EazyDiner Coupon, an EaseMyTrip Coupon and a Lakme Salon coupon for a maximum discount of Rs. 500 on a minimum spends of Rs. 2,000 on
                                        one-time payment for the salon services at Lakme.</li>
                                </ul>
                                <ol start="4">
                                    <li>For availing any of the coupons mentioned above, the terms and conditions of the respective coupon available at <a href="http://www.XYZ.com/tac">XYZ.com/tac</a> will apply to the Participants in addition to these
                                        terms and conditions.</li>
                                </ol>
                                <ol start="5">
                                    <li>XYZ reserves the right, at its sole discretion to change, modify, add, delete or remove portions of the terms and conditions of the Offer, at any time without any prior written notice to the Participant/ Invitee.
                                        The decision of XYZ in this regard shall be final and binding on the Participant or Invitee.</li>
                                </ol>
                                <ol start="6">
                                    <li>This is a limited period offer.</li>
                                </ol>
                                <ol start="7">
                                    <li>This Offer is valid only in India. The minimum age of the Participant or Invitee shall be 18 (eighteen) years.</li>
                                </ol>
                                <ol start="8">
                                    <li>Further, this Offer is conducted on a best efforts basis and shall not, under any circumstances, be construed as any kind of guarantee or warranty or representations or covenants of any nature whatsoever.</li>
                                </ol>
                                <ol start="9">
                                    <li>During the Offer Period, XYZ Points can be redeemed by the Participant and/ or Invitee only by selecting the “Apply XYZ Points” on the payments’ checkout page at the time of purchasing products on the Platform.
                                        The redeemed XYZ Points will not be automatically applied at the time of checkout. XYZ shall not entertain any claim or request towards redemption of the XYZ Points in case the Participant/ Invitee does
                                        not select the “Apply XYZ Points” on the payments’ checkout page at the time of purchasing products on the Platform.</li>
                                </ol>
                                <ol start="10">
                                    <li>XYZ retains the right to share any and all data received from the Participant or Invitee under this Offer or anywhere captured on the Platform, with third parties for the sake of analysis and the Participant/ Invitee
                                        explicitly consents to the same.</li>
                                </ol>
                                <ol start="11">
                                    <li>A Participant/ Invitee can combine this Offer with other discounts available on the Platform during the Offer Period.</li>
                                </ol>
                                <ol start="12">
                                    <li>This Offer shall be subject to force majeure events and on occurrence of such an event, the Offer may be withdrawn at the discretion of XYZ with no prior intimation to the Participant/Invitees. The decision of XYZ
                                        in this regard shall be final and binding on the Participant and/or Invitee.</li>
                                </ol>
                                <ol start="13">
                                    <li>A Participant and a Invitee do not have to purchase on the Platform for XYZ Points to be reflected on their profile accounts.</li>
                                </ol>
                                <ol start="14">
                                    <li>XYZ reserves the right, in its sole discretion, to disqualify any Participant or Invitee that tampers or attempts to violate the Offer. The decision of XYZ in this regard shall be final and binding on the Participant
                                        or Invitee.</li>
                                </ol>
                                <ol start="15">
                                    <li>Participant or Invitee can only utilize XYZ Points to pay 10% of the transaction value on each order.</li>
                                </ol>
                                <ol start="16">
                                    <li>XYZ shall not be responsible towards any losses, including but not limited to any damages, claims costs, injury or any other liability in any form arising due to the participation by a Participant/ Invitee in the
                                        Offer.</li>
                                </ol>
                                <ol start="17">
                                    <li>Participant hereby agrees to indemnify and keep XYZ indemnified and harmless against all losses, claims, damages, liabilities, costs, expenses, claims, suits and proceedings (including attorney’s fee) that may be
                                        suffered by XYZ as a consequence of (i) violation of the terms and conditions of this Offer by Participant/ Invitee; (ii) violation of applicable laws; and (iii) any action or inaction resulting in willful misconduct
                                        or negligence on the part of the Participant/ Invitee.</li>
                                </ol>
                                <ol start="18">
                                    <li>Any dispute or claim relating to this Offer may be raised by the Participant or Invitee on or before 23:59 hours on 25<sup>th</sup> December 2018.</li>
                                </ol>
                                <ol start="19">
                                    <li>This Offer shall be governed in accordance with the applicable laws of India. Courts in Bangalore shall have the exclusive jurisdiction to settle any dispute that may arise under this Offer.</li>
                                </ol>
                                <ol start="20">
                                    <li>This Offer is not applicable for the Participants from the State of Tamil Nadu in view of the provisions of Tamil Nadu Prize Scheme (Prohibition) Act 1979.</li>
                                </ol>
                                <ol start="21">
                                    <li>A Participant or Invitee shall not be bound in any way to participate in this Offer. Any participation is voluntary in nature, and the Offer is being made purely on a best effort basis.&nbsp;</li>
                                </ol>
                                <p>The Participant/ Invitee undertake to irrevocably and unconditionally permit XYZ to post the Offer through various media including but not limited to newspapers, radio, television including news and other channels, internet,
                                    point of sale materials, etc., and the Participant/ Invitee shall not raise any kind of objection, protest or demur whatsoever to such coverage, marketing or activity that may be undertaken by XYZ in this regard.</p>
                                <br><br>
                                <b id="offer3"><u>Early Access - Terms &amp; Condtions</u></b>
                                <p>This document is an electronic record in terms of the Information Technology Act, 2000, and the Rules thereunder as applicable and the amended provisions pertaining to electronic records in various statutes as amended by
                                    the Information Technology Act, 2000. This electronic record does not require any physical or digital signatures. Wherever the context so requires “<strong>Participant</strong>” shall mean the person who pays INR 199
                                    (Rupees One hundred and ninety-nine only) (“<strong>Convenience Fee</strong>”) to transact at the End of Reason Sale December 2018 (“<strong>EORS Dec 2018</strong>”) prices on XYZ app, website or M-site (“<strong>Platform</strong>”)
                                    from (“<strong>Price Reveal Period</strong>”).</p>
                                <p>By participating in this Offer (defined below), the Participant agrees to be bound by these terms and conditions, terms and conditions of the EORS Dec 2018, the Terms of Use, Privacy Policy and other relevant documentation
                                    that are available on the&nbsp;Platform including any modifications, alterations or updates.</p>
                                <p>Before the start of the EORS Dec 2018, XYZ will be revealing all the EORS Dec 2018 prices on the Platform during the Price Reveal Period. XYZ is launching an offer whereby a customer can purchase products on the Platform
                                    during the Price Reveal Period at the EORS Dec 2018 prices by paying the Convenience Fee (“<strong>Offer</strong>”).</p>
                                <p>The Convenience Fee is non-refundable.</p>
                                <p><strong>Eligibility and Disqualification:</strong></p>
                                <br>
                                <li>To be eligible for the Offer, the Participant should:</li>
                                <ol>
                                    <li>be a permanent resident of India;</li>
                                    <li>above the age of 18 years as on 19<sup>th</sup> December, 2018;</li>
                                    <li>have shopped from the same user account;</li>
                                    <li>have placed a prepaid order; and</li>
                                    <li>paid the Convenience Fee.</li>
                                </ol>
                                <p><strong>Other terms and conditions:</strong></p>
                                <ol>
                                    <li>The Offer is available for prepaid transactions during the Price Reveal Period only.</li>
                                    <li>The Convenience Fee is non-refundable. In case, the customer cancels or returns one or more orders placed during the Price Reveal Period, the Convenience Fee will be forfeited by XYZ.</li>
                                    <li>The Convenience Fee allows the Participant to make a single transaction during the Price Reveal Period. A Participant shall have to pay the Convenience Fee for each transaction during the Price Reveal Period. For the
                                        avoidance of doubt, a Participant can purchase multiple products through one transaction.</li>
                                    <li>Participants are not bound in any way to participate in this Offer. Any participation is voluntary and the Offer is being made purely on a best effort basis.</li>
                                    <li>The Convenience Fee shall be refunded to the Participant solely in the event of a XYZ initiated cancellation of any of the products purchased under the transaction for which the Convenience Fee was paid.</li>
                                    <li>XYZ, in its sole discretion, reserves the right to disqualify the Participant from the benefits of this Offer, if any fraudulent activity is identified as being carried out for the purpose of availing the benefits
                                        under the said Offer.</li>
                                    <li>XYZ reserves the right, in its sole discretion, to disqualify any Participant that tampers or attempts to violate these Terms and Conditions.</li>
                                    <li>XYZ reserves the right, at their sole discretion, to change, modify, add or remove portions of this Terms and Conditions, at any time without any prior written notice to the Participant.</li>
                                    <li>The Participant acknowledges and confirms that the Participant is aware of the nature of telecommunications/ internet services and that an e-mail transmission may not be received properly and may be read or be known
                                        to any unauthorised persons. Participant agrees to assume and bear all the risks involved in respect of such errors and misunderstanding and XYZ and the Seller shall not be responsible in any manner.</li>
                                    <li>This Offer is valid only in India.</li>
                                    <li>Further, this Offer does not give any additional guarantee or warranty whatsoever.</li>
                                    <li>The Offer shall be subject to force majeure events and on occurrence of such an event, the Offer may be withdrawn at the discretion of XYZ.</li>
                                    <li>XYZ shall not be responsible for any loss, injury or any other liability arising due to participation by any person in the Offer.</li>
                                    <li>The Participant hereby agrees to indemnify and keep XYZ harmless against all damages, liabilities, costs, expenses, claims, suits and proceedings (including reasonable attorney’s fee) that may be suffered by XYZ
                                        as a consequence of (i) violation of terms of this Terms and Conditions by the Participant; (ii) violation of applicable laws; (iii) any action or inaction resulting in willful misconduct or negligence on the Participant’s
                                        part.</li>
                                </ol>
                                <p>This Terms and Conditions shall be governed in accordance with the applicable laws in India. Courts at Bangalore shall have the exclusive jurisdiction to settle any dispute that may arise under this Terms and Conditions.</p>
                                <br><br>
                                <b id="offer29"><u>Top Spender offer</u></b>
                                <br><br>Partner Gift Voucher Terms and Conditions
                                <br>This document is an electronic record in terms of Information Technology Act, 2000, and the Rules thereunder as applicable and the amended provisions pertaining to electronic records in various statutes as amended by
                                the Information Technology Act, 2000. This electronic record does not require any physical or digital signatures. Wherever the context so requires ?Participant/You/Your? shall mean the person who is an existing XYZ Insider
                                or enrolls to be a XYZ Insider and earns XYZ Insider Points by purchasing products from the XYZ app, website or M-site (?Platform?) starting from 00:00 hours on 19th December, 2018 till 23:59 hours on 25th December,
                                2018 (?Offer Period?).
                                <br>By participating in this Offer (defined below), the Participant agrees to be bound by these terms and conditions, terms and conditions of the EORS Dec 2018, the Terms of Use, Privacy Policy and other relevant documentation
                                that are available on the Platform including any modifications, alterations or updates.
                                <br>1. A Participant who shops products worth
                                <br>(i) Rs. 5,000 during the Offer Period and earns 500 XYZ Insider Points will be eligible to redeem 500 active XYZ Insider Points to get a gift voucher of Rs. 500 from Zoomcar;
                                <br>(ii) Rs. 10,000 during the Offer Period and earns 1000 XYZ Insider Points will be eligible to redeem 1000 active XYZ Insider Points to get a gift voucher of Rs. 500 from BookMyShow;
                                <br>(iii) Rs. 15,000 during the Offer Period and earns 1500 XYZ Insider Points will be eligible to redeem 1500 active XYZ Insider Points to get a gift voucher of Rs. 1,500 from Oyo Rooms; and
                                <br>(iv) Rs. 25,000 during the Offer Period and earns 2500 XYZ Insider Points will be eligible to redeem 2500 active XYZ Insider Points to get a gift voucher of Rs. 2000 from EaseMyTrip (?Offer?).
                                <br>2. Active XYZ Insider Points earned through purchase of products during the Offer Period only can be redeemed for the purposes of getting any of the above listed vouchers.
                                <br>3. For availing any of gift vouchers mentioned above, the Participant will be bound by the terms and conditions of the respective gift voucher and any other terms and conditions imposed by the respective partner (Zoomcar/BookMyShow/Oyo
                                Rooms/EaseMyTrip) will apply to the Participants in addition to these terms and conditions.
                                <br>4. XYZ, in its sole discretion, reserves the right to disqualify any Participant from the benefits of this Offer, if it identifies any fraudulent activity being carried out for the purpose of availing the benefits
                                under the said Offer. The decision of XYZ in this regard shall be final and binding on the Participant.
                                <br>5. XYZ reserves the right, at its sole discretion to change, modify, add, delete or remove portions of the terms and conditions of the Offer, at any time without any prior written notice to the Participant. The decision
                                of XYZ in this regard shall be final and binding on the Participant.
                                <br>6. This is a limited period offer.
                                <br>7. This Offer is valid only in India. The minimum age of the Participant shall be 18 (eighteen) years as on 19th December, 2018.
                                <br>8. Gift vouchers will be made live on the XYZ Insider Platform for You from 28th December 2018 till 31st March 2019 and they can be redeemed on a first come, first serve basis. Offer will be valid till stocks last.
                                <br>9. The validity of the gift vouchers will be as per the terms and conditions of the respective gift voucher.
                                <br>10. You can come to the XYZ Insider Platform to redeem the vouchers basis active Insider Points.
                                <br>11. XYZ Insider Points earned by shopping during EORS Dec 2018 will be in ?due status? till the end of ?return period? (i.e 30 days post delivery). You can either wait till the end of return period to redeem the
                                XYZ Insider Points against these gift vouchers or You can convert Due XYZ Insider Points into Active XYZ Insider Points by following the steps below to redeem the gift voucher(s) that You are eligible for:
                                <br>(i) Go to the XYZ Insider page from homepage top navigation or left menu option.
                                <br>(ii) Below Insider Points, there will be a communication that says ?XX Points on the way?
                                <br>(iii) Click on VIEW DETAILS to go to the points history page
                                <br>(iv) The points history page will have an UNLOCK POINTS button
                                <br>(v) Click on it to confirm that the products will no longer be returnable
                                <br>(vi) Upon confirmation, your XYZ Insider Points will be unlocked
                                <br>12. Further, this Offer is conducted on a best efforts basis and shall not, under any circumstances, be construed as any kind of guarantee or warranty or representations or covenants of any nature whatsoever.
                                <br>13. This Offer shall be subject to force majeure events and on occurrence of such an event, the Offer may be withdrawn at the discretion of XYZ with no prior intimation to the Participant. The decision of XYZ
                                in this regard shall be final and binding on the Participant.
                                <br>14. XYZ shall not be responsible towards any losses, including but not limited to any damages, claims costs, injury or any other liability in any form arising due to the participation by a Participant in the Offer.
                                <br>15. Participant hereby agrees to indemnify and keep XYZ indemnified and harmless against all losses, claims, damages, liabilities, costs, expenses, claims, suits and proceedings (including attorney?s fee) that may
                                be suffered by XYZ as a consequence of (i) violation of the terms and conditions of this Offer by Participant; (ii) violation of applicable laws; and (iii) any action or inaction resulting in willful misconduct or negligence
                                on the part of the Participant.
                                <br>16. Any dispute or claim relating to this Offer may be raised by the Participant or before 23:59 hours on 31st March 2019.
                                <br>17. This Offer shall be governed in accordance with the applicable laws of India. Courts in Bangalore shall have the exclusive jurisdiction to settle any dispute that may arise under this Offer.
                                <br>18. This Offer is not applicable for the Participants from the State of Tamil Nadu in view of the provisions of Tamil Nadu Prize Scheme (Prohibition) Act 1979.
                                <br>19. A Participant shall not be bound in any way to participate in this Offer. Any participation is voluntary in nature, and the Offer is being made purely on a best effort basis.
                                <br>20. The Participant undertake to irrevocably and unconditionally permit XYZ to post the Offer through various media including but not limited to newspapers, radio, television including news and other channels, internet,
                                point of sale materials, etc., and the Participant shall not raise any kind of objection, protest or demur whatsoever to such coverage, marketing or activity that may be undertaken by XYZ in this regard.
                                <br><br>
                                <b id="offer20"><u>MobiKwik cashback offer</u></b>
                                <ol>
                                    <li>Flat 10% cashback.</li>
                                    <li>Max cashback is capped at Rs.150</li>
                                    <li>Offer valid on a minimum transaction of Rs. 1000 </li>
                                    <li>Offer valid once between 2 - 7 November, 2018</li>
                                    <li>Cashback will be credited directly to the MobiKwik wallet balance, not to the SuperCash balance</li>
                                    <li>Cashback will be processed within 15 days from the date of the transaction (this will be manual; we'll get this processed on/before 12-Nov max) </li>
                                    <li>Your SuperCash balance can not be used between 2 - 7 Nov, 2018</li>
                                </ol>
                                <br><br>
                                <b id="offer21"><u>MobiKwik Supercash offer</u></b>
                                <br><br>
                                <b>Offer: Flat 10% SuperCash when you pay with MobiKwik on XYZ </b>
                                <br><br>
                                <br>Flat 10% SuperCash
                                <br>Max SuperCash is capped at Rs.250
                                <br>General Terms &amp; conditions: SuperCash will be cancelled in case of full refunds or cancellation of orders
                                <br>No minimum transaction or coupon code is required to avail the offer
                                <br>SuperCash will be auto credited to customer's MobiKwik wallet within 24 hours
                                <br>To check receipt of SuperCash, follow instructions on the video here: https://blog.mobikwik.com/index.php/kwik-help/
                                <br>If the order is cancelled or left undelivered at XYZ's end, XYZ (not MobiKwik) will initiate a refund
                                <br>XYZ shall be responsible and shall keep MobiKwik indemnified in the event of a customer grievance with respect to quality, quantity, merchantability of the goods/products purchased
                                <br>Offer is not applicable on premium delivery charges, gift wrap, premium delivery charges, gift cards, gold or precious jewellery
                                <br>SuperCash will be cancelled in case of full refunds or cancellation of orders
                                <br>In case of partial refunds/cancellations, refund amount will be adjusted with the SuperCash received on initial payment. Full SuperCash will be rolled back in case of a full refund
                                <br>After the refund is initiated, amount will be credited back to wallet within 7-10 days
                                <br>Cashbacks offered to eligible users will be in the form of Mobikwik SuperCash
                                <br>For FAQs on terms, users can refer to: https://promotions.mobikwik.com/inapp/faq/supercash
                                <br>Both MobiKwik &amp; XYZ reserve the right to discontinue the offer without any prior notice
                                <br>
                                <br>
                                <br><br>
                                <b id="offer33"><u>Yes Bank- RTF Jan'19  - T&amp;C</u></b> This document is an electronic record in terms of Information Technology Act, 2000, and the Rules thereunder as applicable and the amended provisions pertaining
                                to electronic records in various statutes as amended by the Information Technology Act, 2000. This electronic record does not require any physical or digital signatures.
                                <br><br> Wherever the context so requires ?Customer? shall mean the person who owns a valid YES BANK Retail debit or credit card (?YES BANK Card?) and purchases products which are available at www.XYZ.com including its
                                app or M-site (?Platform?) which is owned and operated by XYZ Designs Private Limited ("XYZ?) during the Offer Period.
                                <br><br> YES Bank Limited (?YES BANK?) is launching an offer which is valid from 00:00 hours on 18th January 2019 to 23:59 hours on 22nd January 2019 ("Offer Period?). Under this offer, the YES BANK Card members will get
                                an instant discount of 15% on the product price (including taxes) purchased from the sellers on the Platform if they pay for the products using the YES BANK Card. To be eligible for this discount, the minimum amount paid
                                through the YES BANK Card should be INR 2,500 (Rupees Two Thousand and Five Hundred only). The maximum discount that the customer shall be eligible for is INR 1,500 (Rupees One Thousand Five Hundred only) during the Offer
                                Period ("Offer?).
                                <br><br> By participating in this Offer, the Customer agrees to be bound by these terms and conditions, terms and conditions of the Right to Fashion Sale January 2019 (?RTF Sale?), the Terms of Use, Privacy Policy and other
                                relevant documentation that are available on the Platform including any modifications, alterations or updates.
                                <br><br> 1.This Offer does not require any coupon code and shall not be valid on Corporate Commercial Credit cards, Virtual Debit Cards, and Netsafe Cards, Netbanking. Delinquent and over-limit credit card members will
                                not qualify for this Offer. Further, discounts shall not be applicable for Card-on-delivery transactions, on Gift-Wrap premium delivery charges or for YES BANK NRI customers.
                                <br> 2.Nothing herein amounts to a commitment by YES BANK to conduct further, similar or other offers. This Offer is non-encashable, not extendable and non-negotiable.
                                <br>
                                <p><strong>Other terms and conditions:</strong></p>
                                <br><br> 1.To avail this Offer, purchase should be charged in full to the YES BANK Card. In case a customer makes partial payments through Gift Card/Wallet, the Offer will apply only on the net amount paid through the YES
                                BANK Card towards the purchase during the Offer Period.
                                <br> 2.Any person availing this Offer shall be deemed to have accepted these Terms and Conditions.
                                <br> 3.Exchange/partial cancellation and returns would also qualify for discount amount if net order value is greater than or equal to Rs 2,500.
                                <br> 4.In the event an order is cancelled in part or full, the corresponding value will be deducted from the paid amount while checking eligibility of discount.
                                <br> 5.Under no circumstances will the discount being offered under this Offer be settled with cash in lieu by XYZ or YES BANK.
                                <br> 6.This Offer is being made purely on a ?best effort? basis. YES BANK Card members are not bound in any manner to participate in this Offer and such participation is purely voluntary.
                                <br> 7.If a YES BANK Card member has more than 1 (one) YES BANK Card, spends on the YES BANK Cards cannot be clubbed by the YES BANK Card member to qualify for the Offer. Where a YES BANK Card ember holds more than 1(one)
                                card, the YES BANK Card member should call up the customer call centre and confirm if the said Offer can be availed.
                                <br> 8.YES BANK and XYZ reserve absolute right to withdraw and/or alter any terms of the terms and conditions of the Offer at any time without prior notice.
                                <br> 9.This Offer cannot be clubbed with any other offer from the Platform.
                                <br> 10.All queries regarding the Offer will be entertained till 60 days from Offer fulfilment date. Post such date; YES BANK will not entertain any correspondence or communication regarding this Offer from the YES BANK
                                Card member.
                                <br> 11.XYZ does not endorse any of the products or brands being offered under the Offer and will not accept any liability pertaining to the quality, merchantability, fitness, delivery or after sales service of such
                                products which shall be at the sole liability to the respective sellers of the products on the Platform.
                                <br> 12.XYZ and/or YES BANK reserves the right to disqualify respective YES BANK Card member(s) from the benefits of the Offer, if any fraudulent activity is identified as being carried out for the purpose of availing
                                the benefits under the said Offer or otherwise by use of the YES BANK Card.
                                <br> 13.YES BANK or XYZ provides no assurance on the products and/ or services offered on the Platform.
                                <br> 14.XYZ shall not be liable for any failure by YES BANK to honour the transaction and for any technical issues as a result of which the transaction made by the YES BANK Card member was not successful.
                                <br> 15.This Offer shall be subject to all applicable laws, rules and regulations which are in existence and which may be promulgated anytime by any statutory authority.
                                <br> 16.All government levies like Sales Tax, TDS, any Local Tax, Octroi etc., shall be payable by the YES BANK Card member as applicable at the time of the Offer Period.
                                <br> 17.This Offer shall be governed in accordance with the applicable laws in India. Courts at Mumbai shall have the exclusive jurisdiction to settle any dispute that may arise under this Offer.
                                <br> 18.XYZ and/or YES BANK reserve the right, at any time, without prior notice and without assigning any reason whatsoever, to add/alter/modify/change or vary all of these terms and conditions or to replace, wholly
                                or in part, this Offer by another offer, whether similar to this Offer or not, or to extend or withdraw it altogether. The terms of the above schemes shall be in addition to and not in derogation of the terms contained
                                in the YES BANK Card member agreement.
                                <br> 19.XYZ or the sellers shall not be liable for any failure by YES BANK to honour the transaction and for any technical issues as a result of which the transaction made by the YES BANK Card member was not successful.
                                <br> 20.The YES BANK Card member shall indemnify and hold XYZ and the sellers on the Platform harmless against all damages, liabilities, costs, expenses, claims, suits and proceedings (including reasonable attorney?s
                                fee) that may be suffered by such sellers as a consequence of (i) violation of these terms and conditions, of the terms of user agreement, privacy policy (subject to change) as mentioned on the Platform; (ii) violation
                                of applicable laws; and (iii) any action or inaction resulting in wilful misconduct or negligence on the part of the YES BANK Card member.
                                <br> 21.This Offer is valid only in India. The minimum age of such YES BANK Card member shall be 18 years as of 18th January 2019 and the YES BANK Card member must be an Indian resident and citizen.
                                <br><br>
                                <b id="offer23"><u>Airtel Bank Offer</u></b>
                                <br><br> Offer: Flat 200 Cashback via Airtel Money/Airtel payments Bank on XYZ, min trasanction of Rs. 2000. Applicable for first time users of Airtel Payments Bank/Airtel Money Wallet on XYZ
                                <br> <br>1. The offer is available only for Airtel Payments Bank (APBL) customers : a) wallet customers, b) savings account customers
                                <br>2. Maximum cashback amount per transaction will be Rs 200
                                <br>3. Limited period offer, offer duration : from 1st Mar 2019 till 31st Mar 2019
                                <br>4. Offer is applicable only for first time users of Airtel Payments Bank/Airtel Money Wallet on XYZ
                                <br>5. Minimum purchase of Rs. 2000 mandatory for the customer to be eligible for the offer
                                <br>6. Cashback would be credited within 72 business hours in the user's Airtel Payments Bank wallet or savings account.
                                <br>7. Airtel Payments Bank reserves the right to disqualify any Airtel Payments Bank wallet holder(s) or Airtel Payments Bank savings account holder(s) from the benefits of this offer in case of any fraudulent activity/suspicious
                                transactions.
                                <br>8. Airtel Payments Bank also reserves the right to discontinue this offer without assigning any reasons or without any prior intimation whatsoever.
                                <br>9. In addition to the above, this offer is also subject to Airtel Payments Bank's General Terms &amp; Conditions available on http://www.airtel.in/personal/money/terms-of-use.
                                <br>10. APBL does not warrant or guarantee availability of inventory on XYZ's platform.
                                <br>11. For further details, please send your queries to wecare@airtelbank.com
                                <br>12. NO KYC Wallets are not eligible for this cashback(wallet to be upgraded to a MINKYC with minimum ID &amp; Add verification docs)
                                <br>13. Not applicable on mastercard transactions
                                <br>14. Offer is not valid for Bharti Airtel &amp; Airtel Payments Bank Employees
                                <br>
                                <br>
                                <b id="offer24"><u>ICICI Wednesday Offer</u></b>
                                <br><br>
                                <br>
                                <b>Definitions</b>
                                <ol class="bullet">
                                    <li>“Alliance Partner” shall mean ” XYZ Designs Pvt. Ltd. ” referred as www.XYZ.com, who has entered into an alliance agreement with ICICI Bank for purpose of providing the Offer.</li>
                                    <li>“ICICI Bank Net Banking User” shall mean a person who holds an ICICI Bank savings account, credit card account(s), loan / facility account(s), depository account(s) and/or any other type of account (s), so maintained
                                        with ICICI Bank or its Affiliate which are eligible account(s) for purposes of ICICI Bank Net Banking Service(s) and who has received communication from ICICI Bank with respect to the Offer.</li>
                                    <li>“ICICI Bank Net Banking Service(s) “shall mean internet banking services of ICICI Bank offered to ICICI Bank Net Banking User.</li>
                                    <li>“Offer” shall mean such discount(s) /benefit(s) provided by ICICI bank on purchase of Products/Services using ICICI Bank Net Banking Services and ICICI Bank Credit card, Debit card and Prepaid card, during the Offer
                                        Period on the Website.</li>
                                    <li>“Offer Period” shall mean the period commencing from July 1st, 2017 and ends 31st Aug, 2018 both days inclusive.
                                    </li>
                                    <li>"Primary Terms and Conditions" shall mean the terms and conditions applicable to e ICICI Bank’s internet banking facility/service.</li>
                                    <li>“Products/Services” shall mean the goods/benefits/facilities offered by the Alliance Partner.</li>
                                    <li>“Website” shall mean the following website of the Alliance Partner: www.XYZ.com.</li>
                                    <li>“Void Transaction” shall mean any transaction wherein the transaction has taken place but has been cancelled /rejected /unsuccessful by the ICICI Bank.</li>
                                </ol>
                                <br>
                                <b>All capitalized terms used but not defined herein shall have the respective meanings prescribed to them in the Primary Terms and Conditions. These terms (“Terms”) shall be in addition to and not  in derogation of the Primary Terms and Conditions. To the extent of any inconsistency between these Terms and Primary Terms and Conditions, these Terms shall prevail.</b>
                                <br>
                                <b>Offer</b>
                                <ol class="bullet">
                                    <li>To avail benefits under the Offer, the ICICI Bank Cardholders (ONLY DEBIT &amp; CREDIT and Pockets Prepaid Cards) shall be required to make purchase on the Website/Mobile Application during the Offer Period starting
                                        from 1st July, 2017 and ends 31st Oct, 2018, both days inclusive (On every Wednesday in this period only).
                                    </li>
                                    <li>The Offer is as mentioned: 10% Additional discount on ICICI Bank debit, credit cards &amp; Pockets Prepaid Cards on Every Wednesday (from 1st July, 2017 and ends 31st Oct, 2018, both days inclusive)
                                    </li>
                                    <li>Eligibility Criteria –</li>
                                    <li>Min Transaction Value – Rs.3000/-</li>
                                    <li>Max Cashback Value – Rs.500 per card per month</li>
                                </ol>
                                <br>
                                <b>Redemption Process</b>
                                <ol class="bullet">
                                    <li>Please log on website www.XYZ.com</li>
                                    <li>Transact on Wednesday ONLY using Credit Card, Debit Card and Pockets </li>
                                    <li>The Offer is valid only for select set of ICICI Bank Pocket User(s) and ICICI Bank Cardholders selected at the sole discretion of ICICI Bank and who have received communication about the Offer from ICICI Bank, during
                                        the Offer Period.</li>
                                    <li>Participation in the Offer by the ICICI Bank Net Banking User/s/Cardholders is on a voluntary basis.</li>
                                    <li>The ICICI Bank Net Banking User/s/Cardholders may avail the Offer during the Offer Period or until the stock lasts, whichever is earlier.</li>
                                    <li>The Offer is non-transferable, non-binding and non-encashable.</li>
                                    <li>The Offer is not valid for Void Transactions</li>
                                    <li>The Offer is complete funded by ICICI Bank.</li>
                                </ol>
                                <br>
                                <b>TERMS  PRESCRIBED  BY  THE  ALLIANCE  PARTNER</b>
                                <ol class="bullet">
                                    <li>10% Additional discount on ICICI Bank Debit/Credit cards and Pockets Prepaid cards</li>
                                    <li>Offer valid only on transactions of Rs.3000 &amp; Above</li>
                                    <li>Max discount per Card/per month: Rs.500</li>
                                    <li>Multiple transactions allowed but maximum discount cannot exceed Rs.500 (cumulative for a calendar month)</li>
                                    <li>Offer Not applicable on Corporate &amp; Commercial Credit Cards</li>
                                    <li>This offer is not valid on net banking transactions.</li>
                                    <li>Exchange/Partial cancellation and returns would also qualify for discount amount if net order value is still greater than or equal to Rs.3000</li>
                                    <li>All/any orders fully cancelled would not be eligible for discount</li>
                                    <li>All disputes and escalations shall be addressed within 10 working days from the date of complaint registration</li>
                                </ol>
                                <b>Disclaimers: XYZ.com is not responsible for any typographical error leading to an invalid coupon</b>
                                <br>
                                <b>General Terms</b>
                                <ol class="bullet">
                                    <li> Alliance Partner does not guarantee and make any representation about the usefulness, worthiness and/or character of the discount / benefit or of the Products/Services under the Offer provided by ICICI Bank. ”) </li>
                                    <li>Any tax or other liabilities or charges payable to the government or any other statutory authority/body or any participating establishment, which may arise or accrue to the ICICI Bank Net Banking User/s due to provision
                                        of the Offer, shall be to the sole account of the ICICI Bank Net Banking User/s. Tax deducted at source, if any, on the monetary value of the Offer shall be payable by the ICICI Bank Net Banking User/s.</li>
                                    <li>All issues / queries / complaints / grievances relating to the Offer, if any, shall be addressed to Alliance Partner directly at +91-80-61561999 without any reference to ICICI Bank. The same shall be addressed by Alliance
                                        Partner, only up to a period of 5 days after the date of transaction/purchase.</li>
                                    <li>The existence of a dispute, if any, regarding the Offer shall not constitute a claim against ICICI Bank and shall be addressed directly by the Alliance Partner.</li>
                                    <li>The Offer is not available wherever prohibited and / or on products / services for which such offers cannot be made available for any reason whatsoever.</li>
                                    <li> ICICI Bank/Alliance Partner reserves the right to modify/ change all or any of the terms applicable to the Offer without assigning any reasons or without any prior intimation whatsoever. ICICI Bank also reserves the
                                        right to discontinue the Offer without assigning any reasons or without any prior intimation whatsoever.</li>
                                    <li> If the ICICI Bank Net Banking User ceases to be a ICICI Bank Net Banking User at any time during the currency of the Offer Period, all the benefits under the Offer shall lapse and shall not be available to the ICICI
                                        Bank Net Banking User.</li>
                                </ol>
                                <br>
                                <b>Terms and Conditions</b>
                                <b>In addition to the terms and conditions of such partners offering benefits to ICICI Bank credit/debit cardholders, the following terms are applicable to the Offer.</b>
                                <ol class="bullet">
                                    <li>ICICI Bank holds out no warranty or makes no representation about the quality, delivery or otherwise of the goods and services or the assured vouchers offered by the Alliance Partner on www.XYZ.com. Any dispute or
                                        claim regarding the goods, services and assured vouchers must be resolved by the Card Holder/s with the Alliance Partner directly without any reference to ICICI Bank.</li>
                                    <li>ICICI Bank shall not be liable in any manner whatsoever for any loss/ damage/ claim that may arise out of use or otherwise of any goods/ services / assured gifts / prizes availed by the Card Holder/s under the Program
                                        offered by the Alliance Partner.</li>
                                    <li>ICICI Bank reserves the right to disqualify the Alliance Partner or Card Holder/s from the benefits of the Program if any fraudulent activity is identified as being carried out for the purpose of availing the benefits
                                        under the Program or otherwise by use of the Card.</li>
                                    <li>The assured vouchers are sponsored by the Alliance Partner and the Card Holder/s shall be bound by the terms and conditions stipulated by the Alliance Partner in this regard.</li>
                                    <li>ICICI Bank shall not be held liable for any delay or loss that may be caused in delivery of the goods and services or the assured vouchers.</li>
                                    <li>The Program is not available wherever prohibited and / or on merchandise / products / services for which such programs cannot be offered for any reason whatsoever.</li>
                                </ol>
                                <br><br>
                                <b id="offer25"><u>Express Delivery store</u></b>
                                <br><br>
                                <br>All the time and hours mentioned are business hours according to XYZ
                                <br>Working days/hours or business days/hours according to XYZ is Monday 00:00 hours to Friday 23:59:59 hours
                                <br>Orders must be placed within the time-frame mentioned on the express store page to avail the express delivery option
                                <br>The full order (inclusive all items) should be from the express store to be eligible for express delivery. If other items that are not present in the expressstore are added to the order, it will not be eligible for
                                express delivery
                                <br>Only certain pincodes are eligible for the express delivery option
                                <br>On Weekends and other regional holidays, deliveries are not guaranteed to establishments like offices that might be closed
                                <br>Express delivery is subject to customer availability at the address and the delivery option applies to attempted delivery by the promised date
                                <br><br>
                                <b id="offer26"><u>Terms &amp; Condtions for XYZ Coupons</u></b>
                                <ol>
                                    <li>These promotions are time bound and details regarding start and end time of the promotion will be available on the App and desktop banner.</li>
                                    <li>The offer might get communicated to customers via notification or e-mailers or sms which will land users on the offer page.</li>
                                    <li>The offer will be available for select set of styles as agreed by our participating sellers. App and desktop banners will link to list of styles where offer is running.</li>
                                    <li>As a part of offer there will be extra discounts provided on the selected catalogue applicable for this offer. These discounts will be rendered through coupons as communicated in the offer.</li>
                                    <li>Coupon discount is over and above the discounts running on products and is calculated on post discounted price.</li>
                                    <li>As a part of the construct, some coupons get applicable only if the total value of post discounted price of all the products in the shopping bag exceeds a minimum threshold as mention in "Tap for best price" widget
                                        on product page or "Apply Coupon" page within Shopping Bag.</li>
                                    <li>Maximum discount, ie, product discount and coupon discount, that can be availed on a product are subjected to product level caps enforced by the seller.</li>
                                </ol>
                                <br><br>
                                <b id="offer27"><u>XYZ Points</u></b>
                                <br><br>
                                <b>XYZ Points Special Offer</b>
                                <ol>
                                    <li>Welcome to XYZ. This page details a limited period offer made available on XYZ marketplace platform (XYZ) and may be revoked or suspended by XYZ any time. These terms and conditions are in addition to the
                                        XYZ Terms of Use to which you agree and are bound by using XYZ.</li>
                                </ol>
                                <b>Usage conditions</b>
                                <ol>
                                    <li>400 XYZ Points (worth Rs. 200) added to select customers' account as a one-time acquisition/retention offer.&nbsp;</li>
                                    <li>1 XYZ Points = Re. 1</li>
                                    <li>During this period, a user can redeem these points over multiple orders. These points can be applied on all styles listed on the platform.&nbsp;</li>
                                    <li>XYZ Points can be redeemed by selecting the 'Apply XYZ Points'apos; option on the payments page.</li>
                                    <li>Usage of XYZ Points is restricted to a maximum of 10% of total order amount (inclusive of Shipping Charge and VAT)</li>
                                </ol>
                                <br>
                                <b>General Terms &amp; Conditions :</b>
                                <ol>
                                    <li>Customers have been auto allotted slot timings for this offer. The offer and slot timings are communicated to the customers via SMS, email and notifications. It can also be found in the account during the slot timings.
                                        </li>
                                    <li>This offer shall be valid on successful and fulfilled transactions. For the purpose of this sale a fulfilled transaction shall mean an order placed by the customers on the platform which shall be confirmed and received
                                        by them on their registered address.</li>
                                    <li>All customers shall ensure that the mobile number(s), e-mail address and/or other details provided by them to XYZ are true, accurate and in use, at the time of the online retail transaction. Any liability, consequence
                                        or claim arising on account of any incorrect or outdated information provided by the customer to, same shall solely be borne by the affected customer. XYZ shall not be liable to verify the accuracy and/or correctness
                                        of the information so provided by the customer.</li>
                                    <li>Customers are advised to check the serviceability with the area pin code before placing any order with XYZ.</li>
                                    <li>Extra VAT may apply in addition to the unit sale price on certain products, but the final inclusive price will not exceed the M.R.P. The customer shall be liable to pay the cost.</li>
                                    <li>The images of the products shown are for visual representation only and may vary from the actual product.</li>
                                    <li>Customers are responsible for maintaining the confidentiality of their mobile phones, e-mail accounts and passwords.</li>
                                    <li>The prices of the product may vary during and after the offer is over. Thereby XYZ shall not be held liable for such difference in the price of the product as they are offered by the participating seller/s.</li>
                                    <li>Product warranty shall be regulated by the terms and conditions mentioned on the warranty card of respective products and XYZ shall not be liable or responsible to the customer in for any indirect or consequential
                                        loss or damage.</li>
                                    <li>The balance amount, after the offer has been availed, will have to be paid by the customer at the time of purchase.</li>
                                    <li>XYZ Return &amp; Exchange Policy offers you the option to return or exchange items purchased on XYZ Mobile App within thirty (30) days of the receipt. In case of return of the purchased item, please refer to the
                                        Return Policy on our website or call +91-80-61561999.</li>
                                    <li>Orders may be split into separate shipments depending upon the products ordered for, despite the customer having made one single consolidated payment against one order ID. Separate shipments, after being split, shall
                                        have separate expected delivery period.</li>
                                    <li>By participating in this This offer, all eligible customer agree to be bound by these Terms and Conditions, the Terms of Use, Privacy Policy and other relevant documentation that are available on XYZ including any
                                        modifications, alterations or updates that may be made either by XYZ or the participating seller/s. XYZ shall not be liable or responsible to the customer in for any indirect or consequential loss or damage.</li>
                                    <li>XYZ reserves the right, in its sole discretion, to disqualify any Customer that tampers or attempts to tamper with the deals/ offers or violates these Terms and Conditions or acts in a disruptive manner.</li>
                                    <li>If XYZ has suspicion or knowledge, that any Customers has been involved in any fraudulent or illegal activity directly or indirectly and/or is using the platform to order products not for his/her direct usage during
                                        or after the This offer, then XYZ reserves the right to disqualify that Customers and any related Customers.</li>
                                    <li>XYZ shall not be responsible if some purchase is not registered or is lost due to any network problems such as breakdown of machinery, unclear/ disruption in the network or non-receipt of payment from banks/payment
                                        gateways and/or the cost(s) charged by the network operator(s). Any dispute in connection to the same shall be settled between the end customer and the network operator without involving XYZ.</li>
                                    <li>XYZ shall not be liable for any loss or damage due to Act of God, Governmental actions, force majeure circumstances, or any other reason beyond its control, and shall not be liable to pay any monetary compensation
                                        or otherwise for the same.</li>
                                    <li>XYZ does not hereby warrant that the XYZ Call Center will run concurrently and error free during and/or after this offer and XYZ shall not be directly liable for issues related to technical and/or human error
                                        whatsoever. However, XYZ shall work towards the best interest of the Customer.</li>
                                    <li>This offer is subject to the laws of India and all disputes arising hereunder shall be subject to the jurisdiction of courts, tribunals or any other applicable forum at Bengaluru.</li>
                                </ol>
                                <br><br>
                                <b id="offer28"><u>XYZ Credit - T&amp;C</u></b>
                                <br><br>This document is an electronic record in terms of Information Technology Act, 2000 and published in accordance with the provisions of Rule 3) of the Information Technology (Intermediaries guidelines) Rules, 2011
                                that require publishing the rules and regulations, privacy policy and Terms of Use for access or usage of the platform - www.XYZ.com (hereinafter referred to as Platform)
                                <br><br> Your use of the Platform and services and tools are governed by the following terms and conditions (Terms and Condition) as applicable, including the applicable policies including the privacy policy and Terms of
                                Use available on the Platform. By mere use of the Platform, You shall be contracting with XYZ Designs Private Limited (hereinafter referred to as "Platform),the owner of the Platform. These terms and conditions including
                                the policies constitute your binding obligations, with XYZ.
                                <br><br> For these Terms of Use, wherever the context so requires You or User shall mean any natural or legal person who has agreed to become a buyer on Platform by providing data while registering on the Platform as Registered
                                User. The term XYZ,We,Us,Our shall mean XYZ and its affiliates.
                                <ol>
                                    <li>You may avail the services of the XYZ Credit upon successful registration and creation of an Account on the XYZ App or Website. XYZ Credit is powered by underlying pre-paid payment instrument (PPI) Qwikcilver
                                        Gift Cards. You are bound by the terms and conditions of XYZ Gift cards shared at https://www.XYZ.com/termsofuse-giftcard</li>
                                    <li>The currency of issue of the XYZ Credit shall be Indian Rupees (INR).</li>
                                    <li>You can reload the XYZ Credit by using only those modes of payment as enabled by Us. <br><br> (i) XYZ Credit can be topped up by taking refunds for items returned which were paid via Cash on Delivery (COD) option
                                        (ii) By topping up XYZ Credit via the purchase of a Gift Card with Online Payment Instruments i.e. Net Banking, Debit Cards, Credit Cards, Wallets e.g. PhonePe, Mobiqwik (iii) Adding XYZ Gift Cards to XYZ
                                        Credit account. <br><br> These payment modes may be revised by Us from time to time at Our sole discretion.
                                    </li>
                                    <li>When You opt to load money into XYZ Credit, You will be required to provide certain information like email ID, phone number, address etc ("Payment Details") for the purpose of reloading. These Payment DEtails may
                                        be passed to the Gift Card partner towards Your KYC. You acknowledge and agree that by providing such Payment Details, You authorise Us to reload Your XYZ Credit. The Payment Details You provide must be true,
                                        complete, current and accurate. We do not bear any liability for the consequences if You provide false, incomplete or incorrect Payment Details. </li>
                                    <li>You represent and warrant that You are legally and fully entitled to use any debit card, credit card, bank or any other mode that You may use to load funds into Your XYZ Credit. We shall not be held responsible in
                                        case.You dont have sufficient funds or authorised access to use such payment modes to load Your XYZ Credit.</li>
                                    <li>The limits of XYZ Credit Top-up through the each gift card shall not exceed Rs.10,000/-. You shall provide the minimum details as required by Us. The amount outstanding at any point of time in Your XYZ Credit
                                        does not exceed Rs 50,000/-</li>
                                    <li>You hereby authorize Us to hold, receive and disburse funds in Your XYZ Credit in accordance with any payment instructions We receive from You.</li>
                                    <li>XYZ Credit may not be applicable to purchase of all the items, including Gift Cards, available on the XYZ platform and may be limited only to items sold by specific sellers.</li>
                                    <li>All Transactions shall be subject to the following:<br><br> a. The amount of the outstanding balance existing in the XYZ Credit at the time of initiating a Transaction, (ii) the RBI Regulations, and/or (iii) any
                                        other limits/ conditions as may prescribed by Us from time to time. <br><br> b. XYZ Credit shall be available only on the following operating systems: Android, IOS, XYZ Websites. <br><br> c. Transfer of funds
                                        from Your XYZ Credit to another users XYZ Credit shall not be permitted. <br><br>
                                    </li>
                                    <li>Your XYZ Credit shall be valid and operational for a period of 1 (one) year from the date of activation/issuance to You, until suspended/ terminated by XYZ in accordance with these User Terms, unless the validity
                                        of the XYZ Credit is extended by Us at Our discretion.</li>
                                    <li>The maximum permissible period of non-use of the XYZ Credit (Permissible Dormant Period) shall be 1 (one) year either from the date of issuance or from the date of the last use of the XYZ Credit, whichever is
                                        later. You agree that any outstanding balance in Your XYZ Credit shall be forfeited at the time of expiry of the XYZ Credit or at the time of suspension/termination of Your XYZ Credit for any reason whatsoever.
                                        Information about the forfeiture of Your XYZ Credit balance (due to the impending expiry of Your XYZ Credit) shall be intimated to You at reasonable intervals, during the 45 (forty five) days period prior
                                        to the expiry of Your XYZ Credit. The intimation shall be sent to Your registered e-mail address or phone number shared as Payment Details. We shall not be held responsible or liable for any failure of delivery
                                        of the intimation to You.</li>
                                    <li>You expressly agree and acknowledge that You shall have no claims against Us for the balance forfeited from Your XYZ Credit.</li>
                                    <li>Any and all claims shall be governed by the laws of India and the courts at Bangalore shall have sole jurisdiction.</li>
                                </ol>
                                <br><br>
                                <b id="offer32"><u>Freecharge Offer - T&amp;C</u></b>
                                <br><br>This document is an electronic record in terms of the Information Technology Act, 2000, and the Rules thereunder as applicable and the amended provisions pertaining to electronic records in various statutes as amended
                                by the Information Technology Act, 2000. This electronic record does not require any physical or digital signatures. Wherever the context so requires ?Participant? shall mean a new or an existing customer of XYZ, having
                                an account or opens an account with Freecharge/ has a Freecharge wallet facility, and undertakes a successful transaction on the ?Platform? (defined below) during 00:00 hours of 1st January 2019 till 23:59 hours of 28th
                                February 2019 using ?Freecharge? application as its payment option for the first time on the Platform.
                                <br><br> By participating in this Offer (defined below), the Participant agrees to be bound by these terms and conditions, terms and conditions of the Freecharge, the Terms of Use, Privacy Policy and other relevant documentation
                                that are available on the XYZ website at www.XYZ.com, mobile app and mobile site (?Platform?) including any modifications, revisions, alterations or updates.
                                <br><br>
                                <strong>Offer</strong>:- Freecharge is launching an offer on the Platform. If a Participant pays for the products purchased on the Platform using Freecharge application/ wallet, the Participant shall be eligible to receive
                                instant 10% cashback on their Freecharge account upto Rs. 100, whichever is lower, on the transaction value paid for purchase of such products on the Platform by the Participant (hereinafter referred to as the ?Offer?).
                                The Offer is valid for orders made on the Platform from 00:00 hours on 1st January 2019 to 23:59 hours on 28th February 2019 (?Offer Period?). The Offer is valid only once per Participant.
                                <br><br>
                                <strong>Unique Coupon Code</strong>:- Participants who have successfully participated in the Offer on the Platform, shall also be eligible for receiving additional 100% cashback, upto Rs. 25, in the form of coupon code
                                (?Unique Code?) for using such cashback for recharges and bill payments transacted through Freecharge application/ platform/ wallet. The Unique Code is valid only once per Participant. The Unique Code shall be valid for
                                a period of 1095 days upon receipt by the Participant.
                                <p><strong>Other terms and conditions of the offer :</strong></p>
                                <ol>
                                    <li>Participants are not bound in any way to participate in this Offer. Any participation is voluntary and the Offer is being made purely on a best effort basis.</li>
                                    <li>To avail this Offer, the Participant needs to undertake a successful transaction in the Platform.</li>
                                    <li>This Offer shall not be settled with cash in lieu by XYZ or Freecharge.</li>
                                    <li>The Offer is applicable on purchases made on the Platform via XYZ App/Website and mWeb.</li>
                                    <li>The Participant shall be eligible for the Offer only upon conducting the entire transaction on the Platform using Freecharge. Partial payment for such transaction by the Participant through Freecharge is not allowed
                                        and the participant shall not be eligible to avail the Offer and/or the cashback through Unique Code.</li>
                                    <li>As the Participant is eligible for instant cashback on the Offer, in case of either full or partial order cancellation/ return, Freecharge shall deduct the entire cashback received by the Participant while processing
                                        refund amount to the Participant. The final refund amount shall be processed by Freecharge within 7 to 10 working days.</li>
                                    <li>The Participant can participate in the Offer in the following manner:<br><br> a)Go to the Platform (XYZ App/Website and mWeb) and select the product Participant wishes to buy.<br><br>b)Hit 'Pay with Freecharge Wallet'
                                        option at the time of paying for the products on the Platform.<br><br> c)Instant 10% cashback in Participant?s FreeCharge wallet upon successful transaction on the Platform.
                                    </li>
                                    <li>The Unique Code (as defined above) will be sent to the Participant via an e-mail to the Participant?s registered ID on the XYZ app within 72 hours of successful transaction on the Platform as per the terms and conditions
                                        of the Offer. The Unique Code received by the Participant can be availed upon a minimum transaction of Rs. 25.</li>
                                    <li>The Participant acknowledges and confirms that the Participant is aware of the nature of telecommunications/ internet services and that an e-mail may not be received properly and may be read or be known to any unauthorised
                                        persons. The Participant agrees to assume and bear all the risks involved in respect of such errors and misunderstanding and XYZ and Freecharge shall not be responsible in any manner.</li>
                                    <li>Unique Code has to be applied to receive the cashback by the Participant. The Participant can redeem the Unique Code in the following manner:<br><br>a)Participant to login to its Freecharge app/website and mWeb<br><br>                                        b)Participant to select recharge and bill payments.<br><br> c)Participant to enter the Unique Code received via email to avail cashback offer
                                    </li>
                                    <li>The Participant can use the Unique Code for subsequent transactions on Freecharge</li>
                                    <li>The cashback received on the Unique Code is valid only once per Freecharge account/creditcard/debitcard/net banking/device/FCbalance/UPI transaction/mobile number. However; the cashback received from the Unique Code
                                        cannot be clubbed with any transactions conducted by the Participant through any virtual/ prepaid/ temporary/international cards.</li>
                                    <li>The cashback received through the Unique Code cannot be used for adding cash and for any transaction for bus bookings.</li>
                                    <li>Freecharge/XYZ, in its sole discretion, reserves the right to disqualify a Participant from the benefits of this Offer or cashback through the Unique Code, if any fraudulent activity is suspected or identified as
                                        being carried out for the purpose of availing the benefits under the said Offer.</li>
                                    <li>FreeCharge/ XYZ reserves the right to modify/change all or any of the terms applicable to this Offer or discontinue cashback through Unique Code without assigning any reasons or without any prior intimation whatsoever.
                                        In case of any disputes, FreeCharge?s decision shall be final.</li>
                                    <li>Freecharge/XYZ reserves the right to withdraw and/or alter the Offer and/or cashback through Unique Code at any time without prior notice.</li>
                                    <li>In no event shall Freecharge/XYZ be liable for any abuse or misuse of the Offer and/or cashback through Unique Code due to the negligence of the Participant.</li>
                                    <li>If the Offer and/or anything to be done by XYZ or Freecharge or any other entity in respect of the Offer and/or cashback through Unique Code is prevented or delayed by causes, circumstances or events beyond the control
                                        of XYZ or Freecharge or any other entity, including but not limited to, tampering, unauthorized intervention, interception, fraud, technical failures, floods, fires, accidents, earthquakes, riots, explosions,
                                        wars, hostilities, acts of government, or other causes of like or similar or other character beyond the control of XYZ or Freecharge or the other entity, then XYZ or Freecharge and/or the other entity shall
                                        not be liable for the same to the extent so prevented or delayed, and will not be liable for any consequences and on occurrence of such event, the Offer may be withdrawn at the discretion of Freecharge.</li>
                                    <li>The Participant agrees to indemnify and keep XYZ/ Freecharge harmless against all damages, liabilities, costs, expenses, claims, suits and proceedings (including reasonable attorney?s fee) that may be suffered by
                                        XYZ/ Freecharge as a consequence of (i) violation of terms of this Offer and/or the Unique Code by the Participant; (ii) violation of applicable laws; (iii) any action or inaction resulting in willful misconduct
                                        or negligence on the Participant?s part.</li>
                                    <li>Any taxes or liabilities or charges payable to the Government or any other authority or body, if any, shall be borne directly by the Participant and/or billed to the account of the Participant.</li>
                                    <li>The terms and conditions shall be governed by the laws of India. Any disputes arising out of and in connection with this program shall be subject to the exclusive jurisdiction of the Courts in Bangalore only.</li>
                                    <li>Nothing expressed or implied in this Offer shall in any way waive or amend any of the applicable terms and conditions on the XYZ platform and Freecharge platform.</li>
                                    <li>In case of any query regarding the Offer and/or cashback through the Unique Code or for any other queries on Freecharge, payments, refunds, cancellations etc., please email at care@freecharge.com.</li>
                                    <li>For non-receipt of emails containing the Unique Code by the Participant of the Offer, please contact XYZ customer support at 080 6156 1999.</li>
                                </ol>
                                <br><br>
                                <b id="offer34"><u>Maxessorize American Express Offer</u></b>
                                <br><br>This Offer is being offered by American Express Bank Limited (?American Express? or ?Bank?) on www.XYZ.com, XYZ Mobile App, XYZ Lite Platform (all three collectively ?Platform?) which Platform is owned
                                and operated by XYZ Designs Private Limited (?XYZ?).It is open to all residents of India holding a valid American Express Consumer Card, issued by American Express Banking Corp in India (hereinafter referred to as
                                ?Cardmember? or ?American Express Cardmembers?).
                                <br><br> Offer is valid from 00:00 hrs on 6 th February 2019 (00:00 hrs) to 23:59 hrs 8 th February 2019 (?Offer Period?).
                                <br> Under this Offer, American Express Cardmembers will get an instant discount of 15% on the product price (excluding taxes) purchased from the sellers on the Platform after deduction of any amounts paid using American
                                Express Cards. To be eligible for this Offer, the minimum amount required to be spent through American Express Card should be INR 3000/- or more (three thousand only). The maximum discount that the customer shall be eligible
                                for is INR 1500/- (one thousand five hundred only) during the Offer Period.
                                <br> This Offer does not require any coupon code and shall not be valid on corporate cards and on EMI transactions.
                                <br> Delinquent and over-limit credit card members will not qualify for this Offer. Further, Offer and discounts shall not be applicable for Card-on-delivery transactions, on gift wrap premium delivery charges or for NRI
                                customers.
                                <br> Nothing herein amounts to a commitment by Bank to conduct further, similar or other offers. This Offer is non- encashable, not-extendable and non-negotiable.
                                <br> This Offer is fully funded by American Express Bank only.
                                <br><br>
                                <p><strong>Other terms and conditions:</strong></p>
                                <ol>
                                    <li>1.An American Express Cardmember for the purpose of this Offer means a person holding a Consumer Card, issued by American Express Banking Corp in India only.
                                    </li>
                                    <li>2. This offer is not applicable on Corporate Cards.</li>
                                    <li>3. This Offer is valid on all American Express Consumer Cards issued in India except Corporate Cards.</li>
                                    <li>4. Add on cards will be treated as separate cards.</li>
                                    <li>5. No promotion code is needed to avail the Offer.</li>
                                    <li>6. The maximum discount that can be availed per customer under the Offer and per card is limited to INR 1500.</li>
                                    <li>7. The Offer is not applicable on EMI transactions.</li>
                                    <li>8. Under no circumstance will the discount being offered under this Offer be settled with cash in lieu by XYZ or the Bank.
                                    </li>
                                    <li>9. Customers will have to make the payment with their American Express Consumer Card issued in India only to avail this Offer.
                                    </li>
                                    <li>10. This Offer is not applicable for card on delivery transactions.</li>
                                    <li>11. This Offer is not applicable on gift wrap or premium delivery charges, gift cards, gold and precious jewellery.</li>
                                    <li>12. This Offer is applicable only on eligible purchases on the XYZ Platform.</li>
                                    <li>13. If the Customer redeems his/her payback loyalty points in the transaction, the Customer shall qualify for the Offer only on the net amount paid by the Customer through his/her American Express cards.
                                    </li>
                                    <li>14. This Offer cannot be clubbed with any other American Express offer on XYZ or any other existing offer on Platform.</li>
                                    <li>15. Customer should follow the standard XYZ guidelines and terms and condition for using the XYZ Platform under this Offer.
                                    </li>
                                    <li>16. The discount amount for a transaction cannot be combined with or transferred or assigned to any other transaction.</li>
                                    <li>17. In the event orders are cancelled during and outside the Offer Period, the discount applicable will be deducted from the refund.
                                    </li>
                                    <li>18. American Express or XYZ are not making any commitment to make any offer or provide any discounts beyond the Offer Period.
                                    </li>
                                    <li>19. American Express shall in no way be liable if any Customer is unable to login to XYZ Platform due to incompatibility of device, internet usage plans, or any other reason whatsoever.
                                    </li>
                                    <li>20. All other terms and conditions of the Cardmember Terms and Conditions shall continue to apply to a Cardmember.</li>
                                    <li>21. The Offer is valid only if the Customer?s account continues to be in good standing and payment continues to reach American Express before the payment due date.
                                    </li>
                                    <li>22. American Express does not make any warranties or representation on the delivery, service, quality, quantity, merchantability, suitability or availability of the products or services included in this Offer. The sellers
                                        on XYZ.com shall be responsible for the products purchased by a Customer.
                                    </li>
                                    <li>23. American Express or XYZ are not liable or responsible for any claim(s), dispute(s) regarding delivery, service, suitability, merchantability, availability, quantity or quality made available to the customers
                                        under the Offer on the products/services (shortly referred as ?Claims?). In reference to any Claims, it must be addressed in writing by the customer directly to the XYZ?s sellers. American Express shall not entertain
                                        any communication in this regard. Any query/ contention/dispute raised by any Customer to American Express with respect to the above shall be forwarded to the XYZ and they shall be solely responsible for resolving
                                        such queries/ contentions/disputes within reasonable time.
                                    </li>
                                    <li>24. Acceptance of these terms is a prerequisite for participation in this Offer. Such participation is voluntary and is deemed</li>
                                    as acceptance of the terms and conditions mentioned herein.
                                    <li>25. American Express holds the exclusive right at its sole discretion to refuse or deny the Offer to any Cardmember. The Cardmember shall become ineligible to participate in this Offer if his/her card is cancelled before
                                        the expiry of /during the Offer Period.
                                    </li>
                                    <li>26. Any taxes or liabilities or charges payable to the Government or any other authority or body, if any, shall be borne directly by Cardmember and/or billed to the account of the Cardmember.
                                    </li>
                                    <li>27. Products offered under this Offer are subject to availability from the respective sellers of XYZ and in no circumstances XYZ shall be liable for non-availability of any of the products.
                                    </li>
                                    <li>28. If the Offer and/or anything to be done by American Express or any other entity in respect of the Offer is prevented or delayed by causes, circumstances or events beyond the control of American Express or any other
                                        entity, including but not limited to, tampering, unauthorized intervention, interception, fraud, technical failures, floods, fires, accidents, earthquakes, riots, explosions, wars, hostilities, acts of government,
                                        or other causes of like or similar or other character beyond the control of American Express or the merchant, then American Express and/or XYZ and/or the sellers shall not be liable for the same to the extent
                                        so prevented or delayed, and will not be liable for any consequences and on occurrence of such event, the Offer may be withdrawn at the discretion of American Express.
                                    </li>
                                    <li>29. The Terms and Conditions shall be governed by the laws of India. Any disputes arising out of and in connection with this program shall be subject to the exclusive jurisdiction of the Courts in Bangalore only.
                                    </li>
                                    <li>30. Nothing expressed or implied in this offer shall in any way waive or amend any of the applicable terms and conditions on the Platform.
                                    </li>
                                    <li>31. Under no circumstance will the goods purchased/ services obtained during the Offer period be settled in cash.</li>
                                    <li>32. All government levies like Sales Tax, TDS, any Local Tax, Octroi etc., shall be payable by the Cardmember as applicable at the time the respective Offer was availed.
                                    </li>
                                    <li>33. American Express and XYZ reserve the right to modify or change any of the terms and conditions applicable to this offer at any time with/without prior notice to the Cardmember.
                                    </li>
                                    <li>34. American Express shall not be held liable for any reason, including but not limited to any delay or loss that may be caused in delivery of the services.
                                    </li>
                                    <li>35. XYZ and/or American Express shall not also be liable for any loss or damage whatsoever that may be suffered, or for any personal injury that may be suffered, to a customer / Cardmember, directly or indirectly,
                                        by use or non-use of products/services under the Offer.
                                    </li>
                                    <li>36. American Express and or XYZ reserves the right, at any time, without prior notice and without assigning any reason whatsoever, to add/alter/modify/change/cancel or vary all of these terms and conditions or to
                                        replace, wholly or in part, this Offer by another offer, whether similar to this offer or not, or to extend or withdraw/cancel it altogether at its sole discretion and the same shall be binding on the Cardmember
                                        at all times.
                                    </li>
                                    <li>37. Customers / Cardmember are not bound in any way to participate in this Offer. Any participation is voluntary, and the Offer is being made purely on a best effort basis.
                                    </li>
                                    <li>38. Nothing herein amounts to a commitment by American Express to conduct further, similar or other offer.</li>
                                    <li>39. The above Offer is by way of a special offer for select American Express Cardmembers only and nothing contained herein shall prejudice or affect the terms and conditions of the card member terms and conditions.
                                        The terms of this Offer shall be in addition to and not in derogation of the terms contained in the card member terms and conditions. Payment of fees/service charges/all other amounts due from the Customer to American
                                        Express from usage of credit cards by the Customer under this Offer and/or otherwise will be governed by American Express Terms &amp; Conditions.
                                    </li>
                                    <li>40. A Cardmember without being required to do any further act, shall be deemed to have read, understood and unconditionally accepted the terms and conditions herein. The above terms and conditions need to be read in
                                        conjunction with the American Express Card member Agreement.
                                    </li>
                                    <li>41. The Offer cannot be combined with any other offer and cannot be transferred or assigned to any other person/customer.</li>
                                    <li>42. Any query regarding the offer will be entertained till 8th March 2019. Post such date, XYZ or American Express will not entertain any correspondence or communication regarding this Program from any customers/persons.
                                    </li>
                                    <li>43. All liability with respect to the products purchased under this Offer lies with the respective seller and XYZ and American Express shall be in no way responsible for the same.
                                    </li>
                                    <li>44. XYZ or the sellers shall not be liable for any failure by bank to honour the transaction and for any technical issues as a result of which the transaction made by the Cardmember was not successful.
                                    </li>
                                </ol>
                                <br><br>
                                <b id="offer35"><u>Adidas UB19 box offer</u></b>
                                <p><strong><u>Terms &amp; Conditions:&nbsp;&nbsp; </u></strong></p>
                                <ol>
                                    <li>adidas India Marketing Pvt. Ltd. (hereinafter “adidas”) offers to the general public within the territory of India (except the state of Tamil Nadu) a chance to avail an offer wherein on purchase of adidas &nbsp;Ultraboost
                                        19 shoes from the website <a href="http://www.XYZ.com">XYZ.com</a> or XYZ mobile app the first 10 &nbsp;Customer(s) shall win an exclusive adidas accessories pack (“Box”).</li>
                                </ol>
                                <ol start="2">
                                    <li>This offer is only valid on purchase of adidas Ultraboost 19 shoes from the Website <a href="http://www.XYZ.com">XYZ.com</a> or the XYZ mobile app.</li>
                                </ol>
                                <ol start="3">
                                    <li>This Customer needs to purchase the &nbsp;adidas Ultraboost 19 shoes between 10:00 hrs on 21st February, 2019 to 23:59 hrs on&nbsp; 7<sup>th</sup> March, 2019.</li>
                                </ol>
                                <ol start="4">
                                    <li>First 10 Customers purchasing adidas Ultraboost 19 shoes shall be declared as winner by XYZ.</li>
                                </ol>
                                <ol start="5">
                                    <li>The Offer is valid only on available stock of adidas Ultraboost 19 at the Website/app of XYZ. Neither adidas nor its business partners shall be responsible for non-availability of adidas Ultraboost 19 in size, color
                                        or any other form thereof and shall not be held responsible for making options available for the same.</li>
                                </ol>
                                <ol start="6">
                                    <li>This Offer cannot be combined with any other offer or promotion conducted by adidas.</li>
                                </ol>
                                <ol start="7">
                                    <li>The lucky customers shall be informed by XYZ through email/phone calls/SMS and the Box shall be sent to the winning Customer through courier at the address provided by the Customer.</li>
                                </ol>
                                <ol start="8">
                                    <li>The Box won under this offer cannot be exchanged for cash or for any other adidas merchandise. adidas or its business partners shall not be liable to refund any cash at any point of time to the Customer.</li>
                                </ol>
                                <ol start="9">
                                    <li>adidas reserves the right to withdraw/ suspend/ cancel this offer without prior notice and without assigning any reason thereof, anytime during the term of this offer.</li>
                                </ol>
                                <ol start="10">
                                    <li>adidas shall not be &nbsp;liable for non-fulfillment of any terms of this Offer in full or in part due to any reason beyond adidas's control.</li>
                                </ol>
                                <ol start="11">
                                    <li>The decision of the management of adidas with regards the Offer shall be final and binding. By participating in this offer, the customer warrants that all information provided by the Customer regarding its name, age,
                                        state, city, address, phone number, etc., is true, correct, accurate and complete.</li>
                                </ol>
                                <ol start="12">
                                    <li>Failure by adidas to enforce any of its right at any stage does not constitute a waiver of those rights.</li>
                                </ol>
                                <ol start="13">
                                    <li>By availing this offer the Customer gives his consent to receive promotional SMS by adidas.</li>
                                </ol>
                                <ol start="14">
                                    <li>Participation in this Offer is voluntary. Participation in this Offer constitutes acceptance by the Customer of these terms and conditions.</li>
                                </ol>
                                <ol start="15">
                                    <li>By participating in the offer, each Customer represents and warrants that he/she is legally competent to enter into binding contracts under applicable laws.</li>
                                </ol>
                                <ol start="16">
                                    <li>adidas reserves the right to take photographs/videos of the winning Customer(s) of this offer. Customer shall not have objections to media publications of such photographs/videos and the same may be commercially used
                                        by adidas or its business partners. adidas or its business partners shall not pay any royalty/fees/compensation and the like to the Customer(S) for using the photographs/videos.</li>
                                </ol>
                                <ol start="17">
                                    <li>The Offer is provided on ‘AS-IS’ basis and subject to change without prior notice. By entering the Offer, the customer agrees that adidas shall not be liable for any claims, costs, injuries, losses, or damages of any
                                        kind arising out of or in connection with the Offer (except any liability which may not be excluded under applicable law).</li>
                                </ol>
                                <ol start="18">
                                    <li>The customer hereby agrees to indemnify and keep adidas, its associated companies, its event management company and their respective directors, officers, employees, contractors and agents, indemnified against any and
                                        all losses, claims (including but not limited to third party claims), injuries, costs, fees, fines, penalties, taxes, charges and any other liability arising out of any act of omission, commission, fraud, negligence
                                        or misconduct by the customer.</li>
                                </ol>
                                <ol start="19">
                                    <li>Decision of adidas in relation to the Offer and matter incidental thereto shall be final and binding on the customer. Dispute if any in respect of the Offer should be sent in writing to adidas India Marketing Pvt. Ltd.
                                        , The Arena, Plot No. 53, Sector-32, Institutional Area, Gurugram - 122001. The existence of dispute, if any, shall not constitute a claim against adidas. Any and all disputes, controversies and conflicts (“Disputes”)
                                        arising out of the Offer shall be referred to a sole Arbitrator to be appointed by the Director of adidas in accordance with the Arbitration and the Conciliation Act, 1996. The venue and seat of arbitration shall
                                        be New Delhi and the language used in arbitral proceeding shall be English. Any and all disputes in regard to the Offer or any matter incidental thereto shall be governed by the laws of India. Subject to the dispute
                                        resolution procedure as set out here in above, the courts at New Delhi shall have the exclusive jurisdiction in respect of all the subject matter with relation to the Offer.</li>
                                </ol>
                                <br><br>
                                <b id="offer36"><u>Adidas GHOST DROP offer</u></b>
                                <p><em>&nbsp;</em></p>
                                <p><strong><u>Terms and Conditions :</u></strong></p>
                                <p><strong><u>&nbsp;</u></strong></p>
                                <p><strong><u>Offer:</u></strong></p>
                                <p><strong>“adidas Originals Ghost Drop”</strong> (“Contest”) is a contest promoted and conducted by adidas India Marketing Pvt. Ltd. (“adidas”) within the territory of India (Except the state of Tamil Nadu) wherein the lucky
                                    Contestant shall get an opportunity to win “Ghost Drop” from adidas Originals on terms and conditions mentioned herein below.</p>
                                <p><strong><u>&nbsp;</u></strong></p>
                                <p><strong><u>Terms and Conditions :</u></strong></p>
                                <ol>
                                    <li>To be entitled to participate under this Contest, the Contestant needs to comment on the Instagram post using &nbsp;#ghostdrop #XYZsneakerclub</li>
                                    <li>The Contestant is required to do the above mentioned activity between 20<sup>th</sup> February to 21.00 hrs on 3<sup>rd</sup> March 2019.</li>
                                    <li>The Lucky Contestant shall be selected by a draw of lots and shall be entitled to receive adidas Originals “Ghost Drop” at the sole discretion of adidas &amp; XYZ.</li>
                                </ol>
                                <p>In addition to the above mentioned gratification, 5 Lucky Contestant shall be entitled to win adidas Originals merchandize.</p>
                                <ol start="4">
                                    <li>The Lucky Contestant shall be contacted by XYZ through a direct message on Instagram. It shall be responsibility of the User/Customer to check the message and act on the same.</li>
                                </ol>
                                <ol start="5">
                                    <li>adidas reserves the right to withdraw/ suspend/ cancel this offer without prior notice and without assigning any reason thereof, anytime during the term of this offer.</li>
                                </ol>
                                <ol start="6">
                                    <li>Any Contestant availing this offer shall abide by the all terms and conditions of this offer.</li>
                                </ol>
                                <ol start="7">
                                    <li>Under no circumstance the “Ghost Drops” won under this event be settled in cash/ or exchanged for any other merchandize or other “Ghost Drops”</li>
                                </ol>
                                <p>8.The “Ghost Drops” won under this offer by the winning Contestant can be anything. The “Ghost Drops” may or may not be used by the winning Contestant and no request of size or colour shall be entertained by adidas.</p>
                                <ol start="9">
                                    <li>The winning Contestant is restricted from selling or transferring the Ghost Drop won under this offer to any third person. In case the “Ghost Drop” won under this offer is sold or transferred by the winning Contestant,
                                        adidas shall have the right to claim appropriate damages from the winning Contestant.</li>
                                </ol>
                                <ol start="10">
                                    <li>The Event shall be subject to usual force majeure events and on occurrence of such event, the Offer may be withdrawn at the discretion of adidas.</li>
                                </ol>
                                <ol start="11">
                                    <li>By participating in the Contest, each Contestant represents and warrants that he/she is legally competent to enter into binding contracts under applicable laws. By taking part and/or entering into the Contest, the Contestant
                                        warrants that all information provided by the Contestant regarding its name, age, state, city, address, phone number, etc., is true, correct, accurate and complete and that the Contestant is authorized to provide
                                        such information and consents to the use of such information, including inter-alia personal data, by adidas , its affiliates, consultants, business partners, agents and/or its event management company for marketing
                                        and promotional purposes or otherwise sending communication materials to the Participant, through including but not limited to, direct mailers, emails, short messaging service and/or telephone calls. For such purpose,
                                        Contestant shall be deemed to have given his/her consent to the transfer of all or any personal data to any location within or outside India.</li>
                                </ol>
                                <ol start="12">
                                    <li>adidas reserves the right to take photographs of the Contestant winning under this Event. Contestant shall not have any objections to media publications of such photographs as may be undertaken and/or as may be commercially
                                        used by adidas. adidas shall not pay any royalty/fees/compensation and the like to the winner for using the photographs and/or any other media coverage as may be deemed proper by adidas.</li>
                                    <li>Offer shall be void where prohibited by law.</li>
                                    <li>Failure by adidas to enforce any of its right at any stage does not constitute a waiver of those rights.</li>
                                    <li>The Offer is conducted on ‘AS-IS’ basis and subject to change without prior notice. By entering the Offer, the Contestant agrees that adidas shall not be liable for any claims, costs, injuries, losses, or damages of
                                        any kind arising out of or in connection with the offer ( except any liability which may not be excluded under applicable law).</li>
                                    <li>The Contestant hereby agrees to indemnify and keep adidas and XYZ, its associated companies, its event management company and their respective directors, officers, employees, contractors and agents, indemnified against
                                        any and all losses, claims (including but not limited to third party claims), injuries, costs, fees, fines, penalties, taxes, charges and any other liability arising out of any act of omission, commission, fraud,
                                        negligence or misconduct by the Contestant.</li>
                                    <li>Decision of adidas in relation to the offer and matter incidental thereto shall be final and binding on the Contestant. The existence of dispute, if any, shall not constitute a claim against adidas. Any and all disputes,
                                        controversies and conflicts (“Disputes”) arising out of the Offer shall be referred to a sole Arbitrator to be appointed by the Managing Director of adidas in accordance with the Arbitration and the Conciliation
                                        Act, 1996. The venue and seat of arbitration shall be New Delhi and the language used in arbitral proceeding shall be English. Any and all disputes in regard to the Offer or any matter incidental thereto shall be
                                        governed by the laws of India. Subject to the dispute resolution procedure as set out here in above, the courts at New Delhi shall have the exclusive jurisdiction in respect of all the subject matter with relation
                                        to the Offer.</li>
                                </ol>
                                <b id="offer42"><u>XYZ Beauty Edit - ICICI and IndusInd American Express Offer</u></b>
                                <br><br>
                                <b>Terms and Conditions</b>
                                <br><br> Get extra 15% cashback up to Rs.1,500 on purchases worth Rs. 4,000 and above (exclusive of taxes and delivery charges) with ICICI American Express and IndusInd American Express Cards. The offer is valid from 27th
                                to 29th March, 2019. Extra 15% cashback is over and above the offer of up to 50% offered by XYZ. The Cardmember can avail the offer on XYZ website (https://www.XYZ.com/) or mobile app only. The offer is applicable
                                on supplementary card as well. The maximum cashback of Rs.1500 will be applicable per card only, during the offer period. The entire payment needs to be made through ICICI/IndusInd American Express Card. This offer is not
                                applicable on EMI transactions. This offer is not applicable on gift cards, gift wraps, gold and precious jewellery. Any misuse of this offer is strictly prohibited. There are no blackout dates. For queries related to the
                                offer, please contact XYZ on 08061561999 within 30 days from the offer end date. The cashback will be processed within 90 days from the offer end date.
                                <br><br>
                                <b>American Express® Terms and Conditions</b>
                                <br><br> This offer is valid for Cards issued by a third party bearing the name or trademark or service mark or logo of American Express ("Network Cards ") issued in India. This program is being offered by the participating
                                service establishment only and shall be valid for the period mentioned in the offer. The offer in this program will be fulfilled at the bank’s end only. The cashback will be processed within 90 days from the offer end date.
                                This offer is being made purely on a “best effort” basis. The Cardmembers are not bound in any manner to participate in this offer and any such participation is purely voluntary. American Express is neither responsible
                                for availability of services nor guarantees the quality of the goods/services and is not liable for any defect or deficiency of goods or services so obtained/availed of by the Cardmembers under this offer. Any disputes
                                with regards to the quality of goods/services availed shall be taken up with the merchant/service establishment directly. American Express shall have no liability whatsoever regarding the same. American Express shall not
                                be liable whatsoever for any loss/damage/claims that may arise out of use or non-use of any goods or services availed by Cardmember under this offer. American Express reserves its absolute right to withdraw and/or alter
                                any of the terms and conditions of the offer at any time without prior notice. Nothing expressed or implied in the program shall in any way waive or amend any of the Terms and Conditions of the existing Cardmember agreement
                                with the Card issuer. To receive this offer, purchase should be charged in full to the ICICI/IndusInd American Express® Card. No cash alternative is available. Any disputes arising out of and in connection with this program
                                shall be subject to the exclusive jurisdiction of the courts in the state of Delhi only.
                                <br><br>
                                <b>Terms and Conditions prescribed to ICICI Bank American Express Card Holders:</b>
                                <ul>
                                    <li>
                                        • Please note that the “Trendy Wednesday Offer” (27th March 2019) on XYZ.com cannot be clubbed with this offer. ICICI Bank American Express Credit Cardholder can choose to avail either of the offers during the offer period.
                                    </li>
                                    <li>
                                        • ICICI Bank holds out no warranty or makes no representation about the quality, delivery or otherwise of the goods and services offered by the franchisees of Alliance Partner. Any dispute or claim regarding the products and must be resolved by the Card
                                        Holder/s with the franchisees of alliance partner directly without any reference to ICICI Bank.
                                    </li>
                                    <li>
                                        • ICICI Bank shall not be liable in any manner whatsoever for any loss/ damage/ claim that may arise out of use or otherwise of any products purchased by the Card Holder/s under the alliance offered by the Alliance Partner.
                                    </li>
                                    <li>
                                        • ICICI Bank reserves the right to disqualify the Card Holder/s from the benefits of the Offer, if any fraudulent activity is identified as being carried out for the purpose of availing the benefits under the said Offer or otherwise by use of the Card.
                                    </li>
                                    <li>
                                        • The Card Holder/s shall be bound by the terms and conditions stipulated by the Alliance Partner in this regard.
                                    </li>
                                    <li>
                                        • No substitutions or exchange of Offer, other than what is detailed in the communication sent to the Card Holder/s shall be allowed. However, ICICI Bank reserves the right to substitute and/or change the Offer or any of them, without any intimation or
                                        notice, written or otherwise to the Card Holder/s.
                                    </li>
                                    <li>
                                        • The Program is not available wherever prohibited and/ or on merchandise/ products/ services for which such programs cannot be offered for any reason whatsoever.
                                    </li>
                                </ul>
                                <br><br>
                                <b>IndusInd Bank Terms and Conditions</b>
                                <br><br>
                                <ul>
                                    <li>
                                        • Get 15% Cashback up to a maximum of INR 1500 with IndusInd Bank American Express Credit Cards on minimum transaction of INR 4,000 at https://www.XYZ.com/
                                    </li>
                                    <li>
                                        • These cashback will be in form of a deferred cashback in form of cash credit to the cardmember’s card account
                                    </li>
                                    <li>
                                        • Maximum eligible Discount/Cashback is INR 1500 per Customer’s Card account and Customer can avail the offer only once in the Offer Period.
                                    </li>
                                    <li>
                                        • This offer cannot be clubbed with any other Cashback offer.
                                    </li>
                                    <li>
                                        • Cashback for eligible Customers will be credited by 30thth April, 2019. The Card account of the member should not be in default/delinquent/closed status at the time of crediting the Cashback.
                                    </li>
                                    <li>
                                        • Offer is valid on Eligible transaction done between 27th March, 28th March and 29th March 2019 (both days inclusive).
                                    </li>
                                    <li>
                                        • Offer will be valid only on the transactions done using a Valid and Open IndusInd Bank American Express Credit Card.
                                    </li>
                                    <li>
                                        • This Offer is being made purely on a “best effort” basis. Card members are not bound in any manner to participate in this Offer and any such participation is purely voluntary.
                                    </li>
                                    <li>
                                        • The list of cardmembers eligible for the cashback will be provided by
                                        <XYZ>
                                            , Indusind Bank holds no responsibility in case of non communication of eligibility by
                                            <XYZ></XYZ>
                                        </XYZ>
                                    </li>
                                    <li>
                                        • IndusInd Bank reserves the absolute right to withdraw and / or alter any of the terms and conditions of the Offer at any time without prior notice.
                                    </li>
                                    <li>
                                        • Nothing expressed or implied in the Program shall in any way waive or amend any of the terms and conditions of the existing Cardholder’s agreement with the Card issuer.
                                    </li>
                                    <li>
                                        • All other terms and conditions of the Cardholder’s agreement shall continue to apply.
                                    </li>
                                    <li>
                                        • IndusInd Bank does not make any warranties or representation of the quality, merchantability, suitability or availability of the products or services included in this offer.
                                    </li>
                                    <li>
                                        • The participation is voluntary and deemed as acceptance of the terms and conditions mentioned herein.
                                    </li>
                                    <li>
                                        • All issues / queries / complaints / grievances relating to the services/ products, if any, shall be addressed to the merchant directly.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer?>