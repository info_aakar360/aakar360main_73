<?php echo $header?>
<div class="profile-root bg-white ng-scope">
    <div class="clearfix buffer-top text-center wrapper-1400 bg-color">
        <div class="clearfix wrapper-1140 two-bgs">
            <div class="col-sm-12">
                <div class="profile-background vcard wrapper-1140">
                    <img src="assets/banners/<?=$dimg->image; ?>" class="cover">
                </div>
            </div>
        </div>
        <div class="profile-overlay clearfix wrapper-980 bgs-white ng-isolate-scope" style="width: 980px;" id="profile_start">
            <img itemprop="image" class="profile-icon" src="assets/checkout.png">
            <h1 itemprop="name" class="name hand-writing font-44 font-36-xs">
                <a itemprop="url" class="url">
                    <?=translate('Checkout')?>
                </a>
            </h1>
        </div>
    </div>
    <div class="clearfix wrapper-980 bg-white m-t-xs m-b-md">
        <div class="about-designer clearfix">
            <div class="text-left" style="padding-top: 0px;">
                <div id="content" class="container" style="width: 1145px;">
                    <!--div id="cart-header">
                        Enter information to process your order
                    </div-->
                    <?php if(!isset($_COOKIE['cart'])){ ?>
                        <div class="cart-content text-center" style="padding: 50px 15px;"><h2>You have not added any product to Cart.</h2><a class="btn btn-primary" href="<?=url(''); ?>">Continue Shopping</a></div>
                    <?php }else{ ?>
                        <div id="cart-content" style="padding-top: 5px;">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseCheckout" class="restrict" style="color: #ffffff;">
                                            <h4 class="panel-title">
                                                Checkout Method
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="collapseCheckout" class="panel-collapse collapse <?=(session('customer') == '')? 'in' : ''; ?>">
                                        <div class="panel-body">
                                            <div class="col-md-12">
                                                <div class="col-md-6">
                                                    <h4>CHECKOUT AS A GUEST OR REGISTER</h4>
                                                    <p>
                                                        Register with us for future convenience:
                                                    </p>
                                                    <p>
                                                        <form name="checkout_options">
                                                    <p><label for="guest"><input type="radio" name="options" value="guest" id="guest" checked="true"/> Checkout as Guest</label></p>
                                                    <p><label for="register"><input type="radio" name="options" value="register" id="register"/> Register</label></p>
                                                    </form>
                                                    </p>
                                                    <p>
                                                        Register and save time!
                                                    </p>

                                                    <input name="register" type="button" id="continueBtn" value="<?=translate('Continue') ?>" class="btn btn-primary" />
                                                </div>
                                                <div class="col-md-6">
                                                    <h4>LOGIN</h4>
                                                    <div class="col-md-8">
                                                       <input type="hidden" class="order_session_id" value="{{ $order_session_id }}">
                                                        <?php if(session('customer') == ''){ ?>
                                                            <form action="" method="post" id="login" class="form-horizontal single">

                                                                <?=csrf_field() ?>

                                                                <fieldset>

                                                                    <div class="form-group">

                                                                        <label class="control-label"><?=translate('E-mail') ?></label>

                                                                        <input name="email" type="email" value="<?=isset($_POST['email']) ? $_POST['email'] : '' ?>" class="form-control"  />

                                                                    </div>

                                                                    <div class="form-group">

                                                                        <label class="control-label"><?=translate('Password') ?></label>

                                                                        <input name="password" type="password" class="form-control"  />

                                                                    </div>
                                                                    <a class="smooth pull-left" href="{{route('retail.retailResetPassword')}}">Forgot your password ?</a>
                                                                    <input name="login" type="submit" id="loginBtn" value="<?=translate('Login') ?>" class="btn btn-primary pull-right" />

                                                                </fieldset>

                                                            </form>
                                                        <?php } else{ ?>
                                                            <p>You are already logged in as <br><b><?=customer('name'); ?></b></p>
                                                            <p>
                                                                <input name="logout" type="button" id="logoutBtn" value="<?=translate('Logout') ?>" class="btn btn-primary" />
                                                            </p>
                                                        <?php } ?>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseBilling" class="restrict" style="color: #ffffff;">
                                            <h4 class="panel-title">
                                                Billing Information
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="collapseBilling" class="panel-collapse collapse <?=(session('customer') == '')? '' : 'in'; ?>">
                                        <div class="panel-body">
                                            <p class="formP">

                                            </p>
                                            <input type="button" id="goToShipping" class="btn btn-primary pull-right" value="Continue"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseShipping" class="restrict" style="color: #ffffff;"><h4 class="panel-title">
                                                Shipping Information
                                            </h4></a>
                                    </div>
                                    <div id="collapseShipping" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p class="formS">

                                            </p>
                                            <input type="button" id="goToSM" class="btn btn-primary pull-right" value="Continue"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseSM" class="restrict" style="color: #ffffff;"><h4 class="panel-title">
                                                Shipping Method
                                            </h4></a>
                                    </div>
                                    <div id="collapseSM" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p class="formSM">

                                            </p>
                                            <input type="button" id="goToOrder" class="btn btn-primary pull-right" value="Continue"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOrder" class="restrict" style="color: #ffffff;"><h4 class="panel-title">
                                                Order Review
                                            </h4></a>
                                    </div>
                                    <div id="collapseOrder" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p class="formOrder">

                                            </p>
                                            <input type="button" id="goToPM" class="btn btn-primary pull-right" value="Continue"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapsePI" class="restrict" style="color: #ffffff;"><h4 class="panel-title">
                                                Payment Information
                                            </h4></a>
                                    </div>
                                    <div id="collapsePI" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p class="formPI">

                                            </p>
                                            <input type="button" id="confirmOrder" onclick="razorpayPaymentCheckout();" class="btn btn-primary pull-right" value="Confirm"/>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>




<?php echo $footer?>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<?php if(session('customer') != ''){ ?>
    <script>
        $.ajax({
            url: 'api/billing',
            type: 'post',
            data: 'type=loggedin',
            success: function(data){
                $('#collapseBilling .formP').html(data);
                $('#collapseCheckout').removeClass('in');
                $('#collapseCheckout').attr('area-expanded', 'false');
                $('a[href="#collapseCheckout"]').removeClass('restrict');
                $('a[href="#collapseCheckout"]').unbind('click');
                $('a[href="#collapseBilling"]').removeClass('restrict');
                $('a[href="#collapseBilling"]').unbind('click');
                $('#collapseBilling').addClass('in');
                $('#collapseBilling').attr('area-expanded', 'true');
                $('#collapseBilling').attr('style', '');
            }
        });
    </script>
<?php } ?>
<script>
    function scrollTop(){
        $('html, body').animate({
            scrollTop: $("#profile_start").offset().top
        }, 'slow');
    }
    $('.restrict').on('click', function(){
        return false;
    });
    $("form#login").submit(function(e){
        e.preventDefault();
        var formData = $("form#login").serialize();
        $.ajax({
            url: 'api/login',
            type: 'post',
            data: formData+'&login=true',
            beforeSend: function(){
                $('#loginBtn').val('Logging in...');
                $('#login .alert').remove();
            },
            success: function(datax){
                data = JSON.parse(datax);
                if(data.success == null){
                    $('#login').prepend('<div class="alert alert-danger">'+data.error+'</div>');
                }else{
                    $('#login').prepend('<div class="alert alert-success">'+data.success+'</div>');
                    location.reload();
                }
            },
            complete: function(){
                $('#loginBtn').val('Login');
                scrollTop();
            }
        });
    });
    $('#continueBtn').on('click', function(){
        var target = $('input[name="options"]:checked').val();
        var same = '';
        if(target == null){
            alert('Please select any one option');
        }else{
            $.ajax({
                url: 'api/billing',
                type: 'post',
                data: 'type='+target,
                success: function(data){
                    $('#collapseBilling .formP').html(data);
                    $('#collapseCheckout').removeClass('in');
                    $('#collapseCheckout').attr('area-expanded', 'false');
                    $('a[href="#collapseCheckout"]').removeClass('restrict');
                    $('a[href="#collapseCheckout"]').unbind('click');
                    $('a[href="#collapseBilling"]').removeClass('restrict');
                    $('a[href="#collapseBilling"]').unbind('click');
                    $('#collapseBilling').addClass('in');
                    $('#collapseBilling').attr('area-expanded', 'true');
                    $('#collapseBilling').attr('style', '');

                    var same = data
                },
                complete: function(){
                    scrollTop();
                }
            });
        }
    });
    $('#goToShipping').on('click', function(){
        var target = $('input[name="shipping_address_option"]:checked').val();
        var postcode = $('input[name="postcode"]').val();
        if(target == null){
            alert('Please select any one option');
        }else{
            $.ajax({
                url: 'api/shipping',
                type: 'post',
                data: 'type='+target+'&postcode='+postcode,
                beforeSend: function(){
                    $('#goToShipping').attr('value', 'Loading...');
                },
                success: function(data){
                    if(target == 'same'){
                        $('#collapseShipping .formS').html(data);
                        $('#collapseBilling').removeClass('in');
                        $('#collapseBilling').attr('area-expanded', 'false');
                        $('a[href="#collapseBilling"]').removeClass('restrict');
                        $('a[href="#collapseBilling"]').unbind('click');
                        $('a[href="#collapseShipping"]').removeClass('restrict');
                        $('a[href="#collapseShipping"]').unbind('click');
                        $('#goToSM').trigger('click');
                    }else {
                        $('#collapseShipping .formS').html(data);
                        $('#collapseBilling').removeClass('in');
                        $('#collapseBilling').attr('area-expanded', 'false');
                        $('a[href="#collapseBilling"]').removeClass('restrict');
                        $('a[href="#collapseBilling"]').unbind('click');
                        $('a[href="#collapseShipping"]').removeClass('restrict');
                        $('a[href="#collapseShipping"]').unbind('click');
                        $('#collapseShipping').addClass('in');
                        $('#collapseShipping').attr('area-expanded', 'true');
                        $('#collapseShipping').attr('style', '');
                    }
                },
                complete: function(){
                    $('#goToShipping').attr('value', 'Continue');
                    scrollTop();
                }
            });
        }
    });
    $('#goToSM').on('click', function(){
        var formData = $('.formS form').serialize();
        $.ajax({
            url: 'api/shipping-method',
            type: 'post',
            data: formData,
            beforeSend: function(){
                $('#goToSM').attr('value', 'Loading...');
            },
            success: function(data){
                $('#collapseShipping').removeClass('in');
                $('#collapseShipping').attr('area-expanded', 'false');
                $('a[href="#collapseShipping"]').removeClass('restrict');
                $('a[href="#collapseShipping"]').unbind('click');
                $('a[href="#collapseSM"]').removeClass('restrict');
                $('a[href="#collapseSM"]').unbind('click');
                $('#collapseSM').addClass('in');
                $('#collapseSM').attr('area-expanded', 'true');
                $('#collapseSM').attr('style', '');
                $('.formSM').html(data);
            },
            complete: function(){
                $('#goToSM').attr('value', 'Continue');
                scrollTop();
            }
        });
    });
    $('#goToPM').on('click', function(){
        var formData = $('.formSM form').serialize();
        $.ajax({
            url: 'api/payment-method',
            type: 'post',
            data: formData,
            beforeSend: function(){
                $('#goToPM').attr('value', 'Loading...');
            },
            success: function(data){
                $("#collapsePI").collapse('show');
                $("#collapseOrder").collapse('hide');
                $('.formPI').html(data);
            },
            complete: function(){
                $('#goToPM').attr('value', 'Continue');
                scrollTop();
            }
        });
    });
    $('#goToOrder').on('click', function(){
        var formData = $('.formSM form').serialize();
        $.ajax({
            url: 'api/order-confirm',
            type: 'post',
            data: formData,
            beforeSend: function(){
                $('#goToOrder').attr('value', 'Loading...');
            },
            success: function(data){
                $("#collapseSM").collapse('hide');
                $("#collapseOrder").collapse('show');
                $('.formOrder').html(data);
            },
            complete: function(){
                $('#goToOrder').attr('value', 'Continue');
                scrollTop();
            }
        });
    });

    function razorpaySubscription() {
        var plan_id =  $('.productIds').val();
        $.ajax({
            type:'POST',
            url:'{{route('retail.razorpay-subscription')}}',
            data: {plan_id: plan_id,_token:'{{csrf_token()}}'},
            success:function(response){
                razorpayPaymentCheckout(response)
            }
        })
    }



    function razorpayPaymentCheckout() {
        var plan_id =  $('.productIds').val();
        var totalAmount = $('.grTotal').val();
        var options = {
            "key": "{{ env('RAZORPAY_KEY') }}",
            "amount": (totalAmount*100), // 2000 paise = INR 20
            "name": "Aakar360 Mentors Pvt. Ltd.",
            "description": "Payment",
            "image": "https://aakar360.com/main/images/logo.png",
            "handler": function (response){
                confirmRazorpayPayment(response);
            },
            "notes": {
                "plan_id": plan_id,
            },
            "prefill": {
                "contact": '{{ (customer('mobile') == '')? customer('mobile') : '' }}',
                "email":   '{{ (customer('email') == '')? customer('email') : '' }}',
            },
            "theme": {
                "color": "#003481"
            }
        };
        var rzp1 = new Razorpay(options);
        rzp1.open();
    }

    function confirmRazorpayPayment(response) {
        var plan_id = $('.productIds').val();
        var payment_id = response.razorpay_payment_id;
        $.ajax({
            type:'POST',
            url:'{{route('retail.razorpay-payment')}}',
            data: {paymentId: payment_id,plan_id: plan_id,_token:'{{csrf_token()}}'},
            redirect:true,
            success: function(data){
                if(data == 'success'){
                    window.location.replace('<?php echo route('retail.retailSuccess'); ?>');
                }else{
                    alert(data);
                }
            }
        })
    }
    {{--var SITEURL = '{{URL::to('')}}';--}}
    {{--$.ajaxSetup({--}}
        {{--headers: {--}}
            {{--'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
        {{--}--}}
    {{--});--}}
    {{--$('body').on('click', '#confirmOrder', function(e){--}}
        {{--var totalAmount = $('.grandTotal').val();--}}
        {{--var product_ids =  $('.productIds').val();--}}
        {{--var options = {--}}
            {{--"key": "{{ env('RAZORPAY_KEY') }}",--}}
            {{--"amount": (totalAmount*100), // 2000 paise = INR 20--}}
            {{--"name": "Aakar360 Mentors Pvt. Ltd.",--}}
            {{--"description": "Payment",--}}
            {{--"image": "https://aakar360.com/main/images/logo.png",--}}
            {{--"handler": function (response){--}}
                {{--window.location.href = SITEURL +'/'+ 'payment-success?payment_id='+response.razorpay_payment_id+'&product_ids='+product_ids+'&amount='+totalAmount;--}}
            {{--},--}}
            {{--"prefill": {--}}
                {{--"contact": '{{ (session('customer') == '')? customer('mobile') : '' }}',--}}
                {{--"email":   '{{ (session('customer') == '')? customer('email') : '' }}',--}}
            {{--},--}}
            {{--"theme": {--}}
                {{--"color": "#528FF0"--}}
            {{--}--}}
        {{--};--}}
        {{--var rzp1 = new Razorpay(options);--}}
        {{--rzp1.open();--}}
        {{--e.preventDefault();--}}
    {{--});--}}
    /*document.getElementsClass('buy_plan1').onclick = function(e){
    rzp1.open();
    e.preventDefault();
    }*/
    {{--$('#confirmOrder').on('click', function(){--}}
        {{--$.ajax({--}}
            {{--url: 'api/confirm',--}}
            {{--type: 'POST',--}}
            {{--success: function(data){--}}
                {{--if(data == 'success'){--}}
                    {{--window.location.replace('<?php echo url('success'); ?>');--}}
                {{--}else{--}}
                    {{--window.location.replace('<?php echo url('failed'); ?>');--}}
                {{--}--}}
            {{--}--}}
        {{--});--}}
    {{--});--}}
    $('#logoutBtn').on('click', function(){
        $.ajax({
            url: 'api/logout',
            type: 'post',
            success: function(data){
                location.reload();
            }
        });
    });
</script>