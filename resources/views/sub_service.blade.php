<?php echo $header?>
<section id="at-inner-title-sec" class="pdbtm-165">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-8 pdtop-100">
                <div class="inner-border">
                    <div class="at-inner-title-box text-center style-1">
                        <h1 style="text-transform: none;">Get a designer space you'll love</h1>
                        <p style="text-transform: none;font-size: 20px;">Let our accomplished team of experts submit concepts for your space <b>— it's easy!</b></p>
                    </div>
                </div>
            </div>
            <div class="request-box" style="background: #ffffff;">
                <p>Let me start by saying, I love, love, love, Decorilla. Thanks so much for capturing my vision and bringing my ordinary space to a level of brilliance! Every detail looks absolutely amazing.</p>
                <a class="btn btn-primary btn-sm" data-popup-open="popup-1" href="#">Get Started !</a>
                <p>
                    <?php if(customer('id') !== ''){
                        $user_id = customer('id');

                        $img = DB::select("SELECT * FROM service_wishlist WHERE user_id = '".$user_id."' AND service_id = '".$category_id."'");

                        if(!empty($img)){
                            echo '<button  class="btn btn-primary btn-sm" id="likebutton1" style="width: 90%; display:none; position: absolute; bottom: 40px; left: 0px; margin: 15px; right: 0;">
                        <span class="tickgreen">✔</span> Added to Project</button>';
                        } else{
                            echo '<button class="btn btn-primary likebutton1 btn-sm" id="likebutton1" style="width: 90%; display:none; position: absolute; bottom: 40px; left: 0px; margin: 15px; right: 0;"><i class="fa fa-heart"></i> Add to Project</button>';

                        }} else {
                        echo '<button class="btn btn-primary btn-sm" onClick="getModel()" style="width: 90%; display:none; position: absolute; bottom: 40px; left: 0px; margin: 15px; right: 0;"><i class="fa fa-heart"></i> Add to Project</button>';
                    }?>
                </p>
            </div>
        </div>
    </div>

</section>
<!-- Inner page heading end -->
<section class="how-it-works-sec padding" style="background: transparent">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="at-sec-title">
                    <h2 style="font-size: 24px; text-transform: none;">How It <span>Works</span></h2>
                    <div class="at-heading-under-line">
                        <div class="at-heading-inside-line"></div>
                    </div>
                    <p class="sub-title">Specedoor is the best way to design your home. We work within any budget, big or small. You can start from scratch or work with a designer using your existing furniture pieces.</p>
                </div>
            </div>
        </div>
        <div class="tabs tabs-style-underline">
            <nav>
                <ul>
                    <li><a href="#section-underline-1" class="icon icon-idcard"><span>Pick your designer</span></a></li>
                    <li><a href="#section-underline-2" class="icon icon-personalize"><span>Personalize your design</span></a></li>
                    <li><a href="#section-underline-3" class="icon icon-ideas"><span>Collaborate on ideas</span></a></li>
                    <li><a href="#section-underline-4" class="icon icon-room"><span>Visualize your room</span></a></li>
                    <li><a href="#section-underline-5" class="icon icon-shop"><span>Shop with ease</span></a></li>
                </ul>
            </nav>
            <div class="content-wrap">
                <section id="section-underline-1">
                    <div class="row">
                        <div class="col-md-4 details">
                            <h3>Pick your designer</h3>
                            <p>Specedoor interior designers are vetted professionals and real people. Take our style survey to get matched with your perfect designer based on your style, or explore 100+ designers on your own.</p>
                        </div>
                        <div class="col-md-8"><img src="<?=url('/assets/images/pick-your-designer.jpg');?>" alt="Pick your designer" style="margin-right:-27px;"/></div>
                    </div>
                </section>
                <section id="section-underline-2">
                    <div class="row">
                        <div class="col-md-4 details">
                            <h3>Personalize your design</h3>
                            <p>We work with your budget, style, and unique space. We'll even incorporate any of your existing pieces into the room design. After you book your designer, fill out your room profile to tell us exactly what you are looking for.</p>
                        </div>
                        <div class="col-md-8"><img src="<?=url('/assets/images/personalize-your-design.jpg');?>" alt="Personalize your design" style="margin-right:-27px;"/></div>
                    </div>
                </section>
                <section id="section-underline-3">
                    <div class="row">
                        <div class="col-md-4 details">
                            <h3>Collaborate on ideas</h3>
                            <p>After 2 business days, your designer will come back to you with an initial set of ideas based on your vision. Give feedback along the way to refine a concept for your room. Your happiness is 100% guaranteed.</p>
                        </div>
                        <div class="col-md-8"><img src="<?=url('/assets/images/collaborate-on-ideas.jpg');?>" alt="Collaborate on ideas" style="margin-right:-27px;"/></div>
                    </div>
                </section>
                <section id="section-underline-4">
                    <div class="row">
                        <div class="col-md-4 details">
                            <h3>Visualize your room</h3>
                            <p>It's hard to imagine the perfect room! Share a floor plan and room dimensions, and we'll create a visualization of your room and a floor plan recommendation so you can envision the final result.</p>
                        </div>
                        <div class="col-md-8"><img src="<?=url('/assets/images/visualize-your-room.jpg');?>" alt="Visualize your room" style="margin-right:-27px;"/></div>
                    </div>
                </section>
                <section id="section-underline-5">
                    <div class="row">
                        <div class="col-md-4 details">
                            <h3>Shop with ease</h3>
                            <p>Shop your curated list of products, sourced from over 150 sellers, all in one place. Your ordering concierge will purchase all your pieces and keep a close eye on your orders to ensure everything gets to you, with no hassle.</p>
                        </div>
                        <div class="col-md-8"><img src="<?=url('/assets/images/shop-with-ease.jpg');?>" alt="Shop with ease" style="margin-right:-27px;"/></div>
                    </div>
                </section>
            </div><!-- /content -->
        </div><!-- /tabs -->
    </div>
</section>
<!-- Property start from here -->
<section class="at-property-sec at-property-right-sidebar">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="at-sec-title">
                    <h3 style="text-transform: none;">Services offered by our Experts</h3>
                    <div class="at-heading-under-line">
                        <div class="at-heading-inside-line"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <?php
                $service_id = '';
                foreach($sub_service as $service){
                    $service_id = $service->id;
                    $price = is_numeric($service->price)? c($service->price) : $service->price;
                    echo '	<div class="col-md-3 col-lg-3 col-sm-4 col-xs-6">
                                <a href="service/'.$service->id.'" data-title="'.translate($service->title).'">
                                    <div class="at-property-item at-col-default-mar" id="'.$service->id.'">
                                        <div class="at-property-img">
                                            <img src="'.url('/assets/services/'.image_order($service->images)).'" style="width: 360px; height: 200px;" alt="">
                                        </div>
                                        
                                        <div class="at-property-location" style="min-height: 220px; height: 230px;">
                                            <h5><a href="service/'.$service->id.'" data-title="'.translate($service->title).'">'.translate($service->title).'</a></h5>
                                            <b>Service Starts at '.$price.'</b>
                                            <p>'.substr($service->description, 0, 50).'...</p>';
                    if(Customer('id') != null) {
                        echo '<a onClick="getQuestions(' . $service->id . ')" class="btn btn-primary btn-sm1" style="width: 100%; margin-top: 15px; margin-bottom: 10px;">Get Started !</a>';
                    } else {
                        echo '<a onClick="getModel()" class="btn btn-primary btn-sm" style="width: 100%; margin-top: 15px; margin-bottom: 10px;">Get Started !</a>';
                    }
                    if(customer('id') !== ''){
                        $user_id = customer('id');
                        $layout_id = $service->id;

                        $img = DB::select("SELECT * FROM service_wishlist WHERE user_id = '".$user_id."' AND sub_service_id = '".$layout_id."'");

                        if(!empty($img)){
                            echo '<button class="btn btn-primary btn-sm" id="" style="width: 100%; display:none;"><span class="tickgreen">✔</span> Added to Project</button>';
                        } else{
                            echo '<input type="hidden" name="pid1" class="pid1" value="'.$layout_id.'">
                                                    <button class="btn btn-primary likebutton btn-sm" style="width: 100%; display:none;"><i class="fa fa-heart"></i> Add to Project</button>';

                        }} else {
                        echo '<button class="btn btn-primary btn-sm" onClick="getModel()" style="width: 100%; display:none;"><i class="fa fa-heart"></i> Add to Project</button>';
                    }
                    echo '</div>

                                    </div>
                                </a>
                            </div>';
                }
                ?>
            </div>
        </div>

    </div>
</section>
<section class="at-property-sec at-property-right-sidebar" id="faq" style="background-color: #f4f4f4">
    <div class="container" style="background-color: white; box-shadow: 0 3px 6px rgba(0,0,0,.16); padding: 30px;">
        <div class="row">
            <div class="col-md-12">
                <div class="at-sec-title">
                    <h3 style="text-transform: none;">Frequently Asked Questions</h3>
                    <div class="at-heading-under-line">
                        <div class="at-heading-inside-line"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="padding: 30px 0;">
            <div class="col-md-12 text-center">
                <?php if(customer('id') !== ''){ ?>
                    <button class="btn btn-primary" type="button" data-popup-open="popup-question" value="Ask a Question">Ask a Question</button>
                <?php } else { ?>
                    <button class="btn btn-primary" onClick="getModel()" type="button" value="Ask a Question">Ask a Question</button>
                <?php } ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul>
                    <?php foreach($faqs as $faq){?>
                        <li>
                            <input type="checkbox" checked>
                            <i></i>
                            <h5>Q : <?php echo $faq->question; ?><br><small>
                                    <?php $u = $faq->user_id;
                                    $user = DB::select("SELECT * FROM customers WHERE id = '$u'")[0];
                                    ?>
                                    by <?php echo $user->name; ?> on <?php $d = $faq->created; echo date('d M, Y', strtotime($d)); ?></small></h5>
                            <p>A : <?php echo $faq->answer; ?></p>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>

    </div>
</section>
<section style="padding-top: 30px; padding-bottom: 30px">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="at-sec-title">
                    <h3 style="text-transform: none;">Why Choose <span>Us</span></h3>
                    <div class="at-heading-under-line">
                        <div class="at-heading-inside-line"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="why_sec">
            <div class="faq_dtls apk">
                <div class="inr_wrk_dtls">
                    <div class="container">
                        <div class="row">
                            <div class="agent-carousel-2 slider" data-slick='{"slidesToShow": 4, "slidesToScroll": 1}'>
                                <div class="col-md-3 col-sm-3 ptb-30">
                                    <div class="apk_block">
                                        <div class="apk_block_img">
                                            <img src="<?php echo url('assets/images/ico1.png'); ?>" style="width: 80px;height: auto;margin: auto;"/>
                                        </div>
                                        <span>Value <br>Pricing</span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 ptb-30">
                                    <div class="apk_block">
                                        <div class="apk_block_img">
                                            <img src="<?php echo url('assets/images/ico2.png'); ?>" style="width: 80px;height: auto;margin: auto;"/>
                                        </div>
                                        <span>Tailor Made <br> Designs</span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 ptb-30">
                                    <div class="apk_block">
                                        <div class="apk_block_img">
                                            <i class="icofont icofont-safety" style="color: #888a8b; font-size: 80px;"></i>
                                        </div>
                                        <span>1 Year <br>Warranty</span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 ptb-30">
                                    <div class="apk_block no-brd">
                                        <div class="apk_block_img">
                                            <img src="<?php echo url('assets/images/ico3.png'); ?>" style="width: 80px;height: auto;margin: auto;"/>
                                        </div>
                                        <span>Dedicated project<br>Managers</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if(!empty($link)){?>
            <div class="row">
                <div class="blockquote blockquote--style-1">
                    <div class="row inner-div">
                        <div class="col-md-12">
                            <div class="col-md-3 col-lg-3 col-sm-3 col-xs-4">
                                <img src="<?=url('/assets/products/'.image_order($link->image))?>" class="cont-image"/>
                            </div>
                            <div class="col-md-7 col-lg-7 col-sm-7 col-xs-8" style="padding: 25px 0 0;">
                                <h3 class="cont-content"><?=$link->content; ?></h3>
                            </div>
                            <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12 text-center" style="padding:15px 0 0 0">
                                <a class="btn btn-primary" href="<?=$link->link; ?>" type="submit" style="padding: 10px auto !important;">
                                    GET STARTED
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</section>


<button class="btn btn-primary hidden" id="open-project-modal1" data-popup-open="popup-project">Get Started !</button>
<div class="popup" id="popup-project" data-popup="popup-project">
    <div class="popup-inner" style="padding-top: 120px;">
        <div class="col-lg-12 account" style="border: none; box-shadow: none;">
            <?=csrf_field();?>
            <label class="control-label">Select Your Project</label>
            <button type="button" name="create" class="btn btn-primary" onClick="getcreate()" style="float: right;"><i class="fa fa-plus"></i> Create a new project </button>
            <div id="projectName">

            </div>
        </div>
        <a class="popup-close" id="pclose" data-popup-close="popup-project" href="#">x</a>
    </div>
</div>


<?php echo $footer?>
<script>
    (function() {
        [].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {
            new CBPFWTabs( el );
        });
    })();

    $(function() {
//----- OPEN
        $('[data-popup-open]').on('click', function(e) {
            var targeted_popup_class = jQuery(this).attr('data-popup-open');
            $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
            e.preventDefault();
        });
//----- CLOSE
        $('[data-popup-close]').on('click', function(e) {
            var targeted_popup_class = jQuery(this).attr('data-popup-close');
            $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
            e.preventDefault();
        });
    });



    function getQuestions(service_id){
        event.preventDefault();
        $('#open-service-modal').click();
        $('.popup #data').html('<img class="loader-image" src="assets/images/que-loader.gif"/><p class="loader-label">Loading content...</p>');
        $.ajax({
            url: 'get-service-questions',
            type: 'post',
            data: 'service_id='+service_id+'&_token=<?=csrf_token(); ?>',
            success: function(data){
                $('.popup #data').html(data);
                reloadFormWizard();
            },
            error: function(){

            }
        });
    }

    function getModel(){
        event.preventDefault();
        $('#open-service-modal1').click();
    }
    function reloadFormWizard(){
        var navListItems = $('#data div.setup-panel div a'),
            allWells = $('#data .setup-content'),
            allNextBtn = $('#data .nextBtn');
        allPrevBtn = $('#data .prevBtn');
        allWells.hide();
        var ajax_valid = true;
        navListItems.click(function (e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                $item = $(this);
            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-success').addClass('btn-default');
                $item.addClass('btn-success');
                //allWells.hide('slide', {direction: 'left'}, 1000);
                //allWells.hide();
                allWells.hide();
                $target.show('slide', {direction: 'right'}, 1000);

                //$target.find('input:eq(0)').focus();

            }
        });

        allNextBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('#data div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                curInputs = curStep.find("input[type='text'],input[type='url'],input[type='tel'],select,input[type='radio'],input[type='checkbox'],textarea"),
                isValid = true;
            if(ajax_valid){
                $("#data .form-group").removeClass("has-error");
                for (var i = 0; i < curInputs.length; i++) {

                    if (!curInputs[i].validity.valid) {
                        isValid = false;
                        $(curInputs[i]).closest(".form-group").addClass("has-error");
                    }
                }
            }else{
                isValid = false;
            }

            if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');
        });
        allPrevBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('#data div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a"),
                curInputs = curStep.find("input[type='text'],input[type='url'],input[type='tel'],select,input[type='radio'],input[type='checkbox'],textarea"),
                isValid = true;
            allWells.hide();
            curStep.prev().show('slide', {direction: 'left'}, 1000);
        });

        $('#data div.setup-panel div a.btn-success').trigger('click');
        $('#data .pincode').on('keyup', function(){
            var csrf_token = '<?=csrf_token();?>';
            var pin = $(this).val();
            $.ajax({
                url: 'getlocality',
                type: 'post',
                async:false,
                data: 'pincode='+pin+'&_token='+csrf_token,
                success:function(data){
                    if(data !== null){
                        $('#data .location').html(data);
                        if(data == 'Pincode Not Available' || data == 'Invalid Pincode'){
                            $("#data .form-group").addClass("has-error");
                            ajax_valid = false;
                        }else{
                            $("#data .form-group").removeClass("has-error");
                            ajax_valid = true;
                        }
                    }else{
                        $('#data .location').html('Pincode');
                        $("#data .form-group").addClass("has-error");
                        ajax_valid = false;
                    }
                },
                error: function(){
                    $('#data .location').html('Pincode Not Available');
                    $("#data .form-group").addClass("has-error");
                    ajax_valid = false;
                }
            })
        });
        $('#data .select2').select2({
            placeholder: "Please Select Option...",
            allowClear: true,
            width: '100%'
        });
    }
</script>




<!-- Category Popup Start -->
<div class="popup" data-popup="popup-1">
    <div class="popup-inner">
        <?php if($questions) {  ?>
            <div class="stepwizard">
                <div class="stepwizard-row setup-panel">
                    <div class="stepwizard-step col-xs-3">
                        <a href="#step-0" type="button" class="btn btn-success btn-circle">0</a>
                    </div>
                    <?php
                    $sr = 1;
                    foreach($questions as $question){ ?>
                        <div class="stepwizard-step col-xs-3">
                            <a href="#step-<?=$sr;?>" type="button" class="btn btn-default btn-circle"><?=$sr; ?></a>
                        </div>
                        <?php $sr++; } ?>
                </div>
            </div>
            <form role="form">
                <div class="panel panel-primary setup-content" id="step-0">
                    <div class="panel-body" style="padding:40px 20px 10px 20px;min-height: 350px;">
                        <div class="form-group">
                            <h2 class="text-center" style="text-transform: none;">What is the location of your Project?</h2>
                            <hr style="position: relative; max-width: 100px; margin:40px auto;">
                            <center><i class="fa fa-map-marker" style="font-size:48px;"></i><br><label class="location">Pincode</label><br><input maxlength="6" minlength="6" type="text"  required="required" class="form-control pincode" style="max-width: 200px;margin:20px 0;" placeholder="Enter Location" /></center>
                        </div>
                        <div style="padding: 20px 0;text-align:center">
                            <hr>
                            <button class="btn btn-primary prevBtn" disabled type="button">Previous</button>
                            <button class="btn btn-primary nextBtn" type="button">Next</button>
                        </div>
                    </div>
                </div>
                <?php $sr = 1;
                foreach($questions as $question){ ?>
                    <div class="panel panel-primary setup-content" id="step-<?=$sr; ?>">
                        <div class="panel-body" style="padding:40px 20px 10px 20px;min-height: 350px;">
                            <div class="form-group">
                                <h2 class="text-center" style="text-transform: none;"><?php echo getQuestion($question->question_id); ?></h2>
                                <hr style="position: relative; max-width: 100px; margin:40px auto;">
                                <?php
                                $type = getQuestionType($question->question_id);
                                if($type == 'text'){
                                    ?>
                                    <center><i class="fa fa-map-marker" style="font-size:48px;"></i><br><label class="location">Pincode</label><br><input maxlength="6" minlength="6" type="text"  required="required" class="form-control pincode" style="max-width: 200px;margin:20px 0;" placeholder="Enter Location" /></center>
                                <?php }else if($type == 'single'){
                                    echo '<div style="position:relative; max-width: 350px;margin:auto;">';
                                    $options = json_decode(getQuestionOptions($question->question_id));
                                    $i = 0;
                                    foreach($options as $option){
                                        ?>
                                        <input type="radio" name="answer[]" value="<?=$option;?>" style="margin-right:10px;" id="<?=$sr.$i;?>"></input> <label class="label-hover" for="<?=$sr.$i;?>" style="cursor: pointer;""><?=$option; ?></label><br>
                                        <?php $i++; } echo '</div>'; }
                                else if($type == 'multiple'){
                                    echo '<div style="position:relative; max-width: 350px;margin:auto;">';
                                    $options = json_decode(getQuestionOptions($question->question_id));
                                    $i = 0;
                                    foreach($options as $option){
                                        ?>
                                        <input type="checkbox" name="answer[]" value="<?=$option;?>" style="margin-right:10px;" id="<?=$sr.$i;?>"></input> <label class="label-hover" for="<?=$sr.$i;?>" style="cursor: pointer;""><?=$option; ?></label><br>
                                        <?php $i++; } echo '</div>'; }
                                else if($type == 'textarea'){
                                    echo '<div style="position:relative;width: 60%;margin:auto;">';
                                    ?>
                                    <textarea name="answer[]" style="margin-right:10px;" rows="7" class="form-control" placeholder="Enter a description..."></textarea>
                                    <?php echo '</div>'; }
                                else if($type == 'dropdown'){
                                    echo '<div style="position:relative; max-width: 350px;margin:auto;">
													<select class="form-control select2" name="answer[]">';
                                    $options = json_decode(getQuestionOptions($question->question_id));
                                    $i = 0;
                                    foreach($options as $option){
                                        ?>
                                        <option value="<?=$option;?>"><?=$option; ?></option>
                                        <?php $i++; } echo '</select></div>'; } ?>
                            </div>
                            <div style="padding: 20px 0;text-align:center">
                                <hr>
                                <button class="btn btn-primary prevBtn" <?=($sr == 0) ? 'disabled' : ''; ?> type="button">Previous</button>
                                <?php if($sr != count($questions)){ ?>
                                    <button class="btn btn-primary nextBtn" type="button">Next</button>
                                <?php }else{ ?>
                                    <button class="btn btn-primary" type="submit">Finish</button>
                                <?php } ?>
                            </div>
                        </div>

                    </div>
                    <?php $sr++; } ?>
            </form>
        <?php }else{
            echo 'Ooops...! Something Went Wrong. Please try again later';
        } ?>
        <a class="popup-close" data-popup-close="popup-1" href="#">x</a>
    </div>
</div>
<!-- Category Popup End -->


<!-- Services Popup Start -->
<a class="btn btn-primary hidden" id="open-service-modal1" data-popup-open="popup-login" href="#">Get Started !</a>
<div class="popup" id="popup-login" data-popup="popup-login">
    <div class="popup-inner" style="padding-top: 120px;">
        <div class="col-md-6 account" style="border: none; box-shadow: none;">
            <?php
            if (session('error') != ''){
                echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
            }
            ?>
            <form action="<?php echo url('login-model'); ?>" method="post" class="form-horizontal single">
                <?=csrf_field() ?>
                <input type="hidden" name="ser_id" value="<?php echo $category_id;?>">
                <fieldset>
                    <div class="form-group">
                        <label class="control-label"><?=translate('E-mail') ?></label>
                        <input name="email" type="email" value="<?=isset($_POST['email']) ? $_POST['email'] : '' ?>" class="form-control"  />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?=translate('Password') ?></label>
                        <input name="password" type="password" class="form-control"  />
                    </div>
                    <input name="login" type="submit" value="<?=translate('Login') ?>" class="btn btn-primary" />
                </fieldset>
            </form>
            <div class="text-center"><a class="smooth" href="<?=url('reset-password')?>">Forgot your password ?</a></div>
        </div>
        <a class="popup-close" data-popup-close="popup-login" href="#">x</a>
    </div>
</div>
<!-- Services Popup End -->




<!-- Services Popup Start -->
<a class="btn btn-primary hidden" id="open-service-modal" data-popup-open="popup-service" href="#">Get Started !</a>
<div class="popup" id="popup-service" data-popup="popup-service">
    <div class="popup-inner">
        <div id="data">

        </div>
        <a class="popup-close" data-popup-close="popup-service" href="#">x</a>
    </div>
</div>
<!-- Services Popup End -->

<!-- Ask a Question Popup Start -->
<div class="popup" id="popup-question" data-popup="popup-question">
    <div class="popup-inner">
        <div class="col-md-12">
            <h2 style="text-transform: none;" class="text-center">Ask a Question</h2>
            <form id="contact_form" action="<?php echo url('faq'); ?>" method="post">
                <?=csrf_field() ?>
                <input type="hidden" name="ser_id" value="<?php echo $category_id;?>">
                <div class="col-md-12 col-sm-12 text-center">
                    <textarea class="form-control" name="question" rows="5" placeholder="Write question here" required="required"></textarea>
                    <br>
                    <button class="btn btn-primary" type="submit">SUBMIT</button>
                </div>
            </form>
        </div>
        <a class="popup-close" data-popup-close="popup-question" href="#">x</a>
    </div>
</div>
<!-- Ask a Question Popup End -->

<a class="btn btn-primary hidden" id="project-modal2" data-popup-open="popup-project2" href="#">Get Started !</a>
<div class="popup" id="popup-project2" data-popup="popup-project2">
    <div class="popup-inner" style="padding-top: 120px;">
        <div class="col-md-10 account" style="border: none; box-shadow: none;">
            <?php
            if (session('error') != ''){
                echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
            }
            ?>
            <form action="<?php echo url('create-project'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal single">
                <table class="responsive-table bordered" style="width: 50%; margin: auto;">
                    <tbody>
                    <input type="hidden" name="user_id" value="<?=customer('id'); ?>">
                    <tr>
                        <td>Project Title</td>
                        <td>:</td>
                        <td><input type="text" name="title" required class="form-control"></td>
                    </tr>
                    <tr>
                        <td>Project Image</td>
                        <td>:</td>
                        <td><input type="file" name="image" required class="form-control"></td>
                    </tr>
                    <?php echo csrf_field(); ?>
                    <tr>
                        <td colspan="3"><input type="submit" name="submit" value="Update" class="btn btn-primary btn-lg"></td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <a class="popup-close" data-popup-close="popup-project2" href="#">x</a>
    </div>
</div>


<script>
    $(document).ready(function () {

        var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');
        allPrevBtn = $('.prevBtn');

        allWells.hide();
        var ajax_valid = true;
        navListItems.click(function (e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                $item = $(this);

            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-success').addClass('btn-default');
                $item.addClass('btn-success');
                //allWells.hide('slide', {direction: 'left'}, 1000);
                //allWells.hide();
                allWells.hide();
                $target.show('slide', {direction: 'right'}, 1000);

                //$target.find('input:eq(0)').focus();

            }
        });

        allNextBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                curInputs = curStep.find("input[type='text'],input[type='url'],input[type='tel'],select,input[type='radio'],input[type='checkbox'],textarea"),
                isValid = true;
            if(ajax_valid){
                $(".form-group").removeClass("has-error");
                for (var i = 0; i < curInputs.length; i++) {
                    if (!curInputs[i].validity.valid) {
                        isValid = false;
                        $(curInputs[i]).closest(".form-group").addClass("has-error");
                    }
                }
            }else{
                isValid = false;
            }

            if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');
        });
        allPrevBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a"),
                curInputs = curStep.find("input[type='text'],input[type='url'],input[type='tel'],select,input[type='radio'],input[type='checkbox'],textarea"),
                isValid = true;
            allWells.hide();
            curStep.prev().show('slide', {direction: 'left'}, 1000);
        });

        $('div.setup-panel div a.btn-success').trigger('click');
        $('.pincode').on('keyup', function(){
            var csrf_token = '<?=csrf_token();?>';
            var pin = $(this).val();
            $.ajax({
                url: 'getlocality',
                type: 'post',
                async:false,
                data: 'pincode='+pin+'&_token='+csrf_token,
                success:function(data){
                    if(data !== null){
                        $('.location').html(data);
                        if(data == 'Pincode Not Available' || data == 'Invalid Pincode'){
                            $(".form-group").addClass("has-error");
                            ajax_valid = false;
                        }else{
                            $(".form-group").removeClass("has-error");
                            ajax_valid = true;
                        }
                    }else{
                        $('.location').html('Pincode');
                        $(".form-group").addClass("has-error");
                        ajax_valid = false;
                    }
                },
                error: function(){
                    $('.location').html('Pincode Not Available');
                    $(".form-group").addClass("has-error");
                    ajax_valid = false;
                }
            })
        });
        $('.select2').select2({
            placeholder: "Please Select Option...",
            allowClear: true,
            width: '100%'
        });
    });
</script>
<script>
    $('.likebutton1').click( function () {
        var elel =$(this);
        $.ajax({
            type: "POST",
            url: "service-wishlist",
            data:'_token=<?=csrf_token();?>',
            success: function(data){
                //alert('hello');
                $('#open-project-modal1').click();
                $('#projectName').html(data);
                elel.html('<span class="tickgreen">✔</span> Added to Project');
                elel.unbind();
            }
        });
    });


    function addSerProject(val) {
        var ele =$(this);
        var project_id = val;
        $.ajax({
            type: "POST",
            url: "add-ser-project",
            data:'_token=<?=csrf_token();?>&service_id=<?=$category_id; ?>&project_id='+project_id,
            success: function(data){
                $('#pclose').click();
                ele.unbind();
            }
        });
    }


</script>

<script>

    function addProject(val) {
        var ele =$(this);
        var proid = $('#ghanta').data('id');
        var project_id = val;
        $.ajax({
            type: "POST",
            url: "add-sub-ser-project",
            data:'_token=<?=csrf_token();?>&sub_service_id='+proid+'&project_id='+project_id,
            success: function(data){

                $('#pclose').click();
                ele.unbind();
            }
        });
    }

    $('.likebutton').click( function () {
        var ele =$(this);
        var proid = ele.siblings('.pid1').val();
        $.ajax({
            type: "POST",
            url: "sub-service-wishlist",
            data:'_token=<?=csrf_token();?>&image_id='+proid,
            success: function(data){
                //alert('hello');
                $('#open-project-modal1').click();
                $('#projectName').html(data);
                ele.html('<span class="tickgreen">✔</span> Added to Project');
                ele.unbind();
            }
        });
    });

    function getcreate(){
        event.preventDefault();
        $('#project-modal2').click();
    }
</script>