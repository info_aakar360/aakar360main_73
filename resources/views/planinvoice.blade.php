<?php echo $header?>
    <div class="profile-root bg-white ng-scope">
        <div class="clearfix buffer-top text-center wrapper-1400 bg-color">
            <div class="clearfix wrapper-1140 two-bgs">
                <div class="col-sm-12">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/banners/0e94f355317086ae791f78e1e851d4fb.jpg" class="cover">
                    </div>
                </div>
            </div>
            <div class="profile-overlay clearfix wrapper-980 bgs-white ng-isolate-scope" style="width: 980px;">
                <img itemprop="image" class="profile-icon" src="assets/invoice.png">
                <h1 itemprop="name" class="name hand-writing font-44 font-36-xs">
                    <a itemprop="url" class="url">
                        <?=translate('Invoice')?>
                    </a>
                </h1>
            </div>
        </div>
        <div class="clearfix wrapper-980 bg-white m-t-xs m-b-md">
            <div class="about-designer clearfix">
                <div class="text-left" style="padding-top: 50px;">
                    <div class="sdb-tabl-com sdb-pro-table">
                        <div class="udb-sec udb-prof">
                            <div class="col-md-8">
                                <div class="list">
                                    <div class="title">
                                        <a href="plan_orders"><i class="icon-arrow-left"></i> List</a>
                                    </div>
                                    <div class="title">
                                        <i class="icon-user"></i> Customer details
                                    </div>
                                    <div class="item"><h6><b>Order ID</b> : #<?=$order->id?></h6></div>
                                    <div class="item"><h6><b>Order time</b> : <?=$order->created; ?></h6></div>
                                    <div class="item order">
                                        <h6><b>Shipping Address</b> : </h6>
                                        <p>
                                            <?php $data = json_decode($order->address);

                                            echo '<b>Name : </b> '.$data->shipping_fname.'<br>';
                                            echo '<b>Company : </b> '.$data->shipping_company.'<br>';
                                            echo '<b>Address Line 1 : </b> '.$data->shipping_address_line_1.'<br>';
                                            echo '<b>Address Line 2 : </b> '.$data->shipping_address_line_2.'<br>';
                                            echo '<b>Country : </b> '.getCountryName($data->shipping_country).'<br>';
                                            echo '<b>State : </b> '.getStateName($data->shipping_state).'<br>';
                                            echo '<b>City : </b> '.getCityName($data->shipping_city).'<br>';
                                            echo '<b>Pincode : </b> '.$data->shipping_postcode.'<br>';
                                            echo '<b>Contact No. : </b> '.$data->shipping_mobile.'<br>';
                                            echo '<b>Alternate Contact No. : </b> '.$data->shipping_alternate_contact.'<br>';?>
                                        </p>
                                    </div>
                                    <br/>
                                </div>
                                <div class="list">
                                    <div class="title">
                                        <i class="icon-basket"></i> Plan Details
                                    </div>
                                    <?php
                                    $layout_id = $order->plan_id;
                                    $addons = $order->addon_id;
                                    $package = $order->package_id;
                                    $totalprice = $order->package_price;?>
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>Plan Name</th>
                                                <th>Package Name</th>
                                                <th>Addons Detail</th>
                                                <th class="text-right">Package Price</th>
                                                <th class="text-right">Addons Price</th>
                                                <th class="text-right">Total Price</th>
                                                <th class="text-right">Tax(%)</th>
                                                <th class="text-right">Tax Amount</th>
                                                <th class="text-right">Total Amount</th>
                                            </tr>
                                            </thead>
                                            <?php $serv = DB::select("SELECT * FROM layout_plans WHERE id = '".$layout_id."'")[0];
                                            $pack = '';
                                            $pprice = '';
                                            if($package == '1'){
                                                $pack = $serv->p1_name;
                                                $pprice = $serv->p1_price;
                                            }elseif($package == '2'){
                                                $pack = $serv->p2_name;
                                                $pprice = $serv->p2_price;
                                            }elseif($package == '3'){
                                                $pack = $serv->p3_name;
                                                $pprice = $serv->p3_price;
                                            }

                                            $aprice = $totalprice - $pprice;
                                            $tax = 0;
                                            $taxamt= 0;
                                            $totaladonprice = 0;
                                            $taxprice = 0; ?>
                                            <tr>
                                                <td><?php echo $serv->title; ?></td>
                                                <td><?php echo $pack; ?></td>
                                                <td>
                                                    <?php $ad = explode(',', $addons);
                                                    foreach ($ad as $a){
                                                        $as = DB::select("SELECT * FROM layout_addon WHERE id = '".$a."'")[0];
                                                        echo $as->name.' - '.c($as->price).'<br>';
                                                        $totaladonprice += $as->price;
                                                    } ?>
                                                </td>
                                                <td class="text-right"><?php echo c($pprice); ?></td>
                                                <td class="text-right"><?php echo c($totaladonprice);
                                                    if($serv->tax !== null) {
                                                        $tax = $serv->tax;
                                                        $taxa = 1 + ($tax / 100);
                                                        $taxprice = ($pprice + $totaladonprice) - ($pprice + $totaladonprice)/$taxa;

                                                    } else { $tprice = $totalprice; } ?>
                                                </td>
                                                <td class="text-right"><?php echo c(($pprice + $totaladonprice)); ?></td>
                                                <td class="text-right"><?php echo $tax; ?></td>
                                                <td class="text-right"><?php echo c(number_format($taxprice, 2, '.', '')); ?></td>
                                                <td class="text-right"><?php echo c(($pprice + $totaladonprice)); ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="list">
                                    <div class="title">
                                        <i class="icon-credit-card"></i> Payment details
                                    </div>
                                    <?php
                                    $payment_data = json_decode($order->payment_method,true); ?>
                                    <div class="alert alert-warning" style="margin-bottom: 0;">Order unpaid</div>
                                    <?php if ($payment_data['method'] == 'paypal') { ?>
                                        <div class="item order"><h6><b>Payment method : </b>PayPal</h6></div>
                                        <div class="item order"><h6><b>Payment status : </b><?php echo $payment_data['payment_status']; ?></h6></div>
                                        <div class="item order"><h6><b>Transcation ID : </b><?php echo $payment_data['txn_id']; ?></h6></div>
                                        <div class="item order"><h6><b>Receiver e-mail : </b><?php echo $payment_data['receiver_email']; ?></h6></div>
                                        <div class="item order"><h6><b>Payer e-mail : </b><?php echo $payment_data['payer_email']; ?></h6></div>
                                        <div class="item order"><h6><b>Payment amount : </b><?php echo $payment_data['payment_amount']; ?></h6></div>
                                        <div class="item order"><h6><b>Payment currency : </b><?php echo $payment_data['payment_currency']; ?></h6></div>
                                    <?php } elseif ($payment_data['method'] == 'stripe') { ?>
                                        <div class="item order"><h6><b>Payment method : </b>Stripe</h6></div>
                                        <div class="item order"><h6><b>Charge ID : </b><?php echo $payment_data['charge_id']; ?></h6></div>
                                        <div class="item order"><h6><b>Balance transaction : </b><?php echo $payment_data['balance_transaction']; ?></h6></div>
                                        <div class="item order"><h6><b>Card ID : </b><?php echo $payment_data['card']; ?></h6></div>
                                        <div class="item order"><h6><b>Card brand : </b><?php echo $payment_data['card_brand']; ?></h6></div>
                                        <div class="item order"><h6><b>Last 4 : </b><?php echo $payment_data['last4']; ?></h6></div>
                                        <div class="item order"><h6><b>Expiry month : </b><?php echo $payment_data['exp_month']; ?></h6></div>
                                        <div class="item order"><h6><b>Expiry year : </b><?php echo $payment_data['exp_year']; ?></h6></div>
                                        <div class="item order"><h6><b>Fingerprint : </b><?php echo $payment_data['fingerprint']; ?></h6></div>';
                                    <?php } elseif ($payment_data['method'] == 'cash') { ?>
                                        <div class="item order"><h6><b>Payment method : </b>Cash on delivery</h6></div>
                                    <?php } elseif ($payment_data['method'] == 'bank') { ?>
                                        <div class="item order"><h6><b>Payment method : </b>Bank Transfer</h6></div>
                                    <?php }?>
                                </div>
                                <div class="list">
                                    <div class="title">
                                        <i class="icon-badge"></i>Order status
                                    </div>
                                    <div class="item text-center"><br/><h6><?=$order->status;?></h6><br/></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php echo $footer?>