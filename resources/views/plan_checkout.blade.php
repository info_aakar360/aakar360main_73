<?php echo $header?>

    <div class="profile-root bg-white ng-scope">
        <div class="clearfix buffer-top text-center wrapper-1400 bg-color">
            <div class="clearfix wrapper-1140 two-bgs">
                <div class="col-sm-12">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/banners/<?=$dimg->image; ?>" class="cover">
                    </div>
                </div>
            </div>
            <div class="profile-overlay clearfix wrapper-980 bgs-white ng-isolate-scope" style="width: 980px;">
                <img itemprop="image" class="profile-icon" src="assets/checkout.png">
                <h1 itemprop="name" class="name hand-writing font-44 font-36-xs">
                    <a itemprop="url" class="url">
                        <?=translate('Checkout')?>
                    </a>
                </h1>
            </div>
        </div>
        <div class="clearfix wrapper-980 bg-white m-t-xs m-b-md">
            <div class="about-designer clearfix">
                <div class="text-left" style="padding-top: 0px;">
                    <div id="content" class="container" style="margin-left: -17px; width: 1145px;">
                        <!--div id="cart-header">
                            Enter information to process your order
                        </div-->
                        <div id="cart-content" style="padding-top: 5px;">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseCheckout" class="restrict" style="color: #ffffff;">
                                            <h4 class="panel-title">
                                                Checkout Method
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="collapseCheckout" class="panel-collapse collapse <?=(session('customer') == '')? 'in' : ''; ?>">
                                        <div class="panel-body">
                                            <div class="col-md-12">
                                                <div class="col-md-6">
                                                    <h4>CHECKOUT AS A GUEST OR REGISTER</h4>
                                                    <p>
                                                        Register with us for future convenience:
                                                    </p>
                                                    <p>
                                                        <form name="checkout_options">
                                                            <p>
                                                                <label for="guest">
                                                                    <input type="radio" name="options" value="guest" id="guest" checked="true"/> Checkout as Guest
                                                                </label>
                                                            </p>
                                                            <p>
                                                                <label for="register">
                                                                    <input type="radio" name="options" value="register" id="register"/> Register
                                                                </label>
                                                            </p>
                                                        </form>
                                                    </p>
                                                    <p>
                                                        Register and save time!
                                                    </p>
                                                        <input name="register" type="button" id="continueBtn" value="<?=translate('Continue') ?>" class="btn btn-primary" />
                                                </div>
                                                <div class="col-md-6">
                                                    <h4>LOGIN</h4>
                                                    <div class="col-md-8">
                                                        <?php if(session('customer') == ''){ ?>
                                                            <form action="" method="post" id="login" class="form-horizontal single">
                                                                <?=csrf_field() ?>
                                                                <fieldset>
                                                                    <div class="form-group">
                                                                        <label class="control-label"><?=translate('E-mail') ?></label>
                                                                        <input name="email" type="email" value="<?=isset($_POST['email']) ? $_POST['email'] : '' ?>" class="form-control"  />
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label"><?=translate('Password') ?></label>
                                                                        <input name="password" type="password" class="form-control"  />
                                                                    </div>
                                                                    <a class="smooth pull-left" href="<?=url('reset-password')?>">Forgot your password ?</a>
                                                                    <input name="login" type="submit" id="loginBtn" value="<?=translate('Login') ?>" class="btn btn-primary pull-right" />
                                                                </fieldset>
                                                            </form>
                                                        <?php } else{ ?>
                                                            <p>You are already logged in as <br><b><?=customer('name'); ?></b></p>
                                                            <p>
                                                                <input name="logout" type="button" id="logoutBtn" value="<?=translate('Logout') ?>" class="btn btn-primary" />
                                                            </p>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseBilling" class="restrict" style="color: #ffffff;">
                                            <h4 class="panel-title">
                                                Billing Information
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="collapseBilling" class="panel-collapse collapse <?=(session('customer') == '')? '' : 'in'; ?>">
                                        <div class="panel-body">
                                            <p class="formP">
                                            </p>
                                            <input type="button" id="goToShipping" class="btn btn-primary pull-right" value="Continue"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseShipping" class="restrict" style="color: #ffffff;"><h4 class="panel-title">
                                                Shipping Information
                                            </h4></a>
                                    </div>
                                    <div id="collapseShipping" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p class="formS">
                                            </p>
                                            <input type="button" id="goToSM" class="btn btn-primary pull-right" value="Continue"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseSM" class="restrict" style="color: #ffffff;"><h4 class="panel-title">
                                                Shipping Method
                                            </h4></a>
                                    </div>
                                    <div id="collapseSM" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p class="formSM">
                                            </p>
                                            <input type="button" id="goToPM" class="btn btn-primary pull-right" value="Continue"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapsePI" class="restrict" style="color: #ffffff;"><h4 class="panel-title">
                                                Payment Information
                                            </h4></a>
                                    </div>
                                    <div id="collapsePI" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p class="formPI">
                                            </p>
                                            <input type="button" id="goToOrder" class="btn btn-primary pull-right" value="Continue"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOrder" class="restrict" style="color: #ffffff;"><h4 class="panel-title">
                                                Order Review
                                            </h4></a>
                                    </div>
                                    <div id="collapseOrder" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p class="formOrder">
                                            </p>
                                            <input type="button" id="confirmOrder" class="btn btn-primary pull-right" value="Confirm"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



	
<?php echo $footer?>
<?php if(session('customer') != ''){ ?>
    <script>
        function show2(){
            document.getElementById('div1').style.display = 'block';
        }
        $.ajax({
            url: 'api/plan-billing',
            type: 'post',
            data: 'type=loggedin',
            success: function(data){
                $('#collapseBilling .formP').html(data);
                $('#collapseCheckout').removeClass('in');
                $('#collapseCheckout').attr('area-expanded', 'false');
                $('a[href="#collapseCheckout"]').removeClass('restrict');
                $('a[href="#collapseCheckout"]').unbind('click');
                $('a[href="#collapseBilling"]').removeClass('restrict');
                $('a[href="#collapseBilling"]').unbind('click');
                $('#collapseBilling').addClass('in');
                $('#collapseBilling').attr('area-expanded', 'true');
                $('#collapseBilling').attr('style', '');
            }
        });
    </script>
<?php } ?>
<script>

</script><?php if(session('customer') != ''){ ?>
    <script>
        $.ajax({
            url: 'api/plan-billing',
            type: 'post',
            data: 'type=loggedin',
            success: function(data){
                $('#collapseBilling .formP').html(data);
                $('#collapseCheckout').removeClass('in');
                $('#collapseCheckout').attr('area-expanded', 'false');
                $('a[href="#collapseCheckout"]').removeClass('restrict');
                $('a[href="#collapseCheckout"]').unbind('click');
                $('a[href="#collapseBilling"]').removeClass('restrict');
                $('a[href="#collapseBilling"]').unbind('click');
                $('#collapseBilling').addClass('in');
                $('#collapseBilling').attr('area-expanded', 'true');
                $('#collapseBilling').attr('style', '');
            }
        });
    </script>
<?php } ?>
<script>
    $('.restrict').on('click', function(){
        return false;
    });
    $("form#login").submit(function(e){
        e.preventDefault();
        var formData = $("form#login").serialize();
        $.ajax({
            url: 'api/login',
            type: 'post',
            data: formData+'&login=true',
            beforeSend: function(){
                $('#loginBtn').val('Logging in...');
                $('#login .alert').remove();
            },
            success: function(datax){
                data = JSON.parse(datax);
                if(data.success == null){
                    $('#login').prepend('<div class="alert alert-danger">'+data.error+'</div>');
                }else{
                    $('#login').prepend('<div class="alert alert-success">'+data.success+'</div>');
                    location.reload();
                }
            },
            complete: function(){
                $('#loginBtn').val('Login');
            }
        });
    });
    $('#continueBtn').on('click', function(){
        var target = $('input[name="options"]:checked').val();
        if(target == null){
            alert('Please select any one option');
        }else{
            $.ajax({
                url: 'api/plan-billing',
                type: 'post',
                data: 'type='+target,
                success: function(data){
                    $('#collapseBilling .formP').html(data);
                    $('#collapseCheckout').removeClass('in');
                    $('#collapseCheckout').attr('area-expanded', 'false');
                    $('a[href="#collapseCheckout"]').removeClass('restrict');
                    $('a[href="#collapseCheckout"]').unbind('click');
                    $('a[href="#collapseBilling"]').removeClass('restrict');
                    $('a[href="#collapseBilling"]').unbind('click');
                    $('#collapseBilling').addClass('in');
                    $('#collapseBilling').attr('area-expanded', 'true');
                    $('#collapseBilling').attr('style', '');
                }
            });
        }
    });
    $('#goToShipping').on('click', function(){
        var target = $('input[name="shipping_address_option"]:checked').val();
        if(target == null){
            alert('Please select any one option');
        }else{
            $.ajax({
                url: 'api/plan-shipping',
                type: 'post',
                data: 'type='+target,
                beforeSend: function(){
                    $('#goToShipping').attr('value', 'Loading...');
                },
                success: function(data){
                    if(target == 'same'){
                        $('#collapseShipping .formS').html(data);
                        $('#collapseBilling').removeClass('in');
                        $('#collapseBilling').attr('area-expanded', 'false');
                        $('a[href="#collapseBilling"]').removeClass('restrict');
                        $('a[href="#collapseBilling"]').unbind('click');
                        $('a[href="#collapseShipping"]').removeClass('restrict');
                        $('a[href="#collapseShipping"]').unbind('click');
                        $('#goToSM').trigger('click');
                    }else {
                        $('#collapseShipping .formS').html(data);
                        $('#collapseBilling').removeClass('in');
                        $('#collapseBilling').attr('area-expanded', 'false');
                        $('a[href="#collapseBilling"]').removeClass('restrict');
                        $('a[href="#collapseBilling"]').unbind('click');
                        $('a[href="#collapseShipping"]').removeClass('restrict');
                        $('a[href="#collapseShipping"]').unbind('click');
                        $('#collapseShipping').addClass('in');
                        $('#collapseShipping').attr('area-expanded', 'true');
                        $('#collapseShipping').attr('style', '');
                    }
                },
                complete: function(){
                    $('#goToShipping').attr('value', 'Continue');
                }
            });
        }
    });
    $('#goToSM').on('click', function(){
        var formData = $('.formS form').serialize();
        $.ajax({
            url: 'api/plan-shipping-method',
            type: 'post',
            data: formData,
            beforeSend: function(){
                $('#goToSM').attr('value', 'Loading...');
            },
            success: function(data){
                $('#collapseShipping').removeClass('in');
                $('#collapseShipping').attr('area-expanded', 'false');
                $('a[href="#collapseShipping"]').removeClass('restrict');
                $('a[href="#collapseShipping"]').unbind('click');
                $('a[href="#collapseSM"]').removeClass('restrict');
                $('a[href="#collapseSM"]').unbind('click');
                $('#collapseSM').addClass('in');
                $('#collapseSM').attr('area-expanded', 'true');
                $('#collapseSM').attr('style', '');
                $('.formSM').html(data);
            },
            complete: function(){
                $('#goToSM').attr('value', 'Continue');
            }
        });
    });
    $('#goToPM').on('click', function(){
        var formData = $('.formSM form').serialize();
        $.ajax({
            url: 'api/plan-payment-method',
            type: 'post',
            data: formData,
            beforeSend: function(){
                $('#goToPM').attr('value', 'Loading...');
            },
            success: function(data){
                $('#collapseSM').removeClass('in');
                $('#collapseSM').attr('area-expanded', 'false');
                $('a[href="#collapseSM"]').removeClass('restrict');
                $('a[href="#collapseSM"]').unbind('click');
                $('a[href="#collapsePI"]').removeClass('restrict');
                $('a[href="#collapsePI"]').unbind('click');
                $('#collapsePI').addClass('in');
                $('#collapsePI').attr('area-expanded', 'true');
                $('#collapsePI').attr('style', '');
                $('.formPI').html(data);
            },
            complete: function(){
                $('#goToPM').attr('value', 'Continue');
            }
        });
    });
    $('#goToOrder').on('click', function(){
        var formData = $('.formPI form').serialize();
        $.ajax({
            url: 'api/plan-order-confirm',
            type: 'post',
            data: formData,
            beforeSend: function(){
                $('#goToOrder').attr('value', 'Loading...');
            },
            success: function(data){
                $('#collapsePI').removeClass('in');
                $('#collapsePI').attr('area-expanded', 'false');
                $('a[href="#collapsePI"]').removeClass('restrict');
                $('a[href="#collapsePI"]').unbind('click');
                $('a[href="#collapseOrder"]').removeClass('restrict');
                $('a[href="#collapseOrder"]').unbind('click');
                $('#collapseOrder').addClass('in');
                $('#collapseOrder').attr('area-expanded', 'true');
                $('#collapseOrder').attr('style', '');
                $('.formOrder').html(data);
            },
            complete: function(){
                $('#goToOrder').attr('value', 'Continue');
            }
        });
    });
    $('#confirmOrder').on('click', function(){
        $.ajax({
            url: 'api/plan-confirm',
            type: 'POST',
            success: function(data){
                if(data == 'success'){
                    window.location.replace('<?php echo url('success'); ?>');
                }else{
                    window.location.replace('<?php echo url('failed'); ?>');
                }
            }
        });
    });
    $('#logoutBtn').on('click', function(){
        $.ajax({
            url: 'api/logout',
            type: 'post',
            success: function(data){
                location.reload();
            }
        });
    });
</script>