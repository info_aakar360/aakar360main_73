<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="keywords" content="">
    <meta name="author" content="ThemeSelect">
    <!--    <title>-->
    <!--        --><?//=$title; ?>
    <!--    </title>-->
    <link rel="apple-touch-icon" href="<?=$data['tp']; ?>/admin/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$data['tp']; ?>/admin/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/vendors/data-tables/css/select.dataTables.min.css">

    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/custom/custom.css">
    <link href="<?=$data['tp']; ?>/admin/css/select2.min.css" rel="stylesheet" />
    <!-- END: Custom CSS-->
</head>
<?php
echo $data['header'];
if(isset($_GET['edit'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
             <h5>Edit Home Page Design</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="home-page-design" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
            <form action="" method="post" enctype="multipart/form-data" style="max-width: 100%;">
			'.csrf_field().'
			
				<div class="row">				
				<div class="input-field col s12 m6">						
				Title						
				<input type="text" name="title" class="form-control" value="'.$data['design']->title.'"/>			
						</div></div>
						<div class="row">
					 <div class="input-field col s12 m12">
						Text
						<textarea name="text" id="editor"  class="form-control">'.$data['design']->text.'</textarea>
					  </div></div><div class="row">';
    if (!empty($data['design']->image)){

        echo '<p>Uploading new images will overwrtite current images .</p>';

        echo '<img class="col-md-2" src="'.url('/assets/homepagedesign/'.$data['design']->image).'" required="required"/>';

        echo '<div class="clearfix"></div>';
    }
    echo  '<div class="input-field col s12 m6">
							Design images
							<input type="file" class="form-control" name="image" accept="image/*"/>
						  </div>
						 </div> 
						 <div class="row">
					  <div class="input-field col s12 m6">
							Link
							<select name="link" class="form-control">
							<option value="0"></option>';
    foreach ($data['links'] as $parent){
        echo '<option value="'.$parent->slug.'" '.($parent->slug == $data['design']->link ? 'selected' : '').'>'.$parent->name.'</option>';
    }
    echo '</select>
					  </div>
					  </div>
					  <input name="update" type="submit" value="Edit design" style="padding:3px 25px;" class="btn btn-primary" />
				</div>	
			</form>
			</div>	
			</div>	
			</div>	
			</div>	';
} else {
?>
<div class="row" style="margin-top: 10px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">

                <h5 class="card-title" style="font-size: 20px;">Manage Homepage Design</h5>

                <div class="divider"></div>
                <div class="row" style="position:relative;">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="datatable-editable">
                            <thead>
                            <tr class="bg-blue">
                                <th>Sr. No.</th>
                                <th>Design Image</th>            			<th>Title</th>
                                <th>Text</th>
                                <th>Link</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $sr = 1;
                            echo $data['notices'];
                            foreach ($data['designs'] as $design){
                                echo'<tr>
            <td>'.$sr.'</td>
            <td><img src="../assets/homepagedesign/'.$design->image.'" style="height: 100px; width: 100px;"></td>
            <td>'.$design->title.'</td>            <td>'.$design->text.'</td>
			<td>' . $design->link . '</td>
			<td><a href="home-page-design?edit='.$design->id.'"><i class="icon-pencil"></i></a>
            </td>
          </tr>';


                                echo'</div>
	</div>';
                                $sr++;}
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .button {
        background: gainsboro;
        padding: 5px 20px;
        cursor: pointer;
        border-radius: 30px;
    }
    .mini-button {
        cursor: pointer;
        border-radius: 30px;
        background: transparent;
        padding: 5px 0px;
        display: block;
    }
</style>

<!-- BEGIN VENDOR JS-->
<script src="<?=$data['tp']; ?>/admin/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.js"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$data['tp']; ?>/admin/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$data['tp']; ?>/admin/js/plugins.js" type="text/javascript"></script>
<script src="<?=$data['tp']; ?>/admin/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$data['tp']; ?>/admin/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$data['tp']; ?>/admin/js/select2.min.js"></script>
<script src="<?=$data['tp']; ?>/admin/js/scripts/ui-alerts.js" type="text/javascript"></script>
<!-- <script src="<?php //$data['tp']; ?>/js/scripts/data-tables.js" type="text/javascript"></script> -->
<script>
    $( document ).ready(function() {
        CKEDITOR.replace( 'editor' );
    });
</script>
<?php
echo $data['footer'];
?>



