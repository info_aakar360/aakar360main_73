<?php
echo $data['header'];
if(isset($_GET['edit'])){
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Edit services requests status</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="services_answer" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post" class="form-horizontal single" enctype="multipart/form-data">
			'.csrf_field().'
			
				<fieldset>
					  <div class="form-group">
						<label class="control-label">Status</label>
						<select name="status" class="form-control">';
    if($data['answer']->status == 'Processing') {
        echo '<option value="">Please Select Status</option>
                                <option value="Processing" selected>Processing</option>
                                <option value="Viewed">Viewed</option>
                                <option value="Hold">Hold</option>
                                <option value="Pending">Pending</option>
                                <option value="Completed">Completed</option>
                                <option value="Cancelled">Cancelled</option>';
    } else if($data['answer']->status == 'Viewed') {
        echo '<option value="">Please Select Status</option>
                                <option value="Processing" >Processing</option>
                                <option value="Viewed" selected>Viewed</option>
                                <option value="Hold">Hold</option>
                                <option value="Pending">Pending</option>
                                <option value="Completed">Completed</option>
                                <option value="Cancelled">Cancelled</option>';
    } else if($data['answer']->status == 'Hold') {
        echo '<option value="">Please Select Status</option>
                                <option value="Processing" >Processing</option>
                                <option value="Viewed" >Viewed</option>
                                <option value="Hold" selected>Hold</option>
                                <option value="Pending">Pending</option>
                                <option value="Completed">Completed</option>
                                <option value="Cancelled">Cancelled</option>';
    } else if($data['answer']->status == 'Pending') {
        echo '<option value="">Please Select Status</option>
                                <option value="Processing" >Processing</option>
                                <option value="Viewed" >Viewed</option>
                                <option value="Hold" >Hold</option>
                                <option value="Pending" selected>Pending</option>
                                <option value="Completed">Completed</option>
                                <option value="Cancelled">Cancelled</option>';
    } else if($data['answer']->status == 'Completed') {
        echo '<option value="">Please Select Status</option>
                                <option value="Processing" >Processing</option>
                                <option value="Viewed" >Viewed</option>
                                <option value="Hold" >Hold</option>
                                <option value="Pending" >Pending</option>
                                <option value="Completed" selected>Completed</option>
                                <option value="Cancelled">Cancelled</option>';
    }
    else if($data['answer']->status == 'Cancelled') {
        echo '<option value="">Please Select Status</option>
                                <option value="Processing" >Processing</option>
                                <option value="Viewed" >Viewed</option>
                                <option value="Hold" >Hold</option>
                                <option value="Pending" >Pending</option>
                                <option value="Completed" >Completed</option>
                                <option value="Cancelled" selected>Cancelled</option>';
    }
    echo '</select>
					  </div>
					  <input name="edit" type="submit" value="Edit services" class="btn btn-primary" />
				</fieldset>
			</form>
			</div>
			</div>
			</div>
			</div>';
    ?>


<?php } else { ?>
<div class="row" style="margin-top: -10px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">

                <h3 class="card-title" style="color: #0d1baa;">Manage your services request(s)</h3>

                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="datatable-editable">
                        <thead>
                        <tr class="bg-blue">
                            <th>Sr. No.</th>
                            <th>Service Name</th>
                            <th>User Name</th>
                            <th>Answer</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $sr = 1;
                        echo $data['notices'];
                        foreach ($data['answers'] as $answer){
                            echo'<tr>
                        <td>'.$sr.'</td>
                        <td>'.getServiceName($answer->service_id).'</td>';
                            $u = $answer->user_id;
                            $user = DB::select("SELECT * FROM customers WHERE id = '$u'")[0];
                            echo '<td>'.$user->name.'</td>
                        <td>';
                            $ans = json_decode($answer->answer);
                            //dd($ans);
                            foreach ($ans as $key=>$value){
                                //dd($value);
                                if($key == 'pincode') {
                                    echo '<b>Pincode :</b> ' . $value.'<br>';
                                } else {
                                    $que = DB::select("SELECT * FROM questions WHERE id = '$key'")[0];
                                    //dd($que);
                                    echo '<b>Question :</b> '.$que->question.'<br> <b>Answer :</b> '.$value.'<br>';
                                }
                            }
                            echo '</td>
                        <td>'.$answer->status.'</td>
                        <td><a href="services_answer?edit='.$answer->id.'"><i class="icon-question"></i></a>
                        </td></tr>';

                            $sr++;}
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <style>
        .button {
            background: gainsboro;
            padding: 5px 20px;
            cursor: pointer;
            border-radius: 30px;
        }
        .mini-button {
            cursor: pointer;
            border-radius: 30px;
            background: transparent;
            padding: 5px 0px;
            display: block;
        }
    </style>

    <?php
    echo $data['footer'];
    ?>



