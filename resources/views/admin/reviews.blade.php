	<?php echo $header;?>
    <div class="row" style="margin-top: 10px;">
        <div class="col s12">
            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                <div class="card-content" style="min-height: 550px;">

                    <h5 class="card-title" style="color: #0d1baa;">Manage Your Reviews </h5>

    <div class="table-responsive">
        <table class="table table-striped table-bordered" id="datatable-editable">
            <thead>
            <tr class="bg-blue">
                <th>Sr. No.</th>
                <th>Product Name</th>
                <th>Review</th>
                <th>Customer Detail</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
	<?php
    $sr = 1;
		foreach ($reviews as $review){
			echo'<tr>
            <td>'.$sr.'</td>';
            $rw = DB::table('products')->where('id','=',$review->product)->first();
            if($rw){
                echo '<td>'.$rw->title.' - <b>'.$review->rating.' stars</td>
            <td>'.nl2br(htmlspecialchars($review->review)).'</td>';
            }
			$name = '';
			$email = '';
            $crs = DB::table('customers')->where('id',$review->name)->first();
            //$crs = DB::select("SELECT * FROM customers WHERE id = '".$review->name."'")[0];
			if($crs){
				$name = $crs->name;
				$email = $crs->email;
			}
            echo '<td>'.$name.' - '.$email.'</td>
            <td>';
                echo ($review->active != 1) ? '<a href="reviews?approve='.$review->id.'"><i class="icon-like "></i></a>' : '<i class="icon-check"></i>
            </td>
          </tr>
          <!--div class="bloc">

			<div class="tools"-->';
			echo ($review->active != 1) ? '<!--a href="reviews?approve='.$review->id.'"><i class="icon-like "></i></a-->' : '<!--i class="icon-check"></i-->';
			echo '<!--/div></h5><p>'.nl2br(htmlspecialchars($review->review)).'</p>
			<b>'.$review->name.' - '.$review->email.'</b>
			</div-->';
		$sr++; }?>
            </tbody>
        </table>
    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
	echo $footer;
	?>