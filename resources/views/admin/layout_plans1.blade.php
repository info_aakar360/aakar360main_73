<?php
	echo $header;
	if(isset($_GET['add']))
	{
		echo $notices.'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Add Layout plans</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="layout_plans" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post" enctype="multipart/form-data" style="max-width: 100%;">
				'.csrf_field().'
				
					<div class="row">
						  <div class="form-group">
							<label class="control-label">Layout Plan name</label>
							<input name="title" type="text"  class="form-control" required/>
						  </div>
						  <div class="form-group">
							<label class="control-label">Layout Plan code</label>
							<input name="plan_code" type="text"  class="form-control" required/>
						  </div>
						  </div>
						  <div class="row">
						  <div class="form-group">
							<label class="control-label">Plan Category</label>
							<select name="category" class="form-control select2">';
							foreach ($categories as $category){
								echo '<option value="'.$category->id.'">'.$category->name.'</option>';
								$childs = DB::select("SELECT * FROM design_category WHERE parent = ".$category->id." ORDER BY id DESC");
								foreach ($childs as $child){
									echo '<option value="'.$child->id.'">- '.$child->name.'</option>';
									$subchilds = DB::select("SELECT * FROM design_category WHERE parent = ".$child->id." ORDER BY id DESC");
									foreach ($subchilds as $subchild){
                                        echo '<option value="'.$subchild->id.'"><i style="padding-left: 10px;">--> '.$subchild->name.'</i></option>';
                                    }
								}
							}
							echo '</select>
						  </div>
						  
						  <div class="form-group">
							<label class="control-label">Plan description</label>
							<textarea name="description" type="text" id="" class="form-control editor" required></textarea>
						  </div>
						  </div>
						  <div class="row">
						  <div class="form-group">
							<label class="control-label">Plan specification</label>
							<textarea name="specification" id="" type="text" class="form-control editor1" required></textarea>
						  </div>
						  <div class="form-group">
							<label class="control-label">Plan price</label>
							<input name="price" type="text" class="form-control" required />
						  </div>
						  </div>
						  <div class="row">
						  <div class="form-group">
							<label class="control-label">Tax (%)</label>
							<input name="tax" type="text" class="form-control" required />
						  </div>
						  <div class="form-group">
							<div><label class="control-label">Drawing Views</label></div>
							<input name="drawing_views[]" type="text" class="form-control col-lg-6 pull-left" style="width: 50%;" required/>
							<input type="file" class="form-control col-lg-6 pull-right" name="file" accept="pdf/*"  style="width: 50%;" required/>
						  </div>
						  </div>
						  <div class="row">
						  <div class="form-group">
							<div><label class="control-label">Drawing Views</label></div>
							<input name="drawing_views[]" type="text" class="form-control col-lg-6 pull-left" style="width: 50%;" required/>
							<input type="file" class="form-control col-lg-6 pull-right" name="file1" accept="pdf/*"  style="width: 50%;" required/>
						  </div>
						  
						  <div class="form-group">
							<label class="control-label">Plan images</label>
							<input type="file" class="form-control" name="images[]" multiple="multiple" accept="image/*"  required/>
						  </div>
						  </div>
						  <div class="row">
						  <div class="form-group">
							<label class="control-label">Layout Amminities</label>
							<div class="col-lg-12">
							    <div class="col-lg-6" style="padding: 0px;">
                                    <select name="ammenities[]" class="form-control"  style="border-radius: 0px; border: 1px solid;">
                                        <option value="">Please Select Amminity</option>';
                                            foreach($amminities as $ammy){
                                                echo '<option value="'.$ammy->id.'">'.$ammy->name.'</option>';
                                            }
                                    echo '</select>
                                </div>
							    <div class="col-lg-6" style="padding: 0px;">
							        <input type="text" class="form-control" name="interior_ammenities[]"  style="border-radius: 0px; border: 1px solid;"/> 
                                </div>
                                
                            </div>
                            <div class="row">
                            <div class="col-lg-12">
                                <div class="col-lg-6" style="padding: 0px;">
                                    <select name="ammenities[]" class="form-control"   style="border-radius: 0px; border: 1px solid;">
                                        <option value="">Please Select Amminity</option>';
                                            foreach($amminities as $ammy){
                                                echo '<option value="'.$ammy->id.'">'.$ammy->name.'</option>';
                                            }
                                    echo '</select>
                                </div>
                                </div>
							    <div class="col-lg-6" style="padding: 0px;">
							        <input type="text" class="form-control" name="interior_ammenities[]"   style="border-radius: 0px; border: 1px solid;"/> 
                                </div>
                                
                            </div>
                            </div>
                            <div class="row">
                            <div class="col-lg-12">
                                <div class="col-lg-6" style="padding: 0px;">
                                    <select name="ammenities[]" class="form-control"   style="border-radius: 0px; border: 1px solid;">
                                        <option value="">Please Select Amminity</option>';
                                            foreach($amminities as $ammy){
                                                echo '<option value="'.$ammy->id.'">'.$ammy->name.'</option>';
                                            }
                                    echo '</select>
                                </div>
							    <div class="col-lg-6" style="padding: 0px;">
							        <input type="text" class="form-control" name="interior_ammenities[]"   style="border-radius: 0px; border: 1px solid;"/> 
                                </div>
                               </div> 
                            </div>
                            <div class="row">
                            <div class="col-lg-12">
                                <div class="col-lg-6" style="padding: 0px;">
                                    <select name="ammenities[]" class="form-control"   style="border-radius: 0px; border: 1px solid;">
                                        <option value="">Please Select Amminity</option>';
                                            foreach($amminities as $ammy){
                                                echo '<option value="'.$ammy->id.'">'.$ammy->name.'</option>';
                                            }
                                    echo '</select>
                                </div>
							    <div class="col-lg-6" style="padding: 0px;">
							        <input type="text" class="form-control" name="interior_ammenities[]"   style="border-radius: 0px; border: 1px solid;"/> 
                                </div>
                               </div> 
                            </div>
                            <div class="row">
                            <div class="col-lg-12">
                                <div class="col-lg-6" style="padding: 0px;">
                                    <select name="ammenities[]" class="form-control"   style="border-radius: 0px; border: 1px solid;">
                                        <option value="">Please Select Amminity</option>';
                                            foreach($amminities as $ammy){
                                                echo '<option value="'.$ammy->id.'">'.$ammy->name.'</option>';
                                            }
                                    echo '</select>
                                </div>
							    <div class="col-lg-6" style="padding: 0px;">
							        <input type="text" class="form-control" name="interior_ammenities[]"   style="border-radius: 0px; border: 1px solid;"/> 
                                </div>
                               </div> 
                            </div>
                            <div class="row">
                            <div class="col-lg-12">
                                <div class="col-lg-6" style="padding: 0px;">
                                    <select name="ammenities[]" class="form-control"  style="border-radius: 0px; border: 1px solid;">
                                        <option value="">Please Select Amminity</option>';
                                            foreach($amminities as $ammy){
                                                echo '<option value="'.$ammy->id.'">'.$ammy->name.'</option>';
                                            }
                                    echo '</select>
                                </div>
							    <div class="col-lg-6" style="padding: 0px;">
							        <input type="text" class="form-control" name="interior_ammenities[]" style="border-radius: 0px; border: 1px solid;"/> 
                                </div>
                                
                            </div>
						  </div>
						  
						  
						  <hr>
						  
						  <div class="row">
						  <div class="form-group">
							<label class="control-label">Plan 1 Name</label>
							<input name="p1_name" type="text" class="form-control" />
						  </div>
						  
						  <div class="form-group">
							<label class="control-label">Plan 1 Price</label>
							<input name="p1_price" type="text" class="form-control" />
						  </div>
						  </div>
						  <div class="row">
						  <div class="form-group">
							<label class="control-label">Plan 1 Duration</label>
							<input name="p1_duration" type="text" class="form-control" />
						  </div>
						  
						  <div class="form-group">
							<label class="control-label">Plan 1 description</label>
							<textarea name="p1_description" type="text" id="" class="form-control editor2"></textarea>
						  </div>
						  </div>
						  <div class="row">
						  <div class="form-group">
							<label class="control-label">Plan 2 Name</label>
							<input name="p2_name" type="text" class="form-control" />
						  </div>
						  
						  <div class="form-group">
							<label class="control-label">Plan 2 Price</label>
							<input name="p2_price" type="text" class="form-control" />
						  </div>
						  </div>
						  <div class="row">
						  <div class="form-group">
							<label class="control-label">Plan 2 Duration</label>
							<input name="p2_duration" type="text" class="form-control" />
						  </div>
						  
						  <div class="form-group">
							<label class="control-label">Plan 2 description</label>
							<textarea name="p2_description" type="text" id="" class="form-control editor3"></textarea>
						  </div>
						  </div>
						  <div class="row">
						  <div class="form-group">
							<label class="control-label">Plan 3 Name</label>
							<input name="p3_name" type="text" class="form-control" />
						  </div>
						  
						  <div class="form-group">
							<label class="control-label">Plan 3 Price</label>
							<input name="p3_price" type="text" class="form-control" />
						  </div>
						  </div>
						  <div class="row">
						  <div class="form-group">
							<label class="control-label">Plan 3 Duration</label>
							<input name="p3_duration" type="text" class="form-control" />
						  </div>
						  
						  <div class="form-group">
							<label class="control-label">Plan 3 description</label>
							<textarea name="p3_description" type="text" id="" class="form-control editor4"></textarea>
						  </div>
						  </div>
						  <div class="row">
						  <div class="form-group">
							<label class="control-label">Is Featured</label>
							<input type="radio" name="featured" value="1">Yes &nbsp;&nbsp;<input type="radio" name="featured" value="0" checked>No 
						  </div>
						  </div>
						  <input name="add" type="submit" value="Add plan" class="btn btn-primary" />
					</div>
				</form>
				</div>
				</div>
				</div>
				</div>';
	} elseif(isset($_GET['edit'])){
		echo $notices.'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Edit Layout plans</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="layout_plans" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post" enctype="multipart/form-data" style="max-width: 100%;">
			'.csrf_field().'
				
					<div class="row">
						  <div class="input-field col s12 m6">
							<label class="control-label">Plan code</label>
							<input name="plan_code" type="text" value="'.$plan->plan_code.'" class="form-control"  required/>
						  </div>
						  <div class="input-field col s12 m6">
							<label class="control-label">Plan name</label>
							<input name="title" type="text" value="'.$plan->title.'" class="form-control"  required/>
						  </div>
						  </div>
						  <div class="row">
						  <div class="input-field col s12 m6">
							<label class="control-label">Plan category</label>
							<select name="category" class="form-control">';
							foreach ($categories as $category){
								echo '<option value="'.$category->id.'" '.($category->id == $plan->category ? 'selected' : '').'>'.$category->name.'</option>';
								$childs = DB::select("SELECT * FROM design_category WHERE parent = ".$category->id." ORDER BY id DESC");
								foreach ($childs as $child){
									echo '<option value="'.$child->id.'" '.($child->id == $plan->category ? 'selected' : '').'>- '.$child->name.'</option>';
									$subchilds = DB::select("SELECT * FROM design_category WHERE parent = ".$child->id." ORDER BY id DESC");
									foreach ($subchilds as $subchild){
                                        echo '<option value="'.$subchild->id.'" '.($subchild->id == $plan->category ? 'selected' : '').'><i style="padding-left: 10px;">--> '.$subchild->name.'</i></option>';
                                    }
								}
							}
							echo '</select>
						  </div>
						  
						  <div class="input-field col s12 m6">
							<label class="control-label">Plan description</label>
							<textarea name="description" type="text" id="" class="form-control editor" required>'.$plan->description.'</textarea>
						  </div>
						  </div>
						  <div class="row">
						  <div class="input-field col s12 m6">
							<label class="control-label">Plan specification</label>
							<textarea name="specification" type="text" id="" class="form-control editor1" required>'.$plan->specification.'</textarea>
						  </div>
						  
						  
						  <div class="input-field col s12 m6">
							<label class="control-label">Plan price</label>
							<input name="price" type="text" value="'.$plan->price.'" class="form-control"  required/>
						  </div>
						  </div>
						  <div class="row">
						  <div class="input-field col s12 m6">
							<label class="control-label">Tax (%)</label>
							<input name="tax" type="text" value="'.$plan->tax.'" class="form-control"  required/>
						  </div>
						  <div class="input-field col s12 m6">
							<div><label class="control-label">Drawing Views</label></div>';
							  if (!empty($plan->file)){
								  $fl = explode(',',$plan->file);
								  echo $fl[0];
							  }
							  $dv = json_decode($plan->drawing_views);
							echo '<input name="drawing_views[]" type="text" class="form-control col-lg-6 pull-left" value="'.$dv[0].'"  style="width: 50%;" required/>
							<input type="file" class="form-control col-lg-6 pull-right" name="file1" accept="pdf/*"  style="width: 50%;" />
						  </div>
						  </div>
						  <div class="row">
						  <div class="input-field col s12 m6">
							<div><label class="control-label">Drawing Views</label></div>';
							  if (!empty($plan->file)){
								  $fl = explode(',',$plan->file);
								  echo $fl[1];
							  }
							  $dv = json_decode($plan->drawing_views);
							echo '<input name="drawing_views[]" type="text" class="form-control col-lg-6 pull-left" value="'.$dv[1].'"  style="width: 50%;" required/>
							<input type="file" class="form-control col-lg-6 pull-right" name="file1" accept="pdf/*"  style="width: 50%;" />
						  </div><div class="input-field col s12 m6">';
						  if (!empty($plan->images)){
							  echo '<p>Uploading new images will overwrtite current images .</p>';
							  $images = explode(',',$plan->images);
							  foreach($images as $image){
									echo '<img class="col-md-2" src="'.url('/assets/layout_plans/thumbs/'.$image).'" />';
							  }
							  echo '<div class="clearfix"></div>';
						  }
						  echo '</div><div class="input-field col s12 m6">
							<label class="control-label">Plan images</label>
							<input type="file" class="form-control" name="images[]" multiple="multiple" accept="image/*"/>
						  </div>
						  <div class="row">
                              <div class="col-sm-12">
                                  <div class="input-field col s12 m6">';
                                        $amm = json_decode($plan->interior_ammenities);
                                        $i = 1;
                                        $selected = '';
                                        foreach($amm as $key=>$value) {
                                            echo '<div class="col-lg-12">
                                            <div class="col-lg-6" style="padding: 0px;">
                                                <select name="ammenities[]" class="form-control"   style="border-radius: 0px; border: 1px solid;">
                                                    <option value="">Please Select Amminity</option>';
                                                    foreach ($amminities as $ammy) {
                                                        $selected = '';
                                                        $a = DB::select("SELECT * FROM amminities WHERE id = ".$key)[0];
                                                        if($ammy->id == $a->id){
                                                            $selected = 'selected';
                                                        }
                                                        echo '<option value="' . $ammy->id . '" '.$selected.'>' . $ammy->name . '</option>';
                                                    }
                                                    echo '</select>
                                            </div>
                                            <div class="col-lg-6" style="padding: 0px;">
                                                <input type="text" class="form-control" name="interior_ammenities[]"  value="'.$value.'" style="border-radius: 0px; border: 1px solid;"/> 
                                            </div>
                                        </div>';
                                        $i++; }
                                    echo '</div>
                                </div>
                            </div>
                            
                            <hr>
						  
						  <div class="row">
						  <div class="input-field col s12 m6">
							<label class="control-label">Plan 1 Name</label>
							<input name="p1_name" type="text" value="'.$plan->p1_name.'" class="form-control" />
						  </div>
						  
						  <div class="input-field col s12 m6">
							<label class="control-label">Plan 1 Price</label>
							<input name="p1_price" type="text" value="'.$plan->p1_price.'" class="form-control" />
						  </div></div>
						  <div class="row">
						  <div class="input-field col s12 m6">
							<label class="control-label">Plan 1 Duration</label>
							<input name="p1_duration" type="text" value="'.$plan->p1_duration.'" class="form-control" />
						  </div>
						  
						  <div class="input-field col s12 m6">
							<label class="control-label">Plan 1 description</label>
							<textarea name="p1_description" type="text" id="" class="form-control editor2">'.$plan->p1_description.'</textarea>
						  </div>
						  </div>
						  <div class="row">
						  <div class="input-field col s12 m6">
							<label class="control-label">Plan 2 Name</label>
							<input name="p2_name" type="text" value="'.$plan->p2_name.'" class="form-control" />
						  </div>
						  
						  <div class="input-field col s12 m6">
							<label class="control-label">Plan 2 Price</label>
							<input name="p2_price" type="text" value="'.$plan->p2_price.'" class="form-control" />
						  </div>
						  </div>
						  <div class="row">
						  <div class="input-field col s12 m6">
							<label class="control-label">Plan 2 Duration</label>
							<input name="p2_duration" type="text" value="'.$plan->p2_duration.'" class="form-control" />
						  </div>
						  
						  <div class="input-field col s12 m6">
							<label class="control-label">Plan 2 description</label>
							<textarea name="p2_description" type="text" id="" class="form-control editor3">'.$plan->p2_description.'</textarea>
						  </div>
						  </div>
						  <div class="row">
						 <div class="input-field col s12 m6">
							<label class="control-label">Plan 3 Name</label>
							<input name="p3_name" type="text" value="'.$plan->p3_name.'" class="form-control" />
						  </div>
						  
						  <div class="input-field col s12 m6">
							<label class="control-label">Plan 3 Price</label>
							<input name="p3_price" type="text" value="'.$plan->p3_price.'" class="form-control" />
						  </div>
						  </div>
						  <div class="row">
						  <div class="input-field col s12 m6">
							<label class="control-label">Plan 3 Duration</label>
							<input name="p3_duration" type="text" value="'.$plan->p3_duration.'" class="form-control" />
						  </div>
						  
						  <div class="input-field col s12 m6">
							<label class="control-label">Plan 3 description</label>
							<textarea name="p3_description" type="text" id="" class="form-control editor4">'.$plan->p3_description.'</textarea>
						  </div></div>
						  <div class="row">
						  <div class="input-field col s12 m6">
							<label class="control-label">Is Featured</label>&nbsp;&nbsp;';
							$rval = $plan->featured;
							$yes = '';
							$no = '';
							if($rval == '1'){
							    $yes = 'checked';
                            }else{
							    $no = 'checked';
                            }
							echo '<input type="radio" name="featured" value="1" '.$yes.'>Yes &nbsp;&nbsp;<input type="radio" name="featured" value="0" '.$no.'>No 
						  </div></div>
						  <input name="edit" type="submit" value="Update plan" class="btn btn-primary" />
					</div>
				</form>
				</div>
				</div>
				</div>
				</div>';
 } else { 
	echo '<div class="row" style="margin-top: -13px;">
        <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
        <div class="card-content" style="min-height: 550px;">
        <h5>Layout Plans<a href="layout_plans?add" style="padding:3px 15px;" class="add">Add layout plans</a></h5>
        <h5 class="card-title" style="color: #0d1baa;">Manage Your Layout Plans</h5>
        <div class="table-responsive">
        <table class="table table-striped table-bordered" id="datatable-editable">
        <thead>
        <tr class="bg-blue">
            <th>Sr. No.</th>
            <th>Plan Banner</th>
            <th>Plan Name</th>
            <th>Plan Number</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>';
	$sr = 1;
	echo $notices;
	foreach ($plans as $plan){
	echo '<tr>
            <td>'.$sr.'</td>
            <td><img src="../assets/layout_plans/'.image_order($plan->images).'" style="height: 100px; width: 100px;"></td>
            <td>'.$plan->title.'</td>
            <td>'.$plan->plan_code.'</td>
            <td><a href="layout-plans?delete='.$plan->id.'"><i class="icon-trash"></i></a>
				<a href="layout-plans?edit='.$plan->id.'"><i class="icon-pencil"></i></a>
				<a href="layout-plans/add-questions/'.$plan->slug.'"><i class="icon-question"></i></a>
				<a href="layout-plans/layout_plan_addon/'.$plan->slug.'"><i class="icon-plus"></i></a>
            </td>
          </tr>
		'; $sr++;}
	}
?>
        </tbody>
        </table>
        </div>
</div>
</div>
</div>
</div>
</div>
<style>
	.button {
		background: gainsboro;
		padding: 5px 20px;
		cursor: pointer;
		border-radius: 30px;
	}
	.mini-button {
		cursor: pointer;
		border-radius: 30px;
		background: transparent;
		padding: 5px 0px;
		display: block;
	}
</style>
<script>
    $( document ).ready(function() {
		$('.select2').select2();
    });

    /*$(document).ready(function(){
        //alert($('.remove-me').length);
        var next = $('.remove-me').length;
        $(".add-more").click(function(e){
            var max_fields      = 6;
            e.preventDefault();
            var addto = "#field" + next;
            var addRemove = "#field" + (next);
            next = next + 1;
            var newIn = '<input autocomplete="off" class="input form-control" id="field' + next + '" name="interior_ammenities[]" type="text" style="width: 80%; border: 1px solid;">';
            var newInput = $(newIn);
            var removeBtn = '<button id="remove' + (next - 1) + '" class="btn btn-danger remove-me" style="width: 20%; float: right; margin-top: 0px; border-radius: 0px; border: 1px solid; padding-top: 6px;padding-bottom: 6px;">-</button> <br></div><div id="field">';
            var removeButton = $(removeBtn);
            $(addto).after(newInput);
            $(addRemove).after(removeButton);
            $("#field" + next).attr('data-source',$(addto).attr('data-source'));
            $("#count").val(next);


        });
        $('.remove-me').click(function(e){
            e.preventDefault();
            var fieldNum = this.id.charAt(this.id.length-1);
            //alert(fieldNum);
            var fieldID = "#field" + fieldNum;
            $(this).remove();
            $(fieldID).remove();
        });
    });*/

    $(document).ready(function() {
        var max_fields      = 6;
        var wrapper         = $(".input_fields_wrap");
        var add_button      = $(".add_field_button");

        var x = 1; //initlal text box count
        $(add_button).click(function(e){
            e.preventDefault();
            if(x < max_fields){
                x++;
                $(wrapper).append('<div><input type="text" class="form-control" name="mytext[]" style="width: 80%; border: 1px solid;"/><a href="#" class="remove_field btn btn-danger" style="width: 20%; float: right; margin-top: 0px; border-radius: 0px; border: 1px solid; padding-top: 6px;padding-bottom: 6px;">-</a></div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_field", function(e){
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })
    });

</script>
<?php
	echo $footer;
?>
<script>
    $( document ).ready(function() {
        var editor_config = {
            path_absolute : "<?php echo URL::to('/admin') ?>/",
            selector: "textarea.editor",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            templates: [
                {title: 'Some title 1', description: 'Some desc 1', content: 'My content'},
                {title: 'Some title 2', description: 'Some desc 2', url: 'http://162.144.132.86/~adroweb/'}
            ],
            relative_urls: false,
        };
        tinymce.init(editor_config);
    });
</script>
<script>
    $( document ).ready(function() {
        var editor_config1 = {
            path_absolute : "<?php echo URL::to('/admin') ?>/",
            selector: "textarea.editor1",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            templates: [
                {title: 'Some title 1', description: 'Some desc 1', content: 'My content'},
                {title: 'Some title 2', description: 'Some desc 2', url: 'http://162.144.132.86/~adroweb/'}
            ],
            relative_urls: false,
        };
        tinymce.init(editor_config1);
    });
</script>
<script>
    $( document ).ready(function() {
        var editor_config2 = {
            path_absolute : "<?php echo URL::to('/admin') ?>/",
            selector: "textarea.editor2",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            templates: [
                {title: 'Some title 1', description: 'Some desc 1', content: 'My content'},
                {title: 'Some title 2', description: 'Some desc 2', url: 'http://162.144.132.86/~adroweb/'}
            ],
            relative_urls: false,
        };
        tinymce.init(editor_config2);
    });
</script>
<script>
    $( document ).ready(function() {
        var editor_config3 = {
            path_absolute : "<?php echo URL::to('/admin') ?>/",
            selector: "textarea.editor3",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            templates: [
                {title: 'Some title 1', description: 'Some desc 1', content: 'My content'},
                {title: 'Some title 2', description: 'Some desc 2', url: 'http://162.144.132.86/~adroweb/'}
            ],
            relative_urls: false,
        };
        tinymce.init(editor_config3);
    });
</script>
<script>
    $( document ).ready(function() {
        var editor_config4 = {
            path_absolute : "<?php echo URL::to('/admin') ?>/",
            selector: "textarea.editor4",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            templates: [
                {title: 'Some title 1', description: 'Some desc 1', content: 'My content'},
                {title: 'Some title 2', description: 'Some desc 2', url: 'http://162.144.132.86/~adroweb/'}
            ],
            relative_urls: false,
        };
        tinymce.init(editor_config4);
    });
</script>