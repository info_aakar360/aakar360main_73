<?php
echo $data['header'];
if(isset($_GET['edit'])){
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Edit layout faq</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="layout_faq" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
                <form action="" method="post" style="max-width: 100%;" enctype="multipart/form-data">
			'.csrf_field().'
			
				<div class="row">
					  <div class="input-field col s12 m6">
						Answer
						<textarea class="form-control" name="answer" rows="5" placeholder="Write answer here" required="required">'.$data['faq']->answer.'</textarea>
					  </div>
					  <div class="input-field col s12 m6">
						Status
						<select name="status" class="form-control">';
    if($data['faq']->status == 'Pending') {
        echo '<option value="">Please Select Status</option>
                                <option value="Pending" selected>Pending</option>
                                <option value="Approved">Approved</option>';
    } else if($data['faq']->status == 'Approved') {
        echo '<option value="">Please Select Status</option>
                                <option value="Pending" >Pending</option>
                                <option value="Approved" selected>Approved</option>';
    }
    echo '</select>
					  </div>
					  </div>
					  <input name="edit" type="submit" value="Edit faq" style="padding:3px 25px;" class="btn btn-primary" />
				
			</form>
			</div>
			</div>
			</div>
			</div>';
    ?>


<?php } else { ?>

    <div class="row" style="margin-top: -13px;">
        <div class="col s12">
            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                <div class="card-content" style="min-height: 550px;">
                    <h5>Layout Faq(s)</h5>
                    <h5 class="card-title" style="color: #0d1baa;"> Manage Layout FAQ(s)</h5>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="datatable-editable">
                            <thead>
                            <tr class="bg-blue">
                                <th>Sr. No.</th>
                                <th>Layout Name</th>
                                <th>User Name</th>
                                <th>Question</th>
                                <th>Answer</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $sr = 1;
                            echo $data['notices'];
                            foreach ($data['faqs'] as $faq){
                                $l = $faq->layout_id;
                                $lay = DB::table('layout_plans')->where('id', $l)->first();
                                echo'<tr>
                        <td>'.$sr.'</td>
                        <td>'; if($lay !== null){ echo $lay->title; } else { echo 'NA'; } echo '</td>';
                                $u = $faq->user_id;
                                $user = DB::table('customers')->where('id',$u)->first();
                                if($user !== null){
                                    echo '<td>'.$user->name.'</td>';
                                }else{
                                    echo '<td>NA</td>';
                                }
                                echo '<td>'.$faq->question.'</td>
                        <td>'.$faq->answer.'</td>
                        <td>'.$faq->status.'</td>
                        <td><a href="layout_faq?edit='.$faq->id.'"><i class="icon-question"></i></a>
                        </td></tr>';

                                $sr++;}
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
<?php } ?>
<style>
    .button {
        background: gainsboro;
        padding: 5px 20px;
        cursor: pointer;
        border-radius: 30px;
    }
    .mini-button {
        cursor: pointer;
        border-radius: 30px;
        background: transparent;
        padding: 5px 0px;
        display: block;
    }
</style>

<?php
echo $data['footer'];
?>



