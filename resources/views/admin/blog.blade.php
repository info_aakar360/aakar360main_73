<?php
echo $data['header'];
if(isset($_GET['add']))
{
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Add New Blog</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="blog" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post" enctype="multipart/form-data" style="max-width: 100%;">
				'.csrf_field().'
				
					<div class="row">
						  <div class="input-field col s12 m6">
							Post title
							<input name="title" type="text"  class="form-control" required/>
						  </div>
						  <div class="input-field col s12 m6">
							Post content
							<textarea class="form-control editor" name="content" rows="10" cols="80"></textarea>
						  </div>
						  </div>
						  <div class="row">
                          <div class="input-field col s12 m6">
							Category
							<select name="category" class="form-control">
							<option value="0">Please Select Category</option>';
                            foreach ($data['categories'] as $parent){
                                echo '<option value="'.$parent->id.'">'.$parent->name.'</option>';
                            }
                            echo '</select>
                          </div>
							<div class="input-field col s12 m6">
								Post image
								<input type="file" class="form-control" name="image" accept="image/*"/>
							</div>
							</div>
							<div class="row">
							<div class="input-field col s12 m6">
								Banner image
								<input type="file" class="form-control" name="banner_image" accept="image/*"/>
							</div>
							
							<div class="input-field col s12 m6">
							Product Category
							<select name="pro_cat[]" multiple class="form-control select2">';
                            foreach ($data['procat'] as $brand){
                                echo '<option value="'.$brand->id.'">'.$brand->name.'</option>';
                                $childs = DB::select("SELECT * FROM category WHERE parent = ".$brand->id);
                                foreach ($childs as $child){
                                    echo '<option value="'.$child->id.'">- '.$child->name.'</option>';
                                    $subchilds = DB::select("SELECT * FROM category WHERE parent = ".$child->id);
                                    foreach ($subchilds as $subchild){
                                        echo '<option value="'.$subchild->id.'"><i style="padding-left: 10px;">--> '.$subchild->name.'</i></option>';
                                    }
                                }
                            }
                            echo '</select>
						  </div>
						  </div>
						  <div class="row">
                            <div class="input-field col s12 m6">
                                Section
                                <select name="section" class="form-control">
                                    <option value="retail">Retail</option>
                                    <option value="institutional">Institutional</option>
                                </select>
                            </div></div>
						  <input name="add" type="submit" style="padding: 3px 25px;" value="Add Post" class="btn btn-primary" />
					</div>
				</form></div></div>
				</div></div>';
}
elseif(isset($_GET['edit']))
{
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
        <div class="card-content" style="min-height: 550px;">
        <h5>Edit Blog</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="blog" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            
            <form action=""  enctype="multipart/form-data" method="post" style="max-width: 100%;">
				'.csrf_field().'
				
					<div class="row">
						  <div class="input-field col s12 m6">
							Post title
							<input name="title" value="'.$data['post']->title.'" type="text"  class="form-control" required/>
						  </div>
						  <div class="input-field col s12 m6">
							Post content
							<textarea name="content" class="form-control editor" rows="10" cols="80" required>'.$data['post']->content.'</textarea>
						  </div>
						  </div>
						  <div class="row">
						  <div class="input-field col s12 m6">
							Category
							<select name="category" class="form-control">
							<option value="0"></option>';
    foreach ($data['categories'] as $parent){
        echo '<option value="'.$parent->id.'" '.($parent->id == $data['post']->category ? 'selected' : '').'>'.$parent->name.'</option>';
    }
    echo '</select>
					  </div></div>
						<div class="row">';
    if (!empty($data['post']->images)){
        echo '<p>Uploading new images will overwrtite current images .</p>
							<img class="input-field col s12 m4" src="'.url('/assets/blog/'.$data['post']->images).'" />
							<div class="clearfix"></div><br/>';
    }
    echo '<div class="input-field col s12 m6">
								Post image
								<input type="file" class="form-control" name="image" accept="image/*"/>
						  </div></div>
						  <div class="row">';
    if (!empty($data['post']->banner_image)){
        echo '<p>Uploading new images will overwrtite current images .</p>
							<img class="col-md-4" src="'.url('/assets/blog/'.$data['post']->banner_image).'" />
							<div class="clearfix"></div><br/>';
    }
    echo '<div class="input-field col s12 m6">
								Post image
								<input type="file" class="form-control" name="banner_image" accept="image/*"/>
						  </div></div>
						  <div class="row">
						  <div class="input-field col s12 m6">
							Product category
							<select name="pro_cat[]" multiple class="form-control select2">';
    foreach ($data['procat'] as $category){
        echo '<option value="'.$category->id.'" '.($category->id == $data['post']->category ? 'selected' : '').'>'.$category->name.'</option>';
        $childs = DB::select("SELECT * FROM category WHERE parent = ".$category->id." ORDER BY id DESC");
        foreach ($childs as $child){
            echo '<option value="'.$child->id.'" '.($child->id == $data['post']->category ? 'selected' : '').'>- '.$child->name.'</option>';
            $subchilds = DB::select("SELECT * FROM category WHERE parent = ".$child->id." ORDER BY id DESC");
            foreach ($subchilds as $subchild){
                echo '<option value="'.$subchild->id.'" '.($subchild->id == $data['post']->category ? 'selected' : '').'><i style="padding-left: 10px;">--> '.$subchild->name.'</i></option>';
            }
        }
    }
    echo '</select>
						  </div>
						  
						  <div class="input-field col s12 m6">
                                Section
                                <select name="section" class="form-control">
                                    <option value="retail" '.($data['post']->section == "retail" ? "selected" : "").'>Retail</option>
                                    <option value="institutional" '.($data['post']->section == "institutional" ? "selected" : "").'>Institutional</option>
                                </select>
                          </div></div>
						  <input name="edit" type="submit" value="Edit Post" class="btn btn-primary" />
					</div>
					
				</form>
				</div>
				</div>
				</div>
				</div>';
} else {
?>
<div class="row" style="margin-top: 10px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
                <h5>Blogs<a href="blog?add" style="padding: 3px 15px; font-size: 15px;" class="add">Add Blogs</a></h5>
                <h5 class="card-title" style="color: #0d1baa;">Manage Your Blogs </h5>
                <div class="table-responsive">
                    <table id="dataTableExample1" class="table table-bordered table-striped table-hover">
                        <thead>
                        <tr class="bg-blue">
                            <th>Sr. No.</th>
                            <th>Page Image</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $sr = 1;
                        echo $data['notices'];
                        foreach ($data['posts'] as $post){
                            echo'<tr>
            <td>'.$sr.'</td>
            <td><a href="../blog/'.path($post->title,$post->id).'">'.$post->title.'</a></td>
            <td>
                <a href="blog?delete='.$post->id.'"><i class="icon-trash"></i></a>
						<a href="blog?edit='.$post->id.'"><i class="icon-pencil"></i></a>
            </td>
          </tr>
          <!--div class="bloc">
				<h5>
					<a href="../blog/'.path($post->title,$post->id).'">'.$post->title.'</a>
					<div class="tools">
						<a href="blog?delete='.$post->id.'"><i class="icon-trash"></i></a>
						<a href="blog?edit='.$post->id.'"><i class="icon-pencil"></i></a>
					</div>
				</h5>
				<p>'.mb_substr(strip_tags($post->content),0,130).'...</p>
			</div-->';
                            $sr++; }
                        }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


<?php
echo $data['footer'];
?>
<script>
    $( document ).ready(function() {
        var editor_config = {
            path_absolute : "<?php echo URL::to('/admin') ?>/",
            selector: "textarea.editor",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            templates: [
                {title: 'Some title 1', description: 'Some desc 1', content: 'My content'},
                {title: 'Some title 2', description: 'Some desc 2', url: 'http://162.144.132.86/~adroweb/'}
            ],
            relative_urls: false,
        };
        tinymce.init(editor_config);
    });
</script>
