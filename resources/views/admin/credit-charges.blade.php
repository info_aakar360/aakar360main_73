<?php
echo $data['header'];
if(isset($_GET['add'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 550px;">
    <h5>Add new credit charges</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="credit-charges" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
                    <form action="" method="post" class="form-horizontal single">
			'.csrf_field().'
			
			<fieldset>
				<div class="form-group">
					<label class="control-label">Days</label>
					<input name="days" type="text"  class="form-control" required/>
				</div>
				<div class="form-group">
					<label class="control-label">Amount</label>
					<input name="amount" type="text"  class="form-control" required/>
				</div>
				<input name="add" type="submit" value="Add Charges" style="padding:3px 25px;" class="btn btn-primary" />
			</fieldset>
			</form>
			</div>
			</div>
			</div>
			</div>';
} elseif(isset($_GET['edit'])) {
    echo $data['notices'].'
<div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 550px;">
    <h5>Edit credit charges</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="credit-charges" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
                    <form action="" method="post" class="form-horizontal single">
			'.csrf_field().'
			
			<fieldset>
				<div class="form-group">
					<label class="control-label">Days</label>
					<input name="days" value="'.$data['page']->days.'" type="text"  class="form-control" required/>
				</div>
				<div class="form-group">
					<label class="control-label">Amount</label>
					<input name="amount" value="'.$data['page']->amount.'" type="text"  class="form-control" required/>
				</div>
				<input name="edit" type="submit" value="Edit page" style="padding:3px 25px;" class="btn btn-primary" />
			</fieldset>
		</form>
		</div>
		</div>
		</div>
		</div>';
} else {
    ?>
    <div class="row" style="margin-top: -13px;">
        <div class="col s12">
            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                <div class="card-content" style="min-height: 550px;">
                    <div class="head">
                        <h3>Credit Charges<a href="credit-charges?add" class="add">Add credit charges</a></h3>
                        <p>Manage Credit Charges</p>
                    </div>
                    <div class="table-responsive">
                        <table id="dataTableExample1" class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr class="bg-blue">
                                <th>Sr. No.</th>
                                <th>Days</th>
                                <th>Amount</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $sr = 1;
                            echo $data['notices'];
                            foreach ($data['pages'] as $page){
                                echo'<tr>
            <td>'.$sr.'</td>
            <td>'.$page->days.'</a></td>
            <td>'.$page->amount.'</a></td>
            <td>
                <a href="credit-charges?delete='.$page->id.'"><i class="icon-trash"></i></a>
						<a href="credit-charges?edit='.$page->id.'"><i class="icon-pencil"></i></a>
            </td>
          </tr>';
                                $sr++; }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="<?=$data['tp']?>/admin/summernote/summernote.js"></script>
    <link href="<?=$data['tp']?>/admin/summernote/summernote.css" rel="stylesheet" />
    <script type="text/javascript" src="<?=$data['tp']?>/admin/summernote/custom.js"></script>
<?=$data['footer']?>