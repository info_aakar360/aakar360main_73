<!DOCTYPE html>
<html>
	<head>
		<meta charset='utf-8'/>
		<title>Login | <?=$data['cfg']->name?></title>
		<meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
		<meta content='<?=$data['cfg']->desc?>' name='description'/>
		<meta content='<?=$data['cfg']->key?>' name='keywords'/>
		<!--ASSETS-->
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
		 <link href="https://fonts.googleapis.com/css?family=Montserrat:400" rel="stylesheet"> 
		<link rel="stylesheet" href="<?=$data['tp'];?>/admin/style.css">
		<script src="<?=$data['tp'];?>/assets/jquery.min.js"></script>
		<script src="<?=$data['tp'];?>/assets/bootstrap.min.js"></script>
		<script src="<?=$data['tp'];?>/assets/Chart.min.js"></script>
	</head>
	<body>
		<div class="content-warpper">
			<div class="content">
			<div class="clear"></div>
			<form method="post" action="" class="mini">
				<fieldset>
					<?=(isset($error) ? $error : '')?>

					<img src="<?php echo url('assets/specedoor.png'); ?>">
					<?=csrf_field()?>

					<input placeholder="Email" name="email" type="text">
					<input placeholder="Password" name="pass" type="password">
					<input class="submit" name="login" value="Login" type="submit">
				</fieldset>
			</form>
			<div style="clear:both;padding:10px"></div>
			</div>
		</div>
	</body>
</html>