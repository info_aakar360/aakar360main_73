	<?php echo $header;?>

    <div class="row" style="margin-top: -13px;">
        <div class="col s12">
            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                <div class="card-content" style="min-height: 550px;">
                    <h5>Reviews</h5>
                    <h5 class="card-title" style=" color: #0d1baa;">Manage customer reviews and approve them</h5>
    <div class="table-responsive">
        <table class="table table-striped table-bordered" id="datatable-editable">
            <thead>
            <tr class="bg-blue">
                <th>Sr. No.</th>
                <th>Photo Name</th>
                <th>Review</th>
                <th>Customer Detail</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
	<?php
    $sr = 1;
		foreach ($reviews as $review){
			echo'<tr>
            <td>'.$sr.'</td>';
            $rw = DB::select("SELECT * FROM photo_gallery WHERE id ='".$review->photo."'")[0];
            echo '<td>'.$rw->title.' - <b>'.$review->rating.' stars</td>
            <td>'.nl2br(htmlspecialchars($review->review)).'</td>';
            $crs = DB::select("SELECT * FROM customers WHERE id = '".$review->name."'")[0];
            echo '<td>'.$crs->name.' - '.$crs->email.'</td>
            <td>';
                echo ($review->active != 1) ? '<a href="photo-reviews?approve='.$review->id.'"><i class="icon-like "></i></a>' : '<i class="icon-check"></i>
            </td>
          </tr>
          <!--div class="bloc">
			<h5>'.DB::select("SELECT title FROM products WHERE id ='".$review->product."'")[0]->title.' - <b>'.$review->rating.' stars</b>
			<div class="tools"-->';
			echo ($review->active != 1) ? '<!--a href="reviews?approve='.$review->id.'"><i class="icon-like "></i></a-->' : '<!--i class="icon-check"></i-->';
			echo '<!--/div></h5><p>'.nl2br(htmlspecialchars($review->review)).'</p>
			<b>'.$review->name.' - '.$review->email.'</b>
			</div-->';
		$sr++; }?>
            </tbody>
        </table>
    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
	echo $footer;
	?>