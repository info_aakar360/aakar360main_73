<?php
echo $header;
    echo '<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Update Questions fo <b>'.$plan->title.'</b></h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="layout-plans" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
                <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" style="max-width: 100%;">
			'.csrf_field().'
			
				<fieldset>
					  <div class="form-group col-md-8" style="padding-left: 30px;padding-right: 30px;">
							<label class="control-label">Select Question</label>
							<select name="question" class="form-control select2" required="required">
							<option></option>';
                            foreach ($questions as $question){
                                echo '<option value="'.$question->id.'">'.$question->question.'</option>';
                            }
                            echo '</select>
					  </div>
					  <div class="form-group col-md-2" style="padding-left: 30px;padding-right: 30px;">
						<label class="control-label">Priority</label>
						<input name="priority" type="text" class="form-control" required="required"/>
					  </div>
					  
					  <input name="update" type="submit" value="Add Question" class="btn btn-primary col-md-2" style="padding:3px 25px;"/>
				</fieldset>
			</form>';

?>
        <div class="table-responsive">
			<table class="table table-striped table-bordered" id="datatable-editable">
				<thead>
				<tr class="bg-blue">
					<th>Sr. No.</th>
					<th>Question</th>
					<th>Priority</th>
					<th>Action</th>
				</tr>
				</thead>
				<tbody>
					<?php
					$sr = 1;
					echo $notices;
					foreach ($service_questions as $que){
						echo'<tr>
								<td>'.$sr.'</td>
								<td>'.getQuestion($que->question_id).'</td>
								<td>'.$que->priority.'</td>
								<td>
									<a href="./layout-plans/delete/'.$plan->slug.'/'.$que->id.'"><i class="icon-trash"></i></a>
								</td>
							  </tr>
							 ';
					$sr++;}

							?>
				</tbody>
			</table>
		</div>
<style>
    .button {
        background: gainsboro;
        padding: 5px 20px;
        cursor: pointer;
        border-radius: 30px;
    }
    .mini-button {
        cursor: pointer;
        border-radius: 30px;
        background: transparent;
        padding: 5px 0px;
        display: block;
    }
</style>

<?php
echo $footer;
?>
<script>
    $( document ).ready(function() {
		$('.select2').select2({
			placeholder: "Please Select Question...",
			allowClear: true
		});
	});
</script>