<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="keywords" content="">
    <meta name="author" content="ThemeSelect">
    <!--    <title>-->
    <!--        --><?//=$title; ?>
    <!--    </title>-->
    <link rel="apple-touch-icon" href="<?=$data['tp']; ?>/admin/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$data['tp']; ?>/admin/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/vendors/data-tables/css/select.dataTables.min.css">

    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/custom/custom.css">
    <link href="<?=$data['tp']; ?>/admin/css/select2.min.css" rel="stylesheet" />
    <!-- END: Custom CSS-->
</head>
<?php
echo $data['header'];
if(isset($_GET['add'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 650px;">
            <h5>Add Dealer</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="dealers" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
            <form action="" method="post"  enctype="multipart/form-data" style="max-width: 100%">
			'.csrf_field().'
			
				<div class="row">
                      <div class="input-field col s12 m6">
                        Name
                        <input name="name" type="text"  class="form-control" required="required" />
                      </div>
                      <div class="input-field col s12 m6">
                        Email
                        <input name="email" type="text" class="form-control"  required="required"/>
                      </div>
                </div>
                <div class="row">
                      <div class="input-field col s12 m6">
                        Mobile
                        <input name="mobile" type="text"  class="form-control" required="required" />
                      </div>
                      <div class="input-field col s12 m6">
                            Status
                            <select name="status" class="form-control">
                                <option value="">Please Select Option</option>
                                <option value="0">Pending</option>
                                <option value="1">Approve</option>
                            </select>
                      </div>
                      
                </div>
                <div class="row">
					  <div class="input-field col s12 m12">
                        Password
                        <input name="password" type="password" class="form-control"  required="required"/>
                      </div>
					  
                <div class="row">
					  <input name="add" type="submit" value="Add dealer" style="padding:3px 25px;" class="btn btn-primary" />
				</div>
			</form>
			</div>
			</div>
			</div>
			</div>';
} elseif(isset($_GET['edit'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
                    <h5>Edit Dealer</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="dealers" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
                    <form action="" method="post" enctype="multipart/form-data" style="max-width:  100%">
                    '.csrf_field().'
                        <div class="row">
                              <div class="input-field col s12 m6">
                                Name
                                <input name="name" type="text"  value="'.$data['dealer']->name.'" class="form-control" required="required"/>
                              </div>
                              <div class="input-field col s12 m6">
                                Email
                                <input name="email" type="text" value="'.$data['dealer']->email.'" class="form-control"  required="required"/>
                              </div>
                              
                          </div>
                          <div class="row">
                              <div class="input-field col s12 m6">
                                Phone
                                    <input name="mobile" type="text" value="'.$data['dealer']->mobile.'"/>
                              </div>
                              <div class="input-field col s12 m6">
                                Status
                                    <select name="status" class="form-control">
                                        <option value="">Please Select Option</option>
                                        <option value="0"'; if($data['dealer']->status == '0'){ echo 'selected'; } echo'>Pending</option>
                                        <option value="1"'; if($data['dealer']->status == '1'){ echo 'selected'; } echo'>Approve</option>
                                    </select>
                              </div>
                              
                          </div>
                              <input name="edit" type="submit" value="Edit dealer" style="padding:3px 25px;" class="btn btn-primary" />
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>';
} else {
?>
<div class="row" style="margin-top: 13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
                <h5>Dealers<a href="dealers?add" style="padding:3px 15px; font-size: 15px" class="add">Add Dealer</a></h5>
                <h5 class="card-title" style="color: #0d1baa;">Manage Your Dealers </h5>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="datatable-editable">
                        <thead>
                            <tr class="bg-blue">
                                <th>Sr. No.</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Mobile</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $sr = 1;
                        echo $data['notices'];
                        foreach ($data['dealers'] as $category){
                            echo'<tr>
                            <td>'.$sr.'</td>
                            ';
                            echo'<td>'.$category->name.'</td>
                            <td>'.$category->email.'</td>
                            <td>'.$category->mobile.'</td>
                            <td>
                                <a href="dealers?delete='.$category->id.'"><i class="icon-trash"></i></a>
                                <a href="dealers?edit='.$category->id.'"><i class="icon-pencil"></i></a>
                            </td>
                          </tr>
                          ';
                            $sr++; }
                        }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php
echo $data['footer'];
?>
<script src="<?=$data['tp']; ?>/admin/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.js"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$data['tp']; ?>/admin/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$data['tp']; ?>/admin/js/plugins.js" type="text/javascript"></script>
<script src="<?=$data['tp']; ?>/admin/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$data['tp']; ?>/admin/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$data['tp']; ?>/admin/js/select2.min.js"></script>
<script src="<?=$data['tp']; ?>/admin/js/scripts/ui-alerts.js" type="text/javascript"></script>
<!-- <script src="<?php //$data['tp']; ?>/js/scripts/data-tables.js" type="text/javascript"></script> -->
<script>
    $( document ).ready(function() {
        $('.select2').select2({
            placeholder: "Select top brands",
            allowClear: true
        });
    });
</script>