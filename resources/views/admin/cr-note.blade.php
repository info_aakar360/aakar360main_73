<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="keywords" content="">
    <meta name="author" content="ThemeSelect">
    <!--    <title>-->
    <!--        --><?//=$title; ?>
    <!--    </title>-->
    <link rel="apple-touch-icon" href="<?=$tp; ?>/admin/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/admin/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/vendors/data-tables/css/select.dataTables.min.css">

    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/custom/custom.css">
    <link href="<?=$tp; ?>/admin/css/select2.min.css" rel="stylesheet" />
    <!-- END: Custom CSS-->
</head>

<?php
echo $header;
if(isset($_GET['add'])) {
    echo $notices.'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Add Cr Note </h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="cr-note" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
 <form action="" method="post" style="max-width: 100%">
			'.csrf_field().'
			
			<div class="row">
				<div class="input-field col s12 m6">
					Seller Name
					<select name="seller_id" class="form-control" id="seller_id" required>
					    <option value="">Please Select Seller</option>';
					    foreach ($sellers as $sell){
					        echo '<option value="'.$sell->id.'">'.$sell->name.'</option>';
					    }
                    echo '</select>
				</div>
				
				
				<div class="input-field col s12 m6">
					Invoice No.
					<select name="order_id" class="form-control" id="order_id" onchange="getAmount();" required>
					    <option value="">Please Select Invoice No</option>';
					    foreach ($orderid as $od){
					        echo '<option value="'.$od->order_id.'">'.$od->order_id.'</option>';
					    }
                        echo '</select>
				</div>
				</div>
				<div class="row">
				<div class="input-field col s12 m6">
					Amount
					<input type="text" readonly name="amount" class="form-control" required/>
				</div>
				<div class="input-field col s12 m6">
					Cr Amount
					<input type="text" name="paid_amount" class="form-control" required/>
				</div>
				</div>
				<div class="row">
				<div class="input-field col s12 m6">
					Balance Amount
					<input type="text" readonly name="balance_amount" class="form-control" required/>
				</div>
				
				<div class="input-field col s12 m6">
					Payment Data
					<textarea name="payment_data" class="summernote" rows="10" cols="80" required></textarea>
				</div>
				</div>
				<div class="row">
				<div class="input-field col s12 m6">
					Description
					<textarea name="description" class="summernote" rows="10" cols="80" required></textarea>
				</div>
				<div class="input-field col s12 m6">
					Remark
					<textarea name="remark" class="summernote" rows="10" cols="80" required></textarea>
				</div>
				</div>
				<div class="row">
				<input name="add" type="submit" value="Add Note" style="padding: 3px 25px;" class="btn btn-primary" />
			</div>
			</form>
			</div>
			</div>
			</div>
			</div>';
} else {
    ?>
<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
                <h3>Cr Note<a href="cr-note?add" style="padding:3px 15px;" class="add">Add Note</a></h3>
                <h5 class="card-title" style="color: #0d1baa;">Cr Note</h5>

                <div class="divider"></div>
                <div class="row" style="position:relative;">
                    <div class="col s12 table-responsive"><!--1x 11661593 6YDgrn-->
                        <table id="datatable-editable" class="display">
            <thead>
            <tr role="role">
                <th>Sr. No.</th>
                <th>Seller</th>
                <th>Cr. Amount</th>
                <th>Description</th>
                <th>Date</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $sr = 1;
            echo $notices;
            foreach ($mpayments as $mpay) {
                echo '<tr>
                    <td>' . $sr . '</td>';
                $u = $mpay->seller_id;
                $user = DB::select("SELECT * FROM customers WHERE id = '$u'")[0];
                echo '<td>' . $user->name . '</td>
                    <td>' . $mpay->paid_amount . '</td>
                    <td>' . $mpay->description . '</td>
                    <td>' . $mpay->date_created . '</td>
                    </td>
                </tr>';
                $sr++;
            }
            ?>
            </tbody>
        </table>
    </div>
    <?php
}
?>

<style>
    .button {
        background: gainsboro;
        padding: 5px 20px;
        cursor: pointer;
        border-radius: 30px;
    }
    .mini-button {
        cursor: pointer;
        border-radius: 30px;
        background: transparent;
        padding: 5px 0px;
        display: block;
    }

    .modal-window {
        position: fixed;
        background-color: rgba(200, 200, 200, 0.75);
        top: -70px;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 999;
        opacity: 0;
        pointer-events: none;
        -webkit-transition: all 0.3s;
        -moz-transition: all 0.3s;
        transition: all 0.3s;
    }

    .modal-window:target {
        opacity: 1;
        pointer-events: auto;
    }

    .modal-window>div {
        width: 800px;
        position: relative;
        margin: 10% auto;
        padding: 2rem;
        background: #fff;
        color: #444;
    }

    .modal-window header {
        font-weight: bold;
    }

    .modal-close {
        color: #aaa;
        line-height: 50px;
        font-size: 80%;
        position: absolute;
        right: 0;
        text-align: center;
        top: 0;
        width: 70px;
        text-decoration: none;
    }

    .modal-close:hover {
        color: #000;
    }

    .modal-window h1 {
        font-size: 150%;
        margin: 0 0 15px;
    }
</style>

                    <!-- BEGIN VENDOR JS-->
                    <script src="<?=$tp; ?>/admin/js/vendors.min.js" type="text/javascript"></script>
                    <!-- BEGIN VENDOR JS-->
                    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
                    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
                    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.js"></script>
                    <!-- BEGIN PAGE VENDOR JS-->
                    <script src="<?=$tp; ?>/admin/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
                    <!-- END PAGE VENDOR JS-->
                    <!-- BEGIN THEME  JS-->
                    <script src="<?=$tp; ?>/admin/js/plugins.js" type="text/javascript"></script>
                    <script src="<?=$tp; ?>/admin/js/custom/custom-script.js" type="text/javascript"></script>
                    <script src="<?=$tp; ?>/admin/js/scripts/customizer.js" type="text/javascript"></script>
                    <!-- END THEME  JS-->
                    <!-- BEGIN PAGE LEVEL JS-->

                    <!-- END PAGE LEVEL JS-->
                    <script src="<?=$tp; ?>/admin/js/select2.min.js"></script>
                    <script src="<?=$tp; ?>/admin/js/scripts/ui-alerts.js" type="text/javascript"></script>
                    <!-- <script src="<?php //$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script> -->
<script>
    function getAmount(){
        var seller = $('#seller_id').val();
        var order = $('#order_id').val();
        $.ajax({
            url: 'get-amount',
            type: 'post',
            data: 'seller_id='+seller+'&order_id='+order+'&_token=<?=csrf_token(); ?>',
            success: function(datax){
                var data = JSON.parse(datax);
                $('input[name=amount]').val(data.amount);
                $('input[name=balance_amount]').val(data.balance_amount);
            }
        });
    }
</script>
<?php
echo $footer;
?>
