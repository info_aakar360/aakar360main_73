<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="keywords" content="">
    <meta name="author" content="ThemeSelect">
    <link rel="apple-touch-icon" href="<?=$data['tp']; ?>/admin/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$data['tp']; ?>/admin/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/vendors/data-tables/css/select.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/pages/data-tables.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/custom/custom.css">
    <link href="<?=$data['tp']; ?>/admin/css/select2.min.css" rel="stylesheet" />
</head>
<?php
echo $data['header'];
if(isset($_GET['add_pincodes'])){
    echo $data['notices'];
        echo '<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888; min-height: 600px;">
            <div class="card-content">
                <h5>Add Pincode</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="cities" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
                <form action="" method="post" enctype="multipart/form-data" style="max-width: 100%">
                '.csrf_field().'
                    <div class="row">
                          <div class="input-field">
                                From Weight
                                <input type="text" name="from" class="form-control" required>
                          </div>
                          <div class="input-field">
                                To Weight
                                <input type="text" name="to" class="form-control" required>
                          </div>
                          <div class="input-field col s4 m12">
                                Unit
                                <select name="unit" class="form-control">
                                    <option value="">Please Select</option>';
                                    foreach ($data['units'] as $category) {
                                        echo '<option value="'.$category->id.'">'.$category->name.'</option>';
                                    }
                                echo '</select>
                          </div>
                          <div class="input-field col s4 m12">
                                Pincodes
                                <textarea type="text" name="pincode" class="form-control" placeholder="Add Comma Saprated value" required></textarea>
                          </div>
                          <div class="input-field col s4 m12">
                                Shipping Charges
                                <input type="text" name="price" class="form-control" required>
                          </div>
                      </div>
                      <input name="add_pincode" type="submit" value="Add Pincode" style="padding:3px 25px;" class="btn btn-primary" />
                    </div>
                </form>
                <div class="row">
                    <div class="col s12 m12">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="datatable-editable3">
                                <thead>
                                    <tr class="bg-blue">
                                        <th>Sr. No.</th>
                                        <th>Truck Load</th>
                                        <th>Pincodes</th>
                                        <th>Shipping Charges</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>';
                                $sr = 1;
                                    foreach ($data['pincodes'] as $pincode) {
                                        echo '<tr>
                                            <td>'.$sr.'</td>
                                            <td>'.$pincode->from_weight.'-'.$pincode->to_weight.' '.getUnitSymbol($pincode->unit).'</td>
                                            <td>'.$pincode->pincode.'</td>
                                            <td>'.$pincode->price.'</td>
                                            <td>
                                                <a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="cities?edit_pincode='.$pincode->id.'"><i class="material-icons">edit</i></a>
                                            </td>
                                        </tr>';
                                    $sr++; }
                                echo '</tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>';
} elseif(isset($_GET['edit_pincode'])){
    echo $data['notices'];
    echo '<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content">
                <h5>Edit Pincode</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="cities" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
                <form action="" method="post" enctype="multipart/form-data" style="max-width: 100%">
                '.csrf_field().'
                    <div class="row">
                          <div class="input-field">
                                From Weight
                                <input type="text" name="from" class="form-control" required>
                          </div>
                          <div class="input-field">
                                To Weight
                                <input type="text" name="to" class="form-control" required>
                          </div>
                          <div class="input-field col s4 m12">
                                Category
                                <select name="category_id" class="form-control">
                                    <option value="">Please Select</option>';
                                    foreach ($data['units'] as $category) {
                                        $selected = '';
                                        if($data['pincode']->category_id == $category->id){
                                            $selected = 'selected';
                                        }
                                        echo '<option value="'.$category->id.'" '.$selected.'>'.$category->name.'</option>';
                                    }
                                echo '</select>
                          </div>
                          <div class="input-field col s4 m12">
                                Pincode
                                <textarea type="text" name="pincode" class="form-control" required>'.$data['pincode']->pincode.'</textarea>
                          </div>
                          <div class="input-field col s4 m12">
                                Shipping Charges
                                <input type="text" name="price" class="form-control" value="'.$data['pincode']->price.'" required>
                          </div>
                      </div>
                      <input name="edit_pincode" type="submit" value="Update Pincode" style="padding:3px 25px;" class="btn btn-primary" />
                    </div>
                </form>
            </div>
        </div>
    </div>';
} elseif(isset($_GET['edit'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
                    <h5>Add Pincode</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="cities" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
                    <form action="" method="post" enctype="multipart/form-data" style="max-width:  100%">
                    '.csrf_field().'
                        <div class="row">
                              <div class="input-field col s12 m12">
                                Availability
                                <select name="is_available" class="form-control">
                                    <option value="">Please Select</option>
                                    <option value="1"'; if($data['city']->is_available == '1'){ echo 'selected'; } echo '>Yes</option>
                                    <option value="0"'; if($data['city']->is_available == '0'){ echo 'selected'; } echo '>No</option>
                                </select>
                              </div>
                              <div class="input-field col s12 m12">
                                Image
                                <input type="file" name="image" class="form-control" accept="image/*"> 
                              </div>
                          </div>
                          <input name="edit" type="submit" value="Update Pincodes" style="padding:3px 25px;" class="btn btn-primary" />
                        </div>
                    </form>
            </div>
        </div>
    </div>';
} else {
?>
<div class="row" style="margin-top: 13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
                <h5>Cities</h5>
                <h5 class="card-title" style="color: #0d1baa;">Manage Your Cities </h5>
                <div class="col s12 m12">
                    <div class="col s10 m10">
                        <label> Select State</label>
                        <select class="browser-default" name="state_id" id="state_id">
                            <option value="">Select State</option>
                            <?php foreach ($data['states'] as $state){ ?>}
                                <option value="<?php echo $state->id; ?>"><?php echo $state->name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col s2 m2">
                        <button class="btn myblue waves-effect waves-light submit" style="padding:0 5px; margin-top: 5px" id="filterButton"><i class="material-icons right" style="margin-left:3px">search</i>Search
                        </button>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="cityTable">
                        <thead>
                            <tr class="bg-blue">
                                <th>Sr. No.</th>
                                <th>Name</th>
                                <th>State Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $sr = 1;
                        echo $data['notices'];
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<?php
echo $data['footer'];
?>
<script src="<?=$data['tp']; ?>/admin/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.js"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$data['tp']; ?>/admin/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$data['tp']; ?>/admin/js/plugins.js" type="text/javascript"></script>
<script src="<?=$data['tp']; ?>/admin/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$data['tp']; ?>/admin/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$data['tp']; ?>/admin/js/select2.min.js"></script>
<script src="<?=$data['tp']; ?>/admin/js/scripts/ui-alerts.js" type="text/javascript"></script>
<!-- <script src="<?php //$data['tp']; ?>/js/scripts/data-tables.js" type="text/javascript"></script> -->
<script>
    $( document ).ready(function() {
        $('.select2').select2({
            placeholder: "Select top brands",
            allowClear: true
        });
    });

    $('#cityTable').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "<?=url("admin/get-cities-data") ?>",
            type: 'GET',
            data: function (d) {
                d.state_id = $('#state_id').val();
            }
        },
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'state_id', name: 'state_id' },
            { data: 'action', name: 'action', orderable: false,searchable: false }
        ],
        order: [[0, 'desc']],
        dom: 'lBfrtip',
        buttons: <?=$data['buttons'];?>,
        fixedColumns: true,
        colReorder: true,
        exportOptions:{
            columns: ':visible'
        }
    });
    $('#filterButton').click(function(){
        $('#cityTable').DataTable().draw(true);
    });
</script>