<!DOCTYPE html>

<head xmlns="http://www.w3.org/1999/html">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="keywords" content="">
    <meta name="author" content="ThemeSelect">
    <!--    <title>-->
    <!--        --><?//=$title; ?>
    <!--    </title>-->
    <link rel="apple-touch-icon" href="<?=$tp; ?>/admin/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/admin/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/vendors/data-tables/css/select.dataTables.min.css">

    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/custom/custom.css">
    <link href="<?=$tp; ?>/admin/css/select2.min.css" rel="stylesheet" />
    <!-- END: Custom CSS-->
</head>
<style type="text/css">

    .modal-window {
        position: fixed;
        background-color: rgba(200, 200, 200, 0.75);
        top: -70px;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 999;
        opacity: 0;
        pointer-events: none;
        -webkit-transition: all 0.3s;
        -moz-transition: all 0.3s;
        transition: all 0.3s;
    }

    .modal-window:target {
        opacity: 1;
        pointer-events: auto;
    }

    .modal-window>div {
        width: 800px;
        position: relative;
        margin: 10% auto;
        padding: 2rem;
        background: #fff;
        color: #444;
    }

    .modal-window header {
        font-weight: bold;
    }

    .modal-close {
        color: #aaa;
        line-height: 50px;
        font-size: 80%;
        position: absolute;
        right: 0;
        text-align: center;
        top: 0;
        width: 70px;
        text-decoration: none;
    }

    .modal-close:hover {
        color: #000;
    }

    .modal-window h1 {
        font-size: 150%;
        margin: 0 0 15px;
    }
    </style>
<?php
echo $header;
echo $notices;
$i='test';
?>

<div class="row" style="margin-top: 22px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 650px;">

          <form action="" method="post" class=" col s12 m12 form-horizontal single">
          <?=csrf_field()?>



            <div class="form-group">
                <label class="control-label">Seller name</label>
                <input name="cid" type="hidden"  value="<?=$customer->id;?>" class="form-control" />
                <input name="name" type="text"  value="<?=$customer->name;?>" class="form-control" />
            </div>
            <div class="form-group">
                <label class="control-label">Email</label>
                <input name="email" type="text"  value="<?=$customer->email;?>" class="form-control" />
            </div>
            <div class="form-group">
                <label class="control-label">Mobile</label>
                <input name="mobile" type="text"  value="<?=$customer->mobile;?>" class="form-control" />
            </div>
            <div class="form-group">
                <label class="control-label">Company</label>
                <input name="company" type="text"  value="<?=$customer->company;?>" class="form-control" />
            </div>
            <div class="form-group">
                <label class="control-label">Pan Number</label>
                <input name="pan" type="text"  value="<?=$customer->pan;?>" class="form-control" />
            </div>
            <div class="form-group">
                <label class="control-label">GST Number</label>
                <input name="gst" type="text"  value="<?=$customer->gst;?>" class="form-control" />
            </div>
			<div class="form-group">
				<?php 
					$ycheck = '';
					$ncheck = '';
					if($customer->credit_charges == '1'){
						$ycheck = 'checked';
					}else{
						$ncheck = 'checked';
					}
				?>
				<label class="control-label">Credit Charges</label><br>
                <label style="margin-right: 5px; font-size: 12px;" class="radio_container">
				   <input name="credit_charges" type="radio" id="myes" value="1" <?=$ycheck; ?>/>
				   Yes
				   <input name="credit_charges" type="radio" id="myes" value="1" <?=$ycheck; ?>/>
				   <span class="checkmark"></span>
				</label> <br> 
				<label style="margin-right: 5px; font-size: 12px;" class="radio_container">
				   <input name="credit_charges" type="radio" id="mno" value="0" <?=$ncheck; ?> />
				   NO
				   <input name="credit_charges" type="radio" id="mno" value="0" <?=$ncheck; ?>/>
				   <span class="checkmark"></span>
				</label>
            </div>

            <input name="update" type="submit" value="Update" class="btn btn-primary" />

    </form>

            </div>
        </div>
    </div>
</div>

<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/admin/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.js"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/admin/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/admin/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/admin/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/admin/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/admin/js/select2.min.js"></script>
<script src="<?=$tp; ?>/admin/js/scripts/ui-alerts.js" type="text/javascript"></script>
<!-- <script src="<?php //$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script> -->
</body>

</html>
<?php echo $footer;?>