<?php echo $header;?>

<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
                <h5>Reviews</h5>
                <h5 class="card-title" style="color: #0d1baa;">Manage customer layout plans reviews and approve them</h5>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="datatable-editable">
                        <thead>
                        <tr class="bg-blue">
                            <th>Sr. No.</th>
                            <th>Product Name</th>
                            <th>Review</th>
                            <th>Customer Detail</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $sr = 1;
                        foreach ($reviews as $review){
                            echo'<tr>
            <td>'.$sr.'</td>
            <td>'; $d = DB::table('layout_plans')->where('id',$review->layout)->first(); if($d !== null){ echo $d->title.' - <b>'.$review->rating.' stars'; } else { echo 'NA'; } echo '</td>
            <td>'.nl2br(htmlspecialchars($review->review)).'</td>';
                            $crs = DB::table('customers')->where('id',$review->name)->first();
                            if($crs !== null){
                                echo '<td>'.$crs->name.' - '.$crs->email.'</td>';
                            }else{
                                echo '<td>NA</td>';
                            }
                            echo '<td>';
                            echo ($review->active != 1) ? '<a href="layout_reviews?approve='.$review->id.'"><i class="icon-like "></i></a>' : '<i class="icon-check"></i>
            </td>
          </tr>';
                            $sr++; }?>
                        </tbody>
                    </table>
                </div>
<?php
echo $footer;
?>