<?php
echo $data['header'];
if(isset($_GET['edit']))
{
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Edit payment method</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="payment" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post" style="max-width: 100%;">
				'.csrf_field().'
				
					<div class="row">
						<div class="input-field col s12 m4">
							Payment method title
							<input name="title" value="'.$data['method']->title.'" type="text"  class="form-control" required/>
						</div>';
    $method_options = json_decode($data['method']->options,true);
    foreach ($method_options as $key => $value){
        echo '<div class="input-field col s12 m4">
								'.$key.'
								<input name="'.$key.'" value="'.$value.'" type="text"  class="form-control" required/>
							</div>';
    }
    echo '
						<div class="input-field col s12 m4">
							Payment method status
							<select name="active" class="form-control" >
								<option '.($data['method']->active == 1 ? 'selected' : '').' value="1">Enabled</option>
								<option '.($data['method']->active == 0 ? 'selected' : '').' value="0">Disabled</option>
							</select>
						</div></div>
						<input name="edit" type="submit" value="Update payment method" style="padding:3px 25px;" class="btn btn-primary" />
					</div>
				</form>
				</div>
				</div>
				</div>
				</div>';
} else {
    ?>
    <div class="row" style="margin-top: -13px;">
        <div class="col s12">
            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                <div class="card-content" style="min-height: 650px;">
                    <h3>Payment</h3>
                    <p>Manage your website payment methods</p>

    <?php
    foreach ($data['methods'] as $method){
        echo'<div class="mini bloc">
			<h5>'.$method->title.' <b>('.$method->code.')</b>';
        echo ($method->active == 1) ?  ' <i class="icon-check"></i> ' : ' <i class="icon-close"></i> ';
        echo '<div class="tools"><a href="payment?edit='.$method->id.'"><i class="icon-pencil"></i></a></div></h5>
			</div>';
    }
}
echo $data['footer'];
?>