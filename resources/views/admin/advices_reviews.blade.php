	<?php echo $header;?>
    <div class="row" style="margin-top: 10px;">
        <div class="col s12">
            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                <div class="card-content" style="min-height: 550px;">

                    <h5 class="card-title" style="margin-bottom: 10px;">Manage Your Advice </h5>
    <div class="table-responsive">
        <table class="table table-striped table-bordered" id="datatable-editable">
            <thead>
            <tr class="bg-blue">
                <th>Sr. No.</th>
                <th>Advice Name</th>
                <th>Review</th>
                <th>Customer Detail</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
	<?php
    $sr = 1;
		foreach ($reviews as $review){
			echo'<tr>
            <td>'.$sr.'</td>
            <td>'. $blog = \App\Blog::where('id',$review->advice_id)->select('title')->first();  if(!empty($blog)){echo $blog->title;}' - <b>'.$review->rating.' stars</td>
            <td>'.nl2br(htmlspecialchars($review->review)).'</td>';
            $crs = DB::select("SELECT * FROM customers WHERE id = '".$review->name."'")['0'];
            echo '<td>'.$crs->name.' - '.$crs->email.'</td>
            <td>';
                echo ($review->active != 1) ? '<a href="advices_reviews?approve='.$review->id.'"><i class="icon-like "></i></a>' : '<i class="icon-check"></i>
            </td>
          </tr>';
		$sr++; }?>
            </tbody>
        </table>
    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <?php
	echo $footer;
	?>