<script>
    var links = document.querySelectorAll('a');
    for (var i = 0; i < links.length; i++) {
        if (links[i].getAttribute('href').match(/delete=/g)){
            links[i].addEventListener('click', function(event) {
                event.preventDefault();
                var choice = confirm('Are you sure you want to delete this item?');
                if (choice) {
                    window.location.href = this.getAttribute('href');
                }
            });
        }
    }
</script>

<script>
$(document).ready(function() {
    $('#datatable-editable, #datatable-editable1, #datatable-editable2, #datatable-editable3').DataTable( {
        dom: 'Bfrtip',responsive:true,
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
//$(".switch-check").bootstrapSwitch();
});
$(document).on('click', '.caretx', function(){
    $(this).next('.nested').toggleClass('active');
    $(this).toggleClass("caretx-down");
});
</script>

<script src="<?=$tp?>/assets/jquery.dataTables.min.js"></script>
    <script src="<?=$tp?>/assets/dataTables.buttons.min.js"></script>
    <script src="<?=$tp?>/assets/buttons.flash.min.js"></script>
    <script src="<?=$tp?>/assets/jszip.min.js"></script>
    <script src="<?=$tp?>/assets/pdfmake.min.js"></script>
    <script src="<?=$tp?>/assets/vfs_fonts.js"></script>
    <script src="<?=$tp?>/assets/buttons.html5.min.js"></script>
    <script src="<?=$tp?>/assets/buttons.print.min.js"></script>
            <script src="<?=$tp?>/assets/ckeditor/ckeditor.js"></script>
			<script src="<?=$tp?>/admin/select2.js"></script>
			<script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=r57pgc4blx3klo4t327mktax8eobrp00t7sahz2a7r0ob1a5"></script>
	<script src="<?=$tp; ?>/admin/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.js"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/admin/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/admin/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/admin/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/admin/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/admin/js/select2.min.js"></script>
<script src="<?=$tp; ?>/admin/js/scripts/ui-alerts.js" type="text/javascript"></script>

<script src="<?=$tp?>/admin/js/moment.min.js"></script>
<script src="<?=$tp?>/admin/js/bootstrap-datetimepicker.min.js"></script>
<!-- <script src="<?php //$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script> -->

<!--            <footer class="page-footer footer footer-static footer-dark gradient-45deg-indigo-purple gradient-shadow navbar-border navbar-shadow">-->
<!--                <div class="footer-copyright">-->
<!--                    <div class="container"><span>&copy; 2019          <a href="" target="_blank">Adroweb</a> All rights reserved.</span><span class="right hide-on-small-only">Design and Developed by <a href="">Adroweb</a></span></div>-->
<!--                </div>-->
<!--            </footer>-->