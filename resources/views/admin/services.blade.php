<?php
echo $data['header'];
if(isset($_GET['add'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Add New Services</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="services" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
                <form action="" method="post" enctype="multipart/form-data" style="max-width: 100%;">
			'.csrf_field().'
			
				<div class="row">
					  <div class="input-field col s12 m6">
						Title
						<input name="title" type="text"  class="form-control" />
					  </div>
					                       
					  <div class="input-field col s12 m6">
						Description
						<textarea name="description" class="form-control" ></textarea>
					  </div>
					  </div>
					  <div class="row">
					  <div class="input-field col s12 m6">
						Services images
					    <input type="file" class="form-control" name="images[]" multiple="multiple" accept="image/*"/>
					  </div>
					  <div class="input-field col s12 m6">
						Price
						<input name="price" type="text" class="form-control"  />
					  </div>
					   </div>
					   <div class="row">
						<div class="input-field col s12 m6">
							Display In Home Page<br>
							<input name="display" type="radio" id="yes" value="1" />Yes &nbsp; <input name="display" type="radio" id="no" value="0" />No
						  </div>
					  <div class="input-field col s12 m6">
							Category
							<select name="category" class="form-control select2">
							<option value="0"></option>';
    foreach ($data['parents'] as $parent){
        echo '<option value="'.$parent->id.'">'.$parent->name.'</option>';
    }
    echo '</select>
					  </div>
					  </div>
					  <div class="row">
					  <div class="input-field col s12 m6">
                        Payment Mode<br>
                        <select name="payment_mode" class="form-control">
                            <option value="">Please Select Payment Mode</option>
                            <option value="instant">Instant</option>
                            <option value="later">Later</option>
                        </select>
                      </div>
					   <div class="input-field col s12 m6">
						Tax In Percent
						<input name="tax" type="text" class="form-control"  />
					  </div>
					  </div>
					  <input name="add" type="submit" value="Add services" class="btn btn-primary" />
				
			</form>';
} else if(isset($_GET['edit'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Edit Services</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="services" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
                <form action="" method="post"  enctype="multipart/form-data" style="max-width: 100%;">
			'.csrf_field().'
			    <div class="row">
					  <div class="input-field col s12 m6">
						Title
						<input name="title" type="text"  value="'.$data['services']->title.'" class="form-control" />
					  </div>
					  <div class="input-field col s12 m6">
						Price
						<input name="price" type="text" value="'.$data['services']->price.'" class="form-control"  />
					  </div>
					  </div>
                    <div class="row">
					  <div class="form-group">
						Description
						<textarea name="description" class="form-control">'.$data['services']->description.'</textarea>
					  </div></div><div class="row">';
    if (!empty($data['services']->images)){
        echo '<p>Uploading new images will overwrtite current images .</p>';
        $images = explode(',',$data['services']->images);
        foreach($images as $image){
            echo '<img class="col-md-2" src="'.url('/assets/services/'.$image).'" />';
        }
        echo '<div class="clearfix"></div>';
    }
    echo '<div class="input-field col s12 m6">
							Services images
							<input type="file" class="form-control" name="images[]" multiple="multiple" accept="image/*"/>
						  </div></div>
						  <div class="row">
						  <div class="input-field col s12 m6">
							  Display In Home Page<br>';
    $val = $data['services']->display;
    if($val == '1'){
        echo '<input name="display" type="radio" id="yes" checked="checked" value="1" />Yes &nbsp; <input name="display" type="radio" id="no" value="0" />No';
    }else {
        echo '<input name="display" type="radio" id="yes" value="1" />Yes &nbsp; <input name="display" type="radio" checked="checked"  id="no" value="0" />No';
    }
    echo '</div>
					  <div class="input-field col s12 m6">
							Category
							<select name="category" class="form-control select2">
							<option value="0"></option>';
    foreach ($parents as $parent){
        echo '<option value="'.$parent->id.'" '.($parent->id == $data['services']->category ? 'selected' : '').'>'.$parent->name.'</option>';
    }
    echo '</select>
					  </div></div>
					  <div class="row" style="margin-bottom: 10px;">
					  <div class="input-field col s12 m6">
                        Payment Mode<br>
                        <select name="payment_mode" class="form-control">';
    if($data['services']->payment_mode == 'instant') {
        echo '<option value="">Please Select Payment Mode</option>
                                <option value="instant" selected>Instant</option>
                                <option value="later">Later</option>';
    }else if($data['services']->payment_mode == 'later') {
        echo '<option value="">Please Select Payment Mode</option>
                                <option value="instant" >Instant</option>
                                <option value="later" selected>Later</option>';
    }
    echo '</select>
                      </div>
					  <div class="input-field col s12 m6">
						Tax
						<input name="price" type="text" value="'.$data['services']->tax.'" class="form-control"  />
					  </div>
					  </div>
					  <input name="edit" type="submit" value="Edit services" style="padding: 3px 25px;" class="btn btn-primary" />
				</div>
			</form>
			</div>
			</div>
			</div>
			</div>';
} else {
?>
<div class="row" style="margin-top: 10px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
                <h5>Services<a href="services?add" style="padding:3px 15px; font-size: 15px" class="add">Add Services</a></h5>
                <h5 class="card-title" style="color: #0d1baa;">Manage Your Services </h5>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="datatable-editable">
                        <thead>
                        <tr class="bg-blue">
                            <th>Sr. No.</th>
                            <th>Service Image</th>
                            <th>Service Name</th>
                            <th>Display</th>
                            <th>Payment Mode</th>
                            <th>Tax</th>
                            <th>Service Category</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $sr = 1;
                        echo $data['notices'];
                        foreach ($data['services'] as $service){
                            echo'<tr>
            <td>'.$sr.'</td>
            <td><img src="../assets/services/'.image_order($service->images).'" style="height: 100px; width: 100px;"></td>
            <td>'.$service->title.'</td>';
                            if($service->display == '0'){
                                $disp = 'No';
                            }else{
                                $disp = 'Yes';
                            }
                            echo'<td>'.$disp.'</a></td>
            <td>'.$service->payment_mode.'</td>
            <td>'.$service->tax.' % </td>
                ';

                            $cat = $service->category;
                            $parents = DB::table("services_category")->where('id', '=', $cat)->first();
                            if($parents !== null)
                            {
                                echo '<td>' . $parents->name . '</td>';
                            }
                            else{
                                echo '<td>NA</td>';
                            }

                            echo '<td><a href="services?delete='.$service->id.'"><i class="icon-trash"></i></a>
				<a href="services?edit='.$service->id.'"><i class="icon-pencil"></i></a>
				<a href="update-questions?edit='.$service->id.'"><i class="icon-question"></i></a>
            </td>
          </tr>';


                            echo'</div>
	</div>';
                            $sr++;}
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<style>
    .button {
        background: gainsboro;
        padding: 5px 20px;
        cursor: pointer;
        border-radius: 30px;
    }
    .mini-button {
        cursor: pointer;
        border-radius: 30px;
        background: transparent;
        padding: 5px 0px;
        display: block;
    }
</style>

<?php
echo $data['footer'];
?>



