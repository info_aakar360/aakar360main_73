<?php
echo $header;
if(isset($_GET['add'])) {
    echo $notices.'<form action="" method="post" class="form-horizontal single">
			'.csrf_field().'
			<h5><a href="length"><i class="icon-arrow-left"></i></a>Add new Unit</h5>
				<fieldset>
					  <div class="form-group">
						<label class="control-label">Unit Name</label>
						<input name="name" type="text"  class="form-control" />
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Unit Symbol</label>
						<input name="symbol" type="text"  class="form-control" />
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Unit</label>
						<input name="unit" type="text"  class="form-control" />
					  </div>
					  
					  <input name="add" type="submit" value="Submit" class="btn btn-primary" />
				</fieldset>
			</form>';
} elseif(isset($_GET['edit'])) {

    echo $notices.'<form action="" method="post" class="form-horizontal single">
			'.csrf_field().'
			<h5><a href="length"><i class="icon-arrow-left"></i></a>Edit Units</h5>
				<fieldset>
					  <div class="form-group">
						<label class="control-label">Unit Name</label>
						<input name="name" type="text" value="'.$link->name.'" class="form-control" />
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Unit Symbol</label>
						<input name="symbol" type="text" value="'.$link->symbol.'" class="form-control" />
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Unit</label>
						<input name="unit" type="text" value="'.$link->unit.'" class="form-control" />
					  </div>
					  
					  <input name="edit" type="submit" value="Update" class="btn btn-primary" />
				</fieldset>
			</form>';
} else {
    ?>
    <div class="head">
        <h3>Unit<a href="length?add" class="add">Add Unit</a></h3>
        <p>Manage Units</p>
    </div>
<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-bordered" id="datatable-editable">
                <thead>
                <tr class="bg-blue">
                    <th>Sr. No.</th>
                    <th>Name</th>
                    <th>Symbol</th>
                    <th>Unit</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $sr = 1;
                echo $notices;
                foreach ($links as $category){
                    echo'<tr>
                        <td>'.$sr.'</td>
                        <td>'.$category->name.'</td>
                        <td>'.$category->symbol.'</td>
                        <td>'.$category->unit.'</td>
                        <td>
                            <a href="length?delete='.$category->id.'"><i class="icon-trash"></i></a>
                            <a href="length?edit='.$category->id.'"><i class="icon-pencil"></i></a>
                        </td>
                      </tr>
                      ';
                    $sr++;
                }?>
                </tbody>
            </table>
        </div>
    </div>
</div>
    <?php
}
echo $footer;
?>
<script>
function removeThis($id){
	event.preventDefault();
	$('[data='+$id+']').remove();
}
$('#add-option').on('click', function(){
	var id = $('#que-type > div').length;
	var html = '<div class="col-md-12 options" data="'+id+'"><label class="control-label col-md-12" data="'+id+'">Option '+(id+1)+'</label><input name="option[]" type="text" data="'+id+'" class="form-control col-md-11" style="width: 90%;margin-right: 5px;" /><a onClick="removeThis('+id+');" class="pul-right" style="line-height: 3;" data="'+id+'"><i class="icon-close" style="font-size: 20px;"></i></a></div>';
	$('#add-option').before(html);
});
function getData(data){
	if(data == 'text' || data == 'textarea'){
		$('.options').hide();
	}else{
		$('.options').show();
	}	
}


function addit(val){
    var da = val;
    $.ajax({
        url: 'catData',
        type: 'post',
        data: 'page='+da+'&_token=<?=csrf_token(); ?>',
        success: function(data){
            $('#catdata').html(data);
        }
    });
}

</script>