<!DOCTYPE html>
<html>
	<head>
		<meta charset='utf-8'/>
		<title><?=$title?></title>
		<meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
		<meta content='<?=$cfg->desc?>' name='description'/>
		<meta content='<?=$cfg->key?>' name='keywords'/>
		<!--ASSETS-->
		<base href="<?=url('admin')?>/" />
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400" rel="stylesheet"> 
		
		<link rel="stylesheet" href="<?=$tp?>/admin/select2.css">
		<link rel="stylesheet" href="<?=$tp?>/admin/style.css">
		<link rel="stylesheet" href="<?=$tp?>/assets/dataTables.bootstrap4.css">
		<link rel="stylesheet" href="<?=$tp?>/assets/jquery.dataTables.min.css">
		<link rel="stylesheet" href="<?=$tp?>/assets/buttons.dataTables.min.css">
        <script src="<?=$tp?>/assets/jquery.min.js"></script>
		<script src="<?=$tp?>/assets/bootstrap.min.js"></script>
		<script src="<?=$tp?>/assets/Chart.min.js"></script>
        <script src="<?=$tp?>/assets/jquery.dataTables.js"></script>
        <script src="<?=$tp?>/assets/dataTables.bootstrap4.js"></script>
        <script src="<?=$tp?>/assets/todolist.js"></script>
        
        
	</head>
	<body>
			<aside class="sidebar-left">
                <nav class="navbar navbar-inverse">
                    <div>
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#" style="width: 200px;""><img src="<?=url('assets/sellerkit.png')?>" style="width:100px;height:auto;"></a>
                        </div>
                        <div class="panel-group" id="accordion">
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title" <?=($area == "index" ? "class=active" : "")?>>
                                            <a href="<?=url('admin')?>"><i class="icon-home"></i> <span>Dashboard</span></a>
                                        </h4>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                Seller <i class="icon-plus" style="float: right;"></i>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        <a href="seller" <?=($area == "seller" ? "class=active" : "")?>><span>Seller</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="pendingproduct" <?=($area == "pendingproduct" ? "class=active" : "")?>><span>Pending Product</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="approvalproduct" <?=($area == "approvalproduct" ? "class=active" : "")?>><span>Approval Product</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="rejectproduct" <?=($area == "rejectroduct" ? "class=active" : "")?>><span>Reject Product</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="deleteproduct" <?=($area == "deleteproduct" ? "class=active" : "")?>><span>Deleted Product</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="sellers-payment" <?=($area == "sellers-payment" ? "class=active" : "")?>><span>Sellers Payment</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="seller_po" <?=($area == "seller_po" ? "class=active" : "")?>><span>Sellers PO</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="cr-note" <?=($area == "cr-note" ? "class=active" : "")?>><span>Cr Note</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="dr-note"  <?=($area == "dr-note" ? "class=active" : "")?>><span>Dr Note</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseIns">
                                                Institutional<i class="icon-plus" style="float: right;"></i>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseIns" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        <a href="institutional" <?=($area == "institutional" ? "class=active" : "")?>><span>Users</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="bookings" <?=($area == "bookings" ? "class=active" : "")?>><span>Bookings</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="dispatch-programs" <?=($area == "dispatch" ? "class=active" : "")?>><span>Dispatch Programs</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                                Frontend<i class="icon-plus" style="float: right;"></i>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        <a href="banners" <?=($area == "banners" ? "class=active" : "")?>><span>Manage Banners</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="home-page-design" <?=($area == "home-page-design" ? "class=active" : "")?>><span>Manage Home Page Design</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="mega-menu-image" <?=($area == "mega-menu-image" ? "class=active" : "")?>><span>Manage Mega Menu Image</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="default_image" <?=($area == "default_image" ? "class=active" : "")?>><span>Manage Default Image</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="link" <?=($area == "llink" ? "class=active" : "")?>><span>Manage link</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="pages" <?=($area == "pages" ? "class=active" : "")?>><span>Pages</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                                Products<i class="icon-plus" style="float: right;"></i>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        <a href="manufacturing-hubs" <?=($area == "mhubs" ? "class=active" : "")?>><i class="icon-home"></i> <span>Manufacturing Hubs</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="products" <?=($area == "products" ? "class=active" : "")?>><i class="icon-bag"></i> <span>Products</span></a>
                                                    </td>
                                                </tr>
                                                <!--<tr>
                                                    <td>
                                                        <a href="ph-relations" <?/*=($area == "phrelations" ? "class=active" : "")*/?>><i class="icon-bag"></i> <span>Products-Hub Relations</span></a>
                                                    </td>
                                                </tr>-->
                                                <tr>
                                                    <td>
                                                        <a href="categories" <?=($area == "categories" ? "class=active" : "")?>><span>Categories</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="product-type" <?=($area == "product-type" ? "class=active" : "")?>><span>Product Grade</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="units" <?=($area == "units" ? "class=active" : "")?>><span>Weight Units</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="size" <?=($area == "size" ? "class=active" : "")?>><span>Variant Sizes</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="sizes_group" <?=($area == "sizes_group" ? "class=active" : "")?>><span>Variant Sizes Group</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="filters" <?=($area == "filters" ? "class=active" : "")?>><span>Filters</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="best-for-cat" <?=($area == "best-for-cat" ? "class=active" : "")?>><span>Best For Categories</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="brands" <?=($area == "brands" ? "class=active" : "")?>><span>Brands</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="featured" <?=($area == "featured" ? "class=active" : "")?>><span>Suggested Products</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="customers" <?=($area == "customers" ? "class=active" : "")?>><span>Customers</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="coupons" <?=($area == "coupons" ? "class=active" : "")?>><span>Coupons</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <?php $rew = DB::select("SELECT COUNT(*) as count FROM reviews WHERE active = 0")[0]->count;?>
                                                        <a href="reviews" <?=($area == "reviews" ? "class=active" : "")?>><span>Reviews </span> <span class="label label-info"><?=$rew; ?></span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <?php $ppenfaq = DB::select("SELECT COUNT(*) as count FROM product_faq WHERE status = 'Pending'")[0]->count;?>
                                                        <a href="product_faq" <?=($area == "product_faq" ? "class=active" : "")?>><span>Product FAQ</span> <span class="label label-info"><?=$ppenfaq; ?></span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                                Advices<i class="icon-plus" style="float: right;"></i>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseFour" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        <a href="blog" <?=($area == "blog" ? "class=active" : "")?>><span>Advices</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="best-for-advices" <?=($area == "best-for-advices" ? "class=active" : "")?>><span>Best For Advices</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <?php $apenfaq = DB::select("SELECT COUNT(*) as count FROM advices_faq WHERE status = 'Pending'")[0]->count;?>
                                                        <a href="advices_faq" <?=($area == "advices_faq" ? "class=active" : "")?>><span>Advices FAQ</span> <span class="label label-info"><?=$apenfaq; ?></span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <?php $arew = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 0")[0]->count;?>
                                                        <a href="advices_reviews" <?=($area == "advices_reviews" ? "class=active" : "")?>><span>Advices Reviews</span> <span class="label label-info"><?=$arew; ?></span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="advices_category" <?=($area == "advices_category" ? "class=active" : "")?>><span>Advices Categories</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                                                Services<i class="icon-plus" style="float: right;"></i>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseFive" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        <a href="services_categories" <?=($area == "services_categories" ? "class=active" : "")?>><span>Categories</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="services" <?=($area == "services" ? "class=active" : "")?>><span>Services</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <?php $serreq = DB::select("SELECT COUNT(*) as count FROM services_answer WHERE status = 'Pending'")[0]->count;?>
                                                        <a href="services_answer" <?=($area == "services_answer" ? "class=active" : "")?>><span>Services Request(s)</span> <span class="label label-info"><?=$serreq; ?></span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="questions" <?=($area == "questions" ? "class=active" : "")?>><span>Questions Master</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="s-how-it-works" <?=($area == "s-how-it-works" ? "class=active" : "")?>><span>How It Works</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <?php $penfaq = DB::select("SELECT COUNT(*) as count FROM faq WHERE status = 'Pending'")[0]->count;?>
                                                        <a href="faq" <?=($area == "faq" ? "class=active" : "")?>><span>Services FAQ</span> <span class="label label-info"><?=$penfaq; ?></span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
                                                Design<i class="icon-plus" style="float: right;"></i>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseSix" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        <a href="design_category" <?=($area == "design_category" ? "class=active" : "")?>><span>Categories</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="design-cat-banner" <?=($area == "design-cat-banner" ? "class=active" : "")?>><span>Design Category Banner</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="best-for-layout" <?=($area == "best-for-layout" ? "class=active" : "")?>><span>Best For Layout</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="layout-plans" <?=($area == "layout_plans" ? "class=active" : "")?>><span>Layout Plans</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="layout_addon" <?=($area == "layout_addon" ? "class=active" : "")?>><span>Layout Addon</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="photos" <?=($area == "photos" ? "class=active" : "")?>><span>Photos</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="best-for-photo" <?=($area == "best-for-photo" ? "class=active" : "")?>><span>Best For Photo</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <?php $prew = DB::select("SELECT COUNT(*) as count FROM photo_reviews WHERE active = 0")[0]->count;?>
                                                        <a href="photo-reviews" <?=($area == "photo-reviews" ? "class=active" : "")?>><span>Photo Reviews</span> <span class="label label-info"><?=$prew; ?></span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <?php $lrew = DB::select("SELECT COUNT(*) as count FROM layout_reviews WHERE active = 0")[0]->count;?>
                                                        <a href="layout_reviews" <?=($area == "layout_reviews" ? "class=active" : "")?>><span>Layout Reviews</span> <span class="label label-info"><?=$lrew; ?></span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="amminities" <?=($area == "amminities" ? "class=active" : "")?>><span>Amminities</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <?php $penlfaq = DB::select("SELECT COUNT(*) as count FROM layout_faq WHERE status = 'Pending'")[0]->count;?>
                                                        <a href="layout_faq" <?=($area == "layout_faq" ? "class=active" : "")?>><span>Layout Faq's</span> <span class="label label-info"><?=$penlfaq; ?></span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <?php $modify = DB::select("SELECT COUNT(*) as count FROM modify_plan WHERE status = 'Pending'")[0]->count;?>
                                                        <a href="modify_plan"  <?=($area == "modify_plan" ? "class=active" : "")?>><span>Modify Plan Request(s)</span> <span class="label label-info"><?=$serreq; ?></span></a></li>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="image_likes" <?=($area == "image_likes" ? "class=active" : "")?>><span>Photo Likes</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="image_inspirations" <?=($area == "image_inspirations" ? "class=active" : "")?>><span>Photo Inspirations</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="plan_orders" <?=($area == "plan_orders" ? "class=active" : "")?>><span>Plan Orders</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
                                                Sales<i class="icon-plus" style="float: right;"></i>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseSeven" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        <a href="orders" <?=($area == "orders" ? "class=active" : "")?>><span>Orders</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="stats" <?=($area == "stats" ? "class=active" : "")?>><span>Statistics</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseEight">
                                                Marketing<i class="icon-plus" style="float: right;"></i>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseEight" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        <a href="tracking" <?=($area == "tracking" ? "class=active" : "")?>><span>Tracking</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="newsletter" <?=($area == "newsletter" ? "class=active" : "")?>><span>Newsletter</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseNine">
                                                Visitors<i class="icon-plus" style="float: right;"></i>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseNine" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        <a href="referrers" <?=($area == "referrers" ? "class=active" : "")?>><span>Referrers</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="os" <?=($area == "os" ? "class=active" : "")?>><span>Operating systems</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="browsers" <?=($area == "browsers" ? "class=active" : "")?>><span>Browsers</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
                                                Settings<i class="icon-plus" style="float: right;"></i>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTen" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        <a href="postcodes" <?=($area == "postcodes" ? "class=active" : "")?>><span>Postcodes</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="shipping" <?=($area == "shipping" ? "class=active" : "")?>><span>Shipping cost</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="flat" <?=($area == "flat" ? "class=active" : "")?>><span>Shipping Flat Rate</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="payment" <?=($area == "payment" ? "class=active" : "")?>><span>Payment</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="currency" <?=($area == "currency" ? "class=active" : "")?>><span>Currency</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="units" <?=($area == "units" ? "class=active" : "")?>><span>Weight Units</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="settings" <?=($area == "settings" ? "class=active" : "")?>><span>Settings</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="theme" <?=($area == "theme" ? "class=active" : "")?>><span>Theme settings</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="lang" <?=($area == "lang" ? "class=active" : "")?>><span>Language</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="tokens" <?=($area == "tokens" ? "class=active" : "")?>><span>API Tokens</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="export" <?=($area == "export" ? "class=active" : "")?>><span>Export</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseEleven">
                                                Customization<i class="icon-plus" style="float: right;"></i>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseEleven" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        <a href="registerimage" <?=($area == "registerimage" ? "class=active" : "")?>><span>Register Image</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="slider" <?=($area == "slider" ? "class=active" : "")?>><span>Slider</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="editor" <?=($area == "editor" ? "class=active" : "")?>><span>Theme editor</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="templates" <?=($area == "templates" ? "class=active" : "")?>><span>Templates</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="builder" <?=($area == "builder" ? "class=active" : "")?>><span>Page builder</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="menu" <?=($area == "menu" ? "class=active" : "")?>><span>Main menu</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="bottom" <?=($area == "bottom" ? "class=active" : "")?>><span>Footer menu</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="bottom" <?=($area == "bottom" ? "class=active" : "")?>><span>Extrafields</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                 <!--abhijeet code start-->
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTweleve">
                                                Calculator<i class="icon-plus" style="float: right;"></i>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTweleve" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        <a href="tmtcalculator" <?=($area == "tmt" ? "class=active" : "")?>><span>TMT Bars</span></a>
                                                    </td>
                                                </tr>

                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!--abhijeet code end-->
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwelve">
                                                Administration <i class="icon-plus" style="float: right;"></i>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwelve" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        <a href="support" <?=($area == "support" ? "class=active" : "")?>><span>Support</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="administrators" <?=($area == "administrators" ? "class=active" : "")?>><span>Administrators</span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="profile" <?=($area == "profile" ? "class=active" : "")?>><span>Profile</span></a>
                                                    </td>
                                                </tr>
                                                
                                            </table>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title" <?=($area == "logout" ? "class=active" : "")?>>
                                                <a href="logout"> <span>Logout</span></a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
			</aside>
		<div class="admin">
			<div class="content-warpper">
				<div class="content">
				<div class="clear"></div>