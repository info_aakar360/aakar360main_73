<?php
echo $data['header'];
if(isset($_GET['add'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Add new amminities</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="amminities" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
                <form action="" method="post" class="form-horizontal single"  enctype="multipart/form-data">
			'.csrf_field().'
			
				<div class="row">
					  <div class="form-group">
						<label class="control-label">Name</label>
						<input name="name" type="text"  class="form-control"/>
					  </div>
					  </div>
					  
					  <input name="add" type="submit" value="Add amminities" style="padding:3px 25px;" class="btn btn-primary" />
				
			</form>
			</div>
			</div>
			</div>
			</div>';
} elseif(isset($_GET['edit'])) {
    echo '<div class="row" style="margin-top: -13px;">
'.$data['notices'].'
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Edit amminities</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="amminities" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
                <form action="" method="post" class="form-horizontal single" enctype="multipart/form-data">
			'.csrf_field().'
			
				<div class="row">
					  <div class="form-group">
						<label class="control-label">Name</label>
						<input name="name" type="text"  value="'.$data['amminity']->name.'" class="form-control"/>
					  </div>
					  </div>
					  
					  
					  <input name="edit" type="submit" value="Edit amminities" style="padding:3px 25px;" class="btn btn-primary" />
				
			</form>
			</div>
			</div>
			</div>
			</div>';
} else {
?>
<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
                <h5>Amminities<a href="amminities?add" style="padding:3px 15px;" class="add">Add amminities</a></h5>
                <h5 class="card-title" style="color: #0d1baa;">Manage Your amminities</h5>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="datatable-editable">
                        <thead>
                        <tr class="bg-blue">
                            <th>Sr. No.</th>
                            <th>name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $sr = 1;
                        echo $data['notices'];
                        foreach ($data['amminities'] as $amminity){
                            echo'<tr>
            <td>'.$sr.'</td>
            <td>'.$amminity->name.'</td>
			<td>
                <a href="amminities?delete='.$amminity->id.'"><i class="icon-trash"></i></a>
				<a href="amminities?edit='.$amminity->id.'"><i class="icon-pencil"></i></a>
            </td>
          </tr>';
                            $sr++; }
                        } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php
echo $data['footer'];
?>
