<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="keywords" content="">
    <meta name="author" content="ThemeSelect">
<!--    <title>-->
<!--        --><?//=$title; ?>
<!--    </title>-->
    <link rel="apple-touch-icon" href="<?=$tp; ?>/admin/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/admin/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/vendors/data-tables/css/select.dataTables.min.css">

    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/custom/custom.css">
    <link href="<?=$tp; ?>/admin/css/select2.min.css" rel="stylesheet" />
    <!-- END: Custom CSS-->
</head>

<style type="text/css">

    .modal-window {
        position: fixed;
        background-color: rgba(200, 200, 200, 0.75);
        top: -70px;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 999;
        opacity: 0;
        pointer-events: none;
        -webkit-transition: all 0.3s;
        -moz-transition: all 0.3s;
        transition: all 0.3s;
    }

    .modal-window:target {
        opacity: 1;
        pointer-events: auto;
    }

    .modal-window>div {
        width: 800px;
        position: relative;
        margin: 10% auto;
        padding: 2rem;
        background: #fff;
        color: #444;
    }

    .modal-window header {
        font-weight: bold;
    }

    .modal-close {
        color: #aaa;
        line-height: 50px;
        font-size: 80%;
        position: absolute;
        right: 0;
        text-align: center;
        top: 0;
        width: 70px;
        text-decoration: none;
    }

    .modal-close:hover {
        color: #000;
    }

    .modal-window h1 {
        font-size: 150%;
        margin: 0 0 15px;
    }
</style>
<!-- END: Head-->
<?=$header;?>

<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
                <h2 class="card-title" style="font-size: 28px;">Seller Data</h2>

                    <div class="divider"></div>
                    <div class="row" style="position:relative;">
                        <div class="col s12 table-responsive"><!--1x 11661593 6YDgrn-->
                            <table id="datatable-editable" >
                                <thead>
                                <tr>
                                    <th>Sr. No.</th>
                                    <th>Seller Name</th>
                                    <th>Seller Email</th>
                                    <th>Active</th>
                                    <th>Institutional</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $sr = 1;
                                echo $notices;
                                foreach ($customers as $customer){
                                    $st = $customer->status;
                                    $ist = $customer->institutional_status;
                                    if($st==1){$status = 'Approve';} else{$status =  'Pending';}
                                    if($ist==1){$istatus = 'Active';} else{$istatus =  'Deactive';}
                                    echo'<tr>
            <td>'.$sr.'</td>
            <td>'.$customer->name.'</td>
            <td>'.$customer->email.'</td>
            <td>'.$status.'</td>
             <td>'.$istatus.'</td>
            <td>
                <a href="sellerview?id='.$customer->id.'"><i class="icon-eye"></i></a>
                <a href="seller?edit='.$customer->id.'"><i class="icon-pencil"></i></a>
                <a href="seller?delete='.$customer->id.'"><i class="icon-trash"></i></a>';
                                    if($st==1){
                                        echo '<a href="seller?reject='.$customer->id.'" class="btn btn-xs btn-danger">Reject</a>';
                                    }
                                    else{
                                        echo '<a href="seller?approve='.$customer->id.'" class="btn btn-xs btn-success">Approve</a>';
                                    }
                                    if($ist==1){
                                        echo '<a href="seller?inshide='.$customer->id.'" class="btn btn-xs btn-danger">Institutional Deactive</a>';
                                    }
                                    else{
                                        echo '<a href="seller?insshow='.$customer->id.'" class="btn btn-xs btn-success">Institutional Active</a>';
                                    }
                                    echo '</td>
          </tr>
          <!--div class="mini bloc">
			<h5>
				'.$customer->name.'
				<div class="tools">
					<a href="customers?delete='.$customer->id.'"><i class="icon-trash"></i></a>
				</div>
			</h5>
			<p>'.$customer->email.'</p>
		</div-->';
                                    $sr++; }?>
                                </tbody>
                            </table>
                        </div>
                    </div>

            </div>
        </div>
    </div>
    <?php echo $footer;?>
</div>


<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/admin/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.js"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/admin/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/admin/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/admin/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/admin/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/admin/js/select2.min.js"></script>
<script src="<?=$tp; ?>/admin/js/scripts/ui-alerts.js" type="text/javascript"></script>
<!-- <script src="<?php //$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script> -->
</body>

</html>
