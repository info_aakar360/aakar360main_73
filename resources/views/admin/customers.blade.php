<?php echo $header; ?>
    <div class="row" style="margin-top: 10px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 550px;">

    <h3 class="card-title" style="margin-bottom: 10px;">Manage Your Customers </h3>
    <div class="table-responsive">
        <table class="table table-striped table-bordered" id="datatable-editable">
            <thead>
            <tr class="bg-blue">
                <th>Sr. No.</th>
                <th>Customer Name</th>
                <th>Customer Email</th>
                <th>RFQ</th>
                <th>Show Price</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
<?php
    $sr = 1;
	echo $notices;
	foreach ($customers as $customer){
		echo'<tr>
            <td>'.$sr.'</td>
            <td>'.$customer->name.'</td>
            <td>'.$customer->email.'</td>';

		$ch = '';
		$chp = '';
		if($customer->rfr){
		    $ch = 'checked';
        }
        if($customer->show_price){
            $chp = 'checked';
        }
        echo '<td><label ><input type="checkbox" class="change-rfr" data-key="'.$customer->id.'" data-size="small" data-type="rfq" '.$ch.' style="opacity: 1; position: relative"> Enabled</label></td>
<td><label ><input type="checkbox" class="change-rfr" data-key="'.$customer->id.'" data-size="small" data-type="show_price" '.$chp.' style="opacity: 1; position: relative"> Enabled</label></td>
            <td>
                <a href="customers?delete='.$customer->id.'"><i class="icon-trash"></i></a>
            </td>
          </tr>
          <!--div class="mini bloc">
			<h5>
				'.$customer->name.'
				<div class="tools">
					<a href="customers?delete='.$customer->id.'"><i class="icon-trash"></i></a>
				</div>
			</h5>
			<p>'.$customer->email.'</p>
		</div-->';
	$sr++; }?>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
<?php
	echo $footer;

?>
<script>
    $(document).on('click', '.change-rfr', function(){
        var value = 0;
        if($(this).prop('checked')){
            value = 1;
        }
        var id = $(this).data('key');
        var type = $(this).data('type');
        $.ajax({
            url: 'change-rfr',
            type: 'post',
            data: 'id='+id+'&_token=<?=csrf_token(); ?>&rfr='+value+'&type='+type,
            success: function(data){

            },
            error: function(){

            }
        });
    });
</script>
