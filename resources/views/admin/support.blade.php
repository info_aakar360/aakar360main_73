<?php
echo $data['header'];
if(isset($_GET['reply']))
{
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 550px;">
    <form action="" method="post" class="form-horizontal single">
				'.csrf_field().'
				<h5><a href="support"><i class="icon-arrow-left"></i></a>Reply to : '.$data['ticket']->title.'</h5>
					<fieldset>
						  <div class="form-group">
							<label class="control-label">E-mail title</label>
							<input name="title" type="text" value="RE : '.$data['ticket']->title.'" class="form-control" required/>
						  </div>
						  <div class="form-group">
							<label class="control-label">E-mail reply</label>
							<textarea class="form-control" name="reply" id="reply" rows="10" cols="80" required></textarea>
						  </div>
						  <input name="send" type="submit" value="Send E-mail" class="btn btn-primary" />
					</fieldset>
				</form>
				</div>
				</div>
				</div>
				</div>';
} else {
    ?>

    <div class="row" style="margin-top: -13px;">
        <div class="col s12">
            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                <div class="card-content" style="min-height: 550px;">
                    <div class="head">
                        <h3>Support</h3>
                        <p>Manage customers tickets </p>
                    </div>
    <?php
    echo $data['notices'];
    foreach ($data['tickets'] as $ticket){
        echo'
			<div class="bloc">
				<h5>
					'.$ticket->title.'
					<div class="tools">
						<a href="support?reply='.$ticket->id.'"><i class="icon-action-undo "></i></a>
					</div>
				</h5>
				<p>'.nl2br(htmlspecialchars($ticket->message)).'</p>
				<b>'.$ticket->name.' - '.$ticket->email.'</b>
			</div>';
    }
}
echo $data['footer'];
?>