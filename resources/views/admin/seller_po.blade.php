<?php
echo $data['header'];
if (isset($_GET['view'])){
    echo $data['notices'];
    echo '<br/><div class="col-md-8">
    <div class="list">
        <div class="title">
            <a href="seller_po"><i class="icon-arrow-left"></i></a><i class="icon-user"></i>Customer details
        </div>
        <div class="item order"><h6><b>PO NO.</b> : #'.$data['order']->po_no.'</h6></div>
        <div class="item order"><h6><b>Order time</b> : '.$data['order']->created_date.'</h6></div>
        ';

    echo '</div>
    <div class="list">
        <div class="title">
            <i class="icon-basket"></i>Products
        </div>';
    $products_data = $data['order']->products;
    $products = json_decode($products_data, true);
    if(count($products)>0) {
        $ids = "";
        foreach ($products as $datas) {
            $ids = $ids . $datas['id'] . ",";
            $q[$datas['id']] = $datas['quantity'];
            $options_list[$datas['id']] = $datas['options'];
            $variants[$datas['id']] = $datas['variants'];
        }
        $ids = rtrim($ids, ',');
        $order_products = DB::select("SELECT * FROM products WHERE id IN (" . $ids . ")  ORDER BY id DESC ");
        foreach ($order_products as $product) {
            $options = json_decode($options_list[$product->id], true);
            $option_array = array();
            if ($options) {
                foreach ($options as $option) {
                    $option_array[] = '<i>' . $option['title'] . '</i> : ' . $option['value'];
                }
            }
            echo '<div class="item">
                    <h6>' . $product->title . ' x ' . $q[$product->id] . '
                    <b class="pull-right">
                        <strike>' . c($product->price) . '</strike> 
                        <span class="price">' . c($product->sale) . '</span> 
                    </b>
                    <br/>' . implode('<br/>', $option_array) . '
                    <br>' . $variants[$product->id] . '
                </div>';
        }
    }

    echo '</div>
</div>
<div class="col-md-4">
    
    <div class="list">
        <div class="title">
            <i class="icon-badge"></i>Order status
        </div>';
    $oid = $data['order']->id;
    $stas = DB::select("SELECT * FROM shipping_status WHERE po_id = '$oid'");
    echo '<div class="item order text-center"><br/><h6>';
    foreach($stas as $st) {
        echo 'Status -> '.$st->status.' || Updated Date -> '.$st->update_date.'<br>';
    }
    echo '</h6><br/></div>
        
    </div>
</div>';
} else { ?>


    <div class="row" style="margin-top: -13px;">
        <div class="col s12">
            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                <div class="card-content" style="min-height: 550px;">
                    <h5>Seller PO</h5>
                    <h5 class="card-title" style="color: #0d1baa;">Manage seller po</h5>

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="datatable-editable">
                            <thead>
                            <tr class="bg-blue">
                                <th>Sr. No.</th>
                                <th>Invoice No</th>
                                <th>PO No</th>
                                <th>Seller Name</th>
                                <th>Product Name</th>
                                <th>Status</th>
                                <th>PO Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $sr = 1;
                            echo $data['notices'];
                            foreach ($data['sellerpo'] as $spo){
                                echo'<tr>
                        <td>'.$sr.'</td>
                        <td>'.$spo->order_id.'</td>
                        <td>'.$spo->po_no.'</td>';
                                $u = $spo->customer_id;
                                $user = DB::table("customers")->where('id', '=', $u)->first();
                                if($user !== null) {
                                    echo '<td>' . $user->name . '</td>';
                                }else {
                                    echo '<td>NA</td>';
                                }

//                    echo '<td>';

                                $p = $spo->products;
                                $jp = json_decode($p);
                                echo '<td>';
                                foreach($jp as $key => $value) {
                                    $pid = $value->id;
                                    $pro = DB::table("products")->where('id', '=', $pid)->first();
                                    if($pro !== null) {
                                        echo '<p>' . $pro->title . '</p>';
                                    }else {
                                        echo '<p>NA</p>';
                                    }

                                }
                                echo '</td>';
                                $oid = $spo->id;
//                        $stas = DB::select("SELECT * FROM shipping_status WHERE po_id = '$oid' ORDER BY id DESC")[0];
                                $stas = DB::table("shipping_status")->where('po_id', '=', $oid)->orderBy('id','DESC')->first();

                                if($stas !== null) {
                                    echo '<td>' . $stas->status . '</td>';
                                }else {
                                    echo '<td>NA</td>';
                                }
                                echo '
                        <td>'.$spo->created_date.'</td>
                        <td><a href="seller_po?view='.$spo->id.'"><i class="icon-eye"></i></a>
                        </td></tr>';

                                $sr++;}
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php } ?>
<style>
    .button {
        background: gainsboro;
        padding: 5px 20px;
        cursor: pointer;
        border-radius: 30px;
    }
    .mini-button {
        cursor: pointer;
        border-radius: 30px;
        background: transparent;
        padding: 5px 0px;
        display: block;
    }
</style>

<?php
echo $data['footer'];
?>



