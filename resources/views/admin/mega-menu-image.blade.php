<?php
echo $data['header'];
if(isset($_GET['edit'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 550px;">
    <h5>Edit Mega menu image</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="mega-menu-image" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
                    <form action="" method="post" enctype="multipart/form-data" style="max-width: 100%;">
			'.csrf_field().'
			
				<div class="row">					
					 <div class="input-field col s12 m12">
						Section
						<input type="text" name="section" class="form-control" value="'.$data['menu']->section.'" required="required" />
					  </div></div><div class="row">';
    if (!empty($data['menu']->image)){

        echo '<p>Uploading new images will overwrtite current images .</p>';

        echo '<img class="col-md-2" src="'.url('/assets/megamenuimage/'.$data['menu']->image).'" required="required"/>';

        echo '<div class="clearfix"></div>';
    }
    echo  '<div class="input-field col s12 m6">
							Menu images
							<input type="file" class="form-control" name="image" accept="image/*"/>
						  </div>
						  </div>
					  
					  <div class="row" style="margin-top: 10px;">
					  <input name="update" type="submit" value="Edit image" style="padding:3px 25px;" class="btn btn-primary" />
					  </div>
				</div>
			</form>
			</div>
			</div>
			</div>
			</div>';
} else {
?>

<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
                <div class="head">
                    <h3>Home Page Designs</h3>
                    <p>Manage your mega menu image</p>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="datatable-editable">
                        <thead>
                        <tr class="bg-blue">
                            <th>Sr. No.</th>
                            <th>Menu Image</th>
                            <th>Section</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $sr = 1;
                        echo $data['notices'];
                        foreach ($data['menus'] as $design){
                            echo'<tr>
            <td>'.$sr.'</td>
            <td><img src="../assets/megamenuimage/'.$design->image.'" style="height: 100px; width: 100px;"></td>
            <td>'.$design->section.'</td>            
			<td><a href="mega-menu-image?edit='.$design->id.'"><i class="icon-pencil"></i></a>
            </td>
          </tr>';


                            echo'</div>
	</div>';
                            $sr++;}
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .button {
        background: gainsboro;
        padding: 5px 20px;
        cursor: pointer;
        border-radius: 30px;
    }
    .mini-button {
        cursor: pointer;
        border-radius: 30px;
        background: transparent;
        padding: 5px 0px;
        display: block;
    }
</style>
<script>
    $( document ).ready(function() {
        CKEDITOR.replace( 'editor' );
    });
</script>
<?php
echo $data['footer'];
?>



