<?php
echo $data['header'];
if(isset($_GET['add'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 550px;">
    <h5>Add new Link</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="link" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
                    <form action="" method="post" class="form-horizontal single">
			'.csrf_field().'
			
				<fieldset>
					  <div class="form-group">
						<label class="control-label">Link</label>
						<input name="link" type="text"  class="form-control" />
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Page</label>
						<select name="page" class="form-control" onclick="addit(this.value)">
						    <option value="">Please Select Page</option>
						    <option value="Single Product">Single Product</option>
						    <option value="Brand">Brand</option>
						    <option value="Products">Products</option>
						    <option value="Layout Plan Category">Layout Plan Category</option>
						    <option value="Layout Plan">Layout Plan</option>
						    <option value="Single Plan">Single Plan</option>
						    <option value="Photos Category">Photos Category</option>
						    <option value="Photos">Photos</option>
						    <option value="Photo Gallery">Photo Gallery</option>
						    <option value="Services Category">Services Category</option>
						    <option value="Sub Services">Sub Services</option>
                        </select>
					  </div>
					  <div class="form-group" id="catdata"></div>
					  <div class="form-group">
						<label class="control-label">Content</label>
						<input name="content" type="text"  class="form-control" />
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Image</label>
						<input name="image" type="file"  class="form-control" />
					  </div>
					  
					  <input name="add" type="submit" value="Submit" style="padding:3px 25px;" class="btn btn-primary" />
				</fieldset>
			</form>
			</div>
			</div>
			</div>
			</div>';
} elseif(isset($_GET['edit'])) {

    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 550px;">
    <h5>Edit link</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="link" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
                    <form action="" method="post" class="form-horizontal single">
			'.csrf_field().'
			
				<fieldset>
					  <div class="form-group">
						<label class="control-label">Link</label>
						<input name="link" type="text"  value="'.$data['link']->link.'" class="form-control" />
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Page</label>
						<select name="page" class="form-control">
						    <option value="">Please Select Page</option>';
    if($data['link']->page == 'Single Product') {
        echo '<option value="Single Product" selected>Single Product</option>
						        <option value="Brand">Brand</option>
						        <option value="Products">Products</option>
						        <option value="Layout Plan Category">Layout Plan Category</option>
                                <option value="Layout Plan">Layout Plan</option>
                                <option value="Single Plan">Single Plan</option>
                                <option value="Photos Category">Photos Category</option>
                                <option value="Photos">Photos</option>
                                <option value="Photo Gallery">Photo Gallery</option>
                                <option value="Services Category">Services Category</option>
                                <option value="Sub Services">Sub Services</option>';
    }
    if($data['link']->page == 'Brand') {
        echo '<option value="Single Product">Single Product</option>
                                <option value="Brand" selected>Brand</option>
                                <option value="Products">Products</option>
                                <option value="Layout Plan Category">Layout Plan Category</option>
                                <option value="Layout Plan">Layout Plan</option>
                                <option value="Single Plan">Single Plan</option>
                                <option value="Photos Category">Photos Category</option>
                                <option value="Photos">Photos</option>
                                <option value="Photo Gallery">Photo Gallery</option>
                                <option value="Services Category">Services Category</option>
                                <option value="Sub Services">Sub Services</option>';
    }
    if($data['link']->page == 'Products') {
        echo '<option value="Single Product" >Single Product</option>
                                <option value="Brand">Brand</option>
                                <option value="Products" selected>Products</option>
                                <option value="Layout Plan Category">Layout Plan Category</option>
                                <option value="Layout Plan">Layout Plan</option>
                                <option value="Single Plan">Single Plan</option>
                                <option value="Photos Category">Photos Category</option>
                                <option value="Photos">Photos</option>
                                <option value="Photo Gallery">Photo Gallery</option>
                                <option value="Services Category">Services Category</option>
                                <option value="Sub Services">Sub Services</option>';
    }
    if($data['link']->page == 'Layout Plan Category') {
        echo '<option value="Single Product" >Single Product</option>
                                <option value="Brand">Brand</option>
                                <option value="Products">Products</option>
                                <option value="Layout Plan Category" selected>Layout Plan Category</option>
                                <option value="Layout Plan">Layout Plan</option>
                                <option value="Single Plan">Single Plan</option>
                                <option value="Photos Category">Photos Category</option>
                                <option value="Photos">Photos</option>
                                <option value="Photo Gallery">Photo Gallery</option>
                                <option value="Services Category">Services Category</option>
                                <option value="Sub Services">Sub Services</option>';
    }
    if($data['link']->page == 'Layout Plan') {
        echo '<option value="Single Product" >Single Product</option>
                                <option value="Brand">Brand</option>
                                <option value="Products">Products</option>
                                <option value="Layout Plan Category">Layout Plan Category</option>
                                <option value="Layout Plan" selected>Layout Plan</option>
                                <option value="Single Plan">Single Plan</option>
                                <option value="Photos Category">Photos Category</option>
                                <option value="Photos">Photos</option>
                                <option value="Photo Gallery">Photo Gallery</option>
                                <option value="Services Category">Services Category</option>
                                <option value="Sub Services">Sub Services</option>';
    }
    if($data['link']->page == 'Single Plan') {
        echo '<option value="Single Product" >Single Product</option>
                                <option value="Brand">Brand</option>
                                <option value="Products">Products</option>
                                <option value="Layout Plan Category">Layout Plan Category</option>
                                <option value="Layout Plan">Layout Plan</option>
                                <option value="Single Plan" selected>Single Plan</option>
                                <option value="Photos Category">Photos Category</option>
                                <option value="Photos">Photos</option>
                                <option value="Photo Gallery">Photo Gallery</option>
                                <option value="Services Category">Services Category</option>
                                <option value="Sub Services">Sub Services</option>';
    }
    if($data['link']->page == 'Photos Category') {
        echo '<option value="Single Product" >Single Product</option>
                                <option value="Brand">Brand</option>
                                <option value="Products">Products</option>
                                <option value="Layout Plan Category">Layout Plan Category</option>
                                <option value="Layout Plan">Layout Plan</option>
                                <option value="Single Plan">Single Plan</option>
                                <option value="Photos Category" selected>Photos Category</option>
                                <option value="Photos">Photos</option>
                                <option value="Photo Gallery">Photo Gallery</option>
                                <option value="Services Category">Services Category</option>
                                <option value="Sub Services">Sub Services</option>';
    }
    if($data['link']->page == 'Photos') {
        echo '<option value="Single Product" >Single Product</option>
                                <option value="Brand">Brand</option>
                                <option value="Products">Products</option>
                                <option value="Layout Plan Category">Layout Plan Category</option>
                                <option value="Layout Plan">Layout Plan</option>
                                <option value="Single Plan">Single Plan</option>
                                <option value="Photos Category">Photos Category</option>
                                <option value="Photos" selected>Photos</option>
                                <option value="Photo Gallery">Photo Gallery</option>
                                <option value="Services Category">Services Category</option>
                                <option value="Sub Services">Sub Services</option>';
    }
    if($data['link']->page == 'Photo Gallery') {
        echo '<option value="Single Product" >Single Product</option>
                                <option value="Brand">Brand</option>
                                <option value="Products">Products</option>
                                <option value="Layout Plan Category">Layout Plan Category</option>
                                <option value="Layout Plan">Layout Plan</option>
                                <option value="Single Plan">Single Plan</option>
                                <option value="Photos Category">Photos Category</option>
                                <option value="Photos">Photos</option>
                                <option value="Photo Gallery" selected>Photo Gallery</option>
                                <option value="Services Category">Services Category</option>
                                <option value="Sub Services">Sub Services</option>';
    }
    if($data['link']->page == 'Services Category') {
        echo '<option value="Single Product" >Single Product</option>
                                <option value="Brand">Brand</option>
                                <option value="Products">Products</option>
                                <option value="Layout Plan Category">Layout Plan Category</option>
                                <option value="Layout Plan">Layout Plan</option>
                                <option value="Single Plan">Single Plan</option>
                                <option value="Photos Category">Photos Category</option>
                                <option value="Photos">Photos</option>
                                <option value="Photo Gallery">Photo Gallery</option>
                                <option value="Services Category" selected>Services Category</option>
                                <option value="Sub Services">Sub Services</option>';
    }
    if($data['link']->page == 'Sub Services') {
        echo '<option value="Single Product" >Single Product</option>
                                <option value="Brand">Brand</option>
                                <option value="Products">Products</option>
                                <option value="Layout Plan Category">Layout Plan Category</option>
                                <option value="Layout Plan">Layout Plan</option>
                                <option value="Single Plan">Single Plan</option>
                                <option value="Photos Category">Photos Category</option>
                                <option value="Photos">Photos</option>
                                <option value="Photo Gallery">Photo Gallery</option>
                                <option value="Services Category">Services Category</option>
                                <option value="Sub Services" selected>Sub Services</option>';
    }
    echo '</select>
					  </div>';
    if($data['link']->page == 'Products' || $data['link']->page == 'Single Product' || $data['link']->page == 'Brand') {
        echo '<div class="form-group">
					        <label class="control-label">Product category</label>
                            <select name="category" class="form-control">';
        foreach ($data['categories'] as $category) {
            $selected = '';
            if($data['link']->category == $category->id){
                $selected = 'selected';
            }
            echo '<option value="' . $category->id . '" '.$selected.'>' . $category->name . '</option>';
            $childs = DB::select("SELECT * FROM category WHERE parent = " . $category->id . " ORDER BY id DESC");
            foreach ($childs as $child) {
                $selected1 = '';
                if($link->category == $child->id){
                    $selected1 = 'selected';
                }
                echo  '<option value="' . $child->id . '" '.$selected1.'>- ' . $child->name . '</option>';
                $subchilds = DB::select("SELECT * FROM category WHERE parent = " . $child->id . " ORDER BY id DESC");
                foreach ($subchilds as $subchild) {
                    $selected2 = '';
                    if($link->category == $subchild->id){
                        $selected2 = 'selected';
                    }
                    echo '<option value="' . $subchild->id . '" '.$selected2.'><i style="padding-left: 10px;">--> ' . $subchild->name . '</i></option>';
                }
            }
        }
        echo '</select>
                        </div>';
    }
    echo '<div class="form-group">
						<label class="control-label">Content</label>
						<input name="content" type="text"  value="'.$data['link']->content.'" class="form-control" />
					  </div>';
    if (!empty($data['link']->image)){

        echo '<p>Uploading new images will overwrtite current images .</p>';

        echo '<img class="col-md-2" src="'.url('/assets/products/'.$data['link']->image).'" />';

        echo '<div class="clearfix"></div>';
    }
    echo '
					  
					  <div class="form-group">
						<label class="control-label">Image</label>
						<input name="image" type="file"  class="form-control" />
					  </div>
					  
					  <input name="edit" type="submit" value="Update" style="padding:3px 25px;" class="btn btn-primary" />
				</fieldset>
			</form>';
} else {
?>
<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
                <div class="head">
                    <h3>Link<a href="link?add" style="padding:3px 15px;" class="add">Add link</a></h3>
                    <p>Manage your link</p>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="datatable-editable">
                                <thead>
                                <tr class="bg-blue">
                                    <th>Sr. No.</th>
                                    <th>Link</th>
                                    <th>Page</th>
                                    <th>Content</th>
                                    <th>Image</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $sr = 1;
                                echo $data['notices'];
                                foreach ($data['links'] as $category){
                                    echo'<tr>
                        <td>'.$sr.'</td>
                        <td>'.$category->link.'</td>
                        <td>'.$category->page.'</td>
                        <td>'.$category->content.'</td>
                        <td><img src="../assets/products/'.image_order($category->image).'" style="height: auto; width: 100px;"></td>
                        
            
                        <td>
                            <a href="link?delete='.$category->id.'"><i class="icon-trash"></i></a>
                            <a href="link?edit='.$category->id.'"><i class="icon-pencil"></i></a>
                        </td>
                      </tr>
                      ';
                                    $sr++;
                                }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    }
    echo $data['footer'];
    ?>
    <script>
        function removeThis($id){
            event.preventDefault();
            $('[data='+$id+']').remove();
        }
        $('#add-option').on('click', function(){
            var id = $('#que-type > div').length;
            var html = '<div class="col-md-12 options" data="'+id+'"><label class="control-label col-md-12" data="'+id+'">Option '+(id+1)+'</label><input name="option[]" type="text" data="'+id+'" class="form-control col-md-11" style="width: 90%;margin-right: 5px;" /><a onClick="removeThis('+id+');" class="pul-right" style="line-height: 3;" data="'+id+'"><i class="icon-close" style="font-size: 20px;"></i></a></div>';
            $('#add-option').before(html);
        });
        function getData(data){
            if(data == 'text' || data == 'textarea'){
                $('.options').hide();
            }else{
                $('.options').show();
            }
        }


        function addit(val){
            var da = val;
            $.ajax({
                url: 'catData',
                type: 'post',
                data: 'page='+da+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $('#catdata').html(data);
                }
            });
        }

    </script>