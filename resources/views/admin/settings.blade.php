<?php 
	echo $header;
	echo $notices;
?>
    <div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 550px;">
    <form action="" method="post" style="max-width: 100%;">
	<?=csrf_field()?>
	<h5>Settings</h5>
	<div class="row">
		<!--SEO-->
        <div class="input-field col s12 m6">
			Site title
			<input name="name" type="text"  value="<?php echo $cfg->name;?>" class="form-control" required/>
		</div>
        <div class="input-field col s12 m6">
			Description
			<textarea name="desc" type="text" class="form-control" required><?=$cfg->desc?></textarea>
		</div>
    </div>
        <div class="row">
            <div class="input-field col s12 m6">
			Keywords
			<input name="key" type="text"  value="<?php echo $cfg->key;?>" class="form-control" required/>
		</div>
		<!--Site settings-->
            <div class="input-field col s12 m6">
			Logo
			<input name="logo" type="text"  value="<?php echo $cfg->logo;?>" class="form-control" required/>
		</div>
        </div>
        <div class="row">
            <div class="input-field col s12 m6">
			Registration
			<select name="registration" class="form-control" >
				<option <?php echo (1 == $cfg->registration)?'selected':'';?> value="1">Enabled</option>
				<option <?php echo (0 == $cfg->registration)?'selected':'';?> value="0">Disabled</option>
			</select>
		</div>
            <div class="input-field col s12 m6">
			Translation
			<select name="translations" class="form-control" >
				<option <?php echo (1 == $cfg->translations)?'selected':'';?> value="1">Enabled</option>
				<option <?php echo (0 == $cfg->translations)?'selected':'';?> value="0">Disabled</option>
			</select>
		</div>
        </div>
        <div class="row">
            <div class="input-field col s12 m6">
			Theme
			<select name="theme" class="form-control" >
				<?php $themes = array_filter(glob(base_path('themes').'/*'), 'is_dir');
					foreach($themes as $theme){
						echo '<option ';
						echo (basename($theme) == $cfg->theme)?'selected':'';
						echo ' value="'.basename($theme).'">'.basename($theme).'</option>';
					}
				?>
			</select>
		</div>
            <div class="input-field col s12 m6">
			Default language
			<select name="lang" class="form-control" >
				<?php
					foreach ($languages as $language){
						echo '<option ';
						echo ($language->code == $cfg->lang)?'selected':'';
						echo ' value="'.$language->code.'">'.$language->name.'</option>';
					}
				?>
			</select>
		</div>
        </div>
        <div class="row">
            <div class="input-field col s12 m6">
			Floating cart
			<select name="floating_cart" class="form-control" >
				<option <?php echo (1 == $cfg->floating_cart)?'selected':'';?> value="1">Enabled</option>
				<option <?php echo (0 == $cfg->floating_cart)?'selected':'';?> value="0">Disabled</option>
			</select>
		</div>
		<!--Social links-->
            <div class="input-field col s12 m6">
			Facebook
			<input name="facebook" type="text"  value="<?php echo $cfg->facebook;?>" class="form-control"/>
		</div>
        </div>
        <div class="row">
            <div class="input-field col s12 m6">
			Twitter
			<input name="twitter" type="text"  value="<?php echo $cfg->twitter;?>" class="form-control"/>
		</div>
            <div class="input-field col s12 m6">
			Tumblr
			<input name="tumblr" type="text"  value="<?php echo $cfg->tumblr;?>" class="form-control"/>
		</div>
        </div>
        <div class="row">
            <div class="input-field col s12 m6">
			Youtube
			<input name="youtube" type="text"  value="<?php echo $cfg->youtube;?>" class="form-control"/>
		</div>
            <div class="input-field col s12 m6">
			Instagram
			<input name="instagram" type="text"  value="<?php echo $cfg->instagram;?>" class="form-control"/>
		</div>
        </div>
		<!--Contact details-->
        <div class="row">
            <div class="input-field col s12 m6">
			Site Email
			<input name="email" type="text"  value="<?php echo $cfg->email;?>" class="form-control" required/>
		</div>
            <div class="input-field col s12 m6">
			Phone
			<input name="phone" type="text"  value="<?php echo $cfg->phone;?>" class="form-control" required/>
		</div>
        </div>
        <div class="row" style="margin-bottom: 10px;">
            <div class="input-field col s12 m6">
			Address
			<textarea name="address" type="text" class="form-control" required><?=$cfg->address?></textarea>
		</div>
        </div>
		<input name="save" type="submit" value="Update" style="padding:3px 25px;" class="btn btn-primary" />
    </div>
</form>
<?php echo $footer?>