<?php
echo $data['header'];
if(isset($_GET['add'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Add new category</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="advices_category" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post"  enctype="multipart/form-data" style="max-width: 100%;">
			'.csrf_field().'
			
			<div class="row">
				<div class="input-field col s12 m6">
					Name
					<input name="name" type="text"  class="form-control" required/>
				</div>
				
				<div class="input-field col s12 m6">
                    Parent category
                    <select name="parent" class="form-control">
                    <option value="0"></option>';
    foreach ($data['parents'] as $parent){
        echo '<option value="'.$parent->id.'">'.$parent->name.'</option>';
        $childs = DB::select("SELECT * FROM advices_category WHERE parent = ".$parent->id);
        foreach ($childs as $child){
            echo '<option value="'.$child->id.'">- '.$child->name.'</option>';
            $subchilds = DB::select("SELECT * FROM advices_category WHERE parent = ".$child->id);
            foreach ($subchilds as $subchild){
                echo '<option value="'.$subchild->id.'"><i style="padding-left: 10px;">--> '.$subchild->name.'</i></option>';
            }
        }
    }
    echo '</select>
              </div>
              </div>
              <div class="row">
              <div class="input-field col s12 m6">
                    banner image
                    <input type="file" class="form-control" name="banner_image" accept="image/*"/>
              </div>
              <div class="input-field col s12 m6">
                    Short Description
                    <input type="text" class="form-control" name="short_description"/>
              </div>
              </div>
				
				<input name="add" type="submit" value="Add category" class="btn btn-primary" />
			</div>
			</form>
			</div>
			</div>
			</div>
			</div>';
} elseif(isset($_GET['edit'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Edit category</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="advices_category" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post" style="max-width: 100%;" enctype="multipart/form-data">
			'.csrf_field().'
			
			<div class="row">
				<div class="input-field col s12 m6">
					name
					<input name="name" value="'.$data['category']->name.'" type="text"  class="form-control" required/>
				</div>
				
				<div class="input-field col s12 m6">
                    Parent category
                    <select name="parent" class="form-control">
                    <option value="0"></option>';
    foreach ($data['parents'] as $parent){
        echo '<option value="'.$parent->id.'" '.($parent->id == $data['category']->parent ? 'selected' : '').'>'.$parent->name.'</option>';
        $childs = DB::select("SELECT * FROM advices_category WHERE parent = ".$parent->id);
        foreach ($childs as $child){
            echo '<option value="'.$child->id.'" '.($child->id == $data['category']->parent ? 'selected' : '').'>- '.$child->name.'</option>';
            $subchilds = DB::select("SELECT * FROM advices_category WHERE parent = ".$child->id);
            foreach ($subchilds as $subchild){
                echo '<option value="'.$subchild->id.'" '.($subchild->id == $data['category']->parent ? 'selected' : '').'><i style="padding-left: 10px;">--> '.$subchild->name.'</i></option>';
            }
        }
    }
    echo '</select>
              </div>
              </div>
				<div class="row"><div class="input-field col s12 m6">';
    if (!empty($data['category']->banner_image)){
        echo '<p>Uploading new images will overwrtite current images .</p>
					<img class="col-md-4" src="'.url('/assets/blog/'.$data['category']->banner_image).'" />
					<div class="clearfix"></div><br/>';
    }
    echo '
						banner image
						<input type="file" class="form-control" name="banner_image" accept="image/*"/>
				  </div>
				  
				  <div class="input-field col s12 m6">
                    Short Description
                    <input type="text" class="form-control" name="short_description" value="'.$data['category']->short_description.'"/>
                    </div>
                    </div>
                    <center><input name="edit" type="submit" value="Edit category" style="padding:3px 25px;" class="btn btn-primary" />
                    </center>
				
			</div>
		</form>
		</div>
		</div>
		</div>
		</div>';
} else {
    ?>
    <div class="row" style="margin-top: 10px;">
        <div class="col s12">
            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                <div class="card-content" style="min-height: 550px;">
                    <h5>Advice category<a href="advices_category?add" style="padding: 3px 15px; font-size: 15px;" class="add">Add Categories</a></h5>
                    <h5 class="card-title" style="color: #0d1baa;">Manage Your Advice category</h5>
                    <div class="table-responsive">
                        <table id="dataTableExample1" class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr class="bg-blue">
                                <th>Sr. No.</th>
                                <th>Category Name</th>
                                <th>Parent</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $sr = 1;
                            echo $data['notices'];
                            foreach ($data['categories'] as $page){
                                echo'<tr>
            <td>'.$sr.'</td>
            <td>'.$page->name.'</td>';
                                if($page->parent != 0){
                                    $parent = DB::select("SELECT * FROM advices_category WHERE id = ".$page->parent)[0];
                                    echo'<td>'.$parent->name.'</td>';
                                } else {
                                    echo'<td>No Parent</td>';
                                }
                                echo '<td>
                <a href="advices_category?delete='.$page->id.'"><i class="icon-trash"></i></a>
						<a href="advices_category?edit='.$page->id.'"><i class="icon-pencil"></i></a>
            </td>
          </tr>';
                                $sr++; }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="<?=$data['tp']?>/admin/summernote/summernote.js"></script>
    <link href="<?=$data['tp']?>/admin/summernote/summernote.css" rel="stylesheet" />
    <script type="text/javascript" src="<?=$data['tp']?>/admin/summernote/custom.js"></script>
<?=$data['footer']?>