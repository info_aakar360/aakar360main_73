<?php
echo $header;
    echo '<form action="update-questions?edit='.$_GET['edit'].'" method="post" class="form-horizontal" enctype="multipart/form-data" style="max-width: 100%;">
			'.csrf_field().'
			<h5><a href="services"><i class="icon-arrow-left"></i></a>Update Questions for <b>'.getServiceName($_GET['edit']).'</b></h5>
				<fieldset>
					  <div class="form-group col-md-8" style="padding-left: 30px;padding-right: 30px;">
							<label class="control-label">Select Question</label>
							<select name="question" class="form-control select2" required="required">
							<option></option>';
                            foreach ($questions as $question){
                                echo '<option value="'.$question->id.'">'.$question->question.'</option>';
                            }
                            echo '</select>
					  </div>
					  <div class="form-group col-md-2" style="padding-left: 30px;padding-right: 30px;">
						<label class="control-label">Priority</label>
						<input name="priority" type="text" class="form-control" required="required"/>
					  </div>
					  
					  <input name="update" type="submit" value="Add Question" class="btn btn-primary col-md-2" style="padding-left: 30px;padding-right: 30px;"/>
				</fieldset>
			</form>';

?>
        <div class="table-responsive">
			<table class="table table-striped table-bordered" id="datatable-editable">
				<thead>
				<tr class="bg-blue">
					<th>Sr. No.</th>
					<th>Question</th>
					<th>Priority</th>
					<th>Action</th>
				</tr>
				</thead>
				<tbody>
					<?php
					$sr = 1;
					echo $notices;
					foreach ($service_questions as $que){
						echo'<tr>
								<td>'.$sr.'</td>
								<td>'.getQuestion($que->question_id).'</td>
								<td>'.$que->priority.'</td>
								<td>
									<a href="update-questions?edit='.$_GET['edit'].'&delete='.$que->id.'"><i class="icon-trash"></i></a>
								</td>
							  </tr>
							 ';
					$sr++;}

							?>
				</tbody>
			</table>
		</div>
<style>
    .button {
        background: gainsboro;
        padding: 5px 20px;
        cursor: pointer;
        border-radius: 30px;
    }
    .mini-button {
        cursor: pointer;
        border-radius: 30px;
        background: transparent;
        padding: 5px 0px;
        display: block;
    }
</style>

<?php
echo $footer;
?>
<script>
    $( document ).ready(function() {
		$('.select2').select2({
			placeholder: "Please Select Question...",
			allowClear: true
		});
	});
</script>