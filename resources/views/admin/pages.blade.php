<?php
echo $data['header'];
if(isset($_GET['add'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 550px;">
    <h5>Add new page</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="pages" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
                    <form action="" method="post" class="form-horizontal single">
			'.csrf_field().'
			
			<fieldset>
				<div class="form-group">
					<label class="control-label">Page title</label>
					<input name="title" type="text"  class="form-control" required/>
				</div>
				<div class="form-group">
					<label class="control-label">Page path</label>
					<input name="path" type="text"  class="form-control" required/>
				</div>
				<div class="form-group">
					<label class="control-label">Page content</label>
					<textarea name="content" class="summernote" rows="10" cols="80" required></textarea>
				</div>
				<input name="add" type="submit" value="Add page" style="padding:3px 25px;" class="btn btn-primary" />
			</fieldset>
			</form>
			</div>
			</div>
			</div>
			</div>';
} elseif(isset($_GET['edit'])) {
    echo $data['notices'].'
<div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 550px;">
    <h5>Edit page</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="pages" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
                    <form action="" method="post" class="form-horizontal single">
			'.csrf_field().'
			
			<fieldset>
				<div class="form-group">
					<label class="control-label">Page title</label>
					<input name="title" value="'.$data['page']->title.'" type="text"  class="form-control" required/>
				</div>
				<div class="form-group">
					<label class="control-label">Page path</label>
					<input name="path" value="'.$data['page']->path.'" type="text"  class="form-control" required/>
				</div>
				<div class="form-group">
					<label class="control-label">Page content</label>
					<textarea name="content" class="summernote" rows="10" cols="80" required>'.$data['page']->content.'</textarea>
				</div>
				<input name="edit" type="submit" value="Edit page" style="padding:3px 25px;" class="btn btn-primary" />
			</fieldset>
		</form>
		</div>
		</div>
		</div>
		</div>';
} else {
    ?>
    <div class="row" style="margin-top: -13px;">
        <div class="col s12">
            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                <div class="card-content" style="min-height: 550px;">
                    <div class="head">
                        <h3>Pages<a href="pages?add" class="add">Add page</a></h3>
                        <p>Manage your website pages</p>
                    </div>
                    <div class="table-responsive">
                        <table id="dataTableExample1" class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr class="bg-blue">
                                <th>Sr. No.</th>
                                <th>Page Name</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $sr = 1;
                            echo $data['notices'];
                            foreach ($data['pages'] as $page){
                                echo'<tr>
            <td>'.$sr.'</td>
            <td><a href="../page/'.$page->path.'">'.$page->title.'</a></td>
            <td>
                <a href="pages?delete='.$page->id.'"><i class="icon-trash"></i></a>
						<a href="pages?edit='.$page->id.'"><i class="icon-pencil"></i></a>
            </td>
          </tr>
          <!--div class="mini bloc">
				<h5>
					<a href="../page/'.$page->path.'">'.$page->title.'</a>
					<div class="tools">
						<a href="pages?delete='.$page->id.'"><i class="icon-trash"></i></a>
						<a href="pages?edit='.$page->id.'"><i class="icon-pencil"></i></a>
					</div>
				</h5>
				<p>'.mb_substr(strip_tags($page->content),0,70).'...</p>
				</div-->';
                                $sr++; }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="<?=$data['tp']?>/admin/summernote/summernote.js"></script>
    <link href="<?=$data['tp']?>/admin/summernote/summernote.css" rel="stylesheet" />
    <script type="text/javascript" src="<?=$data['tp']?>/admin/summernote/custom.js"></script>
<?=$data['footer']?>