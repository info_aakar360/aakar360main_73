<?php 
	echo $header;
	echo $notices;
?>
    <div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 550px;">
        <form action="" method="post" style="max-width: 100%;">
	<?=csrf_field()?>
	<h5>Newsletter</h5>
	<div class="row">
        <div class="input-field col s12 m6">
			Users group
			<select name="group" class="form-control">
				<option value="all" selected>All E-mails</option>
				<option value="orders">Order E-mails</option>
				<option value="newsletter">Subscribers E-mails</option>
				<option value="support">Support E-mails</option>
			</select>
		</div>
        <div class="input-field col s12 m6">
			E-mail title
			<input name="title" type="text" value="<?=(isset($_GET['title'])) ? $_GET['title'] : '';?>" class="form-control" required/>
		</div>
    </div>
            <div class="row" style="margin-bottom: 10px;">
                <div class="input-field col s12 m12">
			E-mail content
			<textarea class="form-control" name="content" rows="10" cols="80" required><?=(isset($_GET['content'])) ? $_GET['content'] : '';?></textarea>
		</div>
            </div>
		<input name="send" type="submit" value="Send E-mail" style="padding:3px 25px;" class="btn btn-primary" />
    </div>
</form>
</div>
    </div>
    </div>
    </div>
<?php echo $footer;?>