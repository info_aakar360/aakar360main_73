<?php echo $header; ?>
    <div class="row" style="margin-top: -13px;">
        <div class="col s12">
            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                <div class="card-content" style="min-height: 550px;">
		<h3>Tokens<a href="tokens?add" style="padding:3px 15px;" class="add">Generate a token</a></h3>
		<p>Access advanced API functions that requires an API token</p>

<?php
	echo $notices;
	foreach ($tokens as $token){
		echo'<div class="mini bloc">
			<h5>
				'.$token->token.'
				<div class="tools">
					<a href="tokens?delete='.$token->token.'"><i class="icon-trash"></i></a>
				</div>
			</h5>
			<p>Total requests : '.$token->requests.'</p>
		</div>';
	}
	echo $footer;
?>