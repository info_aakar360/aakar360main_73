<?php
echo $data['header'];
if(isset($_GET['add'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 650px;">
            <h5>Add Design category</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="design_category" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post" enctype="multipart/form-data" style="max-width: 100%;">
			'.csrf_field().'
			
				<div class="row">
					  <div class="input-field col s12 m6">
					    Category name
						<input name="name" type="text"  class="form-control" />
					  </div>
					  <div class="input-field col s12 m6">
						Short Description
						<textarea name="description" class="form-control" placeholder="Enter description..."></textarea>
					  </div>
					  </div>
					  <div class="row">
					  <div class="input-field col s12 m6">
						Category path
						<input name="path" type="text" class="form-control"  />
					  </div>
					  <div class="input-field col s12 m6">
						Category image
						<input name="image" type="file" class="form-control"  />
					  </div>
					  </div>
					  <div class="row">
						<div class="input-field col s12 m6">
							Layout
							<select name="layout" class="form-control">
								<option value="layout_plans">Layout Plan</option>
								<option value="photos">Photos</option>
							</select>
						</div>
					  <div class="input-field col s12 m6">
							Parent category
							<select name="parent" class="form-control">
							<option value="0"></option>';
    foreach ($data['parents'] as $parent){
        echo '<option value="'.$parent->id.'">'.$parent->name.'</option>';
        $childs = DB::select("SELECT * FROM design_category WHERE parent = ".$parent->id);
        foreach ($childs as $child){
            echo '<option value="'.$child->id.'">- '.$child->name.'</option>';
            $subchilds = DB::select("SELECT * FROM design_category WHERE parent = ".$child->id);
            foreach ($subchilds as $subchild){
                echo '<option value="'.$subchild->id.'"><i style="padding-left: 10px;">--> '.$subchild->name.'</i></option>';
            }
        }
    }
    echo '</select>
												  </div>
												  </div>
												  <input name="add" type="submit" value="Add category" class="btn btn-primary" />
											</div>
										</form>
										</div>
										</div>
										</div>
										</div>';
} elseif(isset($_GET['edit'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Edit Design category</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="design_category" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post" enctype="multipart/form-data" style="max-width: 100%;">
			'.csrf_field().'
			
				<div class="row">
					  <div class="input-field col s12 m6">
						Category name
						<input name="name" type="text"  value="'.$data['design_category']->name.'" class="form-control" />
					  </div>
					  <div class="input-field col s12 m6">
						Short Description
						<textarea name="description" class="form-control" placeholder="Enter description...">'.$data['design_category']->description.'</textarea>
					  </div>
					  </div>
					  <div class="row">
					  <div class="input-field col s12 m6">
						Category path
						<input name="path" type="text" value="'.$data['design_category']->path.'" class="form-control"  />
					  </div>';
    if (!empty($data['design_category']->image)){

        echo '<p>Uploading new images will overwrtite current images .</p>';

        echo '<img class="col-md-2" src="'.url('/assets/layout_plans/'.$data['design_category']->image).'" />';

        echo '<div class="clearfix"></div>';
    }

    if (!empty($data['design']->download)){
        echo '<p>Uploading new file will overwrite current file .</p>';
    }
    echo '
					  <div class="input-field col s12 m6">
						Image
						<input type="file" class="form-control" name="image"/>
					  </div>
					  </div>
					  <div class="row">
						<div class="input-field col s12 m6">
							Layout
							<select name="layout" class="form-control" placeholder="Select Layout for this category">
								<option></option>
								<option value="layout_plans" '.($data['design_category']->layout == 'layout_plans' ? 'selected' : '').'>Layout Plan</option>
								<option value="photos" '.($data['design_category']->layout == 'photos' ? 'selected' : '').'>Photos</option>
							</select>
						</div>
					  <div class="input-field col s12 m6">
							Parent category
							<select name="parent" class="form-control">
							<option value="0"></option>';
    foreach ($data['parents'] as $parent){
        echo '<option value="'.$parent->id.'" '.($parent->id == $data['design_category']->parent ? 'selected' : '').'>'.$parent->name.'</option>';
        $childs = DB::select("SELECT * FROM design_category WHERE parent = ".$parent->id);
        foreach ($childs as $child){
            echo '<option value="'.$child->id.'" '.($child->id == $data['design_category']->parent ? 'selected' : '').'>- '.$child->name.'</option>';
            $subchilds = DB::select("SELECT * FROM design_category WHERE parent = ".$child->id);
            foreach ($subchilds as $subchild){
                echo '<option value="'.$subchild->id.'" '.($subchild->id == $data['design_category']->parent ? 'selected' : '').'><i style="padding-left: 10px;">--> '.$subchild->name.'</i></option>';
            }
        }
    }
    echo '</select>
					  </div>
					  </div>
					  <input name="edit" type="submit" value="Edit category" style="padding:3px 25px;" class="btn btn-primary" />
				</div>
			</form>
			</div>
			</div>
			</div>
			</div>';
} else {
    ?>
    <div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 50px;">
    <h5>categories <a href="design_category?add" style="padding:3px 15px;" class="add">Add categories</a></h5>
    <h5 class="card-title" style="color: #0d1baa;">Manage Your Design categories </h5>
    <div class="table-responsive">
    <table class="table table-striped table-bordered" id="datatable-editable">
    <thead>
    <tr class="bg-blue">
        <th>Sr. No.</th>
        <th>Design Image</th>
        <th>Design Name</th>
        <th>Parent Category Name</th>
        <th>Layout</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $sr = 1;
    echo $data['notices'];
    foreach ($data['design_category'] as $category){
        echo'<tr>
            <td>'.$sr.'</td>
            <td><img src="../assets/design/'.image_order($category->image).'" style="height: 100px; width: 100px;"></td>
            <td><a href="../'.$category->path.'">'.$category->name.'</a></td>';
        if($category->parent != 0){
            $parent = DB::select("SELECT * FROM design_category WHERE id = ".$category->parent)[0];
            echo'<td>'.$parent->name.'</td>';
        } else {
            echo'<td>No Parent</td>';
        }
        echo'
			<td>
				'.$category->layout.'
			</td>
            <td><a href="design_category?delete='.$category->id.'"><i class="icon-trash"></i></a>
				<a href="design_category?edit='.$category->id.'"><i class="icon-pencil"></i></a>
            </td>
          </tr>
          <!--div class=" col-md-3">
		<div class="product">
			<div class="pi">
				<img src="../assets/design/'.image_order($category->image).'">
			</div>
			<h5><a href="../'.$category->path.'">'.$category->name.'</a></h5>
			
			<div class="tools">
				<a href="design_category?delete='.$category->id.'"><i class="icon-trash"></i></a>
				<a href="design_category?edit='.$category->id.'"><i class="icon-pencil"></i></a>
			</div>
		</div>
	</div-->';
        $sr++; }
}?>
    </tbody>
    </table>
    </div>
    </div>
    </div>
    </div>
    </div>

<?php
echo $data['footer'];
?>