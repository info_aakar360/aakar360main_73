<?php
echo $data['header'];
if(isset($_GET['add']))
{
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Add new currency</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="currency" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post" style="max-width: 100%;">
				'.csrf_field().'
				
					<div class="row">
						  <div class="input-field col s12 m6">
							Name
							<input name="name" type="text" class="form-control" required />
						  </div>
						  <div class="input-field col s12 m6">
							Code
							<input name="code" type="text" class="form-control" required />
						  </div>
						  </div>
						  <div class="row"style="margin-bottom: 20px;">
						  <div class="input-field col s12 m6">
							Exchange rate
							<input name="rate" type="text" class="form-control" required />
						  </div>
						  </div>
						  <input name="add" type="submit" value="Add currency" style="padding:3px 25px;" class="btn btn-primary" />
					</div>
				</form>
				</div>
				</div>
				</div>
				</div>';
}
elseif(isset($_GET['edit']))
{
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
             <h5>Edit currency</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="currency" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post" style="max-width: 100%;">
				'.csrf_field().'
				
					<div class="row">
						  <div class="input-field col s12 m6">
							Name
							<input name="name" value="'.$data['currency']->name.'" type="text" class="form-control" required />
						  </div>
						  <div class="input-field col s12 m6">
							Code
							<input name="code" value="'.$data['currency']->code.'" type="text" class="form-control" required />
						  </div>
						  </div>
						  <div class="row" style="margin-bottom: 20px;">
						  <div class="input-field col s12 m6">
							Exchange rate
							<input name="rate" value="'.$data['currency']->rate.'" type="text" class="form-control" required />
						  </div>
						  </div>
						  <input name="edit" type="submit" value="Edit currency" style="padding:3px 25px;" class="btn btn-primary" />
					</div>
				</form>
				</div>
				</div>
				</div>
				</div>';
} else {
    ?>
    <div class="row" style="margin-top: -13px;">
        <div class="col s12">
            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                <div class="card-content" style="min-height: 550px;">
                    <h3>Currency<a href="currency?add" style="padding:3px 15px;" class="add">Add currency</a></h3>
                    <p>Manage the currencies you accept in your website</p>

    <?php
    echo $data['notices'];
    foreach ($data['currencies'] as $currency){
        echo '<div class="mini bloc">
			<h5>'.$currency->name.' ('.$currency->code.')
				<div class="tools">';
        echo (count($data['currencies']) != 1) ? '<a href="currency?delete='.$currency->id.'"><i class="icon-trash"></i></a>' : '';
        echo ' <a href="currency?edit='.$currency->id.'"><i class="icon-pencil"></i></a>';
        echo ($currency->default == 0) ? ' <a href="currency?default='.$currency->id.'" title="Set as default"><i class="icon-pin "></i></a>' : ' <i class="icon-check" title="Default currency"></i>';
        echo '</div>
			</h5>
			</div>';
    }
}
echo $data['footer'];
?>