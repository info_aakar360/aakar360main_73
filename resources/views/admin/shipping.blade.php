<?php
echo $data['header'];
if(isset($_GET['add']))
{
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Add New Shipping</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="shipping" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post" style="max-width: 100%">
				'.csrf_field().'
				
					<div class="row">
						<div class="input-field col s12 m6">
							Shipping Name
							<input name="name" type="text" class="form-control" required />
						</div>
						<div class="input-field col s12 m6">
							Category
							<select id="country" name="category[]" class="form-control select2" multiple>';
    foreach ($data['category'] as $cat){
        echo '<option value="'.$cat->id.'">'.$cat->name.'</option>';
    }
    echo '</select>
						  </div>
						  </div>
						  <div class="row">
						<div class="input-field col s12 m6">
							Postcodes (Comma separated values for eg. 110001,110002,110003)
							<textarea name="postcodes" rows="5"></textarea>
						</div>
						  <div class="input-field col s12 m6">
							Shipping cost
							<input name="cost" type="text" class="form-control" required />
						  </div>
						  </div>
						  <div class="row">
						  <div class="input-field col s12 m6">
							Country
							<select id="country" name="country" class="form-control">';
    foreach ($data['countries'] as $country){
        echo '<option value="'.$country->iso.'" data-phone="'.$country->phonecode.'">'.$country->nicename.'</option>';
    }
    echo '</select>
						  </div>
						  </div>
						  <input name="add" type="submit" value="Add cost" style="padding:3px 25px;" class="btn myblue waves-light" />
					</div>
				</form>
				</div>
				</div>
				</div>
				</div>';
}
elseif(isset($_GET['edit']))
{
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Edit Shipping</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="shipping" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post" style="max-width: 100%">
				'.csrf_field().'
				
					<div class="row">
						<div class="input-field col s12 m6">
							Shipping Name
							<input name="name" type="text" class="form-control" required value="'.$data['cost']->name.'"/>
						</div>
						<div class="input-field col s12 m6">
							Category
							<select id="country" name="category[]" class="form-control select2" multiple>';
    $catx = explode(',', $data['cost']->category);
    foreach ($data['category'] as $cat){
        $selected = in_array($cat->id, $catx) ? 'selected' : '';
        echo '<option value="'.$cat->id.'" '.$selected.'>'.$cat->name.'</option>';
    }
    echo '</select>
						  </div>
						  </div>
						  <div class="row">
						<div class="input-field col s12 m6">
							Postcodes (Comma separated values for eg. 110001,110002,110003)
							<textarea name="postcodes" rows="5">'.$data['cost']->postcodes.'</textarea>
						</div>
						  <div class="input-field col s12 m6">
							Shipping cost
							<input name="cost" value="'.$data['cost']->cost.'" type="text" class="form-control" required />
						  </div>
						  </div>
						  <div class="row">
						  <div class="input-field col s12 m6">
							Country
							<select id="country" name="country" class="form-control">';
    foreach ($data['countries'] as $country){
        echo '<option value="'.$country->iso.'" '.($country->iso == $cost->country ? 'selected' : '').'>'.$country->nicename.'</option>';
    }
    echo '</select>
						  </div>
						  </div>
						  <input name="edit" type="submit" value="Edit cost" style="padding:3px 25px;" class="btn btn-primary" />
					</div>
				</form>
				</div>
				</div>
				</div>
				</div>';
} else {
?>
<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
                <h3>Shipping cost<a href="shipping?add" style="padding:3px 15px;" class="add">Add cost</a></h3>
                <p>Set your shipping costs for specific countries</p>

                <?php
                echo $data['notices'];
                foreach ($data['costs'] as $cost){
                    echo '<div class="mini bloc">
			<h5>'.$cost->name.' ('.c($cost->cost).')
				<div class="tools">
					<a href="shipping?delete='.$cost->id.'"><i class="icon-trash"></i></a>
					<a href="shipping?edit='.$cost->id.'"><i class="icon-pencil"></i></a>
				</div>
			</h5>
			</div>';
                }
                }
                echo $data['footer'];
                ?>
                <script>
                    $(document).ready(function(){
                        $('.select2').select2();
                    });
                </script>