<?php
echo $header;
    echo '<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <form action="category-update-questions?edit='.$_GET['edit'].'" method="post"  enctype="multipart/form-data" style="max-width: 100%;">
			'.csrf_field().'
			<h5><a href="services_categories"><i class="icon-arrow-left"></i></a>Update Questions for <b>'.getServiceCategoryName($_GET['edit']).'</b></h5>
				<div class="row">
					  <div class="input-field col s12 m6">
							<label class="control-label">Select Question</label>
							<select name="question" class="form-control select2" required="required">
							<option></option>';
                            foreach ($questions as $question){
                                echo '<option value="'.$question->id.'">'.$question->question.'</option>';
                            }
                            echo '</select>
					  </div>
					  <div class="input-field col s12 m6">
						<label class="control-label">Priority</label>
						<input name="priority" type="text" class="form-control" required="required"/>
					  </div>
					 </div> 
					  <input name="update" type="submit" value="Add Question" class="btn btn-primary col-md-2" style="padding:3px 25px;"/>
				
			</form>
			';

?>
        <div class="table-responsive">
			<table class="table table-striped table-bordered" id="datatable-editable">
				<thead>
				<tr class="bg-blue">
					<th>Sr. No.</th>
					<th>Question</th>
					<th>Priority</th>
					<th>Action</th>
				</tr>
				</thead>
				<tbody>
					<?php
					$sr = 1;
					echo $notices;
					foreach ($service_questions as $que){
						echo'<tr>
								<td>'.$sr.'</td>
								<td>'.getQuestion($que->question_id).'</td>
								<td>'.$que->priority.'</td>
								<td>
									<a href="update-questions?edit='.$_GET['edit'].'&delete='.$que->id.'"><i class="icon-trash"></i></a>
								</td>
							  </tr>
							 ';
					$sr++;}

							?>
				</tbody>
			</table>
		</div>
</div>
</div>
</div>
</div>
<style>
    .button {
        background: gainsboro;
        padding: 5px 20px;
        cursor: pointer;
        border-radius: 30px;
    }
    .mini-button {
        cursor: pointer;
        border-radius: 30px;
        background: transparent;
        padding: 5px 0px;
        display: block;
    }
</style>

<?php
echo $footer;
?>
<script>
    $( document ).ready(function() {
		$('.select2').select2({
			placeholder: "Please Select Question...",
			allowClear: true
		});
	});
</script>