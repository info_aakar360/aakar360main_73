<?php
echo $data['header'];
if(isset($_GET['add'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Add Brands</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="brands" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post"  enctype="multipart/form-data" style="max-width: 100%">
			'.csrf_field().'
			
				<div class="row">
					  <div class="input-field col s12 m6">
						Brand name
						<input name="name" type="text"  class="form-control" />
					  </div>
					  <div class="input-field col s12 m6">
						Brand path
						<input name="path" type="text" class="form-control"  />
					  </div>
					  </div>
					  <div class="row">
					  <div class="input-field col s12 m6">
						Brand logo
						<input name="image" type="file" class="form-control"  />
					  </div>
					  
					  <div class="input-field col s12 m6">
						Brand Banner
						<input name="banner_image" type="file" class="form-control"  />
					  </div>
					  </div>
					  <input name="add" type="submit" value="Add brand" class="btn btn-primary" />
				</div>
			</form>
			</div>
			</div>
			</div>
			</div>';
} elseif(isset($_GET['edit'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Edit Brands</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="brands" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post"  enctype="multipart/form-data" style="max-width: 100%">
			'.csrf_field().'
			
				<div class="row">
					  <div class="input-field col s12 m6">
						Brand name
						<input name="name" type="text"  value="'.$data['brand']->name.'" class="form-control" />
					  </div>
					  <div class="input-field col s12 m6">
						Brand path
						<input name="path" type="text" value="'.$data['brand']->path.'" class="form-control"  />
					  </div>
					  </div>
					  <div class="row"><div class="input-field col s12 m6">';
    if (!empty($data['brand']->image)){

        echo '<p>Uploading new images will overwrtite current images .</p>';

        echo '<img class="col-md-2" src="'.url('/assets/brand/'.$data['brand']->image).'" />';

        echo '<div class="clearfix"></div>';
    }

    echo '
                    
					  <div class="input-field col s12 m6">
						Image
						<input type="file" class="form-control" name="image"/>
					  </div></div><div class="row"><div class="input-field col s12 m6">';
    if (!empty($data['brand']->banner_image)){

        echo '<p>Uploading new images will overwrtite current images .</p>';

        echo '<img class="col-md-2" src="'.url('/assets/brand/'.$data['brand']->banner_image).'" />';

        echo '<div class="clearfix"></div>';
    }

    echo '
					  <div class="input-field col s12 m6">
						Banner Image
						<input type="file" class="form-control" name="banner_image"/>
					  </div>
					   </div>
					   <div class="row">
					  <input name="edit" type="submit" value="Edit brand" style="padding:3px 25px;" class="btn btn-primary" />
					  </div>
				</div>
			</form>
			</div>
			</div>
			</div>
			</div>';
} else {
    ?>
    <div class="row" style="margin-top: 10px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 550px;">
    <h5>Brands<a href="brands?add" style="padding: 3px 15px;" class="add">Add Brands</a></h5>
    <h5 class="card-title" style="color: #0d1baa;">Manage Your Brands </h5>
    <div class="table-responsive">
    <table class="table table-striped table-bordered" id="datatable-editable">
    <thead>
    <tr class="bg-blue">
        <th>Sr. No.</th>
        <th>Brand Logo</th>
        <th>Brand Name</th>
        <th>Banner Image</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $sr = 1;
    echo $data['notices'];
    foreach ($data['brands'] as $brand){
        echo'<tr>
            <td>'.$sr.'</td>
			<td><img src="../assets/brand/'.image_order($brand->image).'" style="height: auto; width: 100px;"></td>
            <td><a href="../'.$brand->path.'">'.$brand->name.'</a></td>
            <td><img src="../assets/brand/'.image_order($brand->banner_image).'" style="height: auto; width: 100px;"></td>
			<td>
                <a href="brands?delete='.$brand->id.'"><i class="icon-trash"></i></a>
				<a href="brands?edit='.$brand->id.'"><i class="icon-pencil"></i></a>
            </td>
          </tr>
          <!--div class="mini bloc">
			<h5>
				<a href="../'.$brand->path.'">'.$brand->name.'</a>
				<div class="tools">
					<a href="brands?delete='.$brand->id.'"><i class="icon-trash"></i></a>
					<a href="brands?edit='.$brand->id.'"><i class="icon-pencil"></i></a>
				</div>
			</h5>
		</div-->';
        $sr++; }
}?>
    </tbody>
    </table>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
<?php
echo $data['footer'];
?>