<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="keywords" content="">
    <meta name="author" content="ThemeSelect">
    <!--    <title>-->
    <!--        --><?//=$title; ?>
    <!--    </title>-->
    <link rel="apple-touch-icon" href="<?=$data['tp']; ?>/admin/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$data['tp']; ?>/admin/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/vendors/data-tables/css/select.dataTables.min.css">

    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/custom/custom.css">
    <link href="<?=$data['tp']; ?>/admin/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet"
          href="https://rawgit.com/Eonasdan/bootstrap-datetimepicker/master/build/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!--    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>-->
    <!--    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->



    <!-- END: Custom CSS-->
</head>

<style type="text/css">

    .modal-window {
        position: fixed;
        background-color: rgba(200, 200, 200, 0.75);
        top: -70px;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 999;
        opacity: 0;
        pointer-events: none;
        -webkit-transition: all 0.3s;
        -moz-transition: all 0.3s;
        transition: all 0.3s;
    }

    .modal-window:target {
        opacity: 1;
        pointer-events: auto;
    }

    .modal-window>div {
        width: 800px;
        position: relative;
        margin: 10% auto;
        padding: 2rem;
        background: #fff;
        color: #444;
    }

    .modal-window header {
        font-weight: bold;
    }

    .modal-close {
        color: #aaa;
        line-height: 50px;
        font-size: 80%;
        position: absolute;
        right: 0;
        text-align: center;
        top: 0;
        width: 70px;
        text-decoration: none;
    }

    .modal-close:hover {
        color: #000;
    }

    .modal-window h1 {
        font-size: 150%;
        margin: 0 0 15px;
    }
</style>
<!-- END: Head-->

<?php echo $data['header'];
if(isset($_GET['updateStatus'])) {
    echo '<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Update Status</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="request-quotation" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
                <form action="request-quotation?updateStatus='.$_GET['updateStatus'].'" method="post" style="margin-top: 90px; max-width: 100%;">
				'.csrf_field().'
					<div class="row">
					<input type="hidden" name="id" value="'.$_GET['updateStatus'].'">
						<div class="input-field col s12">
							Update Status
							<select name="status" class="form-control" id="sel2">
								<option>Select Status</option>
								<option value="1" '.($data['qStatus']->status == '1' ? 'selected' : '').'>Approve</option>
								<option value="0" '.($data['qStatus']->status == '0' ? 'selected' : '').'>Reject</option>
							</select>
						</div>
						<div class="input-field col s12">
							<input name="updateStatus" type="submit" value="Update Status"  class="btn btn-primary btn-sm"/>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
	';
}
elseif(isset($_GET['edit'])) {
    $id = $_GET['edit'];
    $quote = DB::table('request_quotation')->where('id',$id)->first();
    $ph = \App\PhRelations::where('products', $quote->product_id)->first();
    $datas = json_decode($ph->hubs);
    $pids = \Illuminate\Support\Arr::pluck($datas, 'hub');
    $key = array_search($quote->hub_id, $pids);
    $pdata = $datas[$key];
    //dd($pdata);
    $product = DB::table('products')->where('id',$quote->product_id)->first();
    $pvalidity = strtotime($quote->validity) - strtotime(date('Y-m-d H:i:s'));
    $st = 'Pending';
    if($quote->status == 1){
        $st = 'Approved';
        if($quote->validity !== null){
            if(new DateTime() > new DateTime($quote->validity)){
                $st = 'Expired';
            }
        }
    }
    $section = 'Live';
    if($st == 'Expired'){
        $section = 'Expired';
    }
    $validity_time = '';
    if($quote->status == 0){
        $validity_time = '';
    }
    elseif ($quote->status == 1){
        $validity_time = $quote->validity;
    }

    echo  $data['notices'] ;
    echo '<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Request Quotation</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="request-quotation" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
                <form action="request-quotation?edit='.$_GET['edit'].'" method="post" style="margin-top: 90px; max-width: 100%;">
			'.csrf_field().'<div class="row">';
    $bids = explode(',', $quote->brand_id);
    $brand_names = DB::table('products')->whereIn('id', $bids)->get();
    //$name = implode(', ', $brand_names);
    $variant = json_decode($quote->variants);

    $uprice = \App\Customer::where('id', $quote->user_id)->first();

    $html = '<div style="position: relative;">
							<div class="form-group">
							<label class="form-label">Validity</label>
								<div class="input-group date" id="datetimepicker1">
									<input type="text" name="datetime" value="'.$validity_time.'" id="dateValue" class="form-control" >
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
							</div>
						</div>
						<input type="hidden" id="changeDate" value="0">
				<div class="form-group">
				<label class="form-label">Payment Option</label>
					<div class="input-field col s12">

						<select name="payment_size" class="form-control" id="" required>
							<option value="">Select Payment Option</option>
							<option '.(($quote->payment_option == 'next day') ? 'selected' : '').'>next day</option>
							<option '.(($quote->payment_option == '15 days') ? 'selected' : '').'>15 days</option>
							<option '.(($quote->payment_option == '30 days') ? 'selected' : '').'>30 days</option>
							<option '.(($quote->payment_option == 'more than 30 days') ? 'selected' : '').'>more than 30 days</option>
							<option '.(($quote->payment_option == 'Advanced Payment') ? 'selected' : '').'>Advanced Payment</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<div class="input-field col s12">
						<label class="form-label">Difference</label>
						<input name="diff_price" value="'.$quote->price_diff.'" type="text" class="form-control" style=""/>
					</div>
				</div>
				<div class="form-group">
				<label class="form-label">Action</label>
					<div class="input-field col s12">

						<select name="action" class="form-control" style="">
							<option value="1" '.(($quote->action == 1) ? 'selected' : '').'>+</option>
							<option value="0" '.(($quote->action == 0) ? 'selected' : '').'>-</option>
						</select>
					</div>
				</div>';
    if($uprice->institutional_price !== null) {
        $html .= '<div class="form-group">
				    <label class="form-label">Extra charges added to user</label>
					<div class="input-field col s12">

						<span>' . c($uprice->institutional_price) . '</span>
					</div>
				</div>';
    }
    $disablebutton = false;
    if($quote->brand_id){
        if($quote->method == 'bybasic'){

            foreach($bids as $key => $bn) {
                $proDel = DB::table('products')->where('id', $bn)->first();
                $html .= '<div class="row">
					<div class="col s12 m6">'.$proDel->title.'</div>
				</div>';
                $i = 0;

                //print_r($quote->variants);exit;
                //dd($variant);
                $html .= '
				<table class="table table-striped table-bordered" style="background-color: white;">';
                $var = null;
                foreach($variant as $vrx){
                    if($vrx->id == $bn){
                        $var = $vrx;
                    }
                }
                //dd($var);
                $pvar = DB::table('products')->where('id', $bn)->first();
                //dd($var);
                $act = '';
                $price = 0;
                $diff_price = 0;
                /*if ($var->action == 0) {
                    $act = '-';
                } elseif ($var->action == 1) {
                    $act = '+';
                }*/
                if ($quote->status == 0) {
                    if($var->var_price !== 0){
                        $diff_price = $var->var_price;
                    }else{
                        $diff_price = 0;
                    }
                } elseif ($quote->status == 1) {
                    if($var->var_price !== 0){
                        $diff_price = $var->var_price;
                    }else{
                        $diff_price = 0;
                    }
                }


                $html.='<tr style=" border: 1px solid #000000;">
						<td>
							<label class="control-label">Variant Title</label>
							<input type="hidden" name="variant_title[]" class="form-control" value="';
                if($quote->method == 'bybasic'){
                    //dd($proDel->title);
                    $html .= $proDel->title;
                }else{
                    if ($pvar !== null) {
                        $sn = DB::table('size')->where('id', $pvar->variant_title)->first();
                        $html .= htmlspecialchars($sn->name);
                    }
                }
                $html .= '"  style="background-color: white"> <br>';
                if($quote->method == 'bybasic'){
                    $html .= htmlspecialchars($var->title);
                }else{
                    if ($pvar !== null) {
                        $sn = DB::table('size')->where('id', $pvar->variant_title)->first();
                        $html .= $sn->name;
                    }
                }
                $html .= '
						  </td>
						  <td>
								<div>
									<label>Qty</label><br>
									<div>'.$var->quantity.' - '.$var->unit.'</div>
								</div>
						  </td>';
                if($var->basic_price=='waiting for update') {
                    $html .= '<td>
								<div class="input-field col s12 m12 l12">
                                Basic Price
                                <input name="updated_price" value="yes" type="hidden">
                                <input name="basic_price[]" value="' . $var->basic_price . '" type="text" class="form-control" />
                              </div>

						  </td>';
                }else{
                    $html .='<td>
								<div class="input-field col s12 m12 l12">
											Basic Price
											<input name="updated_price" value="no" type="hidden">
											<input name="basic_price[]" value="' . $var->basic_price . '" type="text" class="form-control" readonly />
										  </div>

						  </td>';
                }
                $html .= '<td>
								<div class="input-field col s12 m6">
											Variant Price
											<input name="var_price[]" value="' . $price . '" type="text" class="form-control" readonly />
										  </div>

						  </td>
					  </tr>';

                $html .= '</table>';
            }
        }else{
            foreach($bids as $key => $bn) {
                $proDel = DB::table('products')->where('id', $bn)->first();
                $html .= '<div class="row">
					<div class="col s12 m6">'.$proDel->title.'</div>
				</div>';
                $i = 0;
                $html .= '
				<table class="table table-striped table-bordered" style="background-color: white;">';
                foreach($variant->$bn as $var){
                    $pvar = DB::table('product_variants')->where('id', $var->id)->first();
                    //dd($var);
                    $act = '';
                    $price = 0;
                    $diff_price = 0;
                    /*if ($var->action == 0) {
                        $act = '-';
                    } elseif ($var->action == 1) {
                        $act = '+';
                    }*/
                    if ($quote->status == 0) {
                        if($var->var_price !== 0){
                            $diff_price = $var->var_price;
                        }else{
                            $diff_price = 0;
                        }
                    } elseif ($quote->status == 1) {
                        if($var->var_price !== 0){
                            $diff_price = $var->var_price;
                        }else{
                            $diff_price = 0;
                        }
                    }

                    //
                    $html.='<tr style=" border: 1px solid #000000;">
						<td>
							<label class="control-label">Variant Title</label>
							<input type="hidden" name="variant_title[]" class="form-control" value="';
                    if($quote->method == 'bybasic'){
                        $html .= htmlspecialchars($proDel->title);
                    }else{
                        if ($pvar !== null) {
                            $sn = DB::table('size')->where('id', $pvar->variant_title)->first();
                            $html .= htmlspecialchars($sn->name);
                        }
                    }
                    $html .= '"  style="background-color: white"> <br>';
                    if($quote->method == 'bybasic'){
                        $html .= htmlspecialchars($var->title);
                    }else{
                        if ($pvar !== null) {
                            $sn = DB::table('size')->where('id', $pvar->variant_title)->first();
                            $html .= $sn->name;
                        }
                    }
                    if($var->price_detail->price =='waiting for update'){
                        $disablebutton = true;
                    }
                    $html .= '
						  </td>
						  <td>
								<div>
									<label>Qty</label><br>
									<div>'.$var->quantity.' - '.$var->unit.'</div>
								</div>
						  </td>';
                    if($var->price_detail->price=='waiting for update') {
                        $html .= '<td>
								<div class="input-field col s12 m12 l12">
											Basic Price
											<input name="updated_price" value="yes" type="hidden">
											<input name="basic_price[]" value="' . $var->price_detail->price . '" type="text" class="form-control" />
										  </div>

						  </td>';
                    }else{
                        $html .='<td>
								<div class="input-field col s12 m12 l12">
											Basic Price
											<input name="updated_price" value="no" type="hidden">
											<input name="basic_price[]" value="' . $var->price_detail->price . '" type="text" class="form-control" readonly />
										  </div>

						  </td>';
                    }
                    $html .= '
						  <td>
								<div class="input-field col s12 m6">
									Variant Price
									<input name="var_price[]" value="' . $price . '" type="text" class="form-control" readonly />
							    </div>

						  </td>

					  </tr>';
                }
                $html .= '</table>';
            }
        }
    }elseif($quote->method == 'bybasic'){
        //dd($quote);
        //foreach($brand_names as $bn) {
        $i = 0;

        $html .='<table id="showGroupData" class="table table-striped table-bordered" style="background-color: white;">';

        //print_r($quote->variants);exit;
        foreach ($variant as $var) {
            $pvar = DB::table('product_variants')->where('id', $var->id)->first();
            //dd($var);
            $act = '';
            $price = 0;
            $diff_price = 0;
            /*if ($var->action == 0) {
                $act = '-';
            } elseif ($var->action == 1) {
                $act = '+';
            }*/
            if ($quote->status == 0) {
                if($var->var_price !== 0){
                    $diff_price = $var->var_price;
                }else{
                    $diff_price = 0;
                }
            } elseif ($quote->status == 1) {
                if($var->var_price !== 0){
                    $diff_price = $var->var_price;
                }else{
                    $diff_price = 0;
                }
            }

            //
            $html .= '<tr style=" border: 1px solid #000000;">
				<td>
					<label class="control-label">Variant Title</label>
					<input type="hidden" name="variant_title[]" class="form-control" value="';
            $html .= htmlspecialchars($var->title);
            $html .= '"  style="background-color: white"> <br>';
            $html .= htmlspecialchars($var->title);
            if($pdata->price =='waiting for update'){
                $disablebutton = true;
            }
            $html .= '
				  </td>
				  <td>
						<div class="input-field col s12 m6">
							<label class="control-label">Qty</label><br>
							<div class="input-field col s12 m6">'.$var->quantity.' - '.$var->unit.'</div>
						</div>
				  </td>';
            if($pdata->price=='waiting for update') {
                $html .= '<td>
								<div class="input-field col s12 m12 l12">
											Basic Price
											<input name="updated_price" value="yes" type="hidden">
											<input name="basic_price[]" value="' . $var->basic_price . '" type="text" class="form-control" />
										  </div>

						  </td>';
            }else{
                $html .='<td>
								<div class="input-field col s12 m12 l12">
											Basic Price
											<input name="updated_price" value="no" type="hidden">
											<input name="basic_price[]" value="' . $var->basic_price . '" type="text" class="form-control" readonly />
										  </div>

						  </td>';
            }
            $html .= '
				  <td>
						<div class="input-field col s12 m6">
									Variant Price
									<input name="var_price[]" value="' . $price . '" type="text" class="form-control" readonly />
								  </div>

				  </td>
				  </tr>';
        }
        $html .='</table>';
    }else{
        $i = 0;
        foreach ($variant as $var) {
            $proDel = DB::table('products')->where('id', $var->v_id)->first();
            $pvar = DB::table('product_variants')->where('id', $var->v_id)->first();
            //dd($var);
            $act = '';
            $price = 0;
            $diff_price = 0;
            if ($var->action == 0) {
                $act = '-';
            } elseif ($var->action == 1) {
                $act = '+';
            }
            if ($quote->status == 0) {
                if($var->var_price !== 0){
                    $diff_price = $var->var_price;
                }else{
                    $diff_price = 0;
                }
            } elseif ($quote->status == 1) {
                if($var->var_price !== 0){
                    $diff_price = $var->var_price;
                }else{
                    $diff_price = 0;
                }
            }

//
            $html .= '<tr style=" border: 1px solid #000000;">
            <td>
                <label class="control-label">Variant Title</label>
                <input type="hidden" name="variant_title[]" class="form-control" value="';
            if ($pvar !== null) {
                $sn = DB::table('size')->where('id', $pvar->variant_title)->first();
                $html .= htmlspecialchars($sn->name);
            }
            $html .= '"  style="background-color: white"> <br>';
            if ($pvar !== null) {
                $sn = DB::table('size')->where('id', $pvar->variant_title)->first();
                $html .= $sn->name;
            }
            if($pdata->price=='waiting for update'){
                $disablebutton = true;
            }
            $html .= '
              </td>
			  <td>
                    <div class="input-field col s12 m6">
						<label class="control-label">Qty</label><br>
						<div class="input-field col s12 m6">'.$var->qty.'</div>
					</div>
              </td>';
            if($var->basic_price=='waiting for update') {
                $html .= '<td>
								<div class="input-field col s12 m12 l12">
											Basic Price
											<input name="updated_price" value="yes" type="hidden">
											<input name="basic_price[]" value="' . $var->basic_price . '" type="text" class="form-control"/>
										  </div>

						  </td>';
            }else{
                $html .='<td>
								<div class="input-field col s12 m12 l12">
											Basic Price
											<input name="updated_price" value="no" type="hidden">
											<input name="basic_price[]" value="' . $var->basic_price . '" type="text" class="form-control" readonly />
										  </div>

						  </td>';
            }
            $html .= '
			  <td>
                    <div class="input-field col s12 m6">
                                Variant Price
                                <input name="var_price[]" value="' . $price . '" type="text" class="form-control" readonly />
                              </div>

              </td>
			  </tr>';
        }
        $html .='</table>';
    }
    $disbtn = '';
    if($disablebutton){ $disbtn = 'disabled'; }
    echo '' . $html . '</tr>
				<center>
					<tr>
						<td colspan="7"><input name="request_quotation" type="submit" value="Submit" class="btn btn-primary btn-sm"/></td>
					</tr>
				</center>
			</table>
		</div>
	</form>';
} else{

?>
<div class="row" style="margin-top: 10px;">
    <div class="col s12">
        <div class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
                <h5 class="card-title" style="font-size: 20px;">Manage Request Quotation</h5>

                <div class="divider"></div>
                <div class="row" style="position:relative;">
                </div>
                <?= $data['notices'] ?>

                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="datatable-editable">
                        <thead>
                        <tr class="bg-blue">
                            <th>ID</th>
                            <th>User Details</th>
                            <th>Variants - Quantity</th>
                            <th>Products</th>
                            <th>Payment Option</th>
                            <th>Other Option</th>
                            <th>Location</th>
                            <th>Method</th>
                            <th>Status</th>
                            <th>Created Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        //dd($quotations);
                        foreach ($data['quotations'] as $quotation) {
//dd($quotation->variants);
                            $bids = explode(',', $quotation->brand_id);
                            $brand_names = \Illuminate\Support\Arr::pluck(DB::table('products')->whereIn('id', $bids)->get(), 'title');
                            $name = implode(', ', $brand_names);
                            $status = $quotation->status;
                            echo '<tr>
								<td>' . $quotation->id . '</td>
								<td>' . getCustomerData($quotation->user_id) . '</td>';
                            $variant = json_decode($quotation->variants);
                            //dd($variant);
                            $html = '<ul id="myUL">';
                            if($quotation->method == 'bysize'){
                                foreach($bids as $bid){
                                    foreach ($variant->$bid as  $var) {
                                        $html .= '<li>* ' . $var->title . ' - '.$var->quantity.'</li>';
                                    }
                                }
                            }else{
                                foreach ($variant as  $var) {
                                    $html .= '<li>* ' . $var->title . ' - '.$var->quantity.'</li>';
                                }
                            }

                            echo '<td>' . $html . '</td>
								<td>' . $name . '</td>
								<td>' . $quotation->payment_option . '</td>
								<td>' . $quotation->other_option . '</td>
								<td>' . $quotation->location . '</td>
								<td>' . $quotation->method . '</td>
								<td>' . ($status == 0 ? '<span style="color:yellow;">Waiting For Update</span>' : ($status == 1 ? '<span style="color:green;">Price Updated</span>' : ($status == 2 ? '<span style="color:green;">Sent For Booking</span>' : ($status == 4 ? '<span style="color:red;">Booking cancelled</span>' : ($status == 3 ? '<span class="badge alert-warning">Booking Confirmed</span>' : '<span class="badge alert-success">Delivered</span>'))))) . '</td>
								<td>' . date('d, M Y h:i:s a', strtotime($quotation->created_at)) . '</td>
								<td>';
                            echo '<a href="request-quotation?edit='.$quotation->id.'" class="btn btn-xs btn-primary" id="quote">Edit</a><input type="hidden" id="quotation" class="form-control" value="'.$quotation->id.'">';
                            if ($status == 1) {
                                echo '<a href="request-quotation?updateStatus='.$quotation->id.'&status=1" class="btn btn-xs btn-primary" id="quote">Approved</a><input type="hidden" id="quotation" class="form-control" value="'.$quotation->id.'">';
                            }  else {
                                echo '<a href="request-quotation?updateStatus='.$quotation->id.'&status=0" class="btn btn-xs btn-primary" id="quote">Approve</a><input type="hidden" id="quotation" class="form-control" value="'.$quotation->id.'">';
                            }
                            echo '</td>
							</tr>';
                        }
                        }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
<?php
if(!empty($data['footer'])){
    echo $data['footer'];
}
?>

<script src="<?php echo $data['tp']?>/admin/js/vendors.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.js"></script>
<script src="<?php echo $data['tp']?>/admin/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<script src="<?php echo $data['tp']?>/admin/js/plugins.js" type="text/javascript"></script>
<script src="<?php echo $data['tp']?>/admin/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?php echo $data['tp']?>/admin/js/scripts/customizer.js" type="text/javascript"></script>
<script src="<?php echo $data['tp']?>/admin/js/select2.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script src="<?php echo $data['tp']?>/admin/js/scripts/ui-alerts.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
<script src="<?php echo $data['tp']?>/admin/js/bootstrap-datetimepicker.min.js"></script>


<script>
    $('.date').datetimepicker({
        defaultDate: new Date(),
        format: 'YYYY-MM-DD H:mm:ss',
        sideBySide: true
    });
</script>

<script>

    $( document ).ready(function() {
        $('#quote').on('click', function(){
            var id = $('#quotation').val();

            $.ajax({
                url: 'get_variant_group',
                type: 'post',
                data: 'id='+id+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $('#showGroupData').html('');
                    $('#showGroupData').html(data);
                    var no_of_rows = $('#showGroupData').find('tr').length;
                    var no_of_rows1 = no_of_rows - 1;
                    alert("Total:" + no_of_rows1);

                }
            });
        });

    });

    /*function DateChange() {
        alert('hi');
        $('#changeDate').val('1').trigger('change');
    };*/

</script>