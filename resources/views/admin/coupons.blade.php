<?php
echo $data['header'];
if(isset($_GET['add']))
{
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Add Coupon</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="coupons" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post" style="max-width: 100%;">
				'.csrf_field().'
				
					<div class="row">
						  <div class="input-field col s12 m4">
							Coupon code
							<input name="code" type="text" class="form-control" required />
						  </div>
						  <div class="input-field col s12 m4">
							Coupon discount
							<input name="discount" type="text" class="form-control" required />
						  </div>
						  
						  
						  <div class="input-field col s12 m4">
							Coupon type
							<select name="type" class="form-control" required>
							<option value="%">%</option>
							<option value="$">$</option>
							</select>
						  </div>
						  </div>
						  <input name="add" type="submit" value="Add coupon" style="padding: 3px 25px;" class="btn btn-primary" />
					</div>
				</form>
				
				</div>
				</div>
				</div>
				</div>';
}
elseif(isset($_GET['edit']))
{
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Edit Coupon</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="coupons" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post" style="max-width: 100%;">
				'.csrf_field().'
				
					<div class="row">
						  <div class="input-field col s12 m4">
							Coupon code
							<input name="code" value="'.$data['coupon']->code.'" type="text" class="form-control" required />
						  </div>
						  <div class="input-field col s12 m4">
							Coupon discount
							<input name="discount" value="'.$data['coupon']->discount.'" type="text" class="form-control" required />
						  </div>
						  <div class="input-field col s12 m4">
							Coupon type
							<select name="type" class="form-control" required>
							<option value="%">%</option>
							<option value="$">$</option>
							</select>
						  </div>
						  </div>
						  <input name="edit" type="submit" value="Edit coupon" style="padding: 3px 25px;" class="btn btn-primary" />
					</div>
				</form>
				</div>
				</div>
				</div>
				</div>';
} else {
    ?>
    <div class="row" style="margin-top: 13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 650px;">
    <h3>Coupons<a href="coupons?add" class="add">Add Coupons</a></h3>
    <h5 class="card-title" style="padding: 38px; color: #0d1baa;">Manage Your Coupons </h5>
    <div class="table-responsive">
    <table class="table table-striped table-bordered" id="datatable-editable">
    <thead>
    <tr class="bg-blue">
        <th>Sr. No.</th>
        <th>Coupon Code</th>
        <th>Coupon Type</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $sr = 1;
    echo $data['notices'];
    foreach ($data['coupons'] as $coupon){
        echo '
<tr>
            <td>'.$sr.'</td>
            <td>'.$coupon->code.'</td>
            <td>('.$coupon->discount.$coupon->type.')</td>
            <td>
                <a href="coupons?delete='.$coupon->id.'"><i class="icon-trash"></i></a>
					<a href="coupons?edit='.$coupon->id.'"><i class="icon-pencil"></i></a>
            </td>
          </tr>
          <!--div class="mini bloc">
			<h5>'.$coupon->code.' ('.$coupon->discount.$coupon->type.')
				<div class="tools">
					<a href="coupons?delete='.$coupon->id.'"><i class="icon-trash"></i></a>
					<a href="coupons?edit='.$coupon->id.'"><i class="icon-pencil"></i></a>
				</div>
			</h5>
			</div-->';
        $sr++;}
}?>
    </tbody>
    </table>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
<?php
echo $data['footer'];
?>