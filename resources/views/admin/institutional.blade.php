<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="keywords" content="">
    <meta name="author" content="ThemeSelect">
    <!--    <title>-->
    <!--        --><?//=$title; ?>
    <!--    </title>-->
    <link rel="apple-touch-icon" href="<?=$data['tp']; ?>/admin/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$data['tp']; ?>/admin/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/vendors/data-tables/css/select.dataTables.min.css">

    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/custom/custom.css">
    <link href="<?=$data['tp']; ?>/admin/css/select2.min.css" rel="stylesheet" />
    <!-- END: Custom CSS-->
</head>
<style type="text/css">

    .modal-window {
        position: fixed;
        background-color: rgba(200, 200, 200, 0.75);
        top: -70px;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 999;
        opacity: 0;
        pointer-events: none;
        -webkit-transition: all 0.3s;
        -moz-transition: all 0.3s;
        transition: all 0.3s;
    }

    .modal-window:target {
        opacity: 1;
        pointer-events: auto;
    }

    .modal-window>div {
        width: 800px;
        position: relative;
        margin: 10% auto;
        padding: 2rem;
        background: #fff;
        color: #444;
    }

    .modal-window header {
        font-weight: bold;
    }

    .modal-close {
        color: #aaa;
        line-height: 50px;
        font-size: 80%;
        position: absolute;
        right: 0;
        text-align: center;
        top: 0;
        width: 70px;
        text-decoration: none;
    }

    .modal-close:hover {
        color: #000;
    }

    .modal-window h1 {
        font-size: 150%;
        margin: 0 0 15px;
    }
</style>

<?php echo $data['header'];
if(isset($_GET['add_price'])) {
    echo $data['notices'].'<form action="" method="post" class="form-horizontal single"  enctype="multipart/form-data">
    '.csrf_field().'
    <h5><a href="institutional"><i class="icon-arrow-left"></i></a>Set Price Limit for Institutional</h5>
    <fieldset>
        <div class="form-group">
            <label class="control-label">Enter Amount</label>
            <input name="amount" type="text"  class="form-control"/>
        </div>
        <input name="add_price" type="submit" value="Set price" class="btn btn-primary" />
    </fieldset>
</form>';
}  elseif(isset($_GET['add_discount'])) { ?>
    <div class="row" style="margin-top: -13px;">
        <div class="col s12">
            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                <div class="card-content" style="min-height: 550px;">
                    <h5>Add Discount for Institutional</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="institutional" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
                    <div class="col s12 m12">
                        <form action="institutional?add_discount=<?=$_GET['add_discount']; ?>" method="post" class="form-horizontal" enctype="multipart/form-data" style="max-width: 100%;">
                            <?php echo csrf_field(); echo $notices; ?>
                            <div class="row">
                                <div class="form-group col-md-3" style="padding-left: 30px;padding-right:30px;">
                                    <label class="control-label">Category</label>
                                    <select id="user_type" name="category" class="form-control" required="required">
                                        <option value="">Select Category </option>
                                        <?php foreach ($data['category'] as $size){
                                            echo '<option value="'.$size->id.'">'.htmlspecialchars($size->name).'</option>';
                                        } ?>
                                    </select>
                                </div>

                                <div class="form-group col-md-2" style="padding-left: 30px;padding-right:30px;">
                                    <label class="control-label">Discount</label>
                                    <input name="discount" type="text" class="form-control" required="required"/>
                                </div>
                                <div class="form-group col-md-3" style="padding-left: 30px;padding-right:30px;">
                                    <label class="control-label">Discount Action</label>
                                    <select name="action" class="form-control" required="required">
                                        <option value="">Select Discount Action </option>
                                        <option value="0"> + </option>
                                        <option value="1"> - </option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3" style="padding-left: 30px;padding-right:30px;">
                                    <label class="control-label">Discount Type</label>
                                    <select name="discount_type" class="form-control" required="required">
                                        <option value="">Select Discount Type </option>
                                        <option value="0"> % </option>
                                        <option value="1"> Flat </option>
                                    </select>
                                </div>
                                <input name="add_discount" type="submit" value="Add Discount" style="padding:3px 25px;" class="btn btn-primary col-md-2" />
                            </div>
                        </form>
                    </div>
                    <div class="col s12"><!--1x 11661593 6YDgrn-->
                        <table id="datatable-editable" class="display">
                            <thead>
                            <tr class="bg-blue">
                                <th>Sr. No.</th>
                                <th>Category</th>
                                <th>Discount</th>
                                <th>Discount Type</th>
                                <th>Discount Action</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $sr = 1;
                            echo $data['notices'];
                            foreach ($data['customer_discount'] as $customer) {
                                $discount_type = $customer->discount_type;
                                if ($discount_type == 1) {
                                    $discount_type = 'Flat';
                                } else {
                                    $discount_type = '%';
                                }
                                $discount = $customer->action;
                                if ($discount == 1) {
                                    $discount = '-';
                                } else {
                                    $discount = '+';
                                }
                                echo '<tr>
									<td>'.$sr.'</td>
									<td>'.getCategoryName($customer->category).'</td>
									<td>'.$customer->discount.'</td>
									<td>'.$discount_type.'</td>
									<td>'.$discount.'</td>
									<td>
										<a href="institutional?edit_discount=' . $customer->id . '"><i class="icon-pencil"></i></a>
										<a href="institutional?deleteDiscount=' . $customer->id . '"><i class="icon-trash"></i></a>';
                                echo '</td>
								</tr>';
                                $sr++;
                            } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } elseif(isset($_GET['edit_discount'])) { ?>
    <div class="row" style="margin-top: -13px;">
        <div class="col s12">
            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                <div class="card-content" style="min-height: 550px;">
                    <h5>Edit Discount for Institutional</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="institutional" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
                    <div class="col s12 m12">
                        <form action="institutional?edit_discount=<?=$_GET['edit_discount']; ?>" method="post" class="form-horizontal" enctype="multipart/form-data" style="max-width: 100%;">
                            <?php echo csrf_field();  echo $notices; ?>
                            <div class="row">
                                <div class="form-group col-md-3" style="padding-left: 30px;padding-right:30px;">
                                    <label class="control-label">Category</label>
                                    <select id="user_type" name="category" class="form-control" required="required">
                                        <option value="">Select Category </option>
                                        <?php foreach ($data['category'] as $size){
                                            $catsel = '';
                                            if($data['lengths']->category == $size->id){
                                                $catsel = 'selected';
                                            }
                                            echo'<option value="'.$size->id.'" '.$catsel.'>'.htmlspecialchars($size->name).'</option>';
                                        }?>
                                    </select>
                                </div>

                                <div class="form-group col-md-2" style="padding-left: 30px;padding-right:30px;">
                                    <label class="control-label">Discount</label>
                                    <input name="discount" type="text" class="form-control" value="<?=$data['lengths']->discount; ?>" required="required"/>
                                </div>
                                <div class="form-group col-md-3" style="padding-left: 30px;padding-right:30px;">
                                    <label class="control-label">Discount Action</label>
                                    <select name="action" class="form-control" required="required">
                                        <option value="">Select Discount Action </option>
                                        <option value="0" <?php if($data['lengths']->action == 0) { echo "selected"; } ?>> + </option>
                                        <option value="1" <?php if($data['lengths']->action == 1) { echo "selected"; } ?>> - </option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3" style="padding-left: 30px;padding-right:30px;">
                                    <label class="control-label">Discount Type</label>
                                    <select name="discount_type" class="form-control" required="required">
                                        <option value="">Select Discount Type </option>
                                        <option value="0" <?php if($data['lengths']->discount_type == 0) { echo "selected"; } ?>> % </option>
                                        <option value="1" <?php if($data['lengths']->discount_type == 1) { echo "selected"; } ?>> Flat </option>
                                    </select>
                                </div>
                                <input name="edit_discount" type="submit" value="Edit Discount" style="padding:3px 25px;" class="btn btn-primary col-md-2" />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php }  else {
    ?>
    <div class="row" style="margin-top: -13px;">
        <div class="col s12">
            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                <div class="card-content" style="min-height: 550px;">
                    <h5>Users</h5>
                    <h6 class="card-title">Manage users Data</h6>

                    <div class="divider"></div>
                    <div class="row" style="position:relative;">
                        <div class="col s12 table-responsive"><!--1x 11661593 6YDgrn-->
                            <table id="datatable-editable" class="display">
                                <thead>
                                <tr class="bg-blue">
                                    <th>Sr. No.</th>
                                    <th>Institutional Name</th>
                                    <th>Institutional Email</th>
                                    <th>Pay Limit</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $sr = 1;
                                echo $data['notices'];
                                foreach ($data['customers'] as $customer) {
                                    $status = $customer->status;
                                    if ($status == 1) {
                                        $status = 'Approve';
                                    } else {
                                        $status = 'Pending';
                                    }
                                    echo '<tr>
            <td>' . $sr . '</td>
            <td>' . $customer->name . '</td>
            <td>' . $customer->email . '</td>
            <td>' . $customer->institutional_price . '</td>
            <td>' . $status . '</td>
            <td>
                <a href="sellerview?id=' . $customer->id . '"><i class="icon-eye"></i></a>
                <a href="institutional?edit=' . $customer->id . '"><i class="icon-pencil"></i></a>
				<a data-toggle="tooltip" title="Add Discount" href="institutional?add_discount='.$customer->id.'"><i class="icon-plus"></i></a>
                <a href="institutional?delete=' . $customer->id . '"><i class="icon-trash"></i></a>';
                                    if ($status == 'Approve') {
                                        echo '<a href="institutional?reject=' . $customer->id . '" class="btn btn-xs btn-danger">Reject</a>';
                                    } else {
                                        echo '<a href="institutional?approve=' . $customer->id . '" class="btn btn-xs btn-success">Approve</a>';
                                    }
                                    echo '<a href="institutional?add_price=' . $customer->id . '" class="btn btn-xs btn-info">Set Price</a>';

                                    echo '</td>
          </tr>';
                                    $sr++;
                                } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
}
echo $data['footer'];
?>

<script src="<?=$data['tp']; ?>/admin/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.js"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$data['tp']; ?>/admin/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$data['tp']; ?>/admin/js/plugins.js" type="text/javascript"></script>
<script src="<?=$data['tp']; ?>/admin/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$data['tp']; ?>/admin/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$data['tp']; ?>/admin/js/select2.min.js"></script>
<script src="<?=$data['tp']; ?>/admin/js/scripts/ui-alerts.js" type="text/javascript"></script>
<!-- <script src="<?php //$data['tp']; ?>/js/scripts/data-tables.js" type="text/javascript"></script> -->
<script>
    function editDiscount(vid){
        var ret_id = "<?=isset($_GET['add_discount']) ? $_GET['add_discount'] : 0;?>";
        $.ajax({
            url: 'get-customer-discount',
            type: 'post',
            data: 'id='+vid+'&_token=<?=csrf_token(); ?>&ret_id='+ret_id,
            success: function(datax){

                $("#edit-data").html(datax);
                $('#edit-button').trigger('click');

                $(document).ready(function() {
                    var max_fields      = 10; //maximum input boxes allowed
                    var wrapper   		= $(".input_fields_wrap1"); //Fields wrapper
                    var add_button      = $(".add_field_button1"); //Add button ID


                    var x = 1; //initlal text box count
                    $(add_button).click(function(e){ //on add input button click
                        e.preventDefault();
                        if(x < max_fields){ //max input box allowed
                            x++; //text box increment
                            $(wrapper).append('<div><input type="text" name="length[]"/><a href="#" class="remove_field1">Remove</a></div>'); //add input box
                        }
                    });

                    $(wrapper).on("click",".remove_field1", function(e){ //user click on remove text
                        e.preventDefault(); $(this).parent('div').remove(); x--;
                    })
                });
            }
        });
    }
</script>
<?php if(isset($_GET['add_discount'])) { ?>
    <button type="hidden" data-toggle="modal" data-target="#edit-variant" id="edit-button"></button>
    <div class="modal fade in" id="edit-variant" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="margin-top: 165px;">
            <div style="width:100%; height:100%;" class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Discount</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -35px;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="edit-data"></div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>