<?php
echo $data['header'];
if(isset($_GET['add']))
{
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 550px;">
    <h5>Add new slide</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="slider" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
    <form action="" method="post" enctype="multipart/form-data" style="max-width: 100%;">
				'.csrf_field().'
				
					<div class="row">
						  <div class="form-group col-md-6">
							<label class="control-label">Slide name</label>
							<input name="title" type="text"  class="form-control" required/>
						  </div>
						  <div class="form-group col-md-6">
							<label class="control-label">Slide link</label>
							<input name="link" type="text"  class="form-control" required/>
						  </div>
						  <div class="form-group col-md-6">
							<label class="control-label">Slide image</label>
							<input type="file" class="form-control" name="image" required/>
						  </div>
						  </div>
						  <input name="add" type="submit" value="Add slide" style="padding:3px 25px;" class="btn btn-primary" />
					
				</form>
				</div>
				</div>
				</div>
				</div>';
} elseif(isset($_GET['edit'])){
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 650px;">
    <form action="" method="post" enctype="multipart/form-data" class="form-horizontal single">
			'.csrf_field().'
				<h5><a href="slider"><i class="icon-arrow-left"></i></a>Update slide</h5>
					<div class="row">
						  <div class="form-group">
							<label class="control-label">Slide name</label>
							<input name="title" type="text" value="'.$data['slide']->title.'" class="form-control"  required/>
						  </div>
						  <div class="form-group">
							<label class="control-label">Slide link</label>
							<input name="link" type="text" value="'.$data['slide']->link.'" class="form-control"  required/>
						  </div>
						  <p>Uploading new file will overwrite current file .</p>
						  <div class="form-group">
							<label class="control-label">Image</label>
							<input type="file" class="form-control" name="image" required/>
						  </div>
						  <input name="edit" type="submit" value="Update slide" class="btn btn-primary" />
					</div>
				</form>
				</div>
				</div>
				</div>
				</div>';
} else { ?>
    <div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 550px;">
    <h3>Slider<a href="slider?add" style="padding:3px 15px;" class="add">Add slide</a></h3>
    <p>Manage your website front page slides </p>

    <?php
    echo $data['notices'];
    foreach ($data['slides'] as $slide){
        echo '<div class="bloc">
				<h5>
					'.$slide->title.'
					<div class="tools">
						<a href="slider?delete='.$slide->id.'"><i class="icon-trash"></i></a>
						<a href="slider?edit='.$slide->id.'"><i class="icon-pencil"></i></a>
					</div>
				</h5>
				<img src="../assets/slider/'.$slide->image.'" width="100%">
			</div>';}
}
?>
<?php
echo $data['footer'];
?>