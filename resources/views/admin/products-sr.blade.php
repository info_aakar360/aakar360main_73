<?php
echo $header;
?>
<div class="head">
<h3><?=$data['title']?></h3>
<p>Manage your products special requirements</p>
</div>
<div class="row">
    <div class="col-lg-12">
    <div class="table-responsive">
        <table class="table table-striped table-bordered" id="datatable-editable">
            <thead>
            <tr class="bg-blue">
                <th>Sr. No.</th>
                <th>Customer</th>
                <th>Product</th>
                <th>Message</th>
                <th>File</th>
                <th>Date</th>
            </tr>
            </thead>
            <tbody>
<?php
    $srn = 1;
	echo $notices;
	foreach ($srs as $sr){
		echo'<tr>
            <td>'.$srn.'</td>
			<td>'.getCustomerData($sr->user_id).'</td>
            <td>'.getProductName($sr->product_id).'</td>
            <td>'.$sr->message.'</td>
			<td>
                <a href="'.url('assets/images/'.$sr->file).'" download><i class="icon-cloud-download"></i> Download</a>
            </td>
            <td>'.date_format(date_create($sr->created_at), "d M, Y").'</td>
          </tr>';
	$srn++; }
?>
            </tbody>
        </table>
    </div>
    </div>
    </div>
<?php
echo $footer;
?>