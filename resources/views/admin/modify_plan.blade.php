<?php
echo $data['header'];
if(isset($_GET['edit'])){
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Edit plan status</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="modify_plan" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
                <form action="" method="post" class="form-horizontal single" enctype="multipart/form-data">
			'.csrf_field().'
			
				<div class="row">
					  <div class="form-group">
						<label class="control-label">Status</label>
						<select name="status" class="form-control">';
    if($data['plan']->status == 'Processing') {
        echo '<option value="">Please Select Status</option>
                                <option value="Processing" selected>Processing</option>
                                <option value="Viewed">Viewed</option>
                                <option value="Hold">Hold</option>
                                <option value="Pending">Pending</option>
                                <option value="Completed">Completed</option>
                                <option value="Cancelled">Cancelled</option>';
    } else if($data['plan']->status == 'Viewed') {
        echo '<option value="">Please Select Status</option>
                                <option value="Processing" >Processing</option>
                                <option value="Viewed" selected>Viewed</option>
                                <option value="Hold">Hold</option>
                                <option value="Pending">Pending</option>
                                <option value="Completed">Completed</option>
                                <option value="Cancelled">Cancelled</option>';
    } else if($data['plan']->status == 'Hold') {
        echo '<option value="">Please Select Status</option>
                                <option value="Processing" >Processing</option>
                                <option value="Viewed" >Viewed</option>
                                <option value="Hold" selected>Hold</option>
                                <option value="Pending">Pending</option>
                                <option value="Completed">Completed</option>
                                <option value="Cancelled">Cancelled</option>';
    } else if($data['plan']->status == 'Pending') {
        echo '<option value="">Please Select Status</option>
                                <option value="Processing" >Processing</option>
                                <option value="Viewed" >Viewed</option>
                                <option value="Hold" >Hold</option>
                                <option value="Pending" selected>Pending</option>
                                <option value="Completed">Completed</option>
                                <option value="Cancelled">Cancelled</option>';
    } else if($data['plan']->status == 'Completed') {
        echo '<option value="">Please Select Status</option>
                                <option value="Processing" >Processing</option>
                                <option value="Viewed" >Viewed</option>
                                <option value="Hold" >Hold</option>
                                <option value="Pending" >Pending</option>
                                <option value="Completed" selected>Completed</option>
                                <option value="Cancelled">Cancelled</option>';
    }
    else if($data['plan']->status == 'Cancelled') {
        echo '<option value="">Please Select Status</option>
                                <option value="Processing" >Processing</option>
                                <option value="Viewed" >Viewed</option>
                                <option value="Hold" >Hold</option>
                                <option value="Pending" >Pending</option>
                                <option value="Completed" >Completed</option>
                                <option value="Cancelled" selected>Cancelled</option>';
    }
    echo '</select>
					  </div>
					  </div>
					  <input name="edit" type="submit" value="Edit services" style="padding:3px 25px;" class="btn btn-primary" />
				
			</form>';
    ?>


<?php } else { ?>

    <div class="row" style="margin-top: -13px;">
        <div class="col s12">
            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                <div class="card-content" style="min-height: 550px;">
                    <h5>Plan Request(s)</h5>
                    <h5 class="card-title" style="color: #0d1baa;">Manage your plan request(s)</h5>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="datatable-editable">
                            <thead>
                            <tr class="bg-blue">
                                <th>Sr. No.</th>
                                <th>Plan Name</th>
                                <th>User Name</th>
                                <th>Answer</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $sr = 1;
                            echo $data['notices'];
                            foreach ($data['plans'] as $answer){
                                echo'<tr>
                        <td>'.$sr.'</td>';
                                $p = $answer->plan_id;
                                $pl = DB::table('layout_plans')->where('id',$p)->first();
                                if($pl !== null){
                                    echo '<td>'.$pl->title.'</td>';
                                }else{ echo '<td>NA</td>'; }
                                $u = $answer->user_id;
                                $user = DB::table('customers')->where('id',$u)->first();
                                if($user !== null){
                                    echo '<td>'.$user->name.'</td>';
                                }else{ echo '<td>NA</td>'; }
                                echo '<td>';
                                $ans = json_decode($answer->answer);
                                //dd($ans);
                                foreach ($ans as $key=>$value){
                                    //dd($value);
                                    if($key == 'pincode') {
                                        echo '<b>Pincode :</b> ' . $value.'<br>';
                                    } else {
                                        $que = DB::table('questions')->where('id',$key)->first();
                                        if($que !== null){
                                            echo '<b>Question :</b> '.$que->question.'<br> <b>Answer :</b> '.$value.'<br>';
                                        }else{ echo '<td>NA</td>'; }
                                    }
                                }
                                echo '</td>
                        <td>'.$answer->status.'</td>
                        <td><a href="modify_plan?edit='.$answer->id.'"><i class="icon-question"></i></a>
                        </td></tr>';

                                $sr++;}
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<style>
    .button {
        background: gainsboro;
        padding: 5px 20px;
        cursor: pointer;
        border-radius: 30px;
    }
    .mini-button {
        cursor: pointer;
        border-radius: 30px;
        background: transparent;
        padding: 5px 0px;
        display: block;
    }
</style>

<?php
echo $data['footer'];
?>



