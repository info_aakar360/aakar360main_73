<?php
echo $data['header'];
if(isset($_GET['add'])) {
    echo $data['notices'] . '<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            
            <h5>Add Faq</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="product_faq" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post" enctype="multipart/form-data" style="max-width:100%;">
			' . csrf_field() . '
			
				<div class="row">
				        <div class="input-field col s12 m6">
						Question
						<input type="text" class="form-control" name="question" placeholder="Write Question Here" required="required">
					  </div>
					  <div class="input-field col s12 m6">
						Answer
						<textarea class="form-control" name="answer" rows="5" placeholder="Write Answer Here" required="required"></textarea>
					  </div>
					  </div>
					  <div class="row">
					  <div class="input-field col s12 m6">
						Status
						<select name="status" class="form-control">
						    <option value="">Please Select Status</option>
                            <option value="0">Pending</option>
                            <option value="1">Approved</option>
                        </select>
					  </div>
					  <div class="input-field col s12 m6">
							Category
							<select name="category[]" multiple class="form-control select2">';
    foreach ($data['categories'] as $brand){
        echo '<option value="'.$brand->id.'">'.$brand->name.'</option>';
        $childs = DB::select("SELECT * FROM category WHERE parent = ".$brand->id);
        foreach ($childs as $child){
            echo '<option value="'.$child->id.'">- '.$child->name.'</option>';
            $subchilds = DB::select("SELECT * FROM category WHERE parent = ".$child->id);
            foreach ($subchilds as $subchild){
                echo '<option value="'.$subchild->id.'"><i style="padding-left: 10px;">--> '.$subchild->name.'</i></option>';
            }
        }
    }
    echo '</select>
                      </div>
                      </div>
                      <div class="row">
                      <div class="input-field col s12 m6">
							Section
							<select name="section" class="form-control select2">
							    <option value="retail">Retail</option>
							    <option value="institutional">Institutional</option>
							</select>
                        </div>
					  </div> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp;
					  
					  <input name="add" type="submit" value="Add faq" class="btn btn-primary" />
				</div>
			</form>
			
			</div>
			</div>
			</div>
			</div>';
}
elseif(isset($_GET['edit'])){
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Edit Faq</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="product_faq" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post" enctype="multipart/form-data" style="max-width: 100%;">
			'.csrf_field().'
			
				<div class="row">
                    <div class="input-field col s12 m6">
						Question
						<textarea class="form-control" name="question" rows="5" placeholder="Write answer here" required="required">'.$data['faq']->question.'</textarea>
					  </div>
					 <div class="input-field col s12 m6">
						Answer
						<textarea class="form-control" name="answer" rows="5" placeholder="Write answer here" required="required">'.$data['faq']->answer.'</textarea>
					  </div>
					  </div>
					  <div class="row">
					  <div class="input-field col s12 m6">
						Status
						<select name="status" class="form-control">';
    if($data['faq']->status == 0) {
        echo '<option value="">Please Select Status</option>
                                <option value="0" selected>Pending</option>
                                <option value="1">Approved</option>';
    } else if($data['faq']->status == 1) {
        echo '<option value="">Please Select Status</option>
                                <option value="0" >Pending</option>
                                <option value="1" selected>Approved</option>';
    }
    echo '</select>
					  </div>
					  <div class="input-field col s12 m6">
							Product category
							<select name="category[]" multiple class="form-control select2">';
    foreach ($data['categories'] as $category){
        echo '<option value="'.$category->id.'" '.($category->id == $data['faq']->category ? 'selected' : '').'>'.$category->name.'</option>';
        $childs = DB::select("SELECT * FROM category WHERE parent = ".$category->id." ORDER BY id DESC");
        foreach ($childs as $child){
            echo '<option value="'.$child->id.'" '.($child->id == $data['faq']->category ? 'selected' : '').'>- '.$child->name.'</option>';
            $subchilds = DB::select("SELECT * FROM category WHERE parent = ".$child->id." ORDER BY id DESC");
            foreach ($subchilds as $subchild){
                echo '<option value="'.$subchild->id.'" '.($subchild->id == $data['faq']->category ? 'selected' : '').'><i style="padding-left: 10px;">--> '.$subchild->name.'</i></option>';
            }
        }
    }
    echo '</select>
						  </div>
						   </div>
						  <div class="row">
						  <div class="input-field col s12 m6">
							Section
							<select name="section" class="form-control select2">
							    <option value="retail" '.($data['faq']->section == 'retail' ? 'selected="selected"' : '').'>Retail</option>
							    <option value="institutional" '.($data['faq']->section == 'institutional' ? 'selected="selected"' : '').'>Institutional</option>
							</select>
                        </div>
                       </div> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp;

					  <input name="edit" type="submit" value="Edit faq" style="padding:3px 25px;" class="btn btn-primary" />
				</div>
				
			</form>
			</div>
			</div>
			</div>
			</div>';
    ?>


<?php } else { ?>
    <div class="row" style="margin-top: -13px;">
        <div class="col s12">
            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                <div class="card-content" style="min-height: 550px;">
                    <h3>Products Faq<a href="product_faq?add" style="padding:3px 15px;" class="add">Add Faq</a></h3>
                    <h5 class="card-title" style="color: #0d1baa;">Manage Your Products faq </h5>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="datatable-editable">
                            <thead>
                            <tr class="bg-blue">
                                <th>Sr. No.</th>
                                <th>Question</th>
                                <th>Answer</th>
                                <th>Status</th>
                                <th>Section</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $sr = 1;
                            echo $data['notices'];
                            foreach ($data['faqs'] as $faq){

                                echo'<tr>
                        <td>'.$sr.'</td>
                        <td>'.$faq->question.'</td>
                        <td>'.$faq->answer.'</td>';
                                if($faq->status == 1){
                                    $s = 'Approved';
                                }else{
                                    $s = 'Pending';
                                }
                                echo '<td>'.$s.'</td>
                        <td>'.$faq->section.'</td>
                        <td><a href="product_faq?edit='.$faq->id.'"><i class="icon-pencil"></i></a>
                        </td></tr>';

                                $sr++;}
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
<?php } ?>
<style>
    .button {
        background: gainsboro;
        padding: 5px 20px;
        cursor: pointer;
        border-radius: 30px;
    }
    .mini-button {
        cursor: pointer;
        border-radius: 30px;
        background: transparent;
        padding: 5px 0px;
        display: block;
    }
</style>

<?php
echo $data['footer'];
?>
<script>
    $( document ).ready(function() {
        $('.select2').select2({
            placeholder: "Select categories",
            allowClear: true
        });
    });
</script>


