<?php
echo $data['header'];
if(isset($_GET['add'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Add How it Works</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="s-how-it-works" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
                <form action="" method="post"  enctype="multipart/form-data" style="max-width: 100%;">
			'.csrf_field().'
			
				<div class="row">
				        <div class="input-field col s12 m6">
							Category
							<select id="country" name="cat[]" class="form-control select2" multiple>';
    foreach ($data['service_cats'] as $cat){
        echo '<option value="'.$cat->id.'">'.$cat->name.'</option>';
    }
    echo '</select>
						</div>
					  <div class="input-field col s12 m6">
						Tab Title
						<input name="tab_title" type="text"  class="form-control"/>
					  </div>
					  </div>
					  <div class="row">
					  <div class="input-field col s12 m6">
						Tab Icon
						<input name="tab_icon" type="text"  class="form-control"/>
					  </div>
					  <div class="input-field col s12 m6">
						Content Title
						<input name="content_title" type="text"  class="form-control"/>
					  </div>
					  </div>
					  <div class="row">
					  <div class="input-field col s12 m6">
						Content
						<textarea name="content" type="text" class="form-control"></textarea>
					  </div>
					  <div class="input-field col s12 m6">
						image
						<input name="image" type="file" class="form-control" required="required" />
					  </div>
					  </div>
					  
					  <input name="add" type="submit" value="Add how it works" class="btn btn-primary" />
				 </div>
			</form>
			 </div>
			  </div>
			   </div>
			    </div>';
} elseif(isset($_GET['edit'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Edit How it Works</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="s-how-it-works" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
                <form action="" method="post"  enctype="multipart/form-data" style="max-width: 100%;">
			'.csrf_field().'
			
				<div class="row">
				        <div class="input-field col s12 m6">
							Category
							<select id="country" name="cat[]" class="form-control select2" multiple>';
    $catx = explode(',', $data['how']->cat);
    foreach ($data['service_cats'] as $cat){
        $selected = in_array($cat->id, $catx) ? 'selected' : '';
        echo '<option value="'.$cat->id.'" '.$selected.'>'.$cat->name.'</option>';
    }
    echo '</select>
						  </div>
					  <div class="input-field col s12 m6">
						>Tab Title
						<input name="tab_title" type="text" value="'.$data['how']->tab_title.'"  class="form-control"/>
					  </div>
					  </div>
					  <div class="row">
					  <div class="input-field col s12 m6">
						Tab Icon
						<input name="tab_icon" type="text" value="'.$data['how']->tab_icon.'"  class="form-control"/>
					  </div>
					  <div class="input-field col s12 m6">
						Content Title
						<input name="content_title" type="text" value="'.$data['how']->content_title.'"  class="form-control"/>
					  </div>
					  </div>
					  <div class="row">
					  <div class="input-field col s12 m12">
						Content
						<textarea name="content" type="text" class="form-control">'.$data['how']->content.'</textarea>
					  </div></div><div class="row" style="margin-bottom: 10px;">';
    if (!empty($data['how']->image)){
        echo '<p>Uploading new images will overwrtite current images .</p>';
        echo '<img class="col-md-2" src="'.url('/assets/how_it_works/'.$data['how']->image).'" required="required"/>';
							  echo '<div class="clearfix"></div>';
						  }
    if (!empty($data['banner']->download)){
        echo '<p>Uploading new file will overwrite current file .</p>';
    }
    echo '
					  <div class="input-field col s12 m6">
						image
						<input name="image" type="file" class="form-control"  />
					  </div>
					  </div>
					  <input name="edit" type="submit" value="Edit how it works" style="padding:3px 25px;;" class="btn btn-primary" />
				</div>
			</form>
			</div>
			</div>
			</div>
			</div>';
} else {
?>
<div class="row" style="margin-top: 10px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
                <h5>How it Works<a href="s-how-it-works?add"  class="add" style="padding:3px 15px; font-size: 15px" >Add how it works</a></h5>
                <h5 class="card-title" style=" color: #0d1baa;">Manage Your how it works</h5>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="datatable-editable">
                                <thead>
                                <tr class="bg-blue">
                                    <th>Sr. No.</th>
                                    <th>Image</th>
                                    <th>Category</th>
                                    <th>Tab Title</th>
                                    <th>Content Title</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $sr = 1;
                                echo $data['notices'];
                                foreach ($data['s_how'] as $show){
                                    echo'<tr>
            <td>'.$sr.'</td>
			<td><img src="../assets/how_it_works/'.$show->image.'" style="height: auto; width: 100px;"></td>';
                                    $cats = $show->cat;
                                    $c = explode(',', $cats);
                                    echo '<td>';
                                    foreach($c as $cs) {
                                        $ca = DB::select("SELECT * FROM services_category WHERE id = " . $cs)[0];
                                        echo $ca->name.', ';
                                    }
                                    echo'</td>
			<td>'.$show->tab_title.'</td>
			<td>'.$show->content_title.'</td>
			<td>
                <a href="s-how-it-works?delete='.$show->id.'"><i class="icon-trash"></i></a>
				<a href="s-how-it-works?edit='.$show->id.'"><i class="icon-pencil"></i></a>
            </td>
          </tr>';
                                    $sr++; }
                                } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    echo $data['footer'];
    ?>
    <script>
        $(document).ready(function(){
            $('.select2').select2();
        });
    </script>