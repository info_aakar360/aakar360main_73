<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="keywords" content="">
    <meta name="author" content="ThemeSelect">
    <!--    <title>-->
    <!--        --><?//=$title; ?>
    <!--    </title>-->
    <link rel="apple-touch-icon" href="<?=$data['tp']; ?>/admin/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$data['tp']; ?>/admin/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/vendors/data-tables/css/select.dataTables.min.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/custom/custom.css">
    <link href="<?=$data['tp']; ?>/admin/css/select2.min.css" rel="stylesheet" />
    <!-- END: Custom CSS-->
</head>
<?php
echo $data['header'];
if(isset($_GET['add']))
{
    ?>
    <div class="row" style="margin-top: -13px;">
        <div class="col s12">
            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                <div class="card-content" style="min-height: 550px;">
                    <h5>Add Size</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="size" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
                    <form action="" method="post" style="max-width: 100%;">
                        <?=csrf_field(); echo $data['notices']; ?>
						<div class="row">
							<div class="input-field col s12 m6">
								Name
								<input name="name" type="text" class="form-control" required />
							</div>
							<div class="input-field col s12 m6">
								Select Group
								<select name ="group_id" class="form-control">
									<?php
									foreach ($data['groups'] as $group){
										?>
										<option  value="<?=$group->id?>"><?=$group->name?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="row">
								<div class="row" style="margin-bottom: 20px;">
									<div class="input-field col s12">
										Price (Size Difference)
										<input type="hidden" id="option_count"></input>
										<input name="price[]" type="text" class="form-control" required style="float: left;" />
										<div id="add_option" class="button pull-right mini-button"><i class="icon-plus"></i></div>
									</div>
								</div>
								<div class="col-lg-12" id="options"></div>
								<div class="col-lg-1"></div>
							</div>
							<div class="row">
								<div class="form-group" style="width:100%">
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group" style="float: left; padding:10px;">
												<label class="control-label" style="font-size: 10px;">Length <input type="checkbox"> </label>
												<input name="length" type="text" class="form-control" style="background-color: white; border: 1px solid #000000;"/>
											</div>
											<div class="form-group" style="float: left; padding:10px;">
												<label class="control-label">Length Unit</label>
												<select name="lunit" class="form-control">
													<option value="">Select Unit</option>
													<?php foreach ($data['units'] as $uni){
														echo '<option value="'.$uni->id.'">'.$uni->name.'</option>';
													} ?>
												</select>
											</div>
											<div class="form-group" style="float: left; padding:10px;">
												<label class="control-label" style="font-size: 10px;">Weight <input type="checkbox" name="w_available" value="available"> </label>
												<input name="weight" type="text" class="form-control" style="background-color: white; border: 1px solid #000000;"/>
											</div>
											<!--<div class="form-group" style="float: left; padding:10px;">
												<label class="control-label" style="font-size: 10px;">Light <input type="checkbox" name="l_available" value="available"></label>
												<input name="light[]" type="text" class="form-control" style="background-color: white; border: 1px solid #000000;"/>
											</div>
											<div class="form-group" style="float: left; padding:10px;">
												<label class="control-label" style="font-size: 10px;">S. Light <input type="checkbox" name="s_available" value="available"></label>
												<input name="super_light[]" type="text" class="form-control" style="background-color: white; border: 1px solid #000000;"/>
											</div>-->
											<div class="form-group" style="float: left; padding:10px;">
												<label class="control-label">Unit</label>
												<select name="wunit" class="form-control">
													<option value="">Select Unit</option>
													<?php foreach ($data['units'] as $uni){
														echo '<option value="'.$uni->id.'">'.$uni->name.'</option>';
													} ?>
												</select>
											</div>
											<!--<div id="add_weight_option" class="button pull-right mini-button"><i class="icon-plus"></i></div>-->
										</div>
										
										<div class="col-lg-12" id="weight_options"></div>
										<div class="col-lg-1"></div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s12 m6">
									Priority
									<input name="priority" type="text" class="form-control" required value='0' />
								</div>
							</div>
							<div class="row">
								<input name="add" type="submit" value="Add size" class="btn btn-primary" />
							</div>
						</div>
					</form>
            </div>
        </div>
    </div>
    <?php
}
elseif(isset($_GET['edit']))
{
?>
<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
                <h5>Edit Size</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="size" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
                <form action="" method="post" style="max-width: 100%">
                    <?=csrf_field(); echo $data['notices']; ?>
                    <div class="row">
                        <div class="input-field col s12 m4">Name
                            <input name="var_id" value="<?=$_GET['edit'];?>" type="hidden" />
                            <input name="name" value="<?=htmlspecialchars($data['size']->name)?>" type="text" class="form-control" required />
                        </div>
                        <div class="input-field col s12 m4">
                            Select Group
                            <select name ="group_id" class="form-control">
                                <option value="">Select Group</option>
                                <?php
                                $delcheck = DB::table('sizes_group')->whereRaw("FIND_IN_SET(?, sizes)", $_GET['edit'])->first();
                                $sel = 0;
                                if($delcheck !== null){
                                    $sel = $delcheck->id;
                                }
                                foreach ($data['groups'] as $group){
                                    ?>
                                    <option  value="<?=$group->id?>" <?php if($group->id == $sel) echo 'selected'; ?>><?=$group->name?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <!--                        <div class="input-field col s12 m4">-->
                        <!--                            Price (Size Difference)-->
                        <!--                        </div>-->
                        <div class="row" style="margin-bottom: 20px;">
                            Price (Size Difference)
                            <?php $p[] = ''; foreach ($data['price'] as $price){
                                if($price !== ''){
                                $variants = DB::table('product_variants')
                                    ->join('products','products.id','=','product_variants.product_id')
                                    ->where('product_variants.price',$price->id)
                                    ->select('products.title')
                                    ->groupBy('product_variants.product_id')
                                    ->get();
                                $px = '<div class="input-field col s12 col-lg-12">
                                            <input name="price_id[]" value="'.$price->id.'" type="hidden" class="form-control col-lg-11" />
                                            <input name="price['.$price->id.']" value="'.$price->price.'" type="text" class="form-control col-lg-11" />
                                            <div class="col-sm-1">
                                                <span class="remove-option mini-button"><i class="icon-close"></i></span>
                                            </div>
                                        </div>';
                                $v = array();
                                foreach($variants as $var){
                                    $v[] = $var->title;
                                }
                                $p[] = $px.' - '. implode(', ', $v);
                                ?>

                            <?php } } if($p){ echo implode('<br/><hr/>', $p); } ?>
                            <div class="row">&nbsp;</div>
                            <div class="input-field col s12">
                                <div id="add_option_edit" class="pull-right mini-button btn btn-primary">Add More</div>
                            </div>
                        </div>
                        <div class="col-lg-12" id="options"></div>
                        <div class="col-lg-1"></div>
                    </div>
						<div class="row">
							<div class="form-group" style="width:100%">
								<div class="row">
								Weight
									<div class="col-lg-12">
										<div class="form-group" style="float: left; padding:10px;">
											<label class="control-label" style="font-size: 10px;">Length <input type="checkbox"> </label>
											<input name="length" type="text" class="form-control" value="<?php if($data['sizeWeight']){ echo $data['sizeWeight']->length; } ?>" style="background-color: white; border: 1px solid #000000;"/>
											<input name="sizeId" type="hidden" class="form-control" value="<?php if($data['sizeWeight']){ echo $data['sizeWeight']->id; } ?>" style="background-color: white; border: 1px solid #000000;"/>
										</div>
										<div class="form-group" style="float: left; padding:10px;">
											<label class="control-label">Length Unit</label>
											<select name="lunit" class="form-control">
												<option value="">Select Unit</option>
												<?php foreach ($data['units'] as $uni){
													$lus = '';
													if($data['sizeWeight']){ 
														$lus = $data['sizeWeight']->lunit == $uni->id ? 'selected' : '';
													}
													echo '<option value="'.$uni->id.'" '.$lus.'>'.$uni->name.'</option>';
												} ?>
											</select>
										</div>
										<div class="form-group" style="float: left; padding:10px;">
											<label class="control-label" style="font-size: 10px;">Weight <input type="checkbox" name="w_available" value="available"> </label>
											<input name="weight" type="text" class="form-control" value="<?php if($data['sizeWeight']){ echo $data['sizeWeight']->weight; } ?>" style="background-color: white; border: 1px solid #000000;"/>
										</div>
										<!--<div class="form-group" style="float: left; padding:10px;">
											<label class="control-label" style="font-size: 10px;">Light <input type="checkbox" name="l_available" value="available"></label>
											<input name="light[]" type="text" class="form-control" value="<?php if($data['sizeWeight']){ echo $data['sizeWeight']->light; } ?>" style="background-color: white; border: 1px solid #000000;"/>
										</div>
										<div class="form-group" style="float: left; padding:10px;">
											<label class="control-label" style="font-size: 10px;">S. Light <input type="checkbox" name="s_available" value="available"></label>
											<input name="super_light[]" type="text" class="form-control" value="<?php if($data['sizeWeight']){ echo $data['sizeWeight']->super_light; } ?>" style="background-color: white; border: 1px solid #000000;"/>
										</div>-->
										<div class="form-group" style="float: left; padding:10px;">
											<label class="control-label">Unit</label>
											<select name="wunit" class="form-control">
												<option value="">Select Unit</option>
												<?php foreach ($data['units'] as $uni){
													$wus = '';
													if($data['sizeWeight']){ 
														if($data['sizeWeight']->wunit == $uni->id){
															$wus = 'selected';
														}
													}
													echo '<option value="'.$uni->id.'" '.$wus.'>'.$uni->name.'</option>';
												} ?>
											</select>
										</div>
										<!--<div id="edit_weight_option" class="button pull-right mini-button btn btn-primary">Add More weight</div>-->
									</div>
									
									<div class="col-lg-12" id="edit_weight_options"></div>
									<div class="col-lg-1"></div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s12 m6">
								Priority
								<input name="priority" type="text" class="form-control" required value="<?=$data['size']->priority; ?>" />
							</div>
						</div>
                        <input name="edit" type="submit" value="Edit size" class="btn btn-primary" />
                </form>
                <?php }
                elseif(isset($_GET['multiple_edit']))
                {
                    ?>
                    <div class="row" style="margin-top: -13px;">
                        <div class="col s12">
                            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                                <div class="card-content" style="min-height: 550px;">
                                    <h5>Multiple Edit</h5>
                                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                                        <a class="btn myblue waves-light " style="padding:0 5px;" href="size" >
                                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                                        </a>
                                    </div>
                                    <form action="" method="post" style="max-width: 100%;">
                                        <?=csrf_field(); echo $data['notices']; ?>
                                        <div class="col-12">
                                            <div class="input-field col s12 m4">
                                                Select Group
                                                <select name ="group_id" class="form-control" id="multiple_group">
                                                    <option value="0">Select Group</option>
                                                    <?php
                                                    foreach ($data['groups'] as $group){
                                                        ?>
                                                        <option  value="<?=$group->id?>"><?=$group->name?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">&nbsp;</div>
                                        <div class="row">
                                            <div class="col-lg-12" id="multipleData"></div>
                                        </div>
                                        <div class="row">&nbsp;</div>
                                        <div class="input-field col s12 col-lg-12">
                                            <button type="submit" id="" class="btn btn-info" style="float: right;" name="multiple_edit">Submit</button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                <?php } else {
                    ?>
                    <div class="row" style="margin-top: -13px;">
                        <div class="col s12">
                            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                                <div class="card-content" style="min-height: 550px;">
                                    <div class="col-12">
                                        <div class="input-field col s12 m4">
                                            Select Group
                                            <select name ="group_id" class="form-control" id="group">
                                                <option value="0">Select Group</option>
                                                <?php
                                                foreach ($data['groups'] as $group){
                                                    ?>
                                                    <option  value="<?=$group->id?>"><?=$group->name?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="input-field col s12 m4">
                                            <button type="text" id="btnFiterSubmitSearch" class="btn btn-info">Submit</button>
                                        </div>
                                    </div>
                                    <h5>Size<a href="size?add" style="padding:3px 15px; font-size: 15px;" class="add">Add Size</a><a href="size?multiple_edit" style="padding:3px 15px; font-size: 15px;" class="add">Multiple Edit</a></h5>
                                    <?php echo $data['notices']; ?>
									<h5 class="card-title" style="color: #0d1baa;">Manage Your Size</h5>
                                    <div class="col-lg-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="laravel_datatable">
                                                <thead>
                                                <tr class="bg-blue">
                                                    <th>Sr. No.</th>
                                                    <th>Name</th>
                                                    <th>Price</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>

                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php }
                echo $data['footer'];
                ?>
                <script src="<?=$data['tp']; ?>/admin/js/vendors.min.js" type="text/javascript"></script>
                <!-- BEGIN VENDOR JS-->
                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
                <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.js"></script>
                <!-- BEGIN PAGE VENDOR JS-->
                <script src="<?=$data['tp']; ?>/admin/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
                <!-- END PAGE VENDOR JS-->
                <!-- BEGIN THEME  JS-->
                <script src="<?=$data['tp']; ?>/admin/js/plugins.js" type="text/javascript"></script>
                <script src="<?=$data['tp']; ?>/admin/js/custom/custom-script.js" type="text/javascript"></script>
                <script src="<?=$data['tp']; ?>/admin/js/scripts/customizer.js" type="text/javascript"></script>
                <!-- END THEME  JS-->
                <!-- BEGIN PAGE LEVEL JS-->
                <!-- END PAGE LEVEL JS-->
                <script src="<?=$data['tp']; ?>/admin/js/select2.min.js"></script>
                <script src="<?=$data['tp']; ?>/admin/js/scripts/ui-alerts.js" type="text/javascript"></script>
                <!-- <script src="<?php //$data['tp']; ?>/js/scripts/data-tables.js" type="text/javascript"></script> -->
                <script>
                    $(document).ready( function () {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $('#laravel_datatable').DataTable({
                            processing: true,
                            serverSide: true,
                            ajax: {
                                url: "variantData",
                                type: 'GET',
                                data: function (d) {
                                    d.group = $('#group').val();
                                }
                            },
                            columns: [
                                { data: 'id', name: 'id' },
                                { data: 'name', name: 'name' },
                                { data: 'price', name: 'price' },
                                { data: 'action', name: 'action' }
                            ]
                        });
                    });

                    $('#btnFiterSubmitSearch').click(function(){
                        $('#laravel_datatable').DataTable().draw(true);
                    });


                    $("#add_option").click(function(){
                        $("#options").append(''
                            +'<div class="form-group col-lg-12">'
                            +'    <div class="input-field col s12 m6 col-lg-11">'
                            +'        <input type="text" name="price[]" class="form-control col-lg-11"  placeholder="Price (Size Difference)">'
                            +'    </div>'
                            +'    <div class="input-field col s12 m1">'
                            +'        <span class="remove mini-button"><i class="icon-close"></i></span>'
                            +'    </div>'
                            +'</div>'
                        );
                    });

                    $('body').on('click', '.remove', function(){
                        $(this).parent().parent().remove();
                    });

                    $('body').on('click', '.remove-option', function(){
                        var co = $(this).closest('.form-group').data('no');
                        $(this).parent().parent().remove();
                        if($(this).parent().parent().parent().html() == ''){
                            $(this).parent().parent().parent().html(''
                                +'   <input type="hidden" name="option_set'+co+'[]" value="none" >'
                            );
                        }
                    });

                    $("#options").on('change','.option_type',function(){
                        var co = $(this).closest('.form-group').data('no');
                        if($(this).val() !== 'text'){
                            $(this).closest('div').find(".options").html('<div class="pull-right button add_option">Add option</div>');
                        } else {
                            $(this).closest('div').find(".options").html('<input type="hidden" name="option_set'+co+'[]" value="none" ><input type="hidden" name="option_set'+co+'[]" value="none" >');
                        }
                    });

                    $("#options").on('click','.add_option',function(){
                        var co = $(this).closest('.form-group').data('no');
                        $(this).closest('.options').prepend(''
                            +'    <div style="margin: 10px -15px;">'
                            +'        <div class="col-sm-5">'
                            +'          <input type="text" name="new_price[]" class="form-control required"  placeholder="Price">'
                            +'        </div>'
                            +'        <div class="col-sm-1">'
                            +'          <span class="remove-option mini-button"><i class="icon-close"></i></span>'
                            +'        </div>'
                            +'        <div class="clearfix"></div>'
                            +'    </div>'
                        );
                    });

                    $("#add_option_edit").click(function(){
                        $("#options").append(''
                            +'<div class="form-group col-lg-12">'
                            +'    <div class="input-field col s12 m6 col-lg-11">'
                            +'        <input type="text" name="new_price[]" class="form-control col-lg-11"  placeholder="Price (Size Difference)">'
                            +'    </div>'
                            +'    <div class="input-field col s12 m1">'
                            +'        <span class="remove mini-button"><i class="icon-close"></i></span>'
                            +'    </div>'
                            +'</div>'
                        );
                    });
                </script>
                <script>
                    $(document).ready(function(){
                        $('#multiple_group').on('change', function(){
                            var id = $(this).val();
                            $.ajax({
                                type: 'post',
                                url: 'get-multiple-data',
                                data: {_token: '<?=csrf_token()?>', id: id},
                                success: function(data){
                                    $('#multipleData').html(data);
                                    $(".add_option").click(function(){
                                        var did = $(this).closest('.add_option').data('key');
//                                        alert(did);
                                        $("#options"+did).append(''
                                            +'<div class="form-group col-lg-12">'
                                            +'    <div class="input-field col s12 m6 col-lg-11">'
                                            +'        <input type="text" name="new_price['+did+'][]" class="form-control col-lg-11"  placeholder="Price (Size Difference)">'
                                            +'    </div>'
                                            +'    <div class="input-field col s12 m1">'
                                            +'        <span class="remove mini-button"><i class="icon-close"></i></span>'
                                            +'    </div>'
                                            +'</div>'
                                        );
                                    });
                                },
                                error: function(){
                                    alert('Oops! Something went wrong. Try again.');
                                }
                            })
                        });
                    });
					<?php
					$sel = '';
					foreach ($data['units'] as $uni){
						$sel .= '<option value="'.$uni->name.'">'.$uni->name.'</option>';
					} ?>
					<?php
					$unitSel = '';
					foreach ($data['units'] as $uni){
						$unitSel .= '<option value="'.$uni->name.'">'.$uni->name.'</option>';
					} ?>
					$("#add_weight_option").click(function(){
                        $("#weight_options").append(''
                            +'<div class="row">'
								+'<div class="col-lg-12">'
									+'<div class="form-group" style="float: left; padding:10px;">'
										+'<label class="control-label" style="font-size: 10px;">Length <input type="checkbox"> </label>'
										+'<input name="length[]" type="text" class="form-control" style="background-color: white; border: 1px solid #000000;"/>'
									+'</div>'
									+'<div class="form-group" style="float: left; padding:10px;">'
										+'<label class="control-label">Length Unit</label>'
										+'<select name="lunit[]" class="form-control">'
											+'<?php echo $sel; ?>'
										+'</select>'
									+'</div>'
									+'<div class="form-group" style="float: left; padding:10px;">'
										+'<label class="control-label" style="font-size: 10px;">Heavy <input type="checkbox" name="w_available" value="available"> </label>'
										+'<input name="weight[]" type="text" class="form-control" style="background-color: white; border: 1px solid #000000;"/>'
									+'</div>'
									+'<div class="form-group" style="float: left; padding:10px;">'
										+'<label class="control-label" style="font-size: 10px;">Light <input type="checkbox" name="l_available" value="available"></label>'
										+'<input name="light[]" type="text" class="form-control" style="background-color: white; border: 1px solid #000000;"/>'
									+'</div>'
									+'<div class="form-group" style="float: left; padding:10px;">'
										+'<label class="control-label" style="font-size: 10px;">S. Light <input type="checkbox" name="s_available" value="available"></label>'
										+'<input name="super_light[]" type="text" class="form-control" style="background-color: white; border: 1px solid #000000;"/>'
									+'</div>'
									+'<div class="form-group" style="float: left; padding:10px;">'
										+'<label class="control-label">Unit</label>'
										+'<select name="wunit[]" class="form-control">'
											+'<?php echo $unitSel; ?>'
										+'</select>'
									+'</div>'
									+'    <div class="input-field" style="float: right;">'
									+'        <span class="remove mini-button"><i class="icon-close"></i></span>'
									+'    </div>'
								+'</div>'
							+'</div>'
                        );
                    });
					
					$("#edit_weight_option").click(function(){
                        $("#edit_weight_options").append(''
                            +'<div class="row">'
								+'<div class="col-lg-12">'
									+'<div class="form-group" style="float: left; padding:10px;">'
										+'<label class="control-label" style="font-size: 10px;">Length <input type="checkbox"> </label>'
										+'<input name="length[]" type="text" class="form-control" style="background-color: white; border: 1px solid #000000;"/>'
									+'</div>'
									+'<div class="form-group" style="float: left; padding:10px;">'
										+'<label class="control-label">Length Unit</label>'
										+'<select name="lunit[]" class="form-control">'
											+'<?php echo $sel; ?>'
										+'</select>'
									+'</div>'
									+'<div class="form-group" style="float: left; padding:10px;">'
										+'<label class="control-label" style="font-size: 10px;">Heavy <input type="checkbox" name="w_available" value="available"> </label>'
										+'<input name="weight[]" type="text" class="form-control" style="background-color: white; border: 1px solid #000000;"/>'
									+'</div>'
									+'<div class="form-group" style="float: left; padding:10px;">'
										+'<label class="control-label" style="font-size: 10px;">Light <input type="checkbox" name="l_available" value="available"></label>'
										+'<input name="light[]" type="text" class="form-control" style="background-color: white; border: 1px solid #000000;"/>'
									+'</div>'
									+'<div class="form-group" style="float: left; padding:10px;">'
										+'<label class="control-label" style="font-size: 10px;">S. Light <input type="checkbox" name="s_available" value="available"></label>'
										+'<input name="super_light[]" type="text" class="form-control" style="background-color: white; border: 1px solid #000000;"/>'
									+'</div>'
									+'<div class="form-group" style="float: left; padding:10px;">'
										+'<label class="control-label">Unit</label>'
										+'<select name="wunit[]" class="form-control">'
											+'<?php echo $unitSel; ?>'
										+'</select>'
									+'</div>'
									+'    <div class="input-field" style="float: right;">'
									+'        <span class="remove mini-button"><i class="icon-close"></i></span>'
									+'    </div>'
								+'</div>'
							+'</div>'
                        );
                    });
                </script>