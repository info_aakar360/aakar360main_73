<?php echo $header?>
<style>

</style>
<section class="at-property-sec at-property-right-sidebar" style="padding: 0;">
    <div class="container padlr" id="<?=$product->id;?>">
        <?php
        $pvalidity = -1;
        if($product->is_variable){
            date_default_timezone_set("Asia/Kolkata");
            $pvalidity = strtotime($product->validity) - strtotime(date('Y-m-d H:i:s'));
            if($pvalidity < 0){
                $pvalidity = 0;
            }
        }
        ?>
        <span class="countdown-timer" data-key="<?=$product->id;?>"><?=$pvalidity;?></span>
        <div class="content product-page pull-left">
            <?php if(Session::has('successx')) {
                $msgs = Session::get('successx');
                foreach ($msgs as $msg) {
                    echo '<div class="alert alert-success auto-close">' . $msg . '</div>';
                }
            }
            if(Session::has('msgx')) {
                $msgs = Session::get('msgx');
                foreach ($msgs as $msg) {
                    echo '<div class="alert alert-danger auto-close">' . $msg . '</div>';
                }
            }
            ?>
            <div class="col-8 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner text-center">
                        <?php for($i=0; $i<count($images); $i++){
                            if($i == 0){?>
                                <div class="item active" style="padding: 2px;">
                                    <img src="<?=url('/assets/products/'.$images[$i])?>"/>
                                </div>
                            <?php }  else {?>
                                <div class="item" style="padding: 2px;">
                                    <img src="<?=url('/assets/products/'.$images[$i])?>"/>
                                </div>
                            <?php }
                        } ?>
                    </div>
                    <?php if(count($images) > 1){ ?>
                        <ul class="pro-image-slider" data-slick='{"slidesToShow": 4, "slidesToScroll": 1}'>
                            <?php for($i=0; $i<count($images); $i++){
                                if($i == 0){?>
                                    <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" class="active">
                                        <a href="#"><img src="<?=url('/assets/products/thumbs/'.$images[$i])?>"/></a>
                                    </li>
                                <?php } else {?>
                                    <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>">
                                        <a href="#"><img src="<?=url('/assets/products/thumbs/'.$images[$i])?>"/></a>
                                    </li>
                                <?php }
                            } ?>
                        </ul>
                    <?php } ?>
                </div>
                <div class="clearfix" style="width: 100%; margin-top: 10px;"></div>
            </div>
            <div class="col-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <a href="{{route('retail.retailProducts',[$cat->path])}}" class=""><?=translate($cat->name)?></a>
                <h3 style="font-family: sans-serif; !important; font-size: 15px;"><?=translate($product->title)?></h3>
                <?php if($product->rating_view != '0'){ ?>
                    <div class="rating">
                        <?php $tr = $rating; $i = 0; while($i<5){ $i++;?>
                            <i class="star<?=($i<=$rating) ? '-selected' : '';?>"></i>
                            <?php $tr--; }?>
                        <b> <?=$total_ratings.' '.translate('Reviews')?> </b>
                    </div>
                <?php }else{ ?>
                    <div class="rating">
                        <i class="star"></i>
                        <i class="star"></i>
                        <i class="star"></i>
                        <i class="star"></i>
                        <i class="star"></i>&nbsp;&nbsp;
                        (<b>0 <?=translate('Reviews');?></b>)
                    </div>
                <?php } ?>
                <div class="clearfix" style="width: 100%; margin-top: 10px;"></div>
                <div class="row">
                    <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <b>You are looking for <?php if(isset($_COOKIE['cities'])) { echo $_COOKIE['cities']; } ?> </b>
                    </div>
                    <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="input-group">
                            <select name="select_city" class="form-control">
                                <option value="">Please select</option>
                                <?php foreach($cities as $city){ ?>
                                    <option value="<?php echo $city->name; ?>"><?php echo $city->name; ?></option>
                                <?php } ?>
                            </select>
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="button" style="padding: 9px 12px;">Save</button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="clearfix" style="width: 100%; margin-top: 10px;"></div>
                <?php if(!count($variants)){ ?>
                    <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="alert alert-info">
                            Buy 5 Tonnes & above. Get special price ! Call Now on 06263956550
                        </div>
                    </div>
                <?php } ?>

                <input type="hidden" id="min-qty" value="<?=$product->min_qty;?>"/>
                <input type="hidden" id="max-qty" value="<?=$product->max_qty;?>"/>
                <div class="clearfix" style="width: 100%; margin-top: 10px;"></div>
                <div class="row">&nbsp;</div>
                <?php if($product->max_qty !== 0){ ?>
                    <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="alert alert-info">
                            Maximum order quantity is <b><?php echo $product->max_qty.' '.getUnitSymbol($product->weight_unit); ?></b> . Want to purchase more than <b><?php echo $product->max_qty.' '.getUnitSymbol($product->weight_unit); ?></b> ? Get special price ! Call Now on 06263956550
                        </div>
                    </div>
                <?php } ?>
                <?php if ($product->quantity > 0) { ?>
                    <div class="order">
                        <?php
                        $all_options = json_decode($product->options,true);
                        if(!empty($all_options)){
                            ?>
                            <form class="options" style="background:rgb(249, 250, 252)">
                                <?php
                                foreach($all_options as $i=>$row){
                                    $type = $row['type'];
                                    $name = $row['name'];
                                    $title = $row['title'];
                                    $option = $row['option']; ?>
                                    <div class="option">
                                        <h6><?php echo $title.' :';?></h6>
                                        <?php if($type == 'radio'){ ?>
                                            <div class="custom_radio">
                                                <?php
                                                $i=1;
                                                foreach ($option as $op) {?>
                                                    <label for="<?php echo 'radio_'.$i; ?>" style="display: block;">
                                                        <input type="radio" name="<?php echo $name;?>" value="<?php echo $op;?>" id="<?php echo 'radio_'.$i; ?>">
                                                        <?php echo $op;?>
                                                    </label>
                                                <?php $i++; } ?>
                                            </div>
                                        <?php } else if($type == 'text'){ ?>
                                            <textarea class="form-control" rows="2" style="width:100%" name="<?php echo $name;?>"></textarea>
                                        <?php } else if($type == 'select'){ ?>
                                            <select name="<?php echo $name; ?>" class="form-control" type="text">
                                                <option value=""><?php echo translate('Choose one'); ?></option>
                                                <?php foreach ($option as $op) { ?>
                                                    <option value="<?php echo $op; ?>" ><?php echo $op; ?></option>
                                                <?php } ?>
                                            </select>
                                        <?php } else if($type == 'multi_select') {
                                            $j=1;
                                            foreach ($option as $op){ ?>
                                                <label for="<?php echo 'check_'.$j; ?>" style="display: block;">
                                                    <input type="checkbox" id="<?php echo 'check_'.$j; ?>" name="<?php echo $name;?>[]" value="<?php echo $op;?>">
                                                    <?php echo $op;?>
                                                </label>
                                            <?php $j++; }
                                        }
                                        ?>
                                    </div>
                                <?php } ?>
                            </form>
                        <?php } ?>
                        <?php if(count($bulk_discounts)){ ?>
                            <div class="table-responsive" >
                                <table class="table table-striped table-bordered">
                                    <thead>
                                    <tr class="bg-blue">
                                        <th colspan="3" class="text-center text-success">Buy More Save More</th>
                                    </tr>
                                    <tr class="bg-blue">
                                        <th class="text-center">Quantity</th>
                                        <th class="text-center">Extra Discount<br>(per unit)</th>
                                        <th class="text-center">You Save</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($bulk_discounts as $bd){ $saving = 0.00; ?>
                                        <tr>
                                            <td>
                                                <?=$bd->quantity; ?> or more
                                            </td>
                                            <td class="text-right">
                                                <?php
                                                if($bd->discount_type == 'Flat'){
                                                    echo c($bd->discount);
                                                    $saving = $bd->quantity*$bd->discount;
                                                }else{
                                                    $dis = number_format(($bd->discount*$prc)/100,2,'.', '');
                                                    echo c(($prc-$dis));
                                                    $saving = $bd->quantity*($prc-$dis);
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?=c($saving); ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php } ?>
                        <?php if(count($variants)){ ?>
                            <div class="alert alert-info">
                                Buy 5 Tonnes & above. Get special price ! Call Now on 06263956550
                            </div>
                        <?php } ?>
                        <?php if(!count($variants)){ ?>
                            <div class="price-box col-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <table class="price-table" style="margin-bottom: 15px;">
                                    <?php if(count($bulk_discounts)){ ?>
                                        <tr>
                                            <td class="text-left">Unit Price</td>
                                            <td class="text-right"><strike style="font-size: 16px;"><?php  echo c($product->sale); ?></strike></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">Unit Sale Price</td>
                                            <td class="text-right"><b style="font-size: 18px;" class="dynamic" id="sale_price"><?php  echo c($product->sale); ?></b></td>
                                        </tr>
                                    <?php }else{ ?>
                                        <tr>
                                            <td class="text-left">Unit Price</td>
                                            <td class="text-right"><b style="font-size: 18px;" class="dynamic" id="sale_price"><?=c($product->price)?></b></td>
                                        </tr>
                                    <?php } ?>
                                </table>
                                {{--<?php if(customer('user_type')) { ?>--}}
                                    {{--<p class="final-price dynamic">--}}
                                        {{--<?php echo c($product->price * ($product->min_qty != 0 ? $product->min_qty : 1)); ?>--}}
                                    {{--</p>--}}
                                    <?php
//                                }
                                if(count($bulk_discounts)){
                                    if($product->sale != 0){ ?>
                                        <p class="final-discount dynamic">You save : <?php echo c(($product->price - $product->sale)*($product->min_qty != 0 ? $product->min_qty : 1)); ?> (<?php echo round((($display_price - $display_sale)/$display_price)*100,2);?>%)</p>
                                    <?php } } ?>
                                <div id="error" style="display: none;"></div>
                                <div class="quantity-select" style="width: 100% ">
                                    <input name="quantity" style="width: 100% " class="quantityvar pridvalue<?php echo $product->id; ?>" data-product="<?php echo $product->id; ?>" value="<?php echo $product->min_qty; ?>" >
                                </div>
                                <button class="add-cart btn btn-primary" style="width: 100% " data-id="<?php echo $product->id?>"><i class="icon-basket"></i> <?php echo translate('Add to cart')?></button>

                            </div>
                            {{--<?php if($product->min_qty > 0){ ?>--}}
                            {{--<div class="alert alert-warning">--}}
                                {{--Minimum Order Quantity is <?=$product->min_qty;?>--}}
                            {{--</div>--}}
                            {{--<?php } ?>--}}
                        <?php } ?>
                    </div>
                <?php } else { ?>
                    <div class="order">
                        <p>Quantity unavailable</p>
                    </div>
                <?php } ?>
                <div class="option" style="margin-top: 15px;">
                    <?php if(customer('id') !== ''){
                        $user_id = customer('id');
                        $product_id = $product->id;
                        $img = \App\ProductWishlist::where('user_id',$user_id)->where('product_id',$product_id)->first();
                        if(!empty($img)){
                            ?>
                            <button class="btn btn-primary likebutton" id="likebutton" style="width: 100%;">
                                <span class="tickgreen">✔</span> Added to Project
                            </button>
                        <?php } else{?>
                            <button class="btn btn-primary likebutton" id="likebutton" onclick="productWishlist(this.value);" style="width: 100%; display: none;">
                                <i class="fa fa-heart"></i> Add to Project
                            </button>
                        <?php }} else { ?>
                        <button class="btn btn-primary" onClick="getModel()" style="width: 100%; display: none;"><i class="fa fa-heart"></i> Add to Project</button>
                    <?php } ?>
                </div>
                <div class="option" style="margin-top: 15px;">
                    <?php if(customer('id') !== ''){
                        $user_id = customer('id');
                        $product_id = $product->id;
                        $img = \App\ProductWishlist::where('user_id',$user_id)->where('product_id',$product_id)->first();
                        if(!empty($img)){
                            ?>
                            <button class="btn btn-primary likebutton" id="likebutton" style="width: 100%;">
                                <span class="tickgreen">✔</span> Added to Project
                            </button>
                        <?php } else{?>
                            <button class="btn btn-primary likebutton" id="likebutton" onclick="productWishlist(this.value);" style="width: 100%; display: none;"><i class="fa fa-heart"></i> Add to Project</button>
                        <?php }} else { ?>
                        <button class="btn btn-primary" onClick="getModel()" style="width: 100%; display: none;"><i class="fa fa-heart"></i> Add to Project</button>
                    <?php } ?>
                        {{--<button style="width: 100%;" class="btn btn-primary pull-right" id="showpoptable" data-id="<?=$product->id?>"><i class="icon-eye"></i> <?=translate('Show Section')?></button>--}}
                        {{--<button style="width: 100%;" class="btn btn-primary pull-right" id="showtable" data-id="<?=$product->id?>"><i class="icon-eye"></i> <?=translate('Show Section')?></button>--}}
                </div>
            </div>

            <?php if(count($variants)){
                ?>

                <div class="">
                    <form id="variants-form">
                    <table class="table table-striped table-bordered table-responsive">
                        <thead>
                        <tr class="bg-blue">
                            <th>Product Name</th>
                            <th>Price</th>
                            <th>Sale Price</th>
                            <th>Unit</th>
                            <th>Qty</th>
                            <th class="text-right">Total Price</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($variants as $key => $vart){
                                $customer_id ='';
                                $price = $product->price + $vart->price;
                                if ($product->sale < $product->price && $product->sale != 0) {
                                    $price = $product->sale + $vart->price;
                                }
                                $vartprice = c($product->price+getSize($vart->price));
                                echo'<tr>
                                    <td>'.variantTitle($vart->variant_title).'
                                        <input type="hidden"  class="prlist" name="prlist[]" value="'.$vart->id.'">
                                    </td>';
                                    echo '<td class="price mainprice'.$vart->id.'">
                                        <strike style="font-size: 14px; font-weight: 700;">'.(customer('rfr') == 0 ? c((int)$product->price+getSize((int)$vart->price)) : 'Rate not available');
                                        echo '</strike>
                                    </td>';
                                    $col = 5;
                                    echo '<td class="price" id="sp-' . $vart->id . '" style="color: green;">
                                        <input type="hidden"  class="saleprice'.$vart->id.'" id="saleprice'.$key.'" value="'.$price.'">
                                        ' . (customer('rfr') == 0 ? c($price) : 'Rate not available') .
                                    '</td>';
                                    $dis = '';
                                    if(customer('rfr')){
                                        $dis = 'disabled';
                                    }
                                    $qt = $product->min_qty != 0 ? $product->min_qty : '1';
                                    echo '<td>
                                        <input type="hidden" class="unit'.$vart->id.'" id="unit'.$key.'" name="units[]" value="'.variantTitleWeight($vart->variant_title).'">
                                        '.variantTitleWeight($vart->variant_title).'
                                    </td>
                                    <td>
                                        <div class=" buttons_added_'.$key.' vaributton">
                                            <div class="form-group baseQuantity but qtycss">
                                                <input name="quantity" class="quantityvar pridvalue'.$vart->id.'" onchange="getTotal(this.value,'.$key.','.$vart->id.');" id="quantityvar'.$vart->id.'" data-product="'.$product->id.'" >
                                                <input type="hidden" name="id" class="quantityvarid inputcss"  value="'.$vart->id.'" >
                                            </div>
                                        </div>
                                        <button type="button" style="display: none" name="add-to-cart" class="single_add_to_cart_button_'.$key.' button alt">Add+</button>   
                                    </td>
                                    <td class="text-right">
                                        <b id="tp" class="tp tp-'.$key.'" data-price="0"><span class="singlepr'.$vart->id.' singleprprice">0.00</span></b>
                                    </td>
                                </tr>';
                            }
                            echo '<tr>
                                <td colspan="'.$col.'" class="text-right">Total Sale Price:</td>
                                <td class="text-right"><b id="gt" class="gt">'.(customer('rfr') == 0 ? c('0.00') : 'Rate not available').'</b></td>
                            </tr>';
                            echo '<tr>
                                <td colspan="'.$col.'" class="text-right">Total Market Price:</td>
                                <td class="text-right"><strike id="mp" class="mp" style="font-size: 14px; font-weight: 700;">' . (customer('rfr') == 0 ? c('0.00') : 'Rate not available') . '</strike></td>
                            </tr>';
                            echo '<tr>
                                <td colspan="'.$col.'" class="text-right">Your Savings:</td>
                                <td class="text-right"><b id="savings" class="savings" style="color: green;">' . (customer('rfr') == 0 ? c('0.00') : 'Rate not available') . '</b></td>
                            </tr>';
                            ?>
                        </tbody>
                    </table>
                    <div class="order">
                        <div class="col-8 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                            <div class="error" style="display:none;"></div>
                        </div>
                        <div class="col-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <?php if(customer('rfr') == 0){ ?>
                                <div id="error" style="display: none;"></div>
                                <button style="width: 100%;" class="add-cart-variant btn btn-primary pull-right" id="added" data-id="<?=$product->id?>"><i class="icon-basket"></i> <?=translate('Add to cart')?><span style="display: none;" class ="addd">Added To Cart</span></button>

                            <?php }else{ ?>
                                <div id="error" style="display: none;"></div>
                                <button style="width: 100%;" class="request-rate btn btn-primary pull-right" data-id="<?=$product->id?>"> <?=translate('Request for rate')?></button>
                            <?php } ?>
                        </div>
                    </div>
                </form>
                </div>
            <?php } ?>
        </div>
        <div class="clearfix"></div>
        <div class="panel-group" id="accordion">
            <?php if($product->description_view != '0') {
                if ($product->text != '') { ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                               style="color: #ffffff;"><h4 class="panel-title">
                                    Description
                                </h4></a>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <p><?= $product->text ?></p>
                            </div>
                        </div>
                    </div>
                <?php }
            }?>
            <?php if($product->specification_view != '0'){
                if($product->specification != '') { ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" style="color: #ffffff;">
                                <h4 class="panel-title">
                                    Specification
                                </h4>
                            </a>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p><?= $product->specification ?></p>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            } ?>
            <?php if($product->rating_view != '0'){ ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" style="color: #ffffff;">
                            <h4 class="panel-title">
                                Ratings & Reviews
                            </h4>
                        </a>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="feedback-container">
                                <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <ul class="rating">
                                            <li class="float-left">
                                                <h5 style="margin-bottom: 0px; font-family: Sans-Serif; font-style: bold;">Avarage rating <?=$avg_rating;?></h5>
                                                <div class="rating" style="padding-bottom: 10px;">
                                                    <?php
                                                    $a=$avg_rating;
                                                    $b = explode('.',$a);
                                                    $first = $b[0];
                                                    $rr = $avg_rating;
                                                    $i = 0;
                                                    while($i<5){ $i++;?>
                                                        <i class="star<?=($i<=$avg_rating) ? '-selected' : '';?>"></i>
                                                        <?php $rr--; }
                                                    echo '</div>';
                                                    ?>
                                                    <h5 style=" font-family: Sans-Serif; font-style: bold;">Total rating <?=$total_user;?></h5>
                                                </div>
                                            </li>
                                        </ul>
                                        <hr>
                                        <ul class="rating">
                                            <li class="float-left">
                                                <h5 style=" font-family: Sans-Serif; font-style: bold;">Total comment <?=$total_reviews; ?></h5>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-8 col-lg-8 col-md-8 col-sm-12 col-xs-12 reviews-graph">
                                        <ul>
                                            <li class="float-left">
                                                <?php if($rating5 != ''){ ?>
                                                    <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 25px;">
                                                        <div class="col-2 col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;">5 <i class="fa fa-star"></i></div>
                                                        <div class="col-8 col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="<?php $a = $rating5->rating5_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>"
                                                                     aria-valuemin="0" aria-valuemax="100" style="width:<?php $a = $rating5->rating5_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>%">
                                                                    <span class="sr-only"><?=$rating5->rating5_user;?> Reviews</span>
                                                                </div>
                                                            </div>
                                                            <span class="mob-rev-count">(<?=$rating5->rating5_user;?> Reviews)</span>
                                                        </div>
                                                        <div class="col-2 col-lg-2 col-md-2 col-sm-2 col-xs-2 rev-count">(<?=$rating5->rating5_user;?> Reviews)</div>
                                                    </div>
                                                <?php }
                                                if($rating4 != ''){ ?>
                                                    <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 25px;">
                                                        <div class="col-2 col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;">4 <i class="fa fa-star"></i></div>
                                                        <div class="col-8 col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="<?php $a = $rating4->rating4_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>"
                                                                     aria-valuemin="0" aria-valuemax="100" style="width:<?php $a = $rating4->rating4_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>%">
                                                                    <span class="sr-only"><?=$rating4->rating4_user;?> Reviews</span>
                                                                </div>
                                                            </div>
                                                            <span class="mob-rev-count">(<?=$rating4->rating4_user;?> Reviews)</span>
                                                        </div>
                                                        <div class="col-2 col-lg-2 col-md-2 col-sm-2 col-xs-2 rev-count">(<?=$rating4->rating4_user;?> Reviews)</div>
                                                    </div>
                                                <?php }
                                                if($rating3 != ''){ ?>
                                                    <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 25px;">
                                                        <div class="col-2 col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;">3 <i class="fa fa-star"></i></div>
                                                        <div class="col-8 col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="<?php $a = $rating3->rating3_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>"
                                                                     aria-valuemin="0" aria-valuemax="100" style="width:<?php $a = $rating3->rating3_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>%">
                                                                    <span class="sr-only"><?=$rating3->rating3_user;?> Reviews</span>
                                                                </div>
                                                            </div>
                                                            <span class="mob-rev-count">(<?=$rating3->rating3_user;?> Reviews)</span>
                                                        </div>
                                                        <div class="col-2 col-lg-2 col-md-2 col-sm-2 col-xs-2 rev-count">(<?=$rating3->rating3_user;?> Reviews)</div>
                                                    </div>
                                                <?php }
                                                if($rating2 != ''){ ?>
                                                    <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 25px;">
                                                        <div class="col-2 col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;">2 <i class="fa fa-star"></i></div>
                                                        <div class="col-8 col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="<?php $a = $rating2->rating2_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>"
                                                                     aria-valuemin="0" aria-valuemax="100" style="width:<?php $a = $rating2->rating2_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>%">
                                                                    <span class="sr-only"><?=$rating2->rating2_user;?> Reviews</span>
                                                                </div>
                                                            </div>
                                                            <span class="mob-rev-count">(<?=$rating2->rating2_user;?> Reviews)</span>
                                                        </div>
                                                        <div class="col-2 col-lg-2 col-md-2 col-sm-2 col-xs-2 rev-count">(<?=$rating2->rating2_user;?> Reviews)</div>
                                                    </div>
                                                <?php }
                                                if($rating1 != ''){ ?>
                                                    <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 25px;">
                                                        <div class="col-12 col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;">1 <i class="fa fa-star"></i></div>
                                                        <div class="col-8 col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="<?php $a = $rating1->rating1_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>"
                                                                     aria-valuemin="0" aria-valuemax="100" style="width:<?php $a = $rating1->rating1_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>%">
                                                                    <span class="sr-only"><?=$rating1->rating1_user;?> Reviews</span>
                                                                </div>
                                                            </div>
                                                            <span class="mob-rev-count">(<?=$rating1->rating1_user;?> Reviews)</span>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 rev-count">(<?=$rating1->rating1_user;?> Reviews)</div>
                                                    </div>
                                                <?php } ?>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-bottom: 10px;">
                                <form action="<?php echo url('review');?>" method="post" class="form-horizontal single">
                                    <?=csrf_field();?>
                                    <div id="response" style="float: left; width: 100%"></div>
                                    <?php if(customer('id') !== ''){?>
                                        <h5 style="float: left; width: 100%; margin-top: 10px; border-top: 1px solid #000000; padding-top: 20px;"><?=translate('Add a review')?> :</h5>
                                        <fieldset style="float: left; width: 100%;">
                                            <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <input name="name" readonly value="<?=customer('id')?>" class="form-control" type="hidden">
                                                <input name="product" value="<?=$product->id?>" class="form-control" type="hidden">
                                                <div class="form-group col-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <label class="control-label"><?=translate('Rating')?></label>
                                                    <div id="star-rating">
                                                        <input type="radio" name="rating" class="rating" value="1" />
                                                        <input type="radio" name="rating" class="rating" value="2" />
                                                        <input type="radio" name="rating" class="rating" value="3" />
                                                        <input type="radio" name="rating" class="rating" value="4" />
                                                        <input type="radio" name="rating" class="rating" value="5" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label"><?=translate('Review')?></label>
                                                    <textarea name="review" type="text" rows="5" class="form-control"></textarea>
                                                </div>
                                                <button data-product="<?=$product->id?>" name="submit" class="btn btn-primary" ><?=translate('submit')?></button>
                                            </div>
                                        </fieldset>
                                    <?php } else { ?>
                                        <div class="row" style="padding: 15px 0; margin-top: -55px;">
                                            <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center my">
                                                <button class="btn btn-primary" onClick="getModel()" type="button" value="Login">Login To Review</button>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </form>
                                <br><br><br>
                                <p>
                                    <?php
                                    $count = 0;
                                    foreach($reviews as $review){
                                        $usid = $review->name;
                                        $uimg = DB::select("SELECT * FROM customers WHERE id = '".$usid."'")[0];
                                        echo '<img class="review-image" src="assets/user_image/'; if($uimg->image != ''){ echo $uimg->image; } else { echo 'user.png'; }; echo'">
                                        <div class="review-meta">
                                            <b>'.$uimg->name.'</b><br/>
                                            <span class="time">'.date('M d, Y',$review->time).'</span><br/>
                                        </div>
                                        <div class="review">
                                            <div class="rating pull-right">';
                                                $rr = $review->rating; $i = 0; while($i<5){ $i++;?>
                                                <i class="star<?=($i<=$review->rating) ? '-selected' : '';?>"></i>
                                                <?php $rr--; }
                                            echo '</div>
                                            <div class="clearfix"></div>
                                            <p>'.nl2br($review->review).'</p>
                                        </div>';
                                        $count++;
                                    }
                                    if($count > 0){
                                        echo '<hr/>';
                                    }
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php if($product->faq_view != '0'){?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" style="color: #ffffff;">
                        <h4 class="panel-title">
                            FAQ
                        </h4>
                    </a>
                </div>
                <div id="collapseFour" class="panel-collapse collapse">
                    <div class="panel-body">
                        <section class="at-property-sec at-property-right-sidebar">
                            <div class="">
                                <div class="row" style="padding: 15px 0; margin-top: -55px;">
                                    <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                                        <?php if(customer('id') !== ''){ ?>
                                            <button class="btn btn-primary" type="button" data-popup-open="popup-question" value="Ask a Question">Ask a Question</button>
                                        <?php } else { ?>
                                            <button class="btn btn-primary" onClick="getModel()" type="button" value="Ask a Question">Ask a Question</button>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <ul>
                                            <?php if(isset($faqs)) foreach($faqs as $faq){ ?>
                                                <li>
                                                    <h5>Q : <?php echo $faq->question; ?><br><small>
                                                            by Admin on <?php $d = $faq->time; echo date('d M, Y', strtotime($d)); ?></small></h5>
                                                    <p>A : <?php echo $faq->answer; ?></p>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Ask a Question Popup Start -->
                        <div class="popup" id="popup-question" data-popup="popup-question">
                            <div class="popup-inner">
                                <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <h2 style="text-transform: none;" class="text-center">Ask a Question</h2>
                                    <form id="contact_form" action="<?php echo url('product-faq');?>" method="post">
                                        <?=csrf_field() ?>
                                        <input type="hidden" name="product_id" value="<?=$product->id;?>" />
                                        <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                                            <textarea class="form-control" name="question" rows="5" placeholder="Write question here" required="required"></textarea>
                                            <br>
                                            <button class="btn btn-primary" type="submit">SUBMIT</button>
                                        </div>
                                    </form>
                                </div>
                                <a class="popup-close" data-popup-close="popup-question" href="#">x</a>
                            </div>
                        </div>
                        <!-- Ask a Question Popup End -->
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    </div>
</section>
<?php if($product->fb_cat !== '' && customer('user_type') !== 'institutional' && $vtype != 'institutional'){
    $fb_cats = explode(',', $product->fb_cat); ?>
    <section>
        <div class="container">
            <div class="box clearfix box-with-products">
                <!-- Carousel nav -->
                <a class="next" href="#myCarousel0" id="myCarousel0_next"><span></span></a>
                <a class="prev" href="#myCarousel0" id="myCarousel0_prev"><span></span></a>
                <div class="box-heading">Frequently Bought Together</div>
                <div class="strip-line"></div>
                <div class="box-content products" style="margin: 20px 0;">
                    <div class="box-product">
                        <div id="myCarousel0" class="owl-carousel">
                            <?php
                            foreach($fb_cats as $fb_cat){
                                $pro = null;
                                $pro = getProduct($fb_cat, $product->id);
                                if($pro !== null){ ?>
                                    <div class="item">
                                        <div class="at-property-item at-col-default-mar" id="<?=$pro->id;?>">
                                            <div class="at-property-img">
                                                <a href="{{route('retail.retailProduct',[path($pro->title,$pro->id)])}}" data-title="<?=translate($pro->title)?>">
                                                    <img src="<?=url('/assets/products/'.image_order($pro->images))?>" style="width: 100%; height: 150px;background-color: #060606;" alt="">
                                                </a>
                                                <div class=""></div>
                                            </div>
                                            <div class="at-property-location">
                                                <div class="text-ellipsis">
                                                    <a href="{{route('retail.retailProduct',[path($pro->title,$pro->id)])}}" data-title="<?=translate($pro->title)?>"><?=translate($pro->title)?></a>
                                                </div>
                                                <span class="price"><?=(customer('user_type') == 'institutional' ? c($pro->institutional_price) : c($pro->price))?></span>
                                                <div class="rating">
                                                    <?php
                                                    $rates = getRatings($pro->id);
                                                    $tr = $rates['rating']; $i = 0; while($i<5){ $i++;?>
                                                        <i class="star<?=($i<=$rates['rating']) ? '-selected' : '';?>"></i>
                                                        <?php $tr--; }?>
                                                    (<?=$rates['total_ratings']?>)
                                                </div>
                                                <div class="cart-btn-custom" style="padding-top: 10px;">
                                                    <button class="bg" data-redirect="{{route('retail.retailProduct',[path($pro->title,$pro->id)])}}" data-title="{{translate($pro->title)}}>"><i class="icon-basket"></i> Add to Cart</button>
                                                    <div style="margin-top: -20px;">&nbsp;</div>
                                                    <?php
                                                    if(customer('id') !== ''){
                                                        $user_id = customer('id');
                                                        $product_id = $product->id;
                                                        $img = DB::select("SELECT * FROM product_wishlist WHERE user_id = '".$user_id."' AND product_id = '".$product_id."'");
                                                        if(!empty($img)){
                                                            echo '<button class="bg" id="likebutton"><span class="tickgreen">✔</span> Added to Project</button>';
                                                        } else{
                                                            echo '<input type="hidden" name="pidd" class="pidd" value="'.$product_id.'">
                                                            <button class="bg likebutton1" id="likebutton"><i class="fa fa-heart"></i> Add to Project</button>';
                                                        }
                                                    } else {
                                                        echo '<button class="bg" onClick="getModel()"><i class="fa fa-heart"></i> Add to Project</button>';
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php }
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } if(!empty($related_products)){
    $customer_id ='';
    if(customer('id')){
        $customer_id = customer('sid');
    }
    $pc = false;
    foreach($related_products as $rp){
        $p = getInstitutionalPriceFancy($rp->id,$customer_id,$hub);
        if($p != "NA"){
            $pc = true;
            break;
        }
    }
    if($pc){
        ?>
        <section>
            <div class="container">
                <div class="box clearfix box-with-products">
                    <!-- Carousel nav -->
                    <a class="next" href="#myCarousel" id="myCarousel_next"><span></span></a>
                    <a class="prev" href="#myCarousel" id="myCarousel_prev"><span></span></a>
                    <div class="box-heading">Related products</div>
                    <div class="strip-line"></div>
                    <div class="box-content products" style="margin: 20px 0;">
                        <div class="box-product">
                            <div id="myCarousel" class="owl-carousel">
                                <?php
                                foreach($related_products as $rel){
                                    $customer_id ='';
                                    if(customer('id')){
                                        $customer_id = customer('sid');
                                    }
                                    $pr = getInstitutionalPriceFancy($rel->id,$customer_id,$hub);
                                    if($pr != "NA"){
                                        ?>
                                        <div class="item">
                                            <div class="at-property-item at-col-default-mar" id="<?=$rel->id;?>">
                                                <div class="at-property-img">
                                                    <a href="product/<?=path($rel->title,$rel->id)?>" data-title="<?=translate($rel->title)?>">
                                                        <img src="<?=url('/assets/products/'.image_order($rel->images))?>" style="width: 100%; height: 150px;background-color: #060606;" alt="">
                                                    </a>
                                                    <div class=""></div>
                                                </div>
                                                <div class="at-property-location">
                                                    <div class="text-ellipsis">
                                                        <a href="product/<?=path($rel->title,$rel->id)?>" data-title="<?=translate($rel->title)?>"><?=translate($rel->title)?></a>
                                                    </div>
                                                    <span class="price"><?=(customer('user_type') == 'institutional' ? c($rel->institutional_price) : c($rel->price))?></span>
                                                    <div class="rating">
                                                        <?php
                                                        $rates = getRatings($rel->id);
                                                        $tr = $rates['rating']; $i = 0; while($i<5){ $i++;?>
                                                            <i class="star<?=($i<=$rates['rating']) ? '-selected' : '';?>"></i>
                                                            <?php $tr--; }?>
                                                        (<?=$rates['total_ratings']?>)
                                                    </div>
                                                    <div class="cart-btn-custom" style="padding-top: 10px;">
                                                        <button class="bg" data-redirect="{{route('retail.retailProduct',[path($rel->title, $rel->id)])}}" data-title="<?=translate($rel->title) ?>"><i class="icon-basket"></i> Add to Cart</button>
                                                        <div style="margin-top: -20px;">&nbsp;</div>
                                                        <?php
                                                        if(customer('id') !== ''){
                                                            $user_id = customer('id');
                                                            $product_id = $product->id;
                                                            $img = DB::select("SELECT * FROM product_wishlist WHERE user_id = '".$user_id."' AND product_id = '".$product_id."'");
                                                            if(!empty($img)){
                                                                echo '<button class="bg" id="likebutton"><span class="tickgreen">✔</span> Added to Project</button>';
                                                            } else{
                                                                echo '<input type="hidden" name="pidd" class="pidd" value="'.$product_id.'">
                                                            <button class="bg likebutton2" id="likebutton"><i class="fa fa-heart"></i> Add to Project</button>';
                                                            }
                                                        } else {
                                                            echo '<button class="bg" onClick="getModel()"><i class="fa fa-heart"></i> Add to Project</button>';
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php }
                                } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php } }
if($recent_viewed){
    ?>
    <section>
        <div class="container">
            <div class="box clearfix box-with-products">
                <!-- Carousel nav -->
                <a class="next" href="#myCarousel1" id="myCarousel1_next"><span></span></a>
                <a class="prev" href="#myCarousel1" id="myCarousel1_prev"><span></span></a>
                <div class="box-heading">Recently Viewed</div>
                <div class="strip-line"></div>
                <div class="box-content products" style="margin: 20px 0;">
                    <div class="box-product">
                        <div id="myCarousel1" class="owl-carousel">
                            <?php foreach($recent_viewed as $rel){
                                $customer_id ='';
                                if(customer('id')){
                                    $customer_id = customer('sid');
                                }
                                $prv = getInstitutionalPriceFancy($rel->id,$customer_id,$hub); ?>
                                <div class="item">
                                    <div class="at-property-item at-col-default-mar" id="<?=$rel->id;?>">
                                        <div class="at-property-img">
                                            <a href="{{route('retail.retailProduct',[path($rel->title,$rel->id)])}}" data-title="<?=translate($rel->title)?>">
                                                <img src="<?=url('/assets/products/'.image_order($rel->images))?>" style="width: 100%; height: 150px;background-color: #060606;" alt="">
                                            </a>
                                            <div class=""></div>
                                        </div>
                                        <div class="at-property-location">
                                            <div class="text-ellipsis">
                                                <a href="{{route('retail.retailProduct',[path($rel->title,$rel->id)])}}" data-title="<?=translate($rel->title)?>"><?=translate($rel->title)?></a>
                                            </div>
                                            <span class="price">
                                                <?=(customer('user_type') == 'institutional' ? c($prv) : c($rel->price))?>
                                            </span>
                                            <div class="rating">
                                                <?php $rates = getRatings($rel->id);
                                                $tr = $rates['rating']; $i = 0; while($i<5){ $i++;?>
                                                    <i class="star<?=($i<=$rates['rating']) ? '-selected' : '';?>"></i>
                                                    <?php $tr--; }?>
                                                (<?=$rates['total_ratings']?>)
                                            </div>
                                            <div class="cart-btn-custom" style="padding-top: 10px;">
                                                <button class="bg" data-redirect="{{route('retail.retailProduct',[path($rel->title, $rel->id)])}}" data-title="<?=translate($rel->title) ?>"><i class="icon-basket"></i> Add to Cart</button>
                                                <div style="margin-top: -20px;">&nbsp;</div>
                                                <?php if(customer('id') !== ''){
                                                    $user_id = customer('id');
                                                    $product_id = $product->id;
                                                    $img = DB::select("SELECT * FROM product_wishlist WHERE user_id = '".$user_id."' AND product_id = '".$product_id."'");
                                                    if(!empty($img)){
                                                        echo '<button class="bg" id="likebutton"><span class="tickgreen">✔</span> Added to Project</button>';
                                                    } else{
                                                        echo '<input type="hidden" name="pidd" class="pidd" value="'.$product_id.'">
                                                        <button class="bg likebutton3" id="likebutton" ><i class="fa fa-heart"></i> Add to Project</button>';
                                                    }
                                                } else {
                                                    echo '<button class="bg" onClick="getModel()"><i class="fa fa-heart"></i> Add to Project</button>';
                                                } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } ?>
<?php if(!empty($link)){?>
    <section>
        <div class="container">
            <div class="row">
                <div class="blockquote blockquote--style-1">
                    <div class="row inner-div">
                        <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-3 col-lg-3 col-sm-3 col-xs-4">
                                <img src="<?=url('/assets/products/'.image_order($link->image))?>" class="cont-image"/>
                            </div>
                            <div class="col-7 col-md-7 col-lg-7 col-sm-7 col-xs-8" style="padding: 25px 0 0;">
                                <h3 class="cont-content"><?=$link->content; ?></h3>
                            </div>
                            <div class="col-2 col-md-2 col-lg-2 col-sm-2 col-xs-12 text-center" style="padding:15px 0 0 0">
                                <a class="btn btn-primary" href="<?=$link->link; ?>" type="submit" style="padding: 10px auto !important;">
                                    GET STARTED
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } ?>
<button class="btn btn-primary hidden" id="open-service-modal1" data-popup-open="popup-login">Get Started !</button>
<div class="popup" id="popup-login" data-popup="popup-login">
    <div class="popup-inner" style="padding-top: 120px;">
        <div class="col-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 account" style="border: none; box-shadow: none;">
            <?php
            if (session('error') != ''){
                echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
            } ?>
            <form action="<?php echo url('login-model'); ?>" method="post" class="form-horizontal single">
                <?=csrf_field() ?>
                <fieldset>
                    <div class="form-group">
                        <label class="control-label"><?=translate('E-mail') ?></label>
                        <input name="email" type="email" value="<?=isset($_POST['email']) ? $_POST['email'] : '' ?>" class="form-control"  />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?=translate('Password') ?></label>
                        <input name="password" type="password" class="form-control"  />
                    </div>
                    <input name="login" type="submit" value="<?=translate('Login') ?>" class="btn btn-primary" />
                </fieldset>
            </form>
            <div class="text-center"><a class="smooth" href="{{route('retail.retailResetPassword')}}">Forgot your password ?</a></div>
        </div>
        <a class="popup-close" data-popup-close="popup-login" href="#">x</a>
    </div>
</div>
<?php foreach ($variants as $key => $value) { ?>
    <script type="text/javascript">
        $(function() {
            $('[data-popup-open]').on('click', function(e) {
                var targeted_popup_class = jQuery(this).attr('data-popup-open');
                $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
                e.preventDefault();
            });
            $('[data-popup-close]').on('click', function(e) {
                var targeted_popup_class = jQuery(this).attr('data-popup-close');
                $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
                e.preventDefault();
            });
        });
        $(document).ready(function ()
        {
            $(document).on("click",'.minus-<?= $key ?>',function ()
            {
                var qtyval = $('.qty-<?= $key ?>').val();
                if(qtyval >= 1)
                {
                    var newQTY = parseInt(qtyval) - parseInt(1);
                    if(newQTY > 1)
                    {
                        $('.single_add_to_cart_button_<?= $key ?>').hide();
                        $('.buttons_added_-<?= $key ?>').show();
                    }else if(newQTY == 0)
                    {
                        $('.single_add_to_cart_button_<?= $key ?>').show();
                        $('.buttons_added_-<?= $key ?>').hide();
                    }
                    $('.qty-<?= $key ?>').val(newQTY);
                }
            });
            $(document).on("click",'.plus-<?= $key ?>',function ()
            {
                var qtyval = $('.qty-<?= $key ?>').val();
                var newQTY = parseInt(qtyval) + parseInt(1);
                if(newQTY >= 1)
                {
                    $('.single_add_to_cart_button_<?= $key ?>').hide();
                    $('.buttons_added-<?= $key ?>').show();
                }
                else if(newQTY == 0)
                {
                    $('.single_add_to_cart_button_<?= $key ?>').show();
                    $('.buttons_added-<?= $key ?>').hide();
                }
                $('.qty-<?= $key ?>').val(newQTY);
            });

            $(document).on("click",'.single_add_to_cart_button_<?= $key ?>',function ()
            {
                var newQTY = 1;
                if(newQTY >= 1)
                {
                    $('.single_add_to_cart_button_<?= $key ?>').hide();
                    $('.buttons_added_<?= $key ?>').show();
                }
                else if(newQTY == 0)
                {
                    $('.single_add_to_cart_button_<?= $key ?>').show();
                    $('.buttons_added_<?= $key ?>').hide();
                }
                $('.qty-<?= $key ?>').val(newQTY);
            });
        });

    </script>
<?php } ?>
<button class="btn btn-primary hidden" id="open-project-modal1" data-popup-open="popup-project">Get Started !</button>
<div class="popup" id="popup-project" data-popup="popup-project">
    <div class="popup-inner" style="padding-top: 120px;">
        <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 account" style="border: none; box-shadow: none;">
            <?=csrf_field() ?>
            <label class="control-label">Select Your Project</label>
            <button type="button" name="create" class="btn btn-primary" onClick="getcreate()" style="float: right;"><i class="fa fa-plus"></i> Create a new project </button>
            <div id="projectName"></div>
        </div>
        <a class="popup-close" id="pclose" data-popup-close="popup-project" href="#">x</a>
    </div>
</div>
<a class="btn btn-primary hidden" id="project-modal2" data-popup-open="popup-project2" href="#">Get Started !</a>
<div class="popup" id="popup-project2" data-popup="popup-project2">
    <div class="popup-inner" style="padding-top: 120px;">
        <div class="col-10 col-lg-10 col-md-10 col-sm-10 col-xs-10 account" style="border: none; box-shadow: none;">
            <?php
            if (session('error') != ''){
                echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
            } ?>
            <form action="<?php echo url('create-project'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal single">
                <table class="responsive-table bordered" style="width: 50%; margin: auto;">
                    <tbody>
                    <input type="hidden" name="user_id" value="<?=customer('id'); ?>">
                    <tr>
                        <td>Project Title</td>
                        <td>:</td>
                        <td><input type="text" name="title" required class="form-control"></td>
                    </tr>
                    <tr>
                        <td>Project Image</td>
                        <td>:</td>
                        <td><input type="file" name="image" required class="form-control"></td>
                    </tr>
                    <?php echo csrf_field(); ?>
                    <tr>
                        <td colspan="3"><input type="submit" name="submit" value="Update" class="btn btn-primary btn-lg"></td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <a class="popup-close" data-popup-close="popup-project2" href="#">x</a>
    </div>
</div>
<?php echo $footer?>
<script>
    $(document).ready(function(){
        $('form input').keydown(function (e) {
            if (e.keyCode == 13) {
                var inputs = $(this).parents("form").eq(0).find(":input");
                if (inputs[inputs.index(this) + 1] != null) {
                    inputs[inputs.index(this) + 1].focus();
                }
                e.preventDefault();
                return false;
            }
        });
        $('#bookBy').on('change', function(){
            var data = $(this).val();
            if(data == 'product'){
                $('.variantDiv').hide();
                $('.productDiv').show();
            }else{
                $('.variantDiv').show();
                $('.productDiv').hide();
            }
        });
        $('.byBase').on('click', function(){
            $(this).toggleClass('active');
            $('.byVar').toggleClass('active');
            $('.variantDiv').hide();
            $('.productDiv').show();
        });
        $('.byBaseModal').on('click', function(){
            $(this).toggleClass('active');
            $('.byVarModal').toggleClass('active');
            $('#baseForm').show();
            $('#sizeForm').hide();
        });
        $('.byVarModal').on('click', function(){
            $(this).toggleClass('active');
            $('.byBaseModal').toggleClass('active');
            $('#sizeForm').show();
            $('#baseForm').hide();
        });

        $('#request_quotation').on('click', function(){
            $('#asrModal').modal('show');
        });

        $('.byVar').on('click', function(){
            $(this).toggleClass('active');
            $('.byBase').toggleClass('active');
            $('.variantDiv').show();
            $('.productDiv').hide();
            var $container = $("html,body");
            var $scrollTo = $('.variantDiv');
            $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top  - 120, scrollLeft: 0},300);
        });
        // Function to update counters on all elements with class counter
        <?php if(customer('user_type') != ''){ ?>
        var doUpdate = function() {
            $('.countdown-timer').each(function() {
                var count = parseInt($(this).html());
                var pid = $(this).data('key');
                if (count > 0) {
                    $(this).html(count - 1);
                }else if(count == 0){
                    $('#'+pid+' .price').html('Waiting for update');
                    $('#'+pid+' .tooltipx').remove();
                    $('#'+pid+' .tp').html('Waiting for update');
                }
            });
        };
        // Schedule the update to happen once every second
        setInterval(doUpdate, 1000);
        <?php } ?>
    });
</script>
<script>
    (function() {
        window.inputNumber = function(el) {
            var min = el.attr('min') || false;
            var max = el.attr('max') || false;
            var els = {};
            els.dec = el.prev();
            els.inc = el.next();
            el.each(function() {
                init($(this));
            });
            function init(el) {
                els.dec.on('click', decrement);
                els.inc.on('click', increment);
                function decrement() {
                    var value = el[0].value;
                    value--;
                    if(!min || value >= min) {
                        el[0].value = value;
                    }
                }
                function increment() {
                    var value = el[0].value;
                    value++;
                    if(!max || value <= max) {
                        el[0].value = value++;
                    }
                }
            }
        }
    })();
    inputNumber($('.input-number-old'));

    function decrement(prid) {
        var el = $(".pridvalue"+prid);
        var saleprice = $(".saleprice"+prid).val();
        var totpr = 0;
        var value = el.val();
        var min = el.attr('min') || false;
        var max = el.attr('max') || false;
        value--;
        if(!min || value >= min) {
            el.val(value);
            totpr = saleprice*value;
            $(".singlepr"+prid).text("0.00");
            $(".singlepr"+prid).text(totpr);
            caltotal();
        }
    }

    function increment(prid) {
        var el = $(".pridvalue"+prid);
        var saleprice = $(".saleprice"+prid).val();
        var totpr = 0;
        var min = el.attr('min') || false;
        var max = el.attr('max') || false;
        var value = el.val();
        value++;
        if(!max || value <= max) {
            el.val(value);
            totpr = saleprice*value;
            $(".singlepr"+prid).text("0.00");
            $(".singlepr"+prid).text(totpr);
            caltotal();
        }
    }

    function getTotal(val,index,varid){
        var qty = Number(val);
        var saleprice = Number($('#saleprice'+index).val());
        var hdata = '<input type="hidden" class="tpv" value="'+qty*saleprice+'"><span class="singlepr'+varid+' singleprprice">'+qty*saleprice+'</span>';
        $(".tp-"+index).html(hdata);
        var variants = 0.00;
        var mtotal = 0.00;
        $(".tpv").each(function () {
            variants += Number($(this).val());
        });
        $('.gt').html(variants);
        caltotal();
    }

    function  caltotal() {
        var variants = 0.00;
        $(".singleprprice").each(function () {
            variants += parseInt($(this).text());
        });
        $('.gt').html("0.00");
        $('.gt').html(variants);
        var mainvariants = 0.00;
        $(".prlist").each(function () {
            var prid = $(this).val();
            var el = $(".pridvalue"+prid).val();
            var singpr = $(".mainprice"+prid).text();
            mainvariants += parseInt(singpr*el);
        });
        $('.mp').html("0.00");
        $('.mp').html(mainvariants);
        alert(mainvariants);
        $('.savings').html(mainvariants-variants);
    }

    $('#showtable').on('click', function(){
        var x = document.getElementById("variantDiv");
        var y = document.getElementById("price-box");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
        if (y.style.display === "none") {
            y.style.display = "block";
        } else {
            y.style.display = "none";
        }
    });

    $('#showpoptable').on('click', function(){
//            $(this).toggleClass('active');
//            $('.byVar').toggleClass('active');
//            $('.variantDiv').hide();
//            $('.productDiv').show();
        $('#vartable').modal('show');
    });
</script>
<div class="modal fade" id="modalContactForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog cascading-modal" role="document">
        <!--Content-->
        <div class="modal-content">
            <!--Modal cascading tabs-->
            <div class="modal-c-tabs">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs md-tabs tabs-2 light-blue darken-3" role="tablist">
                    <li class="nav-item">
                        <a style="color: blue;" class="nav-link active" data-toggle="tab" href="#panel7" role="tab">
                            <i class="fas fa-user mr-1"></i>
                            By Basic
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#panel8" role="tab">
                            <i class="fas fa-user-plus mr-1"></i>
                            By Size
                        </a>
                    </li>
                </ul>
                <!-- Tab panels -->
                <div class="tab-content">
                    <!--Panel 7-->
                    <div class="tab-pane fade in show active" id="panel7" role="tabpanel">
                        <!--Body-->
                        <div class="modal-body mb-1">
                            <a href="{{route('retail.retailProducts',[$cat->path])}}" class=""><?=translate($cat->name)?></a>
                            <h3 style="font-family: sans-serif; !important; font-size: 20px;"><?=translate($product->title)?></h3>
                            <div class="quantity-select productDiv" style="margin-top: 10px">
                                <div class="dec rease">-</div>
                                <input name="quantity" class="quantity productQty" data-product="<?=$product->id; ?>" value="<?=$product->min_qty != 0 ? $product->min_qty : '1'; ?>" >
                                <div class="inc rease">+</div>
                            </div>
                            <select name="unit" class="form-control productDiv" style="float: right;max-width: 50%; margin-top: 5px;">
                                <option value="MT">MT</option>
                            </select>
                        </div>
                        <div class="col s12 m6 xl3">
                            <div class="col s1 m1"style="text-align: center;margin-top: 30px;"> <i class="material-icons prefix">person</i></div>
                            <div class="col s10 m10">
                                <label>Select Account Manager</label>
                                <select class="browser-default acc_manager" id="acc_manager" name="acc_manager" tabindex="-1">
                                    <option value="all">All</option>
                                </select>
                            </div>
                        </div>
                        <div class="text-center mt-2">
                            <button class="btn btn-info">Log in <i class="fas fa-sign-in ml-1"></i></button>
                        </div>
                        <!--Footer-->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-info waves-effect ml-auto" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!--/.Panel 7-->
                    <!--Panel 8-->
                    <div class="tab-pane fade" id="panel8" role="tabpanel">
                        <!--Body-->
                        <div class="modal-body">
                            <div class="md-form form-sm mb-5">
                                <i class="fas fa-envelope prefix"></i>
                                <input type="email" id="modalLRInput12" class="form-control form-control-sm validate">
                                <label data-error="wrong" data-success="right" for="modalLRInput12">Your email</label>
                            </div>
                            <div class="md-form form-sm mb-5">
                                <i class="fas fa-lock prefix"></i>
                                <input type="password" id="modalLRInput13" class="form-control form-control-sm validate">
                                <label data-error="wrong" data-success="right" for="modalLRInput13">Your password</label>
                            </div>
                            <div class="md-form form-sm mb-4">
                                <i class="fas fa-lock prefix"></i>
                                <input type="password" id="modalLRInput14" class="form-control form-control-sm validate">
                                <label data-error="wrong" data-success="right" for="modalLRInput14">Repeat password</label>
                            </div>
                            <div class="text-center form-sm mt-2">
                                <button class="btn btn-info">Sign up <i class="fas fa-sign-in ml-1"></i></button>
                            </div>
                        </div>
                        <!--Footer-->
                        <div class="modal-footer">
                            <div class="options text-right">
                                <p class="pt-1">Already have an account? <a href="#" class="blue-text">Log In</a></p>
                            </div>
                            <button type="button" class="btn btn-outline-info waves-effect ml-auto" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!--/.Panel 8-->
                </div>

            </div>
        </div>
        <!--/.Content-->
    </div>
</div>

<!-- Services Popup End -->
<div id="vartable" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                <h4 class="modal-title">Variants</h4>
            </div>
            <div id="rfq-error" style="display: none;"></div>
            <div class="modal-body">
                <form id="variants-form">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr class="bg-blue">
                            <th>Product Name</th>
                            <th>Price</th>
                            <th>Sale Price</th>
                            <th>Unit</th>
                            <th>Qty</th>
                            <th class="text-right">Total Price</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($variants as $key => $vart){
                                $customer_id ='';
                                $price = $product->price + $vart->price;
                                if ($product->sale < $product->price && $product->sale != 0) {
                                    $price = $product->sale + $vart->price;
                                }
                                $vartprice = c($product->price+getSize($vart->price));
                                echo'<tr>
                                    <td>'.variantTitle($vart->variant_title).'
                                        <input type="hidden"  class="prlist" name="prlist[]" value="'.$vart->id.'">
                                    </td>';
                                    echo '<td class="price mainprice'.$vart->id.'">
                                        <strike style="font-size: 14px; font-weight: 700;">'.(customer('rfr') == 0 ? c((int)$product->price+getSize((int)$vart->price)) : 'Rate not available');
                                        echo '</strike>
                                    </td>';
                                    $col = 5;
                                    echo '<td class="price" id="sp-' . $vart->id . '" style="color: green;">
                                        <input type="hidden"  class="saleprice'.$vart->id.'" id="saleprice'.$key.'" value="'.$price.'">
                                        ' . (customer('rfr') == 0 ? c($price) : 'Rate not available') .
                                    '</td>';
                                    $dis = '';
                                    if(customer('rfr')){
                                        $dis = 'disabled';
                                    }
                                    $qt = $product->min_qty != 0 ? $product->min_qty : '1';
                                    echo '<td>
                                        <input type="hidden" class="unit'.$vart->id.'" id="unit'.$key.'" name="units[]" value="'.variantTitleWeight($vart->variant_title).'">
                                        '.variantTitleWeight($vart->variant_title).'
                                    </td>
                                    <td>
                                        <div class=" buttons_added_'.$key.' vaributton">
                                            <div class="form-group baseQuantity but qtycss">
                                                <input name="quantity" class="quantityvar pridvalue'.$vart->id.'" onchange="getTotal(this.value,'.$key.','.$vart->id.');" id="quantityvar'.$vart->id.'" data-product="'.$product->id.'" >
                                                <input type="hidden" name="id" class="quantityvarid inputcss"  value="'.$vart->id.'" >
                                            </div>
                                        </div>
                                        <button type="button" style="display: none" name="add-to-cart" class="single_add_to_cart_button_'.$key.' button alt">Add+</button>
                                    </td>
                                    <td class="text-right">
                                        <b id="tp" class="tp tp-'.$key.'" data-price="0"><span class="singlepr'.$vart->id.' singleprprice">0.00</span></b>
                                    </td>
                                </tr>';
                            }
                            echo '<tr>
                                <td colspan="5" class="text-right">Total Sale Price:</td>
                                <td class="text-right"><b id="gt" class="gt">'.(customer('rfr') == 0 ? c('0.00') : 'Rate not available').'</b></td>
                            </tr>';
                            echo '<tr>
                                <td colspan="5" class="text-right">Total Market Price:</td>
                                <td class="text-right"><strike id="mp" class="mp" style="font-size: 14px; font-weight: 700;">' . (customer('rfr') == 0 ? c('0.00') : 'Rate not available') . '</strike></td>
                            </tr>';
                            echo '<tr>
                                <td colspan="5" class="text-right">Your Savings:</td>
                                <td class="text-right"><b id="savings" class="savings" style="color: green;">' . (customer('rfr') == 0 ? c('0.00') : 'Rate not available') . '</b></td>
                            </tr>'; ?>
                        </tbody>
                    </table>
                    <div class="order">
                        <div class="col-8 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                            <div class="error" style="display:none;"></div>
                        </div>
                        <div class="col-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <?php if(customer('rfr') == 0){ ?>
                                <div id="error" style="display: none;"></div>
                                <button style="width: 100%;" class="add-cart-variant btn btn-primary pull-right" id="added" data-id="<?=$product->id?>"><i class="icon-basket"></i> <?=translate('Add to cart')?><span style="display: none;" class ="addd">Added To Cart</span></button>
                            <?php }else{ ?>
                                <div id="error" style="display: none;"></div>
                                <button style="width: 100%;" class="request-rate btn btn-primary pull-right" data-id="<?=$product->id?>"> <?=translate('Request for rate')?></button>
                            <?php } ?>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>