<?php
$location=array();
foreach ($lat as $lat1)
{
    $latitude='';
    $longitude='';
    if($lat1->lat!='' AND $lat1->lng!='')
    {

       $latitude = $lat1->lat;
       $longitude = $lat1->lng;
        $location[] .= "{lat : ".$latitude.", lng : ".$longitude."}";

    }

//    else
    elseif($lat1->lat=='' && $lat1->lng=='' && $lat1->latitude!='' && $lat1->longitude!='')
    {
        $latitude = $lat1->latitude;
        $longitude = $lat1->longitude;
        $location[] .= "{lat : ".$latitude.", lng : ".$longitude."}";
    }

}


?>
<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="ThemeSelect">
    <title><?=$title; ?></title>
    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?=$tp; ?>/vendors/noUiSlider/nouislider.min.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/select.dataTables.min.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">

    <!--<link rel="stylesheet" type="text/css" href="<? //$tp; ?>/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!-- [if lt IE 9]>
    <!--<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>-->
<!--    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>-->
    <![endif] -->




    <!-- END: Custom CSS-->
</head>
<!-- END: Head-->
<?=$header;?>

<form method="post" action="visit-map" class="col s12 m12" autocomplete="off" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
<div class="row">
    <div class="col s12 m6 xl3">
        <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">format_list_bulleted</i></div>
        <div class="col s10 m10">
            <label>Select Category</label>
            <select class="browser-default category" id="category" name="category[]" multiple  tabindex="-1">

                <?php
                $u_id =  $selected_category;
//                $uid_array = explode(',',$u_id);
                foreach ($customer_category as $cate) {

                    $c_id = $cate->id;
                    ?>
                    <option <?php if (in_array($c_id, $selected_category)) { echo 'selected'; } else { echo ''; } ?> value="<?= $c_id; ?>"><?= $cate->type ?></option>
                    <?php
                }
                ?>
            </select>
        </div>
    </div>
    <div class="col s12 m6 xl3">
        <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">flag</i></div>
        <div class="col s10 m10">
            <label>Select State</label>
            <select class="browser-default states" id="states" name="states[]" multiple  tabindex="-1">
                <?php
                foreach ($states as $state) {
                    $v_id = $state->id;
                    ?>
                    <option <?php if (in_array($v_id, $selected_state)) { echo 'selected'; } else { echo ''; } ?> value="<?=$v_id; ?>"><?= $state->name ?></option>
                    <?php
                }
                ?>
            </select>
        </div>
    </div>
    <div class="col s12 m6 xl3"> <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">adjust</i></div>
        <div class="col s10 m10">
            <label>Select District</label>
            <select class="browser-default district" id="district" name="district[]" multiple  tabindex="-1">

            </select>
        </div>
    </div>

    <div class="col s12 m6 xl3">
        <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">format_list_bulleted</i></div>
        <div class="col s10 m10">
            <label>Select Type</label>
            <select class="browser-default type" id="type" name="type[]" multiple  tabindex="-1">

                <?php
                foreach ($customer_type as $type){
                    $s_id = $type->id;
                    ?>
                    <option <?php if (in_array($s_id, $selected_type)) { echo 'selected'; } else { echo ''; } ?> value="<?=$s_id;?>"><?=$type->type?></option>
                    <?php
                }
                ?>
            </select>
        </div>
    </div>

    <div class="col s12 m6 xl3">
        <div class="col s1 m1"style="text-align: center;margin-top: 30px;"> <i class="material-icons prefix">person</i></div>
        <div class="col s10 m10">
            <label>Select Account Manager</label>
            <select class="browser-default acc_manager" id="acc_manager" name="acc_manager" tabindex="-1">
                <option value="all">All</option>
                <?php
                foreach($users as $user){
                    $us_id = $user->u_id;
                ?>
                   <option <?php if ($us_id == $selected_user) { echo 'selected'; } else { echo ''; } ?> value="<?=$us_id;?>"><?=$user->u_name?></option>
                    <?php
                }
                ?>
            </select>
        </div>
    </div>



    <div class="col s12 m12 " style="text-align:center;" >
        <button class="btn myblue waves-light add_submit" type="submit"  name="search">
            <i class="material-icons right" style="margin-left:3px">search</i>Search
        </button>

    </div>

</div>

</form>


<div class="row">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;padding-bottom: 0">
            <div class="card-content" style="min-height: 500px;">
                <?=$notices;?>


                <div id="map" style="min-height: 500px; height: 80vh;"></div>

            </div>
        </div>
    </div>

</div>

<!-- END: Footer-->
<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.js"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/select2.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/ui-alerts.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<!-- <script src="<?php //$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script> -->
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAB5amm0dLJi65cbZSQSGM3dZn4ctnO22Q&callback=initMap">
</script>
<script>
    function initMap()
    {

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 8,
            center:   {lat: 21.6434468, lng: 80.2424126}
        });

        // Create an array of alphabetical characters used to label the markers.
//        var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        // Add some markers to the map.
        // Note: The code uses the JavaScript Array.prototype.map() method to
        // create an array of markers based on a given "locations" array.
        // The map() method here has nothing to do with the Google Maps API.
        var markers = locations.map(function(location, i) {
            return new google.maps.Marker({
                position: location,

            });
        });

        // Add a marker clusterer to manage the markers.
        var markerCluster = new MarkerClusterer(map, markers,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
    }
    var locations = [<?=implode(',', $location);?>]
</script>
<script>
    $(function() {
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?=url("crm/get-locality") ?>",
                type: 'GET',
                data: function (d) {

                }
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'district_name', name: 'district.name' },
                { data: 'block', name: 'block_id' },
                { data: 'locality', name: 'locality' },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ],
            order: [[0, 'desc']],
            dom: 'lBfrtip',
            buttons: <?=$buttons;?>,
            fixedColumns: true,
            colReorder: true,
            exportOptions:{
                columns: ':visible'
            }
        });
    });


    $('#filterButton').click(function(){
        getMapData(map);

    });

    function getMapData(map){
        var datax = false;
        var category = $('#category').val();
        var state = $('#states').val();
        var district = $('#district').val();
        var customer_class = $('#customer_class').val();
        var type = $('#type').val();

        $.ajax({
            url: 'get-visits-map',
            type: 'POST',
            data: '_token=<?=csrf_token()?>&category='+category+'&state='+state+'&district='+district+'&customer_class='+customer_class+'&type='+type,
            success: function(data){
                datax = JSON.parse(data);
                cdata = map.config.data;
                cdata.datasets[0].data = datax.cdata;
                cdata.labels = datax.labels;
                map.update();
            }
        });
    }
</script>
<script>
    $(document).ready(function(){
//        initMap();
        $('#table').attr('style', 'width: 100%');
        $('.user').select2();
        $('.category').select2();
        $('.states').select2();
        $('.district').select2();
        $('.customer_class').select2();
        $('.type').select2();
        $('.pstype').select2();
        $('.state_list').change(function () {
            var sid = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-district",
                data:'sid='+sid+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $('.dist_list').html(data);
                }
            });
        });

        var startDate = moment('<?=$start_date?>', 'YYYY-MM-DD HH:mm:ss').format('DD, MMM YYYY HH:mm:ss');
        var endDate = moment('<?=$end_date?>', 'YYYY-MM-DD HH:mm:ss').format('DD, MMM YYYY HH:mm:ss');
        $('input[name="dates"]').daterangepicker({
            opens: 'left',
            locale: {
                format: 'DD, MMM YYYY'
            },
            startDate: startDate,
            endDate: endDate
        }, function(start, end, label) {
            $('#date_start').val(start.format('YYYY-MM-DD'));
            $('#date_end').val(end.format('YYYY-MM-DD'));
        });

        $(".states").change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-district-ajax",
                data:'cid='+val+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(".submit").html("Wait...");
                },
                success: function(data){
                    $(".district").html(data);
                    $(".submit").html("Search <i class='material-icons '>search</i>");
                }
            });
        });
        $('.ptype').change(function () {
            var val = $(this).val();

            $.ajax({
                type: "POST",
                url: "get-subtype",
                data:'tid='+val+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $('.pstype').html(data);
                }
            });
        });
    })

    $('.valid_no').blur(function () {
        var a = $(this).val();
        var b = $(this).val().length;
        var c = a.indexOf(",");
        if(c==-1){
            if(b==10){
                $('.add_submit').prop("disabled", false);
                $('.edit_submit').prop("disabled", false);
            }
            else{
                alert("Please Enter 10 digit no");
                $('.add_submit').prop("disabled", true);
                $('.edit_submit').prop("disabled", true);
            }
        }else{
            var str = a;
            var str_array = str.split(',');
            for(var i = 0; i < str_array.length; i++) {
                // Trim the excess whitespace.
                str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
                // Add additional code here, such as:
                var tot = str_array[i].length;
                if(tot!=10){
                    alert("Please Enter 10 digit number before and after comma");
                    $('.add_submit').prop("disabled", true);
                    $('.edit_submit').prop("disabled", true);
                }
                else{
                    $('.add_submit').prop("disabled", false);
                    $('.edit_submit').prop("disabled", false);
                }
            }
        }
    });

    $("input[name=contact_no]").keypress(function (e) {

        if (/\d+|,/i.test(e.key) ){

            console.log("character accepted: " + e.key)
        } else {
            console.log("illegal character detected: "+ e.key)
            return false;
        }

    });


    $(document).on('click','.locality_delete',function () {
        if (confirm("Are you sure?")) {
            var cid = $(this).val();
            $.ajax({
                type: "POST",
                url: "locality_delete",
                data:'lid='+cid+'&_token=<?=csrf_token(); ?>&_method=delete',
                success: function(data){
                    alert('Record Deleted Successfully');
                    location.reload();
                }
            });
        }
        else{

        }
        return false;
    });



</script>
</body>
</html>


