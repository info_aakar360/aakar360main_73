<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="keywords" content="">
    <meta name="author" content="ThemeSelect">
    <title>
        <?=$title; ?>
    </title>
    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/select.dataTables.min.css">

    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <!-- END: Custom CSS-->
</head>

<style type="text/css">

    .modal-window {
        position: fixed;
        background-color: rgba(200, 200, 200, 0.75);
        top: -70px;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 999;
        opacity: 0;
        pointer-events: none;
        -webkit-transition: all 0.3s;
        -moz-transition: all 0.3s;
        transition: all 0.3s;
    }

    .modal-window:target {
        opacity: 1;
        pointer-events: auto;
    }

    .modal-window>div {
        width: 800px;
        position: relative;
        margin: 10% auto;
        padding: 2rem;
        background: #fff;
        color: #444;
    }

    .modal-window header {
        font-weight: bold;
    }

    .modal-close {
        color: #aaa;
        line-height: 50px;
        font-size: 80%;
        position: absolute;
        right: 0;
        text-align: center;
        top: 0;
        width: 70px;
        text-decoration: none;
    }

    .modal-close:hover {
        color: #000;
    }

    .modal-window h1 {
        font-size: 150%;
        margin: 0 0 15px;
    }

</style>
<!-- END: Head-->
<?=$header;?>

<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 500px;">
                <h5 class="card-title" style="padding: 5px; color: #0d1baa;"><?=$title; ?><a href="<?=url('crm/brand-penetration-report/district-wise?'.$_SERVER['QUERY_STRING']);?>" class="btn btn-primary pull-right" style="float: right">District Wise</a></h5>
                <?=$notices;?>

                <div class="row" style="margin-top:30px; ">
                    <form method="get" action="" id="filterForm">
                        <div class="col s12 m6">
                            <i class="material-icons prefix">list</i>
                            <div class="col s12 m12" style="margin-left: 20px;margin-top: -62px;">
                                <label>Select Category</label>

                                <select class="browser-default category" id="category" name="category"  tabindex="-1">
                                    <option value="">All</option>
                                    <?php
                                    foreach ($category as $cate) {
                                        $c_id = $cate->id;
                                        ?>
                                        <option <?=($precat==$c_id)?'selected':''?> value="<?= $c_id; ?>"><?= $cate->name ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </form>
                        <div class="col s12 m6">
                            <i class="material-icons prefix">flag</i>
                            <div class="col s12 m12" style="margin-left: 20px;margin-top: -62px;">
                                <label>Select State</label>
                                <select class="browser-default states" id="states" name="states[]" multiple  tabindex="-1">
                                    <?php
                                    foreach ($states as $state) {
                                        $v_id = $state->id;
                                        ?>
                                        <option value="<?=$v_id; ?>"><?= $state->name ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 10px;" >
                        <button class="btn myblue waves-effect waves-light submit" style="padding:0 5px;" id="filterButton"><i class="material-icons right" style="margin-left:3px">search</i>Search
                        </button>
                    </div>

                </div>
                <div class="divider"></div>

                <div class="row" style="position:relative;">
                    <div class="col s12 table-responsive">
                        <table id="table" class="responsive display">
                            <thead>
                                <tr role="row">
                                    <th><i class="material-icons">arrow_downward</i>State / Brand<i class="material-icons">arrow_forward</i></th>
                                    <?php
                                        foreach($brands as $brand){ ?>
                                            <th><?=$brand->name?></th>
                                        <?php }

                                    ?>

                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.js"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/select2.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/ui-alerts.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<!-- <script src="<?php //$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script> -->
<?php echo $footer ?>
</body>

</html>

<script>
    $(document).ready(function(){
        $('#table').attr('style', 'width: 100%');
        $('.user').select2();
        $('.category').select2();
        $('.states').select2();
        $('.district').select2();
        $('.brand').select2();

        $(".category").change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url:  "<?=url("crm/get-brands") ?>",
                data:'cid='+val+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(".submit").html("Wait...");
                },
                success: function(data){
                    $(".brand").html(data);
                    $(".submit").html("Search <i class='material-icons '>search</i>");
                }
            });
        });
        $('.category').change(function(){
            $('form#filterForm').submit();
        })

    });
</script>
<script>
    $('#table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "<?=url("crm/get-bpr-data-sw") ?>",
            type: 'POST',
            data: function (d) {
                d.category = $('#category').val();
                d.states = $('#states').val();
                d.brand = $('#brand').val();
                d._token = '<?=csrf_token()?>';
            }
        },
        columns: JSON.parse('<?=json_encode($columns); ?>'),
        dom: 'lBfrtip',
        buttons: <?=$buttons;?>,
        fixedColumns: true,
        colReorder: true,
        exportOptions:{
            columns: ':visible'
        }
    });
    $('#filterButton').click(function(){
        $('#table').DataTable().draw(true);
    });
</script>