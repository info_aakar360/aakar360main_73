<!--    tp= http://localhost/aakar360/assets/crm-->
<!--    --><?//=$data['tp'] ?><!--  http://localhost/aakar360/themes/default/assets/img2.jpg-->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$title ?></title>
    <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <base href="<?=url('') ?>/" />
    <link rel="shortcut icon" href="<?=url('') ?>/favicon.ico" type="image/x-icon">

    <link rel="stylesheet" type="text/css" href="<?=$tp;?>/assets/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp;?>/assets/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp;?>/assets/iofrm-style.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp;?>/assets/iofrm-theme3.css">
</head>
<!-- END: Head-->
<body>
<div class="form-body">
    <div class="row">
        <div class="img-holder" style="background-image: url(<?=$tp;?>/assets/img2.jpg)">
            <div class="bg"></div>
            <div class="info-holder">
            </div>
        </div>
        <div class="form-holder">
            <div class="form-content" style="height:100vh;">
                <div class="form-items">
                    <div class="website-logo">
                        <a href="<?=url('')?>">
                            <div class="logo">
                                <img class="logo-size" src="<?=$cfg->logo ?>" alt="">
                            </div>
                        </a>
                    </div>
                    <h3>Enter Your Otp</h3>
                    <?php
                    if (isset($error)){
                        echo '<div class="alert alert-warning">'.translate($error).'</div>';
                    }
                    ?>
                    <form action="{{url('crm/postotp')}}" method="post">
                        <?=csrf_field() ?>
                        <input tabindex="2" class="form-control" type="text" name="otpinpput" placeholder="Enter Otp For Login" required>
                        <div class="form-button">
                            <button id="submit" name="otpsubmit" type="submit" class="ibtn" tab-index="3">Submit</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>