<?php
$qp=0; foreach ($_POST['Qty_pc'] as $qp1) {
    if($qp1!=''){
        $qp = 1;
        break;
    }
}
$qt=0; foreach ($_POST['Qty_mt'] as $qt1) {
    if($qt1!=''){
        $qt = 1;
        break;
    }
}
$rem=0; foreach ($_POST['remark'] as $re) {
    if($re!=''){
        $rem = 1;
        break;
    }
}
$show_ins = 0;
if(isset($_POST['insurance'])){
    foreach($_POST['insurance'] as $i){
        if($i){
            $show_ins = 1;
            break;
        }
    }
}
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <style>
        @page { margin: 180px 25px 130px 25px;}
        header {
            position: fixed;
            top: -180px;
            left: -30px;
            right: 0px;
        }
        footer { position: fixed; bottom: -140px; left: -30px; right: 0px; height: 189px; }
        footer .pagenum:before { content: counter(page); }
    </style>
</head>
<body>
<?php if(isset($_POST['with_letter'])){ ?>
    <header>
        <img src="assets/header1.png" width="800px">
    </header>
    <footer>
        <img src="assets/footer.png" width="800px">
    </footer>
<?php } ?>
<main style="">
    <?php if(isset($_POST['with_letter'])){ ?>
        <table width="100%" style="" cellspacing="0" cellpadding="0">
            <?php if(isset($_POST['date']) != ""){ ?>
                <tr>
                    <td colspan="8" style="text-align: right;">Date : <?=$_POST['date']; ?> </td>
                </tr>
            <?php }?>
            <tr>
                <td colspan="8">
                    <p>To,</p>
                    <?php if(isset($_POST['kindly']) != ""){ ?><p>Kind Attention : Sir <?=$_POST['kindly']; ?></p><?php } ?>

                    <?php if($_POST['customer_name'] != ""){ ?><p>Company Name : <?=$_POST['customer_name']; ?></p><?php } ?>
                    <?php if($_POST['address'] != ""){ ?><p>Address : <?=$_POST['address']; ?></p><?php } ?>

                    <?php if($_POST['mobile'] != ""){ ?><p>Contact No. : <?=$_POST['mobile']; ?></p><?php } ?>
                    <br />

                    <p>Ref : Your inquiry through Mail / Message / Whatsapp / Telephone. </p>
                </td>
            </tr>

            <tr>
                <td colspan="8">&nbsp;</td>
            </tr>
            <?php if($_POST['textline'] != ""){ ?>
                <tr>
                    <td colspan="8">Dear Sir, <br><?=$_POST['textline']; ?></td>
                </tr>
            <?php } ?>
            <tr>
                <td colspan="8">&nbsp;</td>
            </tr>
        </table>
    <?php } ?>
    <table width="100%" style="page-break-inside: auto;" cellspacing="0" cellpadding="0">
        <thead>
        <tr style="border: 1px solid text-align: center;">
            <th style="border: 1px solid; text-align: center;">S. No.</th>

            <th style="border: 1px solid; text-align: center;">Item</th>
            <th style="border: 1px solid; text-align: center;">Brand</th>
            <?php if($qp==1){ ?>
                <th style="border: 1px solid; text-align: center;">Qty in Pc</th>
            <?php } ?>
            <?php if($qt==1){ ?>
                <th style="border: 1px solid; text-align: center;">Qty in MT</th>
            <?php } ?>
            <?php if($_POST['price_hide']==0) {  ?>
                <th style="border: 1px solid; text-align: center;">Basic </th>
                <th style="border: 1px solid; text-align: center;">Size Diffrence</th>
                <th style="border: 1px solid; text-align: center;">Loading</th>
                <?php if($show_ins){ ?>
                    <th style="border: 1px solid; text-align: center;">Insurance</th>
                <?php } ?>
                <th style="border: 1px solid; text-align: center;">Tax Amount</th>
            <?php } ?>
            <th style="border: 1px solid; text-align: center;">Inclusive Tax</th>
            <?php if($qt==1){ ?>
                <th style="border: 1px solid; text-align: center;">Total Estimate</th>
            <?php } ?>
            <?php if($rem==1){ ?>
                <th style="border: 1px solid; text-align: center;">Remark</th>
            <?php } ?>
        </tr>
        </thead>
        <tbody>
        <?php $sr = 1;
        $gtotal = 0;
        $i=0;
        foreach ($_POST['var'] as $b){
            $cols = 5;
            //$p = $_POST['product'][$i];
            ?>
            <tr>
                <td style="border: 1px solid; text-align: center;"><?=$sr; ?></td>

                <td style="border: 1px solid; text-align: center;"><?=variantTitle($_POST['size'][$i]); ?></td>
                <td style="border: 1px solid; text-align: center;"><?=($_POST['brand'][$i] == 'NA' ? 'NA' : getBrandNames($_POST['brand'][$i]));?></td>
                <?php if($qp==1){  $cols++;?>
                    <td style="border: 1px solid; text-align: center;"><?=$_POST['Qty_pc'][$i]; ?></td>
                <?php } ?>
                <?php if($qt==1){ $cols++; ?>
                    <td style="border: 1px solid; text-align: center;"><?=isset($_POST['Qty_mt'][$i]) ? ($_POST['Qty_mt'][$i] =='') ? 1 : $_POST['Qty_mt'][$i] : 1; ?></td>
                <?php } ?>
                <?php if($_POST['price_hide']==0) { $cols = $cols + 4;  ?>
                    <td style="border: 1px solid; text-align: center;"><?=$_POST['basic_price'][$i]; ?></td>
                    <td style="border: 1px solid; text-align: center; padding: 10px;"><?=$_POST['size_diffrence_price'][$i]; ?></td>
                    <td style="border: 1px solid; text-align: center; padding: 10px;"><?=$_POST['loading'][$i]; ?></td>
                    <?php if($show_ins){  $cols++; ?>
                        <td style="border: 1px solid; text-align: center; padding: 10px;"><?=$_POST['insurance'][$i]; ?></td>
                    <?php } ?>
                    <td style="border: 1px solid; text-align: center; padding: 10px;"><?=$_POST['tax_amount'][$i].'<br> ( '.$_POST['tax_set'][$i].' % ) '; ?> </td>
                <?php } ?>
                <td style="border: 1px solid; text-align: center; padding: 10px;"><?=$_POST['total'][$i]; ?></td>
                <?php if($qt==1){  $cols++;?>
                    <td style="border: 1px solid; text-align: center; padding: 10px;"><?=$_POST['estimate'][$i]; ?></td>
                <?php } ?>
                <?php if($rem==1){ ?>
                    <td style="border: 1px solid; text-align: left; padding: 10px;"><?=$_POST['remark'][$i]; ?></td>
                <?php } ?>
            </tr>
            <?php
            if($qt==1){
                $gtotal = $gtotal + $_POST['estimate'][$i];
            }
            $sr++; $i++;
        }
        ?>
        <?php if($qt==1){ ?>
            <tr>
                <td colspan="<?=($cols-2);?>" style="text-align: right;background-color: #505050 !important;color:#ffffff;border: 1px solid; padding: 10px;">Grant Total</td>
                <td colspan="1" style="background-color: #505050 !important;color:#ffffff;border: 1px solid; text-align: center; padding: 10px;"><span id="grand_total"><?=$gtotal;?></span></td>
                <?php if($rem==1){ ?>
                    <td colspan="1" style="background-color: #505050 !important;color:#ffffff;border: 1px solid; text-align: center; padding: 10px;"></td>
                <?php } ?>
            </tr>
        <?php } ?>
        </tbody>
    </table>

    <table width="100%">
        <tr>
            <td colspan="8">&nbsp;</td>
        </tr>
        <tr>
            <td style="padding-left:5px;" colspan="8"><b style="font-size: 16px;">Commercial Terms & Condition</b></td>
        </tr>


        <?php if($_POST['term_gst'] != ""){ ?>
            <tr>
                <td style="padding-left:5px;" colspan="8">
                    <u>GST</u> :- <?=$_POST['term_gst']; ?>
                </td>
            </tr>
        <?php } if($_POST['term_price'] != ""){ ?>
            <tr>
                <td style="padding-left:5px;" colspan="8">
                    <u>Price</u> :- <?=$_POST['term_price']; ?>
                </td>
            </tr>
        <?php }  if($_POST['term_cutting'] != ""){ ?>
            <tr>
                <td style="padding-left:5px;" colspan="8">
                    <u>Freight</u> :- <?=$_POST['term_cutting']; ?>
                </td>
            </tr>

        <?php }  if($_POST['term_delivery'] != ""){ ?>
            <tr>
                <td style="padding-left:5px;" colspan="8">
                    <u>Delivery</u> :- <?=$_POST['term_delivery']; ?>
                </td>
            </tr>
        <?php }  if($_POST['term_payment'] != ""){ ?>
            <tr>
                <td style="padding-left:5px;" colspan="8">
                    <u>Payment Terms</u> :- <?=$_POST['term_payment']; ?>
                </td>
            </tr>
        <?php }  if($_POST['term_date'] != ""){ ?>
            <tr>
                <td style="padding-left:5px;" colspan="8">
                    <u>Validity</u> :- <?=$_POST['term_date']; ?>
                </td>
            </tr>
        <?php }  if($_POST['term_other'] != ""){ ?>
            <tr>
                <td style="padding-left:5px;" colspan="8">
                    <u>Other Charges</u> :- <?=$_POST['term_other']; ?>
                </td>
            </tr>
        <?php } ?>
        <?php if(isset($_POST['with_letter'])){ ?>
            <?php	if($_POST['last_line'] != ""){ ?>
                <tr>
                    <td colspan="8">&nbsp;</td>
                </tr>

            <?php }  if($_POST['last_line'] != ""){ ?>
                <tr>
                    <td style="padding-left:5px;" colspan="8">
                        <?=$_POST['last_line']; ?>
                    </td>
                </tr>
            <?php }  if($_POST['last_line_1'] != ""){ ?>
                <tr>
                    <td style="padding-left:5px;" colspan="8">
                        <?=$_POST['last_line_1']; ?>
                    </td>
                </tr>
            <?php }  if($_POST['shop_address'] != ""){ ?>
                <tr>
                    <td colspan="8">&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align:left; padding-right:10px; width: 180px" >Thanks & Regards <br> <?=$_POST['shop_address'] ?></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            <?php } ?>
        <?php } ?>
    </table>
</main>

</body>
</html>
