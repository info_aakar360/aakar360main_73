<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <title><?=$title; ?></title>
    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <!-- END: Custom CSS-->
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />
</head>
<!-- END: Head-->
<body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu 2-columns  " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

<?=$header; ?>

<!-- BEGIN: Page Main-->
<div id="main" style="padding-left: 0px;">
    <div class="row">
        <div class="col-xs-12 col-lg-12">
            <div class="container">
                <div class="seaction">
                    <!--Invoice-->
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <?php
                            foreach($company as $comp){?>
                            <div id="basic-tabs" class="card card card-default scrollspy" style="background-image: url(<?=url('assets/crm/images/letter_head/'.$comp->latter_head.''); ?>);">
                                <div class="card-content pt-5 pr-5 pb-5 pl-5">

                                    <div id="invoice">
                                        <form method="post" action="pdf">
                                            <?=csrf_field(); ?>
                                            <input type="hidden" value=""  name="category">
                                            <input type="hidden" value=""  name="variants">
                                            <input type="hidden" value="<?=$company_id; ?>"  name="company" id="company">
                                            <input type="hidden" id="model_report_type" value=""  name="report_type">
                                            <div class="invoice-header">
                                                <button type="button" value="only_price" class="btn myblue waves-effect waves-light right only">Tax Paid Rate</button>
                                                <button type="button" value="only_price" class="btn myblue waves-effect waves-light right detail">Breakup Rate</button>

                                                <div class="row section">
                                                    <div class="col s12 m6 l6">
                                                        <img class="mb-2 width-50" src="<?=url('assets/crm/images/company_logo/'.$comp->logo.''); ?>" style="width: 125px; height: 100px;" alt="company logo">
                                                    </div>
                                                    <div class="col s12 m6 l6">
                                                        <h4 class="text-uppercase right-align strong mb-5">Quotation</h4>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                                <div class="row section">
                                                    <div class="col s12 m6 l6">
                                                        <label class="text-uppercase strong mb-2 mt-3">Recipient</label>

                                                        <select name="select_cust" class="cus browser-default" id="cashtomer">
                                                            <?php
                                                            foreach($customers as $co){
                                                                ?>
                                                                <option <?=($cust_id==0)?'':(($cust_id==$co->id)?'selected':'disabled');?> value="<?=$co->id; ?>"><?=$co->name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                        <div class="col s12 m12 l12">&nbsp;</div>
                                                        <div id="custdata">
                                                            <?php
                                                            if($cust_id !=0){
                                                                $com = DB::table('company')->where('id', '=',$company_id)->first();
                                                                $cus = DB::table('crm_customers')->where('id', '=', $cust_id)->first();
                                                                $dist = DB::table('district')->where('id', '=', $cus->district)->first();
                                                                $state = DB::table('states')->where('id', '=', $cus->state)->first();

                                                                $html ='<p>To,</p>
		<p>Kind Attention : Sir <input type="hidden" name="kindly" class="form-control" value="'.$cus->name.'">'.$cus->name.'</p>
        <br/>
		<p>Company Name : <input type="hidden" name="customer_name" class="form-control" value="'.$company_id.'">'.$company_id.'</p>
        <p>Address : <input type="hidden" name="address" class="form-control" value="'.$com->address.'">'.$com->address.' </p>
        
        <p>Contact No. : <input type="hidden" class="form-control" name="mobile" value="'.$cus->contact_no.'">'.$cus->contact_no.'</p>
        <br />
        
        <input type="hidden" value="" class="form-control" name="gst">         
        <input type="hidden" name="city" class="form-control" value="'.$dist->name.'"> 
        <input type="hidden" name="state" class="form-control" value="'.$state->name.'">
        
        <p>Ref : Your inquiry through Mail / Message / Whatsapp / Telephone. </p>
        <br />';
                                                                echo $html;

                                                            }
                                                            else {
                                                                ?>
                                                                <p>Customer - Name : <input type="text" name="customer_name" class="form-control" value=""></p>
                                                                <p>Contact No. : <input type="text" class="form-control" name="mobile" value=""></p>
                                                                <p>GST No. : <input type="text" value="" class="form-control" name="gst"> </p>
                                                                <p>Customer - Address : <input type="text" name="address" class="form-control" value=""> </p>
                                                                <p>District : <input type="text" name="city" class="form-control" value=""> </p>
                                                                <p>State : <input type="text" name="state" class="form-control" value=""></p>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <div class="col s12 m6 l6">
                                                        <div class="invoce-no right-align">
                                                            <label class="text-uppercase strong">Date</label> <input type="text" class="form-control" value="<?php echo date('j, F,Y'); ?>" name="date">
                                                        </div>
                                                    </div>
                                                    <div class="col s12 m12 l12">
                                                        <p>Dear Sir
                                                            <br>
                                                            <input type="text" class="form-control" name="textline" value="We are in receipt of your valuable inquiry and in compliance there of we are here by submitting our best offer as below.">
                                                        </p>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="invoice-table">
                                                <div class="row" style="position:relative;">
                                                    <div class="col s12 table-responsive"><!--1x 11661593 6YDgrn-->
                                                        <?php if($quot=='brand_wise'){
                                                            ?>
                                                            <input type="hidden" value="0" name ="price_hide" class="price_hide">
                                                            <table class="highlight">
                                                                <thead>
                                                                <tr>
                                                                    <th>S. No.</th>
                                                                    <th>Item</th>
                                                                    <th>Brand</th>
                                                                    <th>Qty in Pc</th>
                                                                    <th>Qty in MT</th>
                                                                    <th class="price_details">Basic</th>
                                                                    <th class="price_details">Size Diffrence</th>
                                                                    <th class="price_details">Loading</th>
                                                                    <th class="price_details">Insurance</th>
                                                                    <th class="price_details">Tax Amount</th>
                                                                    <th class="price_details">Adjustment</th>
                                                                    <th>Inclusive Tax</th>
                                                                    <th>Total Estimate</th>
                                                                    <th>Remark</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php $sr = 1;
                                                                $gtotal = 0;
                                                                foreach ($brands as $br) {
                                                                    $br_id =  $br->id;

                                                                    $get_cat = DB::select("SELECT category FROM products  WHERE id = '$br_id'")[0];
                                                                    $get_cat_id = $get_cat->category;
                                                                    $cust_product_price = getCategoryAddPrice($get_cat_id, $cust_id);

                                                                    foreach ($variants as $b) {
                                                                        $vid =  $b->id;
                                                                        $variants_details = DB::select("SELECT p.*,pv.* FROM products as p INNER JOIN product_variants as pv ON p.id = pv.product_id WHERE p.id = '$br_id' AND pv.variant_title = (SELECT variant_title FROM product_variants WHERE id ='$vid')");
                                                                        if(empty($variants_details)){
                                                                            $variants_details = DB::select("SELECT p.* FROM products as p  WHERE p.id = '$br_id'");
                                                                        }

                                                                        $vsr = 0;
                                                                        $qty = 1;
                                                                        foreach ($var_id as $varid){

                                                                            if($brn_id[$vsr]==$br_id && $var_id[$vsr]==$vid ){

                                                                                if($hide_status[$vsr]==0){
                                                                                    ?>
                                                                                    <tr>
                                                                                        <td><input type="hidden" name="var[]" value="<?=$vid; ?>"> <?=$sr?></td>

                                                                                        <td><input class="variant_details" type="hidden" name="size[]" value="<?=$b->variant_title; ?>"><?=variantTitle($b->variant_title); ?></td>
                                                                                        <td>
                                                                                            <select name="brand[]">
                                                                                                <option value="<?=$variants_details[0]->brand_id; ?>"><?=getBrandNames($variants_details[0]->brand_id); ?></option>
                                                                                                <option value="NA">NA</option>
                                                                                            </select>
                                                                                        </td>
                                                                                        <td><input type="text" name="Qty_pc[]" class="form-control"></td>
                                                                                        <td><input type="text" name="Qty_mt[]" class="form-control quantity" value="" data-key="<?=$vid;?>"></td>
                                                                                        <?php
                                                                                        $loading = getLoadingByHub($br_id, $mhid, $cust_type);
                                                                                        if($loading == null){
                                                                                            $loading = 0;
                                                                                        }
                                                                                        $insurance = 25;
                                                                                        $adjustment = 0;

                                                                                        $sd = is_numeric($variants_details[0]->price) ? $variants_details[0]->price : 0;
                                                                                        $sd = getSize($sd);
                                                                                        $pp = is_numeric($variants_details[0]->purchase_price) ? $variants_details[0]->purchase_price : 0;

                                                                                        $pp = $pp + $set_price + $cust_product_price;
                                                                                        $tax = $variants_details[0]->tax;
                                                                                        $tx_val =  ($sd+$pp+$loading+$insurance)*(int)$tax/100;
                                                                                        ?>
                                                                                        <td class="price_details "><input type="hidden" name="basic_price[]" value="<?=$pp; ?>" class="basic_price" data-key="<?=$vid;?>"><?=$pp; ?></td>
                                                                                        <td class="price_details"><input type="hidden" name="size_diffrence_price[]" value="<?=$sd; ?>" class="size_diff" data-key="<?=$vid;?>"><?=$sd; ?></td>
                                                                                        <td class="price_details"><input type="hidden" name="loading[]" value="<?=$loading; ?>" class="loading" data-key="<?=$vid;?>"><?=$loading; ?></td>
                                                                                        <td class="price_details"><input type="text" name="insurance[]" data-key="<?=$vid;?>" value="<?=$insurance;?>" class="form-control insurance"></td>
                                                                                        <input type="hidden" name="tax_set[]" class="tax" data-key="<?=$vid;?>" value="<?=$tax;?>">
                                                                                        <td class="price_details" ><input type="hidden" name="tax_amount[]" class="tax_amount" data-key="<?=$vid;?>" value="<?=$tx_val =  ($sd+$pp+$loading+$insurance)*(int)$tax/100; ?>"><span class="tax_val" data-key="<?=$vid;?>"><?=$tx_val =  ($sd+$pp+$loading+$insurance)*(int)$tax/100; ?></span> (<?=$tax; ?>%)</td>
                                                                                        <td class="price_details"><input type="text" name="adjustment[]" data-key="<?=$vid;?>" value="<?=$adjustment;?>" class="form-control adjustment"></td>
                                                                                        <td data-key="<?=$vid;?>"><input type="hidden" name="total[]" class="total" data-key="<?=$vid;?>" value="<?php echo $result = number_format($tx_val + ($sd+$pp+$loading+$insurance), 0, '.', ''); ?>"><span class="result" data-key="<?=$vid;?>"><?php echo $result; ?></span><input type="hidden" data-key="<?=$vid;?>" class="v_tot_price" value="<?=$result?>"></td>
                                                                                        <td data-key="<?=$vid;?>"><input type="hidden" name="estimate[]" class="estimate_val" data-key="<?=$vid;?>" value="<?php echo $result; ?>"><span class="estimate" data-key="<?=$vid;?>"><?php echo $result; ?></span></td>

                                                                                        <td><input type="text" name="remark[]" class="form-control"></td>
                                                                                    </tr>

                                                                                    <?php
                                                                                    $sr++;
                                                                                    $gtotal = $gtotal + $result;
                                                                                }
                                                                            }
                                                                            $vsr++;
                                                                        } ?>

                                                                        <?php


                                                                    }
                                                                }
                                                                ?>
                                                                <tr>
                                                                    <td colspan="12" style="text-align: right;background-color: #505050 !important;color:#ffffff;">Grant Total</td>
                                                                    <td colspan="1" style="background-color: #505050 !important;color:#ffffff;"><span id="grand_total"><?=$gtotal;?></span></td>
                                                                    <td colspan="1" style="background-color: #505050 !important;color:#ffffff;"><a href="javascript:void(0);" class="refresh_gt"><i class="material-icons" style="color: #ffffff;">cached</i></a></td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                            <?php
                                                        }
                                                        else{
                                                            ?>
                                                            <table class="highlight responsive-table">
                                                                <input type="hidden" value="0" name ="price_hide" class="price_hide">
                                                                <thead>
                                                                <tr>
                                                                    <th style="border: 1px solid; text-align: center;">S. No.</th>
                                                                    <th style="border: 1px solid; text-align: center;">Item</th>
                                                                    <th style="border: 1px solid; text-align: center;">Brand</th>
                                                                    <th style="border: 1px solid; text-align: center;">Length</th>
                                                                    <th style="border: 1px solid; text-align: center;">Qty in Pc</th>
                                                                    <th style="border: 1px solid; text-align: center;">Qty in MT</th>
                                                                    <th class="price_details" style="border: 1px solid; text-align: center;">Basic </th>
                                                                    <th class="price_details" style="border: 1px solid; text-align: center;">Size Diffrence </th>
                                                                    <th class="price_details" style="border: 1px solid; text-align: center;">Inclusive Tax</th>
                                                                    <th style="border: 1px solid; text-align: center;">Total</th>
                                                                    <th style="border: 1px solid; text-align: center;">Remark</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php $sr = 1;

                                                                foreach ($variants as $b) {
                                                                    $vid =  $b->id;
                                                                    $vt =  $b->variant_title;
                                                                    $variants_details = DB::select("SELECT t1.*,p.title,p.purchase_price,p.tax FROM products as p  INNER JOIN (SELECT * FROM product_variants WHERE price = (SELECT min(price) FROM product_variants WHERE variant_title = '$vt')) as t1 ON p.id = t1.product_id");
                                                                    //dd($variants_details[0]);
                                                                    $vsid = $variants_details[0]->product_id;
                                                                    $get_cat = DB::select("SELECT * FROM products  WHERE id = '$vsid'")[0];
                                                                    $get_cat_id = $get_cat->category;

                                                                    if(empty($variants_details)){
                                                                        $variants_details = DB::select("SELECT p.* FROM products as p  WHERE p.id = '$br_id'");
                                                                    }

                                                                    $cust_product_price = getCategoryAddPrice($get_cat_id, $cust_id);

                                                                    ?>
                                                                    <tr>
                                                                        <td style="border: 1px solid; text-align: center;"><input type="hidden" name="var[]" value="<?=$sr; ?>"> <?=$sr?></td>

                                                                        <td style="border: 1px solid; text-align: center;"><input class="variant_details" type="hidden" name="size[]" value="<?=$b->variant_title; ?>"><?=variantTitle($b->variant_title); ?></td>
                                                                        <td style="border: 1px solid; text-align: center;">
                                                                            <select name="brand[]">
                                                                                <option value="<?=$get_cat->brand_id; ?>"><?=getBrandNames($get_cat->brand_id); ?></option>
                                                                                <option value="NA">NA</option>
                                                                            </select>
                                                                        </td>
                                                                        <td style="border: 1px solid; text-align: left; padding: 10px;"><input type="text" name="length[]" class="form-control"></td>
                                                                        <td style="border: 1px solid; text-align: left; padding: 10px;"><input type="text" name="Qty_pc[]" class="form-control"></td>
                                                                        <td style="border: 1px solid; text-align: left; padding: 10px;"><input type="text" name="Qty_mt[]" class="form-control"></td>
                                                                        <?php

                                                                        $sd = is_numeric($variants_details[0]->price) ? $variants_details[0]->price : 0;
                                                                        $sd = getSize($sd);
                                                                        $pp = is_numeric($variants_details[0]->purchase_price) ? $variants_details[0]->purchase_price : 0;
                                                                        $pp = $pp + $set_price + $cust_product_price;
                                                                        $tax = $variants_details[0]->tax;
                                                                        $tx_val =  ($sd+$pp)*(int)$tax/100;
                                                                        ?>
                                                                        <td class="price_details" style="border: 1px solid; text-align: center;"><input type="hidden" name="basic_price[]" value="<?=$pp; ?>"><?=$pp; ?></td>
                                                                        <td class="price_details" style="border: 1px solid; text-align: center; padding: 10px;"><input type="hidden" name="size_diffrence_price[]" value="<?=$sd; ?>"><?=$sd; ?></td>
                                                                        <input type="hidden" name="tax_set[]" value="<?=$tax;?>">
                                                                        <td class="price_details" style="border: 1px solid; text-align: center; padding: 10px;"><input type="hidden" name="tax_amount[]" value="<?=$tx_val =  ($sd+$pp)*$tax/100; ?>"><?=$tx_val =  ($sd+$pp)*$tax/100; ?> (<?=$tax; ?>%)</td>
                                                                        <td style="border: 1px solid; text-align: center; padding: 10px;"><input type="hidden" name="total[]" class="total" value="<?php echo $result = $tx_val + ($sd+$pp); ?>"><?php echo $result = $tx_val + ($sd+$pp); ?></td><input type="hidden" class="v_tot_price" value="<?=$result?>">
                                                                        <td style="border: 1px solid; text-align: left; padding: 10px;"><input type="text" name="remark[]" class="form-control"></td>
                                                                    </tr>
                                                                    <?php $sr++;

                                                                }
                                                                ?>
                                                                </tbody>
                                                            </table>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="invoice-footer mt-6">
                                                <div class="row">
                                                    <div class="col s12 m6 l6">
                                                        <p>Commercial Terms & Condition</p>
                                                        <p>GST :- <input type="text" name="term_gst" value="Included" class="form-control"> </p>
                                                        <p>Price :- <input type="text" class="form-control" value="EX-Factory basis" name="term_price"> </p>
                                                        <p>Freight :- <input type="text" name="term_cutting" value="Extra (if extra gst shall be applicable on freight)" class="form-control"> </p>
                                                        <p>Delivery :- <input type="text" name="term_delivery" value="As mutually agreed" class="form-control"> </p>
                                                        <p>Payment Terms :- <input type="text" name="term_payment" value="Next Day" class="form-control"> </p>
                                                        <p>Validity :- <input type="text" name="term_date" value="One day" class="form-control"> </p>
                                                        <p>Other Charges :- <input type="text" name="term_other" value="" class="form-control"> </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            foreach($company as $comp){?>
                                                <div class="col s12 m12 l12">
                                                    <input type="text" name="last_line" class="form-control" value="We hope you will find our offer attractive and competitive">
                                                    <input type="text" name="last_line_1" class="form-control" value="We are looking forward for your valuable P.O. and wonderfull business relation ahead.">
                                                </div>
                                                <div class="col s12 m12 l12">Thanks & Regards
                                                    <input type="text" name="shop_address" class="form-control" value="<?=$comp->name; ?>">
                                                </div>
                                                <div class="col s12 m12 l12">
                                                    <label class="form-label" style="color: #000"><input type="checkbox" name="with_letter" class="opt" value="1" style="opacity: 1;position: relative;margin-right: 5px;" checked/> With Letter Head</label>
                                                </div>
                                                <div class="col s12 m2 l2">
                                                    <button type="submit" class="btn btn-primary myblue" name="pdfsub">PDF</button>
                                                </div>
                                                <div class="col s12 m2 l2">
                                                    <button type="button" class="btn btn-primary myblue msg">Message</button>
                                                </div>
                                                <div class="col s12 m3 l3">
                                                    <input type="text" value="" class="cust_no" placeholder="Contact Number">
                                                </div>
                                                <div class="col s12 m5 l5">
                                                    <textarea  class="msg_last" placeholder="Type Bottom Msg.">Thank you. &#13;&#10; <?=$comp->name; ?> &#13;&#10; For any query Please Contact &#13;&#10; 9111999799&#13;&#10; 8966885000 </textarea>
                                                </div>
                                                <div class="col s12 m2 l2">
                                                    <button type="button" class="btn btn-primary myblue mail">Mail</button>
                                                </div>
                                                <div class="col s12 m3 l3">
                                                    <input type="text" value="" class="cust_mail" placeholder="Email Id">
                                                </div>
                                            <?php } ?>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- START RIGHT SIDEBAR NAV -->

            </div>
        </div>
    </div>
</div>

<!-- END: Page Main-->



<!-- END: Footer-->
<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/select2.min.js"></script>
<script>
    $('.cus').select2();

    $(".cus").change(function(){
        var val = $(this).val();
        var com = $("#company").val();
//        alert(com);
        $.ajax({
            type: "POST",
            url: "get-cust-data",
            data:'cid='+val+'did='+com+'&_token=<?=csrf_token(); ?>',
            success: function(data){
                var options = JSON.parse(data);
                $("#custdata").html('');
                $("#custdata").html(options.html);
                $(".cust_no").val(options.cno);
                $(".cust_mail").val(options.cmail);
            }
        });


    });
    $('.only').click(function () {
        $('.price_details').css('display','none');
        $('.price_hide').val(1);

    });
    $('.detail').click(function () {
        $('.price_details').css('display','');
        $('.price_hide').val(0);
    });
    $('.msg').click(function () {
        var msg = '';
        $('.variant_details').each(function () {
            var p = $(this).closest('tr').find(".v_tot_price").val();
            var v = $(this).val();
            msg += v +' : '+ p + '\n';
        });
        var msg_last = $('.msg_last').val();

        msg += msg_last;
        var mob = $('.cust_no').val();
        var cname = encodeURIComponent(msg);
        if (confirm(msg)) {
            $.ajax({
                type: "GET",
                url: "https://merasandesh.com/api/sendsms",
                data:'username=smshop&password=Smshop@123&senderid=SUPORT&message='+cname+'&numbers='+mob+'&unicode=0',
                beforeSend: function(){
                    alert('Message Send Successfully');
                    location.reload();
                },
                success: function(data){
                }
            });
        } else {
            alert("Message Not Send.")
        }


    });
    $(document).on('change', '.insurance', function(){
        var $ins = $(this);
        if($ins.val() == ''){
            $ins.val('0');
        }
        var ins_val = $ins.val();
        var vid = $ins.data('key');
        var pp = $('.basic_price[data-key='+vid+']').val();
        var size_diff = $('.size_diff[data-key='+vid+']').val();
        var loading = $('.loading[data-key='+vid+']').val();
        var adj_val = $('.adjustment[data-key='+vid+']').val();
        var tax = $('.tax[data-key='+vid+']').val();

        var total = (parseFloat(pp)+parseFloat(size_diff)+parseFloat(loading)+parseFloat(ins_val));
        var tax_amt = (parseFloat(total))*(parseFloat(tax)/100);

        var gt = total+tax_amt;
        if(adj_val == ''){
            adj_val = 0;
        }
        var fin_amt = parseInt(gt) + parseInt(adj_val);
        var qty = $('.quantity[data-key='+vid+']').val();
        if(qty == '' || qty == 0){
            qty = 1;
        }
        var est = parseInt(qty)*parseInt(fin_amt);
        $('.tax_amount[data-key='+vid+']').val(tax_amt.toFixed(2));
        $('.tax_val[data-key='+vid+']').html(tax_amt.toFixed(2));
        $('.total[data-key='+vid+']').val(fin_amt.toFixed(0));
        $('.result[data-key='+vid+']').html(fin_amt.toFixed(0));
        $('.v_tot_price[data-key='+vid+']').val(fin_amt.toFixed(0));
        $('.estimate_val[data-key='+vid+']').val(est.toFixed(0));
        $('.estimate[data-key='+vid+']').html(est.toFixed(0));

        var fin_amt = 0;
        $('.estimate_val').each(function(){
            var gt = $(this).val();
            fin_amt = parseInt(fin_amt) + parseInt(gt);
        });
        $('#grand_total').html(fin_amt);
    });
    $(document).on('change', '.adjustment', function(){
        var $adj = $(this);
        if($adj.val() == ''){
            $adj.val('0');
        }
        var adj_val = $adj.val();
        var vid = $adj.data('key');
        var pp = $('.basic_price[data-key='+vid+']').val();
        var size_diff = $('.size_diff[data-key='+vid+']').val();
        var loading = $('.loading[data-key='+vid+']').val();
        var insurance = $('.insurance[data-key='+vid+']').val();
        var tax = $('.tax[data-key='+vid+']').val();

        var total = (parseFloat(pp)+parseFloat(size_diff)+parseFloat(loading)+parseFloat(insurance));
        var tax_amt = (parseFloat(total))*(parseFloat(tax)/100);

        var gt = total+tax_amt;
        var fin_amt = gt;

        fin_amt = parseInt(gt) + parseInt(adj_val);
        var qty = $('.quantity[data-key='+vid+']').val();
        if(qty == '' || qty == 0){
            qty = 1;
        }
        var est = parseInt(qty)*parseInt(fin_amt);
        $('.v_tot_price[data-key='+vid+']').val(fin_amt);
        $('.result[data-key='+vid+']').html(fin_amt);
        $('.total[data-key='+vid+']').val(fin_amt);
        $('.estimate_val[data-key='+vid+']').val(est.toFixed(0));
        $('.estimate[data-key='+vid+']').html(est.toFixed(0));

        var fin_amt = 0;
        $('.estimate_val').each(function(){
            var gt = $(this).val();
            fin_amt = parseInt(fin_amt) + parseInt(gt);
        });
        $('#grand_total').html(fin_amt);
    });
    $(document).on('change', '.quantity', function(){
        var $quantity = $(this);
        var qty = $quantity.val();
        if($quantity.val() == '' || qty == 0){
            var qty = 1;
        }
        var vid = $quantity.data('key');
        var adj_val = $('.adjustment[data-key='+vid+']').val();
        if(adj_val == ''){
            adj_val = 0;
        }
        var pp = $('.basic_price[data-key='+vid+']').val();
        var size_diff = $('.size_diff[data-key='+vid+']').val();
        var loading = $('.loading[data-key='+vid+']').val();
        var insurance = $('.insurance[data-key='+vid+']').val();
        var tax = $('.tax[data-key='+vid+']').val();

        var total = (parseFloat(pp)+parseFloat(size_diff)+parseFloat(loading)+parseFloat(insurance));
        var tax_amt = (parseFloat(total))*(parseFloat(tax)/100);

        var gt = total+tax_amt;
        var fin_amt = gt;

        fin_amt = parseInt(gt) + parseInt(adj_val);

        var est = parseInt(qty)*parseInt(fin_amt);
        $('.v_tot_price[data-key='+vid+']').val(fin_amt);
        $('.result[data-key='+vid+']').html(fin_amt);
        $('.total[data-key='+vid+']').val(fin_amt);
        $('.estimate_val[data-key='+vid+']').val(est.toFixed(0));
        $('.estimate[data-key='+vid+']').html(est.toFixed(0));

        var fin_amt = 0;
        $('.estimate_val').each(function(){
            var gt = $(this).val();
            fin_amt = parseInt(fin_amt) + parseInt(gt);
        });
        $('#grand_total').html(fin_amt);
    });
    $(document).on('click', '.refresh_gt', function(){
        var fin_amt = 0;
        $('.estimate_val').each(function(){
            var gt = $(this).val();
            fin_amt = parseInt(fin_amt) + parseInt(gt);
        });
        $('#grand_total').html(fin_amt);
    });
</script>
</body>

</html>