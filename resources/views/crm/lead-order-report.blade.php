<?php
$location=array();
/*foreach ($lat as $lat1)
{
    $latitude='';
    $longitude='';
    if($lat1->lat!='' AND $lat1->lng!='')
    {

        $latitude = $lat1->lat;
        $longitude = $lat1->lng;
        $location[] .= "{lat : ".$latitude.", lng : ".$longitude."}";

    }

//    else
    elseif($lat1->lat=='' && $lat1->lng=='' && $lat1->latitude!='' && $lat1->longitude!='')
    {
        $latitude = $lat1->latitude;
        $longitude = $lat1->longitude;
        $location[] .= "{lat : ".$latitude.", lng : ".$longitude."}";
    }

}*/


?>

<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="keywords" content="">
    <meta name="author" content="ThemeSelect">
    <title>
        <?=$title; ?>
    </title>
    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/select.dataTables.min.css">

    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <!-- END: Custom CSS-->
</head>

<style type="text/css">

    .modal-window {
        position: fixed;
        background-color: rgba(200, 200, 200, 0.75);
        top: -70px;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 999;
        opacity: 0;
        pointer-events: none;
        -webkit-transition: all 0.3s;
        -moz-transition: all 0.3s;
        transition: all 0.3s;
    }

    .modal-window:target {
        opacity: 1;
        pointer-events: auto;
    }

    .modal-window>div {
        width: 800px;
        position: relative;
        margin: 10% auto;
        padding: 2rem;
        background: #fff;
        color: #444;
    }

    .modal-window header {
        font-weight: bold;
    }

    .modal-close {
        color: #aaa;
        line-height: 50px;
        font-size: 80%;
        position: absolute;
        right: 0;
        text-align: center;
        top: 0;
        width: 70px;
        text-decoration: none;
    }

    .modal-close:hover {
        color: #000;
    }

    .modal-window h1 {
        font-size: 150%;
        margin: 0 0 15px;
    }
</style>
<!-- END: Head-->
<?=$header;?>

<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 500px;">
                <h5 class="card-title" style="padding: 5px; color: #0d1baa;">Lead/Order Report</h5>
                <?=$notices;?>

                <?php if(!isset($_GET['add']) && !isset($_GET['edit'])) {?>

                        <div class="row">
                            <div class="col s12 m6 xl4">
                                <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">format_list_bulleted</i></div>
                                <div class="col s10 m10">
                                    <label>Select Category</label>
                                    <select class="browser-default category" id="category" name="category[]" multiple  tabindex="-1">

                                        <?php
                                        $i=0;
                                        foreach ($customer_category as $cate) {
                                            $c_id = $cate->id;
                                            $selected = '';
                                            /*if($i == 0){
                                                $selected = 'selected';
                                            }*/
                                            ?>
                                            <option value="<?= $c_id; ?>" <?=$selected?>><?= $cate->type ?></option>
                                            <?php
                                            $i++;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col s12 m6 xl4">
                                <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">flag</i></div>
                                <div class="col s10 m10">
                                    <label>Select State</label>
                                    <select class="browser-default states" id="states" name="states[]" multiple  tabindex="-1">
                                        <?php
                                        foreach ($states as $state) {
                                            $v_id = $state->id;
                                            ?>
                                            <option value="<?=$v_id; ?>"><?= $state->name ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col s12 m6 xl4"> <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">adjust</i></div>
                                <div class="col s10 m10">
                                    <label>Select District</label>
                                    <select class="browser-default district" id="district" name="district[]" multiple  tabindex="-1">

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        <div class="col s12 m6 xl4">
                            <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">dns</i></div>
                            <div class="col s10 m10">
                                <label>Select Customer Class</label>
                                <select class="browser-default customer_class" id="customer_class" name="cust_class[]" multiple tabindex="-1">
                                    <?php
                                    foreach ($cust_classes as $cust_class) {
                                        $v_id = $cust_class->id;
                                        ?>
                                        <option value="<?=$v_id; ?>"><?= $cust_class->type ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col s12 m6 xl4">
                            <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">format_list_bulleted</i></div>
                            <div class="col s10 m10">
                                <label>Select Type</label>
                                <select class="browser-default type" id="type" name="type[]" multiple  tabindex="-1">

                                    <?php
                                    foreach ($customer_type as $type){
                                        $s_id = $type->id;
                                        ?>
                                        <option  value="<?=$s_id;?>"><?=$type->type?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="input-field col s12 m12 xl4">
                            <div class="col s1 m1"><i class="material-icons prefix" style="margin-top: 30px;">dns</i></div>
                            <div class="col s10 m10">
                                <label>Verified</label>
                                <select name="verified" id="verified" class="ptype" > type
                                    <option value="" >Select Option</option>
                                    <option value="1">Verified</option>
                                    <option value="2">Unverified</option>
                                </select>
                            </div>
                        </div>
                        </div>
                        <div class="row">
                        <div class="input-field col s12 m12 xl4">
                            <div class="col s1 m1"><i class="material-icons prefix" style="margin-top: 30px">dns</i></div>
                            <div class="col s10 m10">
                                <label>Product Category</label>
                                <select class="browser-default pro_category" name="pro_category[]" id="pro_category" multiple tabindex="-1" style="width: 100% !important;" > type
                                    <option value="" >Select Option</option>
                                    <?php foreach ($product_category as $pro_cat){?>
                                        <option  value="<?=$pro_cat->id?>"> <?=$pro_cat->name?> </option>
                                        <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="input-field col s12 m12 xl4">
                            <div class="col s1 m1"><i class="material-icons prefix" style="margin-top: 30px">dns</i></div>
                            <div class="col s10 m10">
                                <label>Pipeline</label>
                                <select class="browser-default stagebyname" name="pipeline" id="pipeline"  tabindex="-1" style="width: 100% !important;" > type
                                    <option value="" >Select Option</option>
                                    <?php foreach ($pipelines as $pipeline){?>
                                        <option  value="<?=$pipeline->id?>"> <?=$pipeline->name?> </option>
                                        <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="input-field col s12 m12 xl4">
                            <div class="col s1 m1"><i class="material-icons prefix" style="margin-top: 30px">dns</i></div>
                            <div class="col s10 m10">
                                <label>Pipeline Stage</label>
                                <select class="browser-default stage" name="pipeline_stage" id="pipeline_stage" multiple tabindex="-1" style="width: 100% !important;" > type
                                    <option value="" >Select Option</option>
                                </select>
                            </div>
                        </div>
                        </div>
                        <div class="row">
                        <div class="input-field col s12 m12 xl4" >
                            <div class="col s1 m1"><i class="material-icons prefix" style="margin-top: 30px">date_range</i></div>
                            <div class="col s10 m10" >
                                <label for="">Date Range
                                    <input type="text" id="date_range1" name="dates1">
                                </label>
                                <input type="hidden" name="start1" id="date_start1" value=""/>
                                <input type="hidden" name="end1" id="date_end1" value=""/>
                            </div>
                        </div>
                        <div class="input-field col s12 m12 xl4">
                            <div class="col s1 m1"><i class="material-icons prefix" style="margin-top: 30px">dns</i></div>
                            <div class="col s10 m10">
                                <label>User</label>
                                <select class="browser-default user" name="user[]" id="user" multiple tabindex="-1" style="width: 100% !important;" > type
                                    <option value="" >Select User</option>
                                    <?php foreach ($users as $user){  ?>
                                        <option  value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                        <?php } ?>
                                </select>
                            </div>
                        </div>
                        </div>
                    <div class="row">
                        <div class="col s12 m12" style="text-align: center">
                            <button class="btn myblue waves-effect waves-light submit" style="padding:0 5px;" id="filterButton"><i class="material-icons right" style="margin-left:3px">search</i>Search
                            </button>
                        </div>
                    </div>
                    <div class="divider"></div>
                    <div class="row" style="position:relative;">
                        <div class="col s12 table-responsive">
                            <h6>All Lead/Order</h6>
                            <table id="lead_table" class="display" style="width: 100%;">
                                <thead>
                                <tr role="row">
                                    <th>Sr.No.</th>
                                    <th>Name</th>
                                    <th style="display:none">Secondary Names</th>
                                    <th>Category</th>
                                    <th>State</th>
                                    <th>District</th>
                                    <th>Pipeline</th>
                                    <th>Stage</th>
                                    <th>Date</th>
                                    <th>Edited_By</th>
                                    <th>Created at</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                            </table>
                        </div>
                    </div>
                <?php } ?>
                <?php if(isset($_GET['add'])){ ?>
                    <h5>Add New Customer
                        <a class="btn myblue waves-light right" href="customers" >
                            <i class="material-icons right">add_circle_outline</i>Home
                        </a>
                    </h5>
                    <form method="post" action="customers" class="col s12 m12">
                        <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">account_circle</i>
                                <input id="Full_Name" type="text" class="validate" name="name">
                                <label for="Full_Name">Full Name</label>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">text_fields</i>
                                <input id="Proprieter" type="tel" class="validate" name="proprieter_name">
                                <label for="Proprieter">Company Name</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">email</i>
                                <input id="Email" type="text" class="validate" name="email">
                                <label for="Email">Email</label>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">phone</i>
                                <input id="Contact" type="tel" class="validate valid_no" name="contact_no"">
                                <label for="Contact">Contact No.</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">verified_user</i>
                                <input id="CID" type="text" class="validate" name="cid">
                                <label for="CID">CID</label>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">view_headline</i>
                                <select name="customer_category" class="cc" required> customer_category
                                    <option selected disabled value="" >Select Category</option>
                                    <?php
                                    foreach ($customer_category as $category){
                                        $s_id = $category->id;
                                        ?>
                                        <option  value="<?=$s_id;?>"><?=$category->type?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="customer_category">Category </label>

                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">view_quilt</i>
                                <select name="type">
                                    <?php
                                    foreach ($customer_type as $type){
                                        $s_id = $type->id;
                                        ?>
                                        <option  value="<?=$s_id;?>"><?=$type->type?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="type">Type </label>


                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">add_location</i>
                                <input id="Address" type="tel" class="validate" name="postal_address">
                                <label for="Address">Postal Address</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">add_location</i>
                                <select  class="state_list " name="state"  style="width: 100%" required>
                                    <?php
                                    foreach ($states as $state){
                                        $s_id = $state->id;
                                        ?>
                                        <option  <?php if(1 !=1) {
                                            echo "selected";
                                        } else {
                                            echo '';
                                        }?> value="<?=$s_id;?>"><?=$state->name?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="State">State</label>
                            </div>
                            <div class="input-field col s12 m6" >

                                <select  class="browser-default district dist_list" name="district" style="width: 100%" required>
                                    <option selected value="">District</option>
                                </select>

                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">security</i>
                                <select name="msg_active">
                                    <option value="0"> Yes</option>
                                    <option value="1"> No</option>
                                </select>
                                <label for="msg_active">Message Active</label>
                            </div>
                            <div class="input-field col s6">
                                <i class="material-icons prefix">security</i>
                                <select name="msg_class">
                                    <?php
                                    foreach ($customer_class as $class){
                                        $s_id = $class->id;
                                        ?>
                                        <option  value="<?=$s_id;?>"><?=$class->type?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="msg_class">Customer Class</label>
                            </div>

                            <div class="row category_price"></div>


                        </div>
                        <button class="btn myblue waves-light right add_submit" type="submit" disabled name="add">Add
                            <i class="material-icons right">save</i>
                        </button>

                    </form>
                <?php } ?>
                <?php if(isset($_GET['edit'])){
                    $cid = $_GET['edit'];
                    $get_cust = DB::select("SELECT * FROM `crm_customers` WHERE id = '$cid'")[0];
                    ?>
                    <h5>Edit Customer
                        <a class="btn myblue waves-light right" href="customers" >
                            <i class="material-icons right">add_circle_outline</i>Home
                        </a>
                    </h5>
                    <form method="post" action="customers" class="col s12">
                        <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                        <input type="hidden" class="ccid" name="cidd" value="<?php echo $cid; ?>">
                        <div class="row">
                            <div class="input-field col s6">
                                <i class="material-icons prefix">account_circle</i>
                                <input id="Full_Name" type="text" class="validate" name="name" value="<?=$get_cust->name;?>">
                                <label for="Full_Name">Full Name</label>
                            </div>
                            <div class="input-field col s6">
                                <i class="material-icons prefix">text_fields</i>
                                <input id="Proprieter" type="tel" class="validate" name="proprieter_name" value="<?=$get_cust->proprieter_name;?>">
                                <label for="Proprieter">Company Name</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <i class="material-icons prefix">email</i>
                                <input id="Email" type="text" class="validate" name="email" value="<?=$get_cust->email;?>">
                                <label for="Email">Email</label>
                            </div>
                            <div class="input-field col s6">
                                <i class="material-icons prefix">phone</i>
                                <input id="Contact" type="tel" class="validate valid_no" name="contact_no" value="<?=$get_cust->contact_no;?>">
                                <label for="Contact">Contact No.</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <i class="material-icons prefix">verified_user</i>
                                <input id="CID" type="text" class="validate" name="cid" value="<?=$get_cust->cid;?>">
                                <label for="CID">CID</label>
                            </div>
                            <div class="input-field col s6">
                                <i class="material-icons prefix">view_headline</i>
                                <select name="customer_category" class="cc1"> customer_category
                                    <?php
                                    foreach ($customer_category as $category){
                                        $s_id = $category->id;
                                        $old_cid = $get_cust->customer_category;
                                        ?>
                                        <option <?=($s_id==$old_cid)?'selected':'';?>  value="<?=$s_id;?>"><?=$category->type?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="Category">Category</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <i class="material-icons prefix">view_quilt</i>
                                <select name="type" class="validate">
                                    <?php
                                    foreach ($customer_category as $type){
                                        $t_id = $type->id;
                                        $old_tid = $get_cust->type;
                                        ?>
                                        <option <?=($t_id==$old_tid)?'selected':'';?>   value="<?=$s_id;?>"><?=$type->type?></option>
                                        <?php
                                    }
                                    ?>
                                </select>

                                <label for="Type">Type</label>
                            </div>
                            <div class="input-field col s6">
                                <i class="material-icons prefix">add_location</i>
                                <input id="Address" type="tel" class="validate" name="postal_address" value="<?=$get_cust->postal_address;?>">
                                <label for="Address">Postal Address</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <i class="material-icons prefix">add_location</i>

                                <select  class="state_list" name="state"  style="width: 100%" required>
                                    <?php
                                    foreach ($states as $state){
                                        $s_id = $state->id;
                                        $old_sid = $get_cust->state;
                                        ?>
                                        <option <?=($s_id==$old_sid)?'selected':'';?>  value="<?=$s_id;?>"><?=$state->name?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="State">State</label>
                            </div>
                            <div class="input-field col s6">
                                <i class="material-icons prefix">add_location</i>
                                <select  class="dist_list" name="district" style="width: 100%" required>

                                    <?php
                                    foreach ($s_districts as $sdist){
                                        $d_id = $sdist->id;
                                        $old_did = $get_cust->district;
                                        ?>
                                        <option <?=($d_id==$old_did)?'selected':'';?>  value="<?=$d_id;?>"><?=$sdist->name?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="District">District</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <i class="material-icons prefix">security</i>
                                <?php $mactive = $get_cust->msg_active; ?>
                                <select name="msg_active">
                                    <option <?=($mactive==0)?'selected':'';?> value="0"> Yes</option>
                                    <option <?=($mactive==1)?'selected':'';?> value="1"> No</option>
                                </select>
                                <label for="State">Message Active</label>
                            </div>
                            <div class="input-field col s6">
                                <i class="material-icons prefix">security</i>
                                <select name="msg_class">
                                    <?php
                                    foreach ($customer_class as $class){
                                        $m_id = $class->id;
                                        $get_mid = $get_cust->class;
                                        ?>
                                        <option <?=($m_id==$get_mid)?'selected':'';?> value="<?=$m_id;?>"><?=$class->type?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="msg_class">Customer Class</label>
                            </div>

                            <div class="row category_price">
                                <?php
                                $cpc = $get_cust->product_category;
                                $ct = $get_cust->customer_category;
                                if($cpc == '' || $cpc == '0'){

                                }
                                else{
                                    $pcid = explode(',',$get_cust->product_category);
                                    $price = explode(',',$get_cust->price);

                                    $i = 0;
                                    foreach ($pcid as $p){
                                        $data2  = DB::select("SELECT * from category  WHERE id = '$p'")[0];
                                        $name = $data2->name;
                                        $id = $data2->id;
                                        ?>
                                        <div class="input-field col s3">
                                            <input  type="hidden"  name="pcid[]" value="<?=$id?>">
                                            <input  type="text" disabled value="<?=$name?>">
                                            <input  name="price[]" type="text"  value="<?=$price[$i]?>">
                                        </div>
                                        <?php
                                        $i++;
                                    }

                                }
                                //die;
                                ?>


                            </div>
                            <input type="text" name="selected_category" id="selected_category" value="">
                            <button class="btn myblue waves-light right edit_submit" type="submit" name="edit">EDIT
                                <i class="material-icons right">save</i>
                            </button>
                        </div>

                    </form>
                <?php } ?>
            </div>
        </div>
    </div>
</div>




<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.js"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/select2.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/ui-alerts.js" type="text/javascript"></script>

<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAB5amm0dLJi65cbZSQSGM3dZn4ctnO22Q&callback=initMap"></script>



<!-- BEGIN PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/scripts/dashboard-modern.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/scripts/form-elements.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<?php echo $footer ?>
</body>
</html>
<script>
    $(document).ready(function(){
        $(function() {
            $('#lead_table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "<?=url("crm/get-leads_order-data") ?>",
                    type: 'GET',
                    data: function (d) {
                        d.category = $('#category').val();
                        d.state = $('#states').val();
                        d.district = $('#district').val();
                        d.cust_class = $('#customer_class').val();
                        d.type = $('#type').val();
                        d.verified = $('#verified').val();
                        d.pro_category = $('#pro_category').val();
                        d.pipeline = $('#pipeline').val();
                        d.stage = $('#pipeline_stage').val();
                        d.date_start1 = $('#date_start1').val();
                        d.date_end1 = $('#date_end1').val();
                        d.user = $('#user').val();

                    }
                },
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'sec_names', name: 'sec_names',visible: false },
                    { data: 'category', name: 'category' },
                    { data: 'state_name', name: 'state_name', },
                    { data: 'district_name', name: 'district_name',searchable: false },
                    { data: 'pipelines_id', name: 'pipelines_id',searchable: false  },
                    { data: 'pipelines_stage', name: 'pipelines_stage',searchable: false  },
                    { data: 'select_date', name: 'select_date',searchable: false },
                    { data: 'edited_by', name: 'edited_by',searchable: false  },
                    { data: 'created_date', name: 'created_date',searchable: false  },
                    { data: 'action', name: 'action', orderable: false,searchable: false }
                ],
                order: [[0, 'desc']],
                dom: 'lBfrtip',
                buttons: <?=$buttons;?>,
                fixedColumns: true,
                colReorder: true,
                exportOptions:{
                    columns: ':visible'
                }
            });
        });
    });
    //    getMapData(map);
    $('#filterButton').click(function(){

        $('#lead_table').DataTable().draw(true);
//        var datax = false;
//        var cate = $('#category').val();
//        var states = $('#states').val();
//        alert(cate);
//        var districts = $('#district').val();
//        var customer_classes = $('#customer_class').val();
//        var types = $('#type').val();
//        $.ajax({
//            url: '/customers-map',
//            type: 'POST',
////            data:{_token:<?////=csrf_token()?>//// ,cate:cate},
//            data: '_token=<?//=csrf_token()?>//&cate='+cate+'&states='+states,
//            success: function(data){
//                alert(cate);
//
//            }
//        });

    });

    /*$('#map_report').click(function(){

//        alert('hello');
        var datax = false;
        var cate = $('#category').val();
        var states = $('#states').val();
//        alert(cate);
        var districts = $('#district').val();
        var customer_classes = $('#customer_class').val();
        var types = $('#type').val();
        $.ajax({
            url: 'customers-map',
            type: 'POST',
//            data:{_token://=csrf_token()?>// ,cate:cate},
            data: '_token=&cate='+cate+'&states='+states,
            success: function(data){
//                alert('hello');

            }
        });

    });*/


    function getMapData(map){
        var datax = false;
        var category = $('#category').val();
        var state = $('#states').val();
        var district = $('#district').val();
        var customer_class = $('#customer_class').val();
        var type = $('#type').val();
        $.ajax({
            url: 'get-visits-map',
            type: 'POST',
            data: '_token=<?=csrf_token()?>&category='+category+'&state='+state+'&district='+district+'&customer_class='+customer_class+'&type='+type,
            success: function(data){
                datax = JSON.parse(data);
//                alert(datax);
                cdata = map.config.data;
                cdata.datasets[0].data = datax.cdata;
                cdata.labels = datax.labels;
                map.update();
            }
        });
    }
</script>
<script>
    $(document).ready(function(){
        $('#table').attr('style', 'width: 100%');
        $('.user').select2();
        $('.category').select2();
        $('.states').select2();
        $('.stage').select2();
        $('.district').select2();
        $('.customer_class').select2();
        $('.type').select2();
        $('.pstype').select2();
        $('.supplier').select2();
        $('.brand').select2();
        $('.pro_category').select2();
        $(".states").change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-district-ajax",
                data:'cid='+val+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(".submit").html("Wait...");
                },
                success: function(data){
                    $(".district").html(data);
                    $(".submit").html("Search <i class='material-icons '>search</i>");
                }
            });
        });
        $('.ptype').change(function () {
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-subtype",
                data:'tid='+val+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $('.pstype').html(data);
                }
            });
        });
        $(".stagebyname").change(function(){

            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-stages-ajax",
                data:'pid='+val+'&_token=<?=csrf_token(); ?>',
                success: function(data){

                    $(".stage").html(data).append('<option value="" disabled selected>Select Stage </option>');
                }
            });
        });
    });
    var startDate1 = moment('<?=$start_date1?>').format('DD-MM-YYYY HH:mm:ss');
    var endDate1 = moment('<?=$end_date1?>').format('DD-MM-YYYY HH:mm:ss');
    $('#date_range1').daterangepicker({
        useCurrent: true,
        opens: 'left',
        locale: {
            format: 'DD-MM-YYYY'
        },
        startDate: startDate1,
        endDate: endDate1
    },  function(start1, end1, label) {
        $('#date_start1').val(start1.format('DD-MM-YYYY'));
        $('#date_end1').val(end1.format('DD-MM-YYYY'));

    });
</script>

<script>
    function initMap()
    {

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 8,
            center:   {lat: 21.6434468, lng: 80.2424126}
        });

        // Create an array of alphabetical characters used to label the markers.
//        var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        // Add some markers to the map.
        // Note: The code uses the JavaScript Array.prototype.map() method to
        // create an array of markers based on a given "locations" array.
        // The map() method here has nothing to do with the Google Maps API.
        var markers = locations.map(function(location, i) {
            return new google.maps.Marker({
                position: location,

            });
        });

        // Add a marker clusterer to manage the markers.
        var markerCluster = new MarkerClusterer(map, markers,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
    }
    var locations = [<?=implode(',', $location);?>]


</script>