<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="ThemeSelect">
    <title><?=$title; ?></title>
    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?=$tp; ?>/vendors/noUiSlider/nouislider.min.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/select.dataTables.min.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">

    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />
    <!--For Spreadsheet-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/Spreadsheet/styles.css">

    <!-- END: Custom CSS-->
    <style>
        table, td, th, tr{
            height: 0rem !important;
            font-size: 0.8rem !important;
        }
        .spreadsheet .select2-container--default .select2-selection--multiple .select2-selection__rendered{
            max-height: 100%;
        }
        .spreadsheet .select2-container--default.select2-container--focus .select2-selection--multiple{
            border: 1px solid gainsboro;
        }
        .spreadsheet .select2-container{
            height: 100%;
        }
        .spreadsheet td{
            max-width: 500px;
            padding: 0px;
        }
        .spreadsheet .select2-container--default .select2-selection--multiple{
            border:0px;
        }
        .spreadsheet .select2-container--default .select2-selection--single{
            border-bottom: none;
        }
       
    </style>
    <style>
        .modal-window {
            position: fixed;
            background-color: rgba(200, 200, 200, 0.75);
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 999;
            opacity: 0;
            pointer-events: none;
            -webkit-transition: all 0.3s;
            -moz-transition: all 0.3s;
            transition: all 0.3s;
        }

        .modal-window:target {
            opacity: 1;
            pointer-events: auto;
        }

        .modal-window>div {
            position: relative;
            padding: 2px 0;
            background: #fff;
            color: #444;
            max-width: 500px;
            margin: 0px auto;
        }

        .modal-window header {
            font-weight: bold;
        }

        .modal-close {
            color: #aaa;
            line-height: 30px;
            font-size: 80%;
            position: absolute;
            right: 0;
            text-align: center;
            top: 0;
            width: 70px;
            text-decoration: none;
        }

        .modal-close:hover {
            color: #000;
        }

        .modal-window h1 {
            font-size: 100%;
            margin: 5px;
        }
        #map{
            height: calc(100vh - 100px);
            width: 100%;
        }
        .referbyclass .select-wrapper input.select-dropdown{
            display: none;
        }

    </style>
    <!-- END: Custom CSS-->
</head>

<!-- END: Head-->
<?=$header;?>
<div class="row">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" >
                <?=$notices;?>
            <h5 class="card-title">Edit Tranportor Details </h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="javascript:history.go(-1)" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
                <?php
                $permission = checkRole($user->u_id,"tran_veri");
                ?>
                <h5><?=$transportor->name." [ ".$transportor->company_name." ] ";?>
                    <?php if($permission && $transportor->verified == 0)
                    { echo '<a href="verify-tronsportor?edit='.$_GET['cid'].'&action=1" class="btn myblue waves-light"><i class="material-icons left" style="margin-right: 5px">check</i> Verify</a>'; } if($transportor->verified == 1){ echo '<span style="font-size: 16px;color: blue"><i class="material-icons" style="font-size: 16px;color: blue;">verified_user</i> Verified</span>';if($permission) { echo '&nbsp;<a href="verify-customer?edit='.$_GET['cid'].'&action=0" class="btn myblue waves-light">Un-Verify</a>';} }?>
                        </h5>

                        <div class="col s12 m12 l12">
                        <ul class="row tabs" >
                            <li class="tab col s5 m5 xl1"><a class="active p-0" href="#Detail">Detail</a></li>
                            <li class="tab col s5 m5 xl1"><a class="p-0" href="#Root">Root</a></li>
                            <li class="tab col s5 m5 xl1"><a class="p-0" href="#Issue">Issue</a></li>
                        </ul>
                    </div>
                <div id="Detail" class="col-lg-12 col-xs-12">
                    <div class="divider"></div>
                    <div class="visits_form">
                     <form method="post" action="transportor_order-edit" class="col s12 m12">
                        <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                         <input type="hidden" name="tid" value="<?php echo  $_GET['cid']; ?>">
                         <?php $editnames = explode(',',$transportor->name);
                         $editconts = explode(',',$transportor->contact_no);
                         $editlandline = explode(',',$transportor->landline);
                         $editcondes = explode(',',$transportor->contact_designation);
                         ?>
                        <div class="row">
                            <div class="input-field col s6 m6">
                                <i class="material-icons prefix">list</i>
                                <input id="Full_Name" type="text" class="validate" name="name" <?php if(!empty($editnames[0])){ ?> value="<?= $editnames[0]; } ?>">
                                <label for="Full_Name">Name</label>
                            </div>
                            <div class="input-field col s6 m6">
                                <i class="material-icons prefix">list</i>
                                <input id="Company_name" type="text" class="validate" name="cname" value="<?=$transportor->company_name?>">
                                <label for="Company_name">Company Name</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s6 m6">
                                <div class="col s1 m1" style="text-align: center;margin-top: 30px;margin-left: -15px;"> <i class="material-icons prefix">flag</i></div>
                                <div class="col s11 m11">
                                    <label>Select State</label>
                                <select class="browser-default states" id="states" name="states" style="width: 510px;">
                                    <?php if(!empty($states)){
                                        foreach ($states as $state) {
                                            $v_id = $state->id;
                                            ?>
                                            <option <?php if($transportor->state == $v_id){echo "selected";} ?> value="<?=$v_id; ?>"><?= $state->name ?></option>
                                            <?php
                                        } }
                                    ?>
                                </select>
                                </div>
                            </div>
                            <div class="col s6 m6">
                                <div class="col s1 m1" style="text-align: center;margin-top: 30px;margin-left: -15px;">
                                    <i class="material-icons prefix">adjust</i>
                                </div>
                                <div class="col s11 m11">
                                    <label>Select District</label>
                                    <select class="browser-default district" id="district" name="district" style="width: 510px;">
                                        <?php $master_district = DB::table('district')->where('state_id', $transportor->state)->first();
                                        if(!empty($master_district)){ ?>
                                            <option value="<?= $master_district->id?>"><?= $master_district->name?></option>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6 m6">
                                <i class="material-icons prefix">list</i>
                                <input id="Company_Address" type="text" class="validate" name="caddress" value="<?=$transportor->company_address?>">
                                <label for="Company_Address">Company Address</label>
                            </div>
                            <div class="input-field col s6 m6">
                                <i class="material-icons prefix">list</i>
                                <input id="contact_number" type="text" class="contact_number" name="contact_number" <?php if(!empty($editconts[0])) { ?> value="<?= $editconts[0]; } ?>">
                                <label for="contact_number">Contact Number</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6 m6">
                                <i class="material-icons prefix">list</i>
                                <select id="Designation" class="desc" name="designation">
                                    <?php foreach ($crm_contax as $desc): ?>' +
                                        <option <?php if($desc->id == 1){ echo  'selected'; } ?> value="<?php echo $desc->id; ?>" ><?php echo $desc->name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <label for="Company_Address">Contact Designation</label>
                            </div>
                            <div class="input-field col s6 m6">
                                <i class="material-icons prefix">list</i>
                                <input id="landline" type="text" class="landline" name="landline" <?php if(!empty($editlandline[0])){?> value="<?= $editlandline[0]; } ?>">
                                <label for="landline">Landline Number</label>
                            </div>
                        </div>

                         <?php $getCust = Session::get('customers');?>
                        <div class="row">
                            <div class="col s6 m6">
                                <div class="col s1 m1" style="text-align: center;margin-top: 30px;margin-left: -15px;"> <i class="material-icons prefix">list</i></div>
                                <div class="col s11 m11 referbyclass">
                                    <label>Refereed By</label>
                                    <select class="selectjs" multiple="" style="display:none" name="listcust[]" id="selectjs">
                                        @if(!empty($crmreffer))
                                            @foreach($crmreffer as $reffered)
                                                <option value="{{$reffered->id}}"  selected="selected" > {{$reffered->name}} </option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                         <!--More Options-->
                         <?php if(!empty($transportor->sec_names)){
                             $sec_names = unserialize($transportor->sec_names);
                             foreach ($sec_names as $key=>$edname){ ?>
                                 <div class="input-field col s6 m6">
                                     <i class="material-icons prefix">account_circle</i>
                                     <input autocomplete="off" class="validate" id="names'+indexs+'" name="names['+$i+']" type="text" value="<?= $edname?>" placeholder="Full Name"/>
                                 </div>
                                 <div class="input-field col s6 m6">
                                     <i class="material-icons prefix">contact_mail</i>
                                     <input autocomplete="off" class="validate" id="emails'+indexs+'" name="emails['+$i+']" type="email" value="<?= $transportor->email?>" placeholder="Email"/>
                                 </div>
                                 <div class="input-field col s6 m6">
                                     <i class="material-icons prefix">contact_phone</i>
                                     <input autocomplete="off" class="validate valid_no" id="contacts'+indexs+'" name="contacts['+$i+']" type="text" <?php if(!empty($editconts[$key])) { ?> value="<?= $editconts[$key]; }?>" onblur="validateContact(contacts'+indexs+')" placeholder="Contact No."/>
                                 </div>
                                 <div class="input-field col s6 m6">
                                     <i class="material-icons prefix">contact_phones</i>
                                     <input autocomplete="off" class="validate" id="landlines'+indexs+'" name="landlines['+$i+']" type="tel" <?php if(!empty($editlandline[$key])){ ?> value="<?= $editlandline[$key]?>" <?php } ?> placeholder="Landline No" />
                                 </div>
                                 <div class="input-field col s6 m6"><i class="material-icons prefix">contact_mail</i>
                                     <select class="browser-default getDesignation" id="desc'+indexs+'" name="descs['+$i+']" style="margin-left: 25px">
                                         <option>Select Designation</option>
                                         <?php foreach ($crm_contax as $desc): ?>
                                             <option <?php if(!empty($editcondes[$key]) && $editcondes[$key] == $desc->id){ echo "selected"; }?> value="<?php echo $desc->id; ?>" ><?php echo $desc->name; ?></option>
                                         <?php endforeach; ?>
                                     </select></div>
                                 <div class="input-field col s6 m6">
                                     <center><button type="button" onclick="removemoreBox('+indexs+')" class="btn btn-sm btn-danger">Remove<i class="fa fa-times" style="margin-left:10px"></i>
                                         </button></center></div><div class="clearfix">
                                 </div></br></br>
                             <?php }}?>
                         <!--End More Options-->
                        <div class="clearfix">

                        </div><br><br>
                        <div id="insertmoreBefore"></div>
                        <div class="clearfix"></div>
                        <button type="button" id="moreButton" class="btn btn-sm btn-info" style="margin-bottom: 20px">
                            Add More <i class="fa fa-plus"></i>
                        </button>
                         <div class="col s12 m12 " style="text-align:center;margin-top: 20px;margin-bottom: 20px;" >
                             <button class="btn myblue waves-light add_submit"  style="padding:0 5px;" type="submit" name="add">SAVE
                                 <i class="material-icons right">save</i>
                             </button>
                         </div>
                    </form>
                    </div>
                </div>
                <div id="Root" class="col-lg-12 col-xs-12" style="display: none">
                    <div class="divider"></div>
                    <div class="Message_form">
                        <form method="post" action="transportor_order-edit?cid=<?php echo  $_GET['cid']; ?>" class="col s12 m12">
                            <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                            <input type="hidden" name="tid" value="<?php echo  $_GET['cid']; ?>">
                            <!--SpreadSheet Start-->
                            <section class="spreadsheet">
                                <table class="spreadsheet__table"
                                       id="table-main">
                                    <thead>
                                        <th style="border-bottom: hidden"></th>
                                        <th colspan="2"  style="text-align: center;border-right-style: solid;border-right-width: 0.5rem; border-right-color: rgb(0 0 0 / 49%);">Root Start</th>
                                        <th colspan="2"  style="text-align: center">Root End</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </thead>
                                    <thead>
                                        <th style="text-align: center">S.No.</th>
                                        <th  style="text-align: center">State</th>
                                        <th  style="text-align: center;border-right-style: solid;border-right-width: 0.5rem; border-right-color: rgb(0 0 0 / 49%);">District</th>
                                         <th  style="text-align: center">State</th>
                                        <th  style="text-align: center;">District</th>
                                        <th  style="text-align: center;border-top: hidden;">Truck Load</th>
                                        <th  style="text-align: center;border-top: hidden">Freight</th>
                                        <th  style="text-align: center;border-top: hidden">Delete</th>
                                    </thead>
                                    <tbody class="spreadsheet__table--body"id="table-body">

                                  <?php   if(empty($transroots)) {
                                      ?>
                                      <input type="hidden" id="tatalrow"   value="1" >
                                    <tr>
                                        <td data-id="1" style="text-align: center">1</td>
                                        <td>
                                            <select class="browser-default ststates" id="ststates" name="ststates[]"    style="width:100% !important;">
                                                <?php if(!empty($states)){
                                                    foreach ($states as $state) {
                                                        $v_id = $state->id;
                                                        ?>
                                                        <option  value="<?=$v_id; ?>"><?= $state->name ?></option>
                                                        <?php
                                                    } }
                                                ?>
                                            </select>
                                        </td>
                                        <td style="text-align: center;border-right-style: solid;border-right-width: 0.5rem; border-right-color: rgb(0 0 0 / 49%);">
                                            <select class="browser-default stdistrict" id="stdistrict" name="stdistrict[]" style="width:100% !important;">
                                                <?php if(!empty($districts)){foreach($districts as $singledata) {?>
                                                    <option   value="<?= $singledata->id ?>"><?= $singledata->name; ?></option>
                                                <?php } } ?>
                                            </select></td>
                                        <td><select class="browser-default edstates" id="edstates" name="edstates[]" style="width:100% !important;">
                                                <?php if(!empty($states)){
                                                    foreach ($states as $state) {
                                                        $v_id = $state->id;
                                                        ?>
                                                        <option  value="<?=$v_id; ?>"><?= $state->name ?></option>
                                                        <?php
                                                    } }
                                                ?>
                                            </select></td>
                                        <td><select class="browser-default eddistrict" id="eddistrict" name="eddistrict[]" style="width:100% !important;">
                                                <?php if(!empty($districts)){foreach($districts as $singledata) {?>
                                                    <option  value="<?= $singledata->id ?>"><?= $singledata->name; ?></option>
                                                <?php } } ?>
                                            </select></td>
                                        <td><select class="browser-default truck_load" id="truck_load" name="truck_load[]" multiple  tabindex="-1" style="width:100% !important;">
                                                <?php if(!empty($truckloads)){
                                                foreach ($truckloads as $truckload) {
                                                $v_id = $truckload->id;
                                                ?>
                                                <option  value="<?=$truckload->load; ?>"><?= $truckload->load ?></option>
                                                <?php
                                                } }
                                                ?>
                                            </select></td>
                                        <td><input type="text" name="freight[]" value="" style="border-bottom: none;"></td>

                                    </tr>
                                  <?php } ?>
                                          {{--Truck Loads--}}
                                  <?php $i=0; if(!empty($transroots)) {  ?>
                                        <input type="hidden" id="tatalrow"   value="{{count($transroots)}}" >
                                        <input type="hidden" id="trans_id"   value="{{isset($_GET['cid'])?$_GET['cid']:''}}" >
                                    <?php foreach ($transroots as $key=>$transroot){  $i ++; ?>
                                        <input type="hidden" id="root_id" name="root_id[<?=$key+1;?>]" value="{{$transroot->id}}" >
                                            <tr>
                                                <td data-id="<?=$i;?>" style="text-align: center;min-width: auto"><?=$i;?></td>
                                                <td>
                                                    <select class="browser-default states" id="ststates<?=$key+1;?>" name="ststates[<?=$key+1;?>]" onchange="getexststate(this,'<?=$key+1;?>');" style="width:100% !important;">
                                                        <?php if(!empty($transroot->ststate)){
                                                            foreach ($states as $state) {
                                                                $v_id = $state->id;
                                                                ?>
                                                                <option <?php  if((is_array($transroot->ststate) && in_array($state->id,$transroot->ststate)) || (!is_array($transroot->ststate) && $transroot->ststate == $state->id)){ echo  'selected'; } ?> value="<?=$v_id; ?>"><?= $state->name ?></option>
                                                                <?php
                                                            } }
                                                        ?>
                                                    </select>
                                                </td>

                                                <td style="text-align: center;border-right-style: solid;border-right-width: 0.5rem; border-right-color: rgb(0 0 0 / 49%);">
                                                    <select class="browser-default stdistrict" id="stdistrict<?=$key+1;?>" name="stdistrict[<?=$key+1;?>]" style="width:100% !important;">
                                                        <?php if(!empty($transroot->stdistrict)){foreach($districts as $singledata) {?>
                                                            <option  <?php if((is_array($transroot->stdistrict) && in_array($singledata->id,$transroot->stdistrict)) || (!is_array($transroot->stdistrict) && $transroot->stdistrict == $singledata->id)){ echo  'selected'; } ?> value="<?= $singledata->id ?>"><?= $singledata->name; ?></option>
                                                        <?php } } ?>
                                                    </select></td>
                                                <td><select class="browser-default states" id="edstates<?=$key+1;?>" name="edstates[<?=$key+1;?>]" onchange="getexedstate(this,'<?=$key+1;?>')"style="width:100% !important;">
                                                        <?php if(!empty($transroot->edstate)){
                                                            foreach ($states as $state) {
                                                                $v_id = $state->id;
                                                                ?>
                                                                <option <?php if((is_array($transroot->edstate) && in_array($v_id,$transroot->edstate)) || (!is_array($transroot->edstate) && $transroot->edstate == $v_id)){ echo  'selected'; } ?> value="<?=$v_id; ?>"><?= $state->name ?></option>
                                                                <?php
                                                            } }
                                                        ?>
                                                    </select></td>
                                                <td><select class="browser-default eddistrict" id="eddistrict<?=$key+1;?>" name="eddistrict[<?=$key+1;?>]" style="width:100% !important;">
                                                        <?php if(!empty($transroot->eddistrict)){foreach($districts as $singledata) {?>
                                                            <option  <?php if((is_array($transroot->eddistrict) && in_array($singledata->id,$transroot->eddistrict)) || (!is_array($transroot->eddistrict) && $transroot->eddistrict == $singledata->id)){ echo  'selected'; } ?> value="<?= $singledata->id ?>"><?= $singledata->name; ?></option>
                                                        <?php } } ?>
                                                    </select></td>
                                                <td><select class="browser-default truck_load" id="truck_load<?=$key+1;?>" name="truck_load[<?=$key+1;?>]" multiple tabindex="-1" style="width:100% !important;">
                                                       @if(!empty($transroot->truckload)) @foreach (explode(',',$transroot->truckload) as $truckload)
                                                        <option  value="{{$truckload}}" selected>{{$truckload}}</option>
                                                        @endforeach @endif
                                                    </select></td>
                                                <td>
                                                    <div class="row">
                                                        <div class="col s8 m8">
                                                            <input type="text" name="freight[<?=$key+1;?>]"  value="<?php echo $transroot->freight; ?>" style="border-bottom: none;">
                                                        </div>
                                                        <div class="col s4 m4">
                                                            <a href="#open-modal3<?php echo $key+1;?>" class="btn myblue waves-light"  style="padding: 0px 7px;height: 30px;min-width: auto;">
                                                                <i class="material-icons small right">show_chart</i>
                                                            </a>
                                                            <!-- Modal -->
                                                            <div id="open-modal3<?php echo $key+1;?>" class="modal-window">
                                                                <div class="col m12" id="graph<?php echo $key+1;?>">
                                                                    <h4 class="card-title" style="margin-top: -10px">Freight Graph</h4>
                                                                    <a href="#modal-close" title="Close" class="modal-close" onclick="viewgraph(this,'<?php echo $key+1;?>')" style="color: #000;font-size: 13px;">close &times;</a>
                                                                    <div class="chart-container" style="position: initial; height:50vh;background: #fff">
                                                                        <canvas id="myChart"></canvas>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- End Modal -->
                                                        </div>
                                                        <div class="col s12 m12" style="margin-top: -36px;">
                                                            @php $dt = new DateTime($transroot->created_at);$dt = $dt->format('Y-m-d'); @endphp
                                                        <p>Last Updated:-{{$dt}}</p>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td style="min-width: auto;"><button value="{{$transroot->id}}" class="btn myred waves-light root_delete" style="padding: 0 7px; width:inherit; height: 31px" ><i class="material-icons small">delete</i></button></td>
                                            </tr>
                                        <?php  }  }?>
                                            <tr id="insertBefore"></tr>
                                    </tbody>
                                </table>
                            </section>
                            <button type="button" id="plusButton" class="btn btn-sm btn-info" style="margin-bottom: 20px">
                                Add More <i class="fa fa-plus"></i>
                            </button>
                            <div class="col s12 m12 " style="text-align:center;margin-top: 20px;margin-bottom: 20px;" >
                                <button class="btn myblue waves-light add_submit"  style="padding:0 5px;" type="submit" name="addroot">SAVE
                                    <i class="material-icons right">save</i>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="Issue" class="col-lg-12 col-xs-12" style="display: none">
                    <div class="divider"></div>
                    <div class="notes_form">
                        <form method="post" action="transportor_order-edit" enctype="multipart/form-data" class="col s12">
                            <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                            <input type="hidden" name="crm_cust_id" value="<?php echo  $transportor->id; ?>">

                            <div class="row">
                                <div class="input-field col s12 m6">
                                    <i class="material-icons prefix">watch_later</i>
                                    <input type="date" class="datepicker" id="dob1" name="select_date" >
                                    <label for="dob1">Date</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12 m12">
                                    <i class="material-icons prefix">view_headline</i>
                                    <input id="Description3" type="text" class="validate" name="description" required>
                                    <label for="Description3">Description</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12 m6">
                                    <div class="file-field">
                                        <div class="btn myblue waves-light left" style="padding:0 5px;">
                                            <span>Choose file</span>
                                            <input type="file" name="image" style="width: 100%" class="btn myblue" id="notes_image">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text" placeholder="Upload your file">
                                        </div>
                                    </div>
                                </div>
                                <div class="input-field col s12 m3">
                                    <p id="notes_img_div">
                                        <img id="notes_image_show" src="#" height="200px" width="200px"  />
                                    </p>
                                </div>

                                <div class="input-field col s12 m3" style="text-align: center">
                                    <button class="btn myblue waves-light" style="padding:0 5px;" type="submit" name="add_issue">Save
                                        <i class="material-icons right">save</i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="divider"></div>
                    <h6>All Issue</h6>
                    <div class="row">
                        <div class="col s12 table-responsive">
                            <table id="notes-table" class="display" style="width: 100%;">
                                <thead>
                                <tr role="row">
                                    <th>Sr.No.</th>
                                    <th>Date</th>
                                    <th>Description</th>
                                    <th>File</th>
                                    <th>edited_by</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

</div>

</div>

</div>
<!-- END: Footer-->
<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/vendors/data-tables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/js/dataTables.select.min.js" type="text/javascript"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/select2.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/ui-alerts.js" type="text/javascript"></script>

<script src="<?=$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script>
<!--For Spreadsheet-->
<script src="<?=$tp; ?>/Spreadsheet/main.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js" integrity="sha512-jNDtFf7qgU0eH/+Z42FG4fw3w7DM/9zbgNPe3wfJlCylVDTT3IgKW5r92Vy9IHa6U50vyMz5gRByIu4YIXFtaQ==" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script>
    var $insertBefore = $('#insertBefore');
    var i = Number($('#tatalrow').val());
    // Add More Inputs
    $('#plusButton').click(function(){
        i = Number(i+1);
        var indexs = i+1;
        //append Options
        $('<tr id="addMoreBox'+indexs+'" class="clearfix"><input type="hidden" id="root_id" name="root_id['+i+']" @if(!empty($transroot->id)) value="" @endif><td style="text-align: center">'+i+'</td><td>' +
            '<select class="browser-default ststates" id="ststates'+indexs+'"  name="ststates['+i+']" style="width:100% !important;"><option value="">Select State</option>' +
            '<?php foreach($states as $state):?>' +
            '<option value="<?=$state->id; ?>"><?= $state->name; ?></option>' +
            '<?php endforeach;?>' +
            '</select></td>' +
            '<td style="text-align: center;border-right-style: solid;border-right-width: 0.5rem; border-right-color: rgb(0 0 0 / 49%);">' +
            '<select class="browser-default stdistrict" id="stdistrict'+indexs+'"  name="stdistrict['+i+']" style="width:100% !important;"></select></td>' +
            '<td><select class="browser-default edstates" id="edstates'+indexs+'"  name="edstates['+i+']" style="width:100% !important;"><option value="">Select State</option>' +
            '<?php foreach($states as $state):?>' +
            '<option value="<?=$state->id; ?>"><?= $state->name; ?></option>' +
            '<?php endforeach;?>' +
            '</select></td>' +
            '<td><select class="browser-default eddistrict" id="eddistrict'+indexs+'"  name="eddistrict['+i+']" style="width:100% !important;"></select></td><td><select class="browser-default" id="truck_load'+indexs+'" name="truck_load['+i+']"  multiple tabindex="-1"  style="width:100% !important;">' +
            '@if(!empty($truckloads))
                    @foreach ($truckloads as $truckload)
                <option  value="{{$truckload->load}}">{{$truckload->load}}</option>@endforeach @endif'+
            '</select></td>' +
            '<td><input type="text" name="freight['+i+']" value="" style="border-bottom: none;"></td></tr>').insertBefore($insertBefore);

        $('#ststates'+indexs).select2();
        $('#edstates'+indexs).select2();
        $('#stdistrict'+indexs).select2();
        $('#eddistrict'+indexs).select2();
        $('#truck_load'+indexs).select2();
        // Remove fields
        function removeBox(index){
            $('#addMoreBox'+index).remove();
        }
        $("#ststates"+indexs).change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-district-ajax",
                data:'cid='+val+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(".submit").html("Wait...");
                },
                success: function(data){
                    $("#stdistrict"+indexs).html(data);
                    $(".submit").html("Search <i class='material-icons'>search</i>");
                }
            });
        });
        $("#edstates"+indexs).change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-district-ajax",
                data:'cid='+val+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(".submit").html("Wait...");
                },
                success: function(data){
                    $("#eddistrict"+indexs).html(data);
                    $(".submit").html("Search <i class='material-icons '>search</i>");
                }
            });
        });

    });



    var ctx = document.getElementById('myChart').getContext('2d');
    options = {
        responsive: true,
        maintainAspectRatio: false,
        aspectRatio: 5,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    };
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: [],
            datasets: [{
                label: 'Root Freight',
                backgroundColor: 'rgba(255, 99, 132, 0.60)',
                borderColor: 'rgb(255, 99, 132)',
                data: ['10','20'],
                borderWidth: 1
            }]
        },

        // Configuration options go here
        options: options
    });


</script>
<script>

    var $insertmoreBefore = $('#insertmoreBefore');
    var $i = 0;
    // Add More Inputs
    $('#moreButton').click(function(){

        $i = $i+1;
        var indexs = $i+1;
        //append Options
        $('<div id="addMore'+indexs+'" class="clearfix"> ' +
            '<div class="input-field col s6 m6"><i class="material-icons prefix">account_circle</i><input autocomplete="off" class="validate" id="names'+indexs+'" name="names['+$i+']" type="text" value="" placeholder="Full Name"/></div>' +
            '<div class="input-field col s6 m6"><i class="material-icons prefix">contact_mail</i><input autocomplete="off" class="validate" id="emails'+indexs+'" name="emails['+$i+']" type="email" value="" placeholder="Email"/></div>' +
            '<div class="input-field col s6 m6"><i class="material-icons prefix">contact_phone</i><input autocomplete="off" class="validate valid_no" id="contacts'+indexs+'" name="contacts['+$i+']" type="text" value="" onblur="validateContact(contacts'+indexs+')" placeholder="Contact No."/></div>' +
            '<div class="input-field col s6 m6"><i class="material-icons prefix">contact_phones</i><input autocomplete="off" class="validate" id="landlines'+indexs+'" name="landlines['+$i+']" type="tel" value="" placeholder="Landline No" pattern="[0-9]{4}-[0-9]{7}"/></div>' +
            '<div class="input-field col s6 m6"><i class="material-icons prefix">contact_mail</i>' +
            '<select class="browser-default getDesignation" id="desc'+indexs+'" name="descs['+$i+']" style="margin-left: 25px">' +
            '<option>Select Designation</option>' +
            '<?php foreach ($crm_contax as $desc): ?>' +
            '<option value="<?php echo $desc->id; ?>" ><?php echo $desc->name; ?></option>'+
            '<?php endforeach; ?>' +
            '</select></div>' +
            '<div class="input-field col s6 m6"><center><button type="button" onclick="removemoreBox('+indexs+')" class="btn btn-sm btn-danger">Remove<i class="fa fa-times" style="margin-left:10px"></i></button></center></div>' + '<div class="clearfix">\n' +
            '</div></br></br></div>').insertBefore($insertmoreBefore);
    });
    // Remove fields
    function removemoreBox(index){
        $('#addMore'+index).remove();
    }

    function getexststate(id,index){
        var val = id.value;
        $.ajax({
            type: "POST",
            url: "get-district-ajax",
            data:'cid='+val+'&_token=<?=csrf_token(); ?>',
            beforeSend: function(){
                $(".submit").html("Wait...");
            },
            success: function(data){
                $("#stdistrict"+index).html(data);
                $(".submit").html("Search <i class='material-icons '>search</i>");
            }
        });
    }
    function getexedstate(id,index){
        var val = id.value;
        $.ajax({
            type: "POST",
            url: "get-district-ajax",
            data:'cid='+val+'&_token=<?=csrf_token(); ?>',
            beforeSend: function(){
                $(".submit").html("Wait...");
            },
            success: function(data){
                $("#eddistrict"+index).html(data);
                $(".submit").html("Search <i class='material-icons '>search</i>");
            }
        });
    }
    function viewgraph(fid,id) {
        if($('#graph'+id).hasClass('show')) {
            $(this).html('<i class="material-icons right" style="margin-left:3px">show_chart</i>');
        }else{
            $(this).html('<i class="material-icons right" style="margin-left:3px">show_chart</i>');
        }
        $('#graph'+id).toggleClass('show');
    }

    $(document).on('click', '.root_delete', function () {
        var rid = $(this).val();
        var tid = $("#trans_id").val();
        $.ajax({
            type: "POST",
            url: "trans-load-delete",
            data:'rid='+rid+'&tid='+tid+'&_token=<?=csrf_token(); ?>',
            success: function(data){
                alert(data);
            }
        });
    });
</script>
<script>
    $(document).ready(function(){
        $('.user').select2();
        $('.pcat').select2();
        $('.states').select2();
        $('#ststates').select2();
        $('.edstates').select2();
        $('.stdistrict').select2();
        $('.eddistrict').select2();
        $('.district').select2();
        $('.referedByCust').select2();
        $('.truck_load').select2();

        $(".selectjs").select2({
            ajax: {
                url: "get-customers-listdata",
                dataType: 'json',
                delay: 250,
                type : "POST",
                data: function (params) {
                    return {
                        q: params.term, // search term
                        _token:'<?=csrf_token(); ?>',
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    return {
                        results: data,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            placeholder: 'Search for a repository',
            minimumInputLength: 1,
            multiple: true,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection
        });
        function formatRepo (repo) {
            return repo.name;
            var $container = $(
                "<div class='select2-result-repository__title'></div>"
            );

            $container.find(".select2-result-repository__title").text(repo.name);
            return $container;
        }

        function formatRepoSelection (repo) {
            return repo.name;

        }

        @if(!empty($crmreffer))
        @foreach($crmreffer as $reffered)
        $('.selectjs').select2().select('val','{{$reffered->id}}');
        @endforeach
        @endif

        $('.pcat').change(function () {
            var pcid = $(this).val();
            var ccid = $('.ccid').val();
            $.ajax({
                type: "POST",
                url: "get-price",
                data:'pcid='+pcid+'&ccid='+ccid+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $('.price').focus();
                    $('.price').val(data);

                }
            });
        });
        $(".states").change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-district-ajax",
                data:'cid='+val+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(".submit").html("Wait...");
                },
                success: function(data){
                    $(".district").html(data);
                    $(".submit").html("Search <i class='material-icons '>search</i>");
                }
            });
        });
        $("#ststates").change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-district-ajax",
                data:'cid='+val+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(".submit").html("Wait...");
                },
                success: function(data){
                    $(".stdistrict").html(data);
                    $(".submit").html("Search <i class='material-icons '>search</i>");
                }
            });
        });

        $("#edstates").change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-district-ajax",
                data:'cid='+val+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(".submit").html("Wait...");
                },
                success: function(data){
                    $(".eddistrict").html(data);
                    $(".submit").html("Search <i class='material-icons '>search</i>");
                }
            });
        });


    });


    $(function() {
        ntable = $('#notes-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?=url("crm/get-issue-data") ?>",
                type: 'GET',
                data: {cid:'<?=isset($_GET['cid']) ? $_GET['cid'] : ''?>'}
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'select_date', name: 'select_date' },
                { data: 'description', name: 'description'},
                { data: 'image', name: 'image', orderable: false, searchable: false },
                { data: 'edited_by', name: 'edited_by', searchable: false },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });
    });

    $(document).on('click', '.idelete', function () {
        var pid = $(this).val();
        var tid = $(".cust_id").val();
        var a = $(this);

        $.ajax({
            type: "POST",
            url: "trans-issue-delete",
            data:'pid='+pid+'&tid='+tid+'&_token=<?=csrf_token(); ?>',
            beforeSend: function(){
                $(a).html('Wait...');
            },
            success: function(data){
                alert(data);
                if(ntable){
                    $('#notes-table').DataTable().ajax.reload();
                }else{
                    location.reload();
                }
            }
        });
    });

</script>
<?php echo $footer ?>
</body>
</html>