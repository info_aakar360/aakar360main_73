<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="ThemeSelect">
    <title>
        <?=$title; ?>
    </title>
    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/select.dataTables.min.css">

    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />
    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <!-- END: Custom CSS-->
</head>

<style type="text/css">

    .modal-window {
        position: fixed;
        background-color: rgba(200, 200, 200, 0.75);
        top: -70px;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 999;
        opacity: 0;
        pointer-events: none;
        -webkit-transition: all 0.3s;
        -moz-transition: all 0.3s;
        transition: all 0.3s;
    }

    .modal-window:target {
        opacity: 1;
        pointer-events: auto;
    }

    .modal-window>div {
        width: 800px;
        position: relative;
        margin: 10% auto;
        padding: 2rem;
        background: #fff;
        color: #444;
    }

    .modal-window header {
        font-weight: bold;
    }

    .modal-close {
        color: #aaa;
        line-height: 50px;
        font-size: 80%;
        position: absolute;
        right: 0;
        text-align: center;
        top: 0;
        width: 70px;
        text-decoration: none;
    }

    .modal-close:hover {
        color: #000;
    }

    .modal-window h1 {
        font-size: 150%;
        margin: 0 0 15px;
    }
    table th, td{
        border:1px solid #d8d3d8;
        padding-left: 15px;
    }
    .tableFixHead {
        overflow: auto;
        height: 560px;
        width:100%
    }

    td:first-child, th:first-child {
        position:sticky;
        left:0;
        z-index:1;
        background-color:white;
    }
    td:nth-child(2),th:nth-child(2)  {
        position:sticky;
        left:40px;
        z-index:1;
        background-color:white;
    }
    .tableFixHead th {
        position: sticky;
        top: 0;
        background: #ffffff;
        z-index:2
    }
    th:first-child , th:nth-child(2) {
        z-index:3

    }
    .pipeline-box{
        box-shadow: 3px 1px 6px 2px #757574;
        position: relative;
        margin-bottom: 15px;
        background: linear-gradient(45deg, rgba(255, 255, 255, 0.02), rgba(204, 204, 204, 0.03)) !important;
        border-radius: 15px;
        transition: all 0.3s ease;
        overflow: hidden;
    }

</style>
<!-- END: Head-->
<?=$header;?>

<div class="row">
    <div class="col s12">
        <div  class="card card-tabs">
            <div class="card-content">
                <div class="row">
                    <div class="col s4 m4">
                        <button class="btn btn-primary center">Add New Sales Pipeline</button>
                    </div>
                </div>


                <?php if(!empty($salespipelines)) {

                    foreach ($salespipelines as $pipeline) {

                        ?>

                        <div class="col-md-4 pipeline-box">
                            <div class="col s12 m12">
                                <div class="row">
                                    <div class="col s8 m8">
                                        <h6 style="margin-top: 50px;">Pipeline Name</h6>
                                    </div>
                                    <div class="col s4 m4">
                                        <h6 style="margin-top: 50px;">Action</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s8 m8">
                                        <input type="text" name="name<?php echo $pipeline->id ?>" class="form-control" id="name<?php echo $pipeline->id ?>" value="<?php echo $pipeline->name ?>">
                                    </div>
                                    <div class="col s4 m4" style="display: flex">
                                        <button class="btn myblue waves-light" style="padding:0 10px;" onclick="editTable('<?php echo $pipeline->id ?>');"><i class="material-icons">edit</i></button>
                                        <button id="deletepipeline" class="btn myred waves-light" style="padding:0 10px;" onclick="deleteTable('<?php echo $pipeline->id ?>');"><i class="material-icons">delete</i></button>
                                        <button   class="btn myblue waves-light" style="padding:0 10px;" onclick="updateTable('<?php echo $pipeline->id ?>');"><i class="material-icons right">save</i></button>
                                    </div>
                                </div>

                            </div>
                            <input type="hidden" name = "pipe_line_id" class="form-control id" value="<?php echo $pipeline->id ?>">
                            <table class="table">
                                <tbody>
                                <tr class="success">
                                    <td id="new">New</td>
                                    <td ></td>
                                </tr>
                                </tbody>
                            </table>
                            <table class="table" id="makeEditable<?php echo $pipeline->id ?>" style="margin-top: -15px">

                                <tbody>

                                <?php
                                if(!empty($pipeline)){

                                    $pipeline_stages = \Illuminate\Support\Arr::pluck(DB::table('pipeline_stages')->where('pipeline_id',$pipeline->id)->get(),'id','stage_name');

                                    foreach($pipeline_stages as $key=>$stage){

                                        ?>
                                        <tr class="success">
                                            <td data-id="<?= $stage ?>"><?php echo $key; ?></td>
                                            <td ><button id="deletepipeline" class="btn myred waves-light" style="padding:0 10px;" onclick="rowDelete('<?= $stage ?>','<?= $key ?>','<?= $pipeline->id ?>')"><i class="material-icons">delete</i></button></td>
                                        </tr>

                                    <?php } }?>
                                </tbody>
                                </tbody>
                            </table>

                            <table class="table" style="margin-top: -15px">
                                <tbody>
                                <tr class="success">
                                    <td id="won">Won</td>
                                    <td ></td>
                                </tr>
                                <tr class="success">
                                    <td id="lost">Lost</td>
                                    <td ></td>
                                </tr>
                                </tbody>
                            </table>
                            <span>
                            <button id="plusButton<?php echo $pipeline->id ?>" onclick="addMore('<?php echo $pipeline->id ?>')">Add Deal Stage</button>
                            </span>
                            <input type="checkbox" id="addDefault" name="addDefault" value="">
                            <label for="addDefault">Marked as Default</label>
                        </div>
                    <?php } } ?>
                <div id="newPipeline" class="col s4 m4 sales_div pipeline-box" style="display: none;"></div>
            </div>
        </div>
    </div>
</div>


<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/data-tables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/js/dataTables.select.min.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/select2.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/ui-alerts.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/bootstable.js" type="text/javascript"></script>
<script>
    /*$('.id').each(function () {
        jsonObj =  [];
        var id= $(this).val();
        $('#makeEditable'+id).SetEditable({
            $addButton: $('#but_add'+id),
            onDelete:function () {
                jsonObj.push($('#row').html());
                console.log(jsonObj);
                $.ajax({
                    type: "POST",
                    url: "update-delete-pipeline",
                    data:'id='+id+'&_token='+'&stages='+jsonObj,
                    success: function(data){

                    }
                });
            }
        });


    });*/

    $(".center").click(function () {
        $("#newPipeline").show();
        $.ajax({
            type: "POST",
            url: "add-default-sales-pipeline",
            data: '_token=<?=csrf_token()?>',
            beforeSend: function(){
                $("#search-box").css("background","#FFF url(assets/LoaderIcon.gif) no-repeat 165px");
            },
            success: function(data){
                $(".sales_div").html(data);
            }
        });
    });
    /*Edit Pipeline*/
    function editTable(id) {
        $("#makeEditable"+id).attr('contenteditable','true');
    }
    /*End Edit Pipeline*/

    /*Update Pipeline*/
    function updateTable(id) {
        jsonObj = [];
        jsonObjrow = [];
        $('#makeEditable'+id+'  tr').each(function() {

            var rowid = $(this).find("td").attr('data-id');
            var stages = $(this).find("td").html();

            jsonObj.push({id:rowid,stages:stages});


        });
        var jsonstring = JSON.stringify(jsonObj)

        var name = $("#name"+id).val();
        $.ajax({
            type: "POST",
            url: "update-pipeline",
            data:'id='+id+'&_token=<?=csrf_token(); ?>'+'&name='+name+'&stages='+jsonstring,
            success: function(data){
                window.location.reload();
            }
        });
    }
    /*End Update Pipeline*/
    /*Delete Pipeline*/
    function deleteTable(id) {
        $.ajax({
            type: "POST",
            url: "delete-pipeline-ajax",
            data:'id='+id+'&_token=<?=csrf_token(); ?>',
            success: function(data){
                alert(data);
                $('#deletepipeline'+id).html('Deleted Successfully!!');
                window.location.reload();
            }
        });
    }
    /*End Delete Pipeline*/

    function rowDelete(id,stage,pipelineid) {
        $.ajax({
            type: "POST",
            url: "update-delete-pipeline",
            data:'id='+id+'&state='+stage+'&pipelineid='+pipelineid+'&_token=<?=csrf_token(); ?>',

            success: function(data){
                alert(data);
                $('#deletepipeline'+id).html('Deleted Successfully!!');
                window.location.reload();
            }
        });
    }

    var $insertBefore = $('#insertBefore');
    var $i = 0;

    // Add More Inputs
    function addMore(id) {
        $i = $i+1;
        markup = "<tr id='row"+$i+"'><td data-id='' contenteditable>This is row "
            + $i + "</td><td><button type='button' onclick='removeBox($i)' class='btn btn-sm btn-danger'><i class='fa fa-times'></i></button></td></tr>";
        tableBody = $("#makeEditable"+id);
        tableBody.append(markup);
    }


    // Remove fields
    function removeBox(index){
        $('#row'+index).remove();
    }


</script>
<?php echo $footer ?>
</body>
</html>
