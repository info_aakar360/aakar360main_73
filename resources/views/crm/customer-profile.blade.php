<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="ThemeSelect">
    <title>
        <?=$title; ?>
    </title>

    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/select.dataTables.min.css">

    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->

    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />
    <!--    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>-->

    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <!-- END: Custom CSS-->
</head>

<style type="text/css">
    .file-field input.file-path{
        height: 33px !important;
    }
    .file-field .btn, .file-field .btn-large, .file-field .btn-small{
        height: 33px !important;
        line-height: 33px !important;
    }
    .modal-window {
        position: fixed;
        background-color: rgba(200, 200, 200, 0.75);
        top: -70px;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 999;
        opacity: 0;
        pointer-events: none;
        -webkit-transition: all 0.3s;
        -moz-transition: all 0.3s;
        transition: all 0.3s;
    }
    .modal-window:target {
        opacity: 1;
        pointer-events: auto;
    }
    .modal-window>div {
        width: 800px;
        position: relative;
        margin: 10% auto;
        padding: 2rem;
        background: #fff;
        color: #444;
    }
    @media only screen and (max-width: 600px) {
        .modal-window>div {
            width: 340px;
            position: relative;
            margin: 10% auto;
            padding: 2rem;
            background: #fff;
            color: #444;
        }
    }

    .modal-window header {
        font-weight: bold;
    }

    .modal-close {
        color: #aaa;
        line-height: 50px;
        font-size: 80%;
        position: absolute;
        right: 0;
        text-align: center;
        top: 0;
        width: 70px;
        text-decoration: none;
    }

    .modal-close:hover {
        color: #000;
    }

    .modal-window h1 {
        font-size: 150%;
        margin: 0 0 15px;
    }
    table th, td{
        border:1px solid #d8d3d8;
        padding-left: 15px;
    }
    .tableFixHead {
        overflow: auto;
        height: 560px;
        width:100%
    }

    .mygray{
        background-color: #aab2ae !important;
    }
    .myred{
        background-color: #eb6667 !important;
    }
    .mygreen{
        background-color: #4bbb50 !important;
    }
    .btn-arrow-right,
    .btn-arrow-left {
        position: relative;
        padding-left: 18px;
        padding-right: 18px;
    }
    .btn-arrow-right {
        padding-left: 36px;
    }
    .btn-arrow-left {
        padding-right: 36px;
    }
    .btn-arrow-right:before,
    .btn-arrow-right:after,
    .btn-arrow-left:before,
    .btn-arrow-left:after { /* make two squares (before and after), looking similar to the button */
        content:"";
        position: absolute;
        top: 7px; /* move it down because of rounded corners */
        width: 22px; /* same as height */
        height: 22px; /* button_outer_height / sqrt(2) */
        background: inherit; /* use parent background */
        border: inherit; /* use parent border */
        border-left-color: transparent; /* hide left border */
        border-bottom-color: transparent; /* hide bottom border */
        border-radius: 0px 4px 0px 0px; /* round arrow corner, the shorthand property doesn't accept "inherit" so it is set to 4px */
        -webkit-border-radius: 0px 4px 0px 0px;
        -moz-border-radius: 0px 4px 0px 0px;
    }
    .btn-arrow-right:before,
    .btn-arrow-right:after {
        transform: rotate(45deg); /* rotate right arrow squares 45 deg to point right */
        -webkit-transform: rotate(45deg);
        -moz-transform: rotate(45deg);
        -o-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
    }
    .btn-arrow-left:before,
    .btn-arrow-left:after {
        transform: rotate(225deg); /* rotate left arrow squares 225 deg to point left */
        -webkit-transform: rotate(225deg);
        -moz-transform: rotate(225deg);
        -o-transform: rotate(225deg);
        -ms-transform: rotate(225deg);
    }
    .btn-arrow-right:before,
    .btn-arrow-left:before { /* align the "before" square to the left */
        left: -11px;
    }
    .btn-arrow-right:after,
    .btn-arrow-left:after { /* align the "after" square to the right */
        right: -11px;
    }
    .btn-arrow-right:after,
    .btn-arrow-left:before { /* bring arrow pointers to front */
        z-index: 1;
    }
    .btn-arrow-right:before,
    .btn-arrow-left:after { /* hide arrow tails background */
        background-color: white;
    }

    .visit {
        display: block !important;
    }
    /* #timeline
     }*/
    #timeline-content {
        margin-top: 50px
        height: 300px;
        text-align: center;
    }

    /* Timeline */

    .timeline1 {
        border-left: 4px solid #004ffc;
        border-bottom-right-radius: 4px;
        border-top-right-radius: 4px;
        background: rgba(255, 255, 255, 0.03);
        color: rgba(255, 255, 255, 0.8);
        font-family: 'Chivo', sans-serif;
        margin: 50px auto;
        letter-spacing: 0.5px;
        position: relative;
        line-height: 1.4em;
        font-size: 1.03em;
        padding: 50px;
        list-style: none;
        text-align: left;
        font-weight: 100;
        max-width: 95%;
    }
    .timeline1 .event {
        border-bottom: 1px dashed rgba(255, 255, 255, 0.1);
        position: relative;
    }
    .event:last-of-type {
        padding-bottom: 0;
        margin-bottom: 0;
        border: none;
    }

    .event :before,

    .event :after {
        position: absolute;
        display: block;
        top: 0;
    }
    .timeline1 .event:before {
        left: -215.5px;
        color: rgba(255, 255, 255, 0.4);
        content: attr(data-date);
        text-align: right;
        font-weight: 100;
        font-size: 0.9em;
        min-width: 120px;
        font-family: 'Saira', sans-serif;
    }
    .timeline1 .event:after{
        box-shadow: 0 0 0 4px #004ffc;
        left: -9.85px;
        background: #313534;
        border-radius: 50%;
        height: 11px;
        width: 11px;
        content: "";
        top: 5px;
        position: absolute;
    }
    .referbyclass .select-wrapper input.select-dropdown{
        display: none;
    }
</style>
<!-- END: Head-->
<?=$header;?>

<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs">
            <div class="card-content" style="min-height: 500px;">
                <?php

                if(session()->get('notices') != ''){
                    echo session()->get('notices');
                }
                ?>
                <?=$notices;?>

                <div class="card-title">
                    <div class="row">
                        <div class="col s3 m3">
                            <a class="btn myblue waves-light" style="padding:0 5px;" href="<?php echo isset($_GET['link']) ? $_GET['link'] : 'customer-data';?>">
                                <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                            </a>
                        </div>
                        <div class="col s1 m1" style="margin-top: 10px;">
                            <?php if(checkRole($user->u_id,"sel_usr")){
                            ?>
                            <a href="#open-modal8" class="" style="padding:0 2px; margin-left: 70px"><i class="material-icons">edit</i></a>

                            <?php } ?>
                        </div>
                        <div class="col s3 m3">
                            <h5>Managed By & Above - </h5>
                        </div>

                        <div class = "col s5 m5">
                            <?php
                            $des = array();
                            $exuser = array();
                            foreach ($designations as $designation){

                                $eux = explode(',',$customer->managed_by);
                                if(in_array('des'.$designation->id, $eux)){

                                    $des[] = $designation->name;
                                }

                            }

                            foreach ($users as $user1){
                                $eux = explode(',',$customer->managed_by);
                                if(in_array($user1->u_id, $eux)){
                                    $exuser[] = $user1->u_name;
                                }

                            }
                            $data =  array_merge($des,$exuser);

                            print_r( "<h5>".implode(',',$data)."</h5>");
                            ?>
                        </div>

                        <div class="col s12 m12 l12">
                            <?php
                            $permission = checkRole($user->u_id,"verify_cust");

                            ?>
                            <h5><?=$customer->name." [ ".$customer->proprieter_name." ] ";?>
                                <?php if($permission && $customer->verified == 0)
                                { echo '<a href="verify-customer?edit='.$_GET['cid'].'&action=1" class="btn myblue waves-light"><i class="material-icons left" style="margin-right: 5px">check</i> Verify</a>'; } if($customer->verified == 1){ echo '<span style="font-size: 16px;color: blue"><i class="material-icons" style="font-size: 16px;color: blue;">verified_user</i> Verified</span>';if($permission) { echo '&nbsp;<a href="verify-customer?edit='.$_GET['cid'].'&action=0" class="btn myblue waves-light">Un-Verify</a>';} }?></h5>

                            <form method="post">
                                <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                <input type="hidden" value="<?=$customer->id?>" class="cust_id">
                                <button  value="1" type="button" class="btn btn-arrow-right change_status
                            <?php
                                if($customer->class == 4){
                                    echo 'mygreen';
                                }
                                else if($customer->class == 5){
                                    echo 'myred';
                                }
                                else if($customer->class == 6){
                                    echo 'mypurple';
                                }
                                else {
                                    echo 'myblue';
                                }
                                ?>" >Raw </button>
                                <a href="#open-modal9"><button id="button1" type="button" class="btn btn-arrow-right
<?php if($customer->class == 4){
                                        echo 'mygreen';
                                    }
                                    else if($customer->class == 5){
                                        echo 'myred';
                                    }
                                    else if($customer->class == 6){
                                        echo 'mypurple';
                                    }
                                    else if($customer->class == 2 || $customer->class == 3 ||  $customer->class == 7){
                                        echo 'myblue';
                                    }
                                    else {
                                        echo 'mygray';
                                    } //($customer->class == 4)?'mygreen':($customer->class == 5)?'myred':($customer->class == 2 || $customer->class == 3 )?'myblue':'mygray';
                                    ?>

                                            ">UnReachable</button></a>
                                <a href="#open-modal5"><button id="button1" type="button" class="btn btn-arrow-right
<?php if($customer->class == 4){
                                        echo 'mygreen';
                                    }
                                    else if($customer->class == 5){
                                        echo 'myred';
                                    }
                                    else if($customer->class == 6){
                                        echo 'mypurple';
                                    }
                                    else if($customer->class == 2 || $customer->class == 3  ){
                                        echo 'myblue';
                                    }
                                    else {
                                        echo 'mygray';
                                    } //($customer->class == 4)?'mygreen':($customer->class == 5)?'myred':($customer->class == 2 || $customer->class == 3 )?'myblue':'mygray';
                                    ?>

                                            ">Contacted</button></a>

                                <a href="#open-modal6"><button type="button" class="btn btn-arrow-right
<?php
                                    if($customer->class == 4){
                                        echo 'mygreen';
                                    }
                                    else if($customer->class == 5){
                                        echo 'myred';
                                    }
                                    else if($customer->class == 6){
                                        echo 'mypurple';
                                    }
                                    else if($customer->class == 3 ){
                                        echo 'myblue';
                                    }
                                    else {
                                        echo 'mygray';
                                    }
                                    ?>
                                            ">Interested</button></a>
                                <a class="btn dropdown-settings btn btn-arrow-right <?php
                                if($customer->class == 4){
                                    echo 'mygreen';
                                }
                                else if($customer->class == 5){
                                    echo 'myred';
                                }
                                else if($customer->class == 6){
                                    echo 'mypurple';
                                }
                                else {
                                    echo 'mygray';
                                }
                                ?> " href="#!" data-target="dropdown1">

                                <span class="hide-on-small-onl"><?php if($customer->class == 5){echo 'Unqualified';} else if($customer->class == 4) { echo 'Converted'; }else if($customer->class == 6) { echo 'Not Interested'; } else {
                                        echo 'Con / Not-In / Unq';
                                    } ?></span>
                                    <i class="material-icons right">arrow_drop_down</i>
                                </a>
                                <ul class="dropdown-content" id="dropdown1" tabindex="0" style="padding: 20px">
                                    <li tabindex="0">
                                        <a href="#open-modal7" class="update_reason btn mygreen converted_btn" data-value="4" style="line-height: 10px !important; color: white !important;">Converted</a>
                                    </li>

                                    <li tabindex="0">
                                        <a href="#open-modal4" class="update_reason btn mypurple" data-value="6" style="line-height: 10px !important; color: white !important;">Not Interested</a>
                                        <!--<button value ='6' class="col m12 s12 btn mypurple change_status" ></button>-->
                                        <!--<button value ='5' class="btn myred change_status">Unqualified</button>-->
                                    </li>
                                    <li tabindex="0">
                                        <a href="#open-modal2" class="update_reason btn myred" data-value="5" style="line-height: 10px !important; color: white !important;">Unqualified</a>
                                        <!--<a style="visibility: hidden;" id="open_reason" href="#open-modal2" data-toggle="modal">open</a>-->
                                    </li>
                                </ul>
                            </form>
                        </div>

                        <div class="col s12 m12 l12">
                            <ul class="row tabs" >
                                <li class="tab col s5 m5 xl1"><a class="active p-0" href="#Visits">Visits</a></li>
                                <li class="tab col s5 m5 xl1"><a class="p-0" href="#Products">Products</a></li>
                                <li class="tab col s5 m5 xl1"><a class="p-0" href="#Project">Project</a></li>
                                <li class="tab col s12 m12 xl1"><a class="p-0" href="#Promotional-material">Promotional</a></li>
                                <li class="tab col s5 m5 xl1"><a class="p-0" href="#Notes">Notes</a></li>
                                <li class="tab col s12 m12 xl2"><a class="p-0" href="#Communication">Communication</a></li>
                                <li class="tab col s12 m12 xl2"><a class="p-0" href="#Lead_Order">Lead/Order</a></li>
                                <li class="tab col s5 m5 xl1"><a target="_blank" class="p-0" href="customers?edit=<?=$customer->id?>">Master</a></li>
                                <li class="tab col s5 m5 xl1"><a class="p-0" href="#Tickets">Tickets</a></li>
                                <li class="tab col s5 m5 xl1"><a class="p-0" href="#Timeline">Timeline</a></li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div id="Timeline" style="display: none;" class="col-lg-12 col-xs-12">
                        <div class="container">
                            <form method="get" action="" class="right col s6 m2" id="timeline_form">
                                <input type="hidden" name="cid" value="<?=isset($_GET['cid']) ? $_GET['cid'] : ''?>"/>
                                <div class="input-field">
                                    <select name="month" id="month"> Month
                                        <option value="1" <?=($month==1) ? 'selected' : ''?>>January</option>
                                        <option value="2" <?=($month==2) ? 'selected' : ''?>>February</option>
                                        <option value="3" <?=($month==3) ? 'selected' : ''?>>March</option>
                                        <option value="4" <?=($month==4) ? 'selected' : ''?>>April</option>
                                        <option value="5" <?=($month==5) ? 'selected' : ''?>>May</option>
                                        <option value="6" <?=($month==6) ? 'selected' : ''?>>June</option>
                                        <option value="7" <?=($month==7) ? 'selected' : ''?>>July</option>
                                        <option value="8" <?=($month==8) ? 'selected' : ''?>>August</option>
                                        <option value="9" <?=($month==9) ? 'selected' : ''?>>September</option>
                                        <option value="10" <?=($month==10) ? 'selected' : ''?>>October</option>
                                        <option value="11" <?=($month==11) ? 'selected' : ''?>>November</option>
                                        <option value="12" <?=($month==12) ? 'selected' : ''?>>December</option>
                                    </select>
                                    <label for="month">Month</label>
                                </div>
                            </form>
                            <?php

                            if(!$timeline_data){
                                echo '<h5>No data available for timeline.</h5>';
                            }else{ ?>
                            <div class="timeline">
                                <div class="timeline-month">
                                    <?php $date = date('Y-'.$month.'-01');
                                    echo date('F, Y', strtotime($date));
                                    ?>
                                    <span><?=count($timeline_data);?> Days</span>
                                </div>
                                <?php foreach($timeline_data as $td){ ?>
                                <div class="timeline-section">
                                    <div class="timeline-date">
                                        <?php
                                        echo date('d, l', strtotime($td[0]->date));
                                        ?>
                                    </div>
                                    <div class="row">
                                        <?php foreach($td as $t){ ?>
                                                    <?php
                                            $data = null;
                                            $tl_user = DB::table('user')->where('u_id', $t->created_by)->first();
                                            switch ($t->section) {
                                                case 'tickets':
                                                    $data = getTimelineData('crm_tickets', $t->section_id);
                                                    if(!($data === null)) {
                                                        echo '<div class="col s12 m4">
                                                <div class="timeline-box">
<div class="box-title">
                                                                    <i class="material-icons text-success" aria-hidden="true">card_membership</i>
                                                                    New Ticket
                                                                 </div>
                                                                 <div class="box-content">
                                                                    <div class="box-item"><strong>Ticket Id</strong>: ' . $data->id . '</div>
                                                                    <div class="box-item"><strong>Type</strong>: '.$data->type.'</div>
                                                                    <div class="box-item"><strong>Priority</strong>: '.$data->priority.'</div>
                                                                    <div class="box-item"><strong>Status</strong>: '.$data->status.'</div>
                                                                 </div><div class="box-footer">- ';
                                                        echo ($tl_user !== null) ? $tl_user->u_name : '';
                                                        echo '</div></div></div>';
                                                    }
                                                    break;
                                                case 'tickets_edit':
                                                    $data = getTimelineData('crm_tickets', $t->section_id);
                                                    if(!($data === null)) {
                                                        echo '<div class="col s12 m4">
                                                                    <div class="timeline-box">
                                                                        <div class="box-title">
                                                                    <i class="material-icons text-success" aria-hidden="true">card_membership</i>
                                                                    Ticket Update
                                                                 </div>

                                                                 <div class="box-content">
                                                                    <div class="box-item"><strong>Ticket Id</strong>: ' . $data->id . '</div>
                                                                    <div class="box-item"><strong>Type</strong>: '.$data->type.'</div>
                                                                    <div class="box-item"><strong>Priority</strong>: '.$data->priority.'</div>
                                                                    <div class="box-item"><strong>Status</strong>: '.$data->status.'</div>
                                                                 </div><div class="box-footer">- ';
                                                        echo ($tl_user !== null) ? $tl_user->u_name : '';
                                                        echo '</div></div></div>';
                                                    }
                                                    break;
                                                case 'visits':
                                                    $data = getTimelineData('crm_visits', $t->section_id);
                                                    if(!($data === null)) {
                                                        echo '<div class="col s12 m4">
                                                <div class="timeline-box"><div class="box-title">
                                                                    <i class="material-icons text-success" aria-hidden="true">transfer_within_a_station</i>
                                                                    New Visit
                                                                 </div>
                                                                 <div class="box-content">
                                                                    <div class="box-item"><strong>Date</strong>: '.date('d F, Y', strtotime($data->select_date)).'</div>
                                                                    <div class="box-item"><strong>Detail</strong>: '.$data->description.'</div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                 </div><div class="box-footer">- ';
                                                        echo ($tl_user !== null) ? $tl_user->u_name : '';
                                                        echo '</div></div></div>';
                                                    }
                                                    break;
//                                                        case 'visits_edit':
//                                                            $data = getTimelineData('crm_visits', $t->section_id);
//                                                            if(!($data === null)) {
//                                                                echo '<div class="col s12 m4">
//                                                <div class="timeline-box"><div class="box-title">
//                                                                    <i class="material-icons text-success" aria-hidden="true">transfer_within_a_station</i>
//                                                                    Visit Update
//                                                                 </div>
//                                                                 <div class="box-content">
//                                                                    <div class="box-item"><strong>Date</strong>: '.date('d F, Y', strtotime($data->select_date)).'</div>
//                                                                    <div class="box-item"><strong>Detail</strong>: '.$data->description.'</div>
//                                                                    <div class="box-item"><strong></strong></div>
//                                                                    <div class="box-item"><strong></strong></div>
//                                                                 </div><div class="box-footer">- ';
//                                                                echo ($tl_user !== null) ? $tl_user->u_name : '';
//                                                                echo '</div></div></div>';
//                                                            }
//                                                            break;
                                                case 'notes':
                                                    $data = getTimelineData('crm_notes', $t->section_id);
                                                    if(!($data === null)) {
                                                        echo '<div class="col s12 m4">
                                                <div class="timeline-box"><div class="box-title">
                                                                    <i class="material-icons text-success" aria-hidden="true">speaker_notes</i>
                                                                    New Note
                                                                 </div>
                                                                 <div class="box-content">
                                                                    <div class="box-item"><strong>Date</strong>: '.date('d F, Y', strtotime($data->select_date)).'</div>
                                                                    <div class="box-item"><strong>Notes</strong>: '.$data->description.'</div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                 </div><div class="box-footer">- ';
                                                        echo ($tl_user !== null) ? $tl_user->u_name : '';
                                                        echo '</div></div></div>';
                                                    }
                                                    break;
                                                case 'notes_edit':
                                                    $data = getTimelineData('crm_notes', $t->section_id);
                                                    if(!($data === null)) {
                                                        echo '<div class="col s12 m4">
                                                <div class="timeline-box"><div class="box-title">
                                                                    <i class="material-icons text-success" aria-hidden="true">speaker_notes</i>
                                                                    Note Update
                                                                 </div>
                                                                 <div class="box-content">
                                                                    <div class="box-item"><strong>Date</strong>: '.date('d F, Y', strtotime($data->select_date)).'</div>
                                                                    <div class="box-item"><strong>Notes</strong>: '.$data->description.'</div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                 </div><div class="box-footer">- ';
                                                        echo ($tl_user !== null) ? $tl_user->u_name : '';
                                                        echo '</div></div></div>';
                                                    }
                                                    break;
                                                case 'projects':
                                                    $data = getTimelineData('crm_customer_projects', $t->section_id);
                                                    if(!($data === null)) {
                                                        echo '<div class="col s12 m4">
                                                <div class="timeline-box"><div class="box-title">
                                                                    <i class="material-icons text-success" aria-hidden="true">work_outline</i>
                                                                    New Project
                                                                 </div>
                                                                 <div class="box-content">
                                                                    <div class="box-item"><strong>Title</strong>: '.$data->project_name.'</div>
                                                                    <div class="box-item"><strong>Type</strong>: '.$data->project_type.'</div>
                                                                    <div class="box-item"><strong>Status</strong>: '.$data->project_status.'</div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                 </div><div class="box-footer">- ';
                                                        echo ($tl_user !== null) ? $tl_user->u_name : '';
                                                        echo '</div></div></div>';
                                                    }
                                                    break;
                                                case 'projects_edit':
                                                    $data = getTimelineData('crm_customer_projects', $t->section_id);
                                                    if(!($data === null)) {
                                                        echo '<div class="col s12 m4">
                                                                            <div class="timeline-box">
                                                                                <div class="box-title">
                                                                                    <i class="material-icons text-success" aria-hidden="true">work_outline</i> Project Update
                                                                                 </div>
                                                                                 <div class="box-content">
                                                                                    <div class="box-item"><strong>Title</strong>: '.$data->project_name.'</div>
                                                                                    <div class="box-item"><strong>Type</strong>: '.$data->project_type.'</div>
                                                                                    <div class="box-item"><strong>Status</strong>: '.$data->project_status.'</div>
                                                                                    <div class="box-item"><strong></strong></div>
                                                                                 </div>
                                                                                 <div class="box-footer">- ';
                                                        echo ($tl_user !== null) ? $tl_user->u_name : '';
                                                        echo '</div></div></div>';
                                                    }
                                                    break;
                                                case 'promotional':
                                                    $data = getTimelineData('crm_promotional', $t->section_id);
                                                    if(!($data === null)) {
                                                        echo '<div class="col s12 m4">
                                                <div class="timeline-box"><div class="box-title">
                                                                    <i class="material-icons text-success" aria-hidden="true">store</i>
                                                                    New Promotional
                                                                 </div>
                                                                 <div class="box-content">
                                                                    <div class="box-item"><strong>Date</strong>: '.date('d F, Y', strtotime($data->select_date)).'</div>
                                                                    <div class="box-item"><strong>Notes</strong>: '.$data->description.'</div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                 </div><div class="box-footer">- ';
                                                        echo ($tl_user !== null) ? $tl_user->u_name : '';
                                                        echo '</div></div></div>';
                                                    }
                                                    break;
                                                case 'promotional_edit':
                                                    $data = getTimelineData('crm_promotional', $t->section_id);
                                                    if(!($data === null)) {
                                                        echo '<div class="col s12 m4">
                                                <div class="timeline-box"><div class="box-title">
                                                                    <i class="material-icons text-success" aria-hidden="true">store</i>
                                                                    Promotional Update
                                                                 </div>
                                                                 <div class="box-content">
                                                                    <div class="box-item"><strong>Date</strong>: '.date('d F, Y', strtotime($data->select_date)).'</div>
                                                                    <div class="box-item"><strong>Notes</strong>: '.$data->description.'</div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                 </div><div class="box-footer">- ';
                                                        echo ($tl_user !== null) ? $tl_user->u_name : '';
                                                        echo '</div></div></div>';
                                                    }
                                                    break;
                                                case 'products':
                                                    $data = getTimelineData('crm_products', $t->section_id);
                                                    if(!($data === null)) {
                                                        echo '<div class="col s12 m4">
                                                <div class="timeline-box"><div class="box-title">
                                                                    <i class="material-icons text-success" aria-hidden="true">store</i>
                                                                    New Products
                                                                 </div>
                                                                 <div class="box-content">
                                                                    <div class="box-item"><strong>Category(s)</strong>: '.getCategoryNames($data->product_id).'</div>
                                                                    <div class="box-item"><strong>Brand(s)</strong>: '.getBrandNames($data->brands).'</div>
                                                                    <div class="box-item"><strong>Notes</strong>: '.$data->description.'</div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                 </div><div class="box-footer">- ';
                                                        echo ($tl_user !== null) ? $tl_user->u_name : '';
                                                        echo '</div></div></div>';
                                                    }
                                                    break;
                                                case 'products_edit':
                                                    $data = getTimelineData('crm_products', $t->section_id);
                                                    if(!($data === null)) {
                                                        echo '<div class="col s12 m4">
                                                <div class="timeline-box"><div class="box-title">
                                                                    <i class="material-icons text-success" aria-hidden="true">store</i>
                                                                    Products Update
                                                                 </div>
                                                                 <div class="box-content">
                                                                    <div class="box-item"><strong>Category(s)</strong>: '.getCategoryNames($data->product_id).'</div>
                                                                    <div class="box-item"><strong>Brand(s)</strong>: '.getBrandNames($data->brands).'</div>
                                                                    <div class="box-item"><strong>Notes</strong>: '.$data->description.'</div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                 </div><div class="box-footer">- ';
                                                        echo ($tl_user !== null) ? $tl_user->u_name : '';
                                                        echo '</div></div></div>';
                                                    }
                                                    break;
                                                case 'calls':
                                                    $data = getTimelineData('crm_call', $t->section_id);
                                                    if(!($data === null)) {
                                                        echo '<div class="col s12 m4">
                                                <div class="timeline-box"><div class="box-title">
                                                                    <i class="material-icons text-success" aria-hidden="true">phone</i>
                                                                    New Call
                                                                 </div>
                                                                 <div class="box-content">
                                                                    <div class="box-item"><strong>Number</strong>: '.$data->customer_number.'</div>
                                                                    <div class="box-item"><strong>Date</strong>: '.date('d F, Y', strtotime($data->select_date)).'</div>
                                                                    <div class="box-item"><strong>Remarks</strong>: '.$data->message.'</div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                 </div><div class="box-footer">- ';
                                                        echo ($tl_user !== null) ? $tl_user->u_name : '';
                                                        echo '</div></div></div>';
                                                    }
                                                    break;
                                                case 'calls_edit':
                                                    $data = getTimelineData('crm_call', $t->section_id);
                                                    if(!($data === null)) {
                                                        echo '<div class="col s12 m4">
                                                <div class="timeline-box"><div class="box-title">
                                                                    <i class="material-icons text-success" aria-hidden="true">phone</i>
                                                                    Call Update
                                                                 </div>
                                                                 <div class="box-content">
                                                                    <div class="box-item"><strong>Number</strong>: '.$data->customer_number.'</div>
                                                                    <div class="box-item"><strong>Date</strong>: '.date('d F, Y', strtotime($data->select_date)).'</div>
                                                                    <div class="box-item"><strong>Remarks</strong>: '.$data->message.'</div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                 </div><div class="box-footer">- ';
                                                        echo ($tl_user !== null) ? $tl_user->u_name : '';
                                                        echo '</div></div></div>';
                                                    }
                                                    break;
                                                case 'sms':
                                                    $data = getTimelineData('crm_message_save', $t->section_id);
                                                    if(!($data === null)) {
                                                        echo '<div class="col s12 m4">
                                                <div class="timeline-box"><div class="box-title">
                                                                    <i class="material-icons text-success" aria-hidden="true">mail_outline</i>
                                                                    New SMS
                                                                 </div>
                                                                 <div class="box-content">
                                                                    <div class="box-item"><strong>Number</strong>: '.$data->mobile.'</div>
                                                                    <div class="box-item"><strong>Remarks</strong>: '.$data->message.'</div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                 </div><div class="box-footer">- ';
                                                        echo ($tl_user !== null) ? $tl_user->u_name : '';
                                                        echo '</div></div></div>';
                                                    }
                                                    break;
                                                case 'class':
                                                    echo '<div class="col s12 m4">
                                                <div class="timeline-box"><div class="box-title">
                                                                    <i class="material-icons text-success" aria-hidden="true">card_membership</i>
                                                                    Class Changed
                                                                 </div>
                                                                 <div class="box-content">
                                                                    <div class="box-item"><strong>Remark</strong>: ' . $t->remark . '</div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                 </div><div class="box-footer">- ';
                                                    echo ($tl_user !== null) ? $tl_user->u_name : '';
                                                    echo '</div></div></div>';
                                                    break;
                                                default:
                                                    echo '';
                                            }
                                            ?>  <?php } ?>

                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <?php } ?>
                        </div>

                    </div>
                    <div id="Tickets" style="display: none;" class="col-lg-12 col-xs-12">
                        <div class="col s12 m12 " style="text-align:center; margin-bottom: 10px" >
                            <button class="btn myblue waves-light ticket_form_button" style="padding:0 5px;">
                                <i class="material-icons right">note_add</i> New Ticket
                            </button>
                        </div>
                        <div class="ticket_form">
                            <form method="post" action="customer-profile" enctype="multipart/form-data" class="col s12">
                                <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                <input type="hidden" name="crm_cust_id" value="<?php echo  $customer->id; ?>">
                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">forum</i>
                                        <select name="type" class="type" required> type
                                            <option selected disabled value="">Select Type</option>
                                            <option value="Question">Question</option>
                                            <option value="Quotation">Enquiry</option>
                                            <option value="Problem">Problem</option>
                                            <option value="Reminder">Reminder</option>
                                        </select>
                                        <label for="type">Type</label>
                                    </div>
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">low_priority</i>
                                        <select name="priority" class="Priority" required> Priority
                                            <option value="" selected disabled>Select Priority</option>
                                            <option value="Low">Low</option>
                                            <option value="Medium">Medium</option>
                                            <option value="High">High</option>
                                            <option value="Urgent">Urgent</option>
                                        </select>
                                        <label for="priority">Priority</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">thumbs_up_down</i>
                                        <select name="status" class="Status" required> Status
                                            <option value="" selected disabled>Select Status</option>
                                            <option value="Pending">Pending</option>
                                            <option value="Closed">Closed</option>
                                        </select>
                                        <label for="status">Status</label>
                                    </div>
                                    <div class="input-field col s12 m6" style="margin-left: -18px;">
                                        <div class="col s1 m1"><i class="material-icons prefix">transfer_within_a_station</i></div>
                                        <div class="col m11 s11">
                                            <label for="Assign_to">Select User</label>
                                            <select class="browser-default Status assignto" name="Assign_to[]" id ="Assign_to"   multiple tabindex="-1">
                                                <?php foreach ($users as $user){
                                                ?>
                                                <option @if($user->u_id == $user_name) selected @endif value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">watch_later</i>
                                        <input type="date" class="datepicker" id="ticket_create" name="ticket_create_date">
                                        <label for="ticket_create">Create Date</label>
                                    </div>
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">watch_later</i>
                                        <input type="date" class="datepicker" id="ticket_due_date" name="ticket_due_date">
                                        <label for="ticket_due_date">Due Date</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m12">
                                        <i class="material-icons prefix">view_headline</i>
                                        <input id="Description" type="text" required class="validate" name="description">
                                        <label for="Description">Description</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <!--<input type="file" name="image" style="width: 100%" class="btn myblue" />-->
                                        <div class="file-field">
                                            <div class="btn myblue waves-light left" style="padding:0 5px;">
                                                <span>Choose file</span>
                                                <input type="file" name="image" style="width: 100%" class="btn myblue" id="ticket_image">
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" type="text" placeholder="Upload your file">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="input-field col s12 m3">
                                        <p id="ticket_img_div">
                                            <img id="ticket_image_show" src="#" height="200px" width="200px"  />
                                        </p>
                                    </div>
                                    <div class="input-field col s12 m3" style="text-align: center">
                                        <button class="btn myblue waves-light " style="padding:0 5px;" type="submit" name="add_ticket">Save
                                            <i class="material-icons right">save</i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="divider"></div>
                        <h6>All Tickets</h6>
                        <div class="row">
                            <div class="col s12 table-responsive">
                                <table id="tickets-table" class="display" style="width: 100%;">
                                    <thead>
                                    <tr role="row">
                                        <th>Ticket ID</th>
                                        <th>Type</th>
                                        <th>Priority</th>
                                        <th>Description</th>
                                        <th>File</th>
                                        <th>Account Manager</th>
                                        <th>Create Date</th>
                                        <th>Due Date</th>
                                        <th>Status</th>
                                        <th>edited_by</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div id="Notes" style="display: none;" class="col-lg-12 col-xs-12">
                        <div class="notes_form">
                            <form method="post" action="customer-profile" enctype="multipart/form-data" class="col s12 card">
                                 <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                 <input type="hidden" class="cust_id" name="crm_cust_id" value="<?php echo  $customer->id; ?>">
                                 <?php if(!empty($note_product) && count($note_product) > 0) { ?>
                                     <?php foreach($note_product as $key=>$value)  {
                                             $pro_cat = explode(',',$value->product_id);
                                             $pro_bra = explode(',',$value->brands);
                                             $pro_sup = explode(',',$value->supplier_id);
                                        ?>
                                     <input type="hidden" name="pro_id[]" id="pro_id[{{$key+1}}][]"  value="{{$value->id}}"  >
                                     <input type="hidden" name="totalnotecount" id="totalnotecount"  value="{{count($note_product)}}"  >
                                     <div class="row">
                                         <div class="input-field col s12 m3">
                                             <i class="material-icons prefix" style="margin-top: 10px">list</i>
                                             <div class="col s12 m12" style="margin-left: 20px">
                                                 Select Category
                                                 <select  class="browser-default category" name="category[{{$key+1}}][]" multiple tabindex="-1" style="width: 100% !important;">
                                                     <?php foreach ($product_category as $category){ ?>
                                                     <option  @if(!empty($pro_cat) && in_array($category->id,$pro_cat)) selected @endif value="<?=$category->id?>"> <?=$category->name?> </option>
                                                     <?php } ?>
                                                 </select>
                                             </div>
                                         </div>
                                         <div class="input-field col s12 m3">
                                             <i class="material-icons prefix" style="margin-top: 10px">local_mall</i>
                                             <div class="col s12 m12" style="margin-left: 20px">
                                                 Select Brand
                                                 <select  class="browser-default brand" name="brand[{{$key+1}}][]" multiple tabindex="-1" style="width: 100% !important;">
                                                     <?php foreach ($brand as $bnd){
                                                     ?>
                                                     <option @if(!empty($pro_bra) && in_array($bnd->id,$pro_bra)) selected @endif  value="<?=$bnd->id?>"> <?=$bnd->name?> </option>
                                                     <?php
                                                     }
                                                     ?>
                                                 </select>
                                             </div>
                                         </div>
                                         <div class="input-field col s12 m2">
                                             <i class="material-icons prefix" style="margin-top: 10px">dns</i>
                                             <div class="col s12 m12" style="margin-left: 20px">
                                                 monthly quantity
                                                 <input type="text" name="moquantity[{{$key+1}}][]"   @if(!empty($value->mothly_quan))  value="{{$value->mothly_quan}}"  @endif>
                                             </div>
                                         </div>
                                         <div class="input-field col s12 m3">
                                             <i class="material-icons prefix" style="margin-top: 10px">transfer_within_a_station</i>
                                             <div class="col s12 m12" style="margin-left: 20px">
                                                 Select Supplier
                                                 <select  class="browser-default supplier" name="supplier[{{$key+1}}][]" multiple tabindex="-1" style="width: 100% !important;">
                                                     <?php foreach ($suppliers as $sup){ ?>
                                                     <option  @if(!empty($sup) && in_array($sup->id,$pro_sup)) selected @endif  value="<?=$sup->id?>"> <?=$sup->name?> </option>
                                                     <?php } ?>
                                                 </select>
                                             </div>
                                         </div>
                                         @if($key==1)
                                             <div class="input-field col s12 m1">
                                                 <button value="{{$value->id}}" class="btn myred waves-light proddelete"  style="padding:0 10px;margin-left: 20px;"><i class="material-icons">remove</i></button>
                                             </div>
                                         @else
                                             <div class="input-field col s12 m1">
                                                 <button value="{{$value->id}}" class="btn myblue waves-light" id="plusNoteButton" style="padding:0 10px;margin-left: 20px;"><i class="material-icons">add</i></button>
                                             </div>
                                         @endif
                                     </div>
                                <?php } } else { ?>
                                     <input type="hidden" name="totalnotecount" id="totalnotecount"  value="1">
                                     <div class="row">
                                         <div class="input-field col s12 m3">
                                             <i class="material-icons prefix" style="margin-top: 10px">list</i>
                                             <div class="col s12 m12" style="margin-left: 20px">
                                                 Select Category
                                                 <select  class="browser-default category" name="cat[1][]" multiple tabindex="-1" style="width: 100% !important;">
                                                     <?php foreach ($product_category as $category){ ?>
                                                     <option  value="<?=$category->id?>"> <?=$category->name?> </option>
                                                     <?php } ?>
                                                 </select>
                                             </div>
                                         </div>
                                         <div class="input-field col s12 m3">
                                             <i class="material-icons prefix" style="margin-top: 10px">local_mall</i>
                                             <div class="col s12 m12" style="margin-left: 20px">
                                                 Select Brand
                                                 <select  class="browser-default brand" name="bran[1][]" multiple tabindex="-1" style="width: 100% !important;">
                                                     <?php foreach ($brand as $bnd){
                                                     ?>
                                                     <option  value="<?=$bnd->id?>"> <?=$bnd->name?> </option>
                                                     <?php
                                                     }
                                                     ?>
                                                 </select>
                                             </div>
                                         </div>
                                         <div class="input-field col s12 m2">
                                             <i class="material-icons prefix" style="margin-top: 10px">dns</i>
                                             <div class="col s12 m12" style="margin-left: 20px">
                                                 monthly quantity
                                                 <input type="text" name="moquant[1][]" class="validate">
                                             </div>
                                         </div>
                                         <div class="input-field col s12 m3">
                                             <i class="material-icons prefix" style="margin-top: 10px">transfer_within_a_station</i>
                                             <div class="col s12 m12" style="margin-left: 20px">
                                                 Select Supplier
                                                 <select  class="browser-default supplier" name="supr[1][]" multiple tabindex="-1" style="width: 100% !important;">
                                                     <?php foreach ($suppliers as $sup){ ?>
                                                     <option  value="<?=$sup->id?>"> <?=$sup->name?> </option>
                                                     <?php } ?>
                                                 </select>
                                             </div>
                                         </div>
                                         <div class="input-field col s12 m1">
                                             <button value="20" class="btn myblue waves-light" id="plusNoteButton" style="padding:0 10px;margin-left: 20px;"><i class="material-icons">add</i></button>
                                         </div>
                                     </div>
                                <?php  } ?>
                                 <div id="insertNoteBefore"></div>
                                 <div class="clearfix"> </div>
                                 <div class="row">
                                     <div class="input-field col s12 m6">
                                         <i class="material-icons prefix" style="margin-top: 10px">list</i>
                                         <div class="col s12 m12" style="margin-left: 20px">
                                             Payment Condition
                                             <select  class="browser-default " name="payCondition" style="width: 100% !important;">
                                                 <option value="">Select Payment Condition</option>
                                                 @for ($i=1;$i<=100;$i++)
                                                     <option  @if(!empty($note_product[0]) && $note_product[0]->pay_condition == $i) selected  @endif  value="{{$i}}">{{$i}}</option>
                                                @endfor
                                             </select>
                                         </div>
                                     </div>
                                     <div class="input-field col s12 m6">
                                         <i class="material-icons prefix" style="margin-top: 10px">local_mall</i>
                                         <div class="col s12 m12" style="margin-left: 20px">
                                             Payment Delay
                                             <select  class="browser-default " name="payDelay" style="width: 97%;">
                                                 <option value="">Select Payment Delay</option>
                                                 @for ($i=1;$i<=100;$i++)
                                                         <option  @if(!empty($note_product[0]) && $note_product[0]->pay_delay == $i) selected  @endif value="{{$i}}">{{$i}}</option>
                                                 @endfor
                                             </select>
                                         </div>
                                     </div>
                                     <div class="input-field col s12 m6">
                                         <i class="material-icons prefix" style="margin-top: 10px">dns</i>
                                         <div class="col s12 m12" style="margin-left: 20px">
                                             Order Lifting
                                             <select  class="browser-default " name="orderLift"  style="width: 100% !important;">
                                                 <option value="">Select Option</option>
                                                 <option @if(!empty($note_product[0]) && $note_product[0]->order_lifting == 'fast') selected  @endif value="fast">Fast</option>
                                                 <option @if(!empty($note_product[0]) && $note_product[0]->order_lifting == 'moderate') selected  @endif value="moderate">Moderate</option>
                                                 <option @if(!empty($note_product[0]) && $note_product[0]->order_lifting == 'late') selected  @endif value="late">Late</option>
                                             </select>
                                         </div>
                                     </div>
                                     <div class="input-field col s12 m6">
                                         <i class="material-icons prefix" style="margin-top: 10px">transfer_within_a_station</i>
                                         <div class="col s12 m12" style="margin-left: 20px" >
                                             Credit Limit
                                              <input type="number" name="creditLimit" @if(!empty($note_product[0])) value="{{$note_product[0]->credit_limit}}" @endif class="validate" style="width: 97%">
                                         </div>
                                     </div>
                                 </div>
                                 @if(!empty($crmreffer) && count($crmreffer) > 0)
                                     @php if(!empty($crmnote)) { $crm_note = explode(',',$crmnote->note); } @endphp
                                       @foreach($crmreffer as $key=>$reffered)
                                        <div class="row" id="addMoreBox2{{$key}}">
                                             <div class="input-field col s12 m6" id="ReferredByField{{$key}}" style="margin-left: -15px">
                                                 <div class="col s1 m1"> <i class="material-icons prefix">list</i></div>
                                                 <div class="col s11 m11 referbyclass">
                                                     <label>Referred By</label>
                                                     <select class="browser-default selectjs" style="display:none;width:107%;!important;" name="listcust[]">
                                                          <option value="{{$reffered->id}}" selected="selected">{{$reffered->name}}</option>
                                                     </select>
                                                 </div>
                                             </div>
                                            <div class="input-field col s12 m5" style="margin-left: 15px">
                                                 <i class="material-icons prefix">list</i>
                                                 <div class="col s12 m12" style="margin-left: 25px">
                                                     <label>Referred by Note</label>
                                                     <input type="text" class="validate" name="notes[]" value="{{$crm_note[$key]}}" >
                                                 </div>
                                             </div>
                                            @if($key==0)
                                                <div class="input-field col s12 m1">
                                                    <button value="20" class="btn myblue waves-light" id="plusButton" style="padding:0 10px;margin-left: 20px;"><i class="material-icons">add</i></button>
                                                </div>
                                            @else
                                                <div class="input-field col s12 m1">
                                                    <button value="20" class="btn myred waves-light" onclick="removeBox2({{$key}})"  style="padding:0 10px;margin-left: 20px;"><i class="material-icons">remove</i></button>
                                                </div>
                                            @endif
                                        </div>
                                    @endforeach
                                 @else
                                     <div class="row">
                                         <div class="input-field col s12 m6" id="ReferredByField" style="margin-left: -15px">
                                             <div class="col s1 m1"> <i class="material-icons prefix">list</i></div>
                                             <div class="col s11 m11 referbyclass">
                                                 <label>Referred By</label>
                                                 <select class="browser-default selectjs" style="display:none;width:107%;!important;" name="listcust[]">
                                                 </select>
                                             </div>
                                         </div>
                                         <div class="input-field col s12 m5">
                                             <div class="col s1 m1"> <i class="material-icons prefix">list</i></div>
                                             <div class="col s11 m11" style="margin-left: 41px;width: 100%">
                                                 <label>Referred by Note</label>
                                                 <input type="text" class="validate" name="notes[]"  >
                                             </div>
                                         </div>
                                         <div class="input-field col s12 m1" style="margin-left: 11px">
                                             <button value="20" class="btn myblue waves-light" id="plusButton" style="padding:0 10px;margin-left: 20px;"><i class="material-icons">add</i></button>
                                         </div>
                                     </div>
                                 @endif
                                     <div id="insertBefore"></div>
                                     <div class="clearfix"> </div>
                                    <div class="row">
                                        <div class="input-field col s12 m12">
                                            <i class="material-icons prefix" style="margin-top: 10px">transfer_within_a_station</i>
                                            <h6 style="margin-left: 34px;">All Images</h6>
                                            <div class="col s12 m12" style="margin-left: 20px;display:flex" >
                                                <?php if(!empty($products)) {
                                                     foreach($products as $key=>$value){ ?>
                                                        <div style="margin:3px">
                                                            <img src="../assets/crm/images/products/{{$value->image}}" style="width:120px;height:120px"></div>
                                               <?php }} ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12 m12">
                                            <i class="material-icons prefix" style="margin-top: 10px">transfer_within_a_station</i>
                                            <div class="col s12 m12" style="margin-left: 20px" >
                                                Addon Charges <a href="#open-modal9" style="padding:0 10px;margin-left: 20px;"><i class="material-icons">add</i></a>
                                                <select name="addonCharges[]" class="browser-default addoncharges" multiple style="width: 100%">
                                                    <option value="">Select Option</option>
                                                    <?php foreach ($addon as $pro) {
                                                    ?>
                                                    <option @if(!empty($note_product[0]) && in_array($pro->add_category.$pro->add_on_charges,explode(',',$note_product[0]->add_on_charge))) selected @endif value="{{$pro->add_category}}{{$pro->add_on_charges}}">{{$pro->add_category}}{{$pro->add_on_charges}}</option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                 <div class="row">
                                     @php   $user = \App\User::where('secure', session('crm'))->first(); @endphp
                                     @if(checkRole($user->u_id,"referrer_mas"))
                                         <div class="input-field col s6 m5" style="margin-left: -15px;">
                                             <div class="col s1 m1"> <i class="material-icons prefix">list</i></div>
                                             <div class="col s10 m10">
                                                 Referrer<label><input id="referrer" name="referrer" type="checkbox" @if($customer->referrer == 1) checked value="1" @endif >
                                                     <span class="district_all" style="text-decoration: none;font-size: 12px;margin-left: 13px;padding-left: 20px;">Select </span></label>

                                             </div>
                                         </div>
                                     @endif
                                     <div class="input-field col s12 m3" style="text-align: center">
                                         <button class="btn myblue waves-light" style="padding:0 5px;" type="submit" name="add_refferer">Save
                                             <i class="material-icons right">save</i>
                                         </button>
                                     </div>
                                 </div>
                            </form>
                            <div class="divider"></div>
                            <form method="post" action="customer-profile" enctype="multipart/form-data" class="col s12 card">
                                <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                <input type="hidden" name="crm_cust_id" value="<?php echo  $customer->id; ?>">
                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">watch_later</i>
                                        <input type="date" class="datepicker" id="dob1" name="select_date" >
                                        <label for="dob1">Date</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m12">
                                        <i class="material-icons prefix">view_headline</i>
                                        <input id="Description3" type="text" class="validate" name="description" required>
                                        <label for="Description3">Description</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <div class="file-field">
                                            <div class="btn myblue waves-light left" style="padding:0 5px;">
                                                <span>Choose file</span>
                                                <input type="file" name="image" style="width: 100%" class="btn myblue" id="notes_image">
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" type="text" placeholder="Upload your file">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="input-field col s12 m3">
                                        <p id="notes_img_div">
                                            <img id="notes_image_show" src="#" height="200px" width="200px"  />
                                        </p>
                                    </div>
                                    <div class="input-field col s12 m3" style="text-align: center">
                                        <button class="btn myblue waves-light" style="padding:0 5px;" type="submit" name="add_notes">Save
                                            <i class="material-icons right">save</i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="divider"></div>
                        <h6>All Notes</h6>
                        <div class="row">
                            <div class="col s12 table-responsive">
                                <table id="notes-table" class="display" style="width: 100%;">
                                    <thead>
                                    <tr role="row">
                                        <th>Sr.No.</th>
                                        <th>Date</th>
                                        <th>Description</th>
                                        <th>File</th>
                                        <th>edited_by</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div id="Project" style="display: none;" class="col-lg-12 col-xs-12">
                        <div class="project_form">
                            <form method="post" action="customer-profile" enctype="multipart/form-data" class="col s12">
                                <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                <input type="hidden" name="crm_cust_id" value="<?php echo  $customer->id; ?>">

                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">create_new_folder</i>
                                        <input type="text"  id="pname" required name="pname">
                                        <label for="pname">Project Name</label>
                                    </div>
                                    <div class="input-field col s12 m6 xl3">
                                        <i class="material-icons prefix">list</i>
                                        <select name="ptype" class="ptype" required> type
                                            <option value="" disabled selected>Select Project Type</option>
                                            <option value="1">Private</option>
                                            <option value="2">Government</option>
                                        </select>
                                        <label for="ptype">Project Type</label>
                                    </div>
                                    <div class="input-field col s12 m12 xl3" style="margin-top: 0px;">

                                        <i class="material-icons prefix" style="margin-top: 10px">dns</i>
                                        <div class="col s12 m12" style="margin-left: 20px">
                                            Project Sub Type
                                            <select class="browser-default pstype" required name="pstype[]" multiple tabindex="-1" style="width: 100% !important;" > type
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">thumbs_up_down</i>
                                        <select name="pstatus" id="pstatus" class="type" required> status
                                            <option value="">Select Status</option>
                                            <option value="Ongoing">Ongoing</option>
                                            <option value="Start">Start</option>
                                            <option value="Completed">Completed</option>
                                        </select>
                                        <label for="status">Status</label>
                                    </div>
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">border_style</i>
                                        <input type="text"  id="sqft" name="sqft">
                                        <label for="sqft">Total Sq.Feet</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">flag</i>
                                        <div class="col s12 m12" style="margin-left: 23px">
                                            <select  class="browser-default states state_list" name="state" style="width: 100%" required>
                                                <option value="">Select State</option>
                                                <?php
                                                foreach ($states as $state){
                                                ?>
                                                <option value="<?=$state->id;?>"><?=$state->name?></option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="input-field col s12 m6" >
                                        <i class="material-icons prefix">adjust</i>
                                        <div class="col s12 m12" style="margin-left: 23px">
                                            <select  class="browser-default district dist_list" name="district" style="width: 100%" required>
                                                <option value="" disabled selected>Select District</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="input-field col s12 m6" >
                                        <i class="material-icons prefix">adjust</i>
                                        <div class="col s12 m12" style="margin-left: 23px">
                                            <select  class="browser-default block" name="block" style="width: 100%" id="block">
                                                <option value="" disabled selected>Select Block</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="input-field col s12 m4" >
                                        <i class="material-icons prefix">location_on</i>
                                        <div class="col s12 m12" style="margin-left: 23px">
                                            <select  class="browser-default locality local_list" id="locality" name="locality" style="width: 100%">
                                                <option value="">Select Locality</option>

                                            </select>
                                        </div>
                                    </div>

                                    <div class="input-field col s12 m2">
                                        <a href="#open-modal3" class="btn myblue waves-light" style="padding: 0 5px; width: fit-content;">Add New
                                            <i class="material-icons right">add_circle_outline</i>
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">navigation</i>
                                        <input type="text"  id="paddress" name="paddress">
                                        <label for="paddress">Project Address</label>
                                    </div>
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">account_circle</i>
                                        <input type="text"  id="cpname" name="cpname">
                                        <label for="cpname">Contact Person Name</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">contact_phone</i>
                                        <input type="text"  id="cpnumber" name="cpnumber">
                                        <label for="cpnumber">Contact Person Number</label>
                                    </div>
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">school</i>
                                        <input type="text"  id="cpdesignation" name="cpdesignation">
                                        <label for="cpdesignation">Contact Person Designation</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m12">
                                        <i class="material-icons prefix">view_headline</i>
                                        <input id="Description4" type="text" class="validate" name="description">
                                        <label for="Description4">Description</label>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <div class="file-field">
                                            <div class="btn myblue waves-light left" style="padding:0 5px;">
                                                <span>Choose file</span>
                                                <input type="file" name="image" style="width: 100%" class="btn myblue" id="project_image">
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" type="text" placeholder="Upload your file">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="input-field col s12 m3">
                                        <p id="project_img_div">
                                            <img id="project_image_show" src="#" height="200px" width="200px"  />
                                        </p>
                                    </div>

                                    <div class="input-field col s12 m3" style="text-align: center;">
                                        <button class="btn myblue waves-light"  style="padding:0 5px;" type="submit" name="add_project">Save
                                            <i class="material-icons right">save</i>
                                        </button>
                                    </div>
                                </div>

                            </form>
                        </div>
                        <div class="divider"></div>
                        <h6>All Project</h6>
                        <div class="row">
                            <div class="col s12 table-responsive">
                                <table id="projects-table" class="display" style="width: 100%;">
                                    <thead>
                                    <tr role="row">
                                        <th>ID</th>
                                        <th>Project Name</th>
                                        <th>Project Type</th>
                                        <th>Project Subtype</th>
                                        <th>Total Sq. Feet</th>
                                        <th>Description</th>
                                        <th>File</th>
                                        <th>Edited By</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div id="Visits" class="col-lg-12 col-xs-12">
                        <div class="visits_form">
                            <form method="post" action="customer-profile" enctype="multipart/form-data" class="col s12">
                                <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                <input type="hidden" name="crm_cust_id" value="<?php echo  $customer->id; ?>">

                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">account_circle</i>
                                        <input id="name" type="text" class="validate" required name="name" value="<?=$customer->name;?>">
                                        <label for="name">Name</label>
                                    </div>
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">contact_phone</i>
                                        <input id="phone_no" type="tel" class="validate" required name="phone_no" value="<?=$customer->contact_no;?>">
                                        <label for="phone_no"> Phone No</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">navigation</i>
                                        <input id="address" type="text" class="validate" name="address" required value="<?=$customer->postal_address;?>">
                                        <label for="address">Address</label>
                                    </div>
                                    <div class="input-field col s12 m6">

                                        <i class="material-icons prefix">watch_later</i>
                                        <input type="date" class="datepicker" required id="dob" name="date">
                                        <label for="dob">Date</label>
                                        <label for="dob">Date</label>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">view_headline</i>
                                        <input id="Description_v" type="text" class="validate" name="description">
                                        <label for="Description_v">Description</label>
                                    </div>
                                    <div class="input-field col s12 m6" style="margin-top: 0px;">
                                        <i class="material-icons prefix" style="margin-top: 10px">person_add</i>
                                        <div class="col s12 m12" style="margin-left: 20px">
                                            Select User

                                            <select class="browser-default user" name="user[]" multiple tabindex="-1" style="width: 100% !important;" required> type
                                                <?php

                                                foreach ($users as $user){

                                                ?>
                                                <option <?php if($user_name==$user->u_id){echo "selected";} ?> value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <div class="file-field">
                                            <div class="btn myblue waves-light left" style="padding:0 5px; height: 33px !important;line-height: 33px !important;">
                                                <span>Choose file</span>
                                                <input type="file" name="image" style="width: 100%" class="btn myblue" id="visit_image">
                                            </div>
                                            <div class="file-path-wrapper" >
                                                <input  class="file-path validate "  type="text" placeholder="Upload your file">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="input-field col s12 m3">
                                        <p id="visit_img_div">
                                            <img id="visit_image_show" src="#" height="200px" width="200px"  />
                                        </p>
                                    </div>

                                    <div class="input-field col s12 m3" style="text-align: center">
                                        <button class="btn myblue waves-light" style="padding:0 5px;" type="submit" name="add_visit">Save
                                            <i class="material-icons right">save</i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="divider"></div>
                        <h6>All Visits</h6>
                        <div class="row">
                            <div class="col s12 table-responsive">
                                <table id="visits-table" class="display" style="width: 100%;">
                                    <thead>
                                    <tr role="row">
                                        <th>ID</th>
                                        <th>Date</th>
                                        <th>User Name</th>
                                        <th>Description</th>
                                        <th>file</th>
                                        <th>edited_by</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <!--                                                                        <td>=DB::table('crm_visits')->join('crm_user_manager','crm_user_manager.id','=','crm_visits.user_id')->select('crm_user_manager.name','crm_visits.id','=','$u_id')->get();<!--</td>-->
                                    <?php $i = 1;  foreach ($visits as $visit) {

                                    $p_subtype1 =explode(',',$visit->user_id);
                                    //                                    $image_file =explode(',',$visit->image);
                                    $st1 = '';
                                    foreach ($p_subtype1 as $stid1) {
                                        $stval1 = DB::table('crm_user_manager')->where('id', $stid1)->first();
                                        if($stval1 !== null)
                                            $st1 = $stval1->name;
                                    }

                                    ?>
                                    <tr>
                                        <td><?=$i?></td>
                                        <td><?=$visit->select_date?></td>
                                        <td><?=$i?></td>

                                        <td><?=$visit->description?></td>
                                        <td>

                                            <a href="../assets/crm/images/visits/<?=$i?>"><img src="../assets/crm/images/visits/<?=$i?>" width="30px" height="30px"/></a></td>

                                        <td>
                                            <button value="<?=$visit->id?>" class="btn myred waves-light vdelete" style="padding:0 10px;"><i class="material-icons">delete</i></button>
                                            <a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="visit-edit?id=<?=$visit->id?>&cid=<?=isset($_GET['cid']) ? $_GET['cid'] : '';?>"><i class="material-icons">edit</i></a>

                                        </td>
                                    </tr>
                                    <?php $i++;
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <?php
                    if($customer->designation) {
                        $singledata = DB::table('crm_contax')->where('id', $customer->designation)->first();
                    }
                    ?>
                    <div id="Communication"  class="col-lg-12 col-xs-12">

                        <ul class="tabs">
                            <li class="tab col s4 p-0"><a class="active p-0" href="#Call">Call</a></li>
                            <li class="tab col s4 p-0"><a class="p-0" href="#Email">Email</a></li>
                            <li class="tab col s4 p-0"><a class="p-0" href="#Message">Message</a></li>
                        </ul>

                        <div id="Message" style="margin-top: 30px;">
                            <pre></pre>
                            <div class="Message_form">
                                <form method="post" action="customer-profile" enctype="multipart/form-data" class="col s12">
                                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                    <input type="hidden" name="crm_cust_id" value="<?php echo  $customer->id; ?>">
                                    <div class="row">
                                        <div class="input-field col s12 m6">
                                            <i class="material-icons prefix">account_circle</i>
                                            <select name="conperson2" id="conperson2" class="conperson">
                                                <option value="<?= $customer->id; ?>"><?= $customer->name; ?></option>
                                                <?php
                                                if($customer->sec_names){ $names = unserialize($customer->sec_names);
                                                foreach($names as $key=>$name){ ?>
                                                <option value="<?= $key; ?>"><?= $name; ?></option>
                                                <?php } }?>
                                            </select>
                                            <label for="conperson">Contact Person</label>
                                        </div>
                                        <div class="input-field col s12 m6">
                                            <i class="material-icons prefix">account_circle</i>
                                            <input type="text" name="contactdesig2" id="contactdesig2" class="contact-designation" <?php if(!empty($singledata)){?>value="<?=$singledata->name; } ?>">
                                            <label for="contactdesig2">Contact Desigantion</label>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="input-field col s12 m6">
                                            <i class="material-icons prefix">account_circle</i>
                                            <input type="tel" class="validate customer_no1" id="customer_no" name="customer_no" value="<?php echo  $customer->contact_no;?>">
                                            <label for="customer_no">Customer No</label>
                                        </div>
                                        <div class="input-field col s12 m6">
                                            <i class="material-icons prefix">account_circle</i>
                                            <input type="tel" class="validate landline_no" id="landline_no" name="landline_no" value="<?=$customer->landline?>">
                                            <label for="landline_no">Landline No</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12 m6">
                                            <i class="material-icons prefix">email</i>
                                            <input id="message" type="text" class="validate" name="message">
                                            <label for="message">Message</label>
                                        </div>
                                        <div class="input-field col s12 m6" style="margin-top: 0px;">
                                            <i class="material-icons prefix" style="margin-top: 10px">person_add</i>
                                            <div class="col s12 m12" style="margin-left: 20px">
                                                Select User

                                                <select class="browser-default user" name="user[]" multiple tabindex="-1" style="width: 100% !important;" required> type
                                                    <?php foreach ($users as $user){ ?>
                                                    <option <?php if($user_name==$user->u_id){echo "selected";} ?> value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12 m12" style="text-align: center">
                                            <button class="btn myblue waves-light send_msg" style="padding:0 5px;" type="submit" name="add_msg">Send
                                                <i class="material-icons right">save</i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="divider"></div>
                            <h6>All Message</h6>
                            <div class="row">
                                <div class="col s12 m12 table-responsive">
                                    <table id="message_table" class="display" style="width: 100%;">
                                        <thead>
                                        <tr role="row">
                                            <th>Sr.No.</th>
                                            <th>user</th>
                                            <th>Message</th>
                                            <th>Date</th>
                                            <th>edited_by</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="Email" style="margin-top: 30px;">
                            <pre></pre>
                            <div class="Email_form">
                                <form method="post" action="customer-profile" enctype="multipart/form-data" class="col s12 m12">
                                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                    <input type="hidden" name="crm_cust_id" value="<?php echo  $customer->id; ?>">
                                    <div class="row">
                                        <div class="input-field col s12 m6">
                                            <i class="material-icons prefix">account_circle</i>
                                            <select name="conperson1" id="conperson1" class="conperson">
                                                <option value="<?= $customer->id; ?>"><?= $customer->name; ?></option>
                                                <?php if($customer->sec_names){
                                                $names = unserialize($customer->sec_names);
                                                foreach($names as $key=>$name){ ?>
                                                <option value="<?= $key; ?>"><?= $name; ?></option>
                                                <?php } }?>
                                            </select>
                                            <label for="conperson">Contact Person</label>
                                        </div>
                                        <div class="input-field col s12 m6">
                                            <i class="material-icons prefix">account_circle</i>
                                            <input type="text" name="contactdesig1" id="contactdesig1" class="contact-designation" <?php if(!empty($singledata)){ ?>value="<?=$singledata->name; } ?>">
                                            <label for="contactdesig1">Contact Desigantion</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12 m6">
                                            <i class="material-icons prefix">contact_mail</i>
                                            <input type="text" class="validate " id="customer_email" name="customer_email" value="<?php echo  $customer->email;?>">
                                            <label for="customer_email">Customer Email</label>
                                        </div>
                                        <div class="input-field col s12 m6">
                                            <i class="material-icons prefix">comment</i>
                                            <input type="text" class="validate" id="email_subject" name="email_subject">
                                            <label for="email_subject">Subject</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12 m6">
                                            <i class="material-icons prefix">email</i>
                                            <input id="message1" type="text" class="validate" name="message">
                                            <label for="message1">Message</label>
                                        </div>
                                        <div class="input-field col s12 m6" style="margin-top: 0px;">
                                            <i class="material-icons prefix" style="margin-top: 10px">person_add</i>
                                            <div class="col s12 m12" style="margin-left: 20px">
                                                Select User
                                                <select class="browser-default user" name="user[]" multiple tabindex="-1" style="width: 100% !important;" required> type
                                                    <?php foreach ($users as $user){ ?>
                                                    <option <?php if($user_name==$user->u_id){echo "selected";} ?> value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12 m12" style="text-align: center">
                                            <button class="btn myblue waves-light" style="padding:0 5px;" type="submit" name="add_notes">Send
                                                <i class="material-icons right">save</i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="divider"></div>
                            <h6>All Email</h6>
                            <div class="row">
                                <div class="col s12 table-responsive">
                                    <table id="email_table" class="display" style="width: 100%;">
                                        <thead>
                                        <tr role="row">
                                            <th>Sr.No.</th>
                                            <th>user</th>
                                            <th>Date</th>
                                            <th>Message</th>
                                            <th>edited_by</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="Call" style="margin-top: 30px;">
                            <pre></pre>
                            <div class="Call_form">
                                <form method="post" action="customer-profile" id = "submitthis" enctype="multipart/form-data" class="col s12 m12">
                                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                    <input type="hidden" name="crm_cust_id" id ="crm_cust_id" value="<?php echo  $customer->id; ?>">
                                    <div class="row">
                                        <div class="input-field col s12 m4">
                                            <i class="material-icons prefix">account_circle</i>
                                            <select name="conperson" id="conperson" class="conperson">
                                                <option value="<?= $customer->id; ?>"><?= $customer->name; ?></option>
                                                <?php
                                                if($customer->sec_names){ $names = unserialize($customer->sec_names);
                                                foreach($names as $key=>$name){
                                                ?>
                                                <option value="<?= $key; ?>"><?= $name; ?></option>
                                                <?php } }?>
                                            </select>
                                            <label for="conperson">Contact Person</label>
                                        </div>
                                        <div class="input-field col s12 m4">
                                            <i class="material-icons prefix">account_circle</i>
                                            <input type="tel" class="validate landline_no" id="landline_no1" name="landline_no" value="<?=$customer->landline?>">
                                            <label for="landline_no">Landline No</label>
                                        </div>
                                        <div class="input-field col s12 m4">
                                            <i class="material-icons prefix">account_circle</i>
                                            <input type="text" name="contact-designation" id="contact-designation" class="validate contact-designation" <?php if(!empty($singledata)) { ?>value="<?=$singledata->name; } ?>">
                                            <label for="contact-designation">Contact Designation</label>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="input-field col s12 m6">
                                            <i class="material-icons prefix">account_circle</i>
                                            <input type="tel" class="validate customer_no1" id="customer_no3" name="customer_no" value="<?php echo  $customer->contact_no;?>">
                                            <label for="customer_no1">Customer No</label>
                                        </div>
                                        <div class="input-field col s12 m6">
                                            <i class="material-icons prefix">watch_later</i>
                                            <input type="date" class="datepicker" id="dob3" name="select_date">
                                            <label for="dob3">Date</label>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12 m6">
                                            <i class="material-icons prefix">content_paste</i>
                                            <input id="message_call" type="text" class="validate" name="message" required>
                                            <label for="message_call">Notes</label>
                                        </div>
                                        <div class="input-field col s12 m6" style="margin-top: 0px;">
                                            <i class="material-icons prefix" style="margin-top: 10px">person_add</i>
                                            <div class="col s12 m12" style="margin-left: 20px">
                                                Select User

                                                <select class="browser-default user" name="user[]" multiple tabindex="-1" style="width: 100% !important;" required> type
                                                    <?php

                                                    foreach ($users as $user){

                                                    ?>
                                                    <option <?php if($user_name==$user->u_id){echo "selected";} ?> value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">list</i>
                                        <select name="ctype" class="ctype" required> type
                                            <option value="" disabled selected>Select Call Type</option>
                                            <option value="1">Incoming</option>
                                            <option value="2" selected>Outgoing</option>
                                            <option value="3">Missed call</option>
                                        </select>
                                        <label for="ptype">Call Type</label>
                                    </div>
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix" style="margin-top: 10px">call</i>
                                        <input class="timepicker form-control" type="text" name="time">
                                        <label for="time">Call Time</label>

                                    </div>
                                    <div class="input-field col s12 m12" style="margin-top: 0px;">
                                        <i class="material-icons prefix" style="margin-top: 10px">view_headline</i>
                                        <div class="col s12 m12" style="margin-left: 20px">
                                            Select Call No Order Reason

                                            <select class="browser-default no_order_reason" name="no_order_reason[]" multiple tabindex="-1" style="width: 100% !important;" > type

                                                <?php foreach ($no_order_reasons as $reason) { ?>
                                                <option value="<?php echo $reason->id ?>"><?php echo $reason->reason ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="input-field col s12 m4" style="margin-top: 0px;">
                                        <i class="material-icons prefix" style="margin-top: 10px">view_headline</i>
                                        <div class="col s12 m12" style="margin-left: 20px">
                                            Add Pipeline

                                            <select class="browser-default stagebyname" name="name"  tabindex="-1" style="width: 100% !important;" > type
                                                <option value="">Select Name</option>
                                                <?php foreach ($sales_pipelines as $pipelines) { ?>
                                                <option value="<?php echo $pipelines->id ?>"><?php echo $pipelines->name ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="input-field col s12 m4" style="margin-top: 0px;">
                                        <i class="material-icons prefix" style="margin-top: 10px">view_headline</i>
                                        <div class="col s12 m12" style="margin-left: 20px">
                                            Pipeline Stage
                                            <select class="browser-default stage" name="stage" tabindex="-1" style="width: 100% !important;" > type
                                                <option value="">Select Stage</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="input-field col s12 m4" style="margin-top: 0px;">
                                        <i class="material-icons prefix" style="margin-top: 10px">view_headline</i>
                                        <div class="col s12 m12" style="margin-left: 20px">
                                            Product Category
                                            <select class="browser-default product_category" name="product_category[]" multiple tabindex="-1" style="width: 100% !important;" > type

                                                <?php foreach ($product_category as $product_cat) {
                                                ?>
                                                <option value="<?php echo $product_cat->id ?>"><?php echo $product_cat->name ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="input-field col s12 m12" id="lead_no_order_reason" style="margin-top: 0px; display:none">
                                        <i class="material-icons prefix" style="margin-top: 10px">view_headline</i>
                                        <div class="col s12 m12" style="margin-left: 20px">
                                            Select Lead No Order Reason
                                            <select class="browser-default lead_no_order_reason" name="lead_no_order_reason[]"  multiple tabindex="-1" style="width: 100% !important;"> type
                                                <option value="">Select Leads No Order Reason</option>
                                                <?php foreach ($no_order_reasons as $reason) { ?>
                                                <option value="<?php echo $reason->id ?>"><?php echo $reason->reason ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                            </div>
                            <div class="row">

                                <div class="input-field col s12 m12" style="text-align: center">
                                    <button class="btn myblue waves-light desablebutton" style="padding:0 5px;" type="submit" name="add_call" >Save
                                        <i class="material-icons right">save</i>
                                    </button>
                                </div>
                            </div>
                            </form>
                            <input type="text" style="display:none" id="noorderselected" val="">
                            <input type="text" style="display:none" id="pipelineselected" val="">
                            <input type="text" style="display:none" id="stageselected" val="">
                            <input type="text" style="display:none" id="categoryselected" val="">
                        </div>
                        <div class="divider"></div>
                        <h6>All Call</h6>
                        <div class="row">
                            <div class="col s12 table-responsive">
                                <table id="call_table" class="display" style="width: 100%;">
                                    <thead>
                                    <tr role="row">
                                        <th>Sr.No.</th>
                                        <th>Conctact Person</th>
                                        <th>Contact Number</th>
                                        <th>Contact Designation</th>
                                        <th>user</th>
                                        <th>Date</th>
                                        <th>Message</th>
                                        <th>Pipeline</th>
                                        <th>edited_by</th>
                                        <th>Time</th>
                                        <th>Created at</th>
                                        <th>Duration</th>
                                        <th>Call Type</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                </table>
                            </div>
                        </div>
                    </div>
                    <div id="Lead_Order" style="display: none;" class="col-lg-12 col-xs-12">
                        <div class="Lead_form">
                            <form method="post" action="customer-profile" enctype="multipart/form-data" class="col s12 m12">
                                <div class="card">
                                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                    <input type="hidden" name="crm_cust_id" value="<?php echo  $customer->id; ?>">
                                    <div class="row">
                                        <div class="input-field col s12 m6">
                                            <i class="material-icons prefix">watch_later</i>
                                            <input type="text" class="datepicker" id="dob3" name="select_date" required>
                                            <label for="dob3">Date</label>
                                        </div>
                                        <div class="input-field col s12 m6">
                                            <i class="material-icons prefix" style="margin-top: 10px">view_headline</i>
                                            <div class="col s12 m12" style="margin-left: 20px">
                                                Add Pipeline

                                                <select class="browser-default stagebyname" name="name"  tabindex="-1" style="width: 100% !important;" required> type
                                                    <option value="">Select Name</option>
                                                    <?php foreach ($sales_pipelines as $pipelines) { ?>
                                                    <option value="<?php echo $pipelines->id ?>"><?php echo $pipelines->name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12 m6">
                                            <i class="material-icons prefix" style="margin-top: 10px">view_headline</i>
                                            <div class="col s12 m12" style="margin-left: 20px">
                                                Pipeline Stage
                                                <select class="browser-default stage" name="stage"  tabindex="-1" style="width: 100% !important;" required> type
                                                    <option value="">Select Stage</option>

                                                </select>
                                            </div>
                                        </div>
                                        <div class="input-field col s12 m6">
                                            <i class="material-icons prefix" style="margin-top: 10px">view_headline</i>
                                            <div class="col s12 m12" style="margin-left: 20px">
                                                Product Category

                                                <select class="browser-default product_category" name="product_category[]" multiple tabindex="-1" style="width: 100% !important;" required> type

                                                    <?php foreach ($product_category as $product_cat) {
                                                    ?>
                                                    <option value="<?php echo $product_cat->id ?>"><?php echo $product_cat->name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="input-field col s12 m12" id="lead_reason" style="margin-top: 0px; display:none">
                                            <i class="material-icons prefix" style="margin-top: 10px">view_headline</i>
                                            <div class="col s12 m12" style="margin-left: 20px">
                                                Select Lead No Order Reason
                                                <select class="browser-default lead_reason" name="lead_reason[]"  multiple tabindex="-1" style="width: 100% !important;"> type
                                                    <option value="">Select Leads No Order Reason</option>
                                                    <?php foreach ($no_order_reasons as $reason) { ?>
                                                    <option value="<?php echo $reason->id ?>"><?php echo $reason->reason ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">

                                    <div class="input-field col s12 m12" style="text-align: center">
                                        <button class="btn myblue waves-light " style="padding:0 5px;" type="submit" name="add_lead">Save
                                            <i class="material-icons right">save</i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col s12 m12 l12">
                            <div class="card">
                                <!--<ul class="row tabs">
                                    <li class="tab col s12 m12 xl2"><a class="active p-0" href="#Activity" style="
    overflow: visible;
">Activity</a></li>
                                </ul>-->
                                <div id="Activity" class="col-lg-4 col-xs-4">
                                    <ul class="row tabs">
                                        <li class="tab col s4 m4 x4"><a class="p-0 active" href="#log" >Call Log</a></li>
                                        <li class="tab col s4 m4 x4"><a class="p-0" href="#eml" >Email</a></li>
                                        <li class="tab col s4 m4 x4"><a class="p-0" href="#msg" >Message</a></li>
                                    </ul>
                                </div>
                                <div id="log" class="col-lg-4 col-xs-4" style="margin-top: 30px; !important">
                                    <div class="row" style="margin-top: 30px; !important;">
                                        <form method="post" action="customer-profile" enctype="multipart/form-data" class="col s12">
                                            <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                            <input type="hidden" name="crm_cust_id" value="<?php echo  $customer->id; ?>">
                                            <div class="row">
                                                <div class="input-field col s12 m4">
                                                    <i class="material-icons prefix">account_circle</i>
                                                    <select name="conperson3" id="conperson3" class="conperson">
                                                        <option value="<?= $customer->id; ?>"><?= $customer->name; ?></option>
                                                        <?php
                                                        if($customer->sec_names){ $names = unserialize($customer->sec_names);
                                                        foreach($names as $key=>$name){
                                                        ?>
                                                        <option value="<?= $key; ?>"><?= $name; ?></option>
                                                        <?php } }?>
                                                    </select>
                                                    <label for="conperson">Contact Person</label>
                                                </div>
                                                <div class="input-field col s12 m4">
                                                    <i class="material-icons prefix">account_circle</i>
                                                    <input type="tel" class="validate landline_no" id="landline_no3" name="landline_no3" value="<?=$customer->landline?>">
                                                    <label for="customer_no1">Landline No</label>
                                                </div>
                                                <div class="input-field col s12 m4">
                                                    <i class="material-icons prefix">account_circle</i>
                                                    <input type="text" class="validate contact-designation" id="contact-designation3" name="contact-designation3" <?php if(!empty($singledata)) { ?>value="<?=$singledata->name; } ?>">
                                                    <label for="contact-designation3">Contact Designation</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s12 m6">
                                                    <i class="material-icons prefix">account_circle</i>
                                                    <input type="tel" class="validate customer_no1" id="customer_no1" name="customer_no" value="<?php echo  $customer->contact_no;?>">
                                                    <label for="customer_no1">Customer No</label>
                                                </div>
                                                <div class="input-field col s12 m6">
                                                    <i class="material-icons prefix">watch_later</i>
                                                    <input type="date" class="datepicker" id="dob3" name="select_date">
                                                    <label for="dob3">Date</label>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="input-field col s12 m6">
                                                    <i class="material-icons prefix">content_paste</i>
                                                    <input id="message_call" type="text" class="validate" name="message" required>
                                                    <label for="message_call">Notes</label>
                                                </div>
                                                <div class="input-field col s12 m6" style="margin-top: 0px;">
                                                    <i class="material-icons prefix" style="margin-top: 10px">person_add</i>
                                                    <div class="col s12 m12" style="margin-left: 20px">
                                                        Select User

                                                        <select class="browser-default user" name="user[]" multiple tabindex="-1" style="width: 100% !important;" required> type
                                                            <?php

                                                            foreach ($users as $user){

                                                            ?>
                                                            <option <?php if($user_name==$user->u_id){echo "selected";} ?> value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="input-field col s12 m6">
                                                <i class="material-icons prefix">list</i>
                                                <select name="ctype" class="ctype" required> type
                                                    <option value="" disabled selected>Select Call Type</option>
                                                    <option value="1">Incoming</option>
                                                    <option value="2" selected>Outgoing</option>
                                                    <option value="3">Missed call</option>
                                                </select>
                                                <label for="ptype">Call Type</label>
                                            </div>
                                            <div class="input-field col s12 m6">
                                                <i class="material-icons prefix" style="margin-top: 10px">call</i>
                                                <input class="timepicker form-control" type="text" name="time">
                                                <label for="time">Call Time</label>

                                            </div>
                                            <div class="row">

                                                <div class="input-field col s12 m12" style="text-align: center">
                                                    <button class="btn myblue waves-light " style="padding:0 5px;" type="submit" name="add_lead_call">Save
                                                        <i class="material-icons right">save</i>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                        <!--Call Log-->

                                        <!--End Call Log-->
                                    </div>
                                </div>
                                <div id="eml" style="margin-top: 30px; !important">
                                    <pre></pre>
                                    <div class="Email_form">
                                        <form method="post" action="customer-profile" enctype="multipart/form-data" class="col s12 m12">
                                            <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                            <input type="hidden" name="crm_cust_id" value="<?php echo  $customer->id; ?>">
                                            <div class="row" style="margin-top: 30px; !important;">
                                                <div class="input-field col s12 m6">
                                                    <i class="material-icons prefix">account_circle</i>
                                                    <select name="conperson5" id="conperson5" class="conperson">
                                                        <option value="<?= $customer->id; ?>"><?= $customer->name; ?></option>
                                                        <?php
                                                        if($customer->sec_names){ $names = unserialize($customer->sec_names);
                                                        foreach($names as $key=>$name){
                                                        ?>
                                                        <option value="<?= $key; ?>"><?= $name; ?></option>
                                                        <?php } }?>
                                                    </select>
                                                    <label for="conperson">Contact Person</label>
                                                </div>
                                                <div class="input-field col s12 m6">
                                                    <i class="material-icons prefix">account_circle</i>
                                                    <input type="text" name="contactdesig5" id="contactdesig5" class="contact-designation" <?php if(!empty($singledata)) { ?>value="<?=$singledata->name; } ?>">
                                                    <label for="contactdesig5">Contact Desigantion</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s12 m6">
                                                    <i class="material-icons prefix">contact_mail</i>
                                                    <input type="text" class="validate" id="customer_email" name="customer_email" value="<?php echo  $customer->email;?>">
                                                    <label for="customer_email">Customer Email</label>
                                                </div>
                                                <div class="input-field col s12 m6">
                                                    <i class="material-icons prefix">comment</i>
                                                    <input type="text" class="validate" id="email_subject" name="email_subject">
                                                    <label for="email_subject">Subject</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s12 m6">
                                                    <i class="material-icons prefix">email</i>
                                                    <input id="message1" type="text" class="validate" name="message">
                                                    <label for="message1">Message</label>
                                                </div>
                                                <div class="input-field col s12 m6" style="margin-top: 0px;">
                                                    <i class="material-icons prefix" style="margin-top: 10px">person_add</i>
                                                    <div class="col s12 m12" style="margin-left: 20px">
                                                        Select User
                                                        <select class="browser-default user" name="user[]" multiple tabindex="-1" style="width: 100% !important;" required> type
                                                            <?php foreach ($users as $user){ ?>
                                                            <option <?php if($user_name==$user->u_id){echo "selected";} ?> value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s12 m12" style="text-align: center">
                                                    <button class="btn myblue waves-light" style="padding:0 5px;" type="submit" name="add_notes">Send
                                                        <i class="material-icons right">save</i>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="divider"></div>
                                </div>
                                <div id="msg">
                                    <pre></pre>
                                    <div class="Message_form">
                                        <form method="post" action="customer-profile" enctype="multipart/form-data" class="col s12">
                                            <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                            <input type="hidden" name="crm_cust_id" value="<?php echo  $customer->id; ?>">
                                            <div class="row" style="margin-top: 30px; !important">
                                                <div class="input-field col s12 m6">
                                                    <i class="material-icons prefix">account_circle</i>
                                                    <select name="conperson6" id="conperson6" class="conperson">
                                                        <option value="<?= $customer->id; ?>"><?= $customer->name; ?></option>
                                                        <?php if($customer->sec_names){ $names = unserialize($customer->sec_names);
                                                        foreach($names as $key=>$name){ ?>
                                                        <option value="<?= $key; ?>"><?= $name; ?></option>
                                                        <?php } }?>
                                                    </select>
                                                    <label for="conperson">Contact Person</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s12 m6">
                                                    <i class="material-icons prefix">account_circle</i>
                                                    <input type="tel" class="validate customer_no1" id="lead_customer_no" name="lead_customer_no" value="<?php echo  $customer->contact_no;?>">
                                                    <label for="lead_customer_no">Customer No</label>
                                                </div>
                                                <div class="input-field col s12 m6">
                                                    <i class="material-icons prefix">account_circle</i>
                                                    <input type="text" name="contactdesig2" id="contactdesig2" class="contact-designation" <?php if(!empty($singledata)) { ?>value="<?=$singledata->name; } ?>">
                                                    <label for="contactdesig2">Contact Desigantion</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s12 m6">
                                                    <i class="material-icons prefix">email</i>
                                                    <input id="lead_message" type="text" class="validate" name="lead_message">
                                                    <label for="lead_message">Message</label>
                                                </div>
                                                <div class="input-field col s12 m6" style="margin-top: 0px;">
                                                    <i class="material-icons prefix" style="margin-top: 10px">person_add</i>
                                                    <div class="col s12 m12" style="margin-left: 20px">
                                                        Select User
                                                        <select class="browser-default user" name="lead_user[]" multiple tabindex="-1" style="width: 100% !important;" required> type
                                                            <?php foreach ($users as $user){ ?>
                                                            <option <?php if($user_name==$user->u_id){echo "selected";} ?> value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s12 m12" style="text-align: center">
                                                    <button class="btn myblue waves-light add_lead_msg" style="padding:0 5px;" type="submit" name="add_lead_msg">Send
                                                        <i class="material-icons right">save</i>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="divider"></div>
                                </div>
                            </div>
                        </div>
                        <div class="divider"></div>
                        <h6>All Lead/Order</h6>
                        <div class="row">
                            <div class="col s12 table-responsive">
                                <table id="lead_table" class="display" style="width: 100%;">
                                    <thead>
                                    <tr role="row">
                                        <th>Sr.No.</th>
                                        <th>Pipeline</th>
                                        <th>Stage</th>
                                        <th>Date</th>
                                        <th>Edited_By</th>
                                        <th>Created at</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="Products" style="display: none;" class="col-lg-12 col-xs-12">
                    <div class="product_form">
                        <form method="post" action="customer-profile" enctype="multipart/form-data" class="col s12 m12">
                            <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                            <input type="hidden" name="crm_cust_id" value="<?php echo  $customer->id; ?>">
                            <div class="row">
                                <div class="input-field col s12 m6">
                                    <i class="material-icons prefix" style="margin-top: 10px">list</i>
                                    <div class="col s12 m12" style="margin-left: 20px">
                                        Select Category
                                        <select  class="browser-default category" name="category[]" required multiple tabindex="-1" style="width: 100% !important;">
                                            <?php foreach ($product_category as $category){ ?>
                                            <option value="<?=$category->id?>"> <?=$category->name?> </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="input-field col s12 m6">
                                    <i class="material-icons prefix" style="margin-top: 10px">dns</i>
                                    <div class="col s12 m12" style="margin-left: 20px">
                                        Price Group
                                        <select  class="browser-default price_group" name="price_group[]" multiple tabindex="-1" style="width: 100% !important;">
                                            <option value="">Select Category First</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12 m6">
                                    <i class="material-icons prefix" style="margin-top: 10px">local_mall</i>
                                    <div class="col s12 m12" style="margin-left: 20px">
                                        Select Brand
                                        <select  class="browser-default brand" name="brand[]" multiple tabindex="-1" style="width: 100% !important;">
                                            <?php foreach ($brand as $bnd){
                                            ?>
                                            <option value="<?=$bnd->id?>"> <?=$bnd->name?> </option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="input-field col s12 m6">
                                    <i class="material-icons prefix" style="margin-top: 10px">transfer_within_a_station</i>
                                    <div class="col s12 m12" style="margin-left: 20px">
                                        Select Supplier
                                        <select  class="browser-default supplier" name="supplier[]" multiple tabindex="-1" style="width: 100% !important;">
                                            <?php foreach ($suppliers as $sup){ ?>
                                            <option value="<?=$sup->id?>"> <?=$sup->name?> </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12 m12">
                                    <i class="material-icons prefix">view_headline</i>
                                    <input id="Description5" type="text" class="validate" name="description">
                                    <label for="Description5">Description</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12 m6">
                                    <div class="file-field">
                                        <div class="btn myblue waves-light left" style="padding:0 5px;">
                                            <span>Choose file</span>
                                            <input type="file" name="image" style="width: 100%" class="btn myblue" id="category_image">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text" placeholder="Upload your file">
                                        </div>
                                    </div>
                                </div>
                                <div class="input-field col s12 m3">
                                    <p id="category_img_div">
                                        <img id="category_image_show" src="#" height="200px" width="200px"  />
                                    </p>
                                </div>
                                {{--<div class="input-field col s12 m3" style="text-align: center">
                                    <button class="btn myblue waves-light" style="padding:0 5px;" type="submit" name="add_product">Save
                                        <i class="material-icons right">save</i>
                                    </button>
                                </div>--}}
                            </div>
                        </form>
                    </div>
                    <div class="divider"></div>
                    <h6>All Products</h6>
                    <div class="row">
                        <div class="col s12 table-responsive">
                            <table id="products_table" class="display" style="width: 100%;">
                                <thead>
                                <tr role="row">
                                    <th>Sr.No.</th>
                                    <th>Product</th>
                                    <th>Brand</th>
                                    <th>Supplier</th>
                                    <th>Price Group</th>
                                    <th>Description</th>
                                    <th>file</th>
                                    <th>edited_by</th>
                                    <th>created_at</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="Promotional-material" style="display: none;" class="col-lg-12 col-xs-12">
                    <div class="promotional_form">
                        <form method="post" action="customer-profile" enctype="multipart/form-data" class="col s12 m12">
                            <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                            <input type="hidden" name="crm_cust_id" value="<?php echo  $customer->id; ?>">
                            <div class="row">
                                <div class="input-field col s12 m6">
                                    <i class="material-icons prefix">watch_later</i>
                                    <input type="date" class="datepicker" id="dob2" name="select_date">
                                    <label for="dob2">Date</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12 m12">
                                    <i class="material-icons prefix">view_headline</i>
                                    <input id="Description6" type="text" class="validate" name="description" required>
                                    <label for="Description6">Description</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12 m12" style="text-align: center">
                                    <button class="btn myblue waves-light" style="padding:0 5px;" type="submit" name="add_promotional">Save
                                        <i class="material-icons right">save</i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="divider"></div>
                    <h6>All Promotional Material</h6>
                    <div class="row">
                        <div class="col s12 table-responsive">
                            <table id="promotinal_material" class="display" style="width: 100%;">
                                <thead>
                                <tr role="row">
                                    <th>Sr.No.</th>
                                    <th>Date</th>
                                    <th>Description</th>
                                    <th>edited_by</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/js/dataTables.select.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/form-layouts.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/select2.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/ui-alerts.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
</body>
</html>
<script type="text/javascript">
    $('.timepicker').datetimepicker({
        format: 'HH:mm:ss'
    });

    function checkInput(){
        $("#checkdate").val('1')
    }
    function removeBox2(index){
        $('#addMoreBox2'+index).remove();
    }

</script>
<script>
    var $insertBefore = $('#insertBefore');
    var $i = 0;
    // Add More Inputs
    $('#plusButton').click(function(e){
        e.preventDefault();
        $i = $i+1;
        var indexs = $i+1;
        //append Options
        $('<div class="row" id="addMoreBox'+indexs+'"><div class="input-field col s12 m6" id="ReferredByField" style="margin-left:-15px">' +
            '<div class="col s1 m1"> <i class="material-icons prefix">list</i></div><div class="col s11 m11 referbyclass">' +
            '<label>Referred By</label><select class="selectjs" style="display:none;width:107%" name="listcust[]"></select>' +
            '</div></div><div class="input-field col s12 m5"><div class="col s1 m1"> <i class="material-icons prefix">list</i>' +
            '</div><div class="col s11 m11" style="margin-left: 41px;width: 100%"><label>Referred by Note</label><input type="text" class="validate" name="notes[]"  required></div></div>' +
            '<div class="input-field col s12 m1" style="margin-left:11px"><button value="20" class="btn myred waves-light" onclick="removeBox1('+indexs+')" style="padding:0 10px;margin-left: 20px;">' +
            '<i class="material-icons">remove</i></button></div></div></div>').insertBefore($insertBefore);

        $(".selectjs").select2({
            ajax: {
                url: "get-customers-listdata",
                dataType: 'json',
                delay: 250,
                type : "POST",
                data: function (params) {
                    return {
                        q: params.term, // search term
                        _token:'<?=csrf_token(); ?>',
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    return {
                        results: data,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            placeholder: 'Search for a repository',
            minimumInputLength: 1,
            multiple: false,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection
        });
        function formatRepo (repo) {
            return repo.name;
            var $container = $(
                "<div class='select2-result-repository__title'></div>"
            );

            $container.find(".select2-result-repository__title").text(repo.name);
            return $container;
        }
        function formatRepoSelection (repo) {
            return repo.name || repo.name;
        }
    });
    // Remove fields
    function removeBox1(index){
        $('#addMoreBox'+index).remove();
    }

    var $insertNoteBefore = $('#insertNoteBefore');
    var $j = Number($('#totalnotecount').val());
    // Add More Inputs
    $('#plusNoteButton').click(function(e){
        e.preventDefault();
        $j = $j+1;
        var indexs = $j+1;
        //append Options
        $('<div class="row" id="addNoteMoreBox'+$j+'"><div class="input-field col s12 m3"><i class="material-icons prefix" style="margin-top: 10px">list</i><div class="col s12 m12" style="margin-left: 20px">' +
            'Select Category <select  class="browser-default category" name="cat['+$j+'][]" multiple tabindex="-1" style="width: 100% !important;">' +
            '@foreach ($product_category as $category)<option value="<?=$category->id?>"> <?=$category->name?> </option>@endforeach</select></div></div>' +
            '<div class="input-field col s12 m3"><i class="material-icons prefix" style="margin-top: 10px">local_mall</i><div class="col s12 m12" style="margin-left: 20px">' +
            'Select Brand<select  class="browser-default brand" name="bran['+$j+'][]" multiple tabindex="-1" style="width: 100% !important;">@foreach ($brand as $bnd)' +
            '<option value="<?=$bnd->id?>"> <?=$bnd->name?> </optin>@endforeach</select></div></div><div class="input-field col s12 m2"><i class="material-icons prefix" style="margin-top: 10px">dns</i>' +
            '<div class="col s12 m12" style="margin-left: 20px">monthly quantity<input type="text" name="moquant['+$j+'][]" class="validate"></div></div><div class="input-field col s12 m3">' +
            '<i class="material-icons prefix" style="margin-top: 10px">transfer_within_a_station</i><div class="col s12 m12" style="margin-left: 20px">Select Supplier' +
            '<select  class="browser-default supplier" name="supr['+$j+'][]" multiple tabindex="-1" style="width: 100% !important;">@foreach ($suppliers as $sup)' +
            '<option value="<?=$sup->id?>"> <?=$sup->name?> </option>@endforeach</select></div></div><div class="input-field col s12 m1"><button value="20" class="btn myred waves-light" onclick="removeBox('+$j+')" style="padding:0 10px;margin-left: 20px;"><i class="material-icons">remove</i></button></div></div>').insertBefore($insertNoteBefore);
        $('.category').select2();
        $('.brand').select2();
        $('.supplier').select2();
    });
    // Remove fields
    function removeBox(index){
        $('#addNoteMoreBox'+index).remove();
    }
</script>
<script>
    var ttable = false;
    var ptable = false;
    var protable = false;
    var prodtable = false;
    var calltable = false;
    var vtable = false;
    var ntable = false;
    var promotable = false;
    var msgtable = false;
    var emailtable = false;
    $(document).ready(function(){
        $('.category').select2();
        $('.pstype').select2();
        $('.brand').select2();
        $('.user').select2();
        $('.supplier').select2();
        $('.states').select2();
        $('.district').select2();
        $('.block').select2();
        $('.locality').select2();
        $('.price_group').select2();
        $('.user_unqualified').select2();
        $('.user_contacted').select2();
        $('.user_interested').select2();
        $('.user_notinterested').select2();
        $('.user_converted').select2();
        $('.user_unreachable').select2();
        $('.no_order_reason').select2();
        $('.lead_no_order_reason').select2();
        $('.lead_reason').select2();
        $('.product_category').select2();
        $('.assignto').select2();
        $('.crm_type').select2();
        $('.addoncharges').select2();
        $(".selectjs").select2({
            ajax: {
                url: "get-customers-listdata",
                dataType: 'json',
                delay: 250,
                type : "POST",
                data: function (params) {
                    return {
                        q: params.term, // search term
                        _token:'<?=csrf_token(); ?>',
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    return {
                        results: data,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            placeholder: 'Search for a repository',
            minimumInputLength: 1,
            multiple: false,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection
        });
        function formatRepo (repo) {
            return repo.name;
            var $container = $(
                "<div class='select2-result-repository__title'></div>"
            );

            $container.find(".select2-result-repository__title").text(repo.name);
            return $container;
        }
        function formatRepoSelection (repo) {
            return repo.name || repo.name;
        }

        @if(!empty($crmreffer))
        @foreach($crmreffer as $reffered)
        $('.selectjs').select2().select('val','{{$reffered->id}}');
        @endforeach
        @endif
        $('#referrer').click(function () {
            var isChecked = $("#referrer").is(":checked");
            if (isChecked) {
                $(this).val('1');
            } else {
                $(this).val('0');
            }
        });

        $(".category").change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-price-groups-ajax",
                data:'cid='+val+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $(".price_group").html(data);
                }
            });
        });

        $(".stage").change(function () {
            if($(this).val() == "lost") {
                $("#lead_no_order_reason").show();
                $("#lead_reason").show();
                $('.lead_no_order_reason').attr('required', true);
                $('.lead_reason').attr('required', true);
            }else{
                $('.lead_reason').attr('required', false);
                $('.lead_no_order_reason').attr('required', false);
                $(".lead_no_order_reason").empty();
                $(".lead_reason").empty();
                $("#lead_no_order_reason").hide();
                $("#lead_reason").hide();
            }
        });

        $(".states").change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-district-ajax",
                data:'cid='+val+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $(".district").html(data).append('<option value="" disabled selected>Select District</option>');
                }
            });
        });
        $('.district').change(function () {
            var did = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-block",
                data:'did='+did+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $(".block").html(data).append('<option value="" disabled selected>Select Block</option>');

                }
            });
        });
        $('.block').change(function () {
            var did = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-locality",
                data:'did='+did+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(".submit").html("Wait...");
                },
                success: function(data){
                    $('#locality').html(data);
                }
            });
        });

        $(".stagebyname").change(function(){
            var val = $(this).val();
            $("#pipelineselected").val(val);
            $.ajax({
                type: "POST",
                url: "get-stages-ajax",
                data:'pid='+val+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $(".stage").html(data);
                }
            });
        });
        $(".conperson").change(function () {

            <?php if(!empty($customer->sec_landline && $customer->sec_names)){
                $landlines = unserialize($customer->sec_landline);
                foreach($landlines as $key=>$landline){ ?>
            if($(this).val() == <?=$key?>) {
                $('.landline_no').val('<?=$landline?>');
            }if($(this).val() == <?=isset($_GET['cid'])?$_GET['cid']:false ?>){
                $('.landline_no').val('<?=$customer->landline?>');
            }
            <?php } } ?>
                <?php if(!empty($customer->sec_contact)){ $contacts = unserialize($customer->sec_contact);
                foreach($contacts as $key=>$contact){ ?>
            if($(this).val() == <?=$key?>) {
                $('.customer_no1').val('<?=$contact?>');
            }if($(this).val() == <?=isset($_GET['cid'])?$_GET['cid']:false ?>){
                $('.customer_no1').val('<?=$customer->contact_no?>');
            }
            <?php } } ?>
                <?php if(!empty($customer->sec_desc)){ $desigs = unserialize($customer->sec_desc);
                foreach($desigs as $key=>$desig){ ?>
            if($(this).val() == <?=$key?>) {
                <?php $singledata1 = DB::table('crm_contax')->where('id', $desig)->first(); if(!empty($singledata1)){?>
                $('.contact-designation').val('<?=$singledata1->name;?>');
                <?php } else { ?>
                $('.contact-designation').val('');
                <?php } ?>
            }if($(this).val() == <?=isset($_GET['cid'])?$_GET['cid']:false ?>){

                $('.contact-designation').val('<?php if($customer->designation &&(!empty($singledata))){ echo $singledata->name;} ?>');
            }
            <?php } } ?>
        });

        var d = new Date();
        var month = d.getMonth()+1;
        var day = d.getDate();
        var output = d.getFullYear() + '-' +
            ((''+month).length<2 ? '0' : '') + month + '-' +
            ((''+day).length<2 ? '0' : '') + day;

        $(".datepicker").val(output);
        $('.datepicker').each(function(){
            var dtpick = $(this);
            if(dtpick.hasClass('dconverted')){
                dtpick.val("<?=date('Y-m-d',strtotime($customer->conversion_date))?>");
            }else{
                dtpick.datepicker("setDate", new Date());
            }
        });

        $('.type1').select2();
        $('.variants').select2();
        $('.ticket_form').hide();

        $('.ticket_form_button').click(function(){
            $('.ticket_form').show();
        });

        $(".category").change(function(){
            var val = $(this).val();
            var cname = $(".category option:selected").html();
            $.ajax({
                type: "POST",
                url: "get-report-option",
                data:'cid='+val+'&cname='+cname+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $("#search-box").css("background","#FFF url(assets/LoaderIcon.gif) no-repeat 165px");
                },
                success: function(data){
                    $(".report_div").html(data);
                    $("#model_report_type").val(data);
                }
            });
        });
        var get_view_type = $('#get_report_view').val();
        if(get_view_type=='brand_wise'){
            $('.brand_wise').show();
        }
        if(get_view_type=='price_wise'){
            $('.price_wise').show();
        }

        $('#visit_img_div').hide();
        function readURLVisit(input) {
            $('#visit_img_div').show();
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#visit_image_show').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#visit_image").change(function(){
            readURLVisit(this);
        });

        $('#ticket_img_div').hide();
        function readURLTicket(input) {
            $('#ticket_img_div').show();
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#ticket_image_show').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#ticket_image").change(function(){
            readURLTicket(this);
        });

        $('#notes_img_div').hide();
        function readURLNotes(input) {
            $('#notes_img_div').show();
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#notes_image_show').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#notes_image").change(function(){
            readURLNotes(this);
        });

        $('#project_img_div').hide();
        function readURLproject(input) {
            $('#project_img_div').show();
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#project_image_show').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#project_image").change(function(){
            readURLproject(this);
        });

        $('#category_img_div').hide();
        function readURLcategory(input) {
            $('#category_img_div').show();
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#category_image_show').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#category_image").change(function(){
            readURLcategory(this);
        });

        $('.change_status').click(function () {
            var val = $(this).val();
            var cid = $(".cust_id").val();
            var user_unqualified = $(".user_unqualified").val();
            var user_notinterested = $(".user_notinterested").val();
            var user_contacted = $(".user_contacted").val();
            var user_interested = $(".user_interested").val();
            var user_converted = $(".user_converted").val();
            var user_unreachable = $(".user_unreachable").val();
            var ureason = $("#ureason").val();
            var dunreachable = $("#dunreachable").val();
            var dunqualied = $(".dunqualied").val();
            var reason = $("#reason").val();
            var notreason = $("#notreason").val();
            var dunqualied = $("#dunqualied").val();
            var dnotinterest = $("#dnotinterest").val();
            var dcontacted = $("#dcontacted").val();
            var dinterested = $("#dinterested").val();
            var dconverted = $("#dconverted").val();
            $.ajax({
                type: "POST",
                url: "customer-class-update",
                data:'cid='+cid+'&class='+val+'&ureason='+ureason+'&user_unreachable='+user_unreachable+'&dunreachable='+dunreachable+'&reason='+reason+'&dunqualied='+dunqualied+'&user_unqualified='+user_unqualified+'&user_notinterested='+user_notinterested+'&user_contacted='+user_contacted+'&user_interested='+user_interested+'&notreason='+notreason+'&user_converted='+user_converted+'&dnotinterest='+dnotinterest+'&dcontacted='+dcontacted+'&dinterested='+dinterested+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    window.location.href = "customer-profile?cid="+cid;

                }
            });
        });

        $(".no_order_reason").change(function(){
            var val = $(this).val();
            $("#noorderselected").val(val);
        });

        $(".stage").change(function(){
            var val = $(this).val();
            $("#stageselected").val(val);
        });

        $(".product_category").change(function(){
            var val = $(this).val();
            $("#categoryselected").val(val);
        });

        $('.desablebutton').click(function(){
            var noorderselected = $("#noorderselected").val();
            var pipelineselected = $("#pipelineselected").val();
            var stageselected = $("#stageselected").val();
            var categoryselected = $("#categoryselected").val();
            if((noorderselected !== '') && (pipelineselected !== '' ||  stageselected !== ''   ||  categoryselected !== '')){
                alert('Select No Order Reason Or Pipelines Section Only');
                return false;
            }else if(noorderselected === '' && (pipelineselected === '' ||  stageselected === ''   ||  categoryselected === '')){
                alert('Select Pipelines Sections ');
                return false;
            }
        });

        $('.add_lead_msg').click(function () {
            var mob = $.trim($("#customer_no").val());
            var cname = encodeURIComponent($("#lead_message").val());
            var cid = $(".cust_id").val();
            if(mob==''){
                alert('Please Enter Number');
                return false;
            }
            else{
                $.ajax({
                    type: "GET",
                    url: "https://merasandesh.com/api/sendsms",
                    data:'username=smshop&password=Smshop@123&senderid=SMSHOP&message='+cname+'&numbers='+mob+'&unicode=0',
                    async: true,
                    beforeSend: function(){
                        $('.send_msg').html('Wait...');
                        $.ajax({
                            type: "POST",
                            url: "msg_save",
                            data:'message='+cname+'&numbers='+mob+'&_token=<?=csrf_token(); ?>',
                            success: function(data){

                            }
                        });
                    },
                    success: function(data){},
                    error: function(){},
                    complete: function(data){
                        location.reload();
                    }
                });
            }
        });

        $('.send_msg').click(function () {
            var mob = $.trim($("#lead_customer_no").val());
            var cname = encodeURIComponent($("#message").val());
            var cid = $(".cust_id").val();
            if(mob==''){
                alert('Please Enter Number');
                return false;
            }
            else{
                $.ajax({
                    type: "GET",
                    url: "https://merasandesh.com/api/sendsms",
                    data:'username=smshop&password=Smshop@123&senderid=AALERT&message='+cname+'&numbers='+mob+'&unicode=0',
                    async: true,
                    beforeSend: function(){
                        $('.send_msg').html('Wait...');
                        $.ajax({
                            type: "POST",
                            url: "msg_save",
                            data:'message='+cname+'&numbers='+mob+'&_token=<?=csrf_token(); ?>',
                            success: function(data){}
                        });
                    },
                    success: function(data){},
                    error: function(){},
                    complete: function(data){
                        location.reload();
                    }
                });
            }
        });

        $('.ptype').change(function () {
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-subtype",
                data:'tid='+val+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $('.pstype').html(data);
                }
            });
        });

        $(document).on('click', '.pdelete', function () {
            var pid = $(this).val();
            var cid = $(".cust_id").val();
            var a = $(this);
            $.ajax({
                type: "POST",
                url: "customer-project-delete",
                data:'pid='+pid+'&cid='+cid+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(a).html('Wait...');
                },
                success: function(data){
                    alert(data);
                    if(ptable){
                        $('#projects-table').DataTable().ajax.reload();
                    }else{
                        location.reload();
                    }
                }
            });
        });

        $(document).on('click', '.tdelete', function () {
            var pid = $(this).val();
            var cid = $(".cust_id").val();
            var a = $(this);
            $.ajax({
                type: "POST",
                url: "customer-ticket-delete",
                data:'pid='+pid+'&cid='+cid+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(a).html('Wait...');
                },
                success: function(data){
                    if(ttable){
                        $('#tickets-table').DataTable().ajax.reload();
                    }else{
                        location.reload();
                    }
                }
            });
        });

        $(document).on('click', '.ndelete', function () {
            var pid = $(this).val();
            var cid = $(".cust_id").val();
            var a = $(this);
            $.ajax({
                type: "POST",
                url: "customer-note-delete",
                data:'pid='+pid+'&cid='+cid+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(a).html('Wait...');
                },
                success: function(data){
                    alert(data);
                    if(ntable){
                        $('#notes-table').DataTable().ajax.reload();
                    }else{
                        location.reload();
                    }
                }
            });
        });

        $(document).on('click', '.vdelete', function () {
            var pid = $(this).val();
            var cid = $(".cust_id").val();
            var a = $(this);
            $.ajax({
                type: "POST",
                url: "customer-visit-delete",
                data:'pid='+pid+'&cid='+cid+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(a).html('Wait...');
                },
                success: function(data){
                    alert(data);
                    if(vtable){
                        $('#visits-table').DataTable().ajax.reload();
                    }else{
                        location.reload();
                    }
                }
            });
        });

        $(document).on('click', '.calldelete', function () {
            var pid = $(this).val();
            var lead_id = $(this).attr('data-id');
            var cid = $(".cust_id").val();
            var a = $(this);
            $.ajax({
                type: "POST",
                url: "customer-call-delete",
                data:'pid='+pid+'&cid='+cid+'&lead_id='+lead_id+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(a).html('Wait...');
                },
                success: function(data){
                    console.log(data);
                    $('#calls-table').DataTable().ajax.reload();
                    location.reload();
                }
            });
        });

        $(document).on('click', '.leaddelete', function () {
            var lid = $(this).val();
            var cid = $(".cust_id").val();
            var a = $(this);
            $.ajax({
                type: "POST",
                url: "customer-lead-delete",
                data:'lid='+lid+'&cid='+cid+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(a).html('Wait...');
                },
                success: function(data){
                    alert(data);
                    $('#lead-table').DataTable().ajax.reload();
                    location.reload();
                }
            });
        });

        $(document).on('click', '.prodelete', function () {
            var pid = $(this).val();
            var cid = $(".cust_id").val();
            var a = $(this);
            $.ajax({
                type: "POST",
                url: "customer-promotional-delete",
                data:'pid='+pid+'&cid='+cid+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(a).html('Wait...');
                },
                success: function(data){
                    if(protable){
                        $('#promos-table').DataTable().ajax.reload();
                    }else{
                        location.reload();
                    }
                }
            });
        });

        $(document).on('click','.proddelete', function (e) {
            e.preventDefault();
            var x = confirm("Are you sure you want to delete?");
            if (x) {
                var pid = $(this).val();
                var cid = $(".cust_id").val();
                var a = $(this);
                $.ajax({
                    type: "POST",
                    url: "customer-product-delete",
                    data: 'pid=' + pid + '&cid=' + cid + '&_token=<?=csrf_token(); ?>',
                    beforeSend: function () {
                        $(a).html('Wait...');
                    },
                    success: function (data) {
                        $('#products-table').DataTable().ajax.reload();
                        location.reload();
                    }
                });
            }
        });
        $('#month').on('change', function(){
            $('#timeline_form').submit();
        });
        $('.update_reason').on('click', function(){
            var status = $(this).data('value');
            if(status == 5){
                $('#open-modal2 h1').html('UNQUALIFIED');
                $('#open-modal2 .change_status').attr('value', 5);
                $('#open_reason').trigger('click');
            }else if(status == 6){
                $('#open-modal2 h1').html('NOT INTERESTED');
                $('#open-modal2 .change_status').attr('value', 6);
                $('#open_reason').trigger('click');
            }
        });
        $('#localitySave').on('click', function(){
            var formData = $('form#localityForm').serialize();
            var btn = $(this);
            $.ajax({
                url: 'save-locality',
                type: 'post',
                data: formData,
                beforeSend: function(){
                    btn.html('Saving...');
                },
                success: function(data){
                    if(data == 'success') {
                        alert('Locality Saved.');
                    }else{
                        alert('Invalid Data.');
                    }
                    btn.html('Save');
                }
            });
        });
        $('#manageBySave').on('click', function(){
            var formData = $('form#manageForm').serialize();
            var btn = $(this);
            $.ajax({
                url: 'save-manageBySave',
                type: 'post',
                data: formData,
                beforeSend: function(){
                    btn.html('Saving...');
                },
                success: function(data){
                    if(data == 'success') {
                        alert('manageBySave Saved.');
                    }
                    btn.html('Save');
                }
            });
        });
    });
</script>
<script>
    $(function() {
        ttable = $('#tickets-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?=url("crm/get-tickets-data") ?>",
                type: 'GET',
                data: {cid:'<?=isset($_GET['cid']) ? $_GET['cid'] : ''?>'}
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'type', name: 'type' },
                { data: 'priority', name: 'priority' },
                { data: 'description', name: 'description'},
                { data: 'image', name: 'image', orderable: false, searchable: false },
                { data: 'acc_manager', name: 'acc_manager', searchable: false, sortable:false },
                { data: 'created_date', name: 'created_date'},
                { data: 'due_date', name: 'due_date',searchable: false },
                { data: 'status', name: 't_visits', searchable: false },
                { data: 'edited_by', name: 'edited_by', searchable: false },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });
    });
    $(function() {
        ntable = $('#notes-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?=url("crm/get-notes-data") ?>",
                type: 'GET',
                data: {cid:'<?=isset($_GET['cid']) ? $_GET['cid'] : ''?>'}
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'select_date', name: 'select_date' },
                { data: 'description', name: 'description'},
                { data: 'image', name: 'image', orderable: false, searchable: false },
                { data: 'edited_by', name: 'edited_by', searchable: false },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });
    });
    $(function() {
        ptable = $('#projects-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?=url("crm/get-projects-data") ?>",
                type: 'GET',
                data: {cid:'<?=isset($_GET['cid']) ? $_GET['cid'] : ''?>'}
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'project_name', name: 'project_name' },
                { data: 'project_type', name: 'crm_project_type.project_type' },
                { data: 'project_subtype', name: 'project_subtype'},
                { data: 'sqft', name: 'sqft'},
                { data: 'description', name: 'description'},
                { data: 'image', name: 'image', orderable: false, searchable: false },
                { data: 'edited_by', name: 'edited_by'},
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });
    });
    $(function() {
        vtable = $('#visits-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?=url("crm/get-visits-data") ?>",
                type: 'GET',
                data: {cid:'<?=isset($_GET['cid']) ? $_GET['cid'] : ''?>'}
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'select_date', name: 'select_date'},
                { data: 'user_id', name: 'user_id'},
                { data: 'description', name: 'description'},
                { data: 'image', name: 'image', orderable: false, searchable: false },
                { data: 'edited_by', name: 'edited_by', searchable: false },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });
        prodtable = $('#products_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?=url("crm/get-products-data") ?>",
                type: 'GET',
                data: {cid:'<?=isset($_GET['cid']) ? $_GET['cid'] : ''?>'}
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'product_id', name: 'product_id'},
                { data: 'brands', name: 'brands'},
                { data: 'supplier_id', name: 'supplier_id'},
                { data: 'price_groups', name: 'price_groups'},
                { data: 'description', name: 'description'},
                { data: 'image', name: 'image', orderable: false, searchable: false },
                { data: 'edited_by', name: 'edited_by', searchable: false },
                { data: 'created_at', name: 'created_at' },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });
        promotable = $('#promotinal_material').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?=url("crm/get-promotion-data") ?>",
                type: 'GET',
                data: {cid:'<?=isset($_GET['cid']) ? $_GET['cid'] : ''?>'}
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'select_date', name: 'select_date'},
                { data: 'description', name: 'description'},
                { data: 'edited_by', name: 'edited_by', searchable: false },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });
        msgtable = $('#message_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?=url("crm/get-message-data") ?>",
                type: 'GET',
                data: {cid:'<?=isset($_GET['cid']) ? $_GET['cid'] : ''?>'}
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'user_id', name: 'user_id'},
                { data: 'message', name: 'message'},
                { data: 'created_at', name: 'created_at'},
                { data: 'edited_by', name: 'edited_by', searchable: false }

            ]
        });
        calltable = $('#call_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?=url("crm/get-call-data") ?>",
                type: 'GET',
                data: {cid:'<?=isset($_GET['cid']) ? $_GET['cid'] : ''?>'}
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'contact_person', name: 'contact_person'},
                { data: 'customer_number', name: 'customer_number'},
                { data: 'contact_designation', name: 'contact_designation'},
                { data: 'user_id', name: 'user_id'},
                { data: 'select_date', name: 'select_date'},
                { data: 'message', name: 'message'},
                { data: 'pipeline', name: 'pipeline'},
                { data: 'edited_by', name: 'edited_by', searchable: false },
                { data: 'time', name: 'time', searchable: false },
                { data: 'created_date', name: 'created_date', searchable: false },
                { data: 'duration', name: 'duration', searchable: false,
                    "render": function (data, type, row) {
                        var seconds = row.duration ;
                        if(seconds!==null){
                            var date = new Date(null);
                            date.setSeconds(seconds); // specify value for SECONDS here
                            var result = date.toISOString().substr(11, 8);
                            return result;
                        }
                        else{
                            return seconds;
                        }
                    }
                },

                { data: 'call_type', name: 'call_type', searchable: false,
                    "render": function (data, type, row) {
                        switch(row.call_type) {
                            case '1' : return 'Incoming Call'; break;
                            case '2' : return 'Outgoing Call'; break;
                            case '3' : return 'Missed Call'; break;
                            default  : return 'N/A';
                        }
                    }
                },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ],
            order: [[0, 'desc']],
        });
        leadtable = $('#lead_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?=url("crm/get-leave_order-data") ?>",
                type: 'GET',
                data: {cid:'<?=isset($_GET['cid']) ? $_GET['cid'] : ''?>'}
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'pipeline', name: 'pipeline'},
                { data: 'stage', name: 'stage'},
                { data: 'select_date', name: 'select_date'},
                { data: 'edited_by', name: 'edited_by', searchable: false },
                { data: 'created_date', name: 'created_date', searchable: false },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ],
            order: [[0, 'desc']],
        });
    });
</script>
<div id="open-modal2" class="modal-window">
    <div>
        <a href="#modal-close" title="Close" class="modal-close">close &times;</a>
        <h1>UNQUALIFIED </h1>
        <div>
            <table border="0">
                <form action="customer-profile" method="post">
                    <input type="hidden" id="model_report_type" value="<?php ?>"  name="report_type">
                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                    <tr>
                        <td>Reason</td>
                        <td ><input type="text" id="reason" name="reason" class="form-control" value="<?=$customer->unqualified_reason;?>"></td>
                    </tr>
                    <tr>
                        <td>Select User</td>
                        <td >
                            <div class="input-field col s12 m6" style="margin-top: 0px;">
                                <div class="col s12 m12" style="margin-left: 20px">
                                    <select class="browser-default user_unqualified" name="user[]" multiple tabindex="-1" style="width: 100% !important;" required> type
                                        <?php foreach ($users as $user){ ?>
                                        <option <?php if($user_name==$user->u_id){echo "selected";} ?> value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td >
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">watch_later</i>
                                <input type="text" class="datepicker" required id="dunqualied" name="date">
                                <label for="dob">Date</label>
                                <label for="dob">Date</label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><button type="button" value="5" class="btn myblue change_status">Save</td>
                    </tr>
                </form>
            </table>
        </div>
    </div>
</div>
<div id="open-modal9" class="modal-window">
    <div>
        <a href="#modal-close" title="Close" class="modal-close">close &times;</a>
        <h1>Add On Charges</h1>
        <div>
            <table border="0">
                <form action="customer-profile" method="post">
                    <input type="hidden" id="model_report_type" value="<?php ?>"  name="report_type">
                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                    <tr>
                        <td>Add On Charges</td>
                        <td ><input type="text" id="addoncharge" name="addoncharge" class="form-control" value=""></td>
                    </tr>
                    <tr>
                        <td>Select Category</td>
                        <td >
                            <div class="input-field col s12 m6" style="margin-top: 0px;">
                                <div class="col s12 m12" style="margin-left: 20px">
                                    <select class="browser-default" id="chargecategory" name="chargecategory[]"  tabindex="-1" style="width: 100% !important;" required> type
                                        <option value="">Select Category</option>
                                        <?php foreach ($product_category as $category){ ?>
                                            <option value="{{$category->name}}">{{$category->name}}</option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><button type="submit" name="add_on_charge" class="btn myblue addOnCharge">Save</td>
                    </tr>
                </form>
            </table>
        </div>
    </div>
</div>

<div id="open-modal4" class="modal-window">
    <div>
        <a href="#modal-close" title="Close" class="modal-close">close &times;</a>
        <h1>NOT Interested </h1>
        <div>
            <table border="0">
                <form action="customer-profile" method="post">
                    <input type="hidden" id="model_report_type" value="<?php ?>"  name="report_type">
                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                    <tr>
                        <td>Reason</td>
                        <td ><input type="text" id="notreason" name="reason" class="form-control" value="<?=$customer->notinterested_reason;?>"></td>
                    </tr>
                    <tr>
                        <td>Select User</td>
                        <td >
                            <div class="input-field col s12 m6" style="margin-top: 0px;">
                                <div class="col s12 m12" style="margin-left: 20px">
                                    <select class="browser-default user_notinterested" name="user[]" multiple tabindex="-1" style="width: 100% !important;" required> type
                                        <?php foreach ($users as $user){ ?>
                                        <option <?php if($user_name==$user->u_id){echo "selected";} ?> value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td >
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">watch_later</i>
                                <input type="date" class="datepicker" required id="dnotinterest" name="date">
                                <label for="dob">Date</label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><button type="button" value="6" class="btn myblue change_status">Save</td>
                    </tr>
                </form>
            </table>
        </div>
    </div>
</div>

<div id="open-modal7" class="modal-window">
    <div>
        <a href="#modal-close" title="Close" class="modal-close">close &times;</a>
        <h1>Converted </h1>
        <div>
            <table border="0">
                <form action="customer-profile" method="post">
                    <input type="hidden" id="model_report_type" value="<?php ?>"  name="report_type">
                    <input type="hidden" value="<?php echo $_GET['cid']; ?>"  name="cid">
                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                    <tr>
                        <td>Converted By</td>
                        <td >
                            <div class="input-field col s12 m6" style="margin-top: 0px;">
                                <div class="col s12 m12" style="margin-left: 20px">
                                    <select class="browser-default user_converted" name="user[]" multiple tabindex="-1" style="width: 100% !important;" required> type
                                        <?php foreach ($users as $user){
                                        $uex = explode(',',$converted_by); ?>
                                        <option <?php if(in_array($user->u_id, $uex)){echo "selected";} ?> value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td >
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">watch_later</i>
                                <input type="text" class="datepicker dconverted" required id="dconverted" name="conversion_date" value="">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><button type="submit" value="4" name="convert_customer" id = "dconversionsubmit" class="btn myblue">Save</button></td>
                    </tr>
                </form>
            </table>
        </div>
    </div>
</div>

<div id="open-modal5" class="modal-window">
    <div>
        <a href="#modal-close" title="Close" class="modal-close">close &times;</a>
        <h1>Contacted</h1>
        <div>
            <table border="0">
                <form action="customer-profile" method="post">
                    <input type="hidden" id="model_report_type" value="<?php ?>"  name="report_type">
                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                    <tr>
                        <td>Select User</td>
                        <td >
                            <div class="input-field col s12 m6" style="margin-top: 0px;">
                                <div class="col s12 m12" style="margin-left: 20px">
                                    <select class="browser-default user_contacted" name="user[]" multiple tabindex="-1" style="width: 100% !important;" required> type
                                        <?php foreach ($users as $user){ ?>
                                        <option <?php if($user_name==$user->u_id){echo "selected";} ?> value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td >
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">watch_later</i>
                                <input type="date" class="datepicker" required id="dcontacted" name="date">
                                <label for="dob">Date</label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><button type="button" value="2" class="btn myblue change_status">Save</td>
                    </tr>
                </form>
            </table>
        </div>
    </div>
</div>
<div id="open-modal6" class="modal-window">
    <div>
        <a href="#modal-close" title="Close" class="modal-close">close &times;</a>
        <h1>Interested</h1>
        <div>
            <table border="0">
                <form action="customer-profile" method="post">
                    <input type="hidden" id="model_report_type" value="<?php ?>"  name="report_type">
                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                    <tr>
                        <td>Select User</td>
                        <td >
                            <div class="input-field col s12 m6" style="margin-top: 0px;">
                                <div class="col s12 m12" style="margin-left: 20px">
                                    <select class="browser-default user_interested" name="user[]" multiple tabindex="-1" style="width: 100% !important;" required> type
                                        <?php foreach ($users as $user){ ?>
                                        <option <?php if($user_name==$user->u_id){echo "selected";} ?> value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td >
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">watch_later</i>
                                <input type="date" class="datepicker" required id="dinterested" name="date">
                                <label for="dob">Date</label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><button type="button" value="3" class="btn myblue change_status">Save</td>
                    </tr>
                </form>
            </table>
        </div>
    </div>
</div>
<div id="open-modal3" class="modal-window">
    <div>
        <a href="#modal-close" title="Close" class="modal-close">close &times;</a>
        <h1>Add Area / Locality</h1>
        <div>
            <table border="0">
                <form method="post" id="localityForm">
                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                    <tr>
                        <td>District</td>
                        <td >
                            <select name="district_pop" class="browser-default" tabindex="-1">
                                <?php foreach ($districts as $district){
                                $s_id = $district->id; ?>
                                <option  value="<?=$s_id;?>"><?=$district->name?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Block</td>
                        <td >
                            <select name="block_pop" class="browser-default" tabindex="-1">
                                <?php foreach ($blocks as $block){
                                $s_id = $block->id; ?>
                                <option  value="<?=$s_id;?>"><?=$block->block?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Locality Name</td>
                        <td ><input type="text" id="locality_pop" name="locality_pop" class="form-control" value=""></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><button type="button" id="localitySave" class="btn myblue">Save</td>
                    </tr>
                </form>
            </table>
        </div>
    </div>
</div>
<div id="open-modal8" class="modal-window">
    <div>
        <a href="#modal-close" title="Close" class="modal-close">close &times;</a>
        <h1>Manage By & Above</h1>
        <div>
            <table border="0">
                <form method="post" id="manageForm">
                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                    <tr>
                        <td>Manage By & Above</td>
                        <td >
                            <input type="hidden" value="<?php echo $_GET['cid'] ?>" name = "cid" id="cid">
                            <select class="browser-default user" name="designation[]" multiple tabindex="-1" style="width: 100% !important;" > type
                                <?php foreach ($designations as $designation){
                                $eux = explode(',',$customer->managed_by); ?>
                                <option <?php if(in_array('des'.$designation->id, $eux)){echo "selected";} ?> value="des<?=$designation->id?>"> <?=$designation->name?> </option>
                                <?php } ?>
                                <option>-----------------------</option>
                                <?php foreach ($users as $user){
                                $eux = explode(',',$customer->managed_by); ?>
                                <option <?php if(in_array($user->u_id, $eux)){echo "selected";} ?> value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><button type="button" id="manageBySave" class="btn myblue">Save</td>
                    </tr>
                </form>
            </table>
        </div>
    </div>
</div>

<div id="open-modal9" class="modal-window">
    <div>
        <a href="#modal-close" title="Close" class="modal-close">close &times;</a>
        <h1>Add On Charge</h1>
        <div>
            <table border="0">
                <form method="post" id="manageForm">
                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                    <tr>
                        <td>Add On</td>
                        <td >
                            <input type="hidden" value="<?php echo $_GET['cid'] ?>" name = "cid" id="cid">
                            <select class="browser-default user" name="designation[]" multiple tabindex="-1" style="width: 100% !important;" > type
                                <?php foreach ($designations as $designation){
                                $eux = explode(',',$customer->managed_by); ?>
                                <option <?php if(in_array('des'.$designation->id, $eux)){echo "selected";} ?> value="des<?=$designation->id?>"> <?=$designation->name?> </option>
                                <?php } ?>
                                <option>-----------------------</option>
                                <?php foreach ($users as $user){
                                $eux = explode(',',$customer->managed_by); ?>
                                <option <?php if(in_array($user->u_id, $eux)){echo "selected";} ?> value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><button type="button" id="manageBySave" class="btn myblue">Save</td>
                    </tr>
                </form>
            </table>
        </div>
    </div>
</div>