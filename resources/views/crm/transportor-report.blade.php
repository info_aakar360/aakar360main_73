<?php
$location=array();
/*foreach ($lat as $lat1)
{
    $latitude='';
    $longitude='';
    if($lat1->lat!='' AND $lat1->lng!='')
    {

        $latitude = $lat1->lat;
        $longitude = $lat1->lng;
        $location[] .= "{lat : ".$latitude.", lng : ".$longitude."}";

    }

//    else
    elseif($lat1->lat=='' && $lat1->lng=='' && $lat1->latitude!='' && $lat1->longitude!='')
    {
        $latitude = $lat1->latitude;
        $longitude = $lat1->longitude;
        $location[] .= "{lat : ".$latitude.", lng : ".$longitude."}";
    }

}*/
?>

<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="keywords" content="">
    <meta name="author" content="ThemeSelect">
    <title>
        <?=$title; ?>
    </title>
    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/select.dataTables.min.css">

    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <!-- END: Custom CSS-->
</head>

<style type="text/css">

    .modal-window {
        position: fixed;
        background-color: rgba(200, 200, 200, 0.75);
        top: -70px;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 999;
        opacity: 0;
        pointer-events: none;
        -webkit-transition: all 0.3s;
        -moz-transition: all 0.3s;
        transition: all 0.3s;
    }

    .modal-window:target {
        opacity: 1;
        pointer-events: auto;
    }

    .modal-window>div {
        width: 800px;
        position: relative;
        margin: 10% auto;
        padding: 2rem;
        background: #fff;
        color: #444;
    }

    .modal-window header {
        font-weight: bold;
    }

    .modal-close {
        color: #aaa;
        line-height: 50px;
        font-size: 80%;
        position: absolute;
        right: 0;
        text-align: center;
        top: 0;
        width: 70px;
        text-decoration: none;
    }

    .modal-close:hover {
        color: #000;
    }

    .modal-window h1 {
        font-size: 150%;
        margin: 0 0 15px;
    }
</style>
<!-- END: Head-->
<?=$header;?>

<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 500px;">
                <h5 class="card-title" style="padding: 5px; color: #0d1baa;">Transportor Report</h5>
                <?=$notices;?>

                <?php if(!isset($_GET['add']) && !isset($_GET['edit'])) {?>
                        <div class="row">
                            <div class="col s12 m6 xl4">
                                <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">account_circle</i></div>
                                <div class="col s10 m10">
                                    <label>Select Name</label>
                                    <select class="browser-default" id="tname" name="tname">
                                        <option value="">Select Name</option>
                                        <?php
                                        foreach ($transportors as $tr) {
                                            ?>
                                            <option value="<?=$tr->name; ?>"><?= $tr->name ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col s12 m6 xl4">
                                <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">person_add</i></div>
                                <div class="col s10 m10">
                                    <label>Company Name</label>
                                    <select class="browser-default" id="cname" name="cname">
                                        <option value="">Select Company Name</option>
                                        <?php
                                        foreach ($transportors as $tr) {
                                            ?>
                                            <option value="<?=$tr->company_name; ?>"><?= $tr->company_name ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col s12 m6 xl4">
                                <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">flag</i></div>
                                <div class="col s10 m10">
                                    <label>Select State</label>
                                    <select class="browser-default states" id="states" name="states[]" multiple  tabindex="-1">
                                        <?php
                                        foreach ($states as $state) {
                                            $v_id = $state->id;
                                            ?>
                                            <option value="<?=$v_id; ?>"><?= $state->name ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m12 xl6">
                                <h6 style="text-align: center">Start Point</h6>
                            </div>
                            <div class="col s12 m12 xl6">
                                <h6 style="text-align: center">End Point</h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m12 xl3">
                                <div class="col s1 m1"> <i class="material-icons prefix" style="margin-top: 30px;">flag</i></div>
                                    <div class="col s10 m10">
                                    <label>Select State</label>
                                    <select class="browser-default states" id="ststates" name="ststates[]" multiple  tabindex="-1">
                                        <?php if(!empty($states)){
                                            foreach ($states as $state) {
                                                $v_id = $state->id;
                                                ?>
                                                <option value="<?=$v_id; ?>"><?= $state->name ?></option>
                                                <?php
                                            } }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col s12 m12 xl3">
                                <div class="col s1 m1"> <i class="material-icons prefix" style="margin-top: 30px;">adjust</i></div>
                                <div class="col s10 m10">
                                    <label>Select District</label>
                                    <select class="browser-default district" id="district" name="district[]" multiple  tabindex="-1">
                                    </select>
                                </div>
                            </div>
                            <div class="col s12 m12 xl3">
                                <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">flag</i></div>
                                <div class="col s10 m10">
                                <label>Select State</label>
                                <select class="browser-default edstates" id="states2" name="states2[]" multiple  tabindex="-1">
                                    <?php if(!empty($states)){foreach ($states as $state) { $v_id = $state->id;?>
                                            <option value="<?=$v_id; ?>"><?= $state->name ?></option>
                                            <?php } } ?>
                                </select>
                                </div>
                            </div>
                                <div class="col s12 m12 xl3">
                                    <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">adjust</i></div>
                                    <div class="col s10 m10">
                                    <label>Select District</label>
                                    <select class="browser-default eddistrict" id="district2" name="district2[]" multiple  tabindex="-1">
                                    </select>
                                    </div>
                                </div>

                        </div>
                        <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                            <button class="btn myblue waves-effect waves-light submit" style="padding:0 5px;" id="filterButton"><i class="material-icons right" style="margin-left:3px">search</i>Search
                            </button>
                        </div>
                    <div class="divider"></div>
                    <div class="row" style="position:relative;">
                        <div class="col s12 table-responsive">
                            <h6>All Transportors</h6>
                            <table id="transportor_table" class="display" style="width: 100%;">
                                <thead>
                                <tr role="row">
                                    <th>Sr.No.</th>
                                    <th>Name</th>
                                    <th>Company Name</th>
                                    <th>Company Address</th>
                                    <th>State</th>
                                    <th>St. State</th>
                                    <th>St. District</th>
                                    <th>Ed. State</th>
                                    <th>Ed. District</th>
                                    <th>Created at</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                            </table>
                        </div>
                    </div>
                <?php } ?>
                <?php if(isset($_GET['add'])){ ?>
                    <h5>Add New Customer
                        <a class="btn myblue waves-light right" href="customers" >
                            <i class="material-icons right">add_circle_outline</i>Home
                        </a>
                    </h5>
                    <form method="post" action="customers" class="col s12 m12">
                        <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">account_circle</i>
                                <input id="Full_Name" type="text" class="validate" name="name">
                                <label for="Full_Name">Full Name</label>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">text_fields</i>
                                <input id="Proprieter" type="tel" class="validate" name="proprieter_name">
                                <label for="Proprieter">Company Name</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">email</i>
                                <input id="Email" type="text" class="validate" name="email">
                                <label for="Email">Email</label>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">phone</i>
                                <input id="Contact" type="tel" class="validate valid_no" name="contact_no"">
                                <label for="Contact">Contact No.</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">verified_user</i>
                                <input id="CID" type="text" class="validate" name="cid">
                                <label for="CID">CID</label>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">view_headline</i>
                                <select name="customer_category" class="cc" required> customer_category
                                    <option selected disabled value="" >Select Category</option>
                                    <?php
                                    foreach ($customer_category as $category){
                                        $s_id = $category->id;
                                        ?>
                                        <option  value="<?=$s_id;?>"><?=$category->type?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="customer_category">Category </label>

                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">view_quilt</i>
                                <select name="type">
                                    <?php
                                    foreach ($customer_type as $type){
                                        $s_id = $type->id;
                                        ?>
                                        <option  value="<?=$s_id;?>"><?=$type->type?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="type">Type </label>


                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">add_location</i>
                                <input id="Address" type="tel" class="validate" name="postal_address">
                                <label for="Address">Postal Address</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">add_location</i>
                                <select  class="state_list " name="state"  style="width: 100%" required>
                                    <?php
                                    foreach ($states as $state){
                                        $s_id = $state->id;
                                        ?>
                                        <option  <?php if(1 !=1) {
                                            echo "selected";
                                        } else {
                                            echo '';
                                        }?> value="<?=$s_id;?>"><?=$state->name?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="State">State</label>
                            </div>
                            <div class="input-field col s12 m6" >

                                <select  class="browser-default district dist_list" name="district" style="width: 100%" required>
                                    <option selected value="">District</option>
                                </select>

                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">security</i>
                                <select name="msg_active">
                                    <option value="0"> Yes</option>
                                    <option value="1"> No</option>
                                </select>
                                <label for="msg_active">Message Active</label>
                            </div>
                            <div class="input-field col s6">
                                <i class="material-icons prefix">security</i>
                                <select name="msg_class">
                                    <?php
                                    foreach ($customer_class as $class){
                                        $s_id = $class->id;
                                        ?>
                                        <option  value="<?=$s_id;?>"><?=$class->type?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="msg_class">Customer Class</label>
                            </div>

                            <div class="row category_price"></div>


                        </div>
                        <button class="btn myblue waves-light right add_submit" type="submit" disabled name="add">Add
                            <i class="material-icons right">save</i>
                        </button>

                    </form>
                <?php } ?>
                <?php if(isset($_GET['edit'])){
                    $cid = $_GET['edit'];
                    $get_cust = DB::select("SELECT * FROM `crm_customers` WHERE id = '$cid'")[0];
                    ?>
                    <h5>Edit Customer
                        <a class="btn myblue waves-light right" href="customers" >
                            <i class="material-icons right">add_circle_outline</i>Home
                        </a>
                    </h5>
                    <form method="post" action="customers" class="col s12">
                        <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                        <input type="hidden" class="ccid" name="cidd" value="<?php echo $cid; ?>">
                        <div class="row">
                            <div class="input-field col s6">
                                <i class="material-icons prefix">account_circle</i>
                                <input id="Full_Name" type="text" class="validate" name="name" value="<?=$get_cust->name;?>">
                                <label for="Full_Name">Full Name</label>
                            </div>
                            <div class="input-field col s6">
                                <i class="material-icons prefix">text_fields</i>
                                <input id="Proprieter" type="tel" class="validate" name="proprieter_name" value="<?=$get_cust->proprieter_name;?>">
                                <label for="Proprieter">Company Name</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <i class="material-icons prefix">email</i>
                                <input id="Email" type="text" class="validate" name="email" value="<?=$get_cust->email;?>">
                                <label for="Email">Email</label>
                            </div>
                            <div class="input-field col s6">
                                <i class="material-icons prefix">phone</i>
                                <input id="Contact" type="tel" class="validate valid_no" name="contact_no" value="<?=$get_cust->contact_no;?>">
                                <label for="Contact">Contact No.</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <i class="material-icons prefix">verified_user</i>
                                <input id="CID" type="text" class="validate" name="cid" value="<?=$get_cust->cid;?>">
                                <label for="CID">CID</label>
                            </div>
                            <div class="input-field col s6">
                                <i class="material-icons prefix">view_headline</i>
                                <select name="customer_category" class="cc1"> customer_category
                                    <?php
                                    foreach ($customer_category as $category){
                                        $s_id = $category->id;
                                        $old_cid = $get_cust->customer_category;
                                        ?>
                                        <option <?=($s_id==$old_cid)?'selected':'';?>  value="<?=$s_id;?>"><?=$category->type?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="Category">Category</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <i class="material-icons prefix">view_quilt</i>
                                <select name="type" class="validate">
                                    <?php
                                    foreach ($customer_category as $type){
                                        $t_id = $type->id;
                                        $old_tid = $get_cust->type;
                                        ?>
                                        <option <?=($t_id==$old_tid)?'selected':'';?>   value="<?=$s_id;?>"><?=$type->type?></option>
                                        <?php
                                    }
                                    ?>
                                </select>

                                <label for="Type">Type</label>
                            </div>
                            <div class="input-field col s6">
                                <i class="material-icons prefix">add_location</i>
                                <input id="Address" type="tel" class="validate" name="postal_address" value="<?=$get_cust->postal_address;?>">
                                <label for="Address">Postal Address</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <i class="material-icons prefix">add_location</i>

                                <select  class="state_list" name="state"  style="width: 100%" required>
                                    <?php
                                    foreach ($states as $state){
                                        $s_id = $state->id;
                                        $old_sid = $get_cust->state;
                                        ?>
                                        <option <?=($s_id==$old_sid)?'selected':'';?>  value="<?=$s_id;?>"><?=$state->name?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="State">State</label>
                            </div>
                            <div class="input-field col s6">
                                <i class="material-icons prefix">add_location</i>
                                <select  class="dist_list" name="district" style="width: 100%" required>

                                    <?php
                                    foreach ($s_districts as $sdist){
                                        $d_id = $sdist->id;
                                        $old_did = $get_cust->district;
                                        ?>
                                        <option <?=($d_id==$old_did)?'selected':'';?>  value="<?=$d_id;?>"><?=$sdist->name?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="District">District</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <i class="material-icons prefix">security</i>
                                <?php $mactive = $get_cust->msg_active; ?>
                                <select name="msg_active">
                                    <option <?=($mactive==0)?'selected':'';?> value="0"> Yes</option>
                                    <option <?=($mactive==1)?'selected':'';?> value="1"> No</option>
                                </select>
                                <label for="State">Message Active</label>
                            </div>
                            <div class="input-field col s6">
                                <i class="material-icons prefix">security</i>
                                <select name="msg_class">
                                    <?php
                                    foreach ($customer_class as $class){
                                        $m_id = $class->id;
                                        $get_mid = $get_cust->class;
                                        ?>
                                        <option <?=($m_id==$get_mid)?'selected':'';?> value="<?=$m_id;?>"><?=$class->type?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="msg_class">Customer Class</label>
                            </div>

                            <div class="row category_price">
                                <?php
                                $cpc = $get_cust->product_category;
                                $ct = $get_cust->customer_category;
                                if($cpc == '' || $cpc == '0'){

                                }
                                else{
                                    $pcid = explode(',',$get_cust->product_category);
                                    $price = explode(',',$get_cust->price);

                                    $i = 0;
                                    foreach ($pcid as $p){
                                        $data2  = DB::select("SELECT * from category  WHERE id = '$p'")[0];
                                        $name = $data2->name;
                                        $id = $data2->id;
                                        ?>
                                        <div class="input-field col s3">
                                            <input  type="hidden"  name="pcid[]" value="<?=$id?>">
                                            <input  type="text" disabled value="<?=$name?>">
                                            <input  name="price[]" type="text"  value="<?=$price[$i]?>">
                                        </div>
                                        <?php
                                        $i++;
                                    }

                                }
                                //die;
                                ?>


                            </div>
                            <input type="text" name="selected_category" id="selected_category" value="">
                            <button class="btn myblue waves-light right edit_submit" type="submit" name="edit">EDIT
                                <i class="material-icons right">save</i>
                            </button>
                        </div>

                    </form>
                <?php } ?>
            </div>
        </div>
    </div>
</div>




<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.js"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/select2.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/ui-alerts.js" type="text/javascript"></script>

<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAB5amm0dLJi65cbZSQSGM3dZn4ctnO22Q&callback=initMap"></script>



<!-- BEGIN PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/scripts/dashboard-modern.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/scripts/form-elements.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<?php echo $footer ?>
</body>
</html>
<script>
    $(document).ready(function(){
        $(function() {
            $('#transportor_table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "<?=url("crm/get-transportor-data") ?>",
                    type: 'GET',
                    data: function (d) {
                        d.tname = $('#tname').val();
                        d.cname = $('#cname').val();
                        d.states = $('#states').val();
                        d.ststates = $('#ststates').val();
                        d.states2 = $('#states2').val();
                        d.district2 = $('#district2').val();
                        d.date_start1 = $('#date_start1').val();
                        d.date_end1 = $('#date_end1').val();

                    }
                },
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'company_name', name: 'company_name' },
                    { data: 'company_address', name: 'company_address' },
                    { data: 'state', name: 'state' },
                    { data: 'ststate', name: 'ststate' },
                    { data: 'stdistrict', name: 'stdistrict', },
                    { data: 'edstate', name: 'edstate',searchable: false },
                    { data: 'eddistrict', name: 'eddistrict',searchable: false  },
                    { data: 'created_at', name: 'created_at',searchable: false  },
                    { data: 'action', name: 'action', orderable: false,searchable: false }
                ],
                order: [[0, 'desc']],
                dom: 'lBfrtip',
                buttons: <?=$buttons;?>,
                fixedColumns: true,
                colReorder: true,
                exportOptions:{
                    columns: ':visible'
                }
            });
        });
    });

    $('#filterButton').click(function(){
        $('#transportor_table').DataTable().draw(true);
    });

    $(document).on('click','.transportor_delete',function () {
       // alert('Hello');
        var cid = $(this).val();
     //   alert(cid);
//            var a = $(this);
        $.ajax({
            type: "POST",
            url: "cust_transportor_delete",
            data:'cid='+cid+'&_token=<?=csrf_token(); ?>',
            success: function(data){

                alert(data);
                location.reload();
            }
        });

    });
</script>
<script>
    $(document).ready(function(){
        $('#table').attr('style', 'width: 100%');
        $('.user').select2();
        $('.category').select2();
        $('.states').select2();
        $('.stage').select2();
        $('.edstates').select2();
        $('.district').select2();
        $('.eddistrict').select2();
        $('.customer_class').select2();
        $('.type').select2();
        $('.pstype').select2();
        $('.supplier').select2();
        $('.brand').select2();
        $('.pro_category').select2();
        $(".states").change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-district-ajax",
                data:'cid='+val+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(".submit").html("Wait...");
                },
                success: function(data){
                    $(".district").html(data);
                    $(".submit").html("Search <i class='material-icons '>search</i>");
                }
            });
        });
        $(".edstates").change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-district-ajax",
                data:'cid='+val+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(".submit").html("Wait...");
                },
                success: function(data){
                    $(".eddistrict").html(data);
                    $(".submit").html("Search <i class='material-icons '>search</i>");
                }
            });
        });

        var startDate1 = moment('<?=$start_date1?>').format('DD-MM-YYYY HH:mm:ss');
        var endDate1 = moment('<?=$end_date1?>').format('DD-MM-YYYY HH:mm:ss');
        $('#date_range1').daterangepicker({
            useCurrent: true,
            opens: 'left',
            locale: {
                format: 'DD-MM-YYYY'
            },
            startDate: startDate1,
            endDate: endDate1
        },  function(start1, end1, label) {
            $('#date_start1').val(start1.format('DD-MM-YYYY'));
            $('#date_end1').val(end1.format('DD-MM-YYYY'));

        });

    });

</script>