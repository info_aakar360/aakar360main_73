<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="ThemeSelect">
    <title>
        <?=$title; ?>
    </title>

    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/select.dataTables.min.css">

    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->

    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />
    <!--    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>-->

    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
    <!-- END: Custom CSS-->
</head>

<style type="text/css">

    .file-field input.file-path{
        height: 33px !important;
    }
    .file-field .btn, .file-field .btn-large, .file-field .btn-small{
        height: 33px !important;
        line-height: 33px !important;
    }

    .modal-window {
        position: fixed;
        background-color: rgba(200, 200, 200, 0.75);
        top: -70px;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 999;
        opacity: 0;
        pointer-events: none;
        -webkit-transition: all 0.3s;
        -moz-transition: all 0.3s;
        transition: all 0.3s;
    }

    .modal-window:target {
        opacity: 1;
        pointer-events: auto;
    }

    .modal-window>div {
        width: 800px;
        position: relative;
        margin: 10% auto;
        padding: 2rem;
        background: #fff;
        color: #444;
    }
    @media only screen and (max-width: 600px) {
        .modal-window>div {
            width: 340px;
            position: relative;
            margin: 10% auto;
            padding: 2rem;
            background: #fff;
            color: #444;
        }
    }

    .modal-window header {
        font-weight: bold;
    }

    .modal-close {
        color: #aaa;
        line-height: 50px;
        font-size: 80%;
        position: absolute;
        right: 0;
        text-align: center;
        top: 0;
        width: 70px;
        text-decoration: none;
    }

    .modal-close:hover {
        color: #000;
    }

    .modal-window h1 {
        font-size: 150%;
        margin: 0 0 15px;
    }
    table th, td{
        border:1px solid #d8d3d8;
        padding-left: 15px;
    }
    .tableFixHead {
        overflow: auto;
        height: 560px;
        width:100%
    }

    .mygray{
        background-color: #aab2ae !important;
    }
    .myred{
        background-color: #eb6667 !important;
    }
    .mygreen{
        background-color: #4bbb50 !important;
    }
    .btn-arrow-right,
    .btn-arrow-left {
        position: relative;
        padding-left: 18px;
        padding-right: 18px;
    }
    .btn-arrow-right {
        padding-left: 36px;
    }
    .btn-arrow-left {
        padding-right: 36px;
    }
    .btn-arrow-right:before,
    .btn-arrow-right:after,
    .btn-arrow-left:before,
    .btn-arrow-left:after { /* make two squares (before and after), looking similar to the button */
        content:"";
        position: absolute;
        top: 7px; /* move it down because of rounded corners */
        width: 22px; /* same as height */
        height: 22px; /* button_outer_height / sqrt(2) */
        background: inherit; /* use parent background */
        border: inherit; /* use parent border */
        border-left-color: transparent; /* hide left border */
        border-bottom-color: transparent; /* hide bottom border */
        border-radius: 0px 4px 0px 0px; /* round arrow corner, the shorthand property doesn't accept "inherit" so it is set to 4px */
        -webkit-border-radius: 0px 4px 0px 0px;
        -moz-border-radius: 0px 4px 0px 0px;
    }
    .btn-arrow-right:before,
    .btn-arrow-right:after {
        transform: rotate(45deg); /* rotate right arrow squares 45 deg to point right */
        -webkit-transform: rotate(45deg);
        -moz-transform: rotate(45deg);
        -o-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
    }
    .btn-arrow-left:before,
    .btn-arrow-left:after {
        transform: rotate(225deg); /* rotate left arrow squares 225 deg to point left */
        -webkit-transform: rotate(225deg);
        -moz-transform: rotate(225deg);
        -o-transform: rotate(225deg);
        -ms-transform: rotate(225deg);
    }
    .btn-arrow-right:before,
    .btn-arrow-left:before { /* align the "before" square to the left */
        left: -11px;
    }
    .btn-arrow-right:after,
    .btn-arrow-left:after { /* align the "after" square to the right */
        right: -11px;
    }
    .btn-arrow-right:after,
    .btn-arrow-left:before { /* bring arrow pointers to front */
        z-index: 1;
    }
    .btn-arrow-right:before,
    .btn-arrow-left:after { /* hide arrow tails background */
        background-color: white;
    }

    .visit {
        display: block !important;
    }
    /* #timeline
    }*/
    #timeline-content {
        margin-top: -45px;
        height: 300px;
        text-align: center;
    }

    /* Timeline */

    .timeline1 {
        border-left: 4px solid #004ffc;
        border-bottom-right-radius: 4px;
        border-top-right-radius: 4px;
        background: rgba(255, 255, 255, 0.03);
        color: rgba(255, 255, 255, 0.8);
        font-family: 'Chivo', sans-serif;
        margin: 50px auto;
        letter-spacing: 0.5px;
        position: relative;
        line-height: 1.4em;
        font-size: 1.03em;
        padding: 50px;
        list-style: none;
        text-align: left;
        font-weight: 100;
        max-width: 95%;
    }
    .timeline1 .event {
        border-bottom: 1px dashed rgba(255, 255, 255, 0.1);
        position: relative;
    }
    .event:last-of-type {
        padding-bottom: 0;
        margin-bottom: 0;
        border: none;
    }

    .event :before,

    .event :after {
        position: absolute;
        display: block;
        top: 0;
    }

    &
    .timeline1 .event:before {
        left: -215.5px;
        color: rgba(255, 255, 255, 0.4);
        content: attr(data-date);
        text-align: right;
        font-weight: 100;
        font-size: 0.9em;
        min-width: 120px;
        font-family: 'Saira', sans-serif;
    }
    .timeline1 .event:after{
        box-shadow: 0 0 0 4px #004ffc;
        left: -9.85px;
        background: #313534;
        border-radius: 50%;
        height: 11px;
        width: 11px;
        content: "";
        top: 5px;
        position: absolute;
    }


</style>
<!-- END: Head-->
<?=$header;?>

<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs">
            <div class="card-content" style="min-height: 500px;">
                <div class="col s3 m3">

                    <a class="btn myblue waves-light" style="padding:0 5px;" href="<?php echo isset($_GET['link']) ? $_GET['link'] : 'customer-data';?>">
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
                <!--- Lead Edit Form --->
                <div id="Lead_Order_Edit" class="col s12 m12 l12">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <div id="Detail" class="col-lg-12 col-xs-12">
                                <div class="visits_form">
                                    <form method="post" action="lead_order-edit" enctype="multipart/form-data" class="col s12">
                                        <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                        <input type="hidden" name="crm_cust_id" value="<?php //echo  $pipelines->id?$pipelines->id:''; ?>">
                                        <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">

                                        <div class="row">
                                            <div class="input-field col s8 m6">
                                                <i class="material-icons prefix">account_circle</i>
                                                <select class="validate stagebyname" name="name"  tabindex="-1" style="width: 100% !important;" required> type
                                                    <option value="">Select Name</option>
                                                    <?php foreach ($sales_pipelines as $pipeline) {

                                                        ?>
                                                        <option <?php if(!empty($leads)){if($pipeline->id==$leads->pipelines_id) {echo "selected";}}?> value="<?php echo $pipeline->id ?>"><?php echo $pipeline->name ?></option>
                                                    <?php } ?>
                                                </select>
                                                <label for="name">Pipeline Name</label>
                                            </div>

                                            <div class="input-field col s8 m6">
                                                <i class="material-icons prefix">navigation</i>
                                                <select  class="browser-default stage" name="stage" style="margin-left:34px" required>
                                                    <option value="" desabled>Select Stage</option>
                                                    <option <?php if($leads->pipelines_stage=="new") {echo "selected";} ?> value="new">New</option>
                                                    <?php
                                                    $stages = DB::table('pipeline_stages')->where('pipeline_id',$leads->pipelines_id)->get();

                                                    foreach ($stages as $stage) {
                                                        ?>
                                                        <option <?php if($leads->pipelines_stage==$stage->stage_name ) {echo "selected";} ?> value="<?php echo $stage->stage_name; ?>"><?php echo $stage->stage_name; ?></option>
                                                    <?php } ?>
                                                    <option <?php if($leads->pipelines_stage=="won") {echo "selected";} ?> value="won">Won</option>
                                                    <option <?php if($leads->pipelines_stage=="lost" ) {echo "selected";} ?> value="lost">Lost</option>
                                                </select>
                                                <label for="stage" style="margin-top: -20px;">Pipeline Stage</label>
                                            </div>
                                        </div>
                                        <div class="row">

                                            <div class="input-field col s8 m6">
                                                <i class="material-icons prefix">watch_later</i>
                                                <input type="text" class="datepicker leaddob" required id="leaddob" name="date"  value="<?php echo date("m/d/Y");?>">
                                                <label for="dob">Date</label>
                                            </div>
                                            <div class="input-field col s8 m6">
                                                <i class="material-icons prefix" style="margin-top: 10px">view_headline</i>
                                                <div class="col s12 m12" style="margin-left: 20px">
                                                    Product Category

                                                    <select class="browser-default product_category" name="product_category[]" multiple tabindex="-1" style="width: 100% !important;" required> type

                                                        <?php foreach ($product_category as $product_cat) {
                                                            ?>
                                                            <option <?php if(in_array($product_cat->id,array($leads->product_category))) {echo "selected";}?> value="<?php echo $product_cat->id ?>"><?php echo $product_cat->name ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="input-field col s12 m12 " id="no_order_reason" style="<?php if(!empty($leads->no_order_reason)){echo 'display:block';}else{ echo 'display: none';} ?>">
                                                <i class="material-icons prefix" style="margin-top: 10px">view_headline</i>
                                                <div class="col s12 m12" style="margin-left: 20px" >
                                                    No Order Reason

                                                    <select class="browser-default no_order_reason" name="no_order_reason[]" multiple tabindex="-1" style="width: 100% !important;" > type

                                                        <?php foreach ($no_order_reasons as $reason) { ?>
                                                            <option <?php if((!empty($leads->no_order_reason)) && ($leads->no_order_reason == $reason->id)){ echo "selected"; } ?> value="<?php echo $reason->id ?>"><?php echo $reason->reason ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-field col s8 m3" style="text-align: center">
                                            <button class="btn myblue waves-light" style="padding:0 5px;" type="submit" name="edit_lead_pipeline">Save
                                                <i class="material-icons right">save</i>
                                            </button>
                                        </div>
                                </div>
                                </form>
                            </div>

                            <div class="col s12 m12 l12">
                                <div id="Activity" class="col-lg-4 col-xs-4">
                                    <ul class="row tabs">
                                        <li class="tab col s4 m4 x4"><a class="p-0" href="#log" >Call Log</a></li>
                                        <li class="tab col s4 m4 x4"><a class="p-0" href="#eml" >Email</a></li>
                                        <li class="tab col s4 m4 x4"><a class="p-0" href="#msg" >Message</a></li>
                                    </ul>
                                </div>
                                <div id="log" class="col-lg-4 col-xs-4">
                                    <div class="row" style="margin-top: 20px;">
                                        <form method="post" action="lead_order-edit" enctype="multipart/form-data" class="col s12">
                                            <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                            <input type="hidden" name="crm_cust_id" value="<?php echo  $_GET['cid']; ?>">
                                            <input type="hidden" name="id" value="<?php echo  $_GET['id']; ?>">

                                            <div class="row">
                                                <div class="input-field col s12 m4">
                                                    <i class="material-icons prefix">account_circle</i>
                                                    <select name="conperson3" id="conperson3" class="conperson">
                                                        <option value="<?= $customer->crm_customer_id; ?>"><?= $customer->name; ?></option>
                                                        <?php
                                                        if(!empty($customer->sec_names)){ $names = unserialize($customer->sec_names);
                                                            foreach($names as $key=>$name){
                                                                ?>
                                                                <option value="<?= $key; ?>"><?= $name; ?></option>
                                                            <?php } }?>
                                                    </select>
                                                    <label for="conperson">Contact Person</label>
                                                </div>
                                                <div class="input-field col s12 m4">
                                                    <i class="material-icons prefix">account_circle</i>
                                                    <input type="tel" class="validate landline_no" id="landline_no3" name="landline_no3" value="<?=$customer->landline?>">
                                                    <label for="customer_no1">Landline No</label>
                                                </div>
                                                <div class="input-field col s12 m4">
                                                    <i class="material-icons prefix">account_circle</i>
                                                    <input type="text" class="validate contact-designation" id="contact-designation3" name="contact-designation3" value="">
                                                    <label for="contact-designation3">Contact Designation</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s12 m6">
                                                    <i class="material-icons prefix">account_circle</i>
                                                    <input type="tel" class="validate customer_no1" id="customer_no1" name="customer_no" value="<?php echo  $customer->contact_no;?>">
                                                    <label for="customer_no1">Customer No</label>
                                                </div>
                                                <div class="input-field col s12 m6">
                                                    <i class="material-icons prefix">watch_later</i>
                                                    <input type="text" class="datepicker" id="dob3" name="select_date" value="<?php echo date("Y-m-d");?>">
                                                    <label for="dob3">Date</label>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="input-field col s12 m6">
                                                    <i class="material-icons prefix">content_paste</i>
                                                    <input id="message_call" type="text" class="validate" name="message" value="" required>
                                                    <label for="message_call">Notes</label>
                                                </div>
                                                <div class="input-field col s12 m6" style="margin-top: 0px;">
                                                    <i class="material-icons prefix" style="margin-top: 10px">person_add</i>
                                                    <div class="col s12 m12" style="margin-left: 20px">
                                                        Select User

                                                        <select class="browser-default user" name="user[]" multiple tabindex="-1" style="width: 100% !important;" required> type
                                                            <?php

                                                            foreach ($users as $user){

                                                                ?>
                                                                <option <?php if($user_name==$user->u_id){echo "selected";} ?> value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="input-field col s12 m6">
                                                <i class="material-icons prefix">list</i>
                                                <select name="ctype" class="ctype" required> type
                                                    <option value="" disabled selected>Select Call Type</option>
                                                    <option value="1">Incoming</option>
                                                    <option value="2" selected>Outgoing</option>
                                                    <option value="3">Missed call</option>
                                                </select>
                                                <label for="ptype">Call Type</label>
                                            </div>
                                            <div class="input-field col s12 m6">
                                                <i class="material-icons prefix" style="margin-top: 10px">call</i>
                                                <input class="timepicker form-control" type="text" name="time">
                                                <label for="time">Call Time</label>

                                            </div>
                                            <div class="row">

                                                <div class="input-field col s12 m12" style="text-align: center">
                                                    <button class="btn myblue waves-light " style="padding:0 5px;" type="submit" name="edit_lead_call">Save
                                                        <i class="material-icons right">save</i>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                        <!--Call Log-->

                                        <?php if(!empty($call_logs) && count($call_logs)>0){ ?>
                                            <h6 style="padding-left: 20px;">  Call Logs Timeline</h6>

                                            <div id="app" class="container">
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div id="timeline-content">
                                                            <ul class="timeline1">
                                                                <input type="hidden" name="leadid"  id = "leadid" value="<?php echo  $_GET['id']; ?>">

                                                                <?php

                                                                foreach ($call_logs as $log ){ ?>

                                                                    <li class="event" data-date="<?php echo $log->select_date ?>">
                                                                        <h6 style="padding-left: 20px" id="edittag<?php echo $log->id ?>"><?php echo $log->message ?></h6>
                                                                        <p style="color: black;padding-left: 20px"><?php echo $log->select_date ?>  <a class="btn myblue waves-light" style="padding:0 10px;" onclick="editCallLog('<?php echo $log->id ?>')"><i class="material-icons">edit</i></a><a class="btn myblue waves-light" style="padding:0 10px;" onclick="deleteCallLog('<?php echo $log->id ?>')"><i class="material-icons">delete</i></a>
                                                                            <button class="btn myblue waves-light" style="padding:0 5px;display:none"  id="updateCallLog<?php echo $log->id ?>" onclick="updateCallLog('<?php echo $log->id ?>')">Update
                                                                                <i class="material-icons right">save</i>
                                                                            </button></p>
                                                                    </li>

                                                                <?php } ?>

                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <!--End Call Log-->
                                    </div>
                                </div>
                                <div id="eml">
                                    <pre></pre>
                                    <div class="Email_form">
                                        <form method="post" action="customer-profile" enctype="multipart/form-data" class="col s12 m12">
                                            <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                            <input type="hidden" name="crm_cust_id" value="<?php echo $_GET['cid']; ?>">
                                            <div class="row">
                                                <div class="input-field col s12 m6">
                                                    <i class="material-icons prefix">account_circle</i>
                                                    <select name="conperson3" id="conperson3" class="conperson">
                                                        <option value="<?= $customer->id; ?>"><?= $customer->name; ?></option>
                                                        <?php
                                                        if($customer->sec_names){ $names = unserialize($customer->sec_names);
                                                            foreach($names as $key=>$name){
                                                                ?>
                                                                <option value="<?= $key; ?>"><?= $name; ?></option>
                                                            <?php } }?>
                                                    </select>
                                                    <label for="conperson">Concern Person</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s12 m6">
                                                    <i class="material-icons prefix">contact_mail</i>
                                                    <input type="text" class="validate" id="customer_email" name="customer_email" value="<?php echo  $customer->email;?>">
                                                    <label for="customer_email">Customer Email</label>
                                                </div>
                                                <div class="input-field col s12 m6">
                                                    <i class="material-icons prefix">comment</i>
                                                    <input type="text" class="validate" id="email_subject" name="email_subject">
                                                    <label for="email_subject">Subject</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s12 m6">
                                                    <i class="material-icons prefix">email</i>
                                                    <input id="message1" type="text" class="validate" name="message">
                                                    <label for="message1">Message</label>
                                                </div>
                                                <div class="input-field col s12 m6" style="margin-top: 0px;">
                                                    <i class="material-icons prefix" style="margin-top: 10px">person_add</i>
                                                    <div class="col s12 m12" style="margin-left: 20px">
                                                        Select User

                                                        <select class="browser-default user" name="user[]" multiple tabindex="-1" style="width: 100% !important;" required> type
                                                            <?php

                                                            foreach ($users as $user){

                                                                ?>
                                                                <option <?php if($user_name==$user->u_id){echo "selected";} ?> value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row">

                                                <div class="input-field col s12 m12" style="text-align: center">
                                                    <button class="btn myblue waves-light" style="padding:0 5px;" type="submit" name="add_notes">Send
                                                        <i class="material-icons right">save</i>
                                                    </button>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                    <div class="divider"></div>

                                </div>
                                <div id="msg">
                                    <pre></pre>
                                    <div class="Message_form">
                                        <form method="post" action="lead_order-edit" enctype="multipart/form-data" class="col s12">
                                            <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                            <input type="hidden" name="crm_cust_id" value="<?php echo  $customer->id; ?>">
                                            <div class="row">
                                                <div class="input-field col s12 m6">
                                                    <i class="material-icons prefix">account_circle</i>
                                                    <select name="conperson3" id="conperson3" class="conperson">
                                                        <option value="<?= $customer->id; ?>"><?= $customer->name; ?></option>
                                                        <?php
                                                        if($customer->sec_names){ $names = unserialize($customer->sec_names);
                                                            foreach($names as $key=>$name){
                                                                ?>
                                                                <option value="<?= $key; ?>"><?= $name; ?></option>
                                                            <?php } }?>
                                                    </select>
                                                    <label for="conperson">Concern Person</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s12 m6">
                                                    <i class="material-icons prefix">account_circle</i>
                                                    <input type="tel" class="validate customer_no1" id="customer_no" name="customer_no" value="<?php echo  $customer->contact_no;?>">
                                                    <label for="customer_no">Customer No</label>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="input-field col s12 m6">
                                                    <i class="material-icons prefix">email</i>
                                                    <input id="message" type="text" class="validate" name="message">
                                                    <label for="message">Message</label>
                                                </div>
                                                <div class="input-field col s12 m6" style="margin-top: 0px;">
                                                    <i class="material-icons prefix" style="margin-top: 10px">person_add</i>
                                                    <div class="col s12 m12" style="margin-left: 20px">
                                                        Select User

                                                        <select class="browser-default user" name="user[]" multiple tabindex="-1" style="width: 100% !important;" required> type
                                                            <?php

                                                            foreach ($users as $user){

                                                                ?>
                                                                <option <?php if($user_name==$user->u_id){echo "selected";} ?> value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">

                                                <div class="input-field col s12 m12" style="text-align: center">
                                                    <button class="btn myblue waves-light send_msg" style="padding:0 5px;" type="submit" name="save_msg">Send
                                                        <i class="material-icons right">save</i>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="divider"></div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="divider"></div>
                    <!--- End Lead Edit Form --->
                </div>

            </div>
        </div>
    </div>
</div>

<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/data-tables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/js/dataTables.select.min.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/form-layouts.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/select2.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
<script src="https://rawgit.com/Eonasdan/bootstrap-datetimepicker/master/build/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/ui-alerts.js" type="text/javascript"></script>
<?php echo $footer ?>
</body>
</html>
<script type="text/javascript">

    $('.timepicker').datetimepicker({

        format: 'HH:mm:ss'

    });

    function checkInput(){
        alert('hi');
        $("#checkdate").val('1')
    }




    //    if($("#checkdate").val() == "") {
    //        alert("please select date");
    //    }

</script>
<script>
    var ttable = false;
    var ptable = false;
    var protable = false;
    var prodtable = false;
    var calltable = false;
    var vtable = false;
    var ntable = false;
    var promotable = false;
    var msgtable = false;
    var emailtable = false;
    $(document).ready(function(){
//        user_notinterested
        $('.category').select2();
        $('.pstype').select2();
        $('.brand').select2();
        $('.user').select2();
        $('.supplier').select2();
        $('.states').select2();
        $('.district').select2();
        $('.block').select2();
        $('.locality').select2();
        $('.price_group').select2();
        $('.user_unqualified').select2();
        $('.user_contacted').select2();
        $('.user_interested').select2();
        $('.user_notinterested').select2();
        $('.user_converted').select2();
        $('.user_unreachable').select2();
        $('.no_order_reason').select2();
        $('.product_category').select2();
        $(".category").change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-price-groups-ajax",
                data:'cid='+val+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $(".price_group").html(data);
                }
            });
        });

        $(".states").change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-district-ajax",
                data:'cid='+val+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $(".district").html(data).append('<option value="" disabled selected>Select District</option>');
                }
            });
        });
        $('.district').change(function () {
            var did = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-block",
                data:'did='+did+'&_token=<?=csrf_token(); ?>',

                success: function(data){
                    $(".block").html(data).append('<option value="" disabled selected>Select Block</option>');

                }
            });
        });
        $('.block').change(function () {
            var did = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-locality",
                data:'did='+did+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(".submit").html("Wait...");
                },
                success: function(data){
                    $('#locality').html(data);
                }
            });
        });
        $(".conperson").change(function () {
            <?php if(!empty($customer->sec_landline)){ $landlines = unserialize($customer->sec_landline);
            foreach($landlines as $key=>$landline){ ?>
            if($(this).val() == <?=$key?>) {
                $('.landline_no').val('<?=$landline?>');
            }if($(this).val() == <?=isset($_GET['cid'])?$_GET['cid']:false ?>){
                $('.landline_no').val('<?=$customer->landline?>');
            }
            <?php } } ?>
            <?php if(!empty($customer->sec_contact)){ $contacts = unserialize($customer->sec_contact);
            foreach($contacts as $key=>$contact){ ?>
            if($(this).val() == <?=$key?>) {
                $('.customer_no1').val('<?=$contact?>');
            }if($(this).val() == <?=isset($_GET['cid'])?$_GET['cid']:false ?>){
                $('.customer_no1').val('<?=$customer->contact_no?>');
            }
            <?php } } ?>
                <?php if(!empty($customer->sec_desc)){ $desigs = unserialize($customer->sec_desc);
                foreach($desigs as $key=>$desig){ ?>
            if($(this).val() == <?=$key?>) {
                <?php $singledata1 = DB::table('crm_contax')->where('id', $desig)->first(); if(!empty($singledata1)){?>
                $('.contact-designation').val('<?=$singledata1->name;?>');
                <?php } else { ?>
                $('.contact-designation').val('');
                <?php } ?>
            }if($(this).val() == <?=isset($_GET['cid'])?$_GET['cid']:false ?>){

                $('.contact-designation').val('<?php if($customer->designation &&(!empty($singledata))){ echo $singledata->name;} ?>');
            }
            <?php } } ?>
        });
        var d = new Date();

        var month = d.getMonth()+1;
        var day = d.getDate();

        var output = d.getFullYear() + '-' +
            ((''+month).length<2 ? '0' : '') + month + '-' +
            ((''+day).length<2 ? '0' : '') + day;

        $('.datepicker').each(function(){
            var dtpick = $(this);
            if(dtpick.hasClass('leaddob')){
                dtpick.val("<?=date('Y-m-d',strtotime($leads->select_date))?>");
            }else{
                dtpick.datepicker("setDate", new Date());
            }
        });

        $('.type1').select2();
        $('.variants').select2();
        $('.ticket_form').hide();

        $('.ticket_form_button').click(function(){
            $('.ticket_form').show();
        });


        $(".category").change(function(){
            var val = $(this).val();
            var cname = $(".category option:selected").html();

            $.ajax({
                type: "POST",
                url: "get-report-option",
                data:'cid='+val+'&cname='+cname+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $("#search-box").css("background","#FFF url(assets/LoaderIcon.gif) no-repeat 165px");
                },
                success: function(data){
                    $(".report_div").html(data);
                    $("#model_report_type").val(data);
                }
            });
        });
        var get_view_type = $('#get_report_view').val();
        if(get_view_type=='brand_wise'){
            $('.brand_wise').show();
        }
        if(get_view_type=='price_wise'){
            $('.price_wise').show();
        }


        $('#visit_img_div').hide();
        function readURLVisit(input) {
            $('#visit_img_div').show();
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#visit_image_show').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#visit_image").change(function(){
            readURLVisit(this);
        });


        $('#ticket_img_div').hide();
        function readURLTicket(input) {
            $('#ticket_img_div').show();
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#ticket_image_show').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#ticket_image").change(function(){
            readURLTicket(this);
        });

        $('#notes_img_div').hide();
        function readURLNotes(input) {
            $('#notes_img_div').show();
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#notes_image_show').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#notes_image").change(function(){
            readURLNotes(this);
        });

        $('#project_img_div').hide();
        function readURLproject(input) {
            $('#project_img_div').show();
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#project_image_show').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#project_image").change(function(){
            readURLproject(this);
        });

        $('#category_img_div').hide();
        function readURLcategory(input) {
            $('#category_img_div').show();
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#category_image_show').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#category_image").change(function(){
            readURLcategory(this);
        });



        $('.change_status').click(function () {
            var val = $(this).val();
            var cid = $(".cust_id").val();
            var user_unqualified = $(".user_unqualified").val();
            var user_notinterested = $(".user_notinterested").val();
            var user_contacted = $(".user_contacted").val();
            var user_interested = $(".user_interested").val();
            var user_converted = $(".user_converted").val();
            var user_unreachable = $(".user_unreachable").val();
            var ureason = $("#ureason").val();
            var dunreachable = $("#dunreachable").val();
            var dunqualied = $(".dunqualied").val();
            var reason = $("#reason").val();
            var notreason = $("#notreason").val();
            var dunqualied = $("#dunqualied").val();
            var dnotinterest = $("#dnotinterest").val();
            var dcontacted = $("#dcontacted").val();
            var dinterested = $("#dinterested").val();
            var dconverted = $("#dconverted").val();
            $.ajax({
                type: "POST",
                url: "customer-class-update",
                data:'cid='+cid+'&class='+val+'&ureason='+ureason+'&user_unreachable='+user_unreachable+'&dunreachable='+dunreachable+'&reason='+reason+'&dunqualied='+dunqualied+'&user_unqualified='+user_unqualified+'&user_notinterested='+user_notinterested+'&user_contacted='+user_contacted+'&user_interested='+user_interested+'&notreason='+notreason+'&user_converted='+user_converted+'&dnotinterest='+dnotinterest+'&dcontacted='+dcontacted+'&dinterested='+dinterested+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    //alert(data);
                    window.location.href = "customer-profile?cid="+cid;
                    //window.location.reload();

                }
            });
        }); zn

        $('.send_msg').click(function () {
            var mob = $.trim($("#customer_no").val());
            var cname = encodeURIComponent($("#message").val());
            var cid = $(".cust_id").val();
            if(mob==''){
                alert('Please Enter Number');
                return false;
            }
            else{
                $.ajax({
                    type: "GET",
                    url: "https://merasandesh.com/api/sendsms",
                    data:'username=smshop&password=Smshop@123&senderid=SMSHOP&message='+cname+'&numbers='+mob+'&unicode=0',
                    async: true,
                    beforeSend: function(){
                        $('.send_msg').html('Wait...');
                        $.ajax({
                            type: "POST",
                            url: "msg_save",
                            data:'message='+cname+'&numbers='+mob+'&_token=<?=csrf_token(); ?>',
                            success: function(data){

                            }
                        });
                    },
                    success: function(data){

                    },
                    error: function(){
//                        alert('Message sending failed.');
                    },
                    complete: function(data){
                        location.reload();
                    }

                });
            }

        });

        $('.ptype').change(function () {
            var val = $(this).val();

            $.ajax({
                type: "POST",
                url: "get-subtype",
                data:'tid='+val+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $('.pstype').html(data);
                }
            });
        });

        $(document).on('click', '.pdelete', function () {
            var pid = $(this).val();
            var cid = $(".cust_id").val();
            var a = $(this);

            $.ajax({
                type: "POST",
                url: "customer-project-delete",
                data:'pid='+pid+'&cid='+cid+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(a).html('Wait...');
                },
                success: function(data){
                    alert(data);
                    if(ptable){
                        $('#projects-table').DataTable().ajax.reload();
                    }else{
                        location.reload();
                    }
                }
            });
        });

        $(document).on('click', '.tdelete', function () {
            var pid = $(this).val();
            var cid = $(".cust_id").val();
            var a = $(this);

            $.ajax({
                type: "POST",
                url: "customer-ticket-delete",
                data:'pid='+pid+'&cid='+cid+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(a).html('Wait...');
                },
                success: function(data){
                    alert(data);
                    if(ttable){
                        $('#tickets-table').DataTable().ajax.reload();
                    }else{
                        location.reload();
                    }
                }
            });
        });

        $(document).on('click', '.ndelete', function () {
            var pid = $(this).val();
            var cid = $(".cust_id").val();
            var a = $(this);

            $.ajax({
                type: "POST",
                url: "customer-note-delete",
                data:'pid='+pid+'&cid='+cid+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(a).html('Wait...');
                },
                success: function(data){
                    alert(data);
                    if(ntable){
                        $('#notes-table').DataTable().ajax.reload();
                    }else{
                        location.reload();
                    }
                }
            });
        });

        $(document).on('click', '.vdelete', function () {
            var pid = $(this).val();
            var cid = $(".cust_id").val();
            var a = $(this);

            $.ajax({
                type: "POST",
                url: "customer-visit-delete",
                data:'pid='+pid+'&cid='+cid+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(a).html('Wait...');
                },
                success: function(data){
                    alert(data);
                    if(vtable){
                        $('#visits-table').DataTable().ajax.reload();
                    }else{
                        location.reload();
                    }
                }
            });
        });

        $(document).on('click', '.calldelete', function () {
            var pid = $(this).val();
            var cid = $(".cust_id").val();
            var a = $(this);

            $.ajax({
                type: "POST",
                url: "customer-call-delete",
                data:'pid='+pid+'&cid='+cid+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(a).html('Wait...');
                },
                success: function(data){
                    alert(data);

                    $('#calls-table').DataTable().ajax.reload();

                    location.reload();

                }
            });
        });



        $(document).on('click', '.prodelete', function () {
            var pid = $(this).val();
            var cid = $(".cust_id").val();
            var a = $(this);



            $.ajax({
                type: "POST",
                url: "customer-promotional-delete",
                data:'pid='+pid+'&cid='+cid+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(a).html('Wait...');
                },
                success: function(data){
                    alert(data);
                    if(protable){
                        $('#promos-table').DataTable().ajax.reload();
                    }else{
                        location.reload();
                    }
                }
            });
        });

        $(document).on('click', '.proddelete', function () {
            var pid = $(this).val();
            var cid = $(".cust_id").val();
            var a = $(this);
//            alert(pid);
            $.ajax({
                type: "POST",
                url: "customer-product-delete",
                data:'pid='+pid+'&cid='+cid+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(a).html('Wait...');
                },
                success: function(data){
                    alert(data);

                    $('#products-table').DataTable().ajax.reload();

                    location.reload();

                }
            });
        });
        $('#month').on('change', function(){
            $('#timeline_form').submit();
        });
        $('.update_reason').on('click', function(){
            var status = $(this).data('value');
            if(status == 5){
                $('#open-modal2 h1').html('UNQUALIFIED');
                $('#open-modal2 .change_status').attr('value', 5);
                $('#open_reason').trigger('click');
            }else if(status == 6){
                $('#open-modal2 h1').html('NOT INTERESTED');
                $('#open-modal2 .change_status').attr('value', 6);
                $('#open_reason').trigger('click');
            }
        });
        $('#localitySave').on('click', function(){
            var formData = $('form#localityForm').serialize();
            var btn = $(this);
            $.ajax({
                url: 'save-locality',
                type: 'post',
                data: formData,
                beforeSend: function(){
                    btn.html('Saving...');
                },
                success: function(data){
                    if(data == 'success') {
                        alert('Locality Saved.');
                    }else{
                        alert('Invalid Data.');
                    }
                    btn.html('Save');
                }
            });
        });
        $('#manageBySave').on('click', function(){
            var formData = $('form#manageForm').serialize();
            var btn = $(this);
            $.ajax({
                url: 'save-manageBySave',
                type: 'post',
                data: formData,
                beforeSend: function(){
                    btn.html('Saving...');
                },
                success: function(data){
                    if(data == 'success') {
                        alert('manageBySave Saved.');
                    }
                    btn.html('Save');
                }
            });
        });
    });

</script>
<script>

    $(".stagebyname").change(function(){

        var val = $(this).val();
        $.ajax({
            type: "POST",
            url: "get-stages-ajax",
            data:'pid='+val+'&_token=<?=csrf_token(); ?>',
            success: function(data){

                $(".stage").html(data);
            }
        });
    });

    $(".stage").change(function () {

        if($(this).val() == "lost") {
            $("#no_order_reason").show();
            $('.no_order_reason').attr('required', true);
        }else{
            $('.no_order_reason').attr('required', false);
            $('.no_order_reason').empty();
            $("#no_order_reason").hide();
        }
    });

    function editCallLog(id){
        $('#edittag'+id).attr('contenteditable','true');
        $('#updateCallLog'+id).show();
    }
    function updateCallLog(id) {
        var leadid = $("#leadid").val();
        var messages = $('#edittag'+id).html();
        $.ajax({
            type: "POST",
            url: "update-call_log-ajax",
            data:'id='+id+'&_token=<?=csrf_token(); ?>'+'&message='+messages+'&leadid='+leadid,
            success: function(data){
                $('#updateCallLog'+id).hide();
                $('#updateCallLog'+id).html('updated Successfully!!');
                $('#updateCallLog'+id).show();
                window.location.reload();
            }
        });
    }
    function deleteCallLog(id) {
        var leadid = $("#leadid").val();
        $.ajax({
            type: "POST",
            url: "delete-call_log-ajax",
            data:'id='+id+'&_token=<?=csrf_token(); ?>'+'&leadid='+leadid,
            success: function(data){
                $('#updateCallLog'+id).hide();
                $('#updateCallLog'+id).html('Deleted Successfully!!');
                $('#updateCallLog'+id).show();
                window.location.reload();
            }
        });
    }
</script>