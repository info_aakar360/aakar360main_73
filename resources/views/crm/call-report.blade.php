<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="keywords" content="">
    <meta name="author" content="ThemeSelect">
    <title>
        <?=$title; ?>
    </title>
    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/select.dataTables.min.css">

    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <!-- END: Custom CSS-->
</head>

<style type="text/css">

    .modal-window {
        position: fixed;
        background-color: rgba(200, 200, 200, 0.75);
        top: -70px;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 999;
        opacity: 0;
        pointer-events: none;
        -webkit-transition: all 0.3s;
        -moz-transition: all 0.3s;
        transition: all 0.3s;
    }

    .modal-window:target {
        opacity: 1;
        pointer-events: auto;
    }

    .modal-window>div {
        width: 800px;
        position: relative;
        margin: 10% auto;
        padding: 2rem;
        background: #fff;
        color: #444;
    }

    .modal-window header {
        font-weight: bold;
    }

    .modal-close {
        color: #aaa;
        line-height: 50px;
        font-size: 80%;
        position: absolute;
        right: 0;
        text-align: center;
        top: 0;
        width: 70px;
        text-decoration: none;
    }

    .modal-close:hover {
        color: #000;
    }

    .modal-window h1 {
        font-size: 150%;
        margin: 0 0 15px;
    }

</style>
<!-- END: Head-->
<?=$header;?>
<div class="row">
    <div class="col s12">
        <div class="container">
            <div class="section">
                <div class="card">
                    <div class="card-content">
                        <h4 class="card-title" style="color: #0d1baa; margin-left: 5px"><?=$title; ?><a href="<?=url('crm/visit-report');?>" class="btn myblue waves-effect waves-light right" style="padding:0 5px; margin-top: 5px">Visit Report</a></h4>
                        <?=$notices;?>

                            <div class="col s12 m4 xl2">
                                <div class="col s1 m1"style="text-align: center;margin-top: 30px;margin-left: -32px;"> <i class="material-icons prefix">list</i></div>
                                <div class="col s11 m11">
                                <label>Select Category</label>
                                <select class="browser-default category" id="category" name="category[]" multiple  tabindex="-1">
                                    <?php
                                    foreach ($customer_category as $cate) {
                                        $c_id = $cate->id;
                                        ?>
                                        <option value="<?= $c_id; ?>"><?= $cate->type ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                </div>
                            </div>


                        <div class="col s12 m4 xl2">
                            <div class="col s1 m1"style="text-align: center;margin-top: 30px;margin-left: -32px;"> <i class="material-icons prefix">flag</i></div>
                            <div class="col s11 m11">
                                <label>Select State</label>
                                <select class="browser-default states" id="states" name="states[]" multiple  tabindex="-1">
                                    <?php
                                    foreach ($states as $state) {
                                        $v_id = $state->id;
                                        ?>
                                        <option value="<?=$v_id; ?>"><?= $state->name ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                    <div class="col s12 m4 xl2">
                        <div class="col s1 m1"style="text-align: center;margin-top: 30px;margin-left: -32px;"> <i class="material-icons prefix">adjust</i></div>
                        <div class="col s11 m11">
                            <label>Select District</label>
                            <select class="browser-default district" id="district" name="district[]" multiple  tabindex="-1">
                            </select>
                        </div>
                    </div>


                        <div class="col s12 m6 xl3">
                            <div class="col s1 m1"style="text-align: center;margin-top: 30px;margin-left: -32px;"> <i class="material-icons prefix">person</i></div>
                            <div class="col s11 m11" style="margin-top: -7px">
                                <label>Select User</label>
                                <select class="browser-default acc_manager" id="acc_manager" name="acc_manager" tabindex="-1">
                                    <option value="all">All</option>
                                    <?php
                                    foreach($users as $user){
                                        echo '<option value="'.$user->u_id.'">'.$user->u_name.'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col s12 m6 xl3">
                            <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">dns</i></div>
                            <div class="col s10 m10">
                                <label>Select Customer Class</label>
                                <select class="browser-default customer_class" id="customer_class" name="cust_class[]" multiple tabindex="-1">
                                    <?php
                                    foreach ($cust_classes as $cust_class) {
                                        $v_id = $cust_class->id;
                                        ?>
                                        <option value="<?=$v_id; ?>"><?= $cust_class->type ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col s12 m6 xl3">
                            <div class="col s1 m1"style="text-align: center;margin-top: 30px;margin-left: -32px;"> <i class="material-icons prefix">date_range</i></div>
                            <div class="col s11 m11" style="margin-top: -7px">
                                <label for="date_range">Date Range
                                    <input type="text" id="date_range" name="dates">
                                </label>
                                <input type="hidden" id="date_start" value="<?=$start_date?>"/>
                                <input type="hidden" id="date_end" value="<?=$end_date?>"/>
                            </div>
                        </div>

                        <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                            <button class="btn myblue waves-effect waves-light" style="padding:0 5px; margin-top: 5px" id="view_graph"><i class="material-icons right" style="margin-left:3px">show_chart</i>View Graph
                            </button>
                            <button class="btn myblue waves-effect waves-light submit" style="padding:0 5px; margin-top: 5px" id="filterButton"><i class="material-icons right" style="margin-left:3px">search</i>Search
                            </button>
                        </div>


                        <div class="divider"></div>
                        <div class="col m12" id="graph" style="margin-top: 10px">
                            <div class="chart-container" style="position: relative; height:40vh; width:80vw">
                                <canvas id="myChart"></canvas>
                            </div>
                        </div>
                        <div class="row" style="position:relative;">
                            <div class="col s12 table-responsive">
                                <table id="table" class="responsive display">
                                    <thead>
                                    <tr role="row">
                                        <th>Sr.No.</th>
                                        <th>Name</th>
                                        <th>Mobile</th>
                                        <th>Category</th>
                                        <th>State</th>
                                        <th>District</th>
                                        <th>Locality</th>
                                        <th>Call Person(s)</th>
                                        <th>Select Date</th>
                                        <th>Time</th>
                                        <th>Created At</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.js"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/select2.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/ui-alerts.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="<?=$tp; ?>/vendors/chartjs/chart.min.js" type="text/javascript"></script>
<?php echo $footer ?>
</body>

</html>

<script>
    $(document).ready(function(){

        $('#table').attr('style', 'width: 100%');
        $('.user').select2();
        $('.category').select2();
        $('.states').select2();
        $('.district').select2();
        $('.customer_class').select2();
        $('#view_graph').on('click', function(){
            if($('#graph').hasClass('show')) {
                $(this).html('<i class="material-icons right" style="margin-left:3px">show_chart</i>View Graph');
            }else{
                $(this).html('<i class="material-icons right" style="margin-left:3px">show_chart</i>Hide Graph');
            }
            $('#graph').toggleClass('show');
        })
        var startDate = moment('<?=$start_date?>', 'YYYY-MM-DD HH:mm:ss').format('DD, MMM YYYY HH:mm:ss');
        var endDate = moment('<?=$end_date?>', 'YYYY-MM-DD HH:mm:ss').format('DD, MMM YYYY HH:mm:ss');
        $('input[name="dates"]').daterangepicker({
            opens: 'left',
            locale: {
                format: 'DD, MMM YYYY'
            },
            startDate: startDate,
            endDate: endDate
        }, function(start, end, label) {
            $('#date_start').val(start.format('YYYY-MM-DD'));
            $('#date_end').val(end.format('YYYY-MM-DD'));
        });
        $(".states").change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-district-ajax",
                data:'cid='+val+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(".submit").html("Wait...");
                },
                success: function(data){
                    $(".district").html(data);
                    $(".submit").html("Search <i class='material-icons '>search</i>");
                }
            });
        });

    });
</script>
<script>
        var ctx = document.getElementById('myChart').getContext('2d');
        options = {
            responsive: true,
            maintainAspectRatio: false,
            aspectRatio: 5,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        };
        var chart = new Chart(ctx, {
            // The type of chart we want to create
            type: 'bar',

            // The data for our dataset
            data: {
                labels: [],
                datasets: [{
                    label: 'Calls',
                    backgroundColor: 'rgba(255, 99, 132, 0.60)',
                    borderColor: 'rgb(255, 99, 132)',
                    data: [],
                    borderWidth: 1
                }]
            },

            // Configuration options go here
            options: options
        });
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?=url("crm/get-calls-data") ?>",
                type: 'GET',
                data: function (d) {
                    d.category = $('#category').val();
                    d.state = $('#states').val();
                    d.district = $('#district').val();
                    d.acc_manager = $('#acc_manager').val();
                    d.cust_class = $('#customer_class').val();
                    d.dates = $('#date_start').val()+'$$'+$('#date_end').val();
                }
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name' },
                { data: 'contact_no', name: 'crm_customers.contact_no' },
                { data: 'type', name: 'customer_category.type' },
                { data: 'state_name', name: 'state_name', searchable: false },
                { data: 'district_name', name: 'district_name', searchable: false },
                { data: 'locality_name', name: 'locality_name', searchable: false },
                { data: 'user_id', name: 'user_id',orderable: false, searchable: false },
                { data: 'select_date', name: 'select_date',searchable: false },
                { data: 'time', name: 'time',searchable: false },
                { data: 'created_date', name: 'created_date',searchable: false },
                { data: 'action', name: 'action', orderable: false,searchable: false }
            ],
            order: [[0, 'desc']],
            dom: 'lBfrtip',
            buttons: <?=$buttons;?>,
            fixedColumns: true,
            colReorder: true,
            exportOptions:{
                columns: ':visible'
            }
        });
    getChartData(chart);
    $('#filterButton').click(function(){
        $('#table').DataTable().draw(true);
        getChartData(chart);
    });
    function getChartData(chart){
        var datax = false;
        var category = $('#category').val();
        var state = $('#states').val();
        var district = $('#district').val();
        var acc_manager = $('#acc_manager').val();
        var dates = $('#date_start').val()+'$$'+$('#date_end').val();
        $.ajax({
            url: 'get-calls-chart',
            type: 'POST',
            data: '_token=<?=csrf_token()?>&category='+category+'&state='+state+'&district='+district+'&acc_manager='+acc_manager+'&dates='+dates,
            success: function(data){
                datax = JSON.parse(data);
                cdata = chart.config.data;
                cdata.datasets[0].data = datax.cdata;
                cdata.labels = datax.labels;
                chart.update();
            }
        });
    }
</script>