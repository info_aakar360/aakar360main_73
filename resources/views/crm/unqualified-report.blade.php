<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="keywords" content="">
    <meta name="author" content="ThemeSelect">
    <title>
        <?=$title; ?>
    </title>
    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/select.dataTables.min.css">

    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />
    <!-- END: Custom CSS-->
</head>

<style type="text/css">

    .modal-window {
        position: fixed;
        background-color: rgba(200, 200, 200, 0.75);
        top: -70px;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 999;
        opacity: 0;
        pointer-events: none;
        -webkit-transition: all 0.3s;
        -moz-transition: all 0.3s;
        transition: all 0.3s;
    }

    .modal-window:target {
        opacity: 1;
        pointer-events: auto;
    }

    .modal-window>div {
        width: 800px;
        position: relative;
        margin: 10% auto;
        padding: 2rem;
        background: #fff;
        color: #444;
    }

    .modal-window header {
        font-weight: bold;
    }

    .modal-close {
        color: #aaa;
        line-height: 50px;
        font-size: 80%;
        position: absolute;
        right: 0;
        text-align: center;
        top: 0;
        width: 70px;
        text-decoration: none;
    }

    .modal-close:hover {
        color: #000;
    }

    .modal-window h1 {
        font-size: 150%;
        margin: 0 0 15px;
    }
</style>
<!-- END: Head-->
<?=$header;?>

<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 500px;">
                <h5 class="card-title" style="padding: 5px; color: #0d1baa;">Unqualified Report<a href="<?=url('crm/notinterested-report');?>" class="btn myblue waves-effect waves-light right" style="padding:0 5px; margin-top: 5px">Not-Interested Report</a></h5>
                <?=$notices;?>

                <?php if(!isset($_GET['add']) && !isset($_GET['edit'])) {?>

                    <div class="row">
                        <div class="col s12 m6 xl3">
                            <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">format_list_bulleted</i></div>
                            <div class="col s10 m10">
                                <label>Select Category</label>
                                <select class="browser-default category" id="category" name="category[]" multiple  tabindex="-1">

                                    <?php
                                    foreach ($customer_category as $cate) {
                                        $c_id = $cate->id;
                                        ?>
                                        <option value="<?= $c_id; ?>"><?= $cate->type ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col s12 m6 xl3">
                            <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">flag</i></div>
                            <div class="col s10 m10">
                                <label>Select State</label>
                                <select class="browser-default states" id="states" name="states[]" multiple  tabindex="-1">
                                    <?php
                                    foreach ($states as $state) {
                                        $v_id = $state->id;
                                        ?>
                                        <option value="<?=$v_id; ?>"><?= $state->name ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col s12 m6 xl3"> <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">adjust</i></div>
                            <div class="col s10 m10">
                                <label>Select District</label>
                                <select class="browser-default district" id="district" name="district[]" multiple  tabindex="-1">

                                </select>
                            </div>
                        </div>
                        <div class="col s12 m6 xl3">
                            <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">dns</i></div>
                            <div class="col s10 m10">
                                <label>Select Customer Class</label>
                                <select class="browser-default customer_class" id="customer_class" name="cust_class"  tabindex="-1">
                                    <option value="all">All</option>
                                    <?php
                                    foreach ($cust_classes as $cust_class) {
                                        $v_id = $cust_class->id;
                                        ?>
                                        <option value="<?=$v_id; ?>"><?= $cust_class->type ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col s12 m6 xl3">
                            <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">format_list_bulleted</i></div>
                            <div class="col s10 m10">
                                <label>Select Type</label>
                                <select class="browser-default type" id="type" name="type[]" multiple  tabindex="-1">

                                    <?php
                                    foreach ($customer_type as $type){
                                        $s_id = $type->id;
                                        ?>
                                        <option  value="<?=$s_id;?>"><?=$type->type?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                            <button class="btn myblue waves-effect waves-light submit" style="padding:0 5px;" id="filterButton"><i class="material-icons right" style="margin-left:3px">search</i>Search
                            </button>



                        </div>

                    </div>
                    <div class="divider"></div>
                    <div class="row" style="position:relative;">
                        <div class="col s12 table-responsive"><!--1x 11661593 6YDgrn-->
                            <table id="table" class="display">
                                <thead>
                                <tr role="row">
                                    <th>Sr.No.</th>
                                    <th>Name</th>
                                    <th>Mobile</th>
                                    <th>Category</th>
                                    <th>State</th>
                                    <th>District</th>
                                    <th style="width: 100px;">Date</th>
                                    <th>Reason</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                <?php } ?>
                <?php if(isset($_GET['add'])){ ?>
                    <h5>Add New Customer
                        <a class="btn myblue waves-light right" href="customers" >
                            <i class="material-icons right">add_circle_outline</i>Home
                        </a>
                    </h5>
                    <form method="post" action="customers" class="col s12 m12">
                        <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">account_circle</i>
                                <input id="Full_Name" type="text" class="validate" name="name">
                                <label for="Full_Name">Full Name</label>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">text_fields</i>
                                <input id="Proprieter" type="tel" class="validate" name="proprieter_name">
                                <label for="Proprieter">Company Name</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">email</i>
                                <input id="Email" type="text" class="validate" name="email">
                                <label for="Email">Email</label>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">phone</i>
                                <input id="Contact" type="tel" class="validate valid_no" name="contact_no"">
                                <label for="Contact">Contact No.</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">verified_user</i>
                                <input id="CID" type="text" class="validate" name="cid">
                                <label for="CID">CID</label>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">view_headline</i>
                                <select name="customer_category" class="cc" required> customer_category
                                    <option selected disabled value="" >Select Category</option>
                                    <?php
                                    foreach ($customer_category as $category){
                                        $s_id = $category->id;
                                        ?>
                                        <option  value="<?=$s_id;?>"><?=$category->type?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="customer_category">Category </label>

                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">view_quilt</i>
                                <select name="type">
                                    <?php
                                    foreach ($customer_type as $type){
                                        $s_id = $type->id;
                                        ?>
                                        <option  value="<?=$s_id;?>"><?=$type->type?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="type">Type </label>


                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">add_location</i>
                                <input id="Address" type="tel" class="validate" name="postal_address">
                                <label for="Address">Postal Address</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">add_location</i>
                                <select  class="state_list " name="state"  style="width: 100%" required>
                                    <?php
                                    foreach ($states as $state){
                                        $s_id = $state->id;
                                        ?>
                                        <option  <?php if(1 !=1) {
                                            echo "selected";
                                        } else {
                                            echo '';
                                        }?> value="<?=$s_id;?>"><?=$state->name?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="State">State</label>
                            </div>
                            <div class="input-field col s12 m6" >

                                <select  class="browser-default district dist_list" name="district" style="width: 100%" required>
                                    <option selected value="">District</option>
                                </select>

                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">security</i>
                                <select name="msg_active">
                                    <option value="0"> Yes</option>
                                    <option value="1"> No</option>
                                </select>
                                <label for="msg_active">Message Active</label>
                            </div>
                            <div class="input-field col s6">
                                <i class="material-icons prefix">security</i>
                                <select name="msg_class">
                                    <?php
                                    foreach ($customer_class as $class){
                                        $s_id = $class->id;
                                        ?>
                                        <option  value="<?=$s_id;?>"><?=$class->type?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="msg_class">Customer Class</label>
                            </div>

                            <div class="row category_price"></div>


                        </div>
                        <button class="btn myblue waves-light right add_submit" type="submit" disabled name="add">Add
                            <i class="material-icons right">save</i>
                        </button>

                    </form>
                <?php } ?>
                <?php if(isset($_GET['edit'])){
                $cid = $_GET['edit'];
                $get_cust = DB::select("SELECT * FROM `crm_customers` WHERE id = '$cid'")[0];
                    ?>
                    <h5>Edit Customer
                    </h5>
                    <form method="post" action="unqualified-report" class="col s12">
                        <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                        <input type="hidden" class="ccid" name="cidd" value="<?php echo $cid; ?>">
                        <div class="row">
                            <div class="input-field col s6">
                                <i class="material-icons prefix">account_circle</i>
                                <input type="text" id="reason" name="reason" class="form-control" value="<?=$get_cust->unqualified_reason;?>">
                                <label for="Full_Name">Reason</label>
                            </div>
                            <input id="CID" type="hidden" class="validate" name="cid">
                            <div class="input-field col s12 m6" style="margin-top: 0px;">
                                <i class="material-icons prefix" style="margin-top: 10px">person_add</i>
                                <div class="col s12 m12" style="margin-left: 20px">
                                    Select User
                                    <select class="browser-default user" name="user[]" multiple tabindex="-1" style="width: 100% !important;" > type
                                        <?php
                                        $u_id =  $get_cust->user_unqualified;
                                        $uid_array = explode(',',$u_id);
                                        foreach ($users as $user){
                                            ?>
                                            <option <?=(in_array($user->u_id,$uid_array))?'selected':''; ?> value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                            <?php
                                        }
                                        ?>
                                    </select>

                                </div>
                            </div>


                        </div>
<center>
                            <button class="btn myblue waves-light  edit_submit" type="submit" name="edit_reason">EDIT
                                <i class="material-icons right">save</i>
                            </button>
</center>
                        </div>

                    </form>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.js"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/select2.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/ui-alerts.js" type="text/javascript"></script>
<!-- <script src="<?php //$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script> -->
<script>
    $(function() {
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?=url("crm/get-unqualified-data") ?>",
                type: 'GET',
                data: function (d) {
                    d.category = $('#category').val();
                    d.state = $('#states').val();
                    d.district = $('#district').val();
                    d.cust_class = $('#customer_class').val();
                    d.type = $('#type').val();

                }
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name' },
                { data: 'contact_no', name: 'contact_no' },
                { data: 'type', name: 'customer_category.type' },
                { data: 'state_name', name: 'state_name', searchable: false },
                { data: 'district_name', name: 'district_name', searchable: false },
                { data: 'unqualified_date', name: 'unqualified_date', searchable: false },
                { data: 'unqualified_reason', name: 'unqualified_reason' },
                { data: 'action', name: 'action', orderable: false,searchable: false }
            ],
            order: [[0, 'desc']],
            dom: 'lBfrtip',
            buttons: <?=$buttons;?>,
            fixedColumns: true,
            colReorder: true,
            exportOptions:{
                columns: ':visible'
            }
        });
    });
    //    getMapData(map);
    $('#filterButton').click(function(){
        $('#table').DataTable().draw(true);
//        getMapData(map);
    });

    function getMapData(map){
        var datax = false;
        var category = $('#category').val();
        var state = $('#states').val();
        d.district = $('#district').val();
        d.cust_class = $('#customer_class').val();
        d.type = $('#type').val();
        d.ptype = $('#ptype').val();
        d.pstype = $('#pstype').val();
        $.ajax({
            url: 'get-visits-map',
            type: 'POST',
            data: '_token=<?=csrf_token()?>&category='+category+'&state='+state+'&district='+district+'&acc_manager='+acc_manager+'&dates='+dates,
            success: function(data){
                datax = JSON.parse(data);
                cdata = chart.config.data;
                cdata.datasets[0].data = datax.cdata;
                cdata.labels = datax.labels;
                chart.update();
            }
        });
    }
</script>
<script>
    $(document).ready(function(){
        $('#table').attr('style', 'width: 100%');
        $('.user').select2();
        $('.category').select2();
        $('.states').select2();
        $('.district').select2();
        $('.customer_class').select2();
        $('.type').select2();


        $(".states").change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-district-ajax",
                data:'cid='+val+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(".submit").html("Wait...");
                },
                success: function(data){
                    $(".district").html(data);
                    $(".submit").html("Search <i class='material-icons '>search</i>");
                }
            });
        });
        $('.ptype').change(function () {
            var val = $(this).val();

            $.ajax({
                type: "POST",
                url: "get-subtype",
                data:'tid='+val+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $('.pstype').html(data);
                }
            });
        });


    });
</script>
<?php echo $footer ?>
</body>
</html>

