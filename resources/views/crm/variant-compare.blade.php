<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="ThemeSelect">
    <title>
        <?=$title; ?>
    </title>
    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/select.dataTables.min.css">

    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />
    <!-- END: Custom CSS-->

    <!-- BEGIN VENDOR JS-->
    <script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="<?=$tp; ?>/vendors/data-tables/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="<?=$tp; ?>/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
    <script src="<?=$tp; ?>/vendors/data-tables/js/dataTables.select.min.js" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN THEME  JS-->
    <script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
    <script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
    <script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
    <!-- END THEME  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="<?=$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->
    <script src="<?=$tp; ?>/js/select2.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/SyntaxHighlighter/3.0.83/styles/shCoreDefault.css" />
    <link rel="stylesheet" href="<?=$tp; ?>/zebra_tooltips.min.css" type="text/css">

    <script src="https://cdn.jsdelivr.net/npm/zebra_pin@2.0.0/dist/zebra_pin.min.js"></script>
    <script src="<?=$tp; ?>/js/zebra_tooltips.min.js"></script>
</head>

<style type="text/css">

    .modal-window {
        position: fixed;
        background-color: rgba(200, 200, 200, 0.75);
        top: -70px;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 999;
        opacity: 0;
        pointer-events: none;
        -webkit-transition: all 0.3s;
        -moz-transition: all 0.3s;
        transition: all 0.3s;
    }

    .modal-window:target {
        opacity: 1;
        pointer-events: auto;
    }

    .modal-window>div {
        position: relative;
        margin-top: 20%;
        padding: 2rem;
        background: #fff;
        color: #444;
    }

    .modal-window header {
        font-weight: bold;
    }

    .modal-close {
        color: #aaa;
        line-height: 50px;
        font-size: 80%;
        position: absolute;
        right: 0;
        text-align: center;
        top: 0;
        width: 70px;
        text-decoration: none;
    }

    .modal-close:hover {
        color: #000;
    }

    .modal-window h1 {
        font-size: 150%;
        margin: 0 0 15px;
    }
    table th, td{
        border:1px solid #d8d3d8;
        padding-left: 15px;
    }
    .tableFixHead {
        overflow: auto;
        height: 79vh;
        width:100%
    }

    td:first-child, th:first-child {
        position:relative;
        left:0;
        z-index:1;
        background-color:white;
    }
    td:nth-child(2),th:nth-child(2)  {
        position:relative;
        left:0px;
        z-index:1;
        background-color:white;
    }
    .tableFixHead th {
        position: relative;
        top: 0;
        background: #ffffff;
        z-index:2
    }
    th:first-child , th:nth-child(2) {
        z-index:3

    }
</style>
<style>

    .ui-tooltip-content::after, .ui-tooltip-content::before {
        content: "";
        position: absolute;
        border-style: solid;
        display: block;
        left: 90px;
    }
    .ui-tooltip-content::before {
        bottom: -10px;
        border-color: #AAA transparent;
        border-width: 10px 10px 0;
    }
    .ui-tooltip-content::after {
        bottom: -7px;
        border-color: white transparent;
        border-width: 10px 10px 0;
    }
</style>
<!-- END: Head-->
<?=$header;?>

<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div class="container">
            <div class="section section-data-tables">
                <div class="row " style="height: 125px;float:none;" id="filters">
                    <div class="col s12 m12 l12">
                        <div id="button-trigger" class="card card card-default scrollspy">
                            <div class="card-content" style="padding: 10px !important;">
                                <h5 class="card-title" style="padding: 10px; color: #0d1baa;">Quotation</h5>

                                <form method="post">
                                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                    <div class="row">
                                        <div class="col s12 m6 xl3">
                                            <div class="col s1 m1" style="text-align: center; margin-top: 30px"> <i class="material-icons prefix">format_list_bulleted</i></div>
                                            <div class="col s10 m10">
                                                <label>Select Category</label>
                                                <select class="browser-default category" name="category[]" multiple tabindex="-1">
                                                    <?php
                                                    foreach ($category as $cate) {
                                                        $c_id = $cate->id;
                                                        ?>
                                                        <option <?php if (in_array($c_id, $selected_category)) { echo 'selected'; } else { echo ''; } ?>
                                                                value="<?= $c_id; ?>"><?= $cate->name ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col s12 m6 xl3">
                                            <div class="col s1 m1" style="text-align: center; margin-top: 30px"> <i class="material-icons prefix">pie_chart</i></div>
                                            <div class="col s10 m10">
                                                <label>Select Variant</label>
                                                <select class="browser-default variants" name="variants[]" multiple tabindex="-1">
                                                    <?php $selected_variant1 = explode(',',$selected_variant);
                                                    foreach ($all_variants as $variant) {
                                                        $v_id = $variant->id;
                                                        ?>
                                                        <option <?php if (in_array($v_id, $selected_variant1)) { echo 'selected'; } else { echo ''; } ?>
                                                                value="<?= $v_id; ?>"><?= $variant->variant_title ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col s12 m6 xl3 ">
                                            <div class="col s1 m1" style="text-align: center; margin-top: 30px"> <i class="material-icons prefix">supervisor_account</i></div>
                                            <div class="col s10 m10">
                                                <label>Select Customer</label>
                                                <select class="browser-default customers" name="customer_search"  tabindex="-1">
                                                    <option value="" disabled selected>Please Select Customer</option>
                                                    <?php
                                                    foreach ($customers as $cust) {
                                                        $custid = $cust->id;
                                                        ?>
                                                        <option <?php if ($cust_id == $custid ) { echo 'selected'; } else { echo ''; } ?>
                                                                value="<?= $custid; ?>"><?= $cust->name ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col s12 m6 xl3">
                                            <div class="col s1 m1" style="text-align: center; margin-top: 30px"> <i class="material-icons prefix">description</i></div>
                                            <div class="col s10 m10">
                                                <label>Select Report Type</label>
                                                <select class="browser-default category" name="report_type" >
                                                    <option value="">Select Report Type</option>
                                                    <option value="brand_wise" <?=($report_type=='brand_wise') ? 'selected' : 'selected'; ?>>Brand Wise</option>
                                                    <option value="price_wise" <?=($report_type=='price_wise') ? 'selected' : ''; ?>>Price Wise</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col s12 m6 xl3">
                                            <div class="col s1 m1" style="text-align: center; margin-top: 30px"> <i class="material-icons prefix">description</i></div>
                                            <div class="col s10 m10">
                                                <label>Select Manufacturing Hub</label>
                                                <select class="browser-default category" name="hub" >
                                                    <option value="">Select Manufacturing Hub</option>
                                                    <?php
                                                    foreach ($mhs as $mh) {
                                                        $sel = $mh->id;
                                                        //dd($mh->title);
                                                        ?>
                                                        <option <?php if ($sel == $mhid ) { echo 'selected'; } else { echo ''; } ?>
                                                            <?php if ($mh->title == 'Ex Raipur ' ) { echo 'selected'; } else { echo ''; } ?>
                                                                value="<?= $sel; ?>"><?=$mh->title ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col s12 m6 xl3">
                                            <div class="col s1 m1" style="text-align: center; margin-top: 30px"> <i class="material-icons prefix">supervisor_account</i></div>
                                            <div class="col s10 m10">
                                                <label>Select Customer Type</label>
                                                <select class="browser-default category" name="cust_type" >
                                                    <option value="">Select Customer Type</option>
                                                    <option value="individual" <?=($cust_type=='individual') ? 'selected' : ''; ?>>Individual</option>
                                                    <option value="institutional" <?=($cust_type=='institutional') ? 'selected' : 'selected'; ?>>Institutional</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col s12 m6 xl3">
                                            <div class="col s1 m1" style="text-align: center; margin-top: 30px"> <i class="material-icons prefix">description</i></div>
                                            <div class="col s10 m10">
                                                <label>Select View Type</label>
                                                <select class="browser-default category" name="view_type" >
                                                    <option value="">Select View Type</option>
                                                    <option value="detail_view" <?=($view_type=='detail_view') ? 'selected' : ''; ?>>Detail View</option>
                                                    <option value="short_view" <?=($view_type=='short_view') ? 'selected' : 'selected'; ?>>Short View</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col s12 m12" style="text-align: center">
                                            <button style="margin-top: 20px;" class="btn myblue waves-effect waves-light submit" type="submit" name="category_find">
                                                <i class="material-icons right">search</i>Search
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row brand_wise" style="float:none">
                    <div class="detail_view" style="float:none" id="fullscreen">
                        <div class="col s12" style="">
                            <div class="card" style="height: 86vh;">
                                <div class="card-content" style="padding-top: 5px;">
                                    <h4 class="card-title">Variants Report  (Brand Wise) (Detail View)
                                        <a href="#open-modalcomp"><button class="col s12 m2 btn btn-primary myblue right btn-sm" style="margin-top: 2px; width: 125px; padding: 1px;" id="quotation3">Quotation</button></a>
                                        <a href="#open-modal2" class="col s12 m2 btn btn-primary myblue right btn-sm" style="margin-top: 2px; width: 125px; padding: 1px;"  id="" >Set Price</a>
                                        <button class="col s12 m2 btn myblue for_unhide right btn-sm" style="margin-top: 2px;  width: 90px; padding: 1px;" > Unhide</button>
                                        <button class="col s12 m2 btn myblue for_hide right btn-sm" style="margin-top: 2px; width: 65px; padding: 1px;"> Hide</button>
                                        <button class="col s12 m2 btn myblue right btn-sm" style="margin-top: 2px; width: 35px; padding: 1px; padding-top: 5px;" onclick="showFilters()">
											<span class="material-icons">
												keyboard_arrow_down
											</span>
                                        </button>
                                        <button class="col s12 m2 btn myblue right btn-sm" style="margin-top: 2px; width: 35px; padding: 1px; padding-top: 5px;" onclick="hideFilters()">
											<span class="material-icons">
												keyboard_arrow_up
											</span>
                                        </button>
                                        <button class="col s12 m2 btn myblue right btn-sm fs-button" style="margin-top: 2px; width: 35px; padding: 1px; padding-top: 5px;">
											<span class="material-icons">
												fullscreen
											</span>
                                        </button>
                                    </h4>

                                    <?php $ps ='';  if($set_price!=0){ $ps .= ' + S '; } if($cust_id!= 0 ){ $ps .= ' + C '; } ?>
                                    <p>Price Shown : P <?=$ps?> </p>

                                    <div class="row ">
                                        <div class="tableFixHead" style="border: 1px solid #243eb6;">
                                            <input type="hidden" value="" id="checked_col">
                                            <table class="table table-striped brand_type_report" id="myTable">
                                                <form id="brand_wise_form" action="pdf-preview" method="post">
                                                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                                    <input type="hidden" name="quotation" value="quotation">
                                                    <input type="hidden" name="set_price" value="<?=$set_price?>">
                                                    <input type="hidden" name="cust_id" value="<?=$cust_id?>">
                                                    <input type="hidden" name="company" value="" id="company">
                                                    <input type="hidden" name="hub" value="<?=$mhid?>" id="hub">
                                                    <input type="hidden" value="<?php echo  $cust_type;?>"  name="cust_type">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 30px;">#</th>
                                                        <th style="width: 250px;">
                                                            <label>
                                                                <input class="v_check" name="for_quot"  type="checkbox">
                                                                <span > Variant Title </span>
                                                            </label>
                                                        </th>
                                                        <?php $vid = array(); $k=3; foreach ($brands as $brand) {
                                                            ?>
                                                            <th style="width: 100px;">
                                                                <input class="brand_id" name="brand_id[]"  type="hidden" value="<?=$brand->id?>">
                                                                <input class="bid" name="bid[]"  type="hidden" value="<?=$brand->id?>">
                                                                <label>
                                                                    <input type="checkbox" checked="checked" data-control-column="<?=$k;?>" class="opt" />
                                                                    <span > <?=$brand->title?> </span>
                                                                </label>
                                                            </th>
                                                            <?php
                                                            $k++;}
                                                        ?>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    <?php $i=1; foreach ($variants as $variant) { ?>
                                                        <tr >
                                                            <td style="padding-top: 0px; padding-bottom: 0px;">
                                                                <?=$i;?>
                                                            </td>
                                                            <td  style="padding-top: 0px; padding-bottom: 0px;">
                                                                <input class="vid" name="vid[]"  type="hidden" value="<?=$variant->id?>">
                                                                <input class="vid1" name="vid1[]"  type="hidden" value="0">
                                                                <input class="vid2" name="vid2[]"  type="hidden" value="0">
                                                                <label>
                                                                    <input class="v_list" name="v_list"  type="checkbox">
                                                                    <span > <?=$vt = variantTitle($variant->variant_title);?> </span>
                                                                </label>
                                                            </td>
                                                            <?php
                                                            foreach ($brands as $brand) {
                                                            $bid = $brand->id;
                                                            ?>
                                                            <td style="text-align: center; !important; padding-top: 0px; padding-bottom: 0px;"  class="table_data">
                                                                <?php

                                                                $variants_details = DB::select("SELECT product_variants.*,p.purchase_price,p.tax FROM product_variants INNER JOIN products as p ON product_variants.product_id = p.id WHERE variant_title = '$variant->variant_title' AND product_id = '$bid' limit 1");
                                                                //echo $bid;
                                                                $get_cat =  DB::table('products')->select('category')->where('id','=',$bid)->first();
                                                                $get_cat_id = $get_cat->category;
                                                                $cust_product_price = getCategoryAddPrice($selected_category, $cust_id);
                                                                if(!count($variants_details)){
                                                                    echo '-' ;
                                                                }
                                                                else{
                                                                ?>
                                                                <input type="hidden" name="var_id[]" class="hide_vid1" value="<?=$variant->id?>">
                                                                <input type="hidden" name="brn_id[]" class="hide_bid1" value="<?=$bid?>">
                                                                <input type="hidden" name="hide_status[]" class="hide_status" value="0">
                                                                <br>
                                                                <span style="border-right: 1px;" class="hide_details">
																<?php
                                                                $loading = getLoadingByHub($bid, $mhid, $cust_type);
                                                                if($loading == null){
                                                                    $loading = 0;
                                                                }
                                                                $sd = is_numeric($variants_details[0]->price) ? $variants_details[0]->price : 0;
                                                                $sd = getSize($sd);
                                                                $wd = $variants_details[0]->weight ? $variants_details[0]->weight : 0.0;
                                                                $light =$variants_details[0]->light ? $variants_details[0]->light : 0.0;
                                                                $su_light = ($variants_details[0]->super_light) ? $variants_details[0]->super_light : 0.0;
                                                                $len = is_numeric($variants_details[0]->length) ? $variants_details[0]->length : 0;
                                                                $pp = is_numeric($variants_details[0]->purchase_price) ? $variants_details[0]->purchase_price : 0;
                                                                $pp = $pp + $set_price + $cust_product_price;
                                                                $tax = $variants_details[0]->tax;
                                                                $txt_val = ((int)$sd+(int)$pp+(int)$loading);
                                                                $tx_val = round($txt_val*(int)$tax/100);
                                                                $result = $tx_val + ($sd+$pp+$loading);
                                                                echo "<b>".$result."</b>";

                                                                echo "<br><spsn style='font-size: 11px;' > (". $pp .' + '.$loading.' + '.$sd . '+' . $tx_val . " )</spsn>";
                                                                echo " </span>";

                                                                $size_weight =  DB::table('size_weight')->where('id',$variants_details[0]->length)->first();

                                                                if($size_weight !== null){
                                                                    $wunit = DB::table('units')->where('id',$size_weight->wunit)->first();
                                                                    $lunit = DB::table('units')->where('id',$size_weight->lunit)->first();
                                                                    echo "<br><span style='font-size: 11px;' >";
                                                                    echo 'L('. $size_weight->length .' '.$lunit->name.'), W( '.$size_weight->weight . ' '.$wunit->name.')';
                                                                    echo "</span>";
                                                                }

                                                                ?><br/>
																	<button style="line-height:20px;height: 20px;width: 20px;" type="button" class="mb-6 btn-floating waves-effect waves-light green darken-1"><i class="material-icons unhide_price" style="font-size: 15px; line-height: 20px;">visibility</i></button>
																	<button  style="line-height:20px;height: 20px;width: 20px;" type="button" value="<?=$bid;?>" class="mb-6 btn-floating waves-effect waves-light red darken-1 hide_price"><i class="material-icons" style="font-size: 15px; line-height: 20px;">visibility_off</i></button><br/>
                                                                    <!--                                                                <hr style="height: 500px; width: 1px;">-->

                                                                    <?php
                                                                    //                                                                echo '<b>Weight(H+L+S)</b><br/>';
                                                                    //                                                                echo "<b>".$result = $wd + $light+$su_light."</b>";
                                                                    //                                                                echo "<br><spsn style='font-size: 11px;' > (". $wd .' + '.$light . '+' . $su_light . " )</spsn><br/>";
                                                                    //                                                                echo '<b>Length(l1+l2+l3+l4)</b><br/>';
                                                                    //                                                                echo "<b>".$result = $len."</b>";
                                                                    }
                                                                    }
                                                                    ?>
                                                        </tr>
                                                        <?php
                                                        $i++; }
                                                    ?>
                                                    </tbody>
                                                </form>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>




                    <div class="short_view" style="float:none" id="fullscreen">
                        <div class="col s12" style="">
                            <div class="card" style="height: 86vh;">
                                <div class="card-content" style="padding-top: 5px;">
                                    <h4 class="card-title">Variants Report (Brand Wise) (Short View) <?php $ps ='';  if($set_price!=0){ $ps .= ' + S '; } if($cust_id!= 0 ){ $ps .= ' + C '; } ?>
                                        Price Shown : P <?=$ps?>
                                        <a href="#open-modalshort"><button class="col s12 m2 btn btn-primary myblue right btn-sm" style="margin-top: 2px; width: 125px; padding: 1px;" id="quotationShort">Quotation</button></a>
                                        <a href="#open-modal2" class="col s12 m2 btn btn-primary myblue right btn-sm" style="margin-top: 2px; width: 125px; padding: 1px;"  id="" >Set Price</a>
                                        <button class="col s12 m2 btn myblue for_unhide right btn-sm" style="margin-top: 2px;  width: 90px; padding: 1px;" > Unhide</button>
                                        <button class="col s12 m2 btn myblue for_hide right btn-sm" style="margin-top: 2px; width: 65px; padding: 1px;"> Hide</button>
                                        <button class="col s12 m2 btn myblue right btn-sm" style="margin-top: 2px; width: 35px; padding: 1px; padding-top: 5px;" onclick="showFilters()">
											<span class="material-icons">
												keyboard_arrow_down
											</span>
                                        </button>
                                        <button class="col s12 m2 btn myblue right btn-sm" style="margin-top: 2px; width: 35px; padding: 1px; padding-top: 5px;" onclick="hideFilters()">
											<span class="material-icons">
												keyboard_arrow_up
											</span>
                                        </button>
                                        <button class="col s12 m2 btn myblue right btn-sm fs-button" style="margin-top: 2px; width: 35px; padding: 1px; padding-top: 5px;">
											<span class="material-icons">
												fullscreen
											</span>
                                        </button>
                                    </h4>



                                    <div class="row ">
                                        <div class="tableFixHead" style="border: 1px solid #243eb6;">
                                            <input type="hidden" value="" id="checked_col">
                                            <table class="table table-striped brand_type_report" id="myTable">
                                                <form id="brand_wise_short_form" action="pdf-preview" method="post">
                                                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                                    <input type="hidden" name="quotation" value="quotation">
                                                    <input type="hidden" name="set_price" value="<?=$set_price?>">
                                                    <input type="hidden" name="cust_id" value="<?=$cust_id?>">
                                                    <input type="hidden" name="company" value="" id="companyshort">
                                                    <input type="hidden" name="hub" value="<?=$mhid?>" id="hub">
                                                    <input type="hidden" value="<?php echo  $cust_type;?>"  name="cust_type">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 30px;">#</th>
                                                        <th style="width: 250px;">
                                                            <label>
                                                                <input class="v_check" name="for_quot"  type="checkbox">
                                                                <span > Variant Title </span>
                                                            </label>
                                                        </th>
                                                        <?php $vid = array(); $k=3; foreach ($brands as $brand) {
                                                            ?>
                                                            <th style="width: 100px;">
                                                                <input class="brand_id" name="brand_id[]"  type="hidden" value="<?=$brand->id?>">
                                                                <input class="bid" name="bid[]"  type="hidden" value="<?=$brand->id?>">
                                                                <label>
                                                                    <input type="checkbox" checked="checked" data-control-column="<?=$k;?>" class="opt" />
                                                                    <span > <?=$brand->title?> </span>
                                                                </label>
                                                            </th>
                                                            <?php
                                                            $k++;}
                                                        ?>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    <tr >
                                                        <td style="padding-top: 0px; padding-bottom: 0px;">
                                                            #
                                                        </td>
                                                        <td  style="padding-top: 0px; padding-bottom: 0px;">
                                                            <b>Basic Price</b>
                                                        </td>
                                                        <?php
                                                        $j=1;
                                                        foreach ($brands as $key=>$brand) {
                                                            $bid = $brand->id;
                                                            $cust_product_price = getCategoryAddPrice($selected_category, $cust_id);
                                                            ?>
                                                            <td style="text-align: center; !important; padding-top: 0px; padding-bottom: 0px;"  class="table_data">
                                                                <?=($brand->purchase_price+$set_price+$cust_product_price);?>
                                                            </td>
                                                            <?php $j++; }?>
                                                    </tr>

                                                    <?php $i=1; foreach ($variants as $v=>$variant) { ?>

                                                        <tr >
                                                            <td style="padding-top: 0px; padding-bottom: 0px;">
                                                                <?=$i;?>
                                                            </td>
                                                            <td  style="padding-top: 0px; padding-bottom: 0px;">
                                                                <input class="vid" name="vid[]"  type="hidden" value="<?=$variant->id?>">
                                                                <input class="vid1" name="vid1[]"  type="hidden" value="0">
                                                                <input class="vid2" name="vid2[]"  type="hidden" value="0">
                                                                <label>
                                                                    <input class="v_list" name="v_list"  type="checkbox">
                                                                    <span > <?=$vt = variantTitle($variant->variant_title);?> </span>
                                                                </label>

                                                            </td>
                                                            <?php
                                                            foreach ($brands as $key=>$brand) {
                                                            $bid = $brand->id;
                                                            ?>
                                                            <td style="text-align: center; !important; padding-top: 0px; padding-bottom: 0px;"  class="table_data">
                                                                <?php

                                                                $variants_details = DB::select("SELECT product_variants.*,p.purchase_price,p.tax FROM product_variants INNER JOIN products as p ON product_variants.product_id = p.id WHERE variant_title = '$variant->variant_title' AND product_id = '$bid' limit 1");
                                                                //echo $bid;
                                                                $get_cat =  DB::table('products')->select('category')->where('id','=',$bid)->first();
                                                                $get_cat_id = $get_cat->category;
                                                                $cust_product_price = getCategoryAddPrice($selected_category, $cust_id);


                                                                if(!count($variants_details)){
                                                                    echo '-' ;
                                                                }
                                                                else{

                                                                    ?>
                                                                    <input type="hidden" name="var_id[]" class="hide_vid1" value="<?=$variant->id?>">
                                                                    <input type="hidden" name="brn_id[]" class="hide_bid1" value="<?=$bid?>">
                                                                    <input type="hidden" name="hide_status[]" class="hide_status" value="0">

                                                                    <?php
                                                                    $loading = getLoadingByHub($bid, $mhid, $cust_type);
                                                                    if($loading == null){
                                                                        $loading = 0;
                                                                    }
                                                                    $sd = is_numeric($variants_details[0]->price) ? $variants_details[0]->price : 0;
                                                                    $sd = getSize($sd);
                                                                    $wd = $variants_details[0]->weight ? $variants_details[0]->weight : 0.0;
                                                                    $light =$variants_details[0]->light ? $variants_details[0]->light : 0.0;
                                                                    $su_light = ($variants_details[0]->super_light) ? $variants_details[0]->super_light : 0.0;
                                                                    $len = is_numeric($variants_details[0]->length) ? $variants_details[0]->length : 0;
                                                                    $pp = is_numeric($variants_details[0]->purchase_price) ? $variants_details[0]->purchase_price : 0;
                                                                    $pp = $pp + $set_price + $cust_product_price;
                                                                    $tax = $variants_details[0]->tax;
                                                                    $txt_val = ((int)$sd+(int)$pp+(int)$loading);
                                                                    $tx_val = round($txt_val*(int)$tax/100);
                                                                    //echo $tx_val;
                                                                    $result = $tx_val + ($sd+$pp+$loading); ?>
                                                                    <a href="javascript: void(0)"
                                                                       class="zebra_tooltips_custom_width_more"
                                                                       title="<b style='text-align: center;'><?php echo $result; ?></b> <hr>
																   <span style='font-size: 11px;' > (<?=$pp; ?>+<?=$loading; ?>+<?=$sd; ?>+<?=$tx_val; ?>)</span>
																   <br><span style='font-size: 11px;' >
																   <?php
                                                                       $size_weight =  DB::table('size_weight')->where('id',$variants_details[0]->length)->first();

                                                                       if($size_weight !== null){
                                                                           $wunit = DB::table('units')->where('id',$size_weight->wunit)->first();
                                                                           $lunit = DB::table('units')->where('id',$size_weight->lunit)->first();
                                                                           echo 'L('. $size_weight->length .' '.$lunit->name.'), W( '.$size_weight->weight . ' '.$wunit->name.')';
                                                                       }?>
																</span>">
                                                                        <?=$sd; ?>
                                                                    </a>
                                                                <?php } ?>
                                                                <script>
                                                                    $(document).ready(function() {
                                                                        new $.Zebra_Tooltips($('.zebra_tooltips_custom_width_more'), {
                                                                            max_width:  470
                                                                        });
                                                                        new $.Zebra_Tooltips($('.zebra_tooltips_custom_width_more'), {
                                                                            max_width:  90
                                                                        });
                                                                    });
                                                                </script>
                                                                <?php }
                                                                ?>

                                                        </tr>
                                                        <?php
                                                        $i++; }
                                                    ?>
                                                    </tbody>

                                                </form>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row price_wise" style="float:none" id="fullscreen">
                    <div class="col s12 m12 l12" style="">
                        <div id="button-trigger" class="card card card-default scrollspy" style="height: 86vh;">
                            <div class="card-content" style="padding-top: 5px;">
                                <h4 class="card-title">Variants Report  (Price Wise)
                                    <a href="#open-modalcompprice" class="col s12 m2 btn btn-primary myblue right" style="margin-top: 2px; width: 125px; padding: 1px;" id="quotation2" >Quotation</a>
                                    <a href="#open-modal2" class="col s12 m2 btn btn-primary myblue right" style="margin-top: 2px; width: 125px; padding: 1px;"  id="" >Set Price</a>
                                    <button class="col s12 m2 btn myblue for_unhide right" style="margin-top: 2px;  width: 90px; padding: 1px;" > Unhide</button>
                                    <button class="col s12 m2 btn myblue for_hide right" style="margin-top: 2px; width: 65px; padding: 1px;"> Hide</button>
                                    <button class="col s12 m2 btn myblue right btn-sm" style="margin-top: 2px; width: 35px; padding: 1px; padding-top: 5px;" onclick="showFilters()">
										<span class="material-icons">
											keyboard_arrow_down
										</span>
                                    </button>
                                    <button class="col s12 m2 btn myblue right btn-sm" style="margin-top: 2px; width: 35px; padding: 1px; padding-top: 5px;" onclick="hideFilters()">
										<span class="material-icons">
											keyboard_arrow_up
										</span>
                                    </button>
                                    <button class="col s12 m2 btn myblue right btn-sm fs-button" style="margin-top: 2px; width: 35px; padding: 1px; padding-top: 5px;">
										<span class="material-icons">
											fullscreen
										</span>
                                    </button>
                                </h4>
                                <?php $ps ='';  if($set_price!=0){ $ps .= ' + S '; } if($cust_id!= 0 ){ $ps .= ' + C '; } ?>
                                <p>Price Shown : P <?=$ps?> (<?=getCategoryAddPrice($selected_category, $cust_id)?>)</p>
                                <div class="row">
                                    <div class="col s12">
                                        <div class="tableFixHead" style="border: 1px solid #243eb6;">
                                            <table class="table table-striped price_type_report" >
                                                <form id="price_wise_form" action="pdf-preview" method="post">
                                                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                                    <input type="hidden" name="quotation_price" value="quotation_price">
                                                    <input type="hidden" name="set_price" value="<?=$set_price?>">
                                                    <input type="hidden" name="cust_id" value="<?=$cust_id?>">
                                                    <input type="hidden" name="company" value="" id="companyp">
                                                    <input type="hidden" name="hub" value="<?=$mhid?>" id="hub">
                                                    <input type="hidden" value="<?php echo  $cust_type;?>"  name="cust_type">
                                                    <thead>
                                                    <tr role="row">
                                                        <th style="width: 30px;">Sr.No.</th>
                                                        <th style="width: 250px;">
                                                            <label>
                                                                <input class="v_check" name="for_quot"  type="checkbox">
                                                                <span > Variant Title </span>
                                                            </label>
                                                        </th>
                                                        <th>Price</th>
                                                        <th style="width: 100px;">Brand</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    <?php $i=1; foreach ($variants as $variant) { ?>
                                                        <tr >
                                                            <td>
                                                                <?=$i;?>
                                                            </td>
                                                            <td>
                                                                <input class="vid" name="vid[]"  type="hidden" value="<?=$variant->id?>">
                                                                <input class="vid1" name="vid1[]"  type="hidden" value="0">
                                                                <input class="vid2" name="vid2[]"  type="hidden" value="0">
                                                                <label>
                                                                    <input class="v_list" name="v_list"  type="checkbox">
                                                                    <span > <?=$vt = variantTitle($variant->variant_title)?> </span>
                                                                </label>
                                                            </td>
                                                            <td >
                                                                <?php
                                                                $variants_details = DB::select("SELECT t1.*,p.title,p.purchase_price,p.tax FROM products as p  INNER JOIN (SELECT * FROM product_variants WHERE price = (SELECT min(price) FROM product_variants WHERE variant_title = '$variant->variant_title')) as t1 ON p.id = t1.product_id")[0];
                                                                $cust_product_price = getCategoryAddPrice($selected_category, $cust_id);
                                                                $sd = is_numeric($variants_details->price) ? $variants_details->price : 0;
                                                                $sd = getSize($sd);
                                                                $pp = is_numeric($variants_details->purchase_price) ? $variants_details->purchase_price : 0;
                                                                $pp = $pp+$set_price + $cust_product_price;
                                                                $tax = $variants_details->tax;
                                                                $tx_val =  ($sd+$pp)*(int)$tax/100;
                                                                echo "<b>".$result = $tx_val + ($sd+$pp)."</b>";
                                                                echo "<br><spsn style='font-size: 11px;' > (". $pp .' + '.$sd . '+' . $tx_val . " )</spsn>";
                                                                echo "<b>".$result = $tx_val + ($sd+$pp)."</b>";
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?= $variants_details->title?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        $i++; }
                                                    ?>
                                                    </tbody>
                                                </form>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- DataTables example -->
        </div>
    </div>
</div>
<input type="hidden" id="get_report_view" value="<?=$report_type;?>">
<input type="hidden" id="get_view_type" value="<?=$view_type;?>">
<script>
    $(document).ready(function(){
        $('.category').select2();
        $('.variants').select2();
        $('.customers').select2();
        $('.company').select2();
        $('.price_wise').hide();
        $('.brand_wise').hide();
        $('.detail_view').hide();
        $('.short_view').hide();
        $(".category").change(function(){
            var val = $(this).val();
            var cname = $(".category option:selected").html();

            $.ajax({
                type: "POST",
                url: "get-report-option",
                data:'cid='+val+'&cname='+cname+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $("#search-box").css("background","#FFF url(assets/LoaderIcon.gif) no-repeat 165px");
                },
                success: function(data){
                    $(".report_div").html(data);
                    $("#model_report_type").val(data);
                    $("#model_view_type").val(data);
                }
            });
        });
        var get_report_type = $('#get_report_view').val();
        var get_view_type = $('#get_view_type').val();
        if(get_report_type=='brand_wise'){
            $('.brand_wise').show();
        }
        if(get_report_type=='price_wise'){
            $('.price_wise').show();
        }

        if(get_view_type=='detail_view'){
            $('.detail_view').show();
        }
        if(get_view_type=='short_view'){
            $('.short_view').show();
        }

        $('.v_check').click(function () {
            $(".brand_type_report tr").has(".v_list").show();
            $(".price_type_report tr").has(".v_list").show();
            var isChecked = $(".v_check").is(":checked");
            if (isChecked) {
                $(".v_list").attr('checked', 'checked');
                $(".vid1").val(1);
                $(".vid2").val(1);

            } else {
                $(".v_list").removeAttr('checked');
                $(".vid1").val(0);
                $(".vid2").val(0);
            }
        });

        $('.v_list').click(function () {
            var isChecked = $(this).is(":checked");

            if (isChecked) {
                $(this).attr('checked', 'checked');
                $(this).closest('tr').find(".vid1").val(1);
                $(this).closest('tr').find(".vid2").val(1);
            } else {
                $(this).removeAttr('checked');
                $(this).closest('tr').find(".vid1").val(0);
                $(this).closest('tr').find(".vid2").val(0);
            }
        });

        $('.opt').change(function(){
            var isChecked = $(this).is(":checked");
            if (isChecked) {
                var b = $(this).closest('th').find(".brand_id").val();
                $(this).closest('th').find(".bid").val(b);
            } else {
                $(this).closest('th').find(".bid").val(0);
            }
            var states = [];
            $('.opt').each(function(){
                if(!$(this).is(':checked')) states.push($(this).data('control-column'));
            });
            $('#checked_col').val(states);
            // setSates(states);
        });

        function setSates(states){
            if(states){
                //  if($.isArray( states )) states = JSON.parse(states); // if sates came from localstorage it will be a string, convert it to an array
                // alert(states);
                var hidVal = document.getElementById('checked_col').value;
                var myArr = hidVal.split(",");

                var myArr2 = hidVal.split(",").length;
                if(myArr2==1){
                    var column = myArr;
//                        alert('column ='+column);
//                        alert('states =' +states);
                    if( column == states){
                        $(this).attr('checked', false);
                        $('#myTable td:nth-child('+column+'), #myTable th:nth-child('+column+')').hide();
                    }
                    else{
                        $(this).attr('checked', true);
                        $('#myTable td:nth-child('+column+'), #myTable th:nth-child('+column+')').show();
                    }
                }
                else{
                    for(i=0; i < myArr.length;i++)
                    {
                        var column = myArr[i];
                        $('.opt').each(function(i,e){
//                                alert('column2 = '+column);
//                                alert('states2 = '+states);
                            if(column == states){
                                $(this).attr('checked', false);
                                $('#myTable td:nth-child('+column+'), #myTable th:nth-child('+column+')').show();
                            }
                            else{
                                $(this).attr('checked', true);
                                $('#myTable td:nth-child('+column+'), #myTable th:nth-child('+column+')').hide();
                            }
                        });
                    }
                }

                /*$('.opt').each(function(i,e){
                    var column =$(this).data('control-column');
                    alert('column ='+column);
                    alert('states =' +states);
                    if($.inArray( column, states ) == -1){
                        $(this).attr('checked', false);
                        $('#myTable td:nth-child('+column+'), #myTable th:nth-child('+column+')').show();
                    }
                    else{
                        $(this).attr('checked', true);
                        $('#myTable td:nth-child('+column+'), #myTable th:nth-child('+column+')').hide();
                    }
                });*/
                localStorage.setItem('states', JSON.stringify(states));
            }

        }
// this will read and set the initial states when the page loads
        //setSates( localStorage.getItem('states') );


        $('.for_hide').click(function () {
            $(".brand_type_report tr").has(".v_list:not(:checked)").hide();
            $(".price_type_report tr").has(".v_list:not(:checked)").hide();
            var states = $('#checked_col').val();
            setSates(states);
        });
        $('.for_unhide').click(function () {
            $(".brand_type_report tr").has(".v_list").show();
            $(".brand_type_report th").has(".opt").show();
            $(".price_type_report tr").has(".v_list").show();
            $(".price_type_report th").has(".opt").show();
            $('.table_data').css('display','');
        });

        $('#quotation').click(function () {
//            alert('hello');
            $('#open-modalcomp').modal('show');
//            $('#brand_wise_form').submit();
        });
        $('#quotation3').click(function () {
//            alert('hello');
            $('#open-modalcomp').modal('show');
//            $('#brand_wise_form').submit();
        });
        $('.company').change(function(){
            var val = $(this).val();
            if(val!== ''){
                $("#company").val(val);
                $('#brand_wise_form').submit();
            }
            else{
                alert('please choose company');
            }
            // setSates(states);
        });
        $('.companyp').change(function(){
            var val = $(this).val();
            if(val!== ''){
                $("#companyp").val(val);
                $('#price_wise_form').submit();
            }
            else{
                alert('please choose company');
            }
            // setSates(states);
        });
        $('.companyshort').change(function(){
            var val = $(this).val();
            if(val!== ''){
                $("#companyshort").val(val);
                $('#brand_wise_short_form').submit();
            }
            else{
                alert('please choose company');
            }
            // setSates(states);
        });
        $('#quotation2').click(function () {
            $('#open-modalcompprice').modal('show');
            //$('#price_wise_form').submit();
        });
        $('#quotationShort').click(function () {
            $('#open-modalshort').modal('show');
            //$('#price_wise_form').submit();
        });
        $('.hide_price').click(function () {
            $(this).closest('td').find(".hide_status").val(1);
            $(this).closest('td').find(".hide_details").css('display','none');
        });
        $('.unhide_price').click(function () {
            $(this).closest('td').find(".hide_status").val(0);
            $(this).closest('td').find(".hide_details").css('display','block');
        });
    });

    function showFilters(){
        $('#filters').show();
    }

    function hideFilters(){
        $('#filters').hide();
    }

    $(document).ready(function(){
        <?php if($view_type !== ''){ ?>
        $('#filters').hide();
        <?php } ?>
    });
</script>
<script>
    $(':button .fullscreen').click(function() {
        $('.brand_wise').css({
            position:'absolute', //or fixed depending on needs
            top: $(window).scrollTop(), // top pos based on scoll pos
            left: 0,
            height: '100%',
            width: '100%'
        });
    });

    $('.fs-button').on('click', function(){
        var elem = document.getElementById('fullscreen');
        if(document.webkitFullscreenElement) {
            document.webkitCancelFullScreen();
        }
        else {
            elem.webkitRequestFullScreen();
        };
    });
</script>
<div id="open-modal2" class="modal-window">
    <div>
        <a href="#modal-close" title="Close" class="modal-close">close &times;</a>
        <h1>Set Price</h1>
        <div>
            <table border="0">
                <form action="variant-compare" method="post">
                    <input type="hidden" value="<?php echo  implode(',', $selected_category);?>"  name="category">
                    <input type="hidden" value="<?php echo  $selected_variant;?>"  name="variants">
                    <input type="hidden" value="<?php echo  $cust_id;?>"  name="customer_search">
                    <input type="hidden" id="model_report_type" value="<?php echo $report_type;?>"  name="report_type">
                    <input type="hidden" id="model_view_type" value="<?php echo $view_type;?>"  name="view_type">
                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                    <tr>
                        <td>Add To Variant Price</td>
                        <td colspan="3"><input type="text" name="price" class="form-control"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="submit" name="plus_price" class="form-control btn myblue"></td>
                    </tr>
                </form>
            </table>
        </div>
    </div>
</div>
<div id="open-modalcomp" class="modal-window">
    <div>
        <a href="#modal-close" title="Close" class="modal-close">close &times;</a>
        <h1>Select Company</h1>
        <div>
            <table border="0">
                <form id="company_form" action="pdf-preview" method="post">
                    <input type="hidden" value="<?php echo  implode(',', $selected_category);?>"  name="category">
                    <input type="hidden" value="<?php echo  $selected_variant;?>"  name="variants">
                    <input type="hidden" value="<?php echo  $cust_id;?>"  name="customer_search">
                    <input type="hidden" value="<?php echo  $company_id;?>"  name="company">
                    <input type="hidden" value="<?php echo  $cust_type;?>"  name="cust_type">
                    <input type="hidden" value="<?php echo  $mhid;?>"  name="hub">
                    <input type="hidden" id="model_report_type" value="<?php echo $report_type;?>"  name="report_type">
                    <input type="hidden" id="model_view_type" value="<?php echo $view_type;?>"  name="view_type">
                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                    <tr>
                        <td>Select company</td>
                        <td colspan="3"><select class="browser-default company" name="company" id="company" tabindex="-1">
                                <option value="" disabled selected>Please Select Company</option>
                                <?php
                                foreach ($company as $comp) {
                                $custid = $comp->id;
                                ?>
                                <option <?php if ($cust_id == $custid ) { echo 'selected'; } else { echo ''; } ?>
                                        value="<?= $custid; ?>"><?= $comp->name ?></option>
                                <?php
                                }
                                ?>
                            </select></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><!--input type="submit" name="company_form" class="form-control btn myblue"--></td>
                    </tr>
                </form>

            </table>

        </div>
    </div>
</div>
<div id="open-modalcompprice" class="modal-window">
    <div>
        <a href="#modal-close" title="Close" class="modal-close">close &times;</a>
        <h1>Select Company</h1>
        <div>
            <table border="0">
                <form id="company_form" action="pdf-preview" method="post">
                    <input type="hidden" value="<?php echo  implode(',', $selected_category);?>"  name="category">
                    <input type="hidden" value="<?php echo  $selected_variant;?>"  name="variants">
                    <input type="hidden" value="<?php echo  $cust_id;?>"  name="customer_search">
                    <input type="hidden" value="<?php echo  $company_id;?>"  name="company">
                    <input type="hidden" value="<?php echo  $cust_type;?>"  name="cust_type">
                    <input type="hidden" value="<?php echo  $mhid;?>"  name="hub">
                    <input type="hidden" id="model_report_type" value="<?php echo $report_type;?>"  name="report_type">
                    <input type="hidden" id="model_view_type" value="<?php echo $view_type;?>"  name="view_type">
                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                    <tr>
                        <td>Select company</td>
                        <td colspan="3"><select class="browser-default companyp" name="company" id="companyp" tabindex="-1">
                                <option value="" disabled selected>Please Select Company</option>
                                <?php
                                foreach ($company as $comp) {
                                $custid = $comp->id;
                                ?>
                                <option <?php if ($cust_id == $custid ) { echo 'selected'; } else { echo ''; } ?>
                                        value="<?= $custid; ?>"><?= $comp->name ?></option>
                                <?php
                                }
                                ?>
                            </select></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><!--input type="submit" name="company_form" class="form-control btn myblue"--></td>
                    </tr>
                </form>

            </table>

        </div>
    </div>
</div>
<div id="open-modalshort" class="modal-window">
    <div>
        <a href="#modal-close" title="Close" class="modal-close">close &times;</a>
        <h1>Select Company</h1>
        <div>
            <table border="0">
                <form id="company_form" action="pdf-preview" method="post">
                    <input type="hidden" value="<?php echo implode(',', $selected_category);?>" name="category">
                    <input type="hidden" value="<?php echo $selected_variant;?>" name="variants">
                    <input type="hidden" value="<?php echo $cust_id;?>" name="customer_search">
                    <input type="hidden" value="<?php echo $company_id;?>" name="company">
                    <input type="hidden" value="<?php echo $cust_type;?>" name="cust_type">
                    <input type="hidden" value="<?php echo $mhid;?>" name="hub">
                    <input type="hidden" id="model_report_type" value="<?php echo $report_type;?>" name="report_type">
                    <input type="hidden" id="model_view_type" value="<?php echo $view_type;?>" name="view_type">
                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                    <tr>
                        <td>Select company</td>
                        <td colspan="3">
                            <select class="browser-default companyshort" name="company" id="companyshort" tabindex="-1">
                                <option value="" disabled selected>Please Select Company</option>
                                <?php
                                foreach ($company as $comp) {
                                $custid = $comp->id;
                                ?>
                                <option <?php if ($cust_id == $custid ){ echo 'selected'; } else { echo ''; } ?> value="<?= $custid; ?>"><?= $comp->name ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><!--input type="submit" name="company_form" class="form-control btn myblue"--></td>
                    </tr>
                </form>

            </table>

        </div>
    </div>
</div>
<div id="open-modal" class="modal-window">
    <div>
        <a href="#modal-close" title="Close" class="modal-close">close &times;</a>
        <h1>Quick Quotation</h1>
        <div>
            <?=csrf_field(); ?>
            <input type="hidden" value="<?php echo implode(',', $selected_category);?>"  name="category">
            <input type="hidden" value="<?php echo $selected_variant;?>"  name="variants">
            <input type="hidden" id="model_report_type" value="<?php echo $report_type;?>"  name="report_type">
            <input type="hidden" id="model_view_type" value="<?php echo $view_type;?>"  name="view_type">
            <table border="0">
                <tr>
                    <td>Customer</td>
                    <td colspan="2">
                        <select name="customer" class="browser-default">
                            <option value="">Please Select Customer</option>
                        </select>
                    </td>
                    <td><a href="#open-modal1" class="btn btn-primary myblue">Add Customer</a> </td>
                </tr>
                <tr>
                    <td>Customer Email</td>
                    <td colspan="3"><input type="text" name="email" class="form-control"></td>
                </tr>
                <tr>
                    <td>Customer Mobile</td>
                    <td colspan="3"><input type="text" name="mobile" class="form-control"></td>
                </tr>
                <tr>
                    <td>
                        <form method="post" action="pdf-preview">
                            <?=csrf_field(); ?>
                            <input type="hidden" value="<?php echo implode(',', $selected_category);?>"  name="category">
                            <input type="hidden" value="<?php echo $selected_variant;?>"  name="variants">
                            <input type="hidden" id="model_report_type" value="<?php echo $report_type;?>"  name="report_type">
                            <input type="hidden" id="model_view_type" value="<?php echo $view_type;?>"  name="view_type">
                            <button type="submit" class="btn btn-primary myblue" name="pdf">PDF</button>
                        </form>
                    </td>
                    <td><button type="button" class="btn btn-primary myblue">Email</button></td>
                    <td><button type="button" class="btn btn-primary myblue">Message</button></td>
                    <td><button type="button" class="btn btn-primary myblue">Go</button></td>
                </tr>
            </table>

        </div>
    </div>
</div>
<div id="open-modal1" class="modal-window">
    <div>
        <a href="#modal-close1" title="Close" class="modal-close1">close &times;</a>
        <h1>Quick Quotation</h1>
        <div>
            <form method="post" action="customers" class="col s12">
                <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                <div class="row">
                    <div class="input-field col s6">
                        <i class="material-icons prefix">account_circle</i>
                        <input id="Full_Name" type="text" class="validate" name="name">
                        <label for="Full_Name">Full Name</label>
                    </div>
                    <div class="input-field col s6">
                        <i class="material-icons prefix">text_fields</i>
                        <input id="Proprieter" type="tel" class="validate" name="proprieter_name">
                        <label for="Proprieter">Proprieter Name</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <i class="material-icons prefix">email</i>
                        <input id="Email" type="text" class="validate" name="email">
                        <label for="Email">Email</label>
                    </div>
                    <div class="input-field col s6">
                        <i class="material-icons prefix">phone</i>
                        <input id="Contact" type="tel" class="validate" name="contact_no">
                        <label for="Contact">Contact No.</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <i class="material-icons prefix">verified_user</i>
                        <input id="CID" type="text" class="validate" name="cid">
                        <label for="CID">CID</label>
                    </div>
                    <div class="input-field col s6">
                        <i class="material-icons prefix">view_headline</i>
                        <select name="customer_category"> customer_category
                            <?php
                            foreach ($customer_category as $category){
                            $s_id = $category->id;
                            ?>
                            <option  value="<?=$s_id;?>"><?=$category->type?></option>
                            <?php
                            }
                            ?>
                        </select>
                        <label for="customer_category">Category </label>

                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <i class="material-icons prefix">view_quilt</i>
                        <select name="type">
                            <?php
                            foreach ($customer_type as $type){
                            $s_id = $type->id;
                            ?>
                            <option  value="<?=$s_id;?>"><?=$type->type?></option>
                            <?php
                            }
                            ?>
                        </select>
                        <label for="type">Type </label>


                    </div>
                    <div class="input-field col s6">
                        <i class="material-icons prefix">add_location</i>
                        <input id="Address" type="tel" class="validate" name="postal_address">
                        <label for="Address">Postal Address</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <i class="material-icons prefix">add_location</i>
                        <select  class="state_list " name="state"  style="width: 100%" required>
                            <?php
                            foreach ($states as $state){
                            $s_id = $state->id;
                            ?>
                            <option  <?php if(1 !=1) {
                                echo "selected";
                            } else {
                                echo '';
                            }?> value="<?=$s_id;?>"><?=$state->name?></option>
                            <?php
                            }
                            ?>
                        </select>
                        <label for="State">State</label>
                    </div>
                    <div class="input-field col s6" >

                        <select  class="browser-default district dist_list" name="district" style="width: 100%" required>
                            <option selected value="">District</option>
                        </select>

                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <i class="material-icons prefix">security</i>
                        <select name="msg_active">
                            <option value="0"> Yes</option>
                            <option value="1"> No</option>
                        </select>
                        <label for="msg_active">Message Active</label>
                    </div>
                    <div class="input-field col s6">
                        <i class="material-icons prefix">security</i>
                        <select name="msg_class">
                            <?php
                            foreach ($customer_class as $class){
                            $s_id = $class->id;
                            ?>
                            <option  value="<?=$s_id;?>"><?=$class->type?></option>
                            <?php
                            }
                            ?>
                        </select>
                        <label for="msg_class">Customer Class</label>
                    </div>


                </div>
                <button class="btn myblue waves-light right" type="submit" name="add" style="margin-top: -13px">Add
                    <i class="material-icons right">save</i>
                </button>

            </form>

        </div>
    </div>
</div>
<?php echo $footer ?>
</body>
</html>
