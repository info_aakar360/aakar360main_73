<style>
    a{ text-decoration: none;  }
    a:hover{text-decoration: none}
    p {  margin: 0 0 0px;  }
</style>
<style>
    .modal_p .modal_p_body,.modal_p1 .modal_p1_body{
        max-width: 1000px;
        margin: 30px auto;
        padding: 10px;
        box-shadow: 0 3px 9px rgb(0 0 0 / 50%);
        border-radius: 5px;
        background: #fff;
        position: relative;
        overflow: hidden;
        height: 65%;
        background-color: lightgrey;
    }
    .modal_p .modal_p_body button:focus,.modal_p1 .modal_p1_body button:focus{
        background-color: white; !important;
    }
    .modal_p,.modal_p1{
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }
    .column {
        float: left;
        width: 60%;
        padding: 0px;
        height: 119px;
    }.column1 {
         float: left;
         width: 40%;
         padding: 0px;
         height: 119px;
     }
    .imagefit{
        width: 100%;
        height: 247px;
        object-fit: cover;
        margin-left: -11px;
        margin-top: -58px;
    }
    /* Clear floats after the columns */
    .row:after {
        content: "";
        display: table;
        clear: both;
    }
</style>
<body onload="initMap()" class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu 2-columns  " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

<!-- BEGIN: Header-->
<header class="page-topbar" id="header">
    <!-- <div class="navbar navbar-fixed">
         <nav class="navbar-main navbar-color nav-collapsible sideNav-lock navbar-dark gradient-45deg-indigo-purple no-shadow">
             <div class="nav-wrapper">
                 <div class="header-search-wrapper hide-on-med-and-down"><i class="material-icons">search</i>
                     <input class="header-search-input z-depth-2" type="text" name="Search" placeholder=" Type here ..">
                 </div>
                <ul class="navbar-list right">

                    <li class="hide-on-med-and-down"><a class="waves-effect waves-block waves-light toggle-fullscreen" href="javascript:void(0);"><i class="material-icons">settings_overscan</i></a></li>
                    <li class="hide-on-large-only"><a class="waves-effect waves-block waves-light search-button" href="javascript:void(0);"><i class="material-icons">search</i></a></li>
                    <li><a class="waves-effect waves-block waves-light notification-button" href="javascript:void(0);" data-target="notifications-dropdown"><i class="material-icons">notifications_none<small class="notification-badge">5</small></i></a></li>
                    <li><a class="waves-effect waves-block waves-light profile-button" href="javascript:void(0);" data-target="profile-dropdown"><span class="avatar-status avatar-online"><img src="<?=$tp; ?>/images/avatar/avatar-7.png" alt="avatar"><i></i></span></a></li>
                    <li><a class="waves-effect waves-block waves-light sidenav-trigger" href="#" data-target="slide-out-right"><i class="material-icons">format_indent_increase</i></a></li>
                </ul>
                <!-- translation-button-->

                <!-- notifications-dropdown-->
                <!--<ul class="dropdown-content" id="notifications-dropdown">
                <li>
                    <h6>NOTIFICATIONS<span class="new badge">5</span></h6>
                </li>
                <li class="divider"></li>
                <li><a class="grey-text text-darken-2" href="#!"><span class="material-icons icon-bg-circle cyan small">add_shopping_cart</span> A new order has been placed!</a>
                    <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
                </li>
                <li><a class="grey-text text-darken-2" href="#!"><span class="material-icons icon-bg-circle red small">stars</span> Completed the task</a>
                    <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>
                </li>
                <li><a class="grey-text text-darken-2" href="#!"><span class="material-icons icon-bg-circle teal small">settings</span> Settings updated</a>
                    <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>
                </li>
                <li><a class="grey-text text-darken-2" href="#!"><span class="material-icons icon-bg-circle deep-orange small">today</span> Director meeting started</a>
                    <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time>
                </li>
                <li><a class="grey-text text-darken-2" href="#!"><span class="material-icons icon-bg-circle amber small">trending_up</span> Generate monthly report</a>
                    <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time>
                </li>
            </ul>-->
                <!-- profile-dropdown-->
                <!--<ul class="dropdown-content" id="profile-dropdown">
                <li><a class="grey-text text-darken-1" href="user-profile-page.html"><i class="material-icons">person_outline</i> Profile</a></li>
                <li><a class="grey-text text-darken-1" href="app-chat.html"><i class="material-icons">chat_bubble_outline</i> Chat</a></li>
                <li><a class="grey-text text-darken-1" href="page-faq.html"><i class="material-icons">help_outline</i> Help</a></li>
                <li class="divider"></li>
                <li><a class="grey-text text-darken-1" href="user-lock-screen.html"><i class="material-icons">lock_outline</i> Lock</a></li>
                <li><a class="grey-text text-darken-1" href="user-login.html"><i class="material-icons">keyboard_tab</i> Logout</a></li>
            </ul>
            </div>
        </nav>
    </div>-->
</header>
<!-- END: Header-->

<!-- BEGIN: SideNav-->
<!--<aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-light sidenav-active-square">-->
<aside class="sidenav-main nav-expanded nav-collapsed nav-collapsible sidenav-light sidenav-active-square" >
    <div class="brand-sidebar">
        <h1 class="logo-wrapper">
            <a class="brand-logo darken-1" href="<?=url('crm/index');?>">
                <img src="<?=$tp; ?>/images/logo/aakar360.png" alt="aakar360 logo"/>
                <span class="logo-text hide-on-med-and-down"></span>
            </a>
       {{--   <a class="navbar-toggler" href="#"><i class="material-icons">radio_button_checked</i></a>--}}
        </h1>
        <button type="button" class="btn btn-primary right" data-toggle="modal" data-target="#crmModal" onclick="showSwitchTab()" style="padding: 3px;margin-top: 12px;margin-right: 17px">
            Swtich To Crm
        </button>
        <span class="logo-text hide-on-med-and-down right" style="padding: 3px;margin-top: 12px;margin-right: 15px">@if(isset($_COOKIE['crmType']) && $_COOKIE['crmType'] == '1') Business @elseif(isset($_COOKIE['crmType']) && $_COOKIE['crmType'] == '2') Retail @else Software @endif </span>
    </div>
    <ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="menu-accordion">
        <li class=" bold">
            <a class="waves-effect waves-cyan " href="<?=url('crm/logout');?>">
                <i class="material-icons md-24">power_settings_new</i><span class="menu-title" data-i18n="">Logout</span>
            </a>
            <p class="navigation-header-sub-text">Logout</p>
        </li>
        <li class="active bold">
            <!--Dashboard-->
            <a class=" waves-effect waves-cyan " href="<?=url('crm/index');?>"><i class="material-icons md-24">home</i>
                <span class="menu-title" data-i18n="">Dashboard
            </a>
            <p class="navigation-header-sub-text">Dashboard</p>

        </li>
        <li class="bold">

            <a class="collapsible-header waves-effect waves-cyan " href="#">
                <!--<p style="font-size: 14px; line-height: 14px;">Campaign</p>-->
                <i class="material-icons md-24">notifications_none</i>
                <span class="menu-title" data-i18n="">Campaign</span>
            </a>
            <p class="navigation-header-sub-text">Campaign</p>

            <div class="collapsible-body">
                <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                    <li>
                        <a class="waves-effect waves-cyan " href="<?=url('crm/sms');?>">
                            <i class="material-icons md-18">mail</i>
                            <span class="menu-title" data-i18n="">SMS</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>

        <li class="bold">
            <a class="waves-effect waves-cyan " href="<?=url('crm/variant-compare');?>">
                <i class="material-icons md-24">book</i>
                <span class="menu-title" data-i18n="">Quotation</span></a>
                <p class="navigation-header-sub-text">Quotation</p>
        </li>

        <li class="bold">
            <a class="waves-effect waves-cyan " href="<?=url('crm/product-price-list');?>">
                <i class="material-icons md-24">assessment</i>
                <span class="menu-title" data-i18n="">Price Entry</span>
            </a>
            <p class="navigation-header-sub-text">Price Entry</p>
        </li>

        <li class="bold"><a class="collapsible-header waves-effect waves-cyan " href="#"><i class="material-icons md-24">group_add</i>
                <span class="menu-title" data-i18n="">Marketing</span></a>
            <p class="navigation-header-sub-text">Marketing</p>
            <div class="collapsible-body">
                <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                    <li>
                        <a class="waves-effect waves-cyan " href="<?=url('crm/marketing-data');?>">
                            <i class="material-icons md-18">pin_drop</i>
                            <span class="menu-title" data-i18n="">Area Data</span>
                        </a>
                    </li>
                    <li>
                        <a class="collapsible-body" href="<?=url('crm/customer-data');?>" data-i18n=""><i class="material-icons md-18">portrait</i><span>Customer Data</span></a>
                    </li>
                    <li>
                        <a class="collapsible-body" href="<?=url('crm/converted-customers');?>" data-i18n=""><i class="material-icons md-18">verified_user</i><span>Converted Customers</span></a>
                    </li>
                    <li>
                        <a class="collapsible-body" href="<?=url('crm/visit-report');?>" data-i18n=""><i class="material-icons md-18">transfer_within_a_station</i><span>Visit/Call Report</span></a>
                    </li>
                    <li>
                        <a class="collapsible-body" href="<?=url('crm/unqualified-report');?>" data-i18n=""><i class="material-icons md-18">transfer_within_a_station</i><span>Unqualified/N-Interested</span></a>
                    </li>
                    <li>
                        <a class="collapsible-body" href="<?=url('crm/cv-frequency');?>" data-i18n=""><i class="material-icons md-18">transfer_within_a_station</i><span>Call/Visit Frequency</span></a>
                    </li>
                    <li>
                        <a class="collapsible-body" href="<?=url('crm/unreachable-report');?>" data-i18n=""><i class="material-icons md-18">transfer_within_a_station</i><span>Unreachable Report</span></a>
                    </li>
                    <li>
                        <a class="collapsible-body" href="<?=url('crm/lead-order-report');?>" data-i18n=""><i class="material-icons md-18">transfer_within_a_station</i><span>Lead/Order Report</span></a>
                    </li>
                    <li>
                        <a class="collapsible-body" href="<?=url('crm/transportor-report');?>" data-i18n=""><i class="material-icons md-18">transfer_within_a_station</i><span>Transpotor Report</span></a>
                    </li>
                    <li>
                        <a class="collapsible-body" href="<?=url('crm/crm-ticket');?>" data-i18n=""><i class="material-icons md-18">report</i><span>Ticket Report</span></a>
                    </li>
                    <li>
                        <a class="collapsible-body" href="<?=url('crm/score-card');?>" data-i18n=""><i class="material-icons md-18">report</i><span>Score Card</span></a>
                    </li><li>
                        <a class="collapsible-body" href="<?=url('crm/calender-report');?>" data-i18n=""><i class="material-icons md-18">report</i><span>Calender Report</span></a>
                    </li>
                    <li>
                        <a class="collapsible-body" href="<?=url('crm/brand-penetration-report/district-wise');?>" data-i18n=""><i class="material-icons md-18">local_mall</i><span>Brand Penetration</span></a>
                    </li>
                    <li>
                        <a class="collapsible-body" href="<?=url('crm/supplier-competition-report/district-wise');?>" data-i18n=""><i class="material-icons md-18">insert_chart</i><span>Supplier Competition</span></a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="bold"><a class="collapsible-header waves-effect waves-cyan " href="#"><i class="material-icons md-24">device_hub</i><span class="menu-title" data-i18n="">Masters</span></a>
            <p class="navigation-header-sub-text">Masters</p>

            <div class="collapsible-body">
                <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                    <li><a class="collapsible-body" href="<?=url('crm/customers');?>" data-i18n=""><i class="material-icons md-18">supervisor_account</i><span>Customer Master</span></a>
                    </li>
                    <li><a class="collapsible-body" href="<?=url('crm/suppliers');?>" data-i18n=""><i class="material-icons md-18">transfer_within_a_station</i><span>Supplier Master</span></a>
                    </li>
                    <li><a class="collapsible-body" href="<?=url('crm/user-manager');?>" data-i18n=""><i class="material-icons md-18">perm_identity</i><span>User Master</span></a>
                    </li>
                    <li><a class="collapsible-body" href="<?=url('crm/designation-master');?>" data-i18n=""><i class="material-icons md-18">perm_identity</i><span>Designation Master</span></a>
                    </li>
                    <li><a class="collapsible-body" href="<?=url('crm/contact-designation');?>" data-i18n=""><i class="material-icons md-18">perm_identity</i><span>Contact Designation</span></a>
                    </li>
                    <li>
                        <a class="collapsible-body" href="<?=url('crm/crm-quick-entry-report');?>" data-i18n=""><i class="material-icons md-18">report</i><span>Quick Entry Report</span></a>
                    </li>
                    <li><a class="collapsible-body" href="<?=url('crm/user-role');?>" data-i18n=""><i class="material-icons md-18">wc</i><span>Role Master</span></a>
                    </li>
                    <li><a class="collapsible-body" href="<?=url('crm/district-master');?>" data-i18n=""><i class="material-icons md-18">merge_type</i><span>District Master</span></a>
                    </li>
                    <li><a class="collapsible-body" href="<?=url('crm/no-order-reason');?>" data-i18n=""><i class="material-icons md-18">merge_type</i><span>No Order Reason</span></a>
                    </li>
                    <li>
                        <a class="collapsible-body" href="<?=url('crm/sales-pipeline');?>" data-i18n=""><i class="material-icons md-18">report</i><span>Sales Pipeline</span></a>
                    </li>
                    <li>
                        <a class="collapsible-body" href="{{route('truckload.index')}}" data-i18n=""><i class="material-icons md-18">report</i><span>Truck Load</span></a>
                    </li>
                    <li>
                        <a class="collapsible-body" href="<?=url('crm/referrermaster');?>" data-i18n=""><i class="material-icons md-18">report</i><span>Referrer Master</span></a>
                    </li>
                    <li><a class="collapsible-body" href="<?=url('crm/block-master');?>" data-i18n=""><i class="material-icons md-18">list</i><span>Block Master</span></a>
                    </li>
                    <li><a class="collapsible-body" href="<?=url('crm/locality-master');?>" data-i18n=""><i class="material-icons md-18">location_on</i><span>Area/Locality Master</span></a>
                    </li>
                    <li><a class="collapsible-body" href="<?=url('crm/category-master');?>" data-i18n=""><i class="material-icons md-18">merge_type</i><span>Category Master</span></a>
                    <li><a class="collapsible-body" href="<?=url('crm/transportor-master');?>" data-i18n=""><i class="material-icons md-18">equalizer</i><span>Transpotor Master</span></a>
                    </li>
                    <li><a class="collapsible-body" href="<?=url('crm/price-groups-master');?>" data-i18n=""><i class="material-icons md-18">attach_money</i><span>Price Groups Master</span></a>
                    </li>
                    <li><a class="collapsible-body" href="<?=url('crm/project-subtype-master');?>" data-i18n=""><i class="material-icons md-18">equalizer</i><span>Project Sub Type</span></a>
                    </li>
                    <li><a class="collapsible-body" href="<?=url('crm/association-master');?>" data-i18n=""><i class="material-icons md-18">wc</i><span>Association</span></a>
                    </li>
                    <li><a class="collapsible-body" href="<?=url('crm/company');?>" data-i18n=""><i class="material-icons md-18">equalizer</i><span>Company</span></a>
                    </li>

                </ul>
            </div>
        </li>
    </ul>
    <div class="navigation-background"></div><a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
</aside>
<!-- END: SideNav-->
<div id="main" class ="main-full" >