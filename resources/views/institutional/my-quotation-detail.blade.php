<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 9/14/2019
 * Time: 4:57 PM
 */
 echo $header?>
 <div class="content-wrapper">

  <div class="row">
   <div class="col-md-12 grid-margin">
    <div class="d-flex justify-content-between flex-wrap">
     <div class="d-flex align-items-end flex-wrap">
      <div class="mr-md-3 mr-xl-5">
       <h2>My Quotations Detail</h2>
       <!--p class="mb-mad-0">Your analytics dashboard template.</p-->
       <div class="d-flex">
        <i class="mdi mdi-home text-muted hover-cursor"></i>
        <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Products&nbsp;/&nbsp;</p>
           <p class="text-primary mb-0 hover-cursor"><a href="{{ route('business.institutionalMyBookings') }}">My Quotations Detail</a></p>
       </div>
      </div>
     </div>
    </div>
   </div>
  </div>

     <div class="container">
         <div class="content product-page pull-left" style="width: 100%;background: white;box-shadow: 0 3px 6px rgb(0 0 0 / 16%)">
             <?php if($_GET['method'] == "bysize"){  ?>
                 <div class="row" style="padding:15px 15px">
                     <div class="col-md-12">
                         <!--Rfq Booking-->
                         <?php if($ret[0]['status'] == "Pending"){  ?>
                         <div class="alert alert-danger" role="alert">
                             Pending for Update!
                         </div>
                         <?php } ?>
                         <h6><?php echo $ret[0]['product_name'];?></h6>

                         <div class="table-responsive">
                             <table class="table table-striped table-bordered">
                                 <thead>
                                 <tr class="bg-blue">
                                     <th>id</th>
                                     <th>Product Name</th>
                                     <th>Price</th>
                                     <th>Unit</th>
                                     <th style="min-width: 126px;">Qty</th>
                                     <th class="text-right">Total Price</th>
                                 </tr>
                                 </thead>
                                 <tbody>
                                 <form id="variants-form">
                                     <input type="hidden" name="type" value="bysize">

                                     <?php
                                     $col = 5; $totalquantity = 0;$gtprice = 0;$titem = 0; foreach ($ret[0]['variant'] as $key=>$vart) {
                                         $product = \App\Products::where('id',$ret[0]['product_id'])->first();
                                         ?>
                                         <!-- <input type="hidden" name="title[]" value="<?php /*echo $variant['title'];*/?>">-->
                                         <input type="hidden" class="prlist" id="prlist<?php echo $vart['id'] ?>" name="prlist[]" value="<?php echo $vart['id'];?>">
                                         <input type="hidden"  class="rfqprlist" name="rfqprlist[]" value="<?php echo $vart['id'];?>">
                                         <input type="hidden"  class="rfqprlist" name="rfqunit[]" value="<?php echo $vart['unit'];?>">
                                         <input type="hidden" id="min-qty" value="<?=$product->institutional_min_quantity;?>"/>
                                         <tr>
                                             <th scope="row"><?php echo $key+1;?></th>
                                             <td><?php echo $vart['title'];?></td>
                                             <td class="price mainprice<?php echo $vart['id']; ?>" id="price<?php echo $vart['id']; ?>"><?php echo $vart['total_price'];?>
                                                 <input type="hidden" class="mainprice<?php echo $vart['id']; ?>" id="mainprice<?php echo $vart['id']; ?>" value="<?php echo $vart['total_price'];?>"></td>
                                             <td>
                                                 <input type="hidden" name="unit[]" readonly id="unit<?php echo $vart['id']; ?>" value="<?php echo $vart['unit'];?>"/><?php echo $vart['unit'];?>
                                             </td>
                                             <td style="min-width:126px;">
                                                 <div class=" buttons_added_'<?php echo  $key ?>' vaributton">
                                                     <div class="form-group baseQuantity but qtycss">
                                                         <div class="dec reasevar btn btn-sm btn-primary" onclick="decrement('<?php echo $vart['id'] ?>')">-</div>
                                                         <input name="quantity[]" class="quantityvar pridvalue<?php echo  $vart['id']; ?>" onchange="getTotal(this.value,<?php echo $key; ?>,<?php echo $vart['id']; ?>);" id="quantityvar'.<?php echo  $vart['id']; ?>.'" data-product="" value="<?php echo  $vart['quantity']; ?>" >
                                                         <input type="hidden" name="id" class="quantityvarid" size="2" value="<?php echo  $vart['id']; ?>" >
                                                         <div class="inc reasevar btn btn-sm btn-primary" onclick="increment('<?php echo $vart['id'] ?>');">+</div>
                                                     </div>
                                                 </div>
                                                 <!--button type="button" style="display: none" name="add-to-cart" class="single_add_to_cart_button_'.$key.' button alt" style="display: block !important">Add+</button-->
                                             </td>
                                             <td class="text-right">
                                                 <b class="tp" id="tp-<?php echo $key ?>" data-price="0"><span class="singlepr<?php echo  $vart['id']; ?> singleprprice"><?php echo $vart['total_price'];?></span></b>
                                             </td>
                                         </tr>

                                     <?php $totalquantity +=  $vart['quantity']; $gtprice += $vart['total_price']; $titem = $key+1; } ?>
                                     <tr>
                                         <td colspan="<?php echo $col; ?>" class="text-right">Total Item:</td>
                                         <td class="text-right"><b id="items"><?php echo $titem; ?></b></td>
                                     </tr>
                                     <!--tr>
                                         <td colspan="<?php echo $col; ?>" class="text-right">Total Quantity:</td>
                                         <td class="text-right"><b id="quantity" style="color: green;"><?php //echo  $totalquantity; ?></b></td>
                                     </tr-->
                                     <?php if($ret[0]['payment_option'] !== 'Select Payment Option') { ?>
                                     <tr>
                                         <td colspan="<?php echo $col; ?>" class="text-right">Payment Option:</td>
                                         <td class="text-right"><b id="option" style="color: green;"><?php echo $ret[0]['payment_option']; ?></b></td>
                                     </tr>
                                     <?php } ?>
                                     <tr>
                                         <td colspan="<?php echo $col; ?>" class="text-right">Location:</td>
                                         <td class="text-right"><b id="location" style="color: green;"><?php echo $ret[0]['location']; ?></b></td>
                                     </tr>
                                     <tr>
                                         <td colspan="<?php echo $col; ?>" class="text-right">Grand Total Price:</td>
                                         <td class="text-right"><b id="gt"><?php echo $gtprice; ?></b></td>
                                     </tr>
                                 </form>
                                 </tbody>
                             </table>
                             <div class="order">
                                 <div class="col-md-8">
                                     <div id="error" style="display:none;"></div>
                                 </div>
                                 <div class="col-md-4">
                                     <?php if($ret[0]['status'] !== "Pending"){ ?>
                                         <button style="width: 100%;"  name="bysize_quote" class="bsbook-now-button btn btn-primary pull-right" data-id="<?php if(isset($_GET['product_id'])){echo $_GET['product_id']; }?>"> <?=translate('Book Now')?></button>
                                     <?php } ?>
                                 </div>
                             </div>
                         </div>
                         <!--End Rfq Booking-->
                     </div>
                 </div>

             <?php } else {  ?>
             <div class="row" style="padding:15px 15px">
                 <div class="col-md-12">
                     <!--Rfq Booking-->
                     <?php if($ret[0]['status'] == "Pending"){ ?>
                     <div class="alert alert-danger" role="alert">
                         Pending for Update!
                     </div>
                     <?php } ?>
                     <h6><?php echo $ret[0]['product_name'];?></h6>
                     <hr>
                     <?php if($ret[0]['status'] == "Pending"){ ?>
                         <div class="row">
                             <div class="col-md-12">
                                 <div class="col-md-4">
                                     <label>Basic Rate : <span style="color: red">Waiting For Update</span></label>
                                 </div>
                                 <div class="col-md-4">
                                    <input type="text" name="pquantity" value="<?php echo $ret[0]['quantity']?>" disabled>
                                 </div>
                                 <div class="col-md-4">
                                    <select name="pqty" class="form-control" disabled>
                                        <option value=""><?php echo $ret[0]['unit'][0]['symbol'] ?></option>
                                    </select>
                                 </div>
                             </div>
                         </div>
                         <br>
                         <div class="row">
                             <div class="col-md-12">
                                 <div class="col-md-6">
                                     <label>Payment Option:</label><span style="float: right"><?php echo $ret[0]['payment_option'];?></span>
                                 </div>
                                 <div class="col-md-6">
                                     <label>Location:</label><span style="float: right"><?php echo $ret[0]['location'];?></span>
                                 </div>
                             </div>
                         </div>
                         <hr>

                     <?php }  ?>
                     <?php if($ret[0]['status'] == "Approved"){ ?>
                     <form method="post" id="directForm">
                         {{ csrf_field() }}
                     <input type="hidden" name="type" id="methodType" value="bybasic">
                     <input type="hidden" id="min-qty" value="<?=$product->institutional_min_quantity;?>"/>
                     <input type="hidden" name="price" id="displayPrice" value="<?php echo $ret[0]['bprice']; ?>">
                     <input type="hidden" name="unit" id="weightUnit" value="<?php echo $ret[0]['unit'][0]['symbol'];?>">
                     <input type="hidden" name="unit" id="without_variants" value="1">
                     <input type="hidden" name="hubid" id="hubId" value="<?php echo $ret[0]['hub_id']; ?>">

                     <input type="hidden" id="page-url" value="<?php echo route("business.institutionalRfqBooking"); ?>"/>
                     <input type="hidden" id="_token" value="<?php echo csrf_token(); ?>"/>

                     <h6><?php echo $ret[0]['product_name']; ?></h6>
                     <hr>
                     <div class="row">
                     <div class="col-md-12">
                     <div class="col-md-4">
                     <label>Basic Rate : <?php echo $ret[0]['bprice']; ?></label>
                         </div>

                         <div class="col-md-4">
                             <input type="text" name="pquantity" class="quantityvar pridvalue<?php echo $ret[0]['product_id']; ?>" data-product="<?php echo $ret[0]['product_id']; ?>" value="<?php echo $ret[0]['quantity']; ?>">
                         </div>
                         <div class="col-md-4">
                             <select name="pqty" class="form-control" disabled>
                                 <option value=""><?php echo $ret[0]['unit'][0]['symbol']; ?></option>
                             </select>
                         </div>
                         </div>
                         </div>
                         <br>
                         <div class="row">
                             <div class="col-md-12">
                                 <div class="col-md-6">
                                     <label>Payment Option:</label><span style="float: right"><?php echo $ret[0]['payment_option'];?></span>
                                 </div>
                                 <div class="col-md-6">
                                     <label>Location:</label><span style="float: right"><?php echo $ret[0]['location'];?></span>
                                 </div>
                             </div>
                         </div>
                         <hr>
                         <div class="row">
                             <div class="col-md-12">
                                 <div class="col-md-8">
                                     <div id="error" style="display:none;"></div>
                                 </div>
                                 <center>
                                     <button type="button" name="base_quote"  value="Book Now" data-id="<?php if(isset($_GET['product_id'])){ echo $_GET['product_id']; }; ?>" class="btn btn-primary btn-sm book-basic-button">Book Now</button>
                                 </center>
                             </div>
                         </div>

                         </form>
                     <?php }  ?>

                 </div>
             </div>
             <?php } ?>
         </div>
     </div>
 </div>
<?php echo $footer?>
<script>
    function decrement(prid) {
        var el = $(".pridvalue"+prid);
        var saleprice = $(".mainprice"+prid).text();
        var totpr = 0;
        var value = el.val();
        var min = el.attr('min') || false;
        var max = el.attr('max') || false;
        value--;
        if(!min || value >= min) {
            el.val(value);
            totpr = saleprice*value;
            $(".singlepr"+prid).text("0.00");
            $(".singlepr"+prid).text(totpr);
            caltotal();
        }
    }

    function increment(prid) {
        var el = $(".pridvalue"+prid);
        var saleprice = $(".mainprice"+prid).text();
        var totpr = 0;
        var min = el.attr('min') || false;
        var max = el.attr('max') || false;
        var value = el.val();
        value++;
        if(!max || value <= max) {
            el.val(value);
            totpr = saleprice*value;
            $(".singlepr"+prid).text("0.00");
            $(".singlepr"+prid).text(totpr);
            caltotal();
        }
    }

    function getTotal(val,index,varid){
        var qty = Number(val);
        var saleprice = Number($('#price'+index).text());
        var hdata = '<input type="hidden" class="tpv" value="'+qty*saleprice+'"><span class="singlepr'+varid+' price">'+qty*saleprice+'</span>';
        $("#tp-"+index).html(hdata);
        var variants = 0.00;
        var mtotal = 0.00;
        $(".tpv").each(function () {
            variants += Number($(this).val());
        });

        $('#gt').html(variants);
        caltotal();
    }

    function  caltotal() {
        var variants = 0.00;
        $(".singleprprice").each(function () {
            variants += parseInt($(this).text());
        });
        $('#gt').html("0.00");
        $('#gt').html(variants);

        var mainvariants = 0.00;
        $(".prlist").each(function () {
            var prid = $(this).val();
            var el = $(".pridvalue"+prid).val();
            var singpr = $(".mainprice"+prid).text();
            mainvariants += parseInt(singpr*el);
        });
        $('#mp').html("0.00");
        $('#mp').html(mainvariants);
        $('#savings').html(mainvariants-variants);
    }

    $("body").on('click','.book-basic-button',function() {

        var id = $(this).data('id');
        var min = $('#min-qty').val();
        var max = $('#max-qty').val();
        var quantity = $(".quantityvar").val();
        var hub_id = $('#hubId').val();
        var urlLink = $('#page-url').val();
        var token = $('#_token').val();
        var displayPrice = $('#displayPrice').val();
        var weightUnit = $('#weightUnit').val();
        var without_variants = $('#without_variants').val();
        var price = [];
        var unit = [];
        var variants = '';
        var credit_id = $("input[type='radio'][name='credit']:checked").val();
        if(!quantity){
            quantity = 0;
            variants = $("#directForm").serialize();
        }
        var type = $("#methodType").val();
        var options = $(".options").serialize();
        $(".book-basic-button").html('<div class="loading"></div>');
        $.ajax({
            url: urlLink,
            type: 'post',
            data: 'id='+id+'&q='+quantity+'&op='+options+'&variants='+variants+'&min='+min+'&price='+price+'&units='+unit+'&max='+max+'&_token='+token+'&hub_id='+hub_id+'&type='+type+'&displayPrice='+displayPrice+'&weightUnit='+weightUnit+'&without_variants='+without_variants+'&credit_id='+credit_id,
            crossDomain: true,
        }).done(function(response) {
            if (response == 'invalid'){
                $("#error").attr('class', 'alert alert-danger');
                $("#error").html('Invalid quantity!');
                $("#error").show();
            }else if (response == 'minimum'){
                $("#error").attr('class', 'alert alert-danger');
                $("#error").html('Minimum order quantity is '+ min + ' unit!');
                $("#error").show();
            }else if (response == 'maximum'){
                $("#error").attr('class', 'alert alert-danger');
                $("#error").html('Maximum order quantity is '+ max + ' unit!');
                $("#error").show();
            }else if (response == 'quantity'){
                $("#error").attr('class', 'alert alert-danger');
                $("#error").html('Please select quantity!');
                $("#error").show();
            }else if (response == 'unavailable'){
                $("#error").attr('class', 'alert alert-danger');
                $("#error").html('Stock unavailable!');
                $("#error").show();
            } else if (response == 'updated') {
                $("#error").attr('class', 'alert alert-success');
                $("#error").html('Cart updated!');
                $("#error").show();
                cart();
            } else {
                $("#error").attr('class', 'alert alert-success');
                $("#error").html('Product booked successfully!');
                $("#error").show();
                cart();
            }
            $(".bsbook-now-button").html('<i class="icon-basket"></i> Book Now');
        }).fail(function() {
            console.log('Failed');
        });
    });

    $("body").on('click','.bsbook-now-button',function() {
        var id = $(this).data('id');
        var min = $('#min-qty').val();
        var max = $('#max-qty').val();
        var type = 'bysize';
        var hub_id = <?php echo $ret[0]['hub_id']?>;

        var quantity = [];
        var variants = [];
        var price = [];
        var unit = [];
        var urlLink = '{{ route('business.institutionalRfqBooking') }}';
        var token = '<?php echo csrf_token(); ?>';

        $(".quantityvarid").each(function () {
            if($(this).parent().parent().is(":visible")){
                var quant = '';
                quantityvarid = parseInt(this.value);
                quant = $(".pridvalue"+quantityvarid).val();
                if(quant){
                    variants.push(quantityvarid);
                    quantity.push(quant);
                    price.push($("#mainprice"+quantityvarid).val());
                    unit.push($("#unit"+quantityvarid).val());
                }
            }
        });
        var options = $(".options").serialize();
        $(".bsbook-now-button").html('<div class="loading"></div>');
        $.ajax({
            url: urlLink,
            type: 'post',
            data: 'id='+id+'&q='+quantity+'&op='+options+'&variants='+variants+'&min='+min+'&price='+price+'&units='+unit+'&max='+max+'&_token='+token+'&hub_id='+hub_id+'&type='+type,
            crossDomain: true,
        }).done(function(response) {
            if (response == 'invalid'){
                $("#error").attr('class', 'alert alert-danger');
                $("#error").html('Invalid quantity!');
                $("#error").show();
            }else if (response == 'minimum'){
                $("#error").attr('class', 'alert alert-danger');
                $("#error").html('Minimum order quantity is '+ min + ' unit!');
                $("#error").show();
            }else if (response == 'maximum'){
                $("#error").attr('class', 'alert alert-danger');
                $("#error").html('Maximum order quantity is '+ max + ' unit!');
                $("#error").show();
            }else if (response == 'quantity'){
                $("#error").attr('class', 'alert alert-danger');
                $("#error").html('Please select quantity!');
                $("#error").show();
            }else if (response == 'unavailable'){
                $("#error").attr('class', 'alert alert-danger');
                $("#error").html('Stock unavailable!');
                $("#error").show();
            } else if (response == 'updated') {
                $("#error").attr('class', 'alert alert-success');
                $("#error").html('Cart updated!');
                $("#error").show();
                cart();
            } else {
                $("#error").attr('class', 'alert alert-success');
                $("#error").html('Product booked successfully!');
                $("#error").show();
                cart();
            }
            $(".bsbook-now-button").html('<i class="icon-basket"></i> Book Now');
        }).fail(function() {
            console.log('Failed');
        });
    });
</script>
