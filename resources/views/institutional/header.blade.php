<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?=$title;?></title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="assets/css/select2.css">
    <link rel="stylesheet" href="<?=$tp;?>/vendors/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="<?=$tp;?>/vendors/base/vendor.bundle.base.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="<?=$tp;?>/vendors/datatables.net-bs4/dataTables.bootstrap4.css">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="<?=$tp;?>/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="<?=$tp;?>/css/style.css">

    <!-- endinject -->
    <link rel="shortcut icon" href="<?=$tp;?>/images/favicon.png" />
</head>
<body class="sidebar-icon-only">
<div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
        <div class="navbar-brand-wrapper d-flex justify-content-center">
            <div class="navbar-brand-inner-wrapper d-flex justify-content-between align-items-center w-100">
                <a class="navbar-brand brand-logo" href="{{ route('business.institutionalDashboard') }}"><img src="<?=$tp;?>/images/aakar360.png" alt="logo"/></a>
                <a class="navbar-brand brand-logo-mini" href="{{ route('business.institutionalDashboard') }}"><img src="<?=$tp;?>/images/logo-mini.svg" alt="logo"/></a>
                <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
                    <span class="mdi mdi-sort-variant"></span>
                </button>
            </div>
        </div>
        <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
            <ul class="navbar-nav navbar-nav-right">
                <li class="nav-item nav-profile dropdown">
                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                        <img src="<?=$tp;?>/images/faces/face5.jpg" alt="profile"/>
                        <span class="nav-profile-name"><?php $username = institutional('name'); $t = explode(' ', $username); echo $t[0]; ?></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                        <a class="dropdown-item" href="{{ route('business.institutionalIndex') }}">
                            <i class="mdi mdi-web text-primary"></i>
                            Visit Website
                        </a>
                        <a class="dropdown-item" href="{{ route('business.institutionalMyProfile') }}">
                            <i class="mdi mdi-account text-primary"></i>
                            My Profile
                        </a>
                        <a class="dropdown-item" href="{{ route('business.institutionalChangePassword') }}">
                            <i class="mdi mdi-lock text-primary"></i>
                            Change Password
                        </a>
                        <a class="dropdown-item" href="{{ route('business.institutionalLogout') }}">
                            <i class="mdi mdi-logout text-primary"></i>
                            Logout
                        </a>
                    </div>
                </li>
            </ul>
            <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
                <span class="mdi mdi-menu"></span>
            </button>
        </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('business.institutionalDashboard') }}">
                        <i class="mdi mdi-home menu-icon"></i>
                        <span class="menu-title">Dashboard</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                        <i class="mdi mdi-cart menu-icon"></i>
                        <span class="menu-title">Products</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="collapse" id="ui-basic">
                        <ul class="nav flex-column sub-menu">
                            <li class="nav-item"> <a class="nav-link" href="<?=route('business.institutionalMyQuotatuins')?>">My Quotations</a></li>
                            <li class="nav-item"> <a class="nav-link" href="<?=route('business.institutionalMyBookings')?>">My Bookings</a></li>
                            {{--<li class="nav-item"> <a class="nav-link" href="<?=route('business.institutionalDispatchProgram')?>">Dispatch Programs</a></li>--}}
                            {{--<li class="nav-item"> <a class="nav-link" href="<?=route('business.institutionalMyOrders')?>">My Orders</a></li>--}}
                        </ul>
                    </div>
                </li>
            </ul>
        </nav>
    <div class="main-panel">