<footer class="footer">
    <div class="d-sm-flex justify-content-center justify-content-sm-between">
        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019 <a href="https://www.aakar360.com/" target="_blank">Aakar360 Mentors Pvt Ltd</a>. All rights reserved.</span>
        <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
    </div>
</footer>
</div>
</div>
<!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->

<!-- plugins:js -->
<script src="<?=$tp;?>/vendors/base/vendor.bundle.base.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="<?=$tp;?>/vendors/chart.js/Chart.min.js"></script>
<script src="<?=$tp; ?>/vendors/data-tables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/js/dataTables.select.min.js" type="text/javascript"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="<?=$tp;?>/js/off-canvas.js"></script>
<script src="<?=$tp;?>/js/hoverable-collapse.js"></script>
<script src="<?=$tp;?>/js/template.js"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="assets/js/select2.js"></script>
<script src="<?=$tp;?>/js/dashboard.js"></script>
<script src="<?=$tp;?>/js/bootstrap-datepicker.min.js"></script>
<script>
    $('.select2').select2({
        width: '100%'
    });
</script>
<!-- End custom js for this page-->
</body>

</html>