<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class CrmMasterQuickEntry extends Model
{

    protected $table = 'crm_master_quick_entry';

}
