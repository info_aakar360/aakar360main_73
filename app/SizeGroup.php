<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class SizeGroup extends Model
{

    protected $table = 'sizes_group';

}
