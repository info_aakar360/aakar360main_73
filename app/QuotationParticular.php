<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class QuotationParticular extends Model
{
    protected $table = 'quotation_particular';
}
