<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class ServiceWishlist extends Model
{
    protected $table = 'service_wishlist';
}
