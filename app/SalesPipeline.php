<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class SalesPipeline extends Model
{
    protected $table = 'sales_pipelines';
}
