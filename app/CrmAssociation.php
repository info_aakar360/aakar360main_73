<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class CrmAssociation extends Model
{

    protected $table = 'crm_association';

}
