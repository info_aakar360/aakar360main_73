<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class MultisellerPayment extends Model
{

    protected $table = 'multiseller_payments';

}
