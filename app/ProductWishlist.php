<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class ProductWishlist extends Model
{
    protected $table = 'product_wishlist';
}
