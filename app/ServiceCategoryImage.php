<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class ServiceCategoryImage extends Model
{

    protected $table = 'services_category_image';

}
