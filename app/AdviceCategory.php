<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class AdviceCategory extends Model
{

    protected $table = 'advices_category';

}
