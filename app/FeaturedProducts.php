<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class FeaturedProducts extends Model
{

    protected $table = 'featured_products';

}
