<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class CrmCustomerProject extends Model
{

    protected $table = 'crm_customer_projects';

}
