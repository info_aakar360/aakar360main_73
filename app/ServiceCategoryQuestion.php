<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class ServiceCategoryQuestion extends Model
{

    protected $table = 'services_category_questions';

}
