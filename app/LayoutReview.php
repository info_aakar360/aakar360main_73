<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class LayoutReview extends Model
{

    protected $table = 'layout_reviews';

}
