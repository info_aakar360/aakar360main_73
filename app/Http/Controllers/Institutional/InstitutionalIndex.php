<?php

namespace App\Http\Controllers\Institutional;

use App\AdviceCategory;
use App\Banner;
use App\Blocks;
use App\Blocs;
use App\Blog;
use App\Brand;
use App\Browser;
use App\Category;
use App\City;
use App\CityWise;
use App\Config;
use App\Countries;
use App\Country;
use App\Currency;
use App\Customer;
use App\CustomerCache;
use App\DefaultImage;
use App\DesignCategory;
use App\Footer;
use App\Http\Controllers\FrontendBaseController;
use App\ImageInspiration;
use App\ImageLike;
use App\Lang;
use App\LayoutWishlist;
use App\ManufacturingHubs;
use App\MegaMenuImage;
use App\Menu;
use App\Os;
use App\PasswordReset;
use App\Payments;
use App\ProductWishlist;
use App\RateRequests;
use App\Referrer;
use App\RegisterImage;
use App\ServiceCategory;
use App\ServiceWishlist;
use App\SpecialRequirements;
use App\Style;
use App\Tracking;
use App\UserProject;
use App\Visitor;
use Illuminate\Auth\Authenticatable;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use SebastianBergmann\CodeCoverage\Node\Directory;
use Image;
use Illuminate\Support\Facades\Session;

//use Auth;

class InstitutionalIndex extends FrontendBaseController
{
    public function institutionalStatic(){
        return view('institutionalStatic');
    }
    public function index()
    {
        $hub = 0;
        if(isset($_COOKIE['aakar_hub'])) {
            $hub = $_COOKIE['aakar_hub'];
        }

        if(isset($_POST['hub'])){
            setcookie('aakar_hub', $_POST['hub'], time()+60*60*24*365);
        }

        $header = $this->institutionalHeader(false,false,false,true);
        $vtype=Session::get('aakar360.visitor_type');
        $style = $this->style;
        if(preg_match("/(youtube.com)\/(watch)?(\?v=)?(\S+)?/", $this->style->media)){
            parse_str(parse_url($this->style->media, PHP_URL_QUERY),$video);
            $media = '<a target="_blank" href="'.$this->style->media.'"><img class="landing-video" src="https://i3.ytimg.com/vi/'.$video['v'].'/mqdefault.jpg"></a>';
        } else {
            // Show image
            $media = '<img class="landing-image" src="'.$this->style->media.'">';
        }
        $show_preferences = false;
        $preferences = null;
        if(institutional('id') !== ''){
            if(isset($_POST['categories'])){
                if($_POST['categories'] != ''){
                    $preferences = Customer::find(institutional('id'))->preferences()->where('type', 'category')->first();
                    if($preferences === null){
                        $pre = new CustomerCache();
                        $pre->user_id = institutional('id');
                        $pre->type = 'category';
                        $pre->cache = $_POST['categories'];
                        $pre->save();
                        return redirect(route('business.institutionalIndex'));
                    }else{
                        $preferences->cache = $_POST['categories'];
                        $preferences->save();
                    }
                }
            }
            $pre = Customer::find(institutional('id'))->preferences()->where('type', 'category')->first();
            if($pre === null){
                $preferences = null;
                $show_preferences = true;
            }else{
                $preferences = explode(',', $pre->cache);
            }
        }else{
            if(isset($_POST['categories'])){
                setcookie('categories', $_POST['categories'], time() + (86400 * 30));
                return redirect(route('business.institutionalIndex'));
            }
            if(isset($_COOKIE['categories'])){
                $preferences = explode(',', $_COOKIE['categories']);
            }else{
                $preferences = null;
            }
            if($preferences === null){
                $show_preferences = true;
            }
        }
        $categories = false;
        if($show_preferences){
            $categories = Category::orderBy('name', 'ASC')->get();
        }
        $blocs1 = Blocks::where('area','=','home')->get();
        if(!empty($blocs1)) {
            $blocs = $blocs1;
        }else{
            $blocs = 'Data Not Available';
        }
        $banners1 = Banner::where('section','=','institutional_home')->orderBy('priority','ASC')->get();
        if(!empty($banners1)){
            $banners = $banners1;
        }else{
            $banners = false;
        }
        $posts1 = Blog::orderBy('time', 'DESC')->limit(8);
        if(!empty($posts1)){
            $posts = $posts1;
        }else{
            $posts = 'Data Not Available';
        }

        $categoryforhomebrand1 = Category::where('popular_in_brands','=',1)->orderBy('popular_brands_priority','ASC')->limit(1);
        if(!empty($categoryforhomebrand1)){
            $categoryforhomebrand = $categoryforhomebrand1;
        }else{
            $categoryforhomebrand = 'Data Not Available';
        }
        $service_categories1 = ServiceCategory::where('parent','=', 0)->orderBy('name','ASC')->get();
        if(!empty($service_categories1)){
            $service_categories = $service_categories1;
        }else{
            $service_categories = 'Data Not Available';
        }
        $design_categories1 = DesignCategory::where('parent','=', 0)->orderBy('name','ASC')->get();
        if (!empty($design_categories1)){
            $design_categories = $design_categories1;
        }else{
            $design_categories = 'Data Not Available';
        }
        $rem_pre = 0;
        if($preferences === null){
            $pcss = Category::where('popular', 1)->orderBy('popular_priority', 'ASC')->skip(0)->take(5)->get();
        }else{
            $pcss = Category::whereIn('id', $preferences)->orderBy('popular_priority', 'ASC')->skip(0)->take(5)->get();
            $re = count($pcss);
            $limit = 5 - $re;
            if($limit){
                $more = Category::where('popular', 1)->orderBy('popular_priority', 'ASC')->skip(0)->take($limit)->get();
                $pcss = $pcss->merge($more);
            }else{
                $rem_pre = $re-5;
            }
        }
        $footer = $this->institutionalFooter();
        $data['tp'] = url("/assets/");
        $categoryforhomebrand1 = Category::where('popular_in_brands','=',1)->orderBy('popular_brands_priority','ASC')->limit(1);
        if(!empty($categoryforhomebrand1)){
            $categoryforhomebrand = $categoryforhomebrand1;
        }else{
            $categoryforhomebrand = 'Data Not Available';
        }
        $top_brands = Brand::orderBy('name', 'ASC')->get();
        $hubs = ManufacturingHubs::orderBy('title')->get();
        return view('frontInstitutional/index')->with(compact('header','style','media','blocs', 'top_brands', 'banners', 'posts','categoryforhomebrand','service_categories','design_categories', 'footer','data', 'preferences', 'show_preferences', 'pcss', 'categories', 'rem_pre', 'hub', 'hubs'));
    }

    public function changeHub(Request $request){
        if(isset($_POST['hub'])){
            setcookie('aakar_hub', $_POST['hub'], time() + (86400 * 30));
        }
        return back();
    }

    public function changePassword()
    {
        $error = '';
        if(isset($_POST['login'])) {
            if (!empty($_POST['oldpassword']) && !empty($_POST['confirm_password']) && !empty($_POST['password'])) {
                // Escape the user entries and insert them to database
                $password = md5($_POST['password']);
                $opassword = md5($_POST['oldpassword']);
                $old = Customer::where('password',$opassword)->first();
                if (!empty($old)) {
                    if ($_POST['password'] != $_POST['confirm_password']) {
                        $error = '<div class="alert alert-warning">Password Missmatch!</div>';
                    } else {
                        $visitor = Customer::where('id',$old->id)->first();
                        $visitor->password = $password;
                        $visitor->save();
                        $error = '<div class="alert alert-success">Password Successfully Changed</div>';
                    }
                }
            } else {
                $error = '<div class="alert alert-warning">All fields are required !</div>';
            }
        }
        $header = $this->institutionalHeader(translate('Change Password'),false,true);
        $dimg = DefaultImage::where('page','Change Password')->first();
        $footer = $this->institutionalFooter();
        return view('change-password')->with(compact('header','error','dimg','footer'));
    }

    public function logout(Request $request)
    {
        // Clear the customer session
        $request->session()->forget('institutional');
        session(['institutional' => '']);
        return redirect('/');
    }


}
