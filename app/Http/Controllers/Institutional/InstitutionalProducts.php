<?php
namespace App\Http\Controllers\Institutional;

use App\AdviceCategory;
use App\Banner;
use App\BestForCategory;
use App\BestForAdvice;
use App\Blocs;
use App\Blog;
use App\Brand;
use App\Browser;
use App\Category;
use App\City;
use App\CityWise;
use App\Config;
use App\CounterOffer;
use App\Countries;
use App\Country;
use App\CreditCharges;
use App\Currency;
use App\Customer;
use App\CustomerCache;
use App\DefaultImage;
use App\DesignCategory;
use App\FeaturedProducts;
use App\Filter;
use App\Footer;
use App\Http\Controllers\FrontendBaseController;
use App\Http\Middleware\Institutional;
use App\ImageInspiration;
use App\ImageLike;
use App\Lang;
use App\LayoutWishlist;
use App\Link;
use App\ManufacturingHubs;
use App\MegaMenuImage;
use App\Menu;
use App\Os;
use App\PasswordReset;
use App\Payments;
use App\PhRelations;
use App\ProductDiscount;
use App\ProductFaq;
use App\Products;
use App\ProductVariant;
use App\ProductWishlist;
use App\RateRequests;
use App\Referrer;
use App\RegisterImage;
use App\RequestQuotation;
use App\Review;
use App\ServiceCategory;
use App\ServiceWishlist;
use App\SpecialRequirements;
use App\Style;
use App\Tracking;
use App\Unit;
use App\UserProject;
use App\Visitor;
use Illuminate\Auth\Authenticatable;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use SebastianBergmann\CodeCoverage\Node\Directory;
use Image;
use Illuminate\Support\Facades\Session;
use Stripe\Product;

//use Auth;
class InstitutionalProducts extends FrontendBaseController
{
    public function products($cat = '', $sub = false)
    {

        if ($cat == '')
        {
            abort(404);
        }

        if (isset($_POST['hub']))
        {

            setcookie('aakar_hub', $_POST['hub'], time() + 60 * 60 * 24 * 365);
            if ($sub)
            {
                return redirect(route('business.institutionalProducts',[$cat, $sub]));
            }
            return redirect(route('business.institutionalProducts',[$cat]));
        }
        $storearray = array();
        $data['show_cities'] = false;
        $preferences = null;
        if (institutional('id') !== '')
        {
            if (isset($_POST['cities']))
            {
                setcookie('cities', $_POST['cities'], time() + (86400 * 30));
                return redirect(route('business.institutionalIndex'));
            }
            if (isset($_COOKIE['cities']))
            {
                $preferences = explode(',', $_COOKIE['cities']);
            }
            else
            {
                $preferences = null;
            }
            if ($preferences === null)
            {
                $data['show_cities'] = true;
            }
        }
        else
        {
            if (isset($_POST['cities']))
            {
                setcookie('cities', $_POST['cities'], time() + (86400 * 30));
                return redirect(route('business.institutionalIndex'));
            }
            if (isset($_COOKIE['cities']))
            {
                $preferences = explode(',', $_COOKIE['cities']);
            }
            else
            {
                $preferences = null;
            }
            if ($preferences === null)
            {
                $data['show_cities'] = true;
            }
        }
        $data['rem_pre'] = 0;
        if ($preferences === null)
        {
            $data['pcss'] = CityWise::get();
        }
        else
        {
            $data['pcss'] = CityWise::whereIn('city', $preferences)->get();
            foreach ($data['pcss'] as $cvalues)
            {
                $cpdata = json_decode($cvalues->prices);
                $storearray[] .= $cpdata[0]->product;
            }
        }
        $data['show_filter'] = true;
        if (institutional('user_type') == '')
        {
            $data['show_filter'] = false;
        }
        $data['show_products'] = false;
        if ($sub)
        {
            $cat = $sub;
            $data['show_filter'] = true;
        }
        $data['categ'] = Category::where('path', $cat)->first();
        if ($data['categ'] === null)
        {
            abort(404);
        }
        $pdata = Category::where('parent', $data['categ']->id)
            ->get();

        if (count($pdata) == 0)
        {
            $data['show_products'] = true;
        }
        $data['header'] = $this->institutionalHeader(translate('Products') , false, true);
        $data['price'] = array();
        $where = array();
        $data['bids'] = array();
        if (isset($_GET['min']) && isset($_GET['max'])){
            if (institutional('user_type') != 'institutional'){
                $where['price'] = "price BETWEEN '" . escape($_GET['min']) . "' AND '" . escape($_GET['max']) . "'";
            }
            else{}
        }
        if (isset($_GET['brands'])){
            $data['bids'] = array_keys($_GET['brands']);
        }
        if (!empty($catlink)){
            $where['search'] = "(category = " . $catlink . ")";
        }
        if ($cat){
            $check = Category::where('id', $data['categ']->id)->get();
            if (count($check) == 0){
                abort(404);
            }
            $data['category'] = Category::where('id', $data['categ']->id)->first();
            $categories[] = $data['category']->id;
            if ($data['category']->parent == 0){
                $childs = Category::where('parent', $data['category']->id)->get();
                foreach ($childs as $child){
                    $categories[] = $child->id;
                }
            }
            $where['cat'] = "category IN (" . implode(',', $categories) . ")";
        }
        $tpbrands = '';
        $data['suggested'] = false;
        $data['top_brands'] = [];
        if ($data['categ']){
            $data['category'] = Category::where('id', $data['categ']->id)->first();
            $tpbrands = explode(',', $data['category']->brands);
            if ($data['category']->brands !== ''){
                $data['top_brands'] = Brand::whereIn('id', $tpbrands)->orderBy('name', 'ASC')->get();
            }
            $sg = Products::where('category', $data['categ']->id)->first();
            $sug_prods = '';
            $data['suggested'] = [];
            if ($sg){
                $sug_prods = explode(',', $sg->product_id);
                $data['suggested'] = Products::where('category', $data['categ']->id)->get();
            }
            $data['product_categories'] = Category::where('parent', $data['categ']->id)->orderBy('name', 'ASC')->get();
            $data['procount'] = Category::where('parent', $data['categ']->id)->count();
        }
        $categoriescon = Category::where('parent', 0)->orderBy('name', 'ASC')->count();
        if ($categoriescon !== ''){
            $data['cats'] = Category::where('parent', 0)->orderBy('name', 'ASC')->get();
        }
        else{ $data['cats'] = 'Data Not Available'; }
        $data['faqs'] = ProductFaq::whereRaw('FIND_IN_SET("' . $data['categ']->id . '",category)')->where('section', 'retail')->orderBy('id', 'DESC')
            ->take(5)->get();
        $data['ladvices'] = Blog::whereRaw('FIND_IN_SET("' . $data['categ']->id . '",pro_cat)')
            ->where('section', 'retail')->orderBy('id', 'DESC')->take(4)->get();
        $data['tadvices'] = Blog::whereRaw('FIND_IN_SET("' . $data['categ']->id . '",pro_cat)')
            ->where('section', 'retail')->orderBy('visits', 'DESC')->take(4)->get();
        $data['best'] = BestForCategory::where('category', $data['categ']->id)
            ->orderBy('id', 'DESC')->get();
        $data['brands'] = array();
        if ($data['categ']->brands){
            $tpbrands = explode(',', $data['categ']->brands);
            $data['brands'] = Brand::whereIn('id', $tpbrands)->orderBy('id', 'DESC')->get();
        }
        $data['filters'] = [];
        $fc = Category::where('id', $data['categ']->id)->first();
        $cfc = $fc->filter;
        if ($cfc !== ''){
            $ccx = explode(',', $cfc);
            foreach ($ccx as $cx){
                $data['filters'] = Filter::where('id', $cx)->get();
            }
        }
        else{ $data['filters'] = ''; }
        $cid[] = $data['categ']->id;
        $c = array();
        $cont = true;
        $temp = $cid;
        while ($cont){
            $c = Arr::pluck(Category::whereIn('parent', $temp)->get() , 'id');
            if (count($c)){
                $cid = array_merge($cid, $c);
                $temp = $c;
            }
            else{ $cont = false; }
        }
        $products = Products::whereIn('category', $cid)->whereIn('id', $storearray);
        $data['link'] = Link::where('page', 'Products')->where('link', '<>', '')->first();
        $data['price']['set_min'] = 0;
        $data['price']['set_max'] = 0;
        $p_min = DB::select("SELECT MIN(price) as min_price, MAX(price) as max_price FROM products where category IN (" . implode(',', $cid) . ")");
        if (count($p_min) > 0){
            $data['price']['min'] = $p_min[0]->min_price;
            $data['price']['set_min'] = $p_min[0]->min_price;
            $data['price']['max'] = $p_min[0]->max_price;
            $data['price']['set_max'] = $p_min[0]->max_price;
        }
        else{
            $data['price']['min'] = 0;
            $data['price']['set_min'] = 0;
            $data['price']['max'] = 0;
        }
        if (isset($_GET['min'])){
            $data['price']['set_min'] = $_GET['min'];
            if (institutional('user_type') != 'institutional') $products = $products->where('price', '>=', $_GET['min']);
        }
        if (isset($_GET['max'])){
            $data['price']['set_max'] = $_GET['max'];
            if (institutional('user_type') != 'institutional') $products = $products->where('price', '<=', $_GET['max']);
        }
        if (count($data['bids']) > 0){
            $products = $products->whereIn('brand_id', $data['bids']);
        }
        $data['hub'] = 0;
        if (isset($_COOKIE['aakar_hub'])){
            $data['hub'] = $_COOKIE['aakar_hub'];
        }
        $data['products'] = $products->get();
        $data['footer'] = $this->institutionalFooter();
        $data['hubs'] = ManufacturingHubs::orderBy('title')->get();
        $data['vtype'] = Session::get('aakar360.visitor_type');
        $data['inst'] = Customer::get();
        return view('frontInstitutional/products')->with($data);
    }

    public function product(Request $request, $product_id){
        $data['hub'] = 0;
        if (isset($_COOKIE['aakar_hub'])){
            $data['hub'] = $_COOKIE['aakar_hub'];
        }
        $data['recent_viewed'] = 0;
        $variants = false;
        $cookie_name = "sellerkit.recent";
        if ($request->session()->get($cookie_name) == null){
            $request->session()->put($cookie_name, explode('-', $product_id) [0]);
        }
        else{
            $data['recent_viewed'] = Products::where('id', $request->session()->get($cookie_name))->get();
            $request->session()->put($cookie_name, $request->session()->get($cookie_name) . ',' . explode('-', $product_id) [0]);
        }
        $check = Products::where('id', explode('-', $product_id) [0])->get();
        if (count($check) == 0){
            abort(404);
        }
        $exp_array = explode('-', $product_id);
        $pid = explode('-', $product_id) [0];
        if (count($exp_array) == 2){
            $data['product'] = DB::select("SELECT * FROM `products` as p1 WHERE p1.sku in (SELECT p2.sku FROM products as p2 WHERE p2.id=$pid) and p1.sale in (Select min(p3.sale) from products as p3 where p3.sku=p1.sku)") [0];
        }else{
            $data['product'] = Products::where('id', $pid)->first();
        }
        $data['cat'] = Category::where('id', $data['product']->category)->first();
        if (empty($data['hub']) && institutional('user_type') == 'institutional'){
            return redirect(route('business.institutionalProducts',[$data['cat']->path]));
        }
        $prid = $data['product']->id;
        $data['variants'] = ProductVariant::where('display', '1')->where('product_id', $prid)->get();
        $data['creditvariant'] = ProductVariant::where('display', '1')->where('product_id', $prid)->first();
        $data['dis_var'] = ProductVariant::where('display', '1')->where('product_id', $prid)->get();
        $data['bulk_discounts'] = ProductDiscount::where('product_id', $prid)->orderBy('quantity', 'ASC')->get();
        $data['total_ratings'] = Review::where('active', '1')->where('product', $prid)->count();
        $data['rating'] = 0;
        if ($data['total_ratings'] > 0){
            $rating_summ = Review::where('active', '1')->where('product', $prid)->sum('rating');
            $data['rating'] = $rating_summ / $data['total_ratings'];
        }
        $data['total_reviews'] = Review::where('active', '1')->where('review', '<>', '')->where('product', $prid)->count();
        $data['ratinga'] = DB::select("SELECT count(*) as total_user, SUM(rating) as total_rating FROM reviews WHERE active = '1' AND product = '" . $prid . "'") [0];
        $data['total_rating'] = $data['ratinga']->total_rating;
        $data['total_user'] = $data['ratinga']->total_user;
        if ($data['total_rating'] == 0){
            $data['avg_rating'] = 0;
        }else{
            $data['avg_rating'] = round($data['total_rating'] / $data['total_user'], 1);
        }
        $data['rating1'] = Review::where('rating', '1')->where('product', $prid)->count('id', 'rating1_user');
        $data['rating2'] = Review::where('rating', '2')->where('product', $prid)->count('id', 'rating2_user');
        $data['rating3'] = Review::where('rating', '3')->where('product', $prid)->count('id', 'rating3_user');
        $data['rating4'] = Review::where('rating', '4')->where('product', $prid)->count('id', 'rating4_user');
        $data['rating5'] = Review::where('rating', '5')->where('product', $prid)->count('id', 'rating5_user');
        $data['images'] = explode(',', $data['product']->images);
        $title = $data['product']->title;
        $desc = $data['product']->text;
        $data['specification'] = $data['product']->specification;
        $data['header'] = $this->institutionalHeader(translate($title) , $desc, true);
        $data['reviews'] = Review::where('active', '1')->where('product', $prid)->orderBy('time', 'DESC')->get();
        $data['tp'] = url("/themes/" . $this->cfg->theme);
        $data['footer'] = $this->institutionalFooter();
        $data['related_products'] = getRelatedProducts($data['product']->category, $data['product']->id);
        $get_sku = Products::where('id', $pid)->select('sku')->first();
        $data['sellers'] = DB::table('products')->join('customers', 'customers.id', '=', 'products.added_by')
            ->where('products.sku', $get_sku->sku)->select('customers.*', 'products.*')->take('5')->get();
        $data['faqs'] = ProductFaq::where('section', 'institutional')->where('status', '1')->orderBy('id', 'DESC')->get();
        $data['product'] = Products::where('id', '=', $data['product']->id)->first();
        $data['cat'] = Category::where('id', '=', $data['product']->category)->first();
        $data['wunits'] = Unit::whereIn('id', explode(',', $data['cat']->weight_unit))->get();
        $data['link'] = Link::where('page', 'Single Product')->first();
        $data['brands'] = Brand::get();
        $data['vtype'] = Session::get('aakar360.visitor_type');
        $data['cities'] = City::where('is_available', '1')->orderBy('name', 'ASC')->get();
        $data['projects'] = UserProject::all();
        $data['units'] = Unit::all();
        $data['creditCharges'] = CreditCharges::all();
        return view('frontInstitutional/product')->with($data);
    }

    public function checkPincode(Request $request){
        $pincode = $request->pincode;
        $html = '';
        if (!empty($pincode)){
            $city = City::whereRaw('FIND_IN_SET(' . $pincode . ',pincodes)')->first();
            if ($city != null){
                $html = '<div class="alert alert-success">
                    Delivery is available in this location.
                </div>';
                return $html;
            }else{
                $html = '<div class="alert alert-warning">
                    Delivery not available in this location.
                </div>';
                return $html;
            }
        }
    }

    public function selectCity(Request $request){
        $cityid = $request->cityid;
        setcookie('cities', $cityid, time() + (86400 * 30));
        return redirect(route('business.institutionalIndex'));
    }

    public function requestQuotation(Request $request){
        $id = false;
        $req = json_decode($request->variant);
        $other = '';
        $location = '';
        $quotation = array();
        if ($request->other_option){
            if ($request->other_option == ''){ $other = ''; }
            else{ $other = $request->other_option; }
        }
        if ($request->location){
            if ($request->location == ''){
                $location = '';
            }else{
                $location = $request->location;
            }
        }
        $vars = stripslashes($request->variants);
        $xx = 0;
        if ($request->met == 'bybasic') {
            $bids = explode(',', $request->b_id);
            $var_data = json_decode(stripcslashes($request->variants));
            foreach ($bids as $bid){
                $varsx = array();
                $br = DB::table('products')->where('id', $bid)->first();
                if ($br !== null){
                    $pricing = getInstitutionalPrice($br->id, Institutional('sid') , $request->hub_id, false, false);
                    foreach ($var_data as $vd){
                        $varsx[] = array(
                            'id' => $bid,
                            'title' => $br->title,
                            'quantity' => $vd->quantity,
                            'unit' => $vd->unit,
                            'hub_id' => $vd->hub_id,
                            'basic_price' => (is_numeric($pricing['price'])) ? round($pricing['price']) : $pricing['price'],
                            'action' => 0,
                            'var_price' => 0,
                            'new_price' => 0,
                        );
                    }
                }
                $quotation[] = array(
                    'cat_id' => $request->c_id,
                    'hub_id' => $request->hub_id,
                    'product_id' => $request->id,
                    'brand_id' => $bid,
                    'payment_option' => $request->payment_option,
                    'other_option' => $other,
                    'method' => $request->met,
                    'location' => $location,
                    'variants' => json_encode($varsx) ,
                    'user_id' => $request->user_id,
                    'credit_charge_id' => $request->credit_charge_id
                );
                $xx++;
            }
        }
        else{
            $bids = explode(',', $request->b_id);
            $var_data = [];
            $variant = explode(',', $request->variants);
            $quantity = explode(',', $request->q);
            $unit = explode(',', $request->units);
            $request->title;
            $var_data = (array)json_decode(stripcslashes($request->variants));
            foreach ($bids as $key => $bid){
                $varsx = array();
                $br = Products::where('id', $bid)->first();
                $brId = false;
                if ($br !== null){
                    if ($br->id){
                        $brId = $br->id;
                    }
                }
                foreach (explode(',', $request->title) as $key => $vd){
                    $varId = false;
                    if ($variant[$key]){
                        $varId = $variant[$key];
                    }
                    $varsx[$bid][] = array(
                        "id" => $variant[$key],
                        "title" => $vd,
                        "quantity" => $quantity[$key],
                        "unit" => $unit[$key],
                        "action" => '',
                        "var_price" => '',
                        "price_detail" => getInstitutionalPrice($brId, Institutional('sid') , $request->hub_id, $varId, false, false) ,
                    );
                }
                $quotation[] = array(
                    'cat_id' => $request->c_id,
                    'hub_id' => $request->hub_id,
                    'product_id' => $request->id,
                    'brand_id' => $bid,
                    'payment_option' => $request->payment_option,
                    'other_option' => $other,
                    'method' => $request->met,
                    'location' => $location,
                    'variants' => json_encode($varsx) ,
                    'user_id' => $request->user_id,
                    'credit_charge_id' => $request->credit_charge_id
                );
                $xx++;
            }
        }
        DB::table('request_quotation')->insert($quotation);
        return response()->json(['message' => 'entry successfully in request quotation ', 'code' => 200], 200);
    }

    public function rfqRequestQuotation(Request $request){
        $id = false;
        $req = json_decode($request->variant);
        $other = '';
        $location = '';
        $quotation = array();
        if (isset($request->other_option)){
            if ($request->other_option == ''){
                $other == '';
            }else{
                $other == $request->other_option;
            }
        }
        if ($request->location){
            if ($request->location == ''){
                $location == '';
            }else{
                $location == $request->location;
            }
        }
        $quotation = array(
            'rfq_id' => $request->rfq_id,
            'cat_id' => $request->cat_id,
            'product_id' => $request->product_id,
            'brand_id' => $request->brand_id,
            'credit_charge_id' => $request->credit_charge_id,
            'hub_id' => $request->hub_id,
            'payment_option' => $request->payment_option,
            'other_option' => $other,
            'method' => $request->method,
            'location' => $request->location,
            'variants' => $request->variants,
            'user_id' => customer('id')
        );
        $items = DB::table('rfq_booking')->insert($quotation);
        if ($items){
            $status = '2';
            $rq = RequestQuotation::where('id', $request->rfq_id)->first();
            $rq->status = $status;
            $rq->save();
        }
        return back()->with("success", ["Booked Successfully !"]);
    }

    public function productWishlist(Request $request)
    {
        $prodcutWishlist = new ProductWishlist();
        $prodcutWishlist->user_id = Auth::user()->id;
        $prodcutWishlist->product_id = $request->product_id;
        $prodcutWishlist->project_id = $request->project_id;
        $prodcutWishlist->save();
    }
    public function counterOffer(Request $request)
    {
        $counterOffer = new CounterOffer();
        $counterOffer->user_id = $request->user_id;
        $counterOffer->product_id = $request->product_id;
        $counterOffer->credit_charge_id = $request->credit_charge_id;
        $counterOffer->hub_id = $request->hub_id;
        $counterOffer->price = $request->price;
        $counterOffer->quantity = $request->counterqty;
        $counterOffer->unit = $request->unit;
        $counterOffer->status = "0";
        $id = $counterOffer->save();
        if ($id){
            return back()->with("success", ["Counter Saved !"]);
        }
        return back()->withErrors('msgx', ['Oops! Something went wrong. Try again later.']);
    }

    public function getVarPriceDetail(Request $request){
        $diffrence = $loading = $tax = $credit = 0;
        $quantity = explode(',', $request->quantity);
        $variant_id = explode(',', $request->variant_id);
        $credit_id = $request->credit_id;
        $weight_unit = $request->weight_unit;
        $creditText = '';
        for ($i = 0;$i < count($variant_id);$i++){
            $quan = $quantity[$i];
            $varid = $variant_id[$i];
            $product_id = $request->product_id;
            $customer_id = $request->customer_id;
            $hub_id = $request->hub_id;
            $priceDetail = getInstitutionalPrice($product_id, $customer_id, $hub_id, $varid, false);
            $diffrence += ((is_numeric((int)$priceDetail['price']) ? (int)$priceDetail['price'] : 0 * $quan));
            $loading += ((is_numeric((int)$priceDetail['loading']) ? (int)$priceDetail['loading'] : 0 * $quan));
            $tax += ((is_numeric((int)$priceDetail['tax']) ? (int)$priceDetail['tax'] : 0 * $quan));
            if ($credit_id !== 'undefined'){
                $creditCharge = CreditCharges::where('id', $credit_id)->first();
                if ($creditCharge !== null) $creditText = $creditCharge->amount . ' per <span class="setUnit">' . getUnitSymbol($weight_unit) . '</span> for ' . $creditCharge->days;
                $credit += ($creditCharge->amount * $quan);
            }
        }
        $html = array();
        $html['difference'] = $diffrence;
        $html['loading'] = $loading;
        $html['tax'] = $tax;
        $html['credit'] = $credit;
        $html['creditText'] = $creditText;
        return $html;
    }

    public function rfqBooking(Request $request){
        if (empty($request->q)){
            return 'quantity';
        }
        $id = $request->id;
        $type = $request->type;
        $product = Products::where('id', $id)->first();
        $basic = '';
        if ($product !== null){
            $basic = $product->purchase_price;
        }
        $tax = '';
        if ($product !== null){
            if ($product->tax !== ''){
                $tax = $product->tax;
            }
        }
        $q = isset($request->q) ? (int)$request->q : "1";
        $min = isset($request->min) ? (int)$request->min : 0;
        $variants = array();
        if ($q == 0 && $type == 'bysize'){
            $variants = (array)json_decode(stripcslashes($request->quantity));
            foreach ($variants as $var){
                $q = $q + $var;
            }
        }
        if ($min == 0){
            if ($q <= $min){
                return 'invalid';
            }
        }else{
            if ($q < $min){
                return 'minimum';
            }
        }
        if (Products::where('id', $id)->first()->quantity < $q){
            return 'unavailable';
        }
        $varData = array();
        if ($request->type == 'bysize' && $request->variants !== null){
            $variant_id = explode(',', $request->variants);
            $quantity = explode(',', $request->q);
            $unit = explode(',', $request->units);
            foreach ($variant_id as $i => $vi){
                $customer_id = institutional('id');
                $hub_id = $request->hub_id;
                $priceDetail = getInstitutionalPrice($id, $customer_id, $hub_id, $vi, false);
                $vari = ProductVariant::where('id', $vi)->first();
                $varData[] = array(
                    "id" => $vi,
                    "title" => variantTitle($vi) ,
                    "quantity" => $quantity[$i],
                    "unit" => $unit[$i],
                    "price_detail" => array(
                        array(
                            "basic" => $priceDetail['pprice'],
                            "loading" => $priceDetail['loading'],
                            "tax" => $priceDetail['tax'],
                            "size_difference" => $priceDetail['var'],
                            "total_amount" => $priceDetail['total']
                        )
                    )
                );
            }
        }else{
            $customer_id = institutional('id');
            $hub_id = $request->hub_id;
            $displayPrice = $request->price;
            $weightUnit = $request->unit;
            $priceDetail = getInstitutionalPrice($id, $customer_id, $hub_id, false, false);
            $varData[] = array(
                "id" => $product->id,
                "title" => $product->title,
                "quantity" => $q,
                "price" => $displayPrice,
                "unit" => $weightUnit,
                "price_detail" => array(
                    array(
                        "basic" => $priceDetail['pprice'],
                        "loading" => $priceDetail['loading'],
                        "tax" => $priceDetail['tax'],
                        "total_amount" => $priceDetail['total']
                    )
                )
            );
        }
        if ($request->type == 'bysize' && $request->units !== null){
            $udata = '{';
            for ($u = 0;$u < count($unit);$u++){
                $udata .= '"' . $variant_id[$u] . '"' . ':' . '"' . $unit[$u] . '",';
            }
            $udata .= '}';
        }else{
            $udata = '';
        }
        $data['variants'] = json_encode($varData);
        $data['product_id'] = $id;
        $data['brand_id'] = $id;
        if ($request->credit_id !== 'undefined') {
            $data['credit_charge_id'] = $request->credit_id;
        }
        $data['user_id'] = institutional('id');
        $data['direct_booking'] = '1';
        $data['hub_id'] = $request->hub_id;
        $data['method'] = $request->type;
        if (isset($request->without_variants)){
            $data['without_variants'] = $request->without_variants;
        }
        $data['unit'] = $udata;
        $in = DB::table('rfq_booking')->insert($data);
        if ($in){
            return 'success';
        }
    }

    public function advicesCat($cat)
    {
        $header = $this->institutionalHeader(false,false,false,true);
        $style = $this->style;
        if($cat) {
            $posts = AdviceCategory::where('parent',$cat)->orderBy('id','DESC')->get();
        }else {
            $posts = Blog::orderBy('time','DESC')->get();
        }
        $footer = $this->footer();
        $cats = AdviceCategory::where('parent','0')->get();
        $data['tp'] = url("/assets/");
        $dimg = AdviceCategory::where('slug', $cat)->first();
        $btotal = Blog::where('category',$cat)->get();
        $featured = Blog::where('featured','1')->limit('12')->get();
        $best = BestForAdvice::orderBy('id','DESC')->limit(2)->get();
        $link = Link::where('page', 'Advices')->first();
        return view('advices_cat')->with(compact('header','style','link','footer','dimg','featured','best','btotal','data','posts','cats'));
    }

    public function advices($cat = false)
    {

        $header = $this->institutionalHeader(false,false,false,true);
        $style = $this->style;
        if($cat) {
            $posts = Blog::where('slug',$cat)->orderBy('time','DESC')->get();
            $dimg = AdviceCategory::where('slug', $cat)->first();
        }else {
            $posts = Blog::orderByDesc('time')->get();
        }
        $footer = $this->institutionalFooter();

        $cats = AdviceCategory::where('parent','=','0')->get();
        $data['tp'] = url("/assets/");

        $btotal = Blog::where('category',$cat)->get();
        $link = Link::where('page', 'Advices')->first();
        return view('frontInstitutional/advices')->with(compact('header','style','link','footer','dimg','btotal','data','posts','cats'));
    }

    public function brand($cat = '')
    {
        if($cat == ''){
            abort(404);
        }
        $header = $this->institutionalHeader(translate('Products'),false,true);
        // Apply the product filters
        $tpbrands = '';
        $suggested = false;
        $top_brands = [];
        $br_id = DB::table('brand')->where('path', $cat)->first();
        //dd($br_id);
        $categ = DB::table('products')->where('brand_id', $br_id->id)->first();
        $price = array();
        $where = array();
        if (!empty($_GET['min']) && !empty($_GET['max'])){
            $where['price'] = "price BETWEEN '".escape($_GET['min'])."' AND '".escape($_GET['max'])."'";
        }
        if (!empty($_GET['search'])){
            $where['search'] = "(title LIKE '%".escape($_GET['search'])."%' OR text LIKE '%".escape($_GET['search'])."%')";
        }
        if (!empty($category)){
            $where['search'] = "(category = ".$categ->id.")";
        }
        if (!empty($catlink)){
            $where['search'] = "(category = ".$catlink.")";
        }
        if ($cat){
            $check = DB::select("SELECT COUNT(*) as count FROM category WHERE id = ".$categ->id);
            if (count($check) == 0){
                abort(404);
            }
            $category = DB::table('category')->where('id', $categ->category)->first();
            $categories[] = $category->id;
            if ($category->parent == 0){
                $childs = DB::select("SELECT * FROM category WHERE parent = ".$category->id." ORDER BY id DESC");
                foreach ($childs as $child){
                    $categories[] = $child->id;
                }
            }
            $where['cat'] = "category IN (".implode(',',$categories).")";
        }
        if($categ){
            $category = DB::table('category')->where('id', $categ->category)->first();
            $tpbrands =  '('.$category->brands.')';
            if($category->brands !== '') {
                $top_brands = DB::select("SELECT * FROM brand WHERE id in " . $tpbrands . " ORDER BY name ASC");
            }
            $sg = DB::select("SELECT product_id from featured_products where category_id = ".$categ->id);
            $sug_prods = '';
            $suggested = [];
            if(count($sg) > 0){
                $sug_prods = $sg[0]->product_id;
                $suggested = DB::select("SELECT * FROM products WHERE id in (".$sug_prods.")");
            }
            $product_categories = DB::select("SELECT * FROM category WHERE parent = ".$categ->id." ORDER BY name ASC");
        }
        $brands = DB::select("SELECT * FROM brand ORDER BY id DESC ");
        $filters = [];
        $fc = DB::table('category')->where('id', $categ->category)->first();
        $cfc = $fc->filter;
        if($cfc !== '') {
            $ccx = explode( ',', $cfc);
            foreach ($ccx as $cx) {
                $filters = DB::select("SELECT * FROM filter WHERE id = '$cx'");
            }
        } else {
            $filters = '';
        }
        $where = ($where) ? 'WHERE ' . implode(' AND ', $where) : '';
        $products = array();
        $products = DB::select("SELECT * FROM products WHERE brand_id = '$cat' ORDER BY id DESC ");
        $bran = DB::table('brand')->where('path', $cat)->first();
        $cats = DB::select("SELECT * FROM category WHERE parent = '0' ORDER BY id DESC ");
        $link = DB::table('link')->where('page', 'Brand')->first();
        if(institutional('user_type') == 'institutional'){
            $faqs = DB::select("SELECT * FROM product_faq WHERE  FIND_IN_SET (".$categ->id.", category) AND section='institutional' ORDER BY id DESC LIMIT 5");
        }else{
            $faqs = DB::select("SELECT * FROM product_faq WHERE  FIND_IN_SET (".$categ->id.", category) AND section='retail' ORDER BY id DESC LIMIT 5");
        }
        $ladvices = DB::select("SELECT * FROM blog WHERE pro_cat in (".$categ->category.") ORDER BY id DESC LIMIT 2");
        $tadvices = DB::select("SELECT * FROM blog WHERE pro_cat in (".$categ->category.") ORDER BY visits DESC LIMIT 2");
        $best = DB::select("SELECT * FROM best_for_category WHERE category = ".$categ->category." ORDER BY id DESC ");
        $footer = $this->footer();
        if(count($products) > 0){
            $p_min = DB::select("SELECT price FROM products $where ORDER BY price ASC LIMIT 1");
            if(count($p_min) > 0){
                $price['min'] = $p_min[0]->price;
            }else{
                $price['min'] = 0;
            }
            $p_max = DB::select("SELECT price FROM products $where ORDER BY price DESC LIMIT 1");
            if(count($p_max) > 0){
                $price['max'] = $p_max[0]->price;
            }else{
                $price['max'] = 0;
            }
        }else{
            $price['min'] = 0;
            $price['max'] = 0;
        }
        return view('brand')->with(compact('header','category','price','link','bran','cats','categ','br_id','faqs','ladvices','tadvices','best','brands','filters','products','footer', 'product_categories', 'top_brands', 'suggested'));
    }

    public function getProducts(Request $request){
        $brands = $request->brands;
        $rem_pre = $request->rem_pre;
        $ret= array();
        $html = '';
        $cat_id = null;
        $nb= 0;
        $preferences = null;
        if(customer('id') !== ''){
            $pre = Customer::find(institutional('id'))->preferences()->where('type', 'category')->first();
            if($pre === null){
                $preferences = null;
            }else{
                $preferences = explode(',', $pre->cache);
            }
        }else{
            if(isset($_POST['categories'])){
                setcookie('categories', $_POST['categories'], time() + (86400 * 30));
                return redirect(route('business.institutionalIndex'));
            }
            if(isset($_COOKIE['categories'])){
                $preferences = explode(',', $_COOKIE['categories']);
            }else{
                $preferences = null;
            }

        }
        $query = Category::where('popular_category', '>', $brands)->where('popular','1')->whereNotIn('id',implode(',',$preferences))->orderBy('popular_priority','ASC')->get();
        if($rem_pre && $preferences !== null){
            $offset = count($preferences) - $rem_pre;
            array_splice($preferences, 0, $offset);
            if(count($preferences)){
                $query = Category::whereIn('id',implode(',',$preferences))->orderBy('popular_priority','ASC')->get();
            }
        }
        $categoryforhomebrand = DB::select($query.' LIMIT 1');
        if(count($categoryforhomebrand)){
            $topbrand = $categoryforhomebrand[0];
            foreach($categoryforhomebrand as $pc){
                $cat_id = $pc->id;
                $nb = $pc->popular_priority;
                $html = '<div class="row">
                    <div class="col-md-12 main-title">
                        <div class="line-separator">
                            <h4 class="title">'.$pc->name.'</h4>
                            <a class="view-all-category" href="'.route('business.institutionalProducts',[$pc->path]).'">View All <i class="fa fa-caret-right"></i></a>
                        </div>
                        <div class="slide-6 theme-arrow product-m new">';
                        $pcp = Products::where('category',$pc->id)->orderBy('id','DESC')->take('6')->get();
                        foreach($pcp as $cp){
                            $html .='<div class="swiper-wrapper">
                                <article class="swiper-slide">
                                    <div class="product-layout">
                                        <div class="product-thumb ">
                                            <div class="product-inner">
                                                <div class="product-image">
                                                    <a href="'.route('business.institutionalProduct',[path($cp->title, $cp->id)]).'">
                                                        <img src="'.url('/assets/products/'.image_order($cp->images)).'" alt="'.translate($cp->title).'" style="height: 138px;">
                                                    </a>
                                                    <div class="action-links">
                                                        <a href="javascript:void(0);" onclick="Shopify.addItem(14650596786287, 1); return false;" class="ajax-spin-cart">
                                                            <i class="fa fa-shopping-cart"></i>
                                                        </a>
                                                        <a class="wishlist action-btn btn-wishlist" href="" title="Wishlist">
                                                            <i class="fa fa-heart"></i>
                                                        </a>

                                                    </div>
                                                </div>
                                                <!-- end of product-image -->
                                                <div class="product-caption">
                                                    <h4 class="product-name text-ellipsis"> <a href="'.route('business.institutionalProduct',[path($cp->title, $cp->id)]).'" title="'.translate($cp->title).'">'.translate($cp->title).'</a></h4>
                                                    <a href="'.route('business.institutionalProduct',[path($cp->title, $cp->id)]).'" class="btn btn-primary btn-sm">View Detail</a>
                                                </div>
                                                <!-- end of product-caption -->
                                            </div>
                                            <!-- end of product-inner -->
                                        </div>
                                        <!-- end of product-thumb -->
                                    </div>
                                </article>
                            </div>';
                        }
                        $html .='</div>
                    </div>
                </div>';
            }
        }
        $ret['brands'] = $brands;
        $ret['nb'] = $nb;
        if($rem_pre){
            $ret['rem_pre'] = (count($preferences) - 1) <= 0 ? 0 : (count($preferences) - 1);
        }else{
            $ret['rem_pre'] = 0;
        }
        $ret['html'] = $html;
        $ret['cat_id'] = $cat_id;
        $nextCat = $categoryforhomebrand = DB::select($query.' LIMIT 2');
        if(count($nextCat) == 2){
            $ret['collapse'] = false;
        }else{
            $ret['collapse'] = true;
        }
        return json_encode($ret);
    }

    public function services_category($services_category = '')
    {
        if ($services_category == ''){
            abort(404);
        }
        $header = $this->institutionalHeader(translate(explode('-',$services_category)[1]),false,true);
        // Apply the product filters

        $categoriescon = DB::select('SELECT COUNT(*) as count FROM category WHERE parent = 0 ORDER BY name ASC');
        if($categoriescon !== '') {
            $cats = DB::select('SELECT * FROM category WHERE parent = 0 ORDER BY name ASC');
        }else{
            $cats = 'Data Not Available';
        }
        $check = DB::select("SELECT COUNT(*) as count FROM services_category WHERE parent = '".explode('-',$services_category)[0]."'")[0];
        if ($check->count == 0){
            abort(404);
        }
        if (!empty($_GET['search']))
        {
            $where['search'] = "(name LIKE '%".escape($_GET['search'])."%' AND parent = '".explode('-',$services_category)[0]."')";
        }
        if (empty($_GET['search']))
        {
            $where['search'] = "(parent = '".explode('-',$services_category)[0]."')";
        }

        $where = ($where) ? 'WHERE ' . implode(' AND ', $where) : '';
        $services_categories = DB::select("SELECT * FROM services_category $where");
        $serv = DB::select("SELECT * FROM services_category WHERE parent = '".explode('-',$services_category)[0]."'");
        $how = DB::select("SELECT * FROM s_how_it_works WHERE FIND_IN_SET('".explode('-',$services_category)[0]."', cat)");
        $icat = DB::select("SELECT * FROM services_category_image WHERE service_category_id = '".explode('-',$services_category)[0]."'");
        $link = DB::table('link')->where('page', 'Services Category')->first();
        $footer = $this->institutionalFooter();
        return view('services_category')->with(compact('header','cats','serv','link','services_categories','services_category', 'how','footer', 'icat'));
    }

    public function layout_plans_cat($slug = '')
    {
        if($slug == ''){
            abort(404);
        }
        $cat = [];
        $plan_cat = false;
        $plan_cat = DB::select("SELECT * FROM design_category WHERE slug = '".$slug."'");
        if(count($plan_cat) <= 0){
            abort(404);
        }else{
            $cat = $plan_cat[0];
        }

        $header = $this->institutionalHeader(translate($cat->name),false,true);
        // Apply the product filters
        $design_categorycon = DB::select('SELECT COUNT(*) as count FROM design_category WHERE parent = 0 ORDER BY name ASC');
        if($design_categorycon !== '') {
            $cats = DB::select('SELECT * FROM design_category WHERE parent = 0 ORDER BY name ASC');
        }else {
            $cats = 'Data Not Available';
        }
        $banner = DB::select("SELECT * FROM design_cat_banner");
        $layout_plans = DB::select('SELECT * FROM design_category Where parent = '.$cat->id.' ORDER BY `name` ASC');
        $featured = DB::select("SELECT * FROM layout_plans Where featured = '1' LIMIT 12");
        $best = DB::select("SELECT * FROM best_for_layout ORDER BY id DESC LIMIT 2");

        $link = DB::table('link')->where('page', 'Layout Plan Category')->first();
        $footer = $this->institutionalFooter();
        return view('layout_plan_cat')->with(compact('header','cat','link','cats', 'banner', 'best', 'featured', 'layout_plans','footer'));
    }

    public function photos_cat($slug = '')
    {
        $cat = [];
        $plan_cat = false;
        if ($slug == ''){
            return redirect('/');
        }
        $plan_cat = DB::select("SELECT * FROM design_category WHERE slug = '".$slug."'");
        if(count($plan_cat) <= 0){
            abort(404);
        }else{
            $cat = $plan_cat[0];
        }
        $header = $this->institutionalHeader(translate($cat->name),false,true);
        $galleries = DB::select("SELECT * FROM design_category where parent = ".$cat->id." ORDER BY name ASC");
        $featured = DB::select("SELECT * FROM photo_gallery where featured = '1' ORDER BY id ASC LIMIT 12");
        $best = DB::select("SELECT * FROM best_for_photo ORDER BY id ASC LIMIT 2");
        // Apply the product filters
        $design_categorycon = DB::select('SELECT COUNT(*) as count FROM design_category WHERE parent = 0 ORDER BY name ASC');
        if($design_categorycon !== '') {
            $cats = DB::select('SELECT * FROM design_category WHERE parent = 0 ORDER BY name ASC');
        }else {
            $cats = 'Data Not Available';
        }
        $link = DB::table('link')->where('page', 'Photos Category')->first();
        $footer = $this->institutionalFooter();
        return view('photos_cat')->with(compact('header', 'cat', 'link','cats', 'featured','best', 'galleries', 'footer'));
    }

    public function review(){
        if (isset($_POST['review'])){
            $name = (int)$_POST['name'];
            $rating = (int)$_POST['rating'];
            $review = escape(htmlspecialchars($_POST['review']));
            $product = (int)$_POST['product'];
            DB::insert("INSERT INTO reviews (name,rating,review,product,time,active) VALUE ('$name','$rating','$review','$product','".time()."','0')");
            return back();
        }
    }

    public function createproject(){
        if (isset($_POST['submit'])){

            //dd('hello');
            // escape review details and insert them into the database
            $title = escape(htmlspecialchars($_POST['title']));
            $user_id = (int)$_POST['user_id'];
            if (request()->file('image')) {
                // Upload the downloadable file to product downloads directory
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'projects/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
            }
            DB::insert("INSERT INTO user_project (title,user_id,image) VALUE ('$title','$user_id','$file')");
            return back();
        }
    }

    public function sendAsr(Request $request){
        if(isset($request->message)){
            $sr = new SpecialRequirements();
            $sr->message = $request->message;
            $sr->product_id = $request->pid;
            $sr->user_id = customer('id');
            $sr->save();
            if (request()->file('image')) {
                // Upload the downloadable file to product downloads directory
                $name1 = request()->file('image')->getClientOriginalName();
                //dd($name);
                $file = md5(time()).'1.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR;
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);

                $sr->file = $file;
                $sr->save();
            }
            return back()->with("success", ["Enquiry sent !"]);
        }
        return back()->withErrors('msg', ['Oops! Something went wrong. Try again later.']);
    }
    public function sendRfr(Request $request){
        if(isset($request->msg)){
            $sr = new RateRequests();
            $sr->message = $request->msg;
            $sr->product_id = $request->pid;
            $sr->user_id = customer('id');
            if(isset($request->variants)){
                $sr->variants = json_encode($request->variants);
            }
            $sr->save();
            if (request()->file('image')) {
                // Upload the downloadable file to product downloads directory
                $name1 = request()->file('image')->getClientOriginalName();
                //dd($name);
                $file = md5(time()).'1.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR;
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);

                $sr->file = $file;
                $sr->save();
            }
            return back()->with("successx", ["Request sent !"]);
        }
        return back()->withErrors('msgx', ['Oops! Something went wrong. Try again later.']);
    }
}