<?php

namespace App\Http\Controllers;

use App\AdviceCategory;
use App\Browser;
use App\Category;
use App\Config;
use App\Country;
use App\Currency;
use App\Customer;
use App\CustomerCache;
use App\DefaultImage;
use App\DesignCategory;
use App\Footer;
use App\Lang;
use App\ManufacturingHubs;
use App\MegaMenuImage;
use App\Menu;
use App\Os;
use App\Payments;
use App\RateRequests;
use App\Referrer;
use App\RegisterImage;
use App\ServiceCategory;
use App\SpecialRequirements;
use App\Style;
use App\Tracking;
use App\Visitor;
use Illuminate\Auth\Authenticatable;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use SebastianBergmann\CodeCoverage\Node\Directory;
use Image;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

//use Auth;

class HomeController extends FrontendBaseController
{

    public function welcome(){
        $data['cfg'] = $this->cfg;
        Session::forget('aakar360.visitor_type');
        Session::save();
        return view('welcome')->with(compact('data'));
    }
    public function change_type($slug){
        Session::put('aakar360.visitor_type', $slug);
        if($slug == 'individual'){
            return redirect(route('retail.retailIndex'));
        }elseif($slug == 'institutional'){
            return redirect(route('business.institutionalIndex'));
        }
    }

    public function register($slug,$title = false,$desc = false,$page = false,$landing = false,$bg = false)
    {
        $all_img = RegisterImage::get();
        $seller_img='';
        $professional_img = '';
        $cusotmer_img = '';
        $institutional_img = '';
        foreach($all_img as $img){
            if($img->user_type=='Seller'){
                $seller_img = $img->image;
            }
            if($img->user_type=='Professional'){
                $professional_img = $img->image;
            }
            if($img->user_type=='Customer'){
                $cusotmer_img = $img->image;
            }
            if($img->user_type=='Institutional'){
                $institutional_img = $img->image;
            }
            if($img->user_type=='Institutional'){
                $institutional_img = $img->image;
            }
        }
        $data = array();
        if($slug=='institutional'){
            $data['img'] = $institutional_img;
            $data['rtitle'] = 'Institutional';
            $data['type'] = 'institutional';
        }
        else if($slug=='seller'){
            $data['img'] = $seller_img;
            $data['rtitle'] = 'Manufacture / Supplier';
            $data['type'] = 'seller';
        }else if($slug=='customer'){
            $data['img'] = $cusotmer_img;
            $data['rtitle'] = 'Customer';
            $data['type'] = 'customer';
        }else if($slug=='professional'){
            $data['img'] = $professional_img;
            $data['rtitle'] = 'Professional';
            $data['type'] = 'professional';
        }else if($slug=='dealer'){
            $data['img'] = $institutional_img;
            $data['rtitle'] = 'Dealer';
            $data['type'] = 'dealer';
        }
        if ($this->cfg->registration == 0 || session('customer') != '') {
            abort('404');
        }
        if(isset($_POST['register'])){
            if (!empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['confirm_password']) && !empty($_POST['user_type'] && !empty($_POST['mobile']))){
                $name = escape(htmlspecialchars($_POST['name']));
                $email = escape(htmlspecialchars($_POST['email']));
                $user_type = escape(htmlspecialchars($_POST['user_type']));
                $password = md5($_POST['password']);
                $mobile = escape(htmlspecialchars($_POST['mobile']));
                if($_POST['password'] != $_POST['confirm_password']){
                    $data['error'] = "Password mismatch!";
                }else if(Customer::where('email',$email)->count() > 0) {
                    $data['error'] = 'This email is already registered!';
                } else{
                    $customer = new Customer();
                    $customer->name = $name;
                    $customer->email = $email;
                    $customer->password = $password;
                    $customer->password = $password;
                    $customer->user_type = $user_type;
                    $customer->sid = md5(microtime());
                    $customer->save();
                    $data['error'] = 'Register Successfully ! Please Wait For Approval.';
                }
            } else {
                $data['error'] = 'All fields are required !';
            }
        }
        $data['header'] = $this->header(translate('Registration'),false,true);
        $data['dimg'] = DefaultImage::where('page','Register')->first();
        $data['footer'] = $this->footer();
        $data['title'] = ($title) ? translate($title).' | '.translate($this->cfg->name) : translate($this->cfg->name);
        $data['desc'] = ($desc) ? $desc : $this->cfg->desc;
        $data['keywords'] = $this->cfg->key;
        $data['tp'] = url("/themes/".$this->cfg->theme);
        $data['cfg'] = $this->cfg;
        return view('register')->with('data',$data);
    }

    public function changePassword()
    {
        if(isset($_POST['login'])) {
            if (!empty($_POST['oldpassword']) && !empty($_POST['confirm_password']) && !empty($_POST['password'])) {
                // Escape the user entries and insert them to database
                $password = md5($_POST['password']);
                $opassword = md5($_POST['oldpassword']);
                $old = Customer::where('password',$opassword)->first();
                if (!empty($old)) {
                    if ($_POST['password'] != $_POST['confirm_password']) {
                        $error = '<div class="alert alert-warning">Password Missmatch!</div>';
                    } else {
                        $visitor = Customer::where('id',$old->id);
                        $visitor->password = $password;
                        $visitor->save();
                        $error = '<div class="alert alert-success">Password Successfully Changed</div>';
                    }
                }
            } else {
                $error = '<div class="alert alert-warning">All fields are required !</div>';
            }
        }
        $header = $this->header(translate('Change Password'),false,true);
        $dimg = DefaultImage::where('page','Change Password')->first();
        $footer = $this->footer();
        return view('change-password')->with(compact('header','error','dimg','footer'));
    }

    public function login($title = false,$desc = false,$page = false,$landing = false,$bg = false, Request $request)
    {
        if ($this->cfg->registration == 0) {
            abort('404');
        }
        $error = '';
        if(isset($_POST['login'])){
            // Check email and password and redirect to the account
            $email = escape($_POST['email']);
            $pass = md5($_POST['password']);
            if (strlen($_POST["password"]) <= 8) {
                $error = "Your Password Must Contain At Least 8 Characters!";
            }
            else if(!preg_match("#[0-9]+#",$_POST["password"])) {
                $error = "Your Password Must Contain At Least 1 Number!";
            }
            else if(!preg_match("#[A-Z]+#",$_POST["password"])) {
                $error = "Your Password Must Contain At Least 1 Capital Letter!";
            }
            else if(!preg_match("#[a-z]+#",$_POST["password"])) {
                $data['error'] = "Your Password Must Contain At Least 1 Lowercase Letter!";
            }else if(empty($email) or empty($pass)){
                $error = 'All fields are required';
            } else {
                $cust = Customer::where('email',$email)->where('password',$pass)->first();

                if(!empty($cust->id)) {
                    $secure_id = md5(microtime());
                    $cust->sid = $secure_id;
                    $cust->save();
                    $preferences = '';
                    if(isset($_COOKIE['cities'])){
                        $preferences = $_COOKIE['cities'];
                    }else{
                        $preferences = null;
                    }

                    Cookie::forever('cities', $preferences);

                    if($cust->user_type == 'individual') {
                        request()->session()->put('customer', $secure_id);
                        if(session('redirect') !== null){
                            return redirect(session('redirect'));
                        }else {
                            return redirect(route('retail.retailIndex'));
                        }
                    }elseif ($cust->user_type == 'seller'){
                        session(['customer' => $secure_id]);
                        if(session('redirect') !== null){
                            return redirect(session('redirect'));
                        }else {
                            return redirect(route('retail.retailSellerAccount'));
                        }
                    }elseif ($cust->user_type == 'professional'){
                        session(['customer' => $secure_id]);
                        if(session('redirect') !== null){
                            return redirect(session('redirect'));
                        }else {
                            return redirect(route('retail.retailProfessionalAccount'));
                        }
                    }elseif ($cust->user_type == 'institutional'){
                        session(['institutional' => $secure_id]);
                        return redirect(route('business.institutionalIndex'));
                    }
                } else {
                    $error = 'Wrong email or password';
                }
            }
        }else{
            if($request->server('HTTP_REFERER') !== null){
                $app_url =  env('APP_URL') !== null ? env('APP_URL') : 'http://localhost/aakar360';
                if(strpos($request->server('HTTP_REFERER'), $app_url) !== false && $request->server('HTTP_REFERER') !== url('/')) {
                    session(['redirect' => $request->server('HTTP_REFERER')]);
                }else{
                    session(['redirect' => null]);
                }
            }
        }
        $header = $this->retailHeader(translate('Login'),false,true);
        $dimg = DefaultImage::where('page', 'Login')->first();
        $footer = $this->footer();
        $data['title'] = ($title) ? translate($title).' | '.translate($this->cfg->name) : translate($this->cfg->name);
        $data['desc'] = ($desc) ? $desc : $this->cfg->desc;
        $data['keywords'] = $this->cfg->key;
        $data['tp'] = url("/themes/".$this->cfg->theme);
        $data['cfg'] = $this->cfg;

        return view('login')->with(compact('header','error','dimg','footer','data'))->render();
    }

    public function institutionalLogin($title = false,$desc = false,$page = false,$landing = false,$bg = false, Request $request)
    {
        if ($this->cfg->registration == 0) {
            abort('404');
        }
        $error = '';
        if(isset($_POST['login'])){
            // Check email and password and redirect to the account
            $email = escape($_POST['email']);
            $pass = md5($_POST['password']);
            if (strlen($_POST["password"]) <= 8) {
                $error = "Your Password Must Contain At Least 8 Characters!";
            }
            else if(!preg_match("#[0-9]+#",$_POST["password"])) {
                $error = "Your Password Must Contain At Least 1 Number!";
            }
            else if(!preg_match("#[A-Z]+#",$_POST["password"])) {
                $error = "Your Password Must Contain At Least 1 Capital Letter!";
            }
            else if(!preg_match("#[a-z]+#",$_POST["password"])) {
                $data['error'] = "Your Password Must Contain At Least 1 Lowercase Letter!";
            }else if(empty($email) or empty($pass)){
                $error = 'All fields are required';
            } else {
                if(Customer::where('email',$email)->where('password',$pass)->count() > 0) {
                    $cust = Customer::where('email',$email)->where('password',$pass)->first();
                    $secure_id = md5(microtime());
                    if($cust->user_type == 'institutional'){
                        if($cust->status == 0){
                            $error = 'Wait For Approval';
                        }
                        else{
                            $cus = Customer::where('email',$email)->first();
                            $cus->sid = $secure_id;
                            $cus->save();
                            Session::put('institutional',$secure_id);
//                            if(session('redirect') !== null){
//                                return redirect(session('redirect'));
//                            }
                            return redirect(route('business.institutionalIndex'));
                        }
                    }
                } else {
                    $error = 'Wrong email or password';
                }

            }
        }else{
            if($request->server('HTTP_REFERER') !== null){
                $app_url =  env('APP_URL') !== null ? env('APP_URL') : 'http://localhost/aakar360';
                if(strpos($request->server('HTTP_REFERER'), $app_url) !== false && $request->server('HTTP_REFERER') !== url('/')) {
                    session(['redirect' => $request->server('HTTP_REFERER')]);
                }else{
                    session(['redirect' => null]);
                }
            }
        }
        $header = $this->retailHeader(translate('Login'),false,true);
        $dimg = DefaultImage::where('page', 'Login')->first();
        $footer = $this->footer();

        $data['title'] = ($title) ? translate($title).' | '.translate($this->cfg->name) : translate($this->cfg->name);
        $data['desc'] = ($desc) ? $desc : $this->cfg->desc;
        $data['keywords'] = $this->cfg->key;
        $data['tp'] = url("/themes/".$this->cfg->theme);
        $data['cfg'] = $this->cfg;

        return view('frontInstitutional/login')->with(compact('header','error','dimg','footer','data'))->render();
    }

    public function loginModal()
    {
        if ($this->cfg->registration == 0) {
            abort('404');
        }
        if(isset($_POST['login'])){
            // Check email and password and redirect to the account
            $email = escape($_POST['email']);
            $pass = md5($_POST['password']);
            if(empty($email) or empty($pass)){
                $error = 'All fields are required';
                request()->session()->put('error', $error);
//                return redirect('sub_service/'.path($service_id).'');
            } else {
                if(Customer::where('email',$email)->where('password',$pass)->count() > 0) {
                    // Generate a new secure ID for this session and redirect to dashboard
                    $secure_id = md5(microtime());
                    $cus = Customer::where('email',$email)->first();
                    $cus->sid = $secure_id;
                    $cus->save();
                    session(['customer' => $secure_id]);
                    return back();
                } else {
                    $error = 'Wrong email or password';
                    request()->session()->put('error', $error);
                    return back();
                }
            }
        }
    }

}