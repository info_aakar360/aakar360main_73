<?php

namespace App\Http\Controllers\CRM;

use App\Http\Controllers\Crm;
use App\Transportor;
use App\TruckLoad;
use App\User;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Toastr;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class TruckLoadController extends Crm
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tp = url("/assets/crm/");
        $header = $this ->header('Crm', 'index');
        $footer = $this ->footer();
        $title = 'Trcuk Load';
        $user = User::where('secure', Session::get('crm'))->first();
        if(!checkRole($user->u_id,"truck_load")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        return view('crm.truck_load-master')->with(compact('header','tp', 'footer', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tp = url("/assets/crm/");
        $header = $this ->header('Crm', 'index');
        $footer = $this ->footer();
        $title = 'Create Truck Load';
        return view('crm.create_truck_load')->with(compact('header','tp', 'footer', 'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $truckload = new TruckLoad();
        $truckload->load = $request->load;
        $truckload->unit = '';
        $truckload->save();
        Toastr::success('Truck Load Added Successfully!!','Success');
        Redirect::back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $truckload = TruckLoad::find($id);
        $tp = url("/assets/crm/");
        $header = $this ->header('Crm', 'index');
        $footer = $this ->footer();
        $title = 'Edit Truck Load';
        return view('crm.edit_truck_load')->with(compact('header','tp', 'footer', 'title','truckload'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $truckload = Transportor::find($id);
        $truckload->load = $request->load;
        $truckload->unit = '';
        $truckload->save();\
        Toastr::success('Truck Load Updated Successfully!!','Success');
        Redirect::back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $truckload = TruckLoad::find($id);
        $truckload->delete();
        Toastr::success('Truck Load Deleted Successfully!!','Success');
        Redirect::back();
    }

    public function getTruckLoadData(){
        $records = TruckLoad::all();
        return DataTables::of($records)
            ->editColumn('load', function($row){
                return $row->load;
            })
            ->addColumn('action', function($row){
                $html ='<button value="'.$row->id.'" data-id="'.$row->id.'" class="btn myred waves-light delete_load" style="padding:0 10px;"><i class="material-icons">delete</i></button><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="'.route('truckload.edit',[$row->id]).'"><i class="material-icons">edit</i></a>';
                return $html;
            })
            ->make(true);
    }
}
