<?php

namespace App\Http\Controllers\CRM;

use App\CrmUserRole;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Crm;
use App\SalesPipeline;
use Illuminate\Http\Request;

class SalesPipelineController extends Crm
{
    public function SalesPipeline(){
        $notices = '';
        $salespipelines = SalesPipeline::all();
        $cfg = $this -> cfg;
        $tp = url("/assets/crm/");
        $header = $this -> header('Crm', 'index');
        $footer = $this -> footer();
        $title = 'Sales Pipelines';
        $roles = CrmUserRole::all();
        return view('crm/sales-pipeline')-> with(compact('header', 'cfg', 'tp', 'footer', 'title', 'notices', 'roles','salespipelines'));
    }
}
