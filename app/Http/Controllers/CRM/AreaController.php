<?php

namespace App\Http\Controllers\CRM;

use App\Blocks;
use App\Category;
use App\CrmCustomer;
use App\District;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Crm;
use App\ProductVariant;
use App\State;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AreaController extends Crm
{
    public function MarketingData(){
        $user = User::where('secure', Session::get('crm'))->first();
        if(!checkRole($user->u_id,"ar_data")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Marketing Data';
        $selected_category = '';
        $selected_variant= '';
        $category = Category::all();
        $all_variants = ProductVariant::all();
        $statesx = State::where('country_id','=',101)->get();
        $report_data = false;
        $districtsx = false;
        $pcc = '0.00';
        $cat =false;
        if(isset($_GET['category']) && isset($_GET['state'])){
            $cat = Category::where('id', $_GET['category'])->first();
            $states = State::whereIn('id', $_GET['state'])->get();
            $districtsx = District::whereIn('state_id', $_GET['state'])->get();
            foreach($states as $state){
                if(isset($_GET['district'])){
                    $districts = District::whereIn('id', $_GET['district'])->where('state_id', $state->id)->get();
                }else{
                    $districts = District::where('state_id', $state->id)->get();
                }

                foreach($districts as $district){
                    $blocks = Blocks::where('district_id', $district->id)->get();
                    $pop = 0;
                    foreach($blocks as $block){
                        $pop += ($block->population == 0 || !is_numeric($block->population)) ? 0 : $block->population;
                    }
                    //$pop = ($district->population === null || !is_numeric($district->population)) ? 0 : $district->population;
                    $pcc = ($cat->consumption_ratio === null || !is_numeric($cat->consumption_ratio)) ? 0 : $cat->consumption_ratio;
                    $contacted = CrmCustomer::where('district', $district->id)->where('class', 2)->count();
                    $interested = CrmCustomer::where('district', $district->id)->where('class', 3)->count();
                    $converted = CrmCustomer::where('district', $district->id)->where('class', 4)->count();
                    $report_data[] = array(
                        'state' => $state->name,
                        'district' => $district->name,
                        'population' => $pop,
                        'consumption' => ($pop*$pcc),
                        'contacted' => $contacted,
                        'interested' => $interested,
                        'converted' => $converted
                    );
                    /*usort($report_data, function($a, $b) {
                        return $a['popuation'] - $b['population'];
                    });*/
                }
            }
        }

        return view('crm/marketing-data')->with(compact('cfg','tp','header','footer','districtsx','title','selected_category','selected_variant','category','all_variants','statesx', 'report_data', 'pcc', 'cat'))->render();
    }
}
