<?php

namespace App\Http\Controllers\CRM;

use App\Blocks;
use App\ContactDesignation;
use App\CrmAssociation;
use App\CrmCustomer;
use App\CrmCustomerProject;
use App\CrmMasterQuickEntry;
use App\CrmProjectSubtype;
use App\CrmVisits;
use App\CustomerCategory;
use App\CustomerClass;
use App\CustomerDesignation;
use App\CustomerType;
use App\District;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Crm;
use App\Locality;
use App\Otp;
use App\State;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CustomerMasterController extends Crm
{

    public function Customers(Request  $request)
    {

        $names = '';
        $emails = '';
        $contacts = '';
        $descs = '';
        $landlines = '';
        $crm_projects = '';
        $user = User::where('secure', Session::get('crm'))->first();
        $logname = $user->u_name;
        $user_name= $user->u_id;
        $luser = $user;
        if(!checkRole($user->u_id,"mas_cst")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';
        $pcid = '';
        $price = '';
        $selected_locality = '';
        $selected_block = '';
        $selected_district=array();
        $localities = array();
        $blocks = array();
        if(isset($_GET['edit'])){
            $cid = $_GET['edit'];
            $selected_district = CrmCustomer::leftJoin('district', 'state_id', '=', 'crm_customers.state')
                ->where('crm_customers.id', '=', $cid)
                ->select('district.name as d_name','district.id as did')
                ->get();
            $selected_block = CrmCustomer::leftJoin('blocks', 'district_id', '=', 'crm_customers.district')
                ->where('crm_customers.id', '=', $cid)
                ->select('blocks.block as b_name','blocks.id as bid')
                ->get();

            $selected_locality =CrmCustomer::leftJoin('locality', 'block_id', '=', 'crm_customers.blocks')
                ->where('crm_customers.id', '=', $cid)
                ->select('locality.locality as l_name','locality.id as lid')
                ->get();
            $localities = Locality::whereIn('block_id', Arr::pluck($selected_block, 'bid'))->get();
            $blocks = Blocks::whereIn('district_id', Arr::pluck($selected_district, 'did'))->get();
            $crm_projects = CrmCustomerProject::where('crm_customer_id',$cid)->first();

        }

        if(isset($_POST['add'])){
            if(isset($_POST['pcid'])){
                $pcid = implode(',',$_POST['pcid']);
                $price = implode(',',$_POST['price']);
            }
            $customer_category = $_POST['customer_category'];
            $cid = $_POST['cid'];
            $name = $_POST['name'];
            $proprieter_name = $_POST['proprieter_name'];
            $type = !empty($_POST['type'])?$_POST['type']:'0';
            $desg = $_POST['desg'];
            $landline = $_POST['landline'];
            $postal_address = !empty($_POST['postal_address'])?$_POST['postal_address']:'0';
            $email = $_POST['email'];
            $contact_no = $_POST['contact_no'];
            $district = $_POST['district'];
            $block = !empty($_POST['block'])?$_POST['block']:'0';
            $locality = !empty($_POST['locality'])?$_POST['locality']:'0';
            $state = $_POST['state'];
            $msg_active = $_POST['msg_active'];
            $class = $_POST['msg_class'];
            $reason = $_POST['reason_value'];
            $lat = !empty($_POST['lat'])?$_POST['lat']:'0';
            $lng = !empty($_POST['lng'])?$_POST['lng']:'0';
            $payment_days = $_POST['payment_days'];
            $user = '';
            if(isset($_POST['user'])) {
                $user = implode(',', $_POST['user']);
            }
            $loguser = '';
            if(isset($_POST['loguser'])) {
                $loguser = implode(',', $_POST['loguser']);
            }
            $assoc = '';
            if(isset($_POST['association'])) {
                $assoc = implode(',', $_POST['association']);
            }
            if(isset($_POST['names'])) {
                $names= serialize($_POST['names']);
            }if(isset($_POST['emails'])) {
                $emails= serialize($_POST['emails']);
            }if(isset($_POST['contacts'])) {
                $contacts= serialize($_POST['contacts']);
            }if(isset($_POST['descs'])) {
                $descs= serialize($_POST['descs']);
            }if(isset($_POST['landlines'])) {
                $landlines= serialize($_POST['landlines']);
            }
            $refered_by = isset($_POST['listcust'])?implode(',',$_POST['listcust']):false;
            $getcrmtype = isset($_POST['crm_type'])?implode(',',$_POST['crm_type']) : '1';
            DB::insert("INSERT INTO `crm_customers`(`customer_category`,`product_category`, `price`, `cid`,`managed_by`,`name`, `proprieter_name`,  `type`,`class`, `postal_address`, `email`, `contact_no`, `blocks`,`locality`, `state`, `msg_active`, `lat`, `lng`, `unqualified_reason`,`payment_days`,`crm_association`,`designation`,`landline`,`district`,`sec_names`,`sec_emails`,`sec_contact`,`sec_desc`,`sec_landline`,`referred_by`,`crm_type`,`edited_by`,`user_id`) VALUES ('$customer_category','$pcid','$price','$cid','$user','$name','$proprieter_name','$type','$class','$postal_address','$email','$contact_no','$block','$locality','$state','$msg_active', '$lat', '$lng','$reason','$payment_days','$assoc','$desg','$landline','$district','$names','$emails','$contacts','$descs','$landlines','$refered_by','$getcrmtype','$logname','$loguser')");
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Customer Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            $id = DB::getPdo()->lastInsertId();

            $crm_customer_project = new CrmCustomerProject();
            $crm_customer_project->crm_customer_id = $id;
            $crm_customer_project->project_name = '';
            $crm_customer_project->sqft = '';
            $crm_customer_project->project_status = '';
            $crm_customer_project->project_address = '';
            $crm_customer_project->person_name = '';
            $crm_customer_project->person_designation = '';
            $crm_customer_project->description = '';
            $crm_customer_project->image = '';
            $crm_customer_project->edited_by = '';
            $crm_customer_project->project_type = $_POST['ptype'];
            $crm_customer_project->project_subtype = isset($_POST['pstype'])?implode(',',$_POST['pstype']) : '';
            $crm_customer_project->state = $_POST['state'];
            $crm_customer_project->district = $_POST['district'];
            $crm_customer_project->block = !empty($_POST['block'])?$_POST['block']:'0';
            $crm_customer_project->locality = !empty($_POST['locality'])?$_POST['locality']:'0';
            $crm_customer_project->crm_type = isset($_POST['crm_type'])?implode(',',$_POST['crm_type']) : '1';
            $crm_customer_project->person_phone = $_POST['contact_no'];
            $crm_customer_project->save();

            if($_POST['quick']){
                $qdata = CrmMasterQuickEntry::where('id', $_POST['quick'])->first();
                $images = explode('<**>', $qdata->image);
                $oldPath = base_path().'/assets/crm/images/user/';
                $newPath = base_path().'/assets/crm/images/visits/';
                foreach($images as $image){
                    try {
                        rename($oldPath . $image, $newPath . $image);
                    }catch(\Exception $ex){

                    }
                }

                DB::insert("INSERT INTO crm_visits(`crm_customer_id`, `name`, `phone_no`, `address`, `select_date`, `description`, `user_id`, `image`, `customer_status`) VALUE ('".$id."', '".$name."', '".$contact_no."', '".$postal_address."', '".date_format(date_create($qdata->created_at), 'Y-m-d')."', '".$qdata->discription."', '".$user."', '".implode(',', $images)."', '".$class."')");
                CrmMasterQuickEntry::where('id', $_POST['quick'])->delete();
            }
            if($_POST['goto']){

                return redirect()->to('crm/customer-profile?cid='.$id);
            }
        }
        if(isset($_POST['edit'])){
            //dd($_POST);
            if(isset($_POST['pcid'])){
                $pcid = implode(',',$_POST['pcid']);
                $price = json_encode($_POST['price']);
            }
            $strict = 0;
            if(isset($_POST['strict'])){
                $strict = 1;
            }

            $cidd = $_POST['cidd'];

            $customer_category = $_POST['customer_category'];
            $cid = $_POST['cid'];
            $name = $_POST['name'];
            $proprieter_name = $_POST['proprieter_name'];
            $type = !empty($_POST['type'])?$_POST['type']:'0';
            $postal_address = !empty($_POST['postal_address'])?$_POST['postal_address']:'';
            $email = $_POST['email'];
            $contact_no = $_POST['contact_no'];
            $desg = $_POST['desg'];
            $landline = $_POST['landline'];
            $district = $_POST['district'];
            $block = !empty($_POST['block'])?$_POST['block']:'0';
            $locality = !empty($_POST['locality'])?$_POST['locality']:'0';
            $state = $_POST['state'];
            $msg_active = $_POST['msg_active'];
            $class = $_POST['msg_class'];
            $reason = $_POST['reason_value'];
            $lat = !empty($_POST['lat'])?$_POST['lat']:'0';
            $lng = !empty($_POST['lng'])?$_POST['lng']:'0';
            $payment_days =$_POST['payment_days'];

            $customer = CrmCustomer::find($cidd);
            if($class >= 5){
                if($customer->class != $class)
                    $msg_active = 1;
            }
            $user = '';
            if(isset($_POST['user'])) {
                $user = implode(',', $_POST['user']);
            }
            $loguser = '';
            if(isset($_POST['loguser'])) {
                $loguser = implode(',', $_POST['loguser']);
            }
            $assoc = '';
            if(isset($_POST['association'])) {
                $assoc = implode(',', $_POST['association']);
            }
            if(isset($_POST['names'])) {
                $names= serialize($_POST['names']);
            }if(isset($_POST['emails'])) {
                $emails= serialize($_POST['emails']);
            }if(isset($_POST['contacts'])) {
                $contacts= serialize($_POST['contacts']);
            }if(isset($_POST['descs'])) {
                $descs= serialize($_POST['descs']);
            }if(isset($_POST['landlines'])) {
                $landlines= serialize($_POST['landlines']);
            }
            $getcrmtype = isset($_POST['crm_type'])?implode(',',$_POST['crm_type']) : '1';
            DB::update("UPDATE `crm_customers` SET `customer_category`='$customer_category',`product_category` = '$pcid',`price` = '$price',`strict_mode`='$strict', `cid`='$cid',`assign_users` = '$user',`name`='$name',`proprieter_name`='$proprieter_name',`type`='$type',`class`='$class',`postal_address`='$postal_address',`email`='$email',`contact_no`='$contact_no',`designation`='$desg',`landline`='$landline',`district`='$district',`blocks`='$block', `locality`='$locality', `sec_names`='$names', `sec_emails`='$emails', `sec_contact`='$contacts', `sec_desc`='$descs', `sec_landline`='$landlines',`state`='$state',`msg_active`='$msg_active', `lat`='$lat', `lng`='$lng',`unqualified_reason`='$reason',`payment_days`='$payment_days',`crm_association`='$assoc',`crm_type`='$getcrmtype',`edited_by`='$logname' ,`user_id`='$loguser',`user_id`='$loguser' WHERE id = '$cidd'");

            $crm_customer_project = CrmCustomerProject::where('crm_customer_id',$cidd)->first();
           if(!empty($crm_customer_project)){
               $crm_customer_project->crm_customer_id = $cidd;
               $crm_customer_project->project_name = '';
               $crm_customer_project->sqft = '';
               $crm_customer_project->project_status = '';
               $crm_customer_project->project_address = '';
               $crm_customer_project->person_name = '';
               $crm_customer_project->person_designation = '';
               $crm_customer_project->description = '';
               $crm_customer_project->image = '';
               $crm_customer_project->edited_by = '';
               $crm_customer_project->project_type = $_POST['ptype'];
               $crm_customer_project->project_subtype = isset($_POST['pstype'])?implode(',',$_POST['pstype']) : '';
               $crm_customer_project->state = $_POST['state'];
               $crm_customer_project->district = $_POST['district'];
               $crm_customer_project->block = !empty($_POST['block'])?$_POST['block']:'0';
               $crm_customer_project->locality = !empty($_POST['locality'])?$_POST['locality']:'0';
               $crm_customer_project->crm_type = isset($_POST['crm_type'])?implode(',',$_POST['crm_type']) : '1';
               $crm_customer_project->person_phone = $_POST['contact_no'];
               $crm_customer_project->save();
           }else{
               $crm_cust_project    =   new CrmCustomerProject();
               $crm_cust_project->crm_customer_id = $cidd;
               $crm_cust_project->project_name = '';
               $crm_cust_project->sqft = '';
               $crm_cust_project->project_status = '';
               $crm_cust_project->project_address = '';
               $crm_cust_project->person_name = '';
               $crm_cust_project->person_designation = '';
               $crm_cust_project->description = '';
               $crm_cust_project->image = '';
               $crm_cust_project->edited_by = '';
               $crm_cust_project->project_type = $_POST['ptype'];
               $crm_cust_project->project_subtype = isset($_POST['pstype'])?implode(',',$_POST['pstype']) : '';
               $crm_cust_project->state = $_POST['state'];
               $crm_cust_project->district = $_POST['district'];
               $crm_cust_project->block = !empty($_POST['block'])?$_POST['block']:'0';
               $crm_cust_project->locality = !empty($_POST['locality'])?$_POST['locality']:'0';
               $crm_cust_project->crm_type = isset($_POST['crm_type'])?implode(',',$_POST['crm_type']) : '1';
               $crm_cust_project->person_phone = $_POST['contact_no'];
               $crm_cust_project->save();
           }

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p> Customer Updated Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';
        }

        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'CRM';
        $states = State::where('country_id','=',101)->get();
        $std_date = date('Y-m-d', strtotime('first day of january this year'));
        $end_date = date('Y-m-d');
        $districts = District::all();
        $customer_class = CustomerClass::all();
        $customer_type = CustomerType::all();
        $association = CrmAssociation::all();
        $customer_category = CustomerCategory::all();
        $users = User::where('user_type','=','crm_user')->get();
        $designation = CustomerDesignation::all();
        $crm_contax = ContactDesignation::all();
        if(isset($_POST['submitCookie'])){
            $crmtype = $_POST['crm_type'];
            setcookie('crmType', $crmtype, time() + (86400 * 30), "/");
            return back();
        }
        return view('crm/customers')->with(compact('header','cfg','tp','footer', 'luser', 'association','title','notices','states','customer_class','customer_type','customer_category','selected_district','user','users', 'designation','selected_locality', 'localities', 'districts','selected_block','blocks', 'buttons','std_date','end_date','crm_contax','crm_projects','user_name'));

    }

    public function store(Request $request){

    }
    public function edit(){

    }
    public function update(){

    }
    public function show(){

    } public function destroy(){

    }
}
