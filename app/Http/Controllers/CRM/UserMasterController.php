<?php

namespace App\Http\Controllers\CRM;

use App\CrmUserRole;
use App\CustomerDesignation;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Crm;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\DataTables;

class UserMasterController extends Crm
{
    public function CrmUserManager(Request $request) {
        $user = User::where('secure', Session::get('crm'))->first();
        $users = User::all();
        $designations = CustomerDesignation::all();
        if(!checkRole($user->u_id,"mas_usr")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';
        if (isset($_POST['add'])) {
            $user = User::where('u_email', $_POST['username'])->first();

            if($user !== null){
                return back()->withErrors(['error' => 'Email ID already exist.']);
            }
            $data['u_name'] = $_POST['name'];
            $data['display_name'] = $_POST['display_name'];
            $data['u_email'] = $_POST['username'];
            $data['u_pass'] = md5($_POST['password']);
            $data['role_id'] = $_POST['role'];
            $data['desigantion_id'] = $_POST['desigantion'];
            $data['user_type'] = 'crm_user';
            DB::table('user') -> insertGetId($data);

            $notices .= '<div class="card-alert card green">
                          <div class = "card-content white-text" >
                         <p> New User  Added Successfully. </p>
                         </div>
                         <button type = "button" class = "close white-text" data-dismiss = "alert" aria-label = "Close">
                         <span aria-hidden = "true" >×</span>
                         </button> </div>';
        }

        if (isset($_POST['edit'])) {

            $uid = $_POST['uid'];
            $name = $_POST['name'];
            $dname = $_POST['display_name'];
            $username = $_POST['username'];
            $password = $_POST['password'];
            $role = $_POST['role'];
            $designation=$_POST['desigantion'];


            DB::update("UPDATE `user` SET `u_name` = '$name',`u_email` = '$username',`display_name`='$dname',`role_id`='$role',`desigantion_id`='$designation' WHERE u_id = '$uid'");
            if($password != ''){
                $password = md5($password);
                DB::update("UPDATE `user` SET `u_pass` = '$password' WHERE u_id = '$uid'");
            }
            $notices .= '<div class="card-alert card green">
                          <div class = "card-content white-text" >
                         <p> New User  Update Successfully. </p>
                         </div>
                         <button type = "button" class = "close white-text" data-dismiss = "alert" aria-label = "Close">
                         <span aria-hidden = "true" >×</span>
                         </button> </div>';

        }

        $cfg = $this -> cfg;
        $tp = url("/assets/crm/");
        $header = $this -> header('Crm', 'index');
        $footer = $this -> footer();
        $title = 'CRM';
        $roles = CrmUserRole::all();
        return view('crm/user-manager') -> with(compact('header', 'cfg', 'tp', 'footer', 'title', 'notices','roles','user','users','designations', 'buttons'));

    }
    public function getUsers(Request $request){
        $records = User::where('user_type', 'crm_user');
        //$records = $records->orderBy('crm_customers.id', 'asc');
        return DataTables::of($records)
            ->addColumn('action', function($records){
                $html = '<a class="btn myblue waves-light" style="padding:0 10px;" href="user-manager?edit='.$records->u_id.'"><i class="material-icons">edit</i></a>';
                return $html;
            })
            ->editColumn('role_id', function($records){
                $pcs = CrmUserRole::where('id', $records->role_id)->get();
                $html = '';
                foreach($pcs as $pc){
                    $html.='<span class="badge success">'.$pc->role.'</span>';
                }
                return $html;
            })
            ->rawColumns(['action', 'role_id'])
            ->make(true);
    }
}
