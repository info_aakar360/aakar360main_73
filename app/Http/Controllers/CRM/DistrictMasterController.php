<?php

namespace App\Http\Controllers\CRM;

use App\CrmCustomer;
use App\CustomerCategory;
use App\CustomerClass;
use App\CustomerType;
use App\District;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Crm;
use App\State;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class DistrictMasterController extends Crm
{
    public function Districtmaster(Request  $request)
    {
        $user = User::where('secure', Session::get('crm'))->first();
        if(!checkRole($user->u_id,"mas_dis")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';
        $selected_district=array();
        if(isset($_POST['add'])){
            // dd($_POST);
//            $population = $_POST['population'];
//            $area = $_POST['area'];
            $district = $_POST['district'];
            $state = $_POST['state'];
            $lat = $_POST['lat'];
            $lng = $_POST['lng'];
//            $density = $_POST['density'];
            DB::insert("INSERT INTO `district`(`state_id`, `name`,`lat`, `lng`) VALUES ('$state','$district','$lat','$lng')");
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New District Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';
        }
        if(isset($_POST['edit'])){

            $didd = $_POST['didd'];
            $district = $_POST['district'];
            $lat = $_POST['lat'];
            $lng = $_POST['lng'];
//            $population = $_POST['population'];
//            $area = $_POST['area'];
//            $Density = $_POST['density'];

            DB::update("UPDATE `district` SET  `name`='$district',`lat`='$lat',`lng`='$lng'  WHERE id = '$didd'");
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>District Updated Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

        }
        if(isset($_GET['edit'])){
            $cid = $_GET['edit'];
            $selected_district = CrmCustomer::leftJoin('district', 'state_id', '=', 'crm_customers.state')
                ->where('crm_customers.id', '=', $cid)
                ->select('district.name as d_name','district.id as did')
                ->get();
        }
        $states = State::where('country_id','=',101)->get();
        $district = District::join('states', 'district.state_id', '=', 'states.id')
            ->where('states.country_id', '=', 101)
            ->select('district.id as district_id','district.name as district_name','states.name as state_name','states.id as state_id')
            ->get();
        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'CRM';
        $customers = CrmCustomer::join('customer_category', 'crm_customers.customer_category', '=', 'customer_category.id')
            ->select('crm_customers.*','customer_category.type')
            ->get();
        $customer_class = CustomerClass::all();
        $customer_type = CustomerType::all();
        $customer_category = CustomerCategory::all();
        return view('crm/district-master')->with(compact('header','cfg','tp','footer', 'title','notices','customers','states','customer_class','customer_type','customer_category','selected_district','district', 'buttons'));

    }
}
