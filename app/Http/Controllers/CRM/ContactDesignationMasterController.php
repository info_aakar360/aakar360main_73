<?php

namespace App\Http\Controllers\CRM;

use App\Category;
use App\ContactDesignation;
use App\CustomerCategoryPrice;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Crm;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ContactDesignationMasterController extends Crm
{
    public function Contactdesignation(){
        $cfg = '';
        $customer_type = '';
        $user = User::where('secure', Session::get('crm'))->first();
        $users = User::where('user_type','=','crm_user')->get();

        if(!checkRole($user->u_id,"mas_cont")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';
        if (isset($_POST['add'])) {
            $data['name'] = $_POST['name'];
            DB::table('crm_contax') -> insertGetId($data);
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Contact  Designation Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';
            return back();
        }

        if(isset($_POST['edit'])){
            $users = '';
            $id = $_POST['cid'];
            $name = $_POST['name'];

            DB::update("UPDATE crm_contax set `name`='$name' where `id`='$id'");
            return back();
        }

        $product_category = Category::all();
        $crm_contax = ContactDesignation::all();
        if (isset($_GET['edit'])) {
            $records = CustomerCategoryPrice::join('customer_category', 'customer_category.id', '=', 'customer_category_price.customer_category')->join('category', 'category.id', '=', 'customer_category_price.product_category')->select(['customer_category_price.*', 'customer_category.type', 'category.name'])->where('customer_category_price.customer_category', $_GET['edit'])->orderBy('category.id', 'ASC')->get();
        }else{
            $records = CustomerCategoryPrice::join('customer_category', 'customer_category.id', '=', 'customer_category_price.customer_category')->join('category', 'category.id', '=', 'customer_category_price.product_category')->select(['customer_category_price.*', 'customer_category.type', 'category.name'])->orderBy('category.id', 'ASC')->get();
        }
        $tp = url("/assets/crm/");
        $header = $this -> header('Crm', 'index');
        $footer = $this -> footer();
        $title = 'CRM';
        return view('crm/contact-designation') -> with(compact('header', 'cfg', 'tp', 'footer', 'title', 'notices', 'customer_type', 'product_category', 'crm_contax','users', 'records', 'buttons'));
    }

}
