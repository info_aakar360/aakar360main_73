<?php

namespace App\Http\Controllers\CRM;

use App\CrmCustomer;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Crm;
use App\NoOrder;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class NoOrderReasonController extends Crm
{
    public function NoOrderReason(Request  $request)
    {
        $user = User::where('secure', Session::get('crm'))->first();
        if(!checkRole($user->u_id,"no_order")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';
        $selected_district=array();
        if(isset($_POST['add'])){
            $reason = $_POST['reason'];

            DB::insert("INSERT INTO `no_order`(`reason`) VALUES ('$reason')");
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>No Order Reason Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';
        }
        if(isset($_POST['edit'])){
            $cid = $_POST['cid'];
            $reason = $_POST['reason'];

            DB::update("UPDATE `no_order` SET  `reason`='$reason'  WHERE id = '$cid'");
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>No Order Reason Updated Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

        }

        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'CRM';
        $customers = CrmCustomer::join('customer_category', 'crm_customers.customer_category', '=', 'customer_category.id')
            ->select('crm_customers.*','customer_category.type')
            ->get();
        $no_orders  = NoOrder::all();


        return view('crm/no-order-reason-master')->with(compact('header','cfg','tp','footer', 'title','notices','customers','no_orders', 'buttons'));

    }
}
