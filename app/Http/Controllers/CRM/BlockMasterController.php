<?php

namespace App\Http\Controllers\CRM;

use App\Blocks;
use App\District;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Crm;
use App\State;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class BlockMasterController extends Crm
{
    public function blockmaster(Request  $request)
    {
        $user = User::where('secure', Session::get('crm'))->first();
        if(!checkRole($user->u_id,"mas_block")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';
        $selected_district=array();
        $districts = array();
        if(isset($_POST['add'])){
            $block = $_POST['block'];
            $district = $_POST['district'];
            $population = $_POST['population'];
            $area = $_POST['area'];
            $density = $_POST['density'];
            $lat = $_POST['lat'];
            $lng = $_POST['lng'];
            DB::insert("INSERT INTO `blocks`(`block`, `district_id`, `population`, `area`, `density`,`lat`, `lng`) VALUES ('$block','$district', '$population', '$area', '$density','$lat', '$lng')");
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Block Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';
        }
        if(isset($_POST['edit'])){
            $didd = $_POST['didd'];
            $block = $_POST['block'];
            $population = $_POST['population'];
            $area = $_POST['area'];
            $density = $_POST['density'];
            $lat = $_POST['lat'];
            $lng = $_POST['lng'];

            DB::update("UPDATE `blocks` SET `block`='$block', `population`='$population', `area`='$area', `density`='$density',`lat`='$lat',`lng`='$lng' WHERE id = '$didd'");
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>Block Updated Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

        }
        $block = null;
        if(isset($_GET['edit'])){
            $cid = $_GET['edit'];
            $block = Blocks::where("id", $cid)->first();
            $sd = District::where('id', $block->district_id)->first();
            $districts = District::where('state_id', $sd->state_id)->get();
        }

        $states = State::where('country_id','=',101)->get();

        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'CRM - Block Master';

        return view('crm/block-master')->with(compact('header','cfg','tp','footer', 'title','notices','states', 'block', 'districts', 'buttons'));

    }
}
