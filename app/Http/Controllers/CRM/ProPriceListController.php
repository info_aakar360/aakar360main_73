<?php

namespace App\Http\Controllers\CRM;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Crm;
use App\Products;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Session;

class ProPriceListController extends Crm
{
    public function ProductPriceList(){
        $user = User::where('secure', Session::get('crm'))->first();
        if(!checkRole($user->u_id,"pric_ent")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $notices = '';
        $cat_id = '';
        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Product Price List';
        $brands = array();
        $variants= array();
        $pro1= array();
        $cid='';
        $selected_category = '';
        $category = Category::all();
        $report_type = '';
        if(isset($_POST['category_find'])){
            $cid = $_POST['category'];
            $cid1[] = $_POST['category'];
            $c = array();
            $cont = true;
            $temp = $cid1;
            while($cont){
                $c = Arr::pluck(Category::whereIn('parent', $temp)->get(), 'id');
                if(count($c)){
                    $cid1 = array_merge($cid1, $c);
                    $temp = $c;
                }else{
                    $cont = false;
                }
            }
            $pro1 = Products::whereIn('category', $cid1)->get();

            $select_category = Category::where('id','=',$cid)->first();
            $selected_category = $select_category->name;
            $cat_id = $select_category->id;
            $brands = Products::where('category','=',$cid)->get();
            $variants = Products::where('category','=',$cid)->get();

        }
        if(isset($_POST['price_save'])){
            $i=0;
            foreach ($_POST['pid'] as $pid){

                $product_id = $_POST['pid'][$i];
                $new_price = $_POST['new_price'][$i];
                $product = Products::find($pid);
                $product->purchase_price = $new_price;
                $product->save();
                $i++;
            }
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>Price Update Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

        }

        return view('crm/product-price-list')->with(compact('header','cfg','tp','footer', 'title','category','brands','cat_id','variants','cid','selected_category','pro1','notices'));
    }

}
