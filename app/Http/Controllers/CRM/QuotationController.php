<?php

namespace App\Http\Controllers\CRM;

use App\Category;
use App\Company;
use App\CrmCustomer;
use App\CustomerCategory;
use App\CustomerClass;
use App\CustomerType;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Crm;
use App\ManufacturingHubs;
use App\Products;
use App\ProductVariant;
use App\State;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class QuotationController extends Crm
{
    public function Variantcompare()
    {
        $user = User::where('secure', Session::get('crm'))->first();
        if (!checkRole($user->u_id, "quot")) {
            return redirect()->to('crm/index')->withErrors(['ermsg' => 'You don\'t have access to this section.']);
        }
        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm', 'index');
        $footer = $this->footer();
        $title = 'Variant Compare';
        $brands = array();
        $variants = array();
        $cid = '';
        $selected_category = array();
        $selected_variant = '';
        $category = Category::all();
        $mhs = ManufacturingHubs::all();
        $all_variants = ProductVariant::join('size', 'size.id', '=', 'product_variants.variant_title')->select('product_variants.id', 'size.name as variant_title')->orderBy('variant_title', 'ASC')->get();
        $report_type = '';
        $view_type = '';
        $set_price = 0;
        $cust_id = '0';
        $company_id = '';
        $mhid = 0;
        if (isset($_POST['hub'])) {
            $mhid = $_POST['hub'];
        }
        $cust_type = '';
        if (isset($_POST['cust_type'])) {
            $cust_type = $_POST['cust_type'];
        }
        if (isset($_POST['customer_search'])) {
            $cust_id = $_POST['customer_search'];
            if (isset($_POST['variants']) && isset($_POST['category'])) {
                if ($_POST['variants'] != '' && $_POST['category'] != '') {
                    //dd($_POST);
                    //dd($_POST['variants']);
                    $vids = $_POST['variants'];
                    $vid = implode(',', $vids);
                    $cids = $_POST['category'];
                    $selected_variant = $vid;
                    $report_type = $_POST['report_type'];
                    $view_type = $_POST['view_type'];
                    if (isset($_POST['price'])) {
                        $set_price = $_POST['price'];
                    }
                    if (isset($_POST['company']))
                        $company_id = $_POST['company'];
                    $get_prod = DB::select("SELECT GROUP_CONCAT(p.category) as cat_id FROM products as p INNER JOIN (SELECT DISTINCT(pv.product_id) FROM `product_variants`  as pv WHERE id IN ($vid)) as t1 ON p.id = t1.product_id")[0];
                    $get_brand_val = $get_prod->cat_id;
                    $brands = Products::whereIn('category', $cids)->get();
                    //dd($brands);
                    $p_ids = Products::selectRaw('GROUP_CONCAT(id) as prod_ids')->whereIn('category', $cids)->first();
                    $tot = explode(',', $p_ids->prod_ids);

                    if (count($tot) != 0)
                        $variants = ProductVariant::whereIn('id', $vids)->join('size', 'size.id', '=', 'product_variants.variant_title')->select(['product_variants.*', 'size.name'])->orderBy('size.name', 'ASC')->get();
                } else {
                    $cid = $_POST['category'];

                    if (!is_array($_POST['category'])) {
                        $cid = explode(',', $_POST['category']);
                    }
                    $selected_category = $cid;
                    $report_type = $_POST['report_type'];
                    $view_type = $_POST['view_type'];
                    if (isset($_POST['price']))
                        $set_price = $_POST['price'];
                    $brands = Products::whereIn('category', $cid)->get();
                    $p_ids = Products::selectRaw('GROUP_CONCAT(id) as prod_ids')->whereIn('category', $cid)->first();
                    $tot = explode(',', $p_ids->prod_ids);
                    if (count($tot) != 0)
                        $variants = ProductVariant::whereIn('product_id', $tot)->join('size', 'size.id', '=', 'product_variants.variant_title')->select(['product_variants.*', 'size.name'])->orderBy('name', 'ASC')->groupBy('variant_title')->get();
                }
            } else {
                $cid = $_POST['category'];
                $selected_category = $cid;
                $report_type = $_POST['report_type'];
                $view_type = $_POST['view_type'];
                if (isset($_POST['price']))
                    $set_price = $_POST['price'];
                $brands = Products::whereIn('category', $cid)->get();
                $p_ids = Products::selectRaw('GROUP_CONCAT(id) as prod_ids')->whereIn('category', $cid)->first();
                $tot = explode(',', $p_ids->prod_ids);
                if (count($tot) != 0)
                    $variants = ProductVariant::whereIn('product_id', $tot)->join('size', 'size.id', '=', 'product_variants.variant_title')->select(['product_variants.*', 'size.name'])->orderBy('name', 'ASC')->groupBy('variant_title')->get();
            }
        }
        if (isset($_POST['category_find'])) {
            if (isset($_POST['customer_search'])) {
                $cust_id = $_POST['customer_search'];
            }
            $report_type = $_POST['report_type'];
            $view_type = $_POST['view_type'];
            if (isset($_POST['variants'])) {
                $vid = implode(',', $_POST['variants']);
                $vidArray = $_POST['variants'];
                $selected_variant = $vid;
                $get_prod = DB::select("SELECT GROUP_CONCAT(p.category) as cat_id FROM products as p INNER JOIN 
(SELECT DISTINCT(pv.product_id) FROM `product_variants`  as pv WHERE id IN ($vid)) as t1 ON p.id = t1.product_id")[0];
                $get_brand_val = $get_prod->cat_id;
                if (is_array($get_brand_val)) {
                    $brands = Products::whereIn('category', $get_brand_val)->get();
                    $p_ids = Products::selectRaw('GROUP_CONCAT(id) as prod_ids')->whereIn('category', $get_brand_val)->first();
                } else {
                    $brands = Products::where('category', $get_brand_val)->get();
                    $p_ids = Products::selectRaw('GROUP_CONCAT(id) as prod_ids')->where('category', $get_brand_val)->first();
                }
                $tot = explode(',', $p_ids->prod_ids);
                if (count($tot))
                    $variants = ProductVariant::whereIn('product_variants.id', $vidArray)->join('size', 'size.id', '=', 'product_variants.variant_title')->select(['product_variants.*', 'size.name'])->orderBy('size.name', 'ASC')->get();
            } else {
                $cid = $_POST['category'];
                $selected_category = $cid;
                $brands = Products::whereIn('category', $cid)->get();
                $p_ids = Products::selectRaw('GROUP_CONCAT(id) as prod_ids')->whereIn('category', $cid)->first();
                $tot = explode(',', $p_ids->prod_ids);
                if ($tot != 0)
                    $variants = ProductVariant::whereIn('product_id', $tot)->join('size', 'size.id', '=', 'product_variants.variant_title')->select(['product_variants.*', 'size.name'])->orderBy('name', 'ASC')->groupBy('variant_title')->get();
            }
        }
        if (isset($_POST['category'])) {
            $selected_category = $_POST['category'];
            if (!is_array($_POST['category'])) {
                $selected_category = explode(',', $_POST['category']);
            }
        }
        $customers = CrmCustomer::all();
        $company = Company::all();
        $customer_class = CustomerClass::all();
        $customer_type = CustomerType::all();
        $customer_category = CustomerCategory::all();
        $states = State::where('country_id', '=', 101)->get();
        return view('crm/variant-compare')->with(compact('header', 'cfg', 'tp', 'footer', 'customer_category', 'company', 'customer_class', 'customer_type', 'view_type', 'states', 'title', 'category', 'all_variants', 'brands', 'variants', 'cid', 'report_type', 'selected_category', 'selected_variant', 'set_price', 'customers', 'cust_id', 'company_id', 'mhs', 'mhid', 'cust_type'));
    }
}
