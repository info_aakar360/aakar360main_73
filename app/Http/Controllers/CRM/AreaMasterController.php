<?php

namespace App\Http\Controllers\CRM;

use App\Blocks;
use App\CrmCustomer;
use App\District;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Crm;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AreaMasterController extends Crm
{
    public function LocalityMaster(Request  $request)
    {
        $user = User::where('secure', Session::get('crm'))->first();
        if(!checkRole($user->u_id,"mas_area")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';
        $selected_district=array();
        if(isset($_POST['add'])){
            $data['district_id'] = $_POST['district'];
            $data['locality'] = $_POST['locality'];
            $data['block_id'] = $_POST['block'];
            DB::table('locality')->insert($data);
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>Area / Locality Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';
        }
        if(isset($_POST['edit'])){
            $lid = $_POST['lid'];
            $did = $_POST['district'];
            $locality = $_POST['locality'];
            $block = $_POST['block'];

            DB::update("UPDATE `locality` SET `district_id`='$did',`locality`='$locality',`block_id`='$block' WHERE id = '$lid'");
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>Area / Locality Updated Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

        }
        if(isset($_GET['edit'])){
            $cid = $_GET['edit'];

            $selected_district = CrmCustomer::leftJoin('district', 'state_id', '=', 'crm_customers.state')
                ->where('crm_customers.id', '=', $cid)
                ->select('district.name as d_name','district.id as did')
                ->get();
        }
        $districts = District::all();
        $blocks = Blocks::all();
        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'CRM - Area / Locality';

        return view('crm/locality-master')->with(compact('header','cfg','tp','footer', 'title','notices','districts','blocks' ,'buttons'));

    }
}