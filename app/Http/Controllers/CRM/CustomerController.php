<?php

namespace App\Http\Controllers\CRM;

use App\AddOnCharge;
use App\Blocks;
use App\Brand;
use App\Category;
use App\CategoryGroup;
use App\CrmAssociation;
use App\CrmCall;
use App\CrmCustomer;
use App\CrmCustomerProject;
use App\CrmMessageSave;
use App\CrmNotes;
use App\CrmProduct;
use App\CrmProjectSubtype;
use App\CrmPromotional;
use App\CrmSupplier;
use App\CrmTickets;
use App\CrmVisits;
use App\CustomerCategory;
use App\CustomerClass;
use App\CustomerDesignation;
use App\CustomerType;
use App\District;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Crm;
use App\LeadOrder;
use App\NoOrder;
use App\ReferrerMaster;
use App\SalesPipeline;
use App\State;
use App\Timeline;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;
use Yajra\DataTables\DataTables;

class CustomerController extends Crm
{
    public function CustomerData( Request $request){
        $lat = '';
        $user = User::where('secure', Session::get('crm'))->first();
        $designations = CustomerDesignation::all();
        $users = User::all();
        if(!checkRole($user->u_id,"cst_data")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';
        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Customer Data';

        /*$category = '';
        $states = '';
        $district = '';
        $a = '';
        if(isset($_POST['search'])){

            if(isset($_POST['category'])){
                $category = implode(',',$_POST['category']);
                $a .= ' and crm_customers.customer_category IN ('.$category.')';
            }

            if(isset($_POST['states'])){
               $states = implode(',',$_POST['states']);
                $a .= ' and crm_customers.state IN ('.$states.')';
            }
            if(isset($_POST['district'])){
                $district = implode(',',$_POST['district']);
                $a .= ' and crm_customers.district IN ('.$district.')';
            }

            $customers = DB::select("SELECT s.name as state_name , d.name as district_name,crm_customers.*,customer_category.type,(SELECT COUNT(id) FROM crm_tickets WHERE crm_customer_id =crm_customers.id ) as tot_ticket , (SELECT COUNT(id) FROM crm_visits WHERE crm_customer_id = crm_customers.id ) as tot_visit,(SELECT COUNT(id) FROM crm_customer_projects WHERE crm_customer_id =crm_customers.id ) as tot_project , (SELECT COUNT(id) FROM crm_promotional WHERE crm_customer_id =crm_customers.id ) as tot_promo ,(SELECT COUNT(id) FROM crm_call WHERE crm_customer_id = crm_customers.id ) as tot_call FROM `crm_customers` INNER JOIN customer_category ON crm_customers.customer_category = customer_category.id INNER JOIN states as s on crm_customers.state = s.id INNER JOIN district as d ON crm_customers.district = d.id WHERE  1=1 $a");

        }
        else{
            $customers = DB::select("SELECT s.name as state_name , d.name as district_name,crm_customers.*,customer_category.type,(SELECT COUNT(id) FROM crm_tickets WHERE crm_customer_id =crm_customers.id ) as tot_ticket , (SELECT COUNT(id) FROM crm_visits WHERE crm_customer_id = crm_customers.id ) as tot_visit,(SELECT COUNT(id) FROM crm_customer_projects WHERE crm_customer_id =crm_customers.id ) as tot_project , (SELECT COUNT(id) FROM crm_promotional WHERE crm_customer_id =crm_customers.id ) as tot_promo ,(SELECT COUNT(id) FROM crm_call WHERE crm_customer_id = crm_customers.id ) as tot_call FROM `crm_customers` INNER JOIN customer_category ON crm_customers.customer_category = customer_category.id INNER JOIN states as s on crm_customers.state = s.id INNER JOIN district as d ON crm_customers.district = d.id ");

        }*/

//            dd($lat);
        $customer_category = CustomerCategory::all();
        $product_category = Category::all();
        $customer_supplier = CrmSupplier::all();
        $customer_brand = Brand::all();
        $customer_type = CustomerType::all();
        $states = State::where('country_id','=',101)->get();
        $cust_classes = CustomerClass::all();
        $association = CrmAssociation::all();
        $s_districts = array();
        if(isset($_GET['edit'])) {
            $cdata = CrmCustomer::find($_GET['edit']);
            $s_districts = District::where('state_id','=',$cdata->state)->get();
        }
        if(isset($_POST['submitCookie'])){
            $crmtype = $_POST['crm_type'];
            setcookie('crmType', $crmtype, time() + (86400 * 30), "/");
            return back();
        }
        return view('crm/customer-data')->with(compact('header','cfg','tp','customer_supplier','customer_brand','footer', 'title','notices','customer_category','states', 's_districts', 'cust_classes','customer_type', 'buttons','lat','product_category','designations','users','association'));
    }
    public function getCustomersData(Request $request){
        $crm_type = isset($_COOKIE['crmType'])?$_COOKIE['crmType']:'1';
        $records = CrmCustomer::join('customer_category', 'customer_category.id', '=', 'crm_customers.customer_category')->where('crm_customers.crm_type',$crm_type)
            ->select('crm_customers.id', 'crm_customers.name', 'crm_customers.sec_names','crm_customers.proprieter_name','crm_customers.converted_by','crm_customers.managed_by','crm_customers.crm_type','crm_customers.referred_by','crm_customers.referrer','crm_customers.product_category','crm_customers.contact_no','crm_customers.sec_contact', 'customer_category.type', 'crm_customers.blocks', DB::raw('(select name from states where states.id = crm_customers.state) as state_name') , DB::raw('(select name from district where district.id = crm_customers.district) as district_name'), DB::raw('(select locality from locality where locality.id = crm_customers.locality) as locality_name') ,DB::raw('(select count(*) from crm_tickets where crm_tickets.crm_customer_id = crm_customers.id) as t_tickets'), DB::raw('(select count(*) from crm_visits where crm_visits.crm_customer_id = crm_customers.id) as t_visits'), DB::raw('(select count(*) from crm_customer_projects where crm_customer_projects.crm_customer_id = crm_customers.id) as t_projects'), DB::raw('(select count(*) from crm_promotional where crm_promotional.crm_customer_id = crm_customers.id) as t_promotional'), DB::raw('(select count(*) from crm_call where crm_call.crm_customer_id = crm_customers.id) as t_call'), DB::raw('(select type from customer_type where customer_type.id = crm_customers.type) as c_type'), 'crm_customers.verified' );
        $count_total = $records->count();
        $category = (isset($_GET["category"])) ? $_GET["category"] : false;
        $state = (isset($_GET["state"])) ? ($_GET["state"]) : false;
        $district = (isset($_GET["district"])) ? ($_GET["district"]) : false;
        $group = (isset($_GET["group"])) ? ($_GET["group"]) : false;
        $class = (isset($_GET["cust_class"])) ? ($_GET["cust_class"]) : false;
        $type = (isset($_GET["type"])) ? ($_GET["type"]) : false;
        $ptype = (isset($_GET["ptype"])) ? ($_GET["ptype"]) : false;
        $pstype = (isset($_GET["pstype"])) ? ($_GET["pstype"]) : false;
        $supplier = (isset($_GET["supplier"])) ? ($_GET["supplier"]) : false;
        $brand = (isset($_GET["brand"])) ? ($_GET["brand"]) : false;
        $verified = (isset($_GET["verified"])) ? ($_GET["verified"]) : false;
        $pro_category = (isset($_GET["pro_category"])) ? ($_GET["pro_category"]) : false;
        $managed_by = (isset($_GET["managed_by"])) ? ($_GET["managed_by"]) : false;
        $association = (isset($_GET['association'])) ? ($_GET['association']) : false;
        $start = $_GET['start'];
        $length = $_GET['length'];
        if($category){
            $records = $records->whereIn('customer_category', $category);
        }if($group){
            $records = $records->whereIn('customer_designation', $group);
        }if($state){
            $records = $records->whereIn('state', $state);
        }if($district){
            $records = $records->whereIn('district', $district);
        }if($class){
            $records = $records->whereIn('class', $class);
        }if($type){
            $records = $records->where('crm_customers.type',$type);
        }if($ptype){
            $c = Arr::pluck(CrmCustomerProject::where('project_type',$ptype)->get()->toArray(), 'crm_customer_id');
            $records = $records->whereIn('crm_customers.id',$c);
        }if($pstype){
            $cd = Arr::pluck(CrmCustomerProject::where('project_type',$_GET["ptype"])->whereIn('project_subtype',$pstype)->get()->toArray(), 'crm_customer_id');
            $records = $records->whereIn('crm_customers.id',$cd);
        }if($supplier){
            $c = Arr::pluck(CrmSupplier::whereIn('id',$supplier)->get()->toArray(), 'id');
            $sup = Arr::pluck(CrmProduct::whereRaw('FIND_IN_SET(?, supplier_id)', $c)->get()->toArray(), 'crm_customer_id');
            $records = $records
                ->whereIn('crm_customers.id',$sup);
        }if($brand){
            $cz = Arr::pluck(Brand::whereIn('id',$brand)->get()->toArray(), 'id');
            $br = Arr::pluck(CrmProduct::whereRaw('FIND_IN_SET(?, brands)', $cz)->get()->toArray(), 'crm_customer_id');
            $records = $records->whereIn('crm_customers.id',$br);
        }if($pro_category){
            $pro_category = implode(',',$pro_category);
            $prop = Arr::pluck(CrmProduct::whereRaw('FIND_IN_SET_X(?, product_id)', $pro_category)->get(), 'crm_customer_id');
            $records = $records->whereIn('crm_customers.id',$prop);
        }if($managed_by){
            $records->whereIn('crm_customers.managed_by',$managed_by);
        }if($verified){
            if($verified == "1"){
                $records = $records->where('crm_customers.verified',$verified);
            }else{
                $records = $records->where('crm_customers.verified','0');
            }
        }if($association) {
            $records = $records->whereIn('crm_customers.crm_association',$association);
        }
        $count_filter = $records->count();
        return DataTables::of($records)
            ->addColumn('action', function($row) use($start, $length){
                $a="customer-data";
                $html = '<a href="customer-profile?cid='.$row->id.'&link='.$a.'" class="btn myblue waves-light" style="padding: 0 1rem"><i class="material-icons">account_circle</i></a>';
                return $html;
            })
            ->editColumn('name', function($row) {
                $html = '';
                $verify = '';
                $referrer = '';
                $referred_by = '';
                if($row->verified){
                    $verify = '<i class="material-icons" style="font-size: 16px;color: blue;">verified_user</i>';
                }if($row->referred_by){
                    $referred_by = '<img src="../assets/crm/icon/referred_by.png" width="15px" height="15px"/>';
                }
                if($row->referrer){
                    $referrer = '<img src="../assets/crm/icon/referrer.png" width="15px" height="15px"/>';
                }
                if(!empty($row->sec_names)) {
                $html = $row->name.','.implode(',',unserialize($row->sec_names)).$verify.$referred_by.$referrer;
                }else{
                $html = $row->name.$verify.$referrer.$referred_by;
                }
                return $html;
            })
            ->editColumn('sec_names', function($row){
                $html = '';
                if(!empty($row->sec_names)) {
                    $html = implode(',',unserialize($row->sec_names));

                }
                return $html;
            })->editColumn('contact_no', function($row){
                $html = '';
                if(!empty($row->sec_contact)){
                    $nos = implode(',',unserialize($row->sec_contact));
                    $html = $row->contact_no.','.'<br>'.$nos;
                }else{
                    $html = $row->contact_no;
                }

                return $html;
            })->editColumn('sec_contact', function($row){
                $html = '';
                if(!empty($row->sec_contact)) {
                    $html = implode(',',unserialize($row->sec_contact));
                }
                return $html;
            })->editColumn('proprieter_name', function($row){
                if($row->proprieter_name){
                    return $row->proprieter_name;
                }
                return 'NA';
            })
            ->addColumn('group', function($row){

                $dex = array();
                $users = explode(',', $row->managed_by);
                $u = Arr::pluck(User::whereIn('u_id', $users)->get(), 'u_name');

                $d = CustomerDesignation::all();
                foreach ($d as $des) {
                    if (in_array('des' . $des->id, $users)) {
                        $dex[] = $des->name;
                    }
                }

                $data =  array_merge($dex,$u);
                return implode(',',$data);
            })->addColumn('referred_by', function($row){
                if(!empty($row->referred_by)){
                    $names = Arr::pluck(CrmCustomer::whereIn('id',explode(',',$row->referred_by))->get(),'name');
                    return implode(',',$names);
                }
            })->addColumn('blocks', function($row){
                $blocks = Arr::pluck(Blocks::where('id',$row->blocks)->get(),'block');
                return $blocks;
            })->with([
                "recordsTotal" => $count_total,
                "recordsFiltered" => $count_filter
            ])
            ->rawColumns(['name','contact_no','action'])
            ->make(true);
    }
    public function CustomerProfile( Request $request)
    {
        $notices = '';
        $messages = '';
        $crmreffer = '';
        $customer = false;
        $cid = '';
        $note_product = '';
        $crmnote = '';
        $addon = '';
        $tickets = array();
        $visits = array();
        $notes = array();
        $projects = array();
        $promotionals = array();
        $products = array();
        $secure = Session::get('crm');
        $user = User::where('secure', Session::get('crm'))->first();
        $sales_pipelines = SalesPipeline::all();
        $designations = CustomerDesignation::all();
        $no_order_reasons = NoOrder::all();

        $selected_option = $user->u_name;
        $crm_type = isset($_COOKIE['crmType']) ? $_COOKIE['crmType'] : '1';
        if (isset($_GET['cid'])) {
            $cid = $_GET['cid'];
            $customer = CrmCustomer::join('customer_category', 'crm_customers.customer_category', '=', 'customer_category.id')
                ->where('crm_customers.id', '=', $cid)
                ->where('crm_customers.crm_type', '=', $crm_type)
                ->select('crm_customers.*', 'crm_customers.type as type_id', 'customer_category.type')
                ->first();

            $visits = CrmVisits::where('crm_customer_id', '=', $cid)->where('crm_type', '=', $crm_type)->get();
            $promotionals = CrmPromotional::where('crm_customer_id', '=', $cid)->where('crm_type', '=', $crm_type)->get();
            $products = CrmProduct::where('crm_customer_id', '=', $cid)->where('crm_type', '=', $crm_type)->get();
            $calls = CrmCall::where('crm_customer_id', '=', $cid)->where('crm_type', '=', $crm_type)->get();
            $note_product = CrmProduct::where('crm_customer_id', $cid)->where('crm_type', $crm_type)->get();
            $addon = AddOnCharge::all();
            if (!empty($customer->contact_no)) {
                $messages = DB::select("SELECT * FROM `crm_message_save` WHERE mobile IN (" . $customer->contact_no . ")");
            }
        }

        if (isset($_POST['add_ticket'])) {
            // dd($_POST);
            $file = '';
            if (request()->file('image')) {
                // Upload the downloadable file to product downloads directory
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()) . '.' . request()->file('image')->getClientOriginalExtension();
                $path = base_path() . '/assets/crm/images/tickets/';
                request()->file('image')->move($path, $file);
                $img = Image::make($path . $file)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path . $file);
            }
            //dd($_POST);
            $cid = $_POST['crm_cust_id'];
            /*$email = $_POST['email'];
            $subject = $_POST['subject'];
            $phone = $_POST['phone'];
            $cc = $_POST['cc'];*/
            $data['crm_customer_id'] = $cid;
            $data['type'] = $_POST['type'];
            $data['create_date'] = $_POST['ticket_create_date'];
            $data['due_date'] = $_POST['ticket_due_date'];
            $data['status'] = $_POST['status'];
            $data['priority'] = $_POST['priority'];
            $data['assign_to'] = isset($_POST['Assign_to']) ? implode(',', $_POST['Assign_to']) : '';
            $data['description'] = $_POST['description'];
            $data['crm_type'] = $crm_type;
            $data['image'] = $file;
            $data['edited_by'] = $user->u_name;
            //   $data['acc_manager'] = $_POST['acc_manager'];

            /*$tag = $_POST['tag'];*/
            $id = DB::table('crm_tickets')->insertGetId($data);

            $timeline = new Timeline();
            $timeline->customer_id = $cid;
            $timeline->section = 'tickets';
            $timeline->section_id = $id;
            $luser = User::where('secure', Session::get('crm'))->first();
            $timeline->created_by = $luser->u_id;
            $timeline->crm_type = $crm_type;
            $timeline->date = date('Y-m-d h:i:s', strtotime($data['create_date']));
            $timeline->save();

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Ticket Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';
            return back()->with('notices', $notices);
        }
        if (isset($_POST['edit_ticket'])) {
            // dd($_POST);
            $cid = $_POST['crm_customer_id'];
            $tid = $_POST['ticket_id'];
            /*$email = $_POST['email'];
            $subject = $_POST['subject'];
            $phone = $_POST['phone'];
            $cc = $_POST['cc'];*/
            $type = $_POST['type'];
            $status = $_POST['status'];
            $priority = $_POST['priority'];
            $assign_to = isset($_POST['assign_to']) ? implode(',', $_POST['assign_to']) : '';
            $description = $_POST['description'];
            //    $acc_manager = $_POST['acc_manager'];
            $due_date = date('Y-m-d', strtotime($_POST['ticket_due_date']));

            /* $tag = $_POST['tag'];*/
            DB::update("UPDATE `crm_tickets` SET `type`='$type',`status`='$status',`priority`='$priority',`assign_to`='$assign_to',`description`='$description',`due_date`='$due_date',`crm_type`='$crm_type' WHERE id = '$tid'");

            $timeline = new Timeline();
            $timeline->customer_id = $cid;
            $timeline->section = 'tickets_edit';
            $timeline->section_id = $tid;
            $luser = User::where('secure', Session::get('crm'))->first();
            $timeline->created_by = $luser->u_id;
            $timeline->crm_type = $crm_type;
            $timeline->date = date('Y-m-d h:i:s');
            $timeline->save();

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Ticket Update Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }

        if (isset($_POST['add_visit'])) {
            //dd($_POST);
            $file = '';
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()) . '.' . request()->file('image')->getClientOriginalExtension();
                $path = base_path() . '/assets/crm/images/visits/';
                request()->file('image')->move($path, $file);
                $img = Image::make($path . $file)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path . $file);
            }

            $cid = $_POST['crm_cust_id'];
            $data['crm_customer_id'] = $cid;
            $data['name'] = $_POST['name'];
            $data['phone_no'] = $_POST['phone_no'];
            $data['address'] = $_POST['address'];
            $data['select_date'] = $_POST['date'];
            $data['description'] = $_POST['description'];
            $data['user_id'] = implode(',', $_POST['user']);
            $data['image'] = $file;
            $data['edited_by'] = $user->u_name;
            $data['crm_type'] = $crm_type;
            $user_name = $user->u_id;

            if ($customer) {
                $data['customer_class'] = $customer->class;
            }
            $id = DB::table('crm_visits')->insertGetId($data);

            $timeline = new Timeline();
            $timeline->customer_id = $cid;
            $timeline->section = 'visits';
            $timeline->section_id = $id;
            $luser = User::where('secure', Session::get('crm'))->first();
            $timeline->created_by = $luser->u_id;
            $timeline->crm_type = $crm_type;
            $timeline->date = date('Y-m-d h:i:s', strtotime($data['select_date']));
            $timeline->save();

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Visit Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        if (isset($_POST['edit_visit'])) {
            $notices = '';
            $file = '';
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()) . '.' . request()->file('image')->getClientOriginalExtension();
                $path = base_path() . '/assets/crm/images/visits/';
                request()->file('image')->move($path, $file);
                $img = Image::make($path . $file)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path . $file);
            }
            $cid = $_POST['crm_customer_id'];
            $vid = $_POST['visit_id'];
            $name = $_POST['name'];
            $phone_no = $_POST['phone_no'];
            $address = $_POST['address'];
            $date = $_POST['date'];
            $description = $_POST['description'];
            $user_id = implode(',', $_POST['user']);
            $visit = CrmVisits::where('id', $vid)->first();
            if ($file != '') {
                $file = $file . ',' . $visit->image;
            }
            DB::update("UPDATE `crm_visits` SET `name`='$name',`phone_no`='$phone_no',`address`='$address',`select_date`='$date',`description`='$description',`user_id` = '$user_id',`image`='$file',`crm_type`='$crm_type' WHERE id = '$vid'");

            $timeline = new Timeline();
            $timeline->customer_id = $cid;
            $timeline->section = 'visits_edit';
            $timeline->section_id = $vid;
            $timeline->crm_type = $crm_type;
            $luser = User::where('secure', Session::get('crm'))->first();
            $timeline->created_by = $luser->u_id;
            $timeline->date = date('Y-m-d h:i:s');
            $timeline->save();

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>Visit Update Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }

        if (isset($_POST['add_notes'])) {

            // dd($_POST);
            $file = '';
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()) . '.' . request()->file('image')->getClientOriginalExtension();
                $path = base_path() . '/assets/crm/images/notes/';
                request()->file('image')->move($path, $file);
                $img = Image::make($path . $file)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path . $file);
            }
            $cid = $_POST['crm_cust_id'];
            $data['crm_customer_id'] = $cid;
            $data['select_date'] = $_POST['select_date'];
            $data['description'] = $_POST['description'];
            $data['image'] = $file;
            $data['crm_type'] = $crm_type;
            $data['edited_by'] = $user->u_name;

            $id = DB::table('crm_notes')->insertGetId($data);

            $timeline = new Timeline();
            $timeline->customer_id = $cid;
            $timeline->section = 'notes';
            $timeline->section_id = $id;
            $timeline->crm_type = $crm_type;
            $luser = User::where('secure', Session::get('crm'))->first();
            $timeline->created_by = $luser->u_id;
            $timeline->date = date('Y-m-d h:i:s', strtotime($data['select_date']));
            $timeline->save();

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Notes Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }

        if (isset($_POST['add_refferer'])) {
            //dd($_POST);
            $cid = $_POST['crm_cust_id'];
            $totalnotecount = isset($_POST['totalnotecount']) ? $_POST['totalnotecount'] : '';
            $pid = isset($_POST['pro_id']) ? $_POST['pro_id'] : '';
            $category = isset($_POST['category']) ? $_POST['category'] : '';
            $brand = isset($_POST['brand']) ? $_POST['brand'] : '';
            $moquantity = isset($_POST['moquantity']) ? $_POST['moquantity'] :'0';
            $supplier = isset($_POST['supplier']) ? $_POST['supplier'] : '';
            $cat = isset($_POST['cat']) ? $_POST['cat'] : '';
            $bran = isset($_POST['bran']) ? $_POST['bran'] : '';
            $moquant = isset($_POST['moquant']) ? $_POST['moquant'] : '0';
            $supr = isset($_POST['supr']) ? $_POST['supr'] : '';
            $payCondition = isset($_POST['payCondition']) ? $_POST['payCondition'] : '';
            $payDelay = isset($_POST['payDelay']) ? $_POST['payDelay'] : '';
            $orderLift = isset($_POST['orderLift']) ? $_POST['orderLift'] : '';
            $creditLimit = isset($_POST['creditLimit']) ? $_POST['creditLimit'] : '';
            $notes = isset($_POST['notes']) ?implode(',',$_POST['notes']) : '';
            $addCharge = isset($_POST['addonCharges']) ?implode(',',$_POST['addonCharges']) : '';
            $referred_by = isset($_POST['listcust'])?implode(',',$_POST['listcust']) : '';
            $referrer = isset($_POST['referrer'])?$_POST['referrer']: '';

            //Update on Customer
            $customer = CrmCustomer::find($cid);
            $customer->referred_by = $referred_by;
            $customer->note = $notes;
            $customer->referrer = $referrer;
            $customer->save();

            //Copy referrer on Master Section
            $checkReferrer = ReferrerMaster::where('crm_customer_id', $cid)->first();
            if ($checkReferrer) {
                $checkReferrer->crm_customer_id = $cid;
                $checkReferrer->name = $customer->name;
                $checkReferrer->email = $customer->email;
                $checkReferrer->contact = $customer->contact_no;
                $checkReferrer->address = $customer->postal_address;
                $checkReferrer->relation = '';
                $checkReferrer->save();
            } else {
                $referrermas = new ReferrerMaster();
                $referrermas->crm_customer_id = $cid;
                $referrermas->name = $customer->name;
                $referrermas->email = $customer->email;
                $referrermas->contact = $customer->contact_no;
                $referrermas->address = $customer->postal_address;
                $referrermas->relation = '';
                $referrermas->save();
            }
            if (!empty($pid)) {
                $cpcid = CrmProduct::whereIn('id', $pid)->where('crm_type', $crm_type)->get();
            }
            if (!empty($cpcid) && count($cpcid) > 0) {
                foreach ($cpcid as $key => $value) {
                    $key = $key + 1;
                    $cpcid = CrmProduct::find($value->id);
                    $cpcid->crm_customer_id = $cid;
                    $cpcid->product_id = !empty($category[$key])?implode(',', $category[$key]):'';
                    $cpcid->description = '';
                    $cpcid->image = '';
                    $cpcid->edited_by = $user->u_id;
                    $cpcid->brands = !empty($brand[$key])?implode(',', $brand[$key]):'';
                    $cpcid->mothly_quan = implode(',', $moquantity[$key]);
                    $cpcid->supplier_id = !empty($supplier[$key])?implode(',', $supplier[$key]):'';
                    $cpcid->pay_condition = $payCondition;
                    $cpcid->pay_delay = $payDelay;
                    $cpcid->order_lifting = $orderLift;
                    $cpcid->credit_limit = $creditLimit;
                    if(!empty($addCharge)){
                        $cpcid->add_on_charge = $addCharge;
                    }
                    $cpcid->status = '1';
                    $cpcid->save();
                }
            }
            if (!empty($moquant) && count($moquant) > 0) {
                foreach ($moquant as $key2 => $value) {
                    $prods = new CrmProduct();
                    $prods->crm_customer_id = $cid;
                    $prods->product_id = !empty($cat[$key2])?implode(',', $cat[$key2]):'';
                    $prods->description = '';
                    $prods->image = '';
                    $prods->edited_by = $user->u_id;
                    $prods->brands = !empty($bran[$key2])?implode(',', $bran[$key2]):'';
                    $prods->mothly_quan = implode(',', $value);
                    $prods->supplier_id = !empty($supr[$key2])?implode(',', $supr[$key2]):'';
                    $prods->pay_condition = $payCondition;
                    $prods->pay_delay = $payDelay;
                    $prods->order_lifting = $orderLift;
                    $prods->credit_limit = $creditLimit;
                    if(!empty($addCharge)){
                        $prods->add_on_charge = $addCharge;
                    }
                    $prods->status = '1';
                    $prods->save();
                }
            }
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>Referrer Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';
            return back()->with('notices', $notices);
        }

        if(isset($_POST['edit_note'])){
            $cid = $_POST['crm_customer_id'];
            $nid = $_POST['note_id'];
            $date = $_POST['select_date'];
            $description = $_POST['description'];
            DB::update("UPDATE `crm_notes` SET `select_date`='$date',`description`='$description',`crm_type`='$crm_type' WHERE id = '$nid'");
            $timeline = new Timeline();
            $timeline->customer_id = $cid;
            $timeline->section = 'notes_edit';
            $timeline->section_id = $nid;
            $timeline->crm_type = $crm_type;
            $luser = User::where('secure', Session::get('crm'))->first();
            $timeline->created_by = $luser->u_id;
            $timeline->date = date('Y-m-d h:i:s');
            $timeline->save();
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>Note Update Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }

        if(isset($_POST['add_project'])){

            $file = '';
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/crm/images/projects/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
            }
            $cid = $_POST['crm_cust_id'];
            $data['crm_customer_id'] = $cid;
            $data['project_name'] = $_POST['pname'];
            $data['project_type'] = $_POST['ptype'];
            $data['project_subtype'] = implode(',',$_POST['pstype']);
            $data['sqft'] = $_POST['sqft'];
            $data['project_status'] = $_POST['pstatus'];
            $data['state'] = $_POST['state'];
            $data['district'] = $_POST['district'];
            $data['locality'] = !empty($_POST['locality'])?$_POST['locality']:0;

//             $data['block'] = $_POST['block'];

            $data['block']  = (isset($_POST['block'])) ? $_POST['block'] : '0';

            $data['project_address'] = $_POST['paddress'];
            $data['person_name'] = $_POST['cpname'];
            $data['person_phone'] = $_POST['cpnumber'];
            $data['person_designation'] = $_POST['cpdesignation'];
            $data['description'] = $_POST['description'];
            $data['image'] = $file;
            $data['edited_by'] = $user->u_name;
            $data['crm_type'] = $crm_type;
//             dd($data);


            $id = DB::table('crm_customer_projects')->insertGetId($data);


            $timeline = new Timeline();
            $timeline->customer_id = $cid;
            $timeline->section = 'projects';
            $timeline->section_id = $id;
            $timeline->crm_type = $crm_type;
            $luser = User::where('secure', Session::get('crm'))->first();
            $timeline->created_by = $luser->u_id;
            $timeline->date = date('Y-m-d h:i:s');
            $timeline->save();

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Projects Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        if(isset($_POST['edit_project'])){

            //dd($_POST);
            $pid = $_POST['project_id'];
            $cid = $_POST['crm_customer_id'];
            $project_name = $_POST['pname'];
            $project_type = $_POST['ptype'];
            $project_subtype = implode(',',$_POST['pstype']);
            $sqft = $_POST['sqft'];
            $project_status = $_POST['pstatus'];
            $state = $_POST['state'];
            $district = $_POST['district'];
            $locality = $_POST['locality'];
            $block = $_POST['block'];
            $project_address = $_POST['paddress'];
            $person_name = $_POST['cpname'];
            $person_phone = $_POST['cpnumber'];
            $person_designation = $_POST['cpdesignation'];
            $description = $_POST['description'];


            DB::update("UPDATE `crm_customer_projects` SET `project_name`='$project_name',`project_type`='$project_type',`project_subtype`='$project_subtype',`block`='$block',`sqft`='$sqft',`project_status`='$project_status',`project_address`='$project_address',`person_name`='$person_name',`person_phone`='$person_phone',`person_designation`='$person_designation',`description`='$description', `state`='$state', `district`='$district', `locality`='$locality', `crm_type`='$crm_type' WHERE id = '$pid'");

            $timeline = new Timeline();
            $timeline->customer_id = $cid;
            $timeline->section = 'projects_edit';
            $timeline->section_id = $pid;
            $timeline->crm_type = $crm_type;
            $luser = User::where('secure', Session::get('crm'))->first();
            $timeline->created_by = $luser->u_id;
            $timeline->date = date('Y-m-d h:i:s');
            $timeline->save();

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p> Project Update Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }

        if(isset($_POST['add_promotional'])){

            $cid = $_POST['crm_cust_id'];
            $data['crm_customer_id'] = $cid;
            $data['select_date'] = $_POST['select_date'];
            $data['description'] = $_POST['description'];
            $data['edited_by'] = $user->u_name;
            $data['crm_type'] = $crm_type;
            $id = DB::table('crm_promotional')->insertGetId($data);

            $timeline = new Timeline();
            $timeline->customer_id = $cid;
            $timeline->section = 'promotional';
            $timeline->section_id = $id;
            $timeline->crm_type = $crm_type;
            $luser = User::where('secure', Session::get('crm'))->first();
            $timeline->created_by = $luser->u_id;
            $timeline->date = date('Y-m-d h:i:s', strtotime($data['select_date']));
            $timeline->save();

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Promotional Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        if(isset($_POST['edit_promotional'])){
            //dd($_POST);
            $pid = $_POST['promotional_id'];
            $cid = $_POST['crm_customer_id'];
            $date = $_POST['select_date'];
            $description = $_POST['description'];

            DB::update("UPDATE `crm_promotional` SET `select_date`='$date',`description`='$description',`crm_type`='$crm_type' WHERE id = '$pid'");

            $timeline = new Timeline();
            $timeline->customer_id = $cid;
            $timeline->section = 'promotional_edit';
            $timeline->section_id = $pid;
            $timeline->crm_type = $crm_type;
            $luser = User::where('secure', Session::get('crm'))->first();
            $timeline->created_by = $luser->u_id;
            $timeline->date = date('Y-m-d h:i:s');
            $timeline->save();

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p> Promotion Update Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }

        if(isset($_POST['add_product'])){
            $file = '';
            if (request()->file('image')) {
                // Upload the downloadable file to product downloads directory
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/crm/images/products/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
            }

            $cid = $_POST['crm_cust_id'];
            $data['crm_customer_id'] = $cid;
            $data['product_id'] = implode(',',$_POST['category']);
            if(isset($_POST['price_group'])== ""){
                $data['price_groups'] = "Group Not Selected";
            }else{
                $data['price_groups'] = implode(',',$_POST['price_group']);
            }
            $data['description'] = $_POST['description'];
            $data['brands'] = isset($_POST['brand']) ? implode(',',$_POST['brand']) : '';
            $data['supplier_id'] = isset($_POST['supplier'])? implode(',',$_POST['supplier']) : '';
            $data['image'] = $file;
            $data['edited_by'] = $user->u_name;
            $data['crm_type'] = $crm_type;
            $id = DB::table('crm_products')->insertGetId($data);

            $timeline = new Timeline();
            $timeline->customer_id = $cid;
            $timeline->section = 'products';
            $timeline->section_id = $id;
            $timeline->crm_type = $crm_type;
            $luser = User::where('secure', Session::get('crm'))->first();
            $timeline->created_by = $luser->u_id;
            $timeline->date = date('Y-m-d h:i:s');
            $timeline->save();

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Product Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        if(isset($_POST['edit_product'])){

            $pid = $_POST['product_id'];
            $cid = $_POST['crm_customer_id'];

            $products = implode(',',$_POST['category']);
            if(isset($_POST['price_group'])== ""){
                $price_groups = "Group Not Selected";
            }else{
                $price_groups = implode(',',$_POST['price_group']);
            }
            $description = $_POST['description'];
            $brand = isset($_POST['brand']) ? implode(',',$_POST['brand']) : '';
            $supplier = isset($_POST['supplier'])? implode(',',$_POST['supplier']) : '';

            DB::update("UPDATE `crm_products` SET `product_id`='$products', `price_groups`='$price_groups',`brands` = '$brand' ,`supplier_id` = '$supplier',`description`='$description',`crm_type`='$crm_type' WHERE id = '$pid'");

            $timeline = new Timeline();
            $timeline->customer_id = $cid;
            $timeline->section = 'products_edit';
            $timeline->section_id = $pid;
            $timeline->crm_type = $crm_type;
            $luser = User::where('secure', Session::get('crm'))->first();
            $timeline->created_by = $luser->u_id;
            $timeline->date = date('Y-m-d h:i:s');
            $timeline->save();

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p> Product Update Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        if(isset($_POST['convert_customer'])){

            $cid = $_POST['cid'];
            $user = implode(',',$_POST['user']);
            $date = $_POST['conversion_date'];
            $class = $_POST['convert_customer'];

            DB::update("UPDATE `crm_customers` SET `converted_by`='$user',`conversion_date`='$date',`class`='$class',`crm_type`='$crm_type' WHERE id = '$cid'");
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>Converted Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        if(isset($_POST['add_call'])){
            $cid = $_POST['crm_cust_id'];
            $data['crm_customer_id'] = $cid;
            $data['customer_number'] = $_POST['customer_no'];
            $data['landline_no'] = $_POST['landline_no'];
            $data['conperson_id'] = $_POST['conperson'];
            $data['contact_person'] = $_POST['conperson'];
            $data['contact_designation'] = $_POST['contact-designation'];
            $data['select_date'] = $_POST['select_date'];
            $data['message'] = $_POST['message'];
            $data['user_id'] = implode(',',$_POST['user']);
            $data['edited_by'] = $user->u_name;
            $data['call_type'] = $request->ctype;
            $data['crm_type'] = $crm_type;
            if(!empty($_POST['no_order_reason'])){
                $data['no_order_reason'] = implode(',',$_POST['no_order_reason'])?implode(',',$_POST['no_order_reason']):'';
            }

            if((!empty($_POST['no_order_reason'])) && (!empty($_POST['name']) || !empty($_POST['stage'])  || !empty($_POST['product_category']))){
                $notices .= '<div class="card-alert card red">
                            <div class="card-content white-text">
                              <p>Select No Order Reason Or Pipelines Section Only</p>
                            </div>
                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                          </div>';
            }
            else if(!empty($_POST['name']) && !empty($_POST['stage'])  && !empty($_POST['product_category'])){
                $lo = new LeadOrder();
                $lo->crm_customer_id = $cid;
                $lo->edited_by = $user->u_name;
                $lo->user_id = implode(',',$_POST['user']);
                $lo->pipelines_id = $_POST['name'];
                $lo->pipelines_stage = $_POST['stage'];
                $lo->select_date = $_POST['select_date'];
                $lo->crm_type = $crm_type;
                if (isset($_POST['product_category'])) {
                    $lo->product_category = implode(',', $_POST['product_category']) ? implode(',', $_POST['product_category']) : '';
                }
                if (isset($_POST['lead_no_order_reason'])) {
                    $lo->no_order_reason = implode(',', $_POST['lead_no_order_reason']) ? implode(',', $_POST['lead_no_order_reason']) : '';
                }

                $lo->save();
                $leadid =  $lo->id;
                $data['lead_id'] = $leadid;
                $id = DB::table('crm_call')->insertGetId($data);

                $timeline = new Timeline();
                $timeline->customer_id = $cid;
                $timeline->section = 'calls';
                $timeline->section_id = $id;
                $timeline->crm_type = $crm_type;
                $luser = DB::table('user')->where('secure', Session::get('crm'))->first();
                $timeline->created_by = $luser->u_id;
                $timeline->date = date('Y-m-d h:i:s', strtotime($data['select_date']));
                $timeline->save();

                $notices .= '<div class="card-alert card green">
                    <div class="card-content white-text">
                      <p>New Call Added Successfully.</p>
                    </div>
                    <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>';
            }
            else if((!empty($_POST['no_order_reason'])) && (empty($_POST['name']) || empty($_POST['stage'])  || empty($_POST['product_category']))){

                $id = DB::table('crm_call')->insertGetId($data);
                $timeline = new Timeline();
                $timeline->customer_id = $cid;
                $timeline->section = 'calls';
                $timeline->section_id = $id;
                $timeline->crm_type = $crm_type;
                $luser = User::where('secure', Session::get('crm'))->first();
                $timeline->created_by = $luser->u_id;
                $timeline->date = date('Y-m-d h:i:s', strtotime($data['select_date']));
                $timeline->save();

                $notices .= '<div class="card-alert card green">
                    <div class="card-content white-text">
                      <p>New Call Added Successfully.</p>
                    </div>
                    <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>';
            }
            else{
                $notices .= '<div class="card-alert card red">
                        <div class="card-content white-text">
                          <p>Select Pipeline Sections</p>
                        </div>
                        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                        </button>
                      </div>';

            }


            return back()->with('notices', $notices);
        }
        if(isset($_POST['add_lead'])){
            $cid = $_POST['crm_cust_id'];
            $data['crm_customer_id'] = $cid;
            $data['pipelines_id'] = $_POST['name'];
            $data['select_date'] = $_POST['select_date'];
            $data['pipelines_stage'] = $_POST['stage'];
            $data['edited_by'] = $user->u_name;
            $data['user_id'] = $user->u_id;
            $data['crm_type'] = $crm_type;
            if(isset($_POST['product_category'])){
                $data['product_category'] = implode(',',$_POST['product_category'])?implode(',',$_POST['product_category']):'';
            }
            if (isset($_POST['lead_reason'])) {
                $data['no_order_reason'] = implode(',', $_POST['lead_reason']) ? implode(',', $_POST['lead_reason']) : '';
            }
            $id = DB::table('lead_orders')->insertGetId($data);
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Lead Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        if(isset($_POST['add_lead_call'])){

            $cid = $_POST['crm_cust_id'];
            $data['crm_customer_id'] = $cid;
            $data['customer_number'] = $_POST['customer_no'];
            $data['landline_no'] = $_POST['landline_no3'];
            $data['conperson_id'] = $_POST['conperson3'];
            $data['contact_person'] = $_POST['conperson3'];
            $data['contact_designation'] = $_POST['contact-designation3'];
            $data['select_date'] = $_POST['select_date'];
            $data['message'] = $_POST['message'];
            $data['user_id'] = implode(',',$_POST['user']);
            $data['edited_by'] = $user->u_name;
            $data['call_type'] = $request->ctype;
            $data['crm_type'] = $crm_type;
            if(!empty($_POST['no_order_reason'])){
                $data['no_order_reason'] = implode(',',$_POST['no_order_reason'])?implode(',',$_POST['no_order_reason']):'';
            }

            $id = DB::table('crm_call')->insertGetId($data);
            $leadid = DB::table('lead_orders')->insertGetId($data);

            $timeline = new Timeline();
            $timeline->customer_id = $cid;
            $timeline->section = 'calls';
            $timeline->section_id = $id;
            $luser = User::where('secure', Session::get('crm'))->first();
            $timeline->created_by = $luser->u_id;
            $timeline->date = date('Y-m-d h:i:s', strtotime($data['select_date']));
            $timeline->save();

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Call Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        if(isset($_POST['add_lead'])){

            $cid = $_POST['crm_cust_id'];
            $data['crm_customer_id'] = $cid;
            $data['pipelines_id'] = $_POST['name'];
            $data['select_date'] = $_POST['select_date'];
            $data['pipelines_stage'] = $_POST['stage'];
            $data['edited_by'] = $user->u_name;
            $data['user_id'] = $user->u_id;
            $data['crm_type'] = $crm_type;
            if(isset($_POST['product_category'])){
                $data['product_category'] = implode(',',$_POST['product_category'])?implode(',',$_POST['product_category']):'';
            }
            if (isset($_POST['lead_reason'])) {
                $data['no_order_reason'] = implode(',', $_POST['lead_reason']) ? implode(',', $_POST['lead_reason']) : '';
            }
            $id = DB::table('lead_orders')->insertGetId($data);
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Lead Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        if(isset($_POST['add_msg'])){
            $cid = $_POST['crm_cust_id'];
            $data['mobile'] = $_POST['customer_no'];
            $data['message'] = $_POST['message'];
            $data['user_id'] = implode(',',$_POST['user']);
            $cus = implode(',',$_POST['user']);
            $data['edited_by'] = $user->u_name;
            $data['crm_type'] = $crm_type;
            $id = DB::table('crm_message_save')->insertGetId($data);
            /*foreach ($cus as $d) {
                $c = DB::select("SELECT * FROM crm_customers WHERE id = '".$d."'");
                $sms = urlencode($_POST['contact_no']);
                $no = urlencode($c[0]->mobile);
                echo $url = "https://merasandesh.com/api/sendsms?username=smshop&password=Smshop@123&senderid=AALERT&message=" . $sms . "&numbers=" . $no . "&unicode=0";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_FAILONERROR, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $output = curl_exec($ch);
                curl_error($ch);
                curl_close($ch);
            }*/

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New message Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        if(isset($_POST['add_lead_msg'])){
            $cid = $_POST['crm_cust_id'];
            $data['mobile'] = $_POST['lead_customer_no'];
            $data['message'] = $_POST['lead_message'];
            $data['user_id'] = implode(',',$_POST['lead_user']);
            $cus = implode(',',$_POST['user']);
            $data['edited_by'] = $user->u_name;
            $data['crm_type'] = $crm_type;
            $leadids = DB::table('lead_message_logs')->insertGetId($data);
            $id = DB::table('crm_message_save')->insertGetId($data);
            /*foreach ($cus as $d) {
                $c = DB::select("SELECT * FROM crm_customers WHERE id = '".$d."'");
                $sms = urlencode($_POST['contact_no']);
                $no = urlencode($c[0]->mobile);
                echo $url = "https://merasandesh.com/api/sendsms?username=smshop&password=Smshop@123&senderid=AALERT&message=" . $sms . "&numbers=" . $no . "&unicode=0";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_FAILONERROR, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $output = curl_exec($ch);
                curl_error($ch);
                curl_close($ch);
            }*/

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New message Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        if(isset($_POST['edit_call'])){
            $cid = $_POST['crm_customer_id'];
            $caid = $_POST['call_id'];
            $customer_no = $_POST['customer_no'];
            $date = $_POST['select_date'];
            $message = $_POST['message'];
            if(isset($_POST['no_order_reason'])){
                $no_order = implode(',',$_POST['no_order_reason']);
            }
            DB::update("UPDATE `crm_call` SET `customer_number`='$customer_no',`select_date`='$date',`message`='$message',`no_order_reason`='$no_order',`crm_type`='$crm_type' WHERE id = '$caid'");

            $timeline = new Timeline();
            $timeline->customer_id = $cid;
            $timeline->section = 'calls_edit';
            $timeline->section_id = $caid;
            $timeline->crm_type = $crm_type;
            $luser = User::where('secure', Session::get('crm'))->first();
            $timeline->created_by = $luser->u_id;
            $timeline->date = date('Y-m-d h:i:s', strtotime($date));
            $timeline->save();

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>Call Update Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        if(isset($_POST['edit_lead_call'])){
            $cid = $_POST['crm_customer_id'];
            $caid = $_POST['call_id'];
            $customer_no = $_POST['customer_no'];
            $date = $_POST['select_date'];
            $message = $_POST['message'];
            if(isset($_POST['no_order_reason'])){
                $no_order = implode(',',$_POST['no_order_reason']);
            }
            DB::update("UPDATE `crm_call` SET `customer_number`='$customer_no',`select_date`='$date',`message`='$message',`no_order_reason`='$no_order',`crm_type`='$crm_type' WHERE id = '$caid'");

            $timeline = new Timeline();
            $timeline->customer_id = $cid;
            $timeline->section = 'calls_edit';
            $timeline->section_id = $caid;
            $timeline->crm_type = $crm_type;
            $luser = User::where('secure', Session::get('crm'))->first();
            $timeline->created_by = $luser->u_id;
            $timeline->date = date('Y-m-d h:i:s', strtotime($date));
            $timeline->save();

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>Call Update Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        if(isset($_POST['add_on_charge'])){
            $charge = new AddOnCharge();
            $charge->add_on_charges = $_POST['addoncharge'];
            $charge->add_category = implode(',',$_POST['chargecategory']);
            $charge->save();
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>Add on Charges Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        $cfg = $this->cfg;
        $c_id = $_GET['cid'];
        $crm_customer =  CrmCustomer::where('id',$c_id)->get();

        $converted_by= $crm_customer[0]->converted_by?$crm_customer[0]->converted_by:'';

        $user_name= $user->u_id;

        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Customer Data';
        $customers = DB::table('crm_customers')
            ->join('customer_category', 'crm_customers.customer_category', '=', 'customer_category.id')
            ->select('crm_customers.*','customer_category.type')
            ->get();
        $product_category = Category::all();
        $brand = Brand::all();
        $suppliers = CrmSupplier::all();
        $users = User::all();
        $month = date('m');
        $start_date = date('Y-m-01 h:i:s');
        $end_date  = date('Y-m-t h:i:s');
        if(isset($_GET['month'])){
            $month=$_GET['month'];
            $start_date = date('Y-'.$month.'-01 h:i:s');
            $end_date  = date('Y-'.$month.'-t 23:59:59');
        }
        $timeline_data = false;
        if(isset($_GET['cid'])) {
            $timeline_count = Timeline::where('customer_id', $_GET['cid'])->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'DESC')->count();

            if ($timeline_count) {
                $timeline_data = Timeline::where('customer_id', $_GET['cid'])->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'DESC')->get()->groupBy(function ($date) {
                    return Carbon::parse($date->date)->format('d');
                });
            }
        }
        $states = State::where('country_id','=',101)->get();
        $districts = District::all();
        $blocks = Blocks::all();
        $customer  = CrmCustomer::find($cid);
        if(!empty($customer->referred_by)){
            $crmreffer = CrmCustomer::whereIn('id',explode(',',$customer->referred_by))->select('id','name')->get();

        }if(!empty($customer->note)){
            $crmnote =$customer;
        }
        if(isset($_POST['submitCookie'])){
            $crmtype = $_POST['crm_type'];
            setcookie('crmType', $crmtype, time() + (86400 * 30), "/");
            return back();
        }
        return view('crm/customer-profile')->with(compact('header','cfg','tp','footer', 'title','notices','customers','customer','cid','visits','promotionals','product_category','products','calls','brand','suppliers','users','messages', 'month', 'timeline_data', 'states', 'districts','user','user_name','designations','converted_by','blocks','no_order_reasons','sales_pipelines','crmreffer','note_product','crmnote','addon'));
    }
    public function getVisitsDataP(Request $request){
        $crm_type = isset($_COOKIE['crmType'])?$_COOKIE['crmType']:'1';
        $records = CrmVisits::where('crm_customer_id', $request->cid)->where('crm_visits.crm_type',$crm_type)->get();
        return DataTables::of($records)
            ->addColumn('action', function($records) use ($request){
                $html ='<button value="'.$records->id.'" class="btn myred waves-light vdelete" style="padding:0 10px;"><i class="material-icons">delete</i></button><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="visit-edit?id='.$records->id.'&cid='.$request->cid.'"><i class="material-icons">edit</i></a>';
                return $html;
            })
            ->editColumn('image', function($records){

                $images = explode(',', $records->image);
                $html = '';
                foreach($images as $image) {
                    /*$imgdata = base64_decode($image);
                    $f = finfo_open();
                    $mime_type = finfo_buffer($f, $imgdata, FILEINFO_MIME_TYPE);*/
                    $path = '/assets/crm/images/visits/';
                    $html .= '<a style="margin-right: 5px;" href="'.url($path.$image).'"><img height="40px" width="40px" src="'.url($path.$image).'" /></a>';
                    /*finfo_close($f);*/
                }
                return $html;
            })
            ->editColumn('user_id', function($records){
                $ids = explode(',',$records->user_id);
                $st = Arr::pluck(User::whereIn('u_id', $ids)->get(), 'u_name');
                return implode(', ', $st);
            })
            ->rawColumns(['image', 'action'])
            ->make(true);
    }
    public function getProductsData(Request $request){
        $crm_type = isset($_COOKIE['crmType'])?$_COOKIE['crmType']:'1';
        $records = CrmProduct::where('crm_customer_id', $request->cid)->where('crm_products.crm_type',$crm_type)->get();

        return Datatables::of($records)
            ->addColumn('action', function($records) use ($request){
                $html ='<button value="'.$records->id.'" class="btn myred waves-light proddelete" style="padding:0 10px;"><i class="material-icons">delete</i></button><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="product-edit?id='.$records->id.'&cid='.$request->cid.'"><i class="material-icons">edit</i></a>';
                return $html;
            })
            ->editColumn('image', function($records){
                return '<a target="_blank" href="../assets/crm/images/products/'.$records->image.'"><img src="../assets/crm/images/products/'.$records->image.'" width="30px" height="30px"/></a>';
            })
            ->editColumn('product_id', function($records){
                $p_subtype =explode(',',$records->product_id);
                $st = Arr::pluck(Category::whereIn('id', $p_subtype)->get(), 'name');
                return implode(', ', $st);
            })
            ->editColumn('brands', function($records){
                $p_subtype1 = explode(',',$records->brands);
                $st1 = Arr::pluck(Brand::whereIn('id', $p_subtype1)->get(), 'name');
                return implode(', ', $st1);
            })
            ->editColumn('supplier_id', function($records){
                $p_subtype2 = explode(',',$records->supplier_id);
                $st2 = Arr::pluck(CrmSupplier::whereIn('id', $p_subtype2)->get(), 'name');
                return implode(', ', $st2);
            })
            ->editColumn('price_groups', function($records){
                $p_subtype3 = explode(',',$records->price_groups);
                $st3 = Arr::pluck(CategoryGroup::whereIn('id', $p_subtype3)->get(), 'group_name');
                return implode(', ', $st3);
            })->editColumn('created_at', function($records){
                return $records->created_date;
            })
            ->rawColumns(['image', 'action'])
            ->make(true);
    }
    public function getProjectsData(Request $request){
        $crm_type = isset($_COOKIE['crmType'])?$_COOKIE['crmType']:'1';
        $records = CrmCustomerProject::join('crm_project_type', 'crm_project_type.id', '=', 'crm_customer_projects.project_type')
            ->where('crm_customer_id', $request->cid)
            ->where('crm_customer_projects.crm_type',$crm_type)
            ->select(['crm_customer_projects.id', 'crm_customer_projects.project_name', 'crm_project_type.project_type', 'crm_customer_projects.project_subtype', 'sqft', 'description', 'image','edited_by']);

        return Datatables::of($records)
            ->addColumn('action', function($records) use ($request){
                $html ='<button value="'.$records->id.'" class="btn myred waves-light pdelete" style="padding:0 10px;"><i class="material-icons">delete</i></button><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="project-edit?id='.$records->id.'&cid='.$request->cid.'"><i class="material-icons">edit</i></a>';
                return $html;
            })
            ->editColumn('image', function($records){
                return '<a target="_blank" href="../assets/crm/images/projects/'.$records->image.'"><img src="../assets/crm/images/projects/'.$records->image.'" width="30px" height="30px"/></a>';
            })
            ->editColumn('project_subtype', function($records){
                $p_subtype = explode(',',$records->project_subtype);
                $st = Arr::pluck(CrmProjectSubtype::whereIn('id', $p_subtype)->get(), 'project_subtype');
                return implode(', ', $st);
            })
            ->rawColumns(['image', 'action'])
            ->make(true);
    }
    public function getPromoData(Request $request){
        $crm_type = isset($_COOKIE['crmType'])?$_COOKIE['crmType']:'1';
        $records = CrmPromotional::where('crm_customer_id', $request->cid)->where('crm_promotional.crm_type',$crm_type)->get();
        return Datatables::of($records)
            ->addColumn('action', function($records) use ($request){
                $html ='<button value="'.$records->id.'" class="btn myred waves-light prodelete" style="padding:0 10px;"><i class="material-icons">delete</i></button><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="promotinal-edit?id='.$records->id.'&cid='.$request->cid.'"><i class="material-icons">edit</i></a>';
                return $html;
            })
            ->rawColumns(['action'])
            ->make(true);
    }
    public function getNotesData(Request $request){
        $crm_type = isset($_COOKIE['crmType'])?$_COOKIE['crmType']:'1';
        $record1 = CrmNotes::where('crm_customer_id', $request->cid)->where('crm_notes.crm_type',$crm_type)->get();
        $record2 = CrmProduct::where('crm_customer_id', $request->cid)->where('crm_products.crm_type',$crm_type)->where('crm_products.status','!=','1')->select('id','created_date as select_date','description','edited_by','crm_type','crm_customer_id')->get();
        $records = $record1->merge($record2);
        return Datatables::of($records)
            ->addColumn('action', function($records) use ($request){
                $html ='<button value="'.$records->id.'" class="btn myred waves-light ndelete" style="padding:0 10px;"><i class="material-icons">delete</i></button><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="note-edit?id='.$records->id.'&cid='.$request->cid.'"><i class="material-icons">edit</i></a>';
                return $html;
            })->editColumn('image', function($records){
                return '<a target="_blank" href="../assets/crm/images/notes/'.$records->image.'"><img src="../assets/crm/images/notes/'.$records->image.'" width="30px" height="30px"/></a>';
            })
            ->rawColumns(['image', 'action'])
            ->make(true);
    }
    public function getTicketsData(Request $request){
        $crm_type = isset($_COOKIE['crmType'])?$_COOKIE['crmType']:'1';
        $records = CrmTickets::join('user','user.u_id','=','crm_tickets.assign_to')->select('crm_tickets.*','user.u_name')
            ->where('crm_customer_id', $request->cid)->where('crm_tickets.crm_type',$crm_type)->get();
        return Datatables::of($records)
            ->addColumn('action', function($records) use ($request){
                $html ='<button value="'.$records->id.'" class="btn myred waves-light tdelete" style="padding:0 10px;"><i class="material-icons">delete</i></button><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="ticket-edit?id='.$records->id.'&cid='.$request->cid.'"><i class="material-icons">edit</i></a>';
                return $html;
            })
            ->editColumn('assign_to', function($records){
                return $records->u_name;
            })
            ->editColumn('image', function($records){
                return '<a target="_blank" href="../assets/crm/images/tickets/'.$records->image.'"><img src="../assets/crm/images/tickets/'.$records->image.'" width="30px" height="30px"/></a>';
            })
            ->rawColumns(['image', 'action'])
            ->make(true);
    }
    public function getCallData(Request $request){
        $crm_type = isset($_COOKIE['crmType'])?$_COOKIE['crmType']:'1';
        $records = CrmCall::where('crm_customer_id', $request->cid)->where('crm_call.crm_type',$crm_type)->get();
        return Datatables::of($records)
            ->addColumn('action', function($row) use ($request){
                if(!empty($row->lead_id)){
                    $html = '<button value="'.$row->id.'" data-id="'.$row->lead_id.'"  class="btn myred waves-light calldelete" style="padding:0 10px;"><i class="material-icons">delete</i></button><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px"  href="lead_order-edit?id='.$row->lead_id.'&cid='.$request->cid.'"><i class="material-icons">edit</i></a>';
                }else{
                    $html ='<button value="'.$row->id.'"  class="btn myred waves-light calldelete" style="padding:0 10px;"><i class="material-icons">delete</i></button><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px"  href="call-edit?id='.$row->id.'&cid='.$request->cid.'"><i class="material-icons">edit</i></a>';
                }
                return $html;
            })
            ->editColumn('user_id', function($row){
                $ids = explode(',',$row->user_id);
                $st = Arr::pluck(User::whereIn('u_id', $ids)->get(), 'u_name');
                return implode(', ', $st);
            })->editColumn('contact_person', function($row){
                $name = Arr::pluck(CrmCustomer::where('id',$_GET['cid'])->get(),'name');
                $sec_name = Arr::pluck(CrmCustomer::where('id',$_GET['cid'])->get(),'sec_names');
                if(isset($_GET['cid']) &&  $_GET['cid'] === $row->contact_person || empty($sec_name[0])){
                    return $name;
                }else{
                    $names = unserialize($sec_name[0]);
                    if($names){
                        foreach($names as $key=>$nam){
                            if($key == $row->contact_person){
                                return $nam;
                            }
                        }
                    }
                    return  'NA';
                }
            })->editColumn('customer_number', function($row){
                return $row->customer_number;
            })->editColumn('contact_designation', function($row){
                if($row->contact_designation){
                    return $row->contact_designation;
                }
                return 'NA';
            })->editColumn('user_id', function($row){
                $ids = explode(',',$row->user_id);
                $st = Arr::pluck(User::whereIn('u_id', $ids)->get(), 'u_name');
                return implode(', ', $st);
            })->editColumn('pipeline', function($row){
                if(!empty($row->lead_id)){
                    $leads = Arr::pluck(LeadOrder::where('lead_orders.id',$row->lead_id)->get(),'pipelines_id');
                    if(!empty($leads)){
                        $pipelinename = Arr::pluck(SalesPipeline::where('id',$leads[0])->get(),'name');
                        if(!empty($pipelinename)){
                            return $pipelinename[0];
                        }else{
                            return 'NA';
                        }
                    }else{
                        return 'NA';
                    }
                }else{
                    return 'NA';
                }

                return 'NA';
            })
            ->rawColumns(['action'])

            ->make(true);
    }
    public function getMessageData(Request $request){
        $crm_type = isset($_COOKIE['crmType'])?$_COOKIE['crmType']:'1';
        $customer = CrmCustomer::join('customer_category', 'crm_customers.customer_category', '=', 'customer_category.id')
            ->where('crm_customers.id','=',$request->cid)->where('crm_customers.crm_type','=',$crm_type)->select(['crm_customers.*', 'crm_customers.type as type_id','customer_category.type'])->first();

        $number = $customer->contact_no;

        $records = CrmMessageSave::whereRaw('FIND_IN_SET(?, mobile)', $number);

        return Datatables::of($records)
            ->editColumn('user_id', function($records){
                $ids = explode(',',$records->user_id);
                $st = Arr::pluck(User::whereIn('u_id', $ids)->get(), 'u_name');
                return implode(', ', $st);
            })
            ->make(true);
    }
    public function getLeaveOrderData(Request $request){
        $crm_type = isset($_COOKIE['crmType'])?$_COOKIE['crmType']:'1';
        $records = LeadOrder::where('crm_customer_id', $request->cid)->where('lead_orders.crm_type',$crm_type)->get();
        return Datatables::of($records)
            ->addColumn('action', function($records) use ($request){
                $html ='<button value="'.$records->id.'" class="btn myred waves-light leaddelete" style="padding:0 10px;"><i class="material-icons">delete</i></button><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="lead_order-edit?id='.$records->id.'&cid='.$request->cid.'"><i class="material-icons">edit</i></a>';
                return $html;
            })
            ->editColumn('pipeline', function($records){
                if(!empty($records->pipelines_id)){
                    $pipeline = SalesPipeline::where('id',$records->pipelines_id)->first();
                    if(!empty($pipeline)){
                        return $pipeline->name;
                    }else{
                        return 'NA';
                    }

                }else{
                    return 'NA';
                }


            })->editColumn('stage', function($records){
                return $records->pipelines_stage;
            })->editColumn('created_date', function($records){
                return $records->created_date;
            })
            ->rawColumns(['action'])
            ->make(true);
    }
    public function ConvertedCustomers( Request $request){
        $user = User::where('secure', Session::get('crm'))->first();
        if(!checkRole($user->u_id,"conv_cst")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';

        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Converted Customers';
        $customer_category = CustomerCategory::all();
        $states = State::where('country_id','=',101)->get();
        $start_date = date('Y-01-01 00:00:00');
        $end_date = date('Y-m-d 23:59:59');
        $s_districts = array();
        if(isset($_GET['edit'])) {
            $cdata = CrmCustomer::find($_GET['edit']);
            $s_districts = District::swhere('state_id','=',$cdata->state)->get();
        }
        $customer_class = CrmCustomer::all();
        $users = User::all();

        return view('crm/converted-customers')->with(compact('header','cfg','tp','footer', 'title','notices','customer_category','states', 's_districts', 'customer_class', 'users','start_date','end_date', 'buttons'));
    }
    public function getConvertedCustomers(Request $request){
        $records = CrmCustomer::join('customer_category', 'customer_category.id', '=', 'crm_customers.customer_category')
            ->select('crm_customers.id', 'crm_customers.name','crm_customers.converted_by','crm_customers.managed_by','crm_customers.contact_no', 'customer_category.type', DB::raw('(select name from states where states.id = crm_customers.state) as state_name') , DB::raw('(select name from district where district.id = crm_customers.district) as district_name'), DB::raw('(select locality from locality where locality.id = crm_customers.locality) as locality_name'), 'crm_customers.conversion_date', 'crm_customers.verified')
            ->where('crm_customers.class', 4);
        $category = (isset($_GET["category"])) ? $_GET["category"] : false;
        $state = (isset($_GET["state"])) ? ($_GET["state"]) : false;
        $district = (isset($_GET["district"])) ? ($_GET["district"]) : false;
        $converted_by = (isset($_GET["converted_by"])) ? ($_GET["converted_by"]) : false;
        $dates = (isset($_GET["dates"])) ? ($_GET["dates"]) : false;

        if($category){
            $records = $records->whereIn('customer_category', $category);
        }
        if($state){
            $records = $records->whereIn('state', $state);
        }
        if($district){
            $records = $records->whereIn('district', $district);
        }
        if($converted_by){
            if($converted_by !== 'all')
                $records = $records->whereRaw('FIND_IN_SET(?, crm_customers.converted_by)', $converted_by);
        }

        if($dates){
            if($dates == '$$'){
                $dates = date('Y-m-d 00:00:00',strtotime('1th January 2019')).'$$'.date('Y-m-d 23:59:59');
            }
            $date_range = explode('$$', $dates);
            $records->whereBetween('crm_customers.conversion_date', [$date_range[0].' 00:00:00', $date_range[1].' 23:59:59']);
        }else{
            $dates = date('Y-m-d 00:00:00').'$$'.date('Y-m-d 23:59:59');
            $date_range = explode('$$', $dates);
            $records->whereBetween('crm_customers.conversion_date', [$date_range[0], $date_range[1]]);
        }if(isset($_COOKIE['crmType'])){
            $records = $records->where('crm_customers.crm_type',$_COOKIE['crmType']);
        }

        //$records = $records->orderBy('crm_customers.id', 'asc');
        return Datatables::of($records)
            ->addColumn('action', function($row){
                $a="converted-customers";
                $html = '<a href="customer-profile?cid='.$row->id.'&link='.$a.'" class="btn myblue waves-light" style="padding: 0 1rem"><i class="material-icons">account_circle</i></a>';
                return $html;
            })
            ->editColumn('converted_by', function($row){
                $users = explode(',', $row->converted_by);
                $u = Arr::pluck(User::whereIn('u_id', $users)->get(), 'u_name');

                return implode(',', $u);
            })
            ->editColumn('managed_by', function($row){
                $dex = array();
                $user = explode(',', $row->managed_by);
                $u = Arr::pluck(User::whereIn('u_id', $user)->get(), 'u_name');
                $d = DB::table('customer_designation')->get();
                foreach ($d as $des) {
                    if (in_array('des' . $des->id, $user)) {
                        $dex[] = $des->name;
                    }
                }
                $data =  array_merge($dex,$u);
                return implode(',',$data);
            })
            ->editColumn('name', function($records){
                $html = '';
                if($records->verified){
                    $html = $records->name.' <i class="material-icons" style="font-size: 16px;color: blue;">verified_user</i>';
                }else{
                    $html = $records->name;
                }
                return $html;
            })
            ->editColumn('conversion_date', function($records){
                return date("d-m-Y", strtotime($records->conversion_date));
            })
            ->rawColumns(['name', 'action'])
            ->make(true);
    }
    public function getCustomersListData(Request $request){
        if(isset($_POST['q'])){
            $search  = $_POST['q'];
            $customers = CrmCustomer::where('name', 'LIKE', '%'.$search.'%')->select('id','name')->get();
            $referedby = ReferrerMaster::where('name', 'LIKE', '%'.$search.'%')->select('id','name')->get();
            $data = $customers->merge($referedby);
            echo json_encode($data);
        }
    }
}