<?php

namespace App\Http\Controllers\Crm;

use App\CategoryGroup;
use App\CrmUserManager;
use App\CustomerCategory;
use App\CustomerClass;
use App\CustomerType;
use App\District;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Crm;
use App\Locality;
use App\State;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class SmsController extends Crm
{
    //
    public function Sms()
    {
        $user = User::where('secure', Session::get('crm'))->first();
        if (!checkRole($user->u_id, "camp_sms")) {
            return redirect()->to('crm/index')->with('ermsg', 'You don\'t have access to this section.');
        }

        $sid = '';
        $did = '';
        $lid = '';
        $notices = '';
        $category_set = '';
        $type_set = '';
        $class_set = '';
        $district = array();
        $locality = array();
        $cid = '';
        $acc_manager = '';
        $pr_groups = '';
        $type = '';
        $class = '';
        $visited = 'all';
        $states = State::where('country_id', '=', 101)->get();
        $custcategory = CustomerCategory::all();
        $custtype = CustomerType::all();
        $users = CrmUserManager::all();
        $cat_groups = CategoryGroup::join('category', 'category.id', '=', 'category_groups.category_id')
            ->select(['category_groups.id as id', 'group_name', 'category.name'])
            ->get();

        $customers = array();
        if (isset($_POST['find'])) {
            $sid = implode(',', $_POST['state']);
            $district = District::whereIn('state_id', $_POST['state'])->get();
            if (isset($_POST['state'])) {
                $sid = implode(',', $_POST['state']);
                $where[] = ' state IN (' . $sid . ') ';
            }
            if (isset($_POST['district'])) {
                $locality = Locality::whereIn('district_id', Arr::pluck($district, 'id'))->get();
                $where = array();
                $did = implode(',', $_POST['district']);
                $where[] = ' district IN (' . $did . ') ';
            }
            if (isset($_POST['locality'])) {
                $locale = implode(',', $_POST['locality']);
                $where[] = ' locality IN (' . $locale . ') ';
            }
            if (isset($_POST['category'])) {
                $cid = implode(',', $_POST['category']);
                $where[] = ' customer_category IN (' . $cid . ') ';
            }
            if (isset($_POST['type'])) {
                $type = implode(',', $_POST['type']);
                $where[] = ' type IN (' . $type . ') ';
            }
            if (isset($_POST['class'])) {
                $class = implode(',', $_POST['class']);
                $where[] = ' class IN (' . $class . ') ';
            }
            if (isset($_POST['acc_manager'])) {
                $acc_manager = implode(',', $_POST['acc_manager']);
                $where[] = ' FIND_IN_SET_X(\'' . $acc_manager . '\', assign_users) ';
            }
            if (isset($_POST['price_group'])) {
                $pr_groups = implode(',', $_POST['price_group']);
                $where[] = ' id IN (select crm_customer_id from crm_products where FIND_IN_SET_X(\'' . $pr_groups . '\', price_groups)) ';
            }
            if (isset($_POST['visited'])) {
                $visited = $_POST['visited'];
                if ($visited == 'yes') {
                    $where[] = ' id IN (select crm_customer_id from crm_visits) ';
                }
            }if(isset($_COOKIE['crmType'])){
                $where[] = ' crm_type IN (' . $_COOKIE['crmType'] . ') ';
            }
            $where[] = ' msg_active = 0 ';
            $category_set = $cid;
            $type_set = $type;
            $class_set = $class;
            $customers = DB::select("SELECT * FROM `crm_customers` WHERE " . implode('AND', $where));

        }
        if (isset($_POST['find_district'])) {
            // dd($_POST);
            $sid = implode(',', $_POST['state']);
            $district = District::whereIn('state_id', $_POST['state'])->get();
            $locality = Locality::whereIn('district_id', Arr::pluck($district, 'id'))->get();
        }

        if (isset($_POST['msg_send'])) {
            dd($_POST['msg_send']);
            $msg = $_POST['msg'];
            $cus = implode(',', $_POST['cust']);
            /*if(isset($_POST['cust'])){
                $cus = implode(',',$_POST['cust']);
                foreach ($cus as $d) {
                    $sms = urlencode($msg);
                    $no = urlencode($d);
                    echo $url = "https://merasandesh.com/api/sendsms?username=smshop&password=Smshop@123&senderid=AALERT&message=" . $sms . "&numbers=" . $no . "&unicode=0";
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_FAILONERROR, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                    $output = curl_exec($ch);
                    curl_error($ch);
                    curl_close($ch);
                    if($output) {
                        $notices .= '<div class="card-alert card green">
                            <div class="card-content white-text">
                              <p>Sms sent Successfully.</p>
                            </div>
                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                          </div>';
                    }else {
                        $notices .= '<div class="card-alert card green">
                            <div class="card-content white-text">
                              <p>Failed</p>
                            </div>
                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                          </div>';
                    }
                }
            }else{
                $sms = urlencode($msg);
                $no = urlencode($_POST['new_no']);
                echo $url = "https://merasandesh.com/api/sendsms?username=smshop&password=Smshop@123&senderid=AALERT&message=" . $sms . "&numbers=" . $no . "&unicode=0";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_FAILONERROR, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $output = curl_exec($ch);
                curl_error($ch);
                curl_close($ch);
                if($output) {
                    $notices .= '<div class="card-alert card green">
                        <div class="card-content white-text">
                          <p>Sms sent Successfully.</p>
                        </div>
                        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                        </button>
                      </div>';
                }else {
                    $notices .= '<div class="card-alert card green">
                        <div class="card-content white-text">
                          <p>Failed</p>
                        </div>
                        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                        </button>
                      </div>';
                }
            }*/
        }
        $customer_class = CustomerClass::all();
        $customer_type = CustomerType::all();
        $customer_category = CustomerCategory::all();
        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('CRM', 'sms');
        $footer = $this->footer();
        $title = 'CRM';

        return view('crm/sms')->with(compact('header', 'cfg', 'tp', 'footer', 'notices', 'title', 'states', 'district', 'locality', 'custcategory', 'custtype', 'customers', 'sid', 'did', 'lid', 'category_set', 'type_set', 'class_set', 'customer_class', 'customer_type', 'customer_category', 'users', 'cat_groups', 'acc_manager', 'pr_groups', 'visited'));
    }
}
