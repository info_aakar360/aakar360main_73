<?php

namespace App\Http\Controllers\CRM;

use App\CrmUserRole;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Crm;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class RoleMasterController extends Crm
{
    public function CrmUserRole(Request $request) {
        $user = User::where('secure', Session::get('crm'))->first();
        if(!checkRole($user->u_id,"mas_rol")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $notices = '';
        if (isset($_POST['add'])) {
            $data['role'] = $_POST['role'];
            DB::table('crm_user_role') -> insertGetId($data);
            $notices .= '<div class="card-alert card green">
                          <div class = "card-content white-text" >
                         <p> New Role  Added Successfully. </p>
                         </div>
                         <button type = "button" class = "close white-text" data-dismiss = "alert" aria-label = "Close">
                         <span aria-hidden = "true" >×</span>
                         </button> </div>';
        }

        if (isset($_POST['edit'])) {
            $uid = $_POST['uid'];
            $role = $_POST['role'];
            $permissions = "";
            if(isset($_POST['permission']))
                $permissions = "".implode(",",$_POST['permission'])."";
            DB::update("UPDATE `crm_user_role` SET `role`='$role', `permissions`='$permissions' WHERE id = '$uid'");
            $notices .= '<div class="card-alert card green">
                          <div class = "card-content white-text" >
                         <p> Role  Update Successfully. </p>
                         </div>
                         <button type = "button" class = "close white-text" data-dismiss = "alert" aria-label = "Close">
                         <span aria-hidden = "true" >×</span>
                         </button> </div>';

        }

        $cfg = $this -> cfg;
        $tp = url("/assets/crm/");
        $header = $this -> header('Crm', 'index');
        $footer = $this -> footer();
        $title = 'CRM';
        $roles = CrmUserRole::all();
        return view('crm/user-role') -> with(compact('header', 'cfg', 'tp', 'footer', 'title', 'notices', 'roles'));

    }
}
