<?php

namespace App\Http\Controllers\CRM;

use App\CrmMasterQuickEntry;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Crm;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Svg\Tag\Image;

class QueryEntryMasterController extends Crm
{
    public function crm_quick_entry_report(Request $request){
        $user = User::where('secure', Session::get('crm'))->first();
        if(!checkRole($user->u_id,"mas_qe_rep")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $entries = CrmMasterQuickEntry::all();
        foreach($entries as $entry){
            $final = array();
            $images = explode('<**>', $entry->image);
            foreach($images as $image) {
                try {
                    if(strpos($image, '.')){
                        $finalz = $image;
                    }else {
                        $imgdata = base64_decode($image);
                        if ($imgdata) {
                            $f = finfo_open();
                            $ext = finfo_buffer($f, $imgdata, FILEINFO_MIME_TYPE);
                            finfo_close($f);
                            $filex = md5(time()) . '.' . substr($ext, 6);
                            $path = base_path() . '/assets/crm/images/user/';
                            Image::make($image)->save($path . $filex);
                            $finalz = $filex;
                        }
                    }
                    $final[] = $finalz;
                }catch(\Exception $ex){

                }

            }
            $fx = implode('<**>', $final);
            DB::update("Update crm_master_quick_entry set image='".$fx."' where id=".$entry->id);
        }
        $notices = '';
        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Customer Quick Entry Report';
        $customer_type = DB::table('customer_category')->get();

        if(isset($_POST['name'])){
            $data['name'] = $_POST['name'];
            $data['company_name'] = $_POST['company_name'];
            $data['discription'] = $_POST['description'];
            $data['location'] = $_POST['location'];
            $data['lat'] = $_POST['lat'];
            $data['lng'] = $_POST['lng'];
            $user = User::where('secure', Session::get('crm'))->first();
            $data['created_by'] = $user->u_id;
            $files = array();
            if($request->hasFile('image')) {
                foreach ($request->file('image') as $file){
                    $files[] = $filex = md5(time()).'.'.$file->getClientOriginalExtension();
                    $path = base_path().'/assets/crm/images/user/';
                    $img = Image::make($path.$filex)->save($path.$filex);
                    //$files[] = base64_encode(file_get_contents($file->path()));
                }
                //$data['image'] = base64_encode(file_get_contents($request->file('image')->path()));
            }
            $data['image'] = implode('<**>', $files);

            DB::table('crm_master_quick_entry')->insert($data);
        }
        return view('crm/crm-quick-entry-report') -> with(compact('header', 'cfg', 'tp', 'footer', 'title', 'notices','customer_type', 'buttons'));
    }
}
