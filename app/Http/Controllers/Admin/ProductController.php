<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\CityWise;
use App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\ManufacturingHubs;
use App\PhRelations;
use App\Products;
use App\Category;
use App\ProductType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;

class ProductController extends Admin
{
    public function manufacturingHubs(){
        $notices = '';
        if(isset($_POST['add'])){
            $data['title'] = $_POST['name'];
            ManufacturingHubs::insert($data);
            $notices .= "<div class='alert mini alert-success'>Hub has been added successfully!</div>";
        }
        if(isset($_POST['edit'])){
            $h = ManufacturingHubs::find($_GET['edit']);
            if($h === null){
                return back();
            }
            $h->title = $_POST['name'];
            $h->save();
            $notices .= "<div class='alert mini alert-success'>Hub edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            if(checkHubLinks($_GET['delete'])){
                $notices .= "<div class='alert alert-danger'>Hub is linked with one or more products!</div>";
            }else {
                DB::table("manufacturing_hubs")->where('id', '=', $_GET['delete'])->delete();
                $notices .= "<div class='alert alert-success'>Hub has been deleted successfully !</div>";
            }
        }
        $header = $this->header('Manufacturing Hubs','mhubs');
        $hubs = ManufacturingHubs::orderBy('id', 'DESC')->get();
        $hub = null;
        if(isset($_GET['edit'])) {
            $hub = ManufacturingHubs::find($_GET['edit']);
        }
        $footer = $this->footer();
        $tp = url("/themes/".$this->cfg->theme);
        return view('admin/manufacturing-hubs')->with(compact('header','notices','tp','hubs','hub','footer'));
    }
    public function products(Request $request)
    {
        $data['notices'] = '';
        $data['variants'] = [];
        $cat_name ='';
        $data['ph'] = null;
        if(isset($request->add)){
            $udata['title'] = $request->title;
            $udata['text'] = $request->text;
            $udata['specification'] = $request->specification;
            $udata['purchase_price'] = $request->purchase_price;
            $udata['price'] = $request->price;
            $udata['tax'] = $request->tax;
            $udata['asr'] = $request->asr;
            $udata['product_showprice'] = $request->sp;
            $udata['category'] = (int)$request->category;
            $udata['description_view'] = (int)$request->description_view;
            $udata['specification_view'] = (int)$request->specification_view;
            $udata['rating_view'] = (int)$request->rating_view;
            $udata['faq_view'] = (int)$request->faq_view;
            $udata['show_weight_length'] = (int)$request->show_weight_length;
            $category_id = (int)$request->category;
            $category_detail = Category::select(['parent','name'])->where('id','=',$category_id)->first();
            $product_max_id = DB::select("SELECT max(id) as maxid FROM products")[0];
            $new_pid = $product_max_id->maxid +1;
            $pid=$category_detail->parent;
            $sku_code ='';
            $cat_name =  $category_detail->name;
            $sku_code = strtolower(substr($cat_name, 0, 3));
            if($pid==0){
                $sku_code = $sku_code;
            }else{
                while($pid !=0){
                    $category_detail = Category::where('id','=',$pid)->get('parent','name');
                    $sku_code = strtolower(substr($category_detail->name, 0, 3)).$sku_code;
                    $pid = $category_detail->parent;
                }
            }
            $sku_code = $sku_code.'-'.$new_pid;
            $udata['sku'] = $sku_code;
            $udata['quantity'] = (int)$request->quantity;
            $udata['images'] = '';
            $udata['download'] = '';
            $udata['fb_cat'] = '';
            $udata['hsn'] = $request->hsn;
            $udata['min_qty'] = $request->min;
            $udata['max_qty'] = $request->max;
            $udata['institutional_min_quantity'] = $request->min_ins;
            $udata['sale'] = $request->sale;
            $udata['weight'] = $request->weight;
            $udata['weight_unit'] = $request->weight_unit;
            $udata['brand_id'] = $request->brand_id;
            if(isset($request->fb_cat)){
                $udata['fb_cat'] = implode(',', $request->fb_cat);
            }
            if(isset($request->is_variable)){
                $udata['is_variable'] = $request->is_variable;
            }
            if(isset($request->validity)){
                $udata['validity'] = date('Y-m-d H:i:s',strtotime($request->validity));
            }
            if(isset($request->substract_stock)){
                $udata['sub_stock'] = $request->substract_stock;
            }
            if(isset($_POST['grade'])){
                $udata['grade'] = implode(',', $_POST['grade']);
            }
            $options = array();
            if (isset($_POST['option_title'])){
                $choice_titles = $_POST['option_title'];
                $choice_types = $_POST['option_type'];
                $choice_no = $_POST['option_no'];
                if(count($choice_titles ) > 0){
                    foreach ($choice_titles as $i => $row) {
                        $choice_options = $_POST['option_set'.$choice_no[$i]];
                        $options[] = array(
                            'no' => $choice_no[$i],
                            'title' => $choice_titles[$i],
                            'name' => 'choice_'.$choice_no[$i],
                            'type' => $choice_types[$i],
                            'option' => $choice_options
                        );
                    }
                }
            }
            $udata['options'] = json_encode($options);
            $product = DB::table('products')->insertGetId($udata);
            if (request()->file('images')) {
                $order = 0;
                $images = array();
                foreach (request()->file('images') as $file) {
                    $name = $file->getClientOriginalName();
                    if (in_array($file->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                        $images[] = $image = md5(time()).'-'.$order.'.'.$file->getClientOriginalExtension();
                        $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'products/';
                        $file->move($path,$image);
                        if($image) {
                            $img = Image::make($path . $image)->resize(800, null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($path . $image);
                            $thumb = Image::make($path . $image)->resize(165, 110)->save($path .'thumbs/'. $image);
                            $order++;
                        }
                    } else {
                        $data['notices'] .= "<div class='alert mini alert-warning'> $name is not a valid format</div>";
                    }
                }
                DB::update("UPDATE products SET images = '".implode(',',$images)."' WHERE id = '".$product."'");
            }
            if (request()->file('download')) {
                $name = request()->file('download')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('download')->getClientOriginalExtension();
                $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'downloads';
                request()->file('download')->move($path,$file);
                DB::update("UPDATE products SET download = '".$file."' WHERE id = '".$product."'");
            }
            if(isset($_POST['hubs']) && isset($_POST['price']) && isset($_POST['lprice'])) {
                $data1['products'] = $product;
                $pprices = $_POST['pprice'];
                $prices = $_POST['iprice'];
                $lprices = $_POST['lprice'];
                $ins_quantity = $_POST['ins_quantity'];
                $ins_validity = $_POST['ins_validity'];
                $hubs = array();
                foreach ($_POST['hubs'] as $key => $h) {
                    $hubs[] = array(
                        'hub' => $h,
                        'pprice' => $pprices[$key],
                        'price' => $prices[$key],
                        'loading' => $lprices[$key],
                        'quantity' => $ins_quantity[$key],
                        'validity' => $ins_validity[$key]
                    );
                }
                $data1['hubs'] = json_encode($hubs);
                PhRelations::insert($data1);
            }else{
                PhRelations::where('products', $product)->delete();
            }
            if(isset($_POST['cities']) && isset($_POST['cwprice']) && isset($_POST['cwlprice'])) {
                $cwpprices = $_POST['cwpprice'];
                $cwprices = $_POST['cwprice'];
                $cwlprices = $_POST['cwlprice'];
                $cwins_quantity = $_POST['cwins_quantity'];
                $cwins_validity = $_POST['cwins_validity'];
                $prodata = array();
                $data11['product_id'] = $product;
                foreach ($_POST['cities'] as $key => $h) {
                    $data11['city'] = $h;
                    $prodata[] = array(
                        'product' => $product,
                        'pprice' => $cwpprices[$key],
                        'price' => $cwprices[$key],
                        'loading' => $cwlprices[$key],
                        'quantity' => $cwins_quantity[$key],
                        'validity' => $cwins_validity[$key]
                    );
                }
                $data11['category_id'] = (int)$_POST["category"];
                $data11['prices'] = json_encode($prodata);
                CityWise::insert($data11);
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Product has been added successfully !</div>";
        }
        if(isset($_POST['edit'])){

            $product = Products::find($_GET['edit']);
            $product->title = escape($_POST["title"]);
            $product->text = escape($_POST["text"]);
            $product->specification = escape($_POST["specification"]);
            $product->purchase_price = $_POST["purchase_price"];
            $product->price = $_POST["price"];
            $product->tax = $_POST["tax"];
            $product->asr = $_POST["asr"];
            $product->product_showprice = $_POST["sp"];
            $product->hsn = $_POST["hsn"];
            $product->min_qty = $_POST["min"];
            $product->max_qty = isset($_POST["max"])?$_POST["max"]:'';
            $product->institutional_min_quantity = $_POST["min_ins"];
            $product->sale = $_POST["sale"];
            $product->weight = $_POST["weight"];
            $product->weight_unit = $_POST["weight_unit"];
            $product->quantity = (int)$_POST["quantity"];
            $product->category = (int)$_POST["category"];
            $product->brand_id = (int)$_POST["brand_id"];
            $product->description_view = (int)$_POST['description_view'];
            $product->specification_view = (int)$_POST['specification_view'];
            $product->rating_view = (int)$_POST['rating_view'];
            $product->faq_view = (int)$_POST['faq_view'];
            $product->show_weight_length = (int)$_POST['show_weight_length'];
            $product->is_variable = 0;
            $product->validity = null;
            $product->sub_stock = 0;
            $fb_cat = '';
            if(isset($_POST['fb_cat'])){
                $product->fb_cat = implode(',', $_POST['fb_cat']);
            }
            if(isset($_POST['is_variable'])){
                $product->is_variable = $_POST['is_variable'];
            }
            if(isset($_POST['validity'])){
                $product->validity = date('Y-m-d H:i:s',strtotime($_POST['validity']));
            }
            if(isset($_POST['substract_stock'])){
                $product->sub_stock = $_POST['substract_stock'];
            }
            $grade = '';
            if(isset($_POST['grade'])){
                $product->grade = implode(',', $_POST['grade']);
            }
            $options = array();
            if (isset($_POST['option_title'])){
                $choice_titles = $_POST['option_title'];
                $choice_types = $_POST['option_type'];
                $choice_no = $_POST['option_no'];
                if(count($choice_titles ) > 0){
                    foreach ($choice_titles as $i => $row) {
                        $choice_options = $_POST['option_set'.$choice_no[$i]];
                        $options[] = array(
                            'no' => $choice_no[$i],
                            'title' => $choice_titles[$i],
                            'name' => 'choice_'.$choice_no[$i],
                            'type' => $choice_types[$i],
                            'option' => $choice_options
                        );
                    }
                }
            }
            $product->options = json_encode($options);
            $product->save();
            if (request()->file('images')) {
                $order = 0;
                $images = array();
                foreach (request()->file('images') as $file) {
                    $name = $file->getClientOriginalName();
                    if (in_array($file->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                        $images[] = $image = md5(time()).'-'.$order.'.'.$file->getClientOriginalExtension();
                        $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'products/';
                        $file->move($path,$image);
                        if($image) {
                            $img = Image::make($path . $image)->resize(800, null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($path . $image);
                            $thumb = Image::make($path . $image)->resize(165, 110)->save($path .'thumbs/'. $image);
                            $order++;
                        }
                    } else {
                        $data['notices'] .= "<div class='alert mini alert-warning'> $name is not a valid format</div>";
                    }
                }
                DB::update("UPDATE products SET images = '".implode(',',$images)."' WHERE id = '".$_GET['edit']."'");
            }
            if (request()->file('download')) {
                $name = request()->file('download')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('download')->getClientOriginalExtension();
                $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'downloads';
                request()->file('download')->move($path,$file);
                DB::update("UPDATE products SET download = '".$file."' WHERE id = '".$_GET['edit']."'");
            }
            if(isset($_POST['hubs']) && isset($_POST['price']) && isset($_POST['lprice'])) {
                $data1['products'] = $_GET['edit'];
                $prices = $_POST['iprice'];
                $lprices = $_POST['lprice'];
                $ins_quantity = $_POST['ins_quantity'];
                $ins_validity = $_POST['ins_validity'];
                $hubs = array();
                foreach ($_POST['hubs'] as $key => $h) {
                    $qty = '';
                    if($key<1){
                        $qty = $ins_quantity[$key];
                    }
                    $hubs[] = array(
                        'hub' => $h,
                        'price' => $prices[$key],
                        'loading' => $lprices[$key],
                        'quantity' => $qty,
                        'validity' => $ins_validity[$key]
                    );
                }
                $ph = PhRelations::where('products', $_GET['edit'])->first();
                if($ph === null) {
                    $data1['hubs'] = json_encode($hubs);
                    PhRelations::insert($data1);
                }else{
                    $ph->products = $_GET['edit'];
                    $ph->hubs = json_encode($hubs);
                    $ph->save();
                }
            }else{
                PhRelations::where('products', $_GET['edit'])->delete();
            }
            if(isset($_POST['cities']) && isset($_POST['cwprice']) && isset($_POST['cwlprice'])) {
                $citiesData = CityWise::where('product_id', $_GET['edit'])->delete();
                if($citiesData) {
                    $cwpprices = $_POST['cwpprice'];
                    $cwprices = $_POST['cwprice'];
                    $cwlprices = $_POST['cwlprice'];
                    $cwins_quantity = $_POST['cwins_quantity'];
                    $cwins_validity = $_POST['cwins_validity'];
                    $prodata = array();
                    $data12['product_id'] = $_GET['edit'];
                    foreach ($_POST['cities'] as $key => $h) {
                        if (!empty($cwpprices[$key]) && !empty($cwprices[$key]) && !empty($cwlprices[$key]) && !empty($cwins_quantity[$key]) && !empty($cwins_validity[$key])) {
                            $data12['city'] = $h;
                            $prodata[] = array(
                                'product' => $_GET['edit'],
                                'pprice' => $cwpprices[$key],
                                'price' => $cwprices[$key],
                                'loading' => $cwlprices[$key],
                                'quantity' => $cwins_quantity[$key],
                                'validity' => $cwins_validity[$key]
                            );
                        }
                    }
                    $data12['category_id'] = (int)$_POST["category"];
                    $data12['prices'] = json_encode($prodata);
                    CityWise::insert($data12);
                }
            }
            $data['notices'] .= '<div class="alert mini alert-success">Product updated successfully !</div>';
        }
        if(isset($_POST['add_variant'])){
            $pid = $_GET['add_variants'];
            $title = $_POST['variant_title'];
            $price = $_POST['price'];
            $display = $_POST['display'];
            $manufacture = $_POST['manufacture'];
            $length = $_POST['length'];
            $s_available = '';
            DB::insert("INSERT INTO product_variants (product_id, variant_title, price,display,length,manufacture)
            VALUE ('$pid','$title','$price', '$display','$length', '$manufacture')");
        }
        if(isset($_POST['add_group_variant'])){
            $pid = $_GET['add_variants'];
            $i=0;
            DB::table('product_variants')->where('product_id','=',$pid)->delete();
            foreach (  $_POST['variant_title'] as $key=>$vt){
                $title = $_POST['variant_title'][$i];
                $check = DB::table('product_variants')->where('variant_title', '=', $title)->where('product_id', '=', $pid)->first();
                $price = '';
                if(isset($_POST['price'][$vt])){
                    $price = ($_POST['price'][$vt] == '' ? 0 : $_POST['price'][$vt]);
                }
                $display = $_POST['display'.$i];
                $manufacture = $_POST['manufacture'.$i];
                $length = '';
                if(isset($_POST['length'.$i])){
                    $length = implode(',', $_POST['length'.$i]);
                }
                $si = '';
                DB::insert("INSERT INTO product_variants (product_id, variant_title, price,display,length,manufacture)
                    VALUE ('$pid','$title','$price', '$display','$length','$manufacture')");
                $i++;
            }
        }
        if(isset($_POST['edit_variant'])){
            $var_id = $_GET['edit_variants'];
            $ret_id = $_GET['ret_id'];
            $title = $_POST['variant_title'];
            $price = $_POST['price'];
            $display = $_POST['display'];
            $manufacture = $_POST['manufacture'];
            $length = $_POST['length'];
            DB::update("UPDATE product_variants SET variant_title='$title', price='$price', display='$display', length='$length', manufacture='$manufacture' WHERE id=$var_id");
            if($ret_id){
                return redirect()->to('admin/products?add_variants='.$ret_id);
            }
        }
        if(isset($_POST['add_discount'])){
            $pid = $_GET['add_discount'];
            $quantity = $_POST['quantity'];
            $discount = $_POST['discount'];
            $discount_type = $_POST['discount_type'];
            $user_type = $_POST['user_type'];
            DB::insert("INSERT INTO product_discount (product_id, quantity, discount,discount_type,user_type) VALUE ('$pid','$quantity','$discount', '$discount_type','$user_type')");
        }
        if(isset($_POST['edit_discount'])){
            $id = $_POST['dis_id'];
            $pid = $_GET['add_discount'];
            $quantity = $_POST['quantity'];
            $discount = $_POST['discount'];
            $discount_type = $_POST['discount_type'];
            DB::update("UPDATE product_discount SET quantity='$quantity', discount='$discount', discount_type='$discount_type' WHERE id=$id");
        }
        if(isset($_GET['delete']) && isset($_GET['add_variants'])){
            DB::table('product_variants')->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-dismissible alert-success'> Variant has been deleted </div>";
        }
        if(isset($_GET['delete']) && isset($_GET['add_discount'])){
            DB::delete("DELETE FROM product_discount WHERE id = '".$_GET['delete']."' ");
            $data['notices'] .= "<div class='alert alert-dismissible alert-success'> Discount has been deleted </div>";
        }
        if(isset($_GET['delete']) && !isset($_GET['add_variants']) && !isset($_GET['add_discount'])){
            $db = DB::table("ph_relations")->where('products', '=', $_GET['delete'])->delete();
            DB::table("products")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-dismissible alert-success'> Product has been deleted </div>";
        }
        if(isset($_GET['edit'])){
            $product = DB::table("products")->where('id', '=', $_GET['edit'])->first();
            $data['product'] = DB::table("products")->where('id', '=', $_GET['edit'])->first();
            $data['cat'] = DB::table('category')->where('id', '=', $product->category)->first();
            $cat = DB::table('category')->where('id', '=', $product->category)->first();
            $data['wunits'] = DB::table('units')->whereIn('id', explode(',', $cat->weight_unit))->get();
            $data['ph'] = PhRelations::where('products', $_GET['edit'])->first();
            $data['cityWise'] = CityWise::where('product_id', $_GET['edit'])->get();
        }
        if(isset($_GET['add_variants'])){
            $data['variants'] = DB::table('product_variants')->where('product_id', '=', $_GET['add_variants'])->get();
            $data['product'] = DB::table('products')->where('id', '=', $_GET['add_variants'])->first();
        }
        if(isset($_GET['add_discount'])){
            $data['discounts_individual'] =  DB::table('product_discount')->where('user_type', '=', 'individual')->where('product_id', '=', $_GET['add_discount'])->orderBy('quantity','ASC')->get();
            $data['discounts_institutional'] = DB::table('product_discount')->where('user_type','=','institutional')->where('product_id','=',$_GET['add_discount'])->orderBy('quantity','ASC')->get();
            $data['product'] = DB::table("products")->where('id', '=', $_GET['add_discount'])->first();
        }
        $data['header'] = $this->header('Products','products');
        $data['products'] = DB::table('products')->orderBy('id','DESC')->get();
        $data['brands'] = DB::table('brand')->orderBy('id','DESC')->get();
        $data['grades'] = DB::table('product_type')->orderBy('id','DESC')->get();
        $data['categories'] = DB::table('category')->where('parent','=',0)->orderBy('id','DESC')->get();
        $data['units'] = DB::table('units')->orderBy('name','ASC')->get();
        $data['lengths'] = DB::table('length')->orderBy('length','ASC')->get();
        $data['sizes'] = DB::table('size')->orderBy('id','ASC')->get();
        $data['footer'] = $this->footer();
        $data['tp'] = url("/themes/".$this->cfg->theme);
        $data['groups'] = DB::table('sizes_group')->orderBy('id','ASC')->get();
        $data['hubs'] = ManufacturingHubs::orderBy('title', 'ASC')->get();
        $data['buttons'] = $this->buttons;
        $data['cities'] = City::where('is_available','1')->get();
        return view('admin/products')->with('data', $data);
    }

    public function productType(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $cid = $_POST['name'];
            DB::insert("INSERT INTO product_type (name) VALUE ('$cid')");
            $data['notices'] .= "<div class='alert mini alert-success'> Data saved successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $cid = $_POST['name'];
            DB::update("UPDATE product_type SET name = '$cid' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Data updated successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table('product_type')->where('id','=',$_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Category has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Product Type','product-type');
        if(isset($_GET['edit'])) {
            $data['type'] = ProductType::where('id',$_GET['edit'])->first();

        }
        $data['footer'] = $this->footer();
        $data['tp'] = url("/themes/".$this->cfg->theme);
        $data['types'] = ProductType::all();
        return view('admin/product-type')->with('data',$data);
    }
    public function units(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $name = $_POST['name'];
            $symbol = $_POST['symbol'];
            $unit = $_POST['unit'];
            $default = $_POST['default'];
            if($default){
                DB::update("UPDATE units SET def=0");
            }
            DB::insert("INSERT INTO units (name, symbol, unit, def) VALUE ('$name', '$symbol', '$unit', $default)");
            $data['notices'] .= "<div class='alert mini alert-success'> Weight Unit has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $name = $_POST['name'];
            $symbol = $_POST['symbol'];
            $unit = $_POST['unit'];
            $default = $_POST['default'];
            if($default){
                DB::update("UPDATE units SET def=0");
            }
            DB::update("UPDATE units SET name = '$name',symbol = '$symbol',unit = '$unit',def = '$default' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Weight Unit edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("units")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Weight Unit has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Weight Units','units');
        $data['units'] = DB::table('units')->orderBy('name','ASC')->get();
        if(isset($_GET['edit'])) {
            $data['unit'] = DB::table('units')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        $data['tp'] = url("/themes/".$this->cfg->theme);
        return view('admin/units')->with('data',$data);
    }
    public function categories(Request $request){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $cat = new Category();
            $cat->name = $_POST['name'];
            $cat->path = $_POST['path'];
            $cat->parent = $_POST['parent'];
            $cat->popular = $_POST['popular'];
            $cat->popular_priority = $_POST['popular_priority'];
            $cat->popular_brands_priority = $_POST['popular_brands_priority'];
            $cat->popular_in_brands = $_POST['popular_in_brands'];
            $cat->consumption_ratio = $_POST['consumption_ratio'];
           /* $cat->is_view = $_POST['is_view'];*/
            $cat->brands = implode(',', $_POST['brands']);
            $cat->weight_unit = implode(',', $_POST['weight_unit']);
            if(isset($_POST['filter'])) {
                $cat->filter = implode(',', $_POST['filter']);
            }

            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'products/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
            }
            if (request()->file('banner')) {
                $bname = request()->file('banner')->getClientOriginalName();
                $bfile = md5(time()).'.'.request()->file('banner')->getClientOriginalExtension();
                $bpath = base_path().'/assets/'.'products/';
                request()->file('banner')->move($bpath,$bfile);
                $img = Image::make($bpath.$bfile)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($bpath.$bfile);
            }
            if (request()->file('brand_banner')) {
                $bbname = request()->file('banner')->getClientOriginalName();
                $bbfile = md5(time()).'.'.request()->file('brand_banner')->getClientOriginalExtension();
                $bbpath = base_path().'/assets/'.'products/';
                request()->file('brand_banner')->move($bbpath,$bbfile);
                $img = Image::make($bbpath.$bbfile)->resize(1000, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($bbpath.$bbfile);
            }
            $cat->image = (isset($file)) ? $file : '';
            $cat->banner = (isset($bfile)) ? $bfile : '';
            $cat->brand_banner = (isset($bbfile)) ? $bbfile : '';
            $cat->save();
//            DB::insert("INSERT INTO category (image,banner,brand_banner) VALUE ('$file','$bfile','$bbfile')");
            $data['notices'] .= "<div class='alert mini alert-success'> Category has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $cat = Category::where('id', $_GET['edit'])->first();
            $cat->name = $_POST['name'];
            $cat->path = $_POST['path'];
            $cat->parent = $_POST['parent'];
            $cat->popular = $_POST['popular'];
            $cat->popular_priority = $_POST['popular_priority'];
            $cat->popular_brands_priority = $_POST['popular_brands_priority'];
            $cat->popular_in_brands = $_POST['popular_in_brands'];
            $cat->consumption_ratio = $_POST['consumption_ratio'];
            /*$cat->is_view = $_POST['is_view'];*/

            if(isset($_POST['filter'])) {
                $cat->filter = implode(',', $_POST['filter']);
            }
            if(isset($_POST['brands'])){
                $cat->brands = implode(',', $_POST['brands']);
            }
            $cat->weight_unit = implode(',', $_POST['weight_unit']);
            $cat->save();
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'products/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
                DB::update("UPDATE category SET image = '$file' WHERE id = '".$_GET['edit']."'");
            }
            if (request()->file('banner')) {
                $bname = request()->file('banner')->getClientOriginalName();
                $bfile = md5(time()).'.'.request()->file('banner')->getClientOriginalExtension();
                $bpath = base_path().'/assets/'.'products/';
                request()->file('banner')->move($bpath,$bfile);
                $img = Image::make($bpath.$bfile)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($bpath.$bfile);
                DB::update("UPDATE category SET banner = '$bfile' WHERE id = '".$_GET['edit']."'");
            }
            if (request()->file('brand_banner')) {
                $bbname = request()->file('brand_banner')->getClientOriginalName();
                $bbfile = md5(time()).'.'.request()->file('brand_banner')->getClientOriginalExtension();
                $bbpath = base_path().'/assets/'.'products/';
                request()->file('brand_banner')->move($bbpath,$bbfile);
                $img = Image::make($bbpath.$bbfile)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($bbpath.$bbfile);
                DB::update("UPDATE category SET brand_banner = '$bbfile' WHERE id = '".$_GET['edit']."'");
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Page edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("category")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Category has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Categories','categories');
        $data['categories'] = DB::table('category')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['category'] = DB::table('category')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        $data['tp'] = url("/themes/".$this->cfg->theme);
        $data['parents'] = DB::table('category')->where('parent','=',0)->orderBy('id','DESC')->get();

        $data['brands'] = DB::table('brand')->orderBy('name','ASC')->get();
        $data['filters'] = DB::table('filter')->orderBy('name','ASC')->get();
        $data['units'] = DB::table('units')->orderBy('id','ASC')->get();
        return view('admin/categories')->with('data',$data);
    }
    public function Size(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $dataa['name'] = $_POST['name'];
            $group_id= $_POST['group_id'];
            $length = $_POST['length'];
            $lunit = $_POST['lunit'];
            $weight = $_POST['weight'];
            $wunit = $_POST['wunit'];
            if(isset($_POST['w_available'])){
                $dataa['w_available'] = 'available';
                $dataa['weight'] = $_POST['weight'];
            }
            if(isset($_POST['l_available'])){
                $dataa['l_available'] = 'available';
                $dataa['light'] = $_POST['light'];
            }
            if(isset($_POST['s_available'])){
                $dataa['s_available'] = 'available';
                $dataa['super_light'] = $_POST['super_light'];
            }
            $dataa['priority'] = $_POST['priority'];
            $get_sizes = DB::table('sizes_group')->where('id','=',$group_id)->first();
            $post = DB::table('size')->insertGetId($dataa);
            if($post){
                foreach($_POST['price'] as $price){
                    $id = DB::insert("INSERT INTO multiple_size (size_id,price) 
                VALUE ('".$post."','".$price."')");
                }

                $id = DB::insert("INSERT INTO size_weight (size_id,length,lunit,weight,wunit) 
					VALUE ('".$post."','".$length."','".$lunit."','".$weight."','".$wunit."')");
            }
            $v_sizes = $get_sizes->sizes.','.$post;
            DB::update("UPDATE sizes_group SET sizes = '$v_sizes' WHERE id = '$group_id'");
            $data['notices'] .= "<div class='alert mini alert-success'> Size has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $var_id = $_POST['var_id'];
            $group_id= $_POST['group_id'];
            $get_sizes = DB::table('sizes_group')->where('id','=',$group_id)->first();
            $get_size_array = explode(',',$get_sizes->sizes);
            if(!in_array($var_id,$get_size_array)){
                array_push($get_size_array,$var_id);
            }
            $v_sizes = implode(',',$get_size_array);
            $name = $_POST['name'];

            $length = $_POST['length'];
            $lunit = $_POST['lunit'];
            $weight = $_POST['weight'];
            $wunit = $_POST['wunit'];

            $priority = $_POST['priority'];
            DB::update("UPDATE sizes_group SET sizes = '$v_sizes' WHERE id = '$group_id'");
            $post = DB::update("UPDATE size SET name = '$name', priority = '$priority' WHERE id = '".$_GET['edit']."'");
            $ids = array();
            if(isset($_POST['price'])){
                foreach($_POST['price'] as $pkey=>$price){
                    $check = DB::table('multiple_size')->where('id',$pkey)->first();
                    if($price !== '') {
                        $ids[] = $pkey;
                        if ($check !== null) {
                            DB::update('UPDATE multiple_size SET price="'.$price.'" WHERE id='.$pkey);
                        }
                    }
                }
            }
            DB::table('multiple_size')->where('size_id', $_GET['edit'])->whereNotIn('id', $ids)->delete();
            if(isset($_POST['new_price'])){
                foreach($_POST['new_price'] as $newprice){
                    if($newprice !== '') {
                        $id = DB::insert("INSERT INTO multiple_size (size_id,price) VALUE ('".$_GET['edit']."','".$newprice."')");
                    }
                }
            }

            if(isset($_POST)){
                if($_POST['sizeId'] !== ''){
                    $post = DB::update("UPDATE size_weight SET length = '$length', lunit = '$lunit', weight = '$weight', wunit = '$wunit' WHERE id = '".$_POST['sizeId']."'");
                }elseif($length !== ''){
                    $id = DB::insert("INSERT INTO size_weight (size_id,length,lunit,weight,wunit) VALUE ('".$_GET['edit']."','".$length."','".$lunit."','".$weight."','".$wunit."')");
                }
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Data Updated successfully !</div>";
        }
        if(isset($_POST['multiple_edit'])){
            $size_id = $_POST['size_id'];
            $priority = $_POST['priority'];
            $prices = array();
            $new_prices = array();
            if(isset($_POST['price'])) {
                $prices = $_POST['price'];
            }
            if(isset($_POST['new_price'])) {
                $new_prices = $_POST['new_price'];
            }
            foreach ($size_id as $key => $pv) {
                if (isset($priority[$pv])) {
                    foreach ($priority[$pv] as $keyP=>$prio) {
                        DB::update('UPDATE size SET priority="'.$prio.'" WHERE id='.$pv);
                    }
                }
                if (isset($prices[$pv])) {
                    foreach ($prices[$pv] as $key1=>$p) {
                        $check = DB::table('multiple_size')->where('id', $key1)->first();
                        if($check !== null){
                            DB::update('UPDATE multiple_size SET price="'.$p.'" WHERE id='.$key1);
                        }
                    }
                }
                if (isset($new_prices[$pv])) {
                    foreach ($new_prices[$pv] as $key2=>$p2) {
                        if($p2 != '') {
                            $id = DB::insert("INSERT INTO multiple_size (size_id,price) VALUE ('" . $pv . "','" . $p2 . "')");
                        }
                    }
                }
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Data Updated successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            $sg = DB::table('product_variants')->where("variant_title", $_GET['delete'])->count();
            if($sg){
                $data['notices'] .= "<div class='alert alert-danger'>Size is linked to one or more Products.</div>";
            }else {
                DB::table('size')->where('id','=',$_GET['delete'])->delete();
                $delcheck = DB::table('sizes_group')->whereRaw("FIND_IN_SET(?, sizes)", $_GET['delete'])->first();
                if($delcheck !== null){
                    $del = explode(',',$delcheck->sizes);
                    $i = array_search($_GET['delete'],$del);
                    if($i){
                        unset($del[$i]);
                    }
                    $imp = implode(',',$del);
                    DB::update('UPDATE sizes_group SET sizes="' . $imp . '" WHERE id=' .$delcheck->id);
                }
                $data['notices'] .= "<div class='alert alert-success'> Size has been deleted successfully !</div>";
            }
        }
        $data['header'] = $this->header('Sizes Master','sizes');
        $data['sizes'] = DB::table('size')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['size'] = DB::table('size')->where('id','=',$_GET['edit'])->first();
            $data['price'] = DB::table('multiple_size')->where('size_id',$_GET['edit'])->get();
            $data['sizeWeight'] = DB::table('size_weight')->where('size_id',$_GET['edit'])->first();
        }
        $data['groups'] = DB::table('sizes_group')->orderBy('id','DESC')->get();
        $data['units'] = DB::select("SELECT * FROM units ORDER BY name ASC");
        $data['footer'] = $this->footer();
        $data['tp'] = url("/themes/".$this->cfg->theme);

        return view('admin/size')->with('data', $data);
    }
    public function SizesGroup(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $datas['name'] = $_POST['name'];
            $datas['sizes'] = '';
            if(isset($_POST['sizes'])){
                $datas['sizes'] = implode(',', $_POST['sizes']);
            }
            $post = DB::table('sizes_group')->insertGetId($datas);
            $data['notices'] .= "<div class='alert mini alert-success'> Sizes group has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $name = $_POST['name'];
            $sizes = implode(',', $_POST['sizes']);
            DB::update("UPDATE sizes_group SET name = '$name', sizes = '$sizes' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Data Updated successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table('sizes_group')->where('id','=',$_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Sizes group has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Sizes Master','sizes');
        $data['sizes'] = DB::table('size')->orderBy('id','DESC')->get();
        $data['groups'] = DB::table('sizes_group')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['size'] = DB::table('sizes_group')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        $data['tp'] = url("/themes/".$this->cfg->theme);
        return view('admin/sizes_group')->with('data',$data);
    }
    public function filters(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $cid = $_POST['name'];
            DB::insert("INSERT INTO filter (name) VALUE ('$cid')");
            $data['notices'] .= "<div class='alert mini alert-success'> Data saved successfully !</div>";
        }
        if(isset($_POST['edit'])){
            $cid = $_POST['name'];
            DB::update("UPDATE filter SET name = '$cid' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Data updated successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table('filter')->where('id','=',$_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Filter has been deleted successfully !</div>";
        }
        if(isset($_POST['add_option'])){
            $cid = $_POST['name'];
            foreach ($cid as $c) {
                $fid = $_POST['filter_id'];
                DB::insert("INSERT INTO filter_options (filter_id, name) VALUE ('$fid', '$c')");
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Data saved successfully !</div>";
        }
        if(isset($_POST['edit_option'])){
            $cid = $_POST['name'];
            DB::update("UPDATE filter_options SET name = '$cid' WHERE id = '".$_GET['edit_option']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Data updated successfully !</div>";
        }
        if(isset($_GET['delete_option']))
        {
            DB::table('filter_options')->where('id','=',$_GET['delete_option'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Filter has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Filters','filters');
        if(isset($_GET['edit'])) {
            $data['filter'] = DB::table('filter')->where('id','=',$_GET['edit'])->first();
        }
        if(isset($_GET['edit_option'])) {
            $data['filter_opt'] = DB::table('filter_options')->where('id','=',$_GET['edit_option'])->first();
        }
        $data['footer'] = $this->footer();
        if(isset($_GET['view_option'])) {
            $data['filter_opts'] = DB::table('filter_options')->where('filter_id','=',$_GET['view_option'])->get();
        }
        $data['filters'] = DB::table('filter')->orderBy('id','ASC')->get();
        return view('admin/filters')->with('data',$data);
    }
    public function BestForCat(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $datas['link'] = $_POST['link'];
            $datas['category'] = $_POST['category'];
            $datas['content'] = $_POST['content'];
            $post = DB::table('best_for_category')->insertGetId($datas);
            if (request()->file('image')) {
                $bname = request()->file('image')->getClientOriginalName();
                $bfile = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $bpath = base_path().'/assets/'.'products/';
                request()->file('image')->move($bpath,$bfile);
                $img = Image::make($bpath.$bfile)->resize(150, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($bpath.$bfile);
                DB::update("UPDATE best_for_category SET image = '".$bfile."' WHERE id = '".$post."'");
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Best For Category has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $link = $_POST['link'];
            $category = $_POST['category'];
            $content = $_POST['content'];
            if (request()->file('image')) {
                $bname = request()->file('image')->getClientOriginalName();
                $bfile = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $bpath = base_path().'/assets/'.'products/';
                request()->file('image')->move($bpath,$bfile);
                $img = Image::make($bpath.$bfile)->resize(150, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($bpath.$bfile);
                DB::update("UPDATE best_for_category SET image = '".$bfile."' WHERE id = '".$_GET['edit']."'");
            }
            DB::update("UPDATE best_for_category SET link = '$link', category = '$category', content = '$content' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Data Updated successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("best_for_category")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Link has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Best For Category Master','best-for-cat');
        $data['bestforcats'] = DB::table('best_for_category')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['bestforcat'] = DB::table('best_for_category')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        $data['categories'] = DB::table('category')->where('parent',0)->orderBy('id','DESC')->get();
        return view('admin/best-for-cat')->with('data',$data);
    }
    public function brands(){
        $data['notices'] = '';
        $file = '';
        $file1 = '';
        if(isset($_POST['add'])){
            $bname = $_POST['name'];
            $path = $_POST['path'];
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'brand/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
            }
            if (request()->file('banner_image')) {
                $name1 = request()->file('banner_image')->getClientOriginalName();
                $file1 = md5(time()).'.'.request()->file('banner_image')->getClientOriginalExtension();
                $path1 = base_path().'/assets/'.'brand/';
                request()->file('banner_image')->move($path1,$file1);
                $img = Image::make($path1.$file1)->resize(1000, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path1.$file1);
            }
            DB::insert("INSERT INTO brand (name,path,image,banner_image) VALUE ('$bname','$path','$file','$file1')");
            $data['notices'] .= "<div class='alert mini alert-success'> Brand has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $bname = $_POST["name"];
            $path = $_POST["path"];
            DB::update("UPDATE brand SET name = '$bname',path = '$path' WHERE id = '".$_GET['edit']."'");
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/brand/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(100, 100, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path,$file);
                DB::update("UPDATE brand SET image = '$file' WHERE id = '".$_GET['edit']."'");
            }
            if (request()->file('banner_image')) {
                $name1 = request()->file('banner_image')->getClientOriginalName();
                $file1 = md5(time()).'.'.request()->file('banner_image')->getClientOriginalExtension();
                $path1 = base_path().'/assets/brand/';
                request()->file('banner_image')->move($path1,$file1);
                $img = Image::make($path1,$file1)->resize(100, 100, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path1.$file1);
                DB::update("UPDATE brand SET banner_image = '$file1' WHERE id = '".$_GET['edit']."'");
            }
            $data['notices'] .= "<div class='alert mini alert-success'>Brand edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("brand")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'>Brand has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Brands','brands');
        $data['brands'] = DB::table('brand')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['brand'] = DB::table('brand')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/brands')->with('data',$data);
    }
    public function featured(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $cid = $_POST['category'];
            $pids = implode(',', $_POST['products']);
            DB::insert("INSERT INTO featured_products (product_id, category_id) VALUE ('$pids','$cid')");
            $data['notices'] .= "<div class='alert mini alert-success'> Data saved successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $cid = $_POST['category'];
            $pids = implode(',', $_POST['products']);
            DB::update("UPDATE featured_products SET category_id = '$cid',product_id = '$pids' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Data updated successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("category")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Category has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Featured Products','featured-products');
        $data['featured_products'] = DB::table('featured_products')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['featured'] =  DB::table('featured_products')->where('id','=',$_GET['edit'])->first();

        }
        $data['footer'] = $this->footer();
        $data['products'] = DB::table('products')->orderBy('title','ASC')->get();
        $data['categories'] = DB::table('category')->orderBy('name','ASC')->get();
        return view('admin/featured')->with('data',$data);
    }
    public function customers(){
        $notices = '';
        if(isset($_GET['delete']))
        {
            DB::table("customers")->where('id', '=', $_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'> Customer deleted successfully !</div>";
        }

        $header = $this->header('Customers','customers');
        $customers = DB::table('customers')->get();
        $footer = $this->footer();
        return view('admin/customers')->with(compact('header','notices','customers','footer'));
    }
    public function dealers(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $name = $_POST['name'];
            $email = $_POST['email'];
            $password = $_POST['password'];
            $user_type = 'dealer';
            DB::insert("INSERT INTO customers (name,email,password,user_type) VALUE ('$name','$email','$password','$user_type')");
            $data['notices'] .= "<div class='alert mini alert-success'> Dealer has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $name = $_POST["name"];
            $email = $_POST["email"];
            $mobile = $_POST["mobile"];
            $status = $_POST["status"];
            DB::update("UPDATE customers SET name = '$name',email = '$email',mobile = '$mobile',status='$status' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Dealer edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("customers")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Dealer has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Dealers','dealers');
        $data['dealers'] = DB::table('customers')->where('user_type','dealer')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['dealer'] = DB::table('customers')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        $data['tp'] = url("/themes/".$this->cfg->theme);
        return view('admin/dealers')->with('data',$data);
    }
    public function coupons(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $code = $_POST['code'];
            $discount = (int)$_POST['discount'];
            $type = $_POST['type'];
            DB::insert("INSERT INTO coupons (code,discount,type) VALUE ('$code','$discount','$type')");
            $data['notices'] .= "<div class='alert mini alert-success'> Coupon has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $code = $_POST['code'];
            $discount = (int)$_POST['discount'];
            $type = $_POST['type'];
            DB::update("UPDATE coupons SET code = '$code',discount = '$discount',type = '$type' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Coupon edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("coupons")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Coupon has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Coupons','coupons');
        $data['coupons'] = DB::table('coupons')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['coupon'] = DB::table('coupons')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/coupons')->with('data',$data);
    }
    public function reviews(){
        $notices = '';
        if(isset($_GET['approve']))
        {
            DB::update("UPDATE reviews SET active = '1' WHERE id = ".$_GET['approve']);
            $notices = "<div class='alert alert-success'> Review has been approved !</div>";
        }
        $header = $this->header('Admin','reviews');
        $reviews = DB::table('reviews')->orderBy('id','DESC')->get();
        $footer = $this->footer();
        return view('admin/reviews')->with(compact('header','notices','reviews','footer'));
    }
    public function productFaq(){
        $data['notices'] = '';
        if(isset($_GET['edit'])){
            $data['faq'] = DB::table('product_faq')->where('id','=',$_GET['edit'])->first();
        }
        if(isset($_POST['edit'])){
            $status = $_POST['status'];
            $answer = $_POST['answer'];
            $section = $_POST['section'];
            DB::update("UPDATE product_faq SET answer = '".$answer."', status = '".$status."', section = '".$section."' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> FAQ successfully updated !</div>";
        }
        $data['header'] = $this->header('FAQ(s) Master','faq');
        $data['footer'] = $this->footer();
        $data['faqs'] = DB::table('product_faq')->orderBy('id','DESC')->get();
        $data['categories'] = DB::table('category')->orderBy('name','ASC')->get();
        return view('admin/product_faq')->with('data',$data);
    }
}
