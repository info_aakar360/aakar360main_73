<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;
class CustomizationController extends Admin
{
    public function Registerimage(){
        $data['notices'] = '';
        if(isset($_POST['edit'])){
            if (request()->file('image')) {
                $file = request()->file('image');
                $name = $file->getClientOriginalName();
                if (in_array($file->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                    $image = md5(time()).'.'.$file->getClientOriginalExtension();
                    $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'register/';
                    $file->move($path,$image);
                    $img = Image::make($path.$image)->resize(1200, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($path.$image);
                } else {
                    $data['notices'] .= "<div class='alert mini alert-warning'> $name is not a valid format</div>";
                }
                DB::update("UPDATE register_image SET image = '".$image."' WHERE id = '".$_GET['edit']."'");
            }
            $data['notices'] .= '<div class="alert mini alert-success">Image has been updated successfully !</div>';
        }
        if(isset($_GET['delete']))
        {
            DB::update("UPDATE register_image SET image = '' WHERE id = '".$_GET['delete']."'");
            $data['notices'] .= "<div class='alert alert-success'>Image has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Slider','slider');
        $data['register'] = DB::table('register_image')->get();
        if(isset($_GET['edit'])) {
            $data['register'] = DB::table('register_image')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/registerimage')->with('data',$data);
    }
    public function slider(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $datas['title'] = $_POST['title'];
            $datas['link'] = $_POST['link'];
            $datas['image'] = '';
            $slide = DB::table('slider')->insertGetId($datas);
            if (request()->file('image')) {
                $file = request()->file('image');
                $name = $file->getClientOriginalName();
                if (in_array($file->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                    $image = md5(time()).'.'.$file->getClientOriginalExtension();
                    $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'slider/';
                    $file->move($path,$image);
                    $img = Image::make($path.$image)->resize(1200, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($path.$image);
                } else {
                    $data['notices'] .= "<div class='alert mini alert-warning'> $name is not a valid format</div>";
                }
                DB::update("UPDATE slider SET image = '".$image."' WHERE id = '".$slide."'");
            }
            $data['notices'] .= "<div class='alert alert-success'> The slide has been published successfully !</div>";
        }
        if(isset($_POST['edit'])){
            DB::update("UPDATE slider SET title = '".escape($_POST['title'])."',link = '".escape($_POST['link'])."' WHERE id = ".$_GET['edit']);
            if (request()->file('image')) {
                $file = request()->file('image');
                $name = $file->getClientOriginalName();
                if (in_array($file->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                    $image = md5(time()).'.'.$file->getClientOriginalExtension();
                    $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'slider/';
                    $file->move($path,$image);
                    $img = Image::make($path.$image)->resize(1200, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($path.$image);
                } else {
                    $data['notices'] .= "<div class='alert mini alert-warning'> $name is not a valid format</div>";
                }
                DB::update("UPDATE slider SET image = '".$image."' WHERE id = '".$_GET['edit']."'");
            }
            $data['notices'] .= '<div class="alert mini alert-success">The slide has been updated successfully !</div>';
        }
        if(isset($_GET['delete']))
        {
            DB::table("slider")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> The slide has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Slider','slider');
        $data['slides'] = DB::table('slider')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['slide'] = DB::table('slider')->orderBy('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/slider')->with('data',$data);
    }
    public function editor(){
        $notices = '';
        $cfg = $this->cfg;
        if (isset($_GET['file'])){
            $file = resource_path('views/'.$_GET['file']);
            if (!file_exists($file)){
                $file = resource_path('views/index.php');
                echo '<div class="alert  alert-warning"> File not found , edititng home.php </div>';
            }
        } else {
            $file = resource_path('views/index.php');
        }
        if (isset($_POST['text']))
        {
            file_put_contents($file, escape($_POST['text']));
            $notices = '<div class="alert  alert-success"> File has been saved </div>';
        }
        $text = file_get_contents($file);
        $header = $this->header('Editor','editor');
        $files = glob(resource_path('views/*.php'), GLOB_BRACE);
        $footer = $this->footer();
        return view('admin/editor')->with(compact('header','notices','cfg','files','text','footer'));
    }
    public function templates(){
        $data['notices'] = '';
        if(isset($_POST['edit'])){
            DB::update("UPDATE templates SET title = '".escape($_POST['title'])."',template = '".escape($_POST['template'])."' WHERE id = ".$_GET['edit']);
            $data['notices'] .= "<div class='alert mini alert-success'> Template edited successfully !</div>";
        }
        if(isset($_GET['edit']))
        {
            $data['template'] = DB::table("templates")->where('id', '=', $_GET['edit'])->first();
        }
        $data['header'] = $this->header('Templates','templates');
        $data['templates'] = DB::table('templates')->get();
        $data['footer'] = $this->footer();
        return view('admin/templates')->with('data',$data);
    }
    public function builder(){
        $data['notices'] = '';
        if (isset($_GET['page'])) {
            $area = 'page';
        } elseif (isset($_GET['post'])) {
            $area = 'post';
        } else {
            $area = 'home';
        }
        if(isset($_GET['save'])){
            $datas = $_POST['data'];
            parse_str($datas,$str);
            $builder = $str['item'];
            foreach($builder as $key => $value){
                $key=$key+1;
                DB::update("UPDATE blocs SET o=$key where id=$value");
            }
            return "Succesfully updated";
        }
        if(isset($_POST['add'])){
            $area = $_POST['area'];
            $content = escape($_POST['content']);
            $title = escape($_POST['title']);
            DB::insert("INSERT INTO blocs (area,content,title) VALUE ('$area','$content','$title')");
            $data['notices'] .= "<div class='alert mini alert-success'> bloc has been added !</div>";
        }
        if(isset($_POST['edit'])){
            $title = escape($_POST["title"]);
            $content = escape($_POST["content"]);
            DB::update("UPDATE blocs SET title = '$title',content = '".$content."' WHERE id = ".$_GET['edit']);
            $data['notices'] .= "<div class='alert mini alert-success'> bloc has been updated successfully !</div>";
        }
        if(isset($_GET['edit'])) {
            $data['bloc'] = DB::select("SELECT * FROM blocs WHERE id = ".$_GET['edit'])[0];
        }
        if(isset($_GET['delete']))
        {
            DB::table("blocs")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> The bloc has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Page builder','builder');
        $data['blocs'] = DB::table('blocs')->where('area','=',$area)->orderBy('o','ASC')->get();
        $data['tp'] = url("/themes/".$this->cfg->theme);
        $data['footer'] = $this->footer();
        return view('admin/builder')->with('data',$data);
    }
    public function menu(){
        $data['notices'] = '';
        if(isset($_GET['save'])){
            $data = $_POST['data'];
            parse_str($data,$str);
            $builder = $str['item'];
            foreach($builder as $key => $value){
                $key=$key+1;
                DB::update("UPDATE menu SET o=$key where id=$value");
            }
            return "Succesfully updated";
        }
        if(isset($_POST['add'])){
            $link = $_POST['link'];
            $title = escape($_POST['title']);
            $parent = $_POST['parent'];
            DB::insert("INSERT INTO menu (link,title,parent) VALUE ('$link','$title','$parent')");
            $data['notices'] .= "<div class='alert mini alert-dismissible alert-success'> menu item has been added !</div>";
        }
        if(isset($_POST['edit'])){
            $title = escape($_POST["title"]);
            $link = $_POST["link"];
            $parent = $_POST["parent"];
            DB::update("UPDATE menu SET title = '$title',link = '$link',parent = '$parent' WHERE id = ".$_GET['edit']);
            $data['notices'] .= "<div class='alert mini alert-dismissible alert-success'> menu item has been updated successfully !</div>";
        }
        if(isset($_GET['edit'])) {
            $data['item'] = DB::table('menu')->where('id','=',$_GET['edit'])->get();
        }
        if(isset($_GET['delete']))
        {
            DB::table("menu")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> The menu item has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Menu','menu');
        $data['items'] = DB::table('menu')->orderBy('o','ASC')->get();
        $data['tp'] = url("/themes/".$this->cfg->theme);
        $data['footer'] = $this->footer();
        $data['parents'] = DB::table('menu')->where('parent','=',0)->orderBy('id','DESC');

        return view('admin/menu')->with('data',$data);
    }
    public function bottom(){
        $data['notices'] = '';
        if(isset($_GET['save'])){
            $datas = $_POST['data'];
            parse_str($datas,$str);
            $builder = $str['item'];
            foreach($builder as $key => $value){
                $key=$key+1;
                DB::update("UPDATE footer SET o=$key where id=$value");
            }
            return "Succesfully updated";
        }
        if(isset($_POST['add'])){
            if (escape($_POST['title']) != "" && $_POST['link'] != ""){
                $link = $_POST['link'];
                $title = escape($_POST['title']);
                DB::insert("INSERT INTO footer (link,title) VALUE ('$link','$title')");
                $data['notices'] .= "<div class='alert mini alert-dismissible alert-success'> footer item has been added !</div>";
            }
        }
        if(isset($_POST['edit'])){
            $title = escape($_POST["title"]);
            $link = $_POST["link"];
            DB::update("UPDATE footer SET title = '$title',link = '$link' WHERE id = ".$_GET['edit']);
            $data['notices'] .= "<div class='alert mini alert-dismissible alert-success'> footer item has been updated successfully !</div>";
        }
        if(isset($_GET['edit'])) {
            $data['item'] = DB::table('footer')->where('id','=',$_GET['edit'])->first();
        }
        if(isset($_GET['delete']))
        {
            DB::table("footer")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> The footer item has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Footer menu','bottom');
        $data['items'] =  DB::table('footer')->orderBy('o','ASC')->get();
        $data['tp'] = url("/themes/".$this->cfg->theme);
        $data['footer'] = $this->footer();
        return view('admin/bottom')->with('data',$data);
    }
}
