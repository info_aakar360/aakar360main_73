<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\SpecialRequirements;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;
class RetailController extends Admin
{
    public function productSr(){
        $notices = '';
        $data['title'] = 'Product Special Requirements';
        $header = $this->header($data['title'],'product_sr');
        $srs = SpecialRequirements::orderBy('id', 'DESC')->get();
        $footer = $this->footer();
        return view('admin/products-sr')->with(compact('header','notices','srs','footer', 'data'));
    }
    public function orders(){
        $data['notices'] = '';
        if(isset($_GET['delete']))
        {
            DB::table("orders")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Order has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Orders','orders');
        $data['fields'] = DB::table('fields')->select('name','code')->orderBy('id','ASC')->get();
        $data['orders'] = DB::table('orders')->orderBy('id','DESC')->get();
        if(isset($_GET['details'])) {
            if(isset($_POST['save'])){
                DB::update("Update orders SET stat = ".$_POST['stat']." WHERE id = '".$_GET['details']."' ");
                $data['notices'] .= "<div class='alert alert-success'> Order status has been changed ! </div>";
            }
            $data['order'] = DB::table('orders')->where('id','=',$_GET['details'])->limit(1)->first();
        }
        $data['foot'] = $this->footer();
        return view('admin/orders')->with('data',$data);
    }
    public function stats(){
        if (isset($_GET['year'])){
            $data['term'] = 'year';
            for ($iDay = 365; $iDay >= 0; $iDay--) {
                $d[366 - $iDay] = "'".date('Y-m-d', strtotime("-" . $iDay . " day"))."'";
                $i[date('Y-m-d', strtotime("-" . $iDay . " day"))] = 0;
                $o[date('Y-m-d', strtotime("-" . $iDay . " day"))] = 0;
                $s[date('Y-m-d', strtotime("-" . $iDay . " day"))] = 0;
                $c[date('Y-m-d', strtotime("-" . $iDay . " day"))] = 0;
            }
        } elseif (isset($_GET['month'])){
            $data['term'] = 'month';
            for ($iDay = 30; $iDay >= 0; $iDay--) {
                $d[31 - $iDay] = "'".date('Y-m-d', strtotime("-" . $iDay . " day"))."'";
                $i[date('Y-m-d', strtotime("-" . $iDay . " day"))] = 0;
                $o[date('Y-m-d', strtotime("-" . $iDay . " day"))] = 0;
                $s[date('Y-m-d', strtotime("-" . $iDay . " day"))] = 0;
                $c[date('Y-m-d', strtotime("-" . $iDay . " day"))] = 0;
            }
        } else {
            $data['term'] = 'week';
            for ($iDay = 6; $iDay >= 0; $iDay--) {
                $d[7 - $iDay] = "'".date('Y-m-d', strtotime("-" . $iDay . " day"))."'";
                $i[date('Y-m-d', strtotime("-" . $iDay . " day"))] = 0;
                $o[date('Y-m-d', strtotime("-" . $iDay . " day"))] = 0;
                $s[date('Y-m-d', strtotime("-" . $iDay . " day"))] = 0;
                $c[date('Y-m-d', strtotime("-" . $iDay . " day"))] = 0;
            }
        }
        $fs = DB::table('visitors')->where('date','=',$d[1])->orderBy('date','ASC')->get();
        $visit = array();
        foreach ($fs as $visits){
            $i[$visits->date] = $visits->visits;
        }
        $yesterday = date('Y-m-d', strtotime(date('Y-m-d') .' -1 day'));
        $yvisits = (!in_array($yesterday,$i)) ? $i[$yesterday] : 0;
        $tvisits = (!in_array(date('Y-m-d'),$i)) ? $i[date('Y-m-d')] : 0;
        $order_query = DB::table('orders')->where('date','=',$d[1])->orderBy('date','ASC')->get();
        foreach ($order_query as $order){
            $o[$order->date] = $o[$order->date]+1;
            $s[$order->date] = $s[$order->date]+$order->summ;
            $c[$order->date] = $o[$order->date] / $i[$order->date]*100;
        }
        if ($yorders = DB::select("SELECT COUNT(*) as count FROM orders WHERE date ='".$yesterday."'")[0]->count > 0){
            $ysales = DB::select("SELECT SUM(summ) as sum FROM orders WHERE date ='".$yesterday."'")[0]->sum;
        } else {
            $ysales = 0;
        }
        $torders = DB::select("SELECT COUNT(*) as count FROM orders WHERE date ='".date('Y-m-d')."'")[0]->count;
        if ($torders > 0){
            $tsales = DB::select("SELECT SUM(summ) as sum FROM orders WHERE date ='".date('Y-m-d')."'")[0]->sum;
        } else {
            $tsales = 0;
        }
        $yconversion = $c[$yesterday];
        $tconversion = $c[date('Y-m-d')];
        $data['morders'] = max($o)+max($o)*2/6+1;
        $data['msales'] = max($s)+max($s)*2/6+1;
        $data['mconversion'] = max($c)+max($c)*2/6+1;
        $data['mvisits'] = max($i)+max($i)*2/6+1;
        $data['porders'] = p($yorders,$torders);
        $data['pvisits'] = p($yvisits,$tvisits);
        $data['psales'] = p($ysales,$tsales);
        $data['pconversion'] = p($yconversion,$tconversion);
        $data['orders'] = DB::select("SELECT COUNT(*) as count FROM orders")[0]->count;
        $data['ssales'] = DB::select("SELECT SUM(summ) as sum FROM orders ")[0]->sum;
        $data['chart'] = array();
        $data['chart']['days'] = implode(', ',$d);
        $data['i'] = implode(', ',$i);
        $data['o'] = implode(', ',$o);
        $data['s'] = implode(', ',$s);
        $data['c'] = implode(', ',$c);
        $data['header'] = $this->header('Statistics','stats');
        $data['cfg'] = $this->cfg;
        $data['footer'] = $this->footer();
        return view('admin/stats')->with('data',$data);
    }
}
