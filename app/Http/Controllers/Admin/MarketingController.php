<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;
class MarketingController extends Admin
{
    public function tracking(){
        $notices = '';
        if(isset($_POST['add'])){
            $name = $_POST['name'];
            $code = $_POST['code'];
            DB::insert("INSERT INTO tracking (name,code) VALUE ('$name','$code')");
            $notices .= "<div class='alert mini alert-success'> Tracking code has been successfully added !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("tracking")->where('id', '=', $_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'> The tracking code has been deleted successfully !</div>";
        }
        $header = $this->header('Tracking','tracking');
        $codes = DB::table("tracking")->orderBy('id','DESC')->get();
        $footer = $this->footer();
        return view('admin/tracking')->with(compact('header','notices','codes','footer'));
    }
    public function newsletter(){
        $notices = '';
        if(isset($_POST['send'])){
            $emails['orders'] = array();
            $emails['support'] = array();
            $emails['newsletter'] = array();
            $orders = DB::table('orders')->select('email')->get();
            foreach ($orders as $order){
                $emails['orders'][$order->email] = $order->email;
            }
            $tickets = DB::table('tickets')->select('email')->get();
            foreach ($tickets as $ticket){
                $emails['support'][$ticket->email] = $ticket->email;
            }
            $subscribers = DB::table('subscribers')->select('email')->get();
            foreach ($subscribers as $subscriber){
                $emails['newsletter'][$subscriber->email] = $subscriber->email;
            }
            if ($_POST['group'] == 'orders') {
                $tos = $emails['orders'];
            } elseif ($_POST['group'] == 'newsletter') {
                $tos = $emails['newsletter'];
            } elseif ($_POST['group'] == 'support') {
                $tos = $emails['support'];
            } else {
                $tos = array_merge($emails['support'],$emails['newsletter'],$emails['orders']);
            }
            foreach ($tos as $to){
                mailing('newsletter',array('title'=>escape($_POST['title']),'email'=>$to,'content'=>nl2br(escape($_POST['content']))),escape($_POST['title']),$to);
            }
            $notices = "<div class='alert mini alert-success'> Newsletter has been successfully sent !</div>";
        }
        $header = $this->header('Newsletter','newsletter');
        $footer = $this->footer();
        return view('admin/newsletter')->with(compact('header','notices','footer'));
    }
}
