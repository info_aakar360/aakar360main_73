<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;
class ServiceController extends Admin
{
    public function services_categories(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $name1 = $_POST['name'];
            $description = $_POST['description'];
            $path1 = $_POST['path'];
            $parent = $_POST['parent'];
            $display = $_POST['display'];
            $list = $_POST['list'];
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'services/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
            }
            DB::insert("INSERT INTO services_category (name,description,display, list, path,parent,image) VALUE ('$name1','$description','$display', '$list','$path1','$parent','$file')");
            $data['notices'] .= "<div class='alert mini alert-success'>Service Category has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $name1 = $_POST["name"];
            $description = $_POST['description'];
            $path1 = $_POST["path"];
            $parent = $_POST["parent"];
            $display = $_POST["display"];
            $list = $_POST["list"];
            DB::update("UPDATE services_category SET name = '$name1',description='$description', display = '$display', list = '$list', path = '$path1',parent = '$parent' WHERE id = '" . $_GET['edit'] . "'");
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'services/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
                DB::update("UPDATE services_category SET image = '$file' WHERE id = '".$_GET['edit']."'");
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Page edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table('services_category')->where('id','=',$_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'>Service Category has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Service Categories','services_categories');
        $data['services_categories'] = DB::table('services_category')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['services_categories'] = DB::table('services_category')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        $data['parents'] = DB::table('services_category')->where('parent',0)->orderBy('id','DESC')->get();
        return view('admin/services_categories')->with('data',$data);
    }
    public function services(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $title = $_POST['title'];
            $description = $_POST['description'];
            $price = $_POST['price'];
            $category = $_POST['category'];
            $display = $_POST['display'];
            $payment_mode = $_POST['payment_mode'];
            if (request()->file('images')) {
                $order = 0;
                $images = array();
                foreach (request()->file('images') as $file) {
                    $name = $file->getClientOriginalName();
                    if (in_array($file->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                        $images[] = $image = md5(time()).'-'.$order.'.'.$file->getClientOriginalExtension();
                        $path = base_path().'/assets/'.'services/';
                        $file->move($path,$image);
                        if($image) {
                            $img = Image::make($path . $image)->resize(800, null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($path.$image);
                            $order++;
                        }
                    } else {
                        $data['notices'] .= "<div class='alert mini alert-warning'> ".$name." is not a valid format</div>";
                    }
                }
            }
            DB::insert("INSERT INTO services (title,description,price,category,images,display,payment_mode) VALUE ('$title','$description','$price','$category','".implode(',',$images)."','$display','$payment_mode')");
            $data['notices'] .= "<div class='alert mini alert-success'>Service has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $title = $_POST['title'];
            $description = $_POST['description'];
            $price = $_POST['price'];
            $category = $_POST['category'];
            $display = $_POST['display'];
            $payment_mode = $_POST['payment_mode'];
            DB::update("UPDATE services SET title = '$title',description = '$description',price = '$price',category = '$category',display = '$display',payment_mode = '$payment_mode' WHERE id = '" . $_GET['edit'] . "'");
            if (request()->file('images')) {
                $order = 0;
                $images = array();
                foreach (request()->file('images') as $file) {
                    $name = $file->getClientOriginalName();
                    if (in_array($file->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                        $images[] = $image = md5(time()).'.'.$order.'.'.$file->getClientOriginalExtension();
                        $path = base_path().'/assets/'.'services/';
                        $file->move($path,$image);
                        if($image) {
                            $img = Image::make($path . $image)->resize(800, null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($path . $image);
                            $order++;
                        }
                    } else {
                        $data['notices'] .= "<div class='alert mini alert-warning'> $name is not a valid format</div>";
                    }
                }
                DB::update("UPDATE services SET images = '".implode(',',$images)."' WHERE id = '".$_GET['edit']."'");
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Page edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table('services')->where('id','=',$_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'>Service has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Services','services');
        $data['services'] = DB::table('services')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['services'] = DB::table('services')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        $data['parents'] = DB::table('services_category')->where('parent','<>',0)->orderBy('id','DESC')->get();
        return view('admin/services')->with('data',$data);
    }
    public function ServicesAnswer(){
        $data['notices'] = '';
        if(isset($_GET['edit'])){
            $data['answer'] = DB::table('services_answer')->where('id','=',$_GET['edit'])->first();
        }
        if(isset($_POST['edit'])){
            $status = $_POST['status'];
            DB::update("UPDATE services_answer SET status = '".$status."' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Status successfully updated !</div>";
        }
        $data['header'] = $this->header('Answer Master','answers');
        $data['footer'] = $this->footer();
        $data['answers'] = DB::table('services_answer')->orderBy('id','DESC')->get();
        return view('admin/services_answer')->with('data',$data);
    }
    public function s_how_it_works(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $cat = implode(',', $_POST['cat']);
            $tab_title = escape($_POST['tab_title']);
            $tab_icon = escape($_POST['tab_icon']);
            $content_title = escape($_POST['content_title']);
            $content = escape($_POST['content']);
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'how_it_works/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
            }
            DB::insert("INSERT INTO s_how_it_works (cat,tab_title,tab_icon,content_title,content,image) 
                VALUE ('$cat','$tab_title','$tab_icon','$content_title','$content','$file')");
            $data['notices'] .= "<div class='alert mini alert-success'> How it works has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $cat = implode(',',$_POST['cat']);
            $tab_title = escape($_POST['tab_title']);
            $tab_icon = escape($_POST['tab_icon']);
            $content_title = escape($_POST['content_title']);
            $content = escape($_POST['content']);
            DB::update("UPDATE s_how_it_works SET cat = '$cat',tab_title = '$tab_title',tab_icon = '$tab_icon',content_title='$content_title',content='$content' WHERE id = '".$_GET['edit']."'");
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'how_it_works/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
                DB::update("UPDATE s_how_it_works SET image = '$file' WHERE id = '".$_GET['edit']."'");
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Page edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table('s_how_it_works')->where('id','=',$_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> How it works has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('How it works','how it works');
        $data['s_how'] = DB::table('s_how_it_works')->get();
        $data['service_cats'] = DB::table('services_category')->get();
        if(isset($_GET['edit'])) {
            $data['how'] = DB::table('s_how_it_works')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/s-how-it-works')->with('data',$data);
    }
    public function Faq(){
        $data['notices'] = '';
        if(isset($_GET['edit'])){
            $data['faq'] = DB::table('faq')->where('id','=',$_GET['edit'])->first();
        }
        if(isset($_POST['edit'])){
            $status = $_POST['status'];
            $answer = $_POST['answer'];
            DB::update("UPDATE faq SET answer = '".$answer."', status = '".$status."' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> FAQ successfully updated !</div>";
        }
        $data['header'] = $this->header('FAQ(s) Master','faq');
        $data['footer'] = $this->footer();
        $data['faqs'] = DB::table('faq')->orderBy('id','DESC')->get();
        $data['categories'] = DB::table('category')->orderBy('name','ASC')->get();
        return view('admin/faq')->with('data',$data);
    }
}
