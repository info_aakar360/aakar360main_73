<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;
class NotificationController extends Admin
{
    public function Notification(){
        define('API_ACCESS_KEY','AAAAd2L0SKc:APA91bHWNCFq52FHj1XBUMjD_dUn6iaCVdV0EZMKTjoSPygXCOLCEIUfLnoPHePCMPriu7l63ASRpgvPJSbXN_cErCsGacQMxUvLWsVXOv9YcZJj3hL6avefq5BdwfBug3h7aHaupKtA');
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $token='';
        $data['notices'] = '';
        $data['customer'] = array();
        $data['customer'] = DB::table('customers')->where('user_type','=','institutional')->get();
        if(isset($_POST['note_send'])){
            $cus = implode(',',$_POST['cust']);
            $customers = DB::table('customers')->whereIn('id',explode(',', $cus))->get();
            foreach($customers as $cust){
                $title = $_POST['title'];
                $cust_id = $cust->id;
                $note_read = 0;
                $token =$cust->gsm_id;
                $notification = $_POST['msg'];
                $note = [
                    'title' =>$title,
                    'body' => $notification
                ];
                $extraNotificationData = ["message" => $note,"moredata" =>'dd'];
                $fcmNotification = [
                    'to'        => $token, //single token
                    'notification' => $note
                ];
                $headers = [
                    'Authorization: key=' . API_ACCESS_KEY,
                    'Content-Type: application/json'
                ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$fcmUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                $result = curl_exec($ch);
                curl_close($ch);
                $result;
                $items = DB::table('notification_read')
                    ->select('customer_id','note_read')
                    ->where('customer_id','=',$cust_id)
                    ->first();
                if($items){
                    $total = $items->note_read+1;
                    DB::update("UPDATE notification_read SET note_read = '$total' WHERE customer_id = '".$cust_id."'");
                }
                else{
                    $id = DB::insert("INSERT INTO notification_read (customer_id,note_read) 
                VALUE ('".$cust_id."',1)");
                }
            }
            $id = DB::insert("INSERT INTO notification (cust_id,title,notification) 
                VALUE ('".$cus."','".$title."','".$notification."')");
            $data['notices'] .= "<div class='alert mini alert-success'> notification has been send successfully !</div>";
        }
        $data['customer_class'] = DB::table('customer_class')->get();
        $data['customer_type'] = DB::table('customer_type')->get();
        $data['customer_category'] = DB::table('customer_category')->get();
        $data['cfg'] = $this->cfg;
        $data['tp'] = url("/assets/crm/");
        $data['header'] = $this->header();
        $data['footer'] = $this->footer();
        $data['title'] = 'Admin';

        return view('admin/notification')->with('data',$data);
    }

    public function retailNotification(){
        define('API_ACCESS_KEY','AAAAeGblRAE:APA91bGsB_syWczmGNpO2Uq8hZmtW_zdxIzNeGo0iWNTnRehVaoN0DCO_vy0KxH9oJiK-1UN6-Y51yGs9Qtz9F94SXrt7AI9FiR3wqENEPS22qHXNioX_io5_EtJKRiQY_Z-AdZP87uL');
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $token='';
        $data['notices'] = '';
        $data['customer'] = array();
        $data['customer'] = DB::table('customers')->where('user_type','<>','institutional')->get();
        if(isset($_POST['note_send'])){
            $cus = implode(',',$_POST['cust']);
            $customers = DB::table('customers')->whereIn('id',explode(',', $cus))->get();
            foreach($customers as $cust){
                $title = $_POST['title'];
                $cust_id = $cust->id;
                $note_read = 0;
                $token =$cust->gsm_id;
                $notification = $_POST['msg'];
                $note = [
                    'title' =>$title,
                    'body' => $notification
                ];
                $extraNotificationData = ["message" => $note,"moredata" =>'dd'];
                $fcmNotification = [
                    'to'        => $token, //single token
                    'notification' => $note
                ];
                $headers = [
                    'Authorization: key=' . API_ACCESS_KEY,
                    'Content-Type: application/json'
                ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$fcmUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                $result = curl_exec($ch);
                curl_close($ch);
                echo $result;
                $items = DB::table('notification_read')
                    ->select('customer_id','note_read')
                    ->where('customer_id','=',$cust_id)
                    ->first();
                if($items){
                    $total = $items->note_read+1;
                    DB::update("UPDATE notification_read SET note_read = '$total' WHERE customer_id = '".$cust_id."'");
                }
                else{
                    $id = DB::insert("INSERT INTO notification_read (customer_id,note_read) 
                VALUE ('".$cust_id."',1)");
                }
            }
            $id = DB::insert("INSERT INTO notification (cust_id,title,notification) 
                VALUE ('".$cus."','".$title."','".$notification."')");
            $data['notices'] .= "<div class='alert mini alert-success'> notification has been send successfully !</div>";
        }
        $data['customer_class'] = DB::table('customer_class')->get();
        $data['customer_type'] = DB::table('customer_type')->get();
        $data['customer_category'] = DB::table('customer_category')->get();
        $data['cfg'] = $this->cfg;
        $data['tp'] = url("/assets/crm/");
        $data['header'] = $this->header();
        $data['footer'] = $this->footer();
        $data['title'] = 'Admin';

        return view('admin/retail_notification')->with('data',$data);
    }
}
