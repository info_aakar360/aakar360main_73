<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;

class AdministrationController extends Admin
{
    public function support(){
        $data['notices'] = '';
        if(isset($_POST['send'])){
            $ticket = DB::table('tickets')->where('id','=',$_GET['reply'])->first();
            mailing('reply',array('title'=>escape($_POST['title']),'email'=>$ticket->email,'reply'=>nl2br($_POST['reply'])),escape($_POST['title']),$ticket->email);
            $data['notices'] = "<div class='alert mini alert-success'> E-mail has been successfully sent !</div>";
        }
        if(isset($_GET['reply'])){
            $data['ticket'] = DB::table('tickets')->where('id','=',$_GET['reply'])->first();
        }
        $data['header'] = $this->header('Support','support');
        $data['tickets'] = DB::table('tickets')->get();
        $data['footer'] = $this->footer();
        return view('admin/support')->with('data',$data);
    }
    public function administrators(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $name = $_POST['name'];
            $email = $_POST['email'];
            $pass = md5($_POST['pass']);
            $secure = md5(time());
            DB::insert("INSERT INTO user (u_name,u_email,u_pass,secure) VALUE ('$name','$email','$pass','$secure')");
            $data['notices'] .= "<div class='alert mini alert-dismissible alert-success'> Admin has been added !</div>";
        }
        if(isset($_POST['edit'])){
            $name = $_POST["name"];
            $email = $_POST['email'];
            $pass = md5($_POST['pass']);
            $admin = DB::select("SELECT * FROM user WHERE u_id = ".$_GET['edit']);
            DB::update("UPDATE user SET u_name = '$name',u_email = '$email',u_pass = '$pass' WHERE u_id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-dismissible alert-success'> Admin informations has been updated successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("user")->where('u_id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-dismissible alert-success'> The admin has been deleted successfully !</div>";
        }
        if(isset($_GET['edit'])){
            $data['admin'] = DB::table('user')->where('u_id','=',$_GET['edit'])->first();
        }
        $data['header'] = $this->header('Administrators','administrators');
        $data['admins'] = DB::table('user')->get();
        $data['footer'] = $this->footer();
        return view('admin/administrators')->with('data',$data);
    }
    public function profile(){
        $notices = '';
        if(isset($_POST['update'])){
            $user = DB::table('user')->where('secure','=',session('admin'))->first();
            $user_name = $_POST['name'];
            $user_email = $_POST['email'];
            if ($_POST['pass'] != ""){
                $user_pass = md5($_POST['pass']);
            } else {
                $user_pass = $user->u_pass;
            }
            DB::update("UPDATE user SET u_name = '$user_name',u_email = '$user_email',u_pass = '$user_pass' WHERE secure = '".session('admin')."'");
            $notices = "<div class='alert alert-success mini'> Profile updated successfully </div>";
        }
        $header = $this->header('Profile','profile');
        $user = DB::table('user')->where('secure','=',session('admin'))->first();
        $footer = $this->footer();
        return view('admin/profile')->with(compact('header','notices','user','footer'));
    }
}
