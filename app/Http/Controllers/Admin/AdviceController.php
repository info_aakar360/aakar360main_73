<?php

namespace App\Http\Controllers\Admin;

use App\AdviceCategory;
use App\Blog;
use App\Category;
use App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;
class AdviceController extends Admin
{
    public function blog(Request $request){
        $data['notices'] = '';
        $image = '';
        $bimage = '';
        $pro_cat = '';
        if(isset($_POST['add'])){

            $blog = new Blog();
            $blog->title = $_POST['title'];
            $blog->slug = makeSlugOf($_POST['title'], 'design_category');
            $blog->content = $_POST['content'];
            $blog->short_des = str_limit(escape($_POST['content']),150);
            $blog->category = $_POST['category'];
            $blog->section = $_POST['section'];
            $blog->date = date('Y-m-d');
            if(isset($_POST['pro_cat'])){
                $blog->pro_cat = implode(',', $_POST['pro_cat']);
            }
            $blog->time = time();
            $blog->images = '';
            $blog->save();
            $post = $blog->id;
            if (request()->file('image')) {
                $file = request()->file('image');
                $name = $file->getClientOriginalName();
                if (in_array($file->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                    $image = md5(time()).'.'.$file->getClientOriginalExtension();
                    $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'blog/';
                    $file->move($path,$image);
                    $img = Image::make($path.$image)->resize(800, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($path.$image);
                } else {
                    $data['notices'] .= "<div class='alert mini alert-warning'> $name is not a valid format</div>";
                }
                DB::update("UPDATE blog SET images = '".$image."' WHERE id = '".$post."'");
                $blogimage = Blog::find($post);
                $blogimage->images = $image;
                $blogimage->save();
            }
            if (request()->file('banner_image')) {
                $bfile = request()->file('banner_image');
                $bname = $bfile->getClientOriginalName();
                if (in_array($bfile->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                    $bimage = md5(time()).'.'.$bfile->getClientOriginalExtension();
                    $bpath = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'blog/';
                    $bfile->move($bpath,$bimage);
                    $img = Image::make($bpath.$bimage)->resize(800, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($bpath.$bimage);
                } else {
                    $data['notices'] .= "<div class='alert mini alert-warning'> $bname is not a valid format</div>";
                }
                $bannerimage = Blog::find($post);
                $bannerimage->banner_image = $bimage;
                $bannerimage->save();

            }
            $data['notices'] .= "<div class='alert alert-success'> Post has been published successfully !</div>";
        }
        if(isset($_POST['edit'])){
            if(isset($_POST['pro_cat'])){
                $pro_cat = implode(',', $_POST['pro_cat']);
            }
            $updateblog = Blog::find($_GET['edit']);
            $updateblog->title = escape($_POST['title']);
            $updateblog->content = escape($_POST['content']);
            $updateblog->short_des = str_limit(escape($_POST['content']),150);
            $updateblog->category = $_POST['category'];
            $updateblog->pro_cat = $pro_cat;
            $updateblog->section = $_POST['section'];
            $updateblog->save();
            if (request()->file('image')) {
                $file = request()->file('image');
                $name = $file->getClientOriginalName();
                if (in_array($file->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                    $image = md5(time()).'.'.$file->getClientOriginalExtension();
                    $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'blog/';
                    $file->move($path,$image);
                    $img = Image::make($path.$image)->resize(800, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($path.$image);
                } else {
                    $data['notices'] .= "<div class='alert mini alert-warning'> $name is not a valid format</div>";
                }
                $updateimage = Blog::find($_GET['edit']);
                $updateimage->images = $image;
                $updateimage->save();
            }
            if (request()->file('banner_image')) {
                $bfile = request()->file('banner_image');
                $bname = $bfile->getClientOriginalName();
                if (in_array($bfile->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                    $bimage = md5(time()).'.'.$bfile->getClientOriginalExtension();
                    $bpath = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'blog/';
                    $bfile->move($bpath,$bimage);
                    $img = Image::make($bpath.$bimage)->resize(800, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($bpath.$bimage);
                } else {
                    $data['notices'] .= "<div class='alert mini alert-warning'> $bname is not a valid format</div>";
                }
                $updatebanner = Blog::find($_GET['edit']);
                $updatebanner->banner_image = $bimage;
                $updatebanner->save();
            }
            $data['notices'] .= '<div class="alert mini alert-success">Post has been updated successfully !</div>';
        }
        if(isset($_GET['delete']))
        {
            $blogdelete = Blog::find($_GET['delete']);
            $blogdelete->delete();
            $data['notices'] .= "<div class='alert alert-success'> Post has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Blog','blog');
        $data['posts'] = Blog::orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['post'] = Blog::where('id','=',$_GET['edit'])->first();
        }
        $data['categories'] = AdviceCategory::all();
        $data['procat'] = Category::where('parent',0)->orderBy('id','DESC')->get();
        $data['footer'] = $this->footer();
        return view('admin/blog')->with('data',$data);
    }
    public function advicesFaq(){
        $data['notices'] = '';
        if(isset($_GET['edit'])){
            $data['faq'] = DB::table('advices_faq')->where('id','=',$_GET['edit'])->first();
        }
        if(isset($_POST['edit'])){
            $status = $_POST['status'];
            $answer = $_POST['answer'];
            DB::update("UPDATE advices_faq SET answer = '".$answer."', status = '".$status."' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> FAQ successfully updated !</div>";
        }
        $data['header'] = $this->header('FAQ(s) Master','faq');
        $data['footer'] = $this->footer();
        $data['faqs'] = DB::table('advices_faq')->orderBy('id','DESC')->get();
        return view('admin/advices_faq')->with('data',$data);
    }
    public function advices_reviews(){
        $notices = '';
        if(isset($_GET['approve']))
        {
            DB::update("UPDATE advices_reviews SET active = '1' WHERE id = ".$_GET['approve']);
            $notices = "<div class='alert alert-success'> Review has been approved !</div>";
        }
        $header = $this->header('Admin','Advices reviews');
        $reviews = DB::table('advices_reviews')->orderBy('id','DESC')->get();
        $footer = $this->footer();
        return view('admin/advices_reviews')->with(compact('header','notices','reviews','footer'));
    }
    public function advices_category(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $advice         =   new AdviceCategory();
            $advice->name   =   $_POST['name'];
            $advice->parent = $_POST['parent'];
            $advice->slug = makeSlugOf($_POST['name'], 'advices_category');;
            $advice->short_description = $_POST['short_description'];
            $datas['banner_image'] = '';
            $advice->save();
            $catdata = $advice->id;
            if (request()->file('banner_image')) {
                $file = request()->file('banner_image');
                $name = $file->getClientOriginalName();
                if (in_array($file->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                    $image = md5(time()).'.'.$file->getClientOriginalExtension();
                    $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'blog/';
                    $file->move($path,$image);
                    $img = Image::make($path.$image)->resize(1200, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($path.$image);
                } else {
                    $data['notices'] .= "<div class='alert mini alert-warning'> $name is not a valid format</div>";
                }
                $banner_image   =   AdviceCategory::find($catdata);
                $banner_image->banner_image = $image;
                $banner_image->save();
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Category has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $updatecategory     =   AdviceCategory::find($_GET['edit']);
            $updatecategory->name = escape($_POST['name']);
            $updatecategory->parent = $_POST['parent'];
            $updatecategory->short_description = $_POST['short_description'];
            $updatecategory->slug = makeSlugOf(escape($_POST['name']), 'advices_category');
            $updatecategory->save();
            if (request()->file('banner_image')) {
                $file = request()->file('banner_image');
                $name = $file->getClientOriginalName();
                if (in_array($file->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                    $image = md5(time()).'.'.$file->getClientOriginalExtension();
                    $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'blog/';
                    $file->move($path,$image);
                    $img = Image::make($path.$image)->resize(1200, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($path.$image);
                } else {
                    $data['notices'] .= "<div class='alert mini alert-warning'> $name is not a valid format</div>";
                }
                $updatebanner    =   AdviceCategory::find($_GET['edit']);
                $updatebanner->banner_image =   $image;
                $updatebanner->save();
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Category edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("advices_category")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Category has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Pages','pages');
        $data['categories'] = DB::table('advices_category')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['category'] = DB::table('advices_category')->where('id','=',$_GET['edit'])->first();
        }
        $data['tp'] = url("/themes/".$this->cfg->theme);
        $data['parents'] = DB::table('advices_category')->where('parent','=',0)->orderBy('id','DESC')->get();
        $data['footer'] = $this->footer();
        return view('admin/advices_category')->with('data',$data);
    }
}
