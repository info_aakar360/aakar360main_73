<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;
class SellerController extends Admin
{
    //
    public function seller(){
        $notices = '';
        if(isset($_GET['delete']))
        {
            DB::table('customers')->where('id','=',$_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'> Customer deleted successfully !</div>";
        }
        if(isset($_GET['approve']))
        {
            DB::update("UPDATE customers SET status = '1' WHERE id = '".$_GET['approve']."'");
            $notices .= "<div class='alert alert-success'> Customer Approve successfully !</div>";
        }
        if(isset($_GET['reject']))
        {
            DB::update("UPDATE customers SET status = '0' WHERE id = '".$_GET['reject']."'");
            $notices .= "<div class='alert alert-success'> Customer Reject successfully !</div>";
        }
        if(isset($_GET['insshow']))
        {
            DB::update("UPDATE customers SET institutional_status = '1' WHERE id = '".$_GET['insshow']."'");
            $notices .= "<div class='alert alert-success'> Institutional detail show successfully !</div>";
        }
        if(isset($_GET['inshide']))
        {
            DB::update("UPDATE customers SET institutional_status = '0' WHERE id = '".$_GET['inshide']."'");
            $notices .= "<div class='alert alert-danger'> Institutional detail Hide successfully !</div>";
        }
        $header = $this->header('Seller','seller');
        $tp = url("/themes/".$this->cfg->theme);
        $customers = DB::table('customers')->where('user_type','=','seller')->get();
        $footer = $this->footer();
        return view('admin/seller')->with(compact('header','notices','tp','customers','footer'));
    }

    public function pendingproduct(){
        $notices = '';

        if(isset($_GET['delete']))
        {
            $pid = $_GET['delete'];
            date_default_timezone_set('Asia/Kolkata');
            $dt_date = date("Y-m-d H:i:s");
            DB::delete("update products SET status='deleted', deleted_at = '".$dt_date."' where id = $pid ");
            $notices .= "<div class='alert alert-success'> Product deleted successfully !</div>";
            return back();
        }
        if(isset($_GET['approve']))
        {
            DB::update("UPDATE products SET status = 'approve', `deleted_at` = NULL WHERE id = '".$_GET['approve']."'");
            $notices .= "<div class='alert alert-success'> Product Approve successfully !</div>";
            return back();
        }
        if(isset($_GET['reject']))
        {
            DB::update("UPDATE products SET status = 'reject', `deleted_at` = NULL WHERE id = '".$_GET['reject']."'");
            $notices .= "<div class='alert alert-success'> Products Reject successfully !</div>";
            return back();
        }
        $header = $this->header('Product','pendingproduct');
        $tp = url("/themes/".$this->cfg->theme);
        $products = DB::select("SELECT *,(select email from customers where id = added_by ) as email FROM `products` WHERE `status` = 'pending' and deleted_at IS NULL and `added_by` <> 0");
        $footer = $this->footer();
        return view('admin/pending-product')->with(compact('header','notices','tp','products','footer'));
    }

    public function approvalproduct(){
        $notices = '';
        $header = $this->header('Approval Product','approvalproduct');
        $products = DB::select("SELECT * FROM `products` WHERE status = 'approve'");
        $footer = $this->footer();
        $tp = url("/themes/".$this->cfg->theme);
        return view('admin/approval-product')->with(compact('header','notices','tp','products','footer'));
    }
    public function rejectproduct(){
        $notices = '';
        $header = $this->header('Reject Product','rejectproduct');
        $products = DB::select("SELECT * FROM `products` WHERE status = 'reject'");
        $footer = $this->footer();
        $tp = url("/themes/".$this->cfg->theme);
        return view('admin/reject-product')->with(compact('header','notices','tp','products','footer'));
    }
    public function deleteproduct(){
        $notices = '';
        $header = $this->header('Deleted Product','deleteproduct');
        $products = DB::select("SELECT * FROM `products` WHERE deleted_at  <> ''");
        $footer = $this->footer();
        $tp = url("/themes/".$this->cfg->theme);
        return view('admin/delete-product')->with(compact('header','notices','tp','products','footer'));
    }

    public function sellersPayment(){
        $notices = '';
        if(isset($_POST['add'])){
            $bal_amount = $_POST['balance_amount'] - $_POST['paid_amount'];
            DB::insert("INSERT INTO multiseller_payments (seller_id,order_id,payment_type,payment_status,payment_method,payment_data,amount,paid_amount,balance_amount,description,remark) 
              VALUE ('".$_POST['seller_id']."','".$_POST['order_id']."','".$_POST['payment_type']."''".$_POST['payment_status']."','".$_POST['payment_method']."','".$_POST['payment_data']."','0','".$_POST['paid_amount']."','".$bal_amount."','".escape($_POST['description'])."','".escape($_POST['remark'])."')");
            $notices .= "<div class='alert mini alert-success'> Payment has been successfully added !</div>";
        }
        $header = $this->header('Sellers Payments','sellers_payments');
        $tp = url("/themes/".$this->cfg->theme);
        $footer = $this->footer();
        $mpayments = DB::table('multiseller_payments')->groupBy('multiseller_payments.id','multiseller_payments.seller_id','multiseller_payments.order_id','multiseller_payments.cr_id','multiseller_payments.dr_id','multiseller_payments.payment_type','multiseller_payments.payment_status','multiseller_payments.payment_method','multiseller_payments.payment_data','multiseller_payments.amount','multiseller_payments.paid_amount','multiseller_payments.balance_amount','multiseller_payments.description','multiseller_payments.remark','multiseller_payments.adjustments','multiseller_payments.date_created','multiseller_payments.date_paid','multiseller_payments.dealer_id')->orderBy('id','DESC')->get();
        $orderid = DB::table('multiseller_payments')->groupBy('multiseller_payments.id','multiseller_payments.seller_id','multiseller_payments.order_id','multiseller_payments.cr_id','multiseller_payments.dr_id','multiseller_payments.payment_type','multiseller_payments.payment_status','multiseller_payments.payment_method','multiseller_payments.payment_data','multiseller_payments.amount','multiseller_payments.paid_amount','multiseller_payments.balance_amount','multiseller_payments.description','multiseller_payments.remark','multiseller_payments.adjustments','multiseller_payments.date_created','multiseller_payments.date_paid','multiseller_payments.dealer_id')->get();
        $sellers = DB::table('customers')->where('user_type','=','seller')->orderBy('id','DESC')->get();
        return view('admin/sellers-payment')->with(compact('header','notices','mpayments','orderid','sellers','tp','footer'));
    }

    public function SellerPo(){
        $data['notices'] = '';
        $data['header'] = $this->header('Seller Po.','seller_po');
        $data['tp'] = url("/themes/".$this->cfg->theme);
        $data['footer'] = $this->footer();
        if(isset($_GET['view'])) {
            $data['order'] = DB::table('seller_po')->where('id','=',$_GET['view'])->limit(1)->first();
        }
        $data['sellerpo'] = DB::table('seller_po')->orderBy('id','DESC')->get();
        return view('admin/seller_po')->with('data',$data);
    }
    public function CrNote(){
        $notices = '';
        if(isset($_POST['add'])){
            $bal_amount = $_POST['balance_amount'] + $_POST['paid_amount'];
            $cr = DB::insert("INSERT INTO cr_note (seller_id,order_id,payment_data,amount,paid_amount,description,remark) 
              VALUE ('".$_POST['seller_id']."','".$_POST['order_id']."','".$_POST['payment_data']."','".$_POST['amount']."','".$_POST['paid_amount']."','".escape($_POST['description'])."','".escape($_POST['remark'])."')");
            if($cr) {
                $id = DB::getPdo()->lastInsertId();
                $lid = 'cr_'.$id;
                $notices .= "<div class='alert mini alert-success'> Cr Note has been successfully added !</div>";

                DB::insert("INSERT INTO multiseller_payments (seller_id,order_id,cr_id,payment_data,amount,paid_amount,balance_amount,description,remark) 
              VALUE ('".$_POST['seller_id']."','".$_POST['order_id']."','".$lid."','".$_POST['payment_data']."','0','".$_POST['paid_amount']."','".$bal_amount."','".escape($_POST['description'])."','".escape($_POST['remark'])."')");
            }
        }

        $header = $this->header('Cr. Note','cr_note');
        $tp = url("/themes/".$this->cfg->theme);
        $footer = $this->footer();
        $mpayments = DB::table('cr_note')->orderBy('id','DESC')->get();
        $orderid = DB::table('multiseller_payments')->groupBy('multiseller_payments.id','multiseller_payments.seller_id','multiseller_payments.order_id','multiseller_payments.cr_id','multiseller_payments.dr_id','multiseller_payments.payment_type','multiseller_payments.payment_status','multiseller_payments.payment_method','multiseller_payments.payment_data','multiseller_payments.amount','multiseller_payments.paid_amount','multiseller_payments.balance_amount','multiseller_payments.description','multiseller_payments.remark','multiseller_payments.adjustments','multiseller_payments.date_created','multiseller_payments.date_paid','multiseller_payments.dealer_id')->get();
        $sellers = DB::table('customers')->where('user_type','=','seller')->orderBy('id','DESC')->get();
        return view('admin/cr-note')->with(compact('header','notices','mpayments','orderid','sellers','tp','footer'));
    }

    public function DrNote(){
        $notices = '';
        if(isset($_POST['add'])){
            $bal_amount = $_POST['balance_amount'] - $_POST['paid_amount'];
            $dr = DB::insert("INSERT INTO dr_note (seller_id,order_id,payment_data,amount,paid_amount,description,remark) 
                VALUE ('".$_POST['seller_id']."','".$_POST['order_id']."','".$_POST['payment_data']."','".$_POST['amount']."','".$_POST['paid_amount']."','".escape($_POST['description'])."','".escape($_POST['remark'])."')");
            if($dr) {
                $id = DB::getPdo()->lastInsertId();
                $lid = 'dr_'.$id;
                $notices .= "<div class='alert mini alert-success'> Dr Note has been successfully added !</div>";

                DB::insert("INSERT INTO multiseller_payments (seller_id,order_id,dr_id,payment_data,amount,paid_amount,balance_amount,description,remark) 
                VALUE ('".$_POST['seller_id']."','".$_POST['order_id']."','".$lid."','".$_POST['payment_data']."','0','".$_POST['paid_amount']."','".$bal_amount."','".escape($_POST['description'])."','".escape($_POST['remark'])."')");
            }
        }
        $header = $this->header('Dr. Note','dr_note');
        $tp = url("/themes/".$this->cfg->theme);
        $footer = $this->footer();
        $mpayments = DB::table('dr_note')->orderBy('id','DESC')->get();
        $orderid = DB::table('multiseller_payments')->groupBy('multiseller_payments.id','multiseller_payments.seller_id','multiseller_payments.order_id','multiseller_payments.cr_id','multiseller_payments.dr_id','multiseller_payments.payment_type','multiseller_payments.payment_status','multiseller_payments.payment_method','multiseller_payments.payment_data','multiseller_payments.amount','multiseller_payments.paid_amount','multiseller_payments.balance_amount','multiseller_payments.description','multiseller_payments.remark','multiseller_payments.adjustments','multiseller_payments.date_created','multiseller_payments.date_paid','multiseller_payments.dealer_id')->get();
        $sellers = DB::table('customers')->where('user_type','=','seller')->orderBy('id','DESC')->get();
        return view('admin/dr-note')->with(compact('header','notices','mpayments','orderid','sellers','tp','footer'));
    }
}
