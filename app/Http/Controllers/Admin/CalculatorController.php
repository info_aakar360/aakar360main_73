<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;
class CalculatorController extends Admin
{
    /*Abhijeet code start*/

    public function Tmtcalculator(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $data['brand_id'] = $_POST['brand_id'];
            $data['brand_name'] = $_POST['brand_name'];
            $data['variant_name'] = $_POST['variant_name'];
            $data['kg'] = $_POST['kg'];
            $data['rod'] = $_POST['rod'];
            $data['bundle'] = $_POST['bundle'];
            $tmt = DB::table('tmt_calculator')->insertGetId($data);
            $data['notices'] .= "<div class='alert alert-success'> TMT conversion created successfully !</div>";
        }
        if(isset($_POST['edit'])){

            $variant_name  = $_POST['variant_name'];
            $kg = $_POST['kg'];
            $rod = $_POST['rod'];
            $bundle = $_POST['bundle'];

            DB::update("UPDATE tmt_calculator SET variant_name = '$variant_name', kg = '$kg' , rod = '$rod' , bundle = '$bundle' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= '<div class="alert mini alert-success">Conversion has been updated successfully !</div>';
        }
        if(isset($_GET['delete']))
        {
            DB::table("tmt_calculator")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'>Conversion has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('TMT Calculator','tmtcalculator');
        $data['tmtbars'] = DB::table('tmt_calculator')->get();
        $data['brand'] = DB::table('brand')->get();
        if(isset($_GET['edit'])) {
            $data['tmtconversion'] = DB::table('tmt_calculator')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/tmtcalculator')->with('data',$data);
    }
    /*Abhijeet code end*/
}
