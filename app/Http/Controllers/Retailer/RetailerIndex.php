<?php

namespace App\Http\Controllers\Retailer;

use App\AdviceCategory;
use App\Banner;
use App\Blocs;
use App\Blog;
use App\Brand;
use App\Browser;
use App\Category;
use App\City;
use App\CityWise;
use App\Config;
use App\Countries;
use App\Country;
use App\Currency;
use App\Customer;
use App\CustomerCache;
use App\DefaultImage;
use App\DesignCategory;
use App\Footer;
use App\Http\Controllers\FrontendBaseController;
use App\ImageInspiration;
use App\ImageLike;
use App\Lang;
use App\LayoutWishlist;
use App\ManufacturingHubs;
use App\MegaMenuImage;
use App\Menu;
use App\Os;
use App\PasswordReset;
use App\Payments;
use App\ProductWishlist;
use App\RateRequests;
use App\Referrer;
use App\RegisterImage;
use App\ServiceCategory;
use App\ServiceWishlist;
use App\SpecialRequirements;
use App\Style;
use App\Tracking;
use App\UserProject;
use App\Visitor;
use Illuminate\Auth\Authenticatable;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use SebastianBergmann\CodeCoverage\Node\Directory;
use Image;
use Illuminate\Support\Facades\Session;

//use Auth;

class RetailerIndex extends FrontendBaseController
{
    public function index()
    {
        $data['cityWise'] = 0;
        if(isset($_COOKIE['cities'])) {
            $data['cityWise'] = $_COOKIE['cities'];
        }
        $data['header'] = $this->retailHeader(false,false,false,true);
        $data['footer'] = $this->footer();
        $vtype=Session::get('aakar360.visitor_type');
        $data['style'] = $this->style;
        if(preg_match("/(youtube.com)\/(watch)?(\?v=)?(\S+)?/", $this->style->media)){
            parse_str(parse_url($this->style->media, PHP_URL_QUERY),$video);
            $data['media'] = '<a target="_blank" href="'.$this->style->media.'"><img class="landing-video" src="https://i3.ytimg.com/vi/'.$video['v'].'/mqdefault.jpg"></a>';
        } else {
            $data['media'] = '<img class="landing-image" src="'.$this->style->media.'">';
        }
        $blocs1 = Blocs::where('area','home')->first();
        if(!empty($blocs1)) {
            $data['blocs'] = $blocs1;
        }else{
            $data['blocs'] = 'Data Not Available';
        }
        $banners1 = Banner::where('section','home')->orderBy('priority','ASC')->get();
        if(count($banners1)){
            $data['banners'] = $banners1;
        }else{
            $data['banners'] = false;
        }
        $posts1 = Blog::orderBy('time','DESC')->take(6)->get();
        if(count($posts1)){
            $data['posts'] = $posts1;
        }else{
            $data['posts'] = 'Data Not Available';
        }
        $categoryforhomebrand1 = Category::where('popular_in_brands',1)->orderBy('popular_brands_priority','ASC')->take(1)->get();
        if(count($categoryforhomebrand1)){
            $data['categoryforhomebrand'] = $categoryforhomebrand1;
        }else{
            $data['categoryforhomebrand'] = 'Data Not Available';
        }
        $service_categories1 = ServiceCategory::where('parent',0)->orderBy('name','ASC')->get();
        if(!empty($service_categories1)){
            $data['service_categories'] = $service_categories1;
        }else{
            $data['service_categories'] = 'Data Not Available';
        }
        $design_categories1 = DesignCategory::where('parent',0)->orderBy('name','ASC')->get();
        if (!empty($design_categories1)){
            $data['design_categories'] = $design_categories1;
        }else{
            $data['design_categories'] = 'Data Not Available';
        }
        $data['show_cities'] = false;
        $preferences = null;
        if(isset($_POST['cities'])){
            setcookie('cities', $_POST['cities'], time() + (86400 * 30));
            return redirect(route('retail.retailIndex'));
        }
        if(isset($_COOKIE['cities'])){
            $preferences = explode(',', $_COOKIE['cities']);
        }else{
            $preferences = null;
        }
        if($preferences === null){
            $data['show_cities'] = true;
        }
        $data['rem_pre'] = 0;
        if($preferences === null){
            $data['pcss'] = CityWise::join(
                'category','category.id','=','city_wise.category_id'
            )->where('category.popular', 1)->orderBy('category.popular_priority', 'ASC')->select('category.*')->skip(0)->take(5)->get();
        }else{
            $data['pcss'] = CityWise::join(
                'category','category.id','=','city_wise.category_id'
            )->where('category.popular', 1)->whereIn('city_wise.city', $preferences)->select('category.*')->orderBy('category.popular_priority', 'ASC')->skip(0)->take(5)->get();
            $re = count($data['pcss']);
            $limit = 5 - $re;
            if($limit){
                $more = CityWise::join(
                    'category','category.id','=','city_wise.category_id'
                )
                    ->where('category.popular', 1)
                    ->whereIn('city_wise.city', $preferences)
                    ->orderBy('category.popular_priority', 'ASC')
                    ->select('category.*')
                    ->skip(0)->take($limit)
                    ->get();
                $data['pcss'] = $data['pcss']->merge($more);
            }else{
                $data['rem_pre'] = $re-5;
            }
        }
        $data['cities'] = City::where('is_available','1')->orderBy('name','ASC')->get();
        $data['tp'] = url("/assets/");
        return view('index')->with('data',$data);
    }
}
