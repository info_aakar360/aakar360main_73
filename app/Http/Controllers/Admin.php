<?php
namespace App\Http\Controllers;

use App\AdviceCategory;
use App\AdviceSave;
use App\Blog;
use App\Bookings;
use App\Category;
use App\City;
use App\CityWise;
use App\CityWiseShippingCharges;
use App\Customer;
use App\DispatchPrograms;
use App\ManufacturingHubs;
use App\PhRelations;
use App\Products;
use App\SpecialRequirements;
use App\State;
use App\Unit;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;
use ZipArchive;
use Illuminate\Support\Facades\Auth;
use Image;
use Yajra\Datatables\Datatables;

class Admin extends Controller
{
    public $cfg;
    public $style;
    public $buttons;
    public function __construct()
    {
        $this->cfg = DB::select('SELECT * FROM config WHERE id = 1') [0];
        $this->style = DB::select('SELECT * FROM style') [0];
        $this->buttons = "['copy', 'excel', 'pdf']";
    }

    public function header($title = 'Admin', $area = false)
    {
        $title = $title . ' | ' . $this
                ->cfg->name;
        $cfg = $this->cfg;
        $tp = url("/themes/" . $this
                ->cfg
                ->theme);
        return view('admin/header')
            ->with(compact('title', 'cfg', 'tp', 'area'))
            ->render();
    }

    public function footer()
    {
        $tp = url("/themes/" . $this
                ->cfg
                ->theme);
        return view('admin/footer')
            ->with(compact('tp'))
            ->render();
    }

    public function login()
    {
        if (isset($_POST['login']))
        {

            $email = escape($_POST['email']);
            $pass = md5($_POST['pass']);
            if (empty($email) or empty($pass))
            {
                $error = '<div class="alert alert-dismissible alert-danger"> All fields are required </div>';
            }
            else
            {
                if (DB::select("SELECT COUNT(*) as count FROM user WHERE u_email = '" . $email . "' AND u_pass = '" . $pass . "' ") [0]->count > 0)
                {
                    $secure_id = md5(microtime());
                    DB::update("UPDATE user SET secure = '$secure_id' WHERE u_email = '" . $email . "'");
                    session(['admin' => $secure_id]);
                    return redirect('admin');
                }
                else
                {
                    $error = '<div class="alert alert-dismissible alert-danger"> Wrong email or password </div>';
                }
            }
        }
        $data['cfg'] = $this->cfg;
        $data['tp'] = url("/themes/" . $this
                ->cfg
                ->theme);
        return view('admin/login')
            ->with('data', $data)->render();
    }
    public function logout()
    {
        session(['admin' => '']);
        return redirect('admin/login');
    }

    public function index()
    {
        for ($day = 6;$day >= 0;$day--)
        {
            $d[7 - $day] = "'" . date('Y-m-d', strtotime("-" . $day . " day")) . "'";
            $i[date('Y-m-d', strtotime("-" . $day . " day")) ] = 0;
            $o[date('Y-m-d', strtotime("-" . $day . " day")) ] = 0;
            $s[date('Y-m-d', strtotime("-" . $day . " day")) ] = 0;
            $c[date('Y-m-d', strtotime("-" . $day . " day")) ] = 0;
        }
        $fs = DB::table("visitors")->where('date', '>', $d[1])->orderBy('date', 'ASC')
            ->get();
        foreach ($fs as $visits)
        {
            $i[$visits
                ->date] = $visits->visits;
        }
        $yesterday = date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'));
        $yvisits = (!in_array($yesterday, $i)) ? $i[$yesterday] : 0;
        $tvisits = (!in_array(date('Y-m-d') , $i)) ? $i[date('Y-m-d') ] : 0;
        $order_query = DB::select("SELECT * FROM orders WHERE date > " . $d[1] . " ORDER BY date ASC");
        foreach ($order_query as $order)
        {
            $o[$order->date] = $o[$order->date] + 1;
            $s[$order->date] = $s[$order
                    ->date] + $order->summ;
            $c[$order->date] = $o[$order->date] / $i[$order->date] * 100;
        }
        if ($yorders = DB::table("orders")->where('date', '=', $yesterday)->count() > 0)
        {
            $ysales = DB::table("orders")->select(DB::raw('SUM(summ) as sum'))
                ->where('date', '=', $yesterday)->sum('summ');
        }
        else
        {
            $ysales = 0;
        }
        $torders = DB::table("visitors")->select(DB::raw('COUNT(*) as count'))
            ->where('date', '=', date('Y-m-d'))
            ->count();
        if ($torders > 0)
        {
            $tsales = DB::table("orders")->select(DB::raw('SUM(summ) as sum'))
                ->where('date', '=', date('Y-m-d'))
                ->sum('summ');
        }
        else
        {
            $tsales = 0;
        }
        $yconversion = $c[$yesterday];
        $tconversion = $c[date('Y-m-d') ];
        $mvisits = max($i) + max($i) * 2 / 6 + 1;
        $morders = max($o) + max($o) * 2 / 6 + 1;
        $msales = max($s) + max($s) * 2 / 6 + 1;
        $mconversion = max($c) + max($c) * 2 / 6 + 1;
        $porders = p($yorders, $torders);
        $pvisits = p($yvisits, $tvisits);
        $psales = p($ysales, $tsales);
        $pconversion = p($yconversion, $tconversion);
        $stat['0'] = DB::table("orders")->select(DB::raw('COUNT(*) as count'))
            ->count();
        $stat['1'] = DB::table("orders")->select(DB::raw('COUNT(*) as count'))
            ->where('stat', '=', 1)
            ->count();
        $stat['2'] = DB::table("orders")->select(DB::raw('COUNT(*) as count'))
            ->where('stat', '=', 2)
            ->count();
        $stat['3'] = DB::table("orders")->select(DB::raw('COUNT(*) as count'))
            ->where('stat', '=', 3)
            ->count();
        $stat['4'] = DB::table("orders")->select(DB::raw('COUNT(*) as count'))
            ->where('stat', '=', 4)
            ->count();
        $emails = array();
        $emails['orders'] = DB::table("orders")->select(DB::raw('COUNT(email) as count'))
            ->count('email');
        $emails['support'] = DB::table("tickets")->select(DB::raw('COUNT(email) as count'))
            ->count('email');
        $emails['newsletter'] = DB::table("subscribers")->select(DB::raw('COUNT(email) as count'))
            ->count('email');
        $chart = array();
        $chart['days'] = implode(', ', $d);
        $i = implode(', ', $i);
        $o = implode(', ', $o);
        $s = implode(', ', $s);
        $c = implode(', ', $c);
        $ssales = DB::table("orders")->select(DB::raw('SUM(summ) as sum'))
            ->sum('summ');
        $orders = DB::table("orders")->orderBy('id', 'DESC')
            ->limit(3)
            ->get();
        $reviews = DB::table("reviews")->orderBy('id', 'DESC')
            ->limit(3)
            ->get();
        $tickets = DB::table("tickets")->orderBy('id', 'DESC')
            ->limit(3)
            ->get();
        $referrers = DB::table("referrer")->orderBy('visits', 'DESC')
            ->limit(3)
            ->get();
        $oss = DB::table("os")->orderBy('visits', 'DESC')
            ->limit(3)
            ->get();
        $browsers = DB::table("browsers")->orderBy('visits', 'DESC')
            ->limit(3)
            ->get();
        $subscribers = DB::table("subscribers")->limit(6)
            ->get();
        $countries = DB::table("country")->orderBy('visitors', 'DESC')
            ->orderBy('orders', 'DESC')
            ->limit(10)
            ->get();
        $cfg = $this->cfg;
        $tp = url("/themes/" . $this
                ->cfg
                ->theme);
        $header = $this->header('Admin', 'index');
        $footer = $this->footer();
        return view('admin/index')
            ->with(compact('header', 'cfg', 'tp', 'd', 'i', 'o', 's', 'c', 'stat', 'porders', 'pvisits', 'psales', 'pconversion', 'ssales', 'orders', 'reviews', 'tickets', 'referrers', 'oss', 'browsers', 'emails', 'subscribers', 'countries', 'chart', 'morders', 'mvisits', 'mconversion', 'msales', 'footer'));
    }

    public function map()
    {
        $json = array();
        $countries = DB::table("country")->orderBy('id', 'ASC')
            ->get();
        foreach ($countries as $country)
        {
            $json[strtolower($country->iso) ] = array(
                'total' => $country->orders,
                'visitors' => $country->visitors,
            );
        }
        return json_encode($json);
    }

    public function phRelations()
    {
        $notices = '';
        if (isset($_POST['add']))
        {
            $data['hubs'] = implode(',', $_POST['hubs']);
            $prices = $_POST['price'];
            $lprices = $_POST['lprice'];
            $pros = array();
            foreach ($_POST['products'] as $key => $p)
            {
                $pros[] = array(
                    'product' => $p,
                    'price' => $prices[$key],
                    'loading' => $lprices[$key]
                );
            }
            $data['products'] = json_encode($pros);
            PhRelations::insert($data);
            $notices .= "<div class='alert mini alert-success'>PH Relation has been added successfully!</div>";
        }
        if (isset($_POST['edit']))
        {
            $ph = PhRelations::find($_GET['edit']);
            if ($ph === null)
            {
                return back();
            }
            $ph->hubs = implode(',', $_POST['hubs']);
            $prices = $_POST['price'];
            $lprices = $_POST['lprice'];
            $pros = array();
            foreach ($_POST['products'] as $key => $p)
            {
                $pros[] = array(
                    'product' => $p,
                    'price' => $prices[$key],
                    'loading' => $lprices[$key]
                );
            }
            $ph->products = json_encode($pros);
            $ph->save();
            $notices .= "<div class='alert mini alert-success'>PH Relation edited successfully !</div>";
        }
        if (isset($_GET['delete']))
        {
            DB::table('ph_relations')->where('id', $_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'>PH Relations has been deleted successfully !</div>";
        }
        $header = $this->header('Products-Hubs Relations', 'phrelations');
        $hubs = ManufacturingHubs::orderBy('title', 'ASC')->get();
        $products = Products::orderBy('title', 'ASC')->get();
        $phs = PhRelations::orderBy('id', 'DESC')->get();
        $ph = null;
        if (isset($_GET['edit']))
        {
            $ph = PhRelations::find($_GET['edit']);
        }
        $footer = $this->footer();
        return view('admin/ph-relations')
            ->with(compact('header', 'notices', 'hubs', 'phs', 'ph', 'products', 'footer'));
    }
    public function updateQuestions()
    {
        $notices = '';
        $header = $this->header('Question Master', 'questions');
        if (isset($_POST['update']))
        {
            DB::table("services_questions")->where('service_id', '=', $_GET['edit'])->where('question_id', '=', $_POST['question'])->delete();
            DB::insert("INSERT INTO services_questions (service_id,question_id,priority) VALUE ('" . $_GET['edit'] . "','" . $_POST['question'] . "','" . $_POST['priority'] . "')");
            $notices .= "<div class='alert mini alert-success'> Question has been successfully added !</div>";
        }
        if (isset($_GET['delete']))
        {
            DB::table("services_questions")->where('id', '=', $_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'>Question has been deleted successfully !</div>";
        }
        $footer = $this->footer();
        $questions = DB::table('questions')->orderBy('question', 'ASC')
            ->get();
        $service_questions = DB::table('services_questions')->where('service_id', '=', $_GET['edit'])->orderBy('priority', 'ASC')
            ->get();
        return view('admin/update-questions')
            ->with(compact('questions', 'header', 'footer', 'notices', 'service_questions'));
    }

    public function questions()
    {
        $notices = '';
        if (isset($_POST['add']))
        {
            $question = $_POST['question'];
            $options = json_encode($_POST['option']);
            $weightage = $_POST['weightage'];
            $type = $_POST['type'];
            DB::insert("INSERT INTO questions (question,options,weightage,type) VALUE ('$question','$options','$weightage','$type')");
            $notices .= "<div class='alert mini alert-success'> Question has been successfully added !</div>";
        }
        if (isset($_POST['edit']))
        {
            $question = $_POST['question'];
            $options = json_encode($_POST['option']);
            $weightage = $_POST['weightage'];
            $type = $_POST['type'];
            DB::update("UPDATE questions SET question = '$question',options = '$options',weightage = '$weightage',type = '$type' WHERE id = '" . $_GET['edit'] . "'");
            $notices .= "<div class='alert mini alert-success'> Data Updated successfully !</div>";
        }
        if (isset($_GET['delete']))
        {
            DB::table("questions")->where('id', '=', $_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'> Question has been deleted successfully !</div>";
        }
        $header = $this->header('Question Master', 'questions');
        $services = DB::table('services')->orderBy('id', 'DESC')
            ->get();
        $que = DB::table('questions')->orderBy('name', 'ASC')
            ->get();
        if (isset($_GET['edit']))
        {
            $ques = DB::table('questions')->where('id', '=', $_GET['edit'])->first();
        }
        $footer = $this->footer();
        return view('admin/questions')
            ->with(compact('header', 'notices', 'services', 'ques', 'que', 'question', 'option_1', 'option_2', 'option_3', 'option_4', 'weightage', 'type', 'footer'));
    }

    public function catData()
    {
        $html = '';
        $page = $_POST['page'];
        if ($page == 'Products' || $page == 'Single Product' || $page == 'Brand')
        {
            $categories = DB::table('category')->where('parent', 0)
                ->orderBy('id', 'DESC')
                ->get();
            $html = '
                <label class="control-label">Product category</label>
                <select name="category" class="form-control">';
            foreach ($categories as $category)
            {
                $html .= '<option value="' . $category->id . '">' . $category->name . '</option>';
                $childs = DB::table("category")->where('parent', '=', $category->id)
                    ->orderBy('id', 'DESC')
                    ->get();
                foreach ($childs as $child)
                {
                    $html .= '<option value="' . $child->id . '">- ' . $child->name . '</option>';
                    $subchilds = DB::table("category")->where('parent', '=', $child->id)
                        ->orderBy('id', 'DESC')
                        ->get();
                    foreach ($subchilds as $subchild)
                    {
                        $html .= '<option value="' . $subchild->id . '"><i style="padding-left: 10px;">--> ' . $subchild->name . '</i></option>';
                    }
                }
            }
            $html .= '</select>';
            return $html;
        }
        else
        {
            return $html;
        }
    }

    public function fields()
    {
        $notices = '';
        if (isset($_POST['add']))
        {
            $name = $_POST['name'];
            $code = $_POST['code'];
            DB::statement(DB::raw("ALTER TABLE `orders` ADD `" . $code . "` VARCHAR(255) NOT NULL"));
            DB::insert("INSERT INTO fields (name,code) VALUE ('$name','$code')");
            $notices .= "<div class='alert mini alert-success'> field has been added !</div>";
        }
        if (isset($_POST['edit']))
        {
            $name = $_POST["name"];
            $code = $_POST["code"];
            $field = DB::table('fields')->where('id', '=', $_GET['edit'])->first();
            DB::statement(DB::raw("ALTER TABLE `orders` CHANGE `" . $field->code . "` `" . $code . "` VARCHAR(255) NOT NULL"));
            DB::update("UPDATE fields SET name = '$name',code = '$code' WHERE id = '" . $_GET['edit'] . "'");
            $notices .= "<div class='alert mini alert-success'> Field has been updated successfully !</div>";
        }
        if (isset($_GET['delete']))
        {
            $field = DB::table('fields')->where('id', '=', $_GET['delete'])->first();
            DB::statement(DB::raw("ALTER TABLE `orders` DROP `" . $field->code . "`"));
            DB::table("fields")
                ->where('id', '=', $_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'> The field has been deleted successfully !</div>";
        }
        if (isset($_GET['edit']))
        {
            $field = DB::table('fields')->where('id', '=', $_GET['edit'])->first();
        }
        $header = $this->header('Extrafields', 'fields');
        $fields = DB::table('fields')->get();
        $footer = $this->footer();
        return view('admin/fields')
            ->with(compact('header', 'notices', 'field', 'fields', 'footer'));
    }

    public function updatePhotos()
    {
        $image = '';
        $notices = '';
        $header = $this->header('Add Photos', 'add-photos-to-gallery');
        if (!isset($_GET['gallery']))
        {
            return redirect('/admin/photos');
        }
        elseif ($_GET['gallery'] == '')
        {
            return redirect('/admin/photos');
        }
        $photo = DB::table('photo_gallery')->where('id', '=', $_GET['gallery'])->first();
        if (isset($_POST['update']))
        {
            if (!$photo)
            {
                abort(404);
            }
            if (request()->file('image'))
            {
                $name = request()->file('image')
                    ->getClientOriginalName();
                $file = md5(time()) . '.' . request()->file('image')
                        ->getClientOriginalExtension();
                $path = base_path() . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'gallery' . DIRECTORY_SEPARATOR . 'photos/';
                request()->file('image')
                    ->move($path, $file);
                $img = Image::make($path . $file)->resize(800, null, function ($constraint)
                {
                    $constraint->aspectRatio();
                })
                    ->save($path . $file);
                $thumb = Image::make($path . $file)->resize(165, 110)
                    ->save($path . 'thumbs/' . $file);
                DB::insert("INSERT INTO photos (gallery_id,image,priority) VALUE (" . $photo->id . ",'" . $file . "'," . $_POST['priority'] . ")");
            }
            $notices .= "<div class='alert mini alert-success'> Photo has been successfully added !</div>";
        }
        if (isset($_GET['delete']))
        {
            DB::table("photos")->where('id', '=', $_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'>Photo has been deleted successfully !</div>";
            return redirect('admin/update-photos?gallery=' . $_GET['gallery']);
        }
        $footer = $this->footer();
        $photo_gallery = DB::table('photos')->orderBy('id', 'ASC')
            ->get();
        $photos = DB::table('photos')->where('gallery_id', '=', $photo->id)
            ->orderBy('priority')
            ->get();
        return view('admin/update-photos')
            ->with(compact('photo_gallery', 'header', 'footer', 'notices', 'photo', 'photos'));
    }

    public function CategoryupdateQuestions()
    {
        $notices = '';
        $header = $this->header('Update Category Questions', 'category-question-entry');
        if (isset($_POST['update']))
        {
            DB::table("service_category_questions")->where('service_id', '=', $_GET['edit'])->where('question_id', '=', $_POST['question'])->delete();
            DB::insert("INSERT INTO service_category_questions (service_id,question_id,priority) VALUE ('" . $_GET['edit'] . "','" . $_POST['question'] . "','" . $_POST['priority'] . "')");
            $notices .= "<div class='alert mini alert-success'> Question has been successfully added !</div>";
        }
        if (isset($_GET['delete']))
        {
            DB::table("service_category_questions")->where('id', '=', $_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'>Question has been deleted successfully !</div>";
        }
        $footer = $this->footer();
        $questions = DB::table('questions')->orderBy('question', 'ASC')
            ->get();
        $service_questions = DB::table('service_category_questions')->where('service_id', '=', $_GET['edit'])->orderBy('priority')
            ->get();
        return view('admin/category-update-questions')
            ->with(compact('questions', 'header', 'footer', 'notices', 'service_questions'));
    }

    public function layout_plan_questions($slug = '')
    {
        if ($slug == '')
        {
            abort(404);
        }
        $plan = DB::table('layout_plans')->where('slug', '=', $slug)->first();
        if (!$plan)
        {
            abort(404);
        }
        $notices = '';
        $header = $this->header('Update Layout Plan Questions', 'layout-plan-questions-entry');
        if (isset($_POST['update']))
        {
            DB::table("layout_plan_questions")->where('plan_id', '=', $plan->id)
                ->where('question_id', '=', $_POST['question'])->delete();
            DB::insert("INSERT INTO layout_plan_questions (plan_id,question_id,priority) VALUE (" . $plan->id . "," . $_POST['question'] . "," . $_POST['priority'] . ")");
            $notices .= "<div class='alert mini alert-success'> Question has been successfully added !</div>";
        }
        if (isset($_GET['delete']))
        {
            DB::table("layout_plan_questions")->where('id', '=', $_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'>Question has been deleted successfully !</div>";
        }
        $footer = $this->footer();
        $questions = DB::table('questions')->orderBy('question', 'ASC')
            ->get();
        $service_questions = DB::table('layout_plan_questions')->where('plan_id', '=', $plan->id)
            ->orderBy('priority')
            ->get();
        return view('admin/layout-update-questions')
            ->with(compact('questions', 'header', 'footer', 'notices', 'plan', 'service_questions'));
    }

    public function layout_plan_addon($slug = '')
    {
        if ($slug == '')
        {
            abort(404);
        }
        $plan = DB::table('layout_plans')->where('slug', '=', $slug)->first();
        if (!$plan)
        {
            abort(404);
        }
        $notices = '';
        $header = $this->header('Add Layout Plan Addon', 'layout-plan-addon-entry');
        if (isset($_POST['update']))
        {
            DB::table("layout_plan_addon")->where('layout_plan_id', '=', $plan->id)
                ->where('layout_addon_id', '=', $_POST['layout_addon_id'])->delete();
            DB::insert("INSERT INTO layout_plan_addon (layout_plan_id,layout_addon_id) VALUE (" . $plan->id . "," . $_POST['layout_addon_id'] . ")");
            $notices .= "<div class='alert mini alert-success'> Addon has been successfully added !</div>";
        }
        if (isset($_GET['delete']))
        {
            DB::table("layout_plan_addon")->where('id', '=', $_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'>Addon has been deleted successfully !</div>";
        }
        $footer = $this->footer();
        $addons = DB::table('layout_addon')->get();
        $plan_addons = DB::table('layout_plan_addon')->where('plan_id', '=', $plan->id)
            ->get();
        return view('admin/layout_plan_addon')
            ->with(compact('addons', 'header', 'footer', 'notices', 'plan', 'plan_addons'));
    }

    public function layout_plan_addon_delete($slug, $addon_id)
    {
        $notices = '';
        if ($slug == '')
        {
            abort(404);
        }
        $plan = DB::table('layout_plans')->where('slug', '=', $slug)->first();
        if (!$plan)
        {
            abort(404);
        }
        DB::table("layout_plan_addon")->where('id', '=', $addon_id)->delete();
        $notices .= "<div class='alert alert-success'>Addon has been deleted successfully !</div>";
        $header = $this->header('Update Layout Plan Addons', 'layout-plan-addons-entry');
        $footer = $this->footer();
        $addons = DB::table('layout_addon')->get();
        $service_questions = DB::table('layout_plan_addon')->where('layout_plan_id', '=', $plan->id)
            ->orderBy('priority')
            ->get();
        return back();
    }

    public function layout_plan_delete_question($slug, $que_id)
    {
        $notices = '';
        if ($slug == '')
        {
            abort(404);
        }
        $plan = DB::table('layout_plans')->where('slug', '=', $slug)->first();
        if (!$plan)
        {
            abort(404);
        }
        DB::table("layout_plan_questions")->where('id', '=', $que_id)->delete();
        $notices .= "<div class='alert alert-success'>Question has been deleted successfully !</div>";
        $header = $this->header('Update Layout Plan Questions', 'layout-plan-questions-entry');
        $footer = $this->footer();
        $questions = DB::table('questions')->orderBy('question', 'ASC')
            ->get();
        $service_questions = DB::table('layout_plan_questions')->where('plan_id', '=', $plan->id)
            ->orderBy('priority')
            ->get();
        return back();
    }

    public function Categoryquestions()
    {
        $notices = '';
        if (isset($_POST['add']))
        {
            $service_id = $_POST['service_id'];
            $question_id = $_POST['question_id'];
            $priority = $_POST['priority'];
            DB::insert("INSERT INTO service_category_questions (service_id,question_id,priority) VALUE ('$service_id','$question_id','$priority')");
            $notices .= "<div class='alert mini alert-success'> Question has been successfully added !</div>";
        }
        if (isset($_POST['edit']))
        {
            $service_id = $_POST['service_id'];
            $question_id = $_POST['question_id'];
            $priority = $_POST['priority'];
            DB::update("UPDATE service_category_questions SET service_id = '$service_id',question_id = '$question_id',priority = '$priority' WHERE id = '" . $_GET['edit'] . "'");
            $notices .= "<div class='alert mini alert-success'> Data Updated successfully !</div>";
        }
        if (isset($_GET['delete']))
        {
            DB::table("service_category_questions")->where('id', '=', $_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'> Question has been deleted successfully !</div>";
        }
        $header = $this->header('Service Category Question Master', 'service_category_questions');
        $services = DB::table("services_category")->orderBy('id', 'DESC')
            ->get();
        $que = DB::table('service_category_questions')->orderBy('id', 'DESC')
            ->get();
        $questions = DB::table('questions')->orderBy('id', 'DESC')
            ->get();
        if (isset($_GET['edit']))
        {
            $ques = DB::table('service_category_questions')->where('id', '=', $_GET['edit'])->first();
        }
        $footer = $this->footer();
        return view('admin/service_category_questions')
            ->with(compact('header', 'notices', 'services', 'questions', 'ques', 'que', 'question', 'option_1', 'option_2', 'option_3', 'option_4', 'weightage', 'type', 'footer'));
    }

    public function Categoryimage()
    {
        $notices = '';
        $header = $this->header('Update Category Images', 'category-image-entry');
        if (isset($_POST['update']))
        {
            $gallery_category_id = $_POST['gallery_category_id'];
            $image_1 = $_POST['image_1'];
            $image_2 = $_POST['image_2'];
            $image_3 = $_POST['image_3'];
            $image_4 = $_POST['image_4'];
            $image_5 = $_POST['image_5'];
            DB::insert("INSERT INTO services_category_image (service_category_id,gallery_category_id,image_1,image_2,image_3,image_4,image_5) 
            VALUE ('" . $_GET['edit'] . "','" . $gallery_category_id . "','" . $image_1 . "','" . $image_2 . "','" . $image_3 . "','" . $image_4 . "','" . $image_5 . "')");
            $notices .= "<div class='alert mini alert-success'> Image has been successfully added !</div>";
        }
        if (isset($_GET['delete']))
        {
            DB::table('services_category_image')->where('id', '=', $_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'>Question has been deleted successfully !</div>";
        }
        if (isset($_GET['delete']))
        {
            DB::table('services_category_image')->where('service_category_id', '=', $_GET['edit'])->where('gallery_category_id', '=', $_POST['gallery_category_id'])->delete();
            $notices .= "<div class='alert mini alert-success'> Image has been successfully deleted !</div>";
        }
        $footer = $this->footer();
        $category = DB::table('design_category')->orderBy('id', 'ASC')
            ->get();
        $gallery = DB::table('services_category_image')->where('service_category_id', '=', $_GET['edit'])->orderBy('id')
            ->get();
        return view('admin/service_category_image')
            ->with(compact('category', 'header', 'footer', 'gallery', 'notices', 'service_questions'));
    }

    public function Getimage()
    {
        $option = '<option value="">Please Select Image</option>';
        $category_id = $_POST['category_id'];
        $images = DB::table('photo_gallery')->where('category', '=', $category_id)->get();
        foreach ($images as $image)
        {
            $option .= '
            <option value="' . $image->id . '">' . $image->title . '</option>';
        }
        return $option;
    }

    public function getVariant(Request $request)
    {
        $html = '';
        $response = array();
        $units = DB::table('units')->orderBy('name', 'ASC')
            ->get();
        $lengths = DB::table('length')->orderBy('length', 'ASC')
            ->get();
        $sizes = DB::table('size')->orderBy('id', 'DESC')
            ->get();
        $response = DB::table('product_variants')->where('id', '=', $_POST['id'])->first();
        $si = DB::table('size')->where('id', $response->variant_title)
            ->first();
        $price = DB::table('multiple_size')->where('size_id', $si->id)
            ->get();
        $sizeWeight = DB::table('size_weight')->where('size_id', $si->id)
            ->first();
        $html = '<form action="products?edit_variants=' . $_POST['id'] . '&ret_id=' . $_POST['ret_id'] . '" method="post" enctype="multipart/form-data" style="max-width:100%">
            ' . csrf_field() . '
					<fieldset>
						  <div class="form-group">
								<label class="control-label">Variant Title</label>
								<select name="variant_title" class="form-control select2">';
        foreach ($sizes as $size)
        {
            $sselected = '';
            if ($size->id == $response->variant_title)
            {
                $sselected = 'selected';
            }
            $html .= '<option value="' . $size->id . '" ' . $sselected . '>' . $size->name . '</option>';
        }
        $html .= '</select>
						  </div>
						  <div class="form-group">
							<label class="control-label">Length</label>';
        if ($sizeWeight)
        {
            $html .= '<span>' . $sizeWeight->length . ' - ' . getUnitSymbol($sizeWeight->lunit) . '<input type="hidden" value="' . $sizeWeight->length . '" name="length"></span>';
        }
        $html .= '</div>
						  <div class="form-group">
							<label class="control-label">Weight</label>';
        if ($sizeWeight)
        {
            $html .= '<span>' . $sizeWeight->weight . ' - ' . getUnitSymbol($sizeWeight->wunit) . '<input type="hidden" value="' . $sizeWeight->weight . '" name="weight"></span>';
        }
        $html .= '</div>
						  <div class="form-group">
							<label class="control-label">Size Difference</label>
							<select name="price" class="form-control select2">';
        if ($price)
        {
            foreach ($price as $pr)
            {
                $lus = '';
                if ($pr->id == $response->price)
                {
                    $lus = 'selected';
                }
                $html .= '<option value="' . $pr->id . '" ' . $lus . '>' . $pr->price . '</option>';
            }
        }
        $html .= '</select>
						  </div>
						  <div class="form-group">
							Display<br>';
        $ycheck = '';
        $ncheck = '';
        if ($response->display == '1')
        {
            $ycheck = 'checked';
        }
        else
        {
            $ncheck = 'checked';
        }
        $html .= '<label style="margin-right: 5px; font-size: 12px;" class="radio_container">
                                   <input name="display" type="radio" id="dyes" value="1" ' . $ycheck . '/>
                                   Yes
                                   <input name="display" type="radio" id="dyes" value="1" ' . $ycheck . '/>
                                   <span class="checkmark"></span>
                               </label> <br> 
                               <label style="margin-right: 5px; font-size: 12px;" class="radio_container">
                                   <input name="display" type="radio" id="dno" value="0" ' . $ncheck . ' />
                                   NO
                                   <input name="display" type="radio" id="dno" value="0" ' . $ncheck . '/>
                                   <span class="checkmark"></span>
                               </label>
						  </div>
						  <div class="form-group">
							Manufacture<br>';
        $ycheck = '';
        $ncheck = '';
        if ($response->manufacture == '1')
        {
            $ycheck = 'checked';
        }
        else
        {
            $ncheck = 'checked';
        }
        $html .= '<label style="margin-right: 5px; font-size: 12px;" class="radio_container">
                                   <input name="manufacture" type="radio" id="myes" value="1" ' . $ycheck . '/>
                                   Yes
                                   <input name="manufacture" type="radio" id="myes" value="1" ' . $ycheck . '/>
                                   <span class="checkmark"></span>
                               </label> <br> 
                               <label style="margin-right: 5px; font-size: 12px;" class="radio_container">
                                   <input name="manufacture" type="radio" id="mno" value="0" ' . $ncheck . ' />
                                   NO
                                   <input name="manufacture" type="radio" id="mno" value="0" ' . $ncheck . '/>
                                   <span class="checkmark"></span>
                               </label>
						  </div>
						  <input name="edit_variant" type="submit" value="Edit Variant" class="btn btn-primary btn-sm"/>
						  
					</fieldset>
				</form>';
        return $html;
    }

    public function getVariantData(Request $request)
    {
        $var = DB::table('size')->where('id', $request->id)
            ->first();
        if ($var !== null)
        {
            $l = explode(',', $var->length);
            $data['length1'] = '';
            if (isset($l[0]))
            {
                $data['length1'] = $l[0];
            }
            $data['length2'] = '';
            if (isset($l[1]))
            {
                $data['length2'] = $l[1];
            }
            $data['length3'] = '';
            if (isset($l[2]))
            {
                $data['length3'] = $l[2];
            }
            $data['length4'] = '';
            if (isset($l[3]))
            {
                $data['length4'] = $l[3];
            }
            $price = DB::table('multiple_size')->where('size_id', $var->id)
                ->get();
            $p = '';
            if ($price)
            {
                foreach ($price as $pr)
                {
                    $p .= '<option value="' . $pr->id . '">' . $pr->price . '</option>';
                }
            }

            $size_weight = DB::table('size_weight')->where('size_id', $var->id)
                ->first();
            $psw = '';
            if ($size_weight !== null)
            {
                $data['length_value'] = $size_weight->length . ' - ' . getUnitSymbol($size_weight->lunit);
                $data['weight_value'] = $size_weight->weight . ' - ' . getUnitSymbol($size_weight->wunit);
            }

            $data['price'] = $p;
            $data['w_available'] = $var->w_available;
            $data['l_available'] = $var->l_available;
            $data['s_available'] = $var->s_available;
            $data['weight'] = $var->weight;
            $data['light'] = $var->light;
            $data['super_light'] = $var->super_light;
            $data['wunit'] = $var->wunit;
            return response()
                ->json($data, 200);
        }
        return response()->json(['error' => 'Failed'], 500);
    }

    public function getDiscount(Request $request)
    {
        $response = array();
        $response = DB::table('product_discount')->where('id', '=', $_POST['id'])->first();
        return json_encode($response);
    }

    /*Abhijeet code add*/
    public function myProduct(Request $request)
    {
        $cid = $request->id;
        $notices = '';
        if (isset($_GET['approve']))
        {
            DB::update("UPDATE products SET status = 'approve' WHERE id = '" . $_GET['approve'] . "'");
            $notices .= "<div class='alert alert-success'> Customer Approve successfully !</div>";
        }
        if (isset($_GET['reject']))
        {
            DB::update("UPDATE products SET status = 'pending' WHERE id = '" . $_GET['reject'] . "'");
            $notices .= "<div class='alert alert-success'> Customer Reject successfully !</div>";
        }
        $header = $this->header('Seller', 'seller');
        $products = DB::table('products')->where('added_by', '=', $cid)->get();
        $username = DB::table('customers')->where('id', '=', $cid)->get();
        $footer = $this->footer();
        return view('admin/myproduct')
            ->with(compact('header', 'notices', 'username', 'products', 'footer'));
    }
    /// ABHIJEET CODE START
    public function institutional()
    {
        $data['notices'] = '';
        if (isset($_GET['delete']))
        {
            DB::table('customers')->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Customer deleted successfully !</div>";
        }
        if (isset($_GET['approve']))
        {
            DB::update("UPDATE customers SET status = '1' WHERE id = '" . $_GET['approve'] . "'");
            $data['notices'] .= "<div class='alert alert-success'> Customer Approve successfully !</div>";
        }
        if (isset($_GET['reject']))
        {
            DB::update("UPDATE customers SET status = '0' WHERE id = '" . $_GET['reject'] . "'");
            $data['notices'] .= "<div class='alert alert-success'> Customer Reject successfully !</div>";
        }
        if (isset($_POST['add_price']))
        {
            $price = $_POST['amount'];
            $id = $_GET['add_price'];
            DB::update("UPDATE customers SET institutional_price = '$price' WHERE id = '$id'");
            $data['notices'] .= "<div class='alert alert-success'> Set Amount successfully !</div>";
        }
        if (isset($_GET['add_discount']))
        {
            $data['category'] = DB::table('category')->get();
            $data['customer_discount'] = DB::table('customer_discount')->where('customer_id', $_GET['add_discount'])->get();
        }
        if (isset($_GET['edit']))
        {
            $data['category'] = DB::table('category')->get();
            $data['customerDiscount'] = DB::table('customer_discount')->where('id', $_GET['edit'])->first();
        }
        if (isset($_GET['edit_discount']))
        {
            $data['category'] = DB::table('category')->get();
            $data['lengths'] = DB::table('customer_discount')->where('id', '=', $_GET['edit_discount'])->first();
        }
        if (isset($_POST['add_discount']))
        {
            $datas['category'] = $_POST['category'];
            $datas['discount'] = $_POST['discount'];
            $datas['action'] = $_POST['action'];
            $datas['discount_type'] = $_POST['discount_type'];
            $datas['customer_id'] = $_GET['add_discount'];
            DB::table('customer_discount')->insert($datas);
            $data['notices'] .= "<div class='alert alert-success'> Discount added successfully !</div>";
        }
        if (isset($_GET['deleteDiscount']))
        {
            DB::table('customer_discount')->where('id', '=', $_GET['deleteDiscount'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Discount deleted successfully !</div>";
        }
        if (isset($_POST['edit_discount']))
        {
            $cat = $_POST['category'];
            $discount = $_POST['discount'];
            $action = $_POST['action'];
            $discount_type = $_POST['discount_type'];
            $id = $_GET['edit_discount'];
            DB::update("UPDATE customer_discount SET category='$cat', discount='$discount', action='$action', discount_type='$discount_type' WHERE id=$id");
            $data['notices'] .= "<div class='alert alert-success'> Discount edited successfully !</div>";
        }
        $data['header'] = $this->header('Institutional', 'institutional');
        $data['customers'] = DB::table('customers')->where('user_type', '=', 'institutional')
            ->get();
        $data['tp'] = url("/themes/" . $this
                ->cfg
                ->theme);
        $data['footer'] = $this->footer();
        return view('admin/institutional')
            ->with('data', $data);
    }

    public function sellerView()
    {
        $notices = '';
        if (isset($_GET['id']))
        {
            $pid = $_GET['id'];
            $customer = DB::table('customers')->where('id', '=', $pid)->first();
        }
        if (isset($_POST['update']))
        {
            $id = $_POST['cid'];
            $name = $_POST['name'];
            $email = $_POST['email'];
            $mobile = $_POST['mobile'];
            $company = $_POST['company'];
            $pan = $_POST['pan'];
            $gst = $_POST['gst'];
            $creditCharges = $_POST['credit_charges'];
            DB::update("UPDATE customers SET name = '$name',email = '$email',mobile = '$mobile',company = '$company',pan = '$pan',gst = '$gst',credit_charges = '$creditCharges' WHERE id = '" . $id . "'");
            $notices = "<div class='alert alert-success mini'> Profile updated successfully </div>";
        }
        $tp = url("/themes/" . $this
                ->cfg
                ->theme);
        $header = $this->header('Seller', 'seller');
        $footer = $this->footer();
        return view('admin/sellerview')
            ->with(compact('header', 'notices', 'tp', 'customer', 'footer'));
    }
    public function MatchSku()
    {
        $sku_get = $_POST['sku'];
        $sku_exp = explode('-', $sku_get);
        $sku = $sku_exp[0];
        $product = DB::table('products')->where('sku', 'LIKE', "%$sku%")->get();
        if (count($product) == 1)
        {
            echo "not_match";
        }
        else
        {
            echo "match";
        }
    }

    public function GetAmount()
    {
        $bal_amt = DB::table('multiseller_payments')->where('seller_id', '=', $_POST['seller_id'])->where('order_id', '=', $_POST['order_id'])->orderBy('id', 'DESC')
            ->first();
        $amt = DB::table('multiseller_payments')->where('seller_id', '=', $_POST['seller_id'])->where('order_id', '=', $_POST['order_id'])->orderBy('id', 'ASC')
            ->first();
        $response = array(
            'balance_amount' => array(
                $bal_amt->balance_amount
            ) ,
            'amount' => array(
                $amt->amount
            )
        );
        return json_encode($response);
    }

    public function BestForAdvices()
    {
        $notices = '';
        if (isset($_POST['add']))
        {
            $data['link'] = $_POST['link'];
            $data['title'] = $_POST['title'];
            $data['content'] = $_POST['content'];
            $post = DB::table('best_for_advices')->insertGetId($data);
            if (request()->file('image'))
            {
                $bname = request()->file('image')
                    ->getClientOriginalName();
                $bfile = md5(time()) . '.' . request()->file('image')
                        ->getClientOriginalExtension();
                $bpath = base_path() . '/assets/' . 'products/';
                request()->file('image')
                    ->move($bpath, $bfile);
                $img = Image::make($bpath . $bfile)->resize(1200, null, function ($constraint)
                {
                    $constraint->aspectRatio();
                })
                    ->save($bpath . $bfile);
                DB::update("UPDATE best_for_advices SET image = '" . $bfile . "' WHERE id = '" . $post . "'");
            }
            $notices .= "<div class='alert mini alert-success'> Best For Advices has been successfully added !</div>";
        }
        if (isset($_POST['edit']))
        {
            $link = $_POST['link'];
            $category = $_POST['title'];
            $content = $_POST['content'];
            if (request()->file('image'))
            {
                $bname = request()->file('image')
                    ->getClientOriginalName();
                $bfile = md5(time()) . '.' . request()->file('image')
                        ->getClientOriginalExtension();
                $bpath = base_path() . '/assets/' . 'products/';
                request()->file('image')
                    ->move($bpath, $bfile);
                $img = Image::make($bpath . $bfile)->resize(1200, null, function ($constraint)
                {
                    $constraint->aspectRatio();
                })
                    ->save($bpath . $bfile);
                DB::update("UPDATE best_for_advices SET image = '" . $bfile . "' WHERE id = '" . $_GET['edit'] . "'");
            }
            DB::update("UPDATE best_for_advices SET link = '$link', title = '$category', content = '$content' WHERE id = '" . $_GET['edit'] . "'");
            $notices .= "<div class='alert mini alert-success'> Data Updated successfully !</div>";
        }
        if (isset($_GET['delete']))
        {
            DB::table('best_for_advices')->where('id', '=', $_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'> Link has been deleted successfully !</div>";
        }
        $header = $this->header('Best For Category Master', 'best-for-cat');
        $bestforcats = DB::table('best_for_photo')->orderBy('id', 'DESC')
            ->get();
        if (isset($_GET['edit']))
        {
            $bestforcat = DB::table('best_for_advices')->where('id', '=', $_GET['edit'])->first();
        }
        $footer = $this->footer();
        return view('admin/best-for-advices')
            ->with(compact('header', 'notices', 'bestforcats', 'bestforcat', 'footer'));
    }

    public function Length()
    {
        $notices = '';
        if (isset($_POST['add']))
        {
            $data['name'] = $_POST['name'];
            $data['symbol'] = $_POST['symbol'];
            $data['unit'] = $_POST['unit'];
            $post = DB::table('units')->insertGetId($data);
            $notices .= "<div class='alert mini alert-success'> Unit has been successfully added !</div>";
        }
        if (isset($_POST['edit']))
        {
            $name = $_POST['name'];
            $symbol = $_POST['symbol'];
            $unit = $_POST['unit'];
            DB::update("UPDATE units SET name = '$name', symbol = '$symbol', unit = '$unit' WHERE id = '" . $_GET['edit'] . "'");
            $notices .= "<div class='alert mini alert-success'> Data Updated successfully !</div>";
        }
        if (isset($_GET['delete']))
        {
            DB::table('units')->where('id', '=', $_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'> Link has been deleted successfully !</div>";
        }
        $header = $this->header('Length Master', 'length');
        $links = DB::table('units')->orderBy('id', 'DESC')
            ->get();
        if (isset($_GET['edit']))
        {
            $link = DB::table('units')->where('id', '=', $_GET['edit'])->first();
        }
        $footer = $this->footer();
        return view('admin/length')
            ->with(compact('header', 'notices', 'links', 'link', 'footer'));
    }

    public function getSizesGroup()
    {
        $id = $_POST['id'];
        $units = DB::table('units')->orderBy('name', 'ASC')
            ->get();
        $grou = DB::table('sizes_group')->where('id', '=', $id)->first();
        $sid = explode(',', $grou->sizes);
        $html = '';
        $i = 0;
        foreach ($sid as $gr)
        {
            $sp = DB::table('size')->where('id', '=', $gr)->first();
            if ($sp !== null)
            {
                $html .= '<tr style=" border: 1px solid #000000;">
            <td style="padding: 10px;">
                <label class="control-label">Variant Title</label>
                <input type="hidden" name="variant_title[]" class="form-control" value="' . $sp->id . '"  style="background-color: white"> <br>' . $sp->name . '
              </td>
              <td style="padding: 10px;">
                <label class="control-label">Length</label>';
                $sizeWeight = DB::table('size_weight')->where('size_id', $gr)->first();
                if ($sizeWeight)
                {
                    $html .= '<input type="hidden" name="length' . $i . '[]" value="' . $sizeWeight->id . '"><span>' . $sizeWeight->length . ' - ' . $sizeWeight->lunit . '</span></br><span>' . $sizeWeight->weight . ' - ' . $sizeWeight->wunit . '</span>';
                }
                $html .= '</td>
              <td style="padding: 10px;">
                <label class="control-label">Size Difference</label>
                <select  name="price[' . $sp->id . ']" class="form-control select2" style="background-color: white" >';
                $price = DB::table('multiple_size')->where('size_id', $gr)->get();
                $p = '';
                if ($price)
                {
                    foreach ($price as $pr)
                    {
                        $html .= '<option value="' . $pr->id . '">' . $pr->price . '</option>';
                    }
                }
                $html .= '</select>
              </td>
              <td style="padding: 10px;">
                 Display<br>
                <label style="margin-right: 5px; font-size: 12px;" class="radio_container">
                    <input name="display' . $i . '" type="radio" id="dyes" value="1" />
                    Yes
                    <input name="display' . $i . '" type="radio" id="dyes" value="1" />
                    <span class="checkmark"></span>
                </label> <br> 
                <label style="margin-right: 5px; font-size: 12px;" class="radio_container">
                    <input name="display' . $i . '" type="radio" id="dno" value="0" checked />
                    NO
                    <input name="display' . $i . '" type="radio" id="dno" value="0" checked/>
                    <span class="checkmark"></span>
                </label>
              </td>
              <td style="padding: 10px;">
                Manufacture<br>
                 <label style="margin-right: 5px; font-size: 12px;" class="radio_container">
                    <input name="manufacture' . $i . '" type="radio" id="myes" value="1" />
                    Yes
                    <input name="manufacture' . $i . '" type="radio" id="myes" value="1" />
                    <span class="checkmark"></span>
                </label> <br> 
                <label style="margin-right: 5px; font-size: 12px;" class="radio_container">
                    <input name="manufacture' . $i . '" type="radio" id="mno" value="0" checked />
                    NO
                    <input name="manufacture' . $i . '" type="radio" id="mno" value="0" checked/>
                    <span class="checkmark"></span>
                </label>
              </td>
          </tr>';
                $i++;
            }
        }
        $html .= '<tr>
            <td colspan="7" style="text-align: right"><input name="add_group_variant" type="submit" value="Add Variant" style="float: right" class="btn btn-primary btn-sm"/></td>
          </tr>';
        return $html;
    }
    public function getVariantGroup()
    {
        $id = $_POST['id'];
        $units = DB::table('units')->orderBy('name', 'ASC')
            ->get();
        $grou = DB::table('sizes_group')->where('id', '=', $id)->first();
        $sid = explode(',', $grou->sizes);
        $html = '';
        $i = 0;
        foreach ($sid as $gr)
        {
            $sp = DB::table('size')->where('id', '=', $gr)->first();
            $html .= '<tr style=" border: 1px solid #000000;">
            <td style="padding: 10px;">
                <label class="control-label">Variant Title</label>
                <input type="hidden" name="variant_title[]" class="form-control" value="' . htmlspecialchars($sp->name) . '"  style="background-color: white"> <br>' . $sp->name . '
              </td>
              <td style="padding: 10px; width: 225px;">
                    <label class="control-label">Length</label>
                    <div class="col-lg-12">&nbsp;</div>
                    <input type="text" name="length1[]" class="form-control"  style="background-color: white; width: 50px; float: left; border: 1px solid #000000;">&nbsp;
                    <input type="text" name="length2[]" class="form-control"  style="background-color: white; width: 50px; float: left; border: 1px solid #000000;">&nbsp;
                    <input type="text" name="length3[]" class="form-control"  style="background-color: white; width: 50px; float: left; border: 1px solid #000000;">&nbsp;
                    <input type="text" name="length4[]" class="form-control"  style="background-color: white; width: 50px; float: left; border: 1px solid #000000;">
                         
              </td>
              <td style="padding: 10px;">
                    <label class="control-label">Unit</label>
                    <select name="lunit[]" class="form-control" style="background-color: white">';
            foreach ($units as $uni)
            {
                $html .= '<option value="' . $uni->name . '">' . $uni->name . '</option>';
            }
            $html .= '</select>
              </td>
              <td style="padding: 10px;">
                <label class="control-label">Size Difference</label>
                <input name="price[]" type="text" class="form-control"  value="' . $sp->price . '" style="background-color: white"/>
              </td>
              <td style="padding: 10px; width: 220px;">
                    <div style="width: 65px; float: left;">
                        <label class="control-label" style="font-size: 10px;">Heavy <input type="checkbox" name="w_available" value="available"> </label>
                        <input name="weight[]" type="text" value="' . $sp->weight . '" class="form-control" style="background-color: white; border: 1px solid #000000;"/>
                    </div>
                    <div style="width: 65px; float: left;">
                        <label class="control-label" style="font-size: 10px;">Light <input type="checkbox" name="l_available" value="available"></label>
                        <input name="light[]" type="text" class="form-control" style="background-color: white; border: 1px solid #000000;"/>
                    </div>
                    <div style="width: 65px; float: left;">
                        <label class="control-label" style="font-size: 10px;">S. Light <input type="checkbox" name="s_available" value="available"></label>
                        <input name="super_light[]" type="text" class="form-control" style="background-color: white; border: 1px solid #000000;"/>
                    </div>
              </td>
              <td style="padding: 10px;">
                    <label class="control-label">Unit</label>
                    <select name="wunit[]" class="form-control" style="background-color: white">';
            foreach ($units as $unit)
            {
                $html .= '<option value="' . $unit->name . '">' . $unit->name . '</option>';
            }
            $html .= '</select>
              </td>
              <td style="padding: 10px;">
                <label class="control-label">Display</label><br>
                <input name="display' . $i . '" type="radio" id="yes" value="1" /><label for="yes">Yes</label> <br> 
                <input name="display' . $i . '" type="radio" id="no" value="0" checked /><label for="no">No</label>
              </td>
              <td style="padding: 10px;">
                <label class="control-label">Manufacture</label><br>
                <input name="manufacture' . $i . '" type="radio" id="yes" value="1" checked /><label for="yes">Yes</label> <br> 
                <input name="manufacture' . $i . '" type="radio" id="no" value="0" /><label for="no">No</label>
              </td>
          </tr>';
            $i++;
        }
        $html .= '<tr>
            <td colspan="7" style="text-align: right"><input name="add_group_variant" type="submit" value="Add Variant" style="float: right" class="btn btn-primary btn-sm"/></td>
          </tr>';
        return $html;
    }
    public function changeRfr(Request $request)
    {
        $cust = Customer::find($request->id);
        if ($cust !== null)
        {
            if ($request->type == 'rfq')
            {
                $cust->rfr = $request->rfr;
            }
            elseif ($request->type == 'show_price')
            {
                $cust->show_price = $request->show_price;
            }
            $cust->save();
            return 'success';
        }
        return 'failed';
    }

    public function notice(Request $request)
    {
        define('API_ACCESS_KEY', 'AAAAd2L0SKc:APA91bHWNCFq52FHj1XBUMjD_dUn6iaCVdV0EZMKTjoSPygXCOLCEIUfLnoPHePCMPriu7l63ASRpgvPJSbXN_cErCsGacQMxUvLWsVXOv9YcZJj3hL6avefq5BdwfBug3h7aHaupKtA');
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $tokenList = 'cGPr63b0VdE:APA91bHxW1TMQS6bONI2JX9L64DMF302B__0EpgXLeKUCwh5zgt3beCotIAOFKDX6sYmcZUrViU1paV69blXlpmoztZ4NcP8pNtQLPCivfGN6xh-F3jePVOmBTorhx4xBVmWzoE-o155,dtRMVYW4Y7w:APA91bFOnY7lSj42DT02wPRVo6q_1voGrqbUf4g3M45osOL1nnGAG6nc4mHEKc3s7srViNa28IbvLT16YYyHirLplVSihLzazYCLV-9rucJ0q55qtXP2zeuOxym1cr2EVTAkdLjG2WeU';
        $notification = ['title' => 'Aakar360', 'body' => 'Welcome Aakar360.', 'icon' => 'myIcon', 'sound' => 'mySound'];
        $extraNotificationData = ["message" => $notification, "moredata" => 'dd'];
        $fcmNotification = ['registration_ids' => $tokenList, //multple token array
            'notification' => $notification, 'data' => $extraNotificationData];

        $headers = ['Authorization: key=' . API_ACCESS_KEY, 'Content-Type: application/json'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);
        echo $result;
    }

    public function variantMultipleDelete(Request $request)
    {
        $ids = $request->ids;
        DB::table('product_variants')
            ->whereIn('id', explode(",", $ids))->delete();
        return response()
            ->json(['status' => true, 'message' => "variants deleted successfully."]);
    }

    public function variantData()
    {
        $group = (!empty($_GET["group"])) ? ($_GET["group"]) : 0;
        $retData = null;
        if ($group)
        {
            $sg = DB::table('sizes_group')->where('id', $group)->first();
            if ($sg !== null)
            {
                $sizes = explode(',', $sg->sizes);
                $retData = DB::table('size')->whereIn('id', $sizes);
            }
            else
            {
                $retData = DB::table('size');
            }
        }
        else
        {
            $retData = DB::table('size');
        }
        $users = $retData->select(['id', 'name'])
            ->orderBy('name', 'ASC');
        return Datatables::of($users)->addColumn('price', function ($users)
        {
            $price = DB::table('multiple_size')->where('size_id', $users->id)
                ->get();
            $p = array();
            if ($price)
            {
                foreach ($price as $pr)
                {
                    $variants = DB::table('product_variants')->join('products', 'products.id', '=', 'product_variants.product_id')
                        ->where('product_variants.price', $pr->id)
                        ->select('products.title')
                        ->groupBy('product_variants.product_id')
                        ->get();
                    $px = $pr->price;
                    $v = array();
                    foreach ($variants as $var)
                    {
                        $v[] = $var->title;
                    }
                    $p[] = '<b><u>' . $px . '</u></b> - ' . implode(', ', $v);
                }
            }
            return implode('<br/><hr/>', $p);
        })->addColumn('action', function ($users)
        {
            return '<a href="size?delete=' . $users->id . '"><i class="icon-trash"></i></a>
                <a href="size?edit=' . $users->id . '"><i class="icon-pencil"></i></a>';
        })
            ->rawColumns(['price', 'action'])
            ->make(true);
    }

    public function getMultipleData(Request $request)
    {
        $html = '';
        $sg = DB::table('sizes_group')->where('id', $request->id)
            ->first();
        if ($sg !== null)
        {
            $sizes = explode(',', $sg->sizes);
            $retData = DB::table('size')->whereIn('id', $sizes)->get();
            foreach ($retData as $ret)
            {
                $html .= '<div class="col-lg-12" style=" border: 1px solid #000000; padding-bottom: 5px;">
                    <div class="col-lg-3">
                        ' . $ret->name . '
                        <input type="hidden" name="size_id[]" value="' . $ret->id . '" class="form-control">
                    </div>
					<div class="col-lg-3">
						Priority
                        <input type="text" name="priority[' . $ret->id . '][]" value="' . $ret->priority . '" class="form-control" >
                    </div>
					
					';
                $price = DB::table('multiple_size')->where('size_id', $ret->id)
                    ->get();
                if (count($price))
                {
                    $p = array();
                    $html .= '<div class="col-lg-6">';
                    foreach ($price as $pr)
                    {
                        $html .= '<input type="hidden" name="id[]" value="' . $pr->id . '">';
                        $variants = DB::table('product_variants')->join('products', 'products.id', '=', 'product_variants.product_id')
                            ->where('product_variants.price', $pr->id)
                            ->select('products.title')
                            ->groupBy('product_variants.product_id')
                            ->get();
                        $px = '<input type="text" name="price[' . $ret->id . '][' . $pr->id . ']" value="' . $pr->price . '" class="form-control" style="height: 30px !important;">';
                        $v = array();
                        foreach ($variants as $var)
                        {
                            $v[] = $var->title;
                        }
                        $p[] = $px . ' - ' . implode(', ', $v);

                    }
                    $html .= implode('<br/><hr/>', $p);
                    $html .= '</div>';
                    $html .= '<div id="add_option" class="button pull-right mini-button add_option" data-key="' . $ret->id . '"><i class="icon-plus"></i></div>
                    <div class="col-lg-12 options" id="options' . $ret->id . '"></div><div class="col-lg-1"></div>';
                }
                else
                {
                    $html .= '<div class="col-lg-6">';
                    $html .= '<input type="text" name="new_price[' . $ret->id . '][]" class="form-control" style="height: 30px !important;">';
                    $html .= '</div>';
                    $html .= '<div id="add_option" class="button pull-right mini-button add_option" data-key="' . $ret->id . '"><i class="icon-plus"></i></div>
                    <div class="col-lg-12 options' . $ret->id . '" id="options"></div><div class="col-lg-1"></div>';
                }
                $html .= '</div><div class="col-lg-12">&nbsp;</div> ';
            }
        }
        return $html;
    }

    public function getCustomerDiscount(Request $request)
    {
        $html = '';
        $response = array();
        $category = DB::table('category')->get();
        $lengths = DB::table('customer_discount')->where('id', '=', $_POST['id'])->first();
        $html = '<form action="institutional?edit_discount=' . $_POST['id'] . '&ret_id=' . $_POST['ret_id'] . '" method="post" class="form-horizontal" enctype="multipart/form-data" style="max-width: 100%;">
		   ' . csrf_field() . '
			<div class="row">
				<div class="form-group col-md-3" style="padding-left: 30px;padding-right:30px;">
					<label class="control-label">Category</label>
					<select id="user_type" name="category" class="form-control" required="required">
						<option value="">Select Category </option>';
        foreach ($category as $size)
        {
            $catsel = '';
            if ($lengths->category == $size->id)
            {
                $catsel = 'selected';
            }
            $html .= '<option value="' . $size->id . '" ' . $catsel . '>' . htmlspecialchars($size->name) . '</option>';
        }
        $html .= '</select>
				</div>
				  
				<div class="form-group col-md-2" style="padding-left: 30px;padding-right:30px;">
					<label class="control-label">Discount</label>
					<input name="discount" type="text" class="form-control" value="' . $lengths->discount . '" required="required"/>
				</div>
				<div class="form-group col-md-3" style="padding-left: 30px;padding-right:30px;">
					<label class="control-label">Discount Action</label>
					<select name="action" class="form-control" required="required">
						<option value="">Select Discount Action </option>
						<option value="0"';
        if ($lengths->action == 0)
        {
            $html .= "selected";
        }
        $html .= '> + </option>
						<option value="1"';
        if ($lengths->action == 1)
        {
            $html .= "selected";
        }
        $html .= '> - </option>
					</select>
				</div>
				<div class="form-group col-md-3" style="padding-left: 30px;padding-right:30px;">
					<label class="control-label">Discount Type</label>
					<select name="discount_type" class="form-control" required="required">
						<option value="">Select Discount Type </option>
						<option value="0"';
        if ($lengths->discount_type == 0)
        {
            $html .= "selected";
        }
        $html .= '> % </option>
						<option value="1"';
        if ($lengths->discount_type == 1)
        {
            $html .= "selected";
        }
        $html .= '> Flat </option>
					</select>
				</div>
				<input name="edit_discount" type="submit" value="Edit Discount" style="padding:3px 25px;" class="btn btn-primary col-md-2" />
			</div>
		</form>';
        return $html;
    }

    public function getCitiesData(Request $request)
    {
        $states = State::where('country_id', '101')->get()->pluck('id');
        $records = City::whereIn('state_id', $states)->orderBy('name', 'ASC');

        $state_id = (isset($_GET["state_id"])) ? $_GET["state_id"] : false;
        if ($state_id)
        {
            $records = $records->where('state_id', $state_id);
        }
        return Datatables::of($records)->addColumn('action', function ($records) use ($request)
        {
            $html = '<a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="cities?edit=' . $records->id . '"><i class="material-icons">edit</i></a>
                <a style="padding:0 10px; margin-left: 10px" data-toggle="tooltip" title="Add Pincodes" href="cities?add_pincodes=' . $records->id . '"><i class="icon-plus"></i></a>';
            return $html;
        })->editColumn('state_id', function ($records)
        {
            return getStateName($records->state_id);
        })
            ->rawColumns(['action'])
            ->make(true);
    }
}