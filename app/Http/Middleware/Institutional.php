<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;

class Institutional
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(DB::select("SELECT COUNT(*) as count FROM customers WHERE sid = '".session('institutional')."' ")[0]->count > 0){
            return $next($request);
        }
        return redirect(route('business.institutionalInstitutionalLogin'));
    }
}
