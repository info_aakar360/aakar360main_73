<?php

namespace App\Http\Middleware;

use App\Otp;
use Illuminate\Support\Facades\DB;
use Closure;
use Illuminate\Support\Facades\Session;

class CrmLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $ipaddress =  getIp();
        $sip = Session::get('ipaddress');
        $secure = Session::get('crm');
        if(empty($secure)){
            return redirect('crm/login');
        }
        $icheck = Otp::where('status','1')
            ->where(function ($q) use ($sip, $ipaddress) {
                $q->where('ipaddress', $ipaddress)->orWhere('ipaddress', $sip);
            })->first();
        if(!empty($icheck)) {
            $time = date('Y-m-d H:i:s');
            $updated = $icheck->updated_at;
            if(!empty($updated)){
                $hours = differenceInHours($updated,$time);

                if($hours > 12)
                {
                    return redirect(route('otp'));
                }
                if (DB::select("SELECT COUNT(*) as count FROM user WHERE secure = '" . $secure . "' ")[0]->count > 0) {
                    return $next($request);
                }
                return redirect('crm/login');
            }else{
                return redirect('crm/login');
            }
        }else{
            return redirect('crm/login');
        }
    }
}
