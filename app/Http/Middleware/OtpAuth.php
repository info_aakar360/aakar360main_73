<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Otp;

class OtpAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $ipaddress =  getIp();
        $sip = Session::get('ipaddress');
        $icheck = Otp::where('ipaddress', $ipaddress)->orWhere('ipaddress', $sip)->first();
        if(empty($icheck)){
            return redirect('crm/otp');
        }
        return $next($request);
    }
}
