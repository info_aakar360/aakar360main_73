<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class QuickQuote extends Model
{
    protected $table = 'quick_quote';
}
