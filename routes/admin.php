<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "Admin" middleware group. Enjoy building your Admin!
|
*/

Route::match(['get', 'post'],'/login', [ 'as' => 'login', 'uses' => 'Admin@login']);
Route::get('/', 'Admin@index')->middleware('Login');
Route::get('/logout', 'Admin@logout')->middleware('Login');
Route::get('/map', 'Admin@map')->middleware('Login');
Route::match(['get', 'post'],'/products', 'Admin\ProductController@products')->middleware('Login');
Route::match(['get', 'post'],'/banners', 'Admin\FrontendController@banners')->middleware('Login');
Route::match(['get', 'post'],'/s-how-it-works', 'Admin\ServiceController@s_how_it_works')->middleware('Login');
Route::match(['get', 'post'],'/home-page-design', 'Admin\FrontendController@home_page_design')->middleware('Login');
Route::match(['get', 'post'],'/mega-menu-image', 'Admin\FrontendController@mega_menu_image')->middleware('Login');
Route::match(['get', 'post'],'/categories', 'Admin\ProductController@categories')->middleware('Login');
Route::match(['get', 'post'],'/brands', 'Admin\ProductController@brands')->middleware('Login');
Route::match(['get', 'post'],'/manufacturing-hubs', 'Admin\ProductController@manufacturingHubs')->middleware('Login');
/*Route::match(['get', 'post'],'/ph-relations', 'Admin@phRelations')->middleware('Login');*/
Route::match(['get', 'post'],'/featured', 'Admin\ProductController@featured')->middleware('Login');
Route::match(['get', 'post'],'/services_categories', 'Admin\ServiceController@services_categories')->middleware('Login');
Route::match(['get', 'post'],'/services', 'Admin\ServiceController@services')->middleware('Login');
Route::match(['get', 'post'],'/design_category', 'Admin\DesignController@design_category')->middleware('Login');
Route::match(['get', 'post'],'/layout-plans', 'Admin\DesignController@layout_plan')->middleware('Login');
Route::match(['get', 'post'],'/photos', 'Admin\DesignController@photos')->middleware('Login');
Route::match(['get', 'post'],'/update-photos', 'Admin@updatePhotos')->middleware('Login');
Route::match(['get', 'post'],'/update-questions', 'Admin@updateQuestions')->middleware('Login');
Route::match(['get', 'post'],'/questions', 'Admin@questions')->middleware('Login');
Route::match(['get', 'post'],'/category-update-questions', 'Admin@CategoryupdateQuestions')->middleware('Login');
Route::match(['get', 'post'],'/services_answer', 'Admin\ServiceController@ServicesAnswer')->middleware('Login');
Route::match(['get', 'post'],'/modify_plan', 'Admin\DesignController@modifyPlan')->middleware('Login');
Route::match(['get', 'post'],'/faq', 'Admin\ServiceController@Faq')->middleware('Login');
Route::match(['get', 'post'],'/layout_faq', 'Admin\DesignController@layoutFaq')->middleware('Login');
Route::match(['get', 'post'],'/product_faq', 'Admin\ProductController@productFaq')->middleware('Login');
Route::match(['get', 'post'],'/service_category_questions', 'Admin@Categoryquestions')->middleware('Login');
Route::match(['get', 'post'],'/service_category_image', 'Admin@Categoryimage')->middleware('Login');
Route::match(['get', 'post'],'/get_image', 'Admin@Getimage')->middleware('Login');
Route::match(['get', 'post'],'/pages', 'Admin\FrontendController@pages')->middleware('Login');
Route::match(['get', 'post'],'/blog', 'Admin\AdviceController@blog')->middleware('Login');
Route::get('/customers', 'Admin\ProductController@customers')->middleware('Login');
Route::post('/change-rfr', 'Admin@changeRfr')->middleware('Login');
Route::match(['get', 'post'],'/coupons', 'Admin\ProductController@coupons')->middleware('Login');
Route::match(['get', 'post'],'/postcodes', 'Admin\SettingsController@postcodes')->middleware('Login');
Route::match(['get', 'post'],'/shipping', 'Admin\SettingsController@shipping')->middleware('Login');
Route::match(['get', 'post'],'/flat', 'Admin\SettingsController@flat')->middleware('Login');
Route::match(['get', 'post'],'/units', 'Admin\ProductController@units')->middleware('Login');
Route::match(['get', 'post'],'/reviews', 'Admin\ProductController@reviews')->middleware('Login');
Route::match(['get', 'post'],'/photo-reviews', 'Admin\DesignController@photoreviews')->middleware('Login');
Route::match(['get', 'post'],'/layout_reviews', 'Admin\DesignController@layout_reviews')->middleware('Login');
Route::match(['get', 'post'],'/orders', 'Admin\RetailController@orders')->middleware('Login');
Route::get('/stats', 'Admin\RetailController@stats')->middleware('Login');
Route::match(['get', 'post'],'/tracking', 'Admin\MarketingController@tracking')->middleware('Login');
Route::match(['get', 'post'],'/newsletter', 'Admin\MarketingController@newsletter')->middleware('Login');
Route::get('/referrers', 'Admin\VisitorController@referrers')->middleware('Login');
Route::get('/os', 'Admin\VisitorController@os')->middleware('Login');
Route::get('/browsers', 'Admin\VisitorController@browsers')->middleware('Login');
Route::match(['get', 'post'],'/payment', 'Admin\SettingsController@payment')->middleware('Login');
Route::match(['get', 'post'],'/currency', 'Admin\SettingsController@currency')->middleware('Login');
Route::match(['get', 'post'],'/settings', 'Admin\SettingsController@settings')->middleware('Login');
Route::match(['get', 'post'],'/theme', 'Admin\SettingsController@theme')->middleware('Login');
Route::match(['get', 'post'],'/lang', 'Admin\SettingsController@lang')->middleware('Login');
Route::get('/tokens', 'Admin\SettingsController@tokens')->middleware('Login');
Route::get('/export', 'Admin\SettingsController@export')->middleware('Login');
Route::match(['get', 'post'],'/slider', 'Admin\CustomizationController@slider')->middleware('Login');
Route::match(['get', 'post'],'/editor', 'Admin\CustomizationController@editor')->middleware('Login');
Route::match(['get', 'post'],'/templates', 'Admin\CustomizationController@templates')->middleware('Login');
Route::match(['get', 'post'],'/builder', 'Admin\CustomizationController@builder')->middleware('Login');
Route::match(['get', 'post'],'/menu', 'Admin\CustomizationController@menu')->middleware('Login');
Route::match(['get', 'post'],'/bottom', 'Admin\CustomizationController@bottom')->middleware('Login');
Route::match(['get', 'post'],'/fields', 'Admin@fields')->middleware('Login');
Route::match(['get', 'post'],'/support', 'Admin\AdministrationController@support')->middleware('Login');
Route::match(['get', 'post'],'/administrators', 'Admin\AdministrationController@administrators')->middleware('Login');
Route::match(['get', 'post'],'/profile', 'Admin\AdministrationController@profile')->middleware('Login');
Route::match(['get', 'post'],'/layout_addon', 'Admin\DesignController@layout_addon')->middleware('Login');
Route::match(['get', 'post'],'/layout-plans/add-questions/{slug}', 'Admin@layout_plan_questions')->middleware('Login');
Route::match(['get', 'post'],'/layout-plans/layout_plan_addon/{slug}', 'Admin@layout_plan_addon')->middleware('Login');
Route::match(['get', 'post'],'/amminities', 'Admin\DesignController@amminities')->middleware('Login');
Route::match(['get', 'post'],'/image_likes', 'Admin\DesignController@image_likes')->middleware('Login');
Route::match(['get', 'post'],'/image_inspirations', 'Admin\DesignController@image_inspirations')->middleware('Login');
Route::match(['get', 'post'],'/plan_orders', 'Admin\DesignController@plan_orders')->middleware('Login');
Route::get('/layout-plans/delete/{slug}/{que_id}', 'Admin@layout_plan_delete_question')->middleware('Login');
Route::get('/layout-plans/delete/{slug}/{addon_id}', 'Admin@layout_plan_addoon_delete')->middleware('Login');
Route::post('/get-variant', 'Admin@getVariant')->middleware('Login');
Route::post('/get-variant-data', 'Admin@getVariantData')->middleware('Login');
Route::post('/get-discount', 'Admin@getDiscount')->middleware('Login');
Route::match(['get', 'post'],'/advices_faq', 'Admin\AdviceController@advicesFaq')->middleware('Login');
Route::match(['get', 'post'],'/advices_reviews', 'Admin\AdviceController@advices_reviews')->middleware('Login');
Route::match(['get', 'post'],'/advices_category', 'Admin\AdviceController@advices_category')->middleware('Login');
Route::match(['get', 'post'],'/best-for-cat', 'Admin\ProductController@BestForCat')->middleware('Login');
Route::match(['get', 'post'],'/services_reviews', 'Admin@services_reviews')->middleware('Login');
Route::match(['get', 'post'],'/product-type', 'Admin\ProductController@productType')->middleware('Login');
Route::match(['get', 'post'],'/filters', 'Admin\ProductController@filters')->middleware('Login');
Route::match(['get', 'post'],'/best-for-layout', 'Admin\DesignController@BestForLayout')->middleware('Login');
Route::match(['get', 'post'],'/design-cat-banner', 'Admin\DesignController@DesignCatBanner')->middleware('Login');
Route::match(['get', 'post'],'/best-for-photo', 'Admin\DesignController@BestForPhoto')->middleware('Login');
Route::match(['get', 'post'],'/length', 'Admin@Length')->middleware('Login');
Route::match(['get', 'post'],'/size', 'Admin\ProductController@Size')->middleware('Login');
Route::match(['get', 'post'],'/sizes_group', 'Admin\ProductController@SizesGroup')->middleware('Login');
Route::match(['get', 'post'],'/get_sizes_group', 'Admin@getSizesGroup')->middleware('Login');
Route::match(['get', 'post'],'/get_variant_group', 'Admin@getVariantGroup')->middleware('Login');

Route::match(['get', 'post'],'/sellers-payment', 'Admin\SellerController@sellersPayment')->middleware('Login');
Route::match(['get', 'post'],'/get-amount', 'Admin@GetAmount');
Route::match(['get', 'post'],'/cr-note', 'Admin\SellerController@CrNote');
Route::match(['get', 'post'],'/dr-note', 'Admin\SellerController@DrNote');
Route::match(['get', 'post'],'/seller_po', 'Admin\SellerController@SellerPo');
Route::match(['get', 'post'],'/default_image', 'Admin\FrontendController@DefaultImage');
Route::match(['get', 'post'],'/link', 'Admin\FrontendController@Link');
Route::match(['get', 'post'],'/catData', 'Admin@catData');
Route::match(['get', 'post'],'/product_sr', 'Admin\RetailController@productSr');

Route::get('/seller', 'Admin\SellerController@seller')->middleware('Login');
Route::match(['get', 'post'],'institutional', 'Admin@institutional')->middleware('Login');
Route::match(['get', 'post'],'bookings', 'Admin\InstitutionalController@bookings')->middleware('Login');
Route::match(['get', 'post'],'dispatch-programs', 'Admin\InstitutionalController@dispatchPrograms')->middleware('Login');
Route::match(['get', 'post'],'request-quotation', 'Admin\InstitutionalController@requestQuotation')->middleware('Login');
Route::match(['get', 'post'],'/sellerview', 'Admin@sellerView')->middleware('Login');
Route::get('/pendingproduct', 'Admin\SellerController@pendingproduct')->middleware('Login');
Route::get('/deleteproduct', 'Admin\SellerController@deleteproduct')->middleware('Login');
Route::get('/approvalproduct', 'Admin\SellerController@approvalproduct')->middleware('Login');
Route::get('/rejectproduct', 'Admin\SellerController@rejectproduct')->middleware('Login');
Route::match(['get', 'post'],'/match-sku', 'Admin@MatchSku');
Route::get('/myproduct', 'Admin@myProduct')->middleware('Login');
Route::get('/notice', 'Admin@notice')->middleware('Login');
Route::match(['get', 'post'],'/registerimage', 'Admin\CustomizationController@Registerimage')->middleware('Login');
Route::match(['get', 'post'],'/tmtcalculator', 'Admin\CalculatorController@Tmtcalculator')->middleware('Login');
Route::match(['get', 'post'],'/notification', 'Admin\NotificationController@Notification')->middleware('Login');
Route::match(['get', 'post'],'/retail_notification', 'Admin\NotificationController@retailNotification')->middleware('Login');
Route::match(['get', 'post'],'/request-for-call', 'Admin\InstitutionalController@requestForCall')->middleware('Login');
Route::match(['get', 'post'],'/quick-quote', 'Admin\InstitutionalController@quickQuote')->middleware('Login');
Route::match(['get', 'post'],'/mentor-program', 'Admin\InstitutionalController@mentorProgram')->middleware('Login');
Route::match(['get', 'post'],'/credit-charges', 'Admin\InstitutionalController@creditCharges')->middleware('Login');
Route::match(['get', 'post'],'/counter-offer', 'Admin\InstitutionalController@counterOffer')->middleware('Login');
Route::post('/get-customer-discount', 'Admin@getCustomerDiscount')->middleware('Login');
Route::match(['get', 'post'],'/cities', 'Admin\SettingsController@cities')->middleware('Login');
Route::match(['get', 'post'],'/get-cities-data', 'Admin@getCitiesData')->middleware('Login');

Route::post('/variantMultipleDelete', 'Admin@variantMultipleDelete')->middleware('Login');
Route::post('/get-multiple-data', 'Admin@getMultipleData')->middleware('Login');
Route::get('variantData', 'Admin@variantData')->middleware('Login');

Route::match(['get', 'post'],'/dealers', 'Admin\ProductController@dealers')->middleware('Login');