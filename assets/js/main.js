﻿(function($) {

    "use strict";

    // preloader
    $(window).load(function(){
        $('#preloader').fadeOut('slow',function(){$(this).remove();});
    });


    // ScrollToFixed
    $('.fixed-header').scrollToFixed();


    // flexslider Start
    $('.flexslider').flexslider({
        animation: "fade",
        animationSpeed: 1000,
        slideshowSpeed: 7000,
        controlNav: true,
        directionNav: false,
        playText: 'Play',
    });


    // Tab Slider
    $('#myCarousel').carousel({
        interval:   4000
    });

    var clickEvent = false;
    $('#myCarousel').on('click', '.nav a', function() {
        clickEvent = true;
        $('.nav li').removeClass('active');
        $(this).parent().addClass('active');
    }).on('slid.bs.carousel', function(e) {
        if(!clickEvent) {
            var count = $('.nav').children().length -1;
            var current = $('.nav li.active');
            current.removeClass('active').next().addClass('active');
            var id = parseInt(current.data('slide-to'));
            if(count == id) {
                $('.nav li').first().addClass('active');
            }
        }
        clickEvent = false;
    });



    // Parallax background
    $('.jarallax').jarallax({
        speed: 0.5,
    })

    // Water ripples animation
    /*$.ripples("#water-animation");
    $('#water-animation').ripples({
        resolution: 512,
        dropRadius: 20,
        perturbance: 0.04
    });*/


    // Light box
    $('.gallery').featherlightGallery({
        gallery: {
            fadeIn: 300,
            fadeOut: 300
        },
        openSpeed: 300,
        closeSpeed: 300
    });
    $('.gallery2').featherlightGallery({
        gallery: {
            next: 'next »',
            previous: '« previous'
        },
        variant: 'featherlight-gallery2'
    });


    // owl-carousel for main-slider
    $('.main-slider').owlCarousel({
        loop:true,
        margin: 0,
        dots:false,
        nav:false,
        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause:false,
        autoplaySpeed:1200,
        navText: [
            '<i class="fa fa-long-arrow-left" aria-hidden="true"></i>',
            '<i class="fa fa-long-arrow-right" aria-hidden="true"></i>'
        ],
        responsive: {
            0: {
                items:1
            },
            600:{
                items:1
            },
            1000: {
                items:1
            }
        }
    });


    // slick-carousel for agent
    $(".agent-carousel").slick({
        dots: false,
        arrows: true,
        infinite: false,
        autoplay: false,
        speed: 800,
        autoplaySpeed: 3000,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 750,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 470,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }

        ]

    });
	$(".agent-carousel-2").slick({
        dots: false,
        arrows: true,
        infinite: false,
        autoplay: false,
        speed: 800,
        autoplaySpeed: 3000,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 750,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 470,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }

        ]

    });
	$(".agent-carousel-3").slick({
        dots: false,
        arrows: true,
        infinite: false,
        autoplay: true,
        speed: 800,
        autoplaySpeed: 3000,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 750,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 470,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }

        ]

    });
    $(".agent-carousel-4").slick({
        dots: false,
        arrows: true,
        infinite: false,
        autoplay: false,
        speed: 800,
        autoplaySpeed: 3000,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 620,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }

        ]

    });
	$(".regular").slick({
		dots: false,
        arrows: true,
        infinite: false,
        autoplay: false,
        speed: 800,
		slidesToShow: 3,
		slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 470,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }

        ]
	});
	// slick-carousel for agent
    $(".brand-carousel").slick({
        dots: false,
        arrows: true,
        infinite: false,
        autoplay: false,
        speed: 800,
        autoplaySpeed: 3000,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 750,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }

        ]

    });

    // slick-carousel for brand
    $(".brand-carousel").slick({
        dots: false,
        arrows: false,
        infinite: true,
        autoplay: true,
        speed: 800,
        autoplaySpeed: 3000,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 750,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }

        ]

    });
    $(".pro-image-slider").slick({
        dots: false,
        arrows: true,
        infinite: false,
        autoplay: false,
        speed: 800,
        autoplaySpeed: 3000,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 750,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }

        ]

    });


    // Range slider
    $(".slider-range").slider({
        range: true,
        min: 0,
        max: 5000,
        values: [ 500, 3000 ],
        slide: function( event, ui ) {
            $( ".amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
        }
    });
    $( ".amount" ).val( "$" + $( ".slider-range" ).slider( "values", 0 ) +
        " - $" + $( ".slider-range" ).slider( "values", 1 ) );

    // Range slider two
    $( ".slider-range-two" ).slider({
        range: true,
        min: 0,
        max: 3000,
        values: [ 300, 1200 ],
        slide: function( event, ui ) {
            $( ".amount-two" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
        }
    });
    $( ".amount-two" ).val( "$" + $( ".slider-range-two" ).slider( "values", 0 ) +
        " - $" + $( ".slider-range-two" ).slider( "values", 1 ) );



    // Select changer script
    $(document).on('change', '.div-toggle', function() {
        var target = $(this).data('target');
        var show = $("option:selected", this).data('show');
        $(target).children().addClass('hide');
        $(show).removeClass('hide');
    });
    $(document).ready(function(){
        $('.div-toggle').trigger('change');
    });



    // Text rotating script
    $( "#text-animation" ).morphext({
        // The [in] animation type. Refer to Animate.css for a list of available animations.
        animation: "fadeInDown",
        // An array of phrases to rotate are created based on this separator. Change it if you wish to separate the phrases differently (e.g. So Simple | Very Doge | Much Wow | Such Cool).
        separator: ",",
        speed: 4000,
        complete: function () {
            // Called after the entrance animation is executed.
        }
    });


    // Bootstrap accordion
    function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .find(".more-less")
            .toggleClass('glyphicon-plus glyphicon-minus');
    }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);


    //Scrollup
    dyscrollup.init({
        showafter : 500,
        scrolldelay : 1000,
        position : 'right',
        shape : 'squre',
        width : "20",
        height : "20"
    });
	$('#ca-container').contentcarousel();
$('#ca-container1').contentcarousel();
	
	


})(window.jQuery);

function getRelatedPlans(){
		$.ajax({
		url: 'get-related-plans',
		type: 'post',
		data: 'slug='+$('input[name=design_slug]').val()+'&_token='+$('input[name=_token]').val(),
		beforeSend: function(){
			$('#data1').html('');
		},
		success: function(data){
			$('#data1').html(data);
		},
		complete: function(data){
			var $container = $('.grid');
			$container.masonry({
				  columnWidth:10, 
				  gutterWidth: 15,
				  itemSelector: '.grid-item'
			});
		}
	});
}
function getRecentViewed(){
	$.ajax({
		url: 'get-recent-viewed-plans',
		type: 'post',
		data: 'slug='+$('input[name=design_slug]').val()+'&_token='+$('input[name=_token]').val(),
		beforeSend: function(){
			$('#data2').html('');
		},
		success: function(data){
			$('#data2').html(data);
		},
		complete: function(data){
			var $container = $('.grid');
			$container.masonry({
				  columnWidth:10, 
				  gutterWidth: 15,
				  itemSelector: '.grid-item'
			});
		}
	});
}
function getRelatedGallery(){
		$.ajax({
		url: 'get-related-galleries',
		type: 'post',
		data: 'slug='+$('input[name=gallery_slug]').val()+'&_token='+$('input[name=_token]').val(),
		beforeSend: function(){
			$('#data1').html('');
		},
		success: function(data){
			$('#data1').html(data);
		},
		complete: function(data){
			var $container = $('.grid');
			$container.masonry({
				  columnWidth:10, 
				  gutterWidth: 15,
				  itemSelector: '.grid-item'
			});
		}
	});
}
function getRecentViewedGallery(){
	$.ajax({
		url: 'get-recent-viewed-galleries',
		type: 'post',
		data: 'slug='+$('input[name=gallery_slug]').val()+'&_token='+$('input[name=_token]').val(),
		beforeSend: function(){
			$('#data2').html('');
		},
		success: function(data){
			$('#data2').html(data);
		},
		complete: function(data){
			var $container = $('.grid');
			$container.masonry({
				  columnWidth:10, 
				  gutterWidth: 15,
				  itemSelector: '.grid-item'
			});
		}
	});
}

$("#owl-demo").owlCarousel({
    autoPlay: true,
    infinite: true,
    items: 1,
    itemsDesktop: [1199, 1],
    itemsDesktopSmall: [979, 1]
});

$("#owl-product").owlCarousel({
    autoPlay: true,
    infinite: true,
    items: 4,
    itemsDesktop: [1199, 4],
    itemsDesktopSmall: [979, 4]
});

$("#brand-demo").owlCarousel({
    autoPlay: true,
    infinite: true,
    items: 6,
    itemsDesktop: [1199, 6],
    itemsDesktopSmall: [979, 6]
});
$(".bregular").slick({
        dots: false,
        arrows: true,
        infinite: false,
        autoplay: false,
        speed: 800,
        slidesToShow: 6,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 6,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 470,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }

        ]
    });
    
    /*$("#featured-products").owlCarousel({
        lazyLoad: !0,
            loop: !0,
            margin: 0,
            responsiveClass: !0,
            nav: !1,
            navText: ['<i class="icon-angle-left">', '<i class="icon-angle-right">'],
            dots: !0,
            autoplay: !0,
            autoplayTimeout: 15e3,
            items: 1
    });*/
    $("#carousel-custom-dots .owl-dot").click(function() {
        $(".product-single-carousel").trigger("to.owl.carousel", [e(this).index(), 300]);
    });
	function animateCSS(element, animationName, callback) {
		var node = $(element);
		node.addClass('animated '+animationName);

		function handleAnimationEnd() {
			node.removeClass('animated '+animationName);
			node.unbind('animationend');

			if (typeof callback === 'function') callback()
		}

		node.on('animationend', handleAnimationEnd)
	}
	$('.at-header-topbar a[href*="#"]').on('click', function(e) {
		  e.preventDefault()

		  $('html, body').animate(
			{
			  scrollTop: $($(this).attr('href')).offset().top - 80,
			},
			500,
			'linear'
		  );
		  setTimeout(function(){
			  animateCSS('.anim1', 'fadeIn', false);
		  }, 800);
		});
		var ad_list_top = [];
		var ad_list_bottom = [];
		var translate = [];
		$('.verticleCarouselContainer').find('.vcx_item:first-child').addClass('vis');
		if($('.verticleCarouselContainer').length){
			$('.verticleCarouselContainer').each(function(index){
				$(this).find('.adviceList').attr('data-index', index);
				ad_list_top[index] = 0;
				ad_list_bottom[index] = $(this).find('.adviceList').height();
				translate[index] = 0;
			});
			$('.scroll-up').addClass('disabled');
		}
		
		$('.vc_controls').on('click', function(){
			var ele = $(this);
			if(!ele.hasClass('disabled')){
				var target = $('#'+ele.data('scroll'));
				var visItem = target.find('.vis');
				if(ele.hasClass('scroll-up')){
					if(ad_list_top[target.data('index')] != 0){
						translate[target.data('index')] = translate[target.data('index')] + visItem.height() + 20;
						ad_list_top[target.data('index')] = translate[target.data('index')];
						ad_list_bottom[target.data('index')] = target.height() + translate[target.data('index')];
						target.attr('style', 'transform: translateY('+translate[target.data('index')]+'px)');
						//alert(ad_list_top[target.data('index')]);
						if(ad_list_top[target.data('index')] == 0){
							ele.addClass('disabled');
						}
						if($('.scroll-down').hasClass('disabled')){
							$('.scroll-down').removeClass('disabled');
						}
						if(visItem.prev() !== null){
							visItem.prev().addClass('vis');
							visItem.removeClass('vis');
						}
					}
				}else if(ele.hasClass('scroll-down')){
					if(ad_list_bottom[target.data('index')] != 0){
						if(translate[target.data('index')] - (visItem.height() + 20) + target.height() !== 0){
							translate[target.data('index')] = translate[target.data('index')] - (visItem.height() + 20);
							ad_list_top[target.data('index')] = translate[target.data('index')] + 6;
							ad_list_bottom[target.data('index')] = target.height() + translate[target.data('index')];
							if(ad_list_bottom[target.data('index')] !== 0){
								target.attr('style', 'transform: translateY('+translate[target.data('index')]+'px)');
							}
							if(ad_list_bottom[target.data('index')] == 0){
								ele.addClass('disabled');
							}else if(ad_list_bottom[target.data('index')] - 203 == 0){
								ele.addClass('disabled');
							}else if(ad_list_bottom[target.data('index')] == (visItem.height() + 20)){
								ele.addClass('disabled');
							}
							if($('.scroll-up').hasClass('disabled')){
								$('.scroll-up').removeClass('disabled');
							}
							if(visItem.next() !== null){
								visItem.next().addClass('vis');
								visItem.removeClass('vis');
							}
						}else{
							ele.addClass('disabled');
							if($('.scroll-up').hasClass('disabled')){
								$('.scroll-up').removeClass('disabled');
							}
						}
					}else{
						
					}
				}
			}
		});
		
	jQuery(window).load(function(){

		jQuery('.slider-banner-active').flexslider({
			animation: "fade",
			directionNav : true,
			slideshow: true,
			slideshowSpeed: 7000,
			animationSpeed: 600
		});

	});
		
		
	$('.slide-6').slick({
		dots: false,
        arrows: true,
		infinite: false,
		speed: 300,
		slidesToShow: 6,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 1367,
				settings: {
					slidesToShow: 6,
					slidesToScroll: 1,
				}
			},
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 420,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					margin:0
				}
			}

		]
	});

	$(document).ready(function(){
		window.matchMedia('screen and (max-width: 991px)') 
		{
			if($('.cat_accord')){
				$('.cat_accord').click();
			}
		}
        $('.slide-7').slick({
            dots: false,
            infinite: false,
            speed: 300,
            slidesToShow: 5,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 1367,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 420,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                }

            ]
        });
	});
	$(document).on('click', '*', function(e){
	    if($(this).data('redirect')) {
	        e.preventDefault();
            window.location.href = $(this).data('redirect').toString();
        }
    });
    $(document).ready(function() {
        $(".alert.auto-close").fadeTo(3000, 500).slideUp(500, function() {
            $(".alert.auto-close").slideUp(500);
        });
    });

    $(document).on('click', '.change-hub', function(){
        $('#rateModal').modal({
            backdrop: 'static',
            keyboard: false,
            show: true
        });
    });
function submitHub(){
    if($('input[name=hub]:checked').val()) {
        $('form#hubForm').submit();
    }else{
        alert('Please select rate type to proceed!')
    }
}