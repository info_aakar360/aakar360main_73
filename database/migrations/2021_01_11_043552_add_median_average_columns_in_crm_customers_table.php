<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMedianAverageColumnsInCrmCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *ge
     * @return void
     */
    public function up()
    {
        Schema::table('crm_customers', function (Blueprint $table) {
            $table->decimal('call_average', 10, 2)->default(0.00);
            $table->decimal('call_median', 10, 2)->default(0.00);
            $table->decimal('visit_average', 10, 2)->default(0.00);
            $table->decimal('visit_median', 10, 2)->default(0.00);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_customers', function (Blueprint $table) {
            $table->dropColumn('call_average');
            $table->dropColumn('call_median');
            $table->dropColumn('visit_average');
            $table->dropColumn('visit_median');
        });
    }
}
