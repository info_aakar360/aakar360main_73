<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldToCrmPromotionalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_promotional', function (Blueprint $table) {
            $table->string('crm_type')->default('1')->comment('1 for business,2 for retail,3 for software');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_promotional', function (Blueprint $table) {
            $table->dropColumn('crm_type');
        });
    }
}
