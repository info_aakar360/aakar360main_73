<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRootsToTransportorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transportor', function (Blueprint $table) {
            $table->string('ststate')->nullable();
            $table->string('stdistrict')->nullable();
            $table->string('edstate')->nullable();
            $table->string('eddistrict')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transportor', function (Blueprint $table) {
            $table->dropColumn('st_state');
            $table->dropColumn('st_district');
            $table->dropColumn('ed_state');
            $table->dropColumn('ed_district');
        });
    }
}
