<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lead_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('crm_customer_id');
            $table->date('select_date');
            $table->string('edited_by');
            $table->string('user_id');
            $table->string('pipelines_id');
            $table->string('pipelines_stage');
            $table->string('product_category');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lead_orders');
    }
}
