<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdFieldsToLeadCallLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lead_call_logs', function (Blueprint $table) {
            $table->string('customer_number');
            $table->string('select_date');
            $table->string('message');
            $table->string('user_id');
            $table->string('edited_by');
            $table->string('call_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
