<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSecColumnToTransportorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transportor', function (Blueprint $table) {
            $table->string('sec_names')->nullable();
            $table->string('sec_contacts')->nullable();
            $table->string('sec_landline')->nullable();
            $table->string('sec_designation')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transportor', function (Blueprint $table) {
            //
        });
    }
}
