<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesPipelineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_pipelines', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->default('Default Pipeline');
            $table->string('stages')->default('{"1":"new","2":"Follow-Up","3":"Under Review","4":"Demo","5":"Negotiation","6":"Won","7":"Lost"}');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_pipelines');
    }
}
