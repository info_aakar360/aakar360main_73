<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFreightLastUpdatedToTransportorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transportor', function (Blueprint $table) {
            $table->text('all_freight')->after('freight')->nullable();
            $table->text('freight_last_updated')->after('all_freight')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transportor', function (Blueprint $table) {
            //
        });
    }
}
