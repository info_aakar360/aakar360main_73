<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCityWiseShippingCharges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('city_wise_shipping_charges', function (Blueprint $table) {
            $table->text('pincode')->change();
            $table->integer('category_id')->default(null)->change();
            $table->integer('from_weight')->default(0);
            $table->integer('to_weight')->default(0);
            $table->integer('unit')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('city_wise_shipping_charges', function (Blueprint $table) {
            //
        });
    }
}
