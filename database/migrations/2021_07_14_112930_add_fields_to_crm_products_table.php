<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToCrmProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_products', function (Blueprint $table) {
            $table->string('note')->after('description')->nullable();
            $table->string('mothly_quan')->after('supplier_id')->nullable();
            $table->string('pay_condition')->after('crm_type')->nullable();
            $table->string('pay_delay')->after('pay_condition')->nullable();
            $table->string('order_lifting')->after('pay_delay')->nullable();
            $table->string('credit_limit')->after('order_lifting')->nullable();
            $table->string('status')->after('credit_limit')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_products', function (Blueprint $table) {
            $table->dropColumn('note');
            $table->dropColumn('mothly_quan');
            $table->dropColumn('pay_condition');
            $table->dropColumn('pay_delay');
            $table->dropColumn('order_lifting');
            $table->dropColumn('credit_limit');
            $table->dropColumn('status');
        });
    }
}
